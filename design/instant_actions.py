INSTANT_ACTION_TYPES = ["AddPopulationToPlanet",
        "ApplyArbitraryTargettedBuffToSelf","ApplyBuffToFirstSpawnerNoFilterNoRange",
        "ApplyBuffToIncomingHyperspacers","ApplyBuffToLastSpawnerNoFilterNoRange",
        "ApplyBuffToLastSpawnerWithTravelNoFilterNoRange","ApplyBuffToLocalOrbitBody",
        "ApplyBuffToLocalOrbitBodyWithTravel","ApplyBuffToSelf",
        "ApplyBuffToSelfWithFilter","ApplyBuffToTarget","ApplyBuffToTargetNoFilterNoRange",
        "ApplyBuffToTargetNoRange","ApplyBuffToTargetOnWeaponFired","ApplyBuffToTargetWithEntryVehicles",
        "ApplyBuffToTargetWithTravel","ApplyBuffToTargetWithTravelNoFilterNoRange",
        "ApplyBuffToTargetsAtAdjacentOrbitBodies","ApplyBuffToTargetsAtOrbitBody",
        "ApplyBuffToTargetsInColumn","ApplyBuffToTargetsInDirectionalCone",
        "ApplyBuffToTargetsInRadius","ApplyBuffToTargetsInRadiusOfTargetWithTravel",
        "ApplyBuffToTargetsInRadiusWithChainTravel","ApplyBuffToTargetsInRadiusWithTravel",
        "ApplyBuffToTargetsLinked","ApplyForceFromSpawner","ApplyImpulseFromSpawner",
        "ApplyImpulseInSpawnerDirection","ApplyOrRemoveBuffToSelf",
        "ApplyRelationshipModifierToOwnerOfPlanetInCurrentGravityWell",
        "ApplyTargettedBuffToSelf","ApplyTargettedBuffToSelfNoFilterNoRange",
        "ApplyTargettedBuffToSelfNoRange","AttractDebris","ChangePlayerIndexToFirstSpawner",
        "ChangePlayerIndexToNeutral","ClearRecordedDamage","ColonizePlanet",
        "ConvertDamageToAntiMatter","ConvertFrigateToResources",
        "ConvertNearbyDebrisToHull","ConvertSquadMembersToMines","CreateCannonShell",
        "CreateClonedFrigate","CreateFrigate","CreateFrigateAtArbitraryTarget",
        "CreateFrigateAtTarget","CreateIllusionFighters","CreatePlanetModule",
        "CreateSpaceMine","CreateSquad","CreateStarBase",
        "DoAllegianceChangeToPlanet","DoDamage","DoDamagePerEntityInRadius",
        "DoDamagePerLastSpawnerPopulation","DoDamagePercOfCurrentHull",
        "DoDamageToPlanet","DoInterrupt","DoInterruptUltimate","Explore",
        "ForceAttackersToRepickAttackTarget","GainAntimatterEqualToTarget",
        "GainHullEqualToTarget","GainShieldEqualToTarget","GiveCreditsToPlayer",
        "IncreaseOwnerAbilityLevel","InitializeMovementTowardLastSpawner",
        "InitializeRandomMotion","MakeDead","MatchTargetVelocity","PlayAttachedEffect",
        "PlayDetachedEffectsInRadius","PlayPersistantAttachedEffect",
        "PlayPersistantBeamEffect","PropagateWeaponDamageReceivedToTargetsInRadius",
        "RecordDamage","RemoveAntiMatter","RemoveAntiMatterPerc","RemoveBuffOfType",
        "ResetPhysicsState","RestoreAntiMatter","RestoreHullPoints",
        "RestoreHullPointsPerc","RestoreShieldPoints","RestoreShieldPointsPerc",
        "ResurrectCapitalShip","RetaliateBounty","RetaliateDamage",
        "SetTauntTargetToLastSpawner","SpawnResourceExtractors","SpawnShipsAtPlanet",
        "StealAntiMatterForFirstSpawner","StealResources","TeleportTowardsArbitraryTarget",
        "TeleportTowardsMoveTarget","TeleportTowardsTarget","TiltUpVectorInTargetDirection"]
ActionStealResources = ["StealResources"]
ActionSpawnShips = ["SpawnShipsAtPlanet"]
ActionPropWeaponDmg = ["PropagateWeaponDamageReceivedToTargetsInRadius"]
ActionNumEffect = ["PlayDetachedEffectsInRadius","GiveCreditsToPlayer"]
ActionCreateStarbase = ["CreateStarBase"]
ActionIllusionFighters = ["CreateIllusionFighters"]
ActionCreateShell = ["CreateCannonShell"]
ActionDebrisToHull = ["ConvertNearbyDebrisToHull"]
ActionColonizePlanet = ["ColonizePlanet"]
ActionChangePlayerFirstSpawner = ["ChangePlayerIndexToFirstSpawner"]
ActionSquadToMines = ["ConvertSquadMembersToMines"]
ActionCreateFrgTarget = ["CreateFrigateAtTarget"]
ActionCreateClonedFrg = ["CreateClonedFrigate"]
ActionCreateFrg = ["CreateFrigate"]
ActionCreateUnit = ["CreateFrigateAtArbitraryTarget","CreatePlanetModule",
        "CreateSpaceMine","CreateSquad"]
ActionDoDmgPerEntity = ["DoDamagePerEntityInRadius"]
ActionDoDmg = ["DoDamage","DoDamagePerLastSpawnerPopulation",
        "DoDamagePercOfCurrentHull"]
ActionApplyTargetWeapEffNum = ["ApplyBuffToTargetsInRadiusWithTravel"]
ActionApplyCone = ["ApplyBuffToTargetsInDirectionalCone"]
ActionApplyEntryVehicle = ["ApplyBuffToTargetWithEntryVehicles"]
ActionApplyWeap = ["ApplyBuffToLastSpawnerWithTravelNoFilterNoRange",
        "ApplyBuffToTargetWithTravelNoFilterNoRange"]
ActionApplyTargetWeap = ["ApplyBuffToLocalOrbitBodyWithTravel",
        "ApplyBuffToTargetWithTravel",
        "ApplyBuffToTargetsInRadiusOfTargetWithTravel",
        "ApplyBuffToTargetsInRadiusWithChainTravel"]
ActionApplyTargetEff = ["ApplyBuffToTargetsAtAdjacentOrbitBodies",
        "ApplyBuffToTargetsAtOrbitBody","ApplyBuffToTargetsInColumn",
        "ApplyBuffToTargetsInRadius","ApplyTargettedBuffToSelf","ApplyBuffToTarget"]
ActionApplyEffLvl = ["ApplyArbitraryTargettedBuffToSelf"]
ActionApplyEffWithFilter = ["ApplyBuffToTargetsLinked",
        "ApplyTargettedBuffToSelfNoRange","ApplyBuffToIncomingHyperspacers",
        "ApplyBuffToLocalOrbitBody","ApplyBuffToTargetNoRange"]
ActionWeaponEffect = ["PlayPersistantBeamEffect"]
ActionApplyWithFilter = ["ApplyBuffToSelfWithFilter","ApplyBuffToTargetOnWeaponFired"]
ActionApplyWithEffect = ["ApplyBuffToSelf","ApplyOrRemoveBuffToSelf",
        "ApplyTargettedBuffToSelfNoFilterNoRange"]
ActionWithEffect = ["PlayPersistantAttachedEffect","PlayAttachedEffect"]
ActionApplyBuff = ["ApplyBuffToFirstSpawnerNoFilterNoRange",
        "ApplyBuffToTargetNoFilterNoRange","RemoveBuffOfType",
        "ApplyBuffToLastSpawnerNoFilterNoRange"]
ActionNum = ["AddPopulationToPlanet","AttractDebris","ConvertDamageToAntiMatter",
        "ConvertFrigateToResources","DoAllegianceChangeToPlanet",
        "DoDamageToPlanet",
        "RemoveAntiMatter","RemoveAntiMatterPerc","RestoreAntiMatter",
        "RestoreHullPoints","RestoreHullPointsPerc","RestoreShieldPoints",
        "RestoreShieldPointsPerc","RetaliateBounty","RetaliateDamage",
        "SpawnResourceExtractors","StealAntiMatterForFirstSpawner",
        "ApplyRelationshipModifierToOwnerOfPlanetInCurrentGravityWell",
        "ApplyForceFromSpawner","ApplyImpulseFromSpawner",
        "ApplyImpulseInSpawnerDirection",
        "IncreaseOwnerAbilityLevel","InitializeMovementTowardLastSpawner",
        "InitializeRandomMotion","TeleportTowardsMoveTarget",
        "TeleportTowardsTarget","TiltUpVectorInTargetDirection"]
BuffInstantAction = ["ChangePlayerIndexToNeutral","ClearRecordedDamage",
        "DoInterrupt","DoInterruptUltimate","Explore",
        "ForceAttackersToRepickAttackTarget","GainAntimatterEqualToTarget",
        "GainHullEqualToTarget","GainShieldEqualToTarget","MakeDead",
        "MatchTargetVelocity","RecordDamage","ResetPhysicsState",
        "ResurrectCapitalShip","SetTauntTargetToLastSpawner",
        "TeleportTowardsArbitraryTarget"]
