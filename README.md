PySoaSE
=======
[![build status](https://gitlab.com/pysoase/pysoase/badges/master/build.svg)](https://gitlab.com/pysoase/pysoase/commits/master)

Welcome to PySoaSE!

This Python program is intended for modders working on mods for the PC-Game 
Sins of a Solar Empire. It provides facilities to test mods for syntactical and 
semantical correctness. 

Features
--------

* Written in Python - should work on any modern system
* Test whether referenced *Entity* files are actually present
* Test whether referenced *Brushes* are actually in any of your mod's .brushes files
* Test whether referenced *Strings* are actually in the String files
* Test for the existence of referenced *Explosions*
* Test for the existence of referenced *Asteroid/Dust Cloud definitions*
* Test for the existence of referenced *Particles*
* Test whether referenced Entities/Brushfiles/Player Themes etc. have a 
  corresponding *manifest entry*
* Test whether files referenced in manifests actually exist
* Test whether referenced *Sounds* exist

> PySoaSE ist still in development. Not all Sins files have yet been implemented.

Documentation
-------------

For usage and installation instructions, please refer to the 
[manual](http://pysoase.readthedocs.org/).

Changelog
---------

For a list of changes in each release, see the [Changelog](CHANGELOG.md).

New Ideas
---------

I'm always happy to accept new feature requests. You've come across a problem 
in your mod? Finally figured out that bug which has been pestering you for 
weeks? Please head over to the [Issue Tracker](https://gitlab.com/pysoase/pysoase/issues) 
and open an enhancement request for a new check.

Reporting Bugs
--------------

Please report bugs on the [Issue Tracker](https://gitlab.com/pysoase/pysoase/issues).

When you do, please mention any exceptions and paste the whole output your 
invocation produces. If you can, please also attach the file(s) from your mod 
which produced the error.

Contributions
-------------

If you would like to contribute code, please feel welcome! If you would like 
to contribute, there is really only one rule: Please also add unit tests, with 
the same structure as those already present in the `tests` package. And 
don't worry about extensive tests - for now, I'm mostly interested in simple 
statement coverage. 

Where code quality is concerned: When you have a look at the current code, you 
will see that I should be the last one to tell other people how their code ought 
to look. So code however you like, as long as the code runs correctly. Just 
one request: Use CamelCase naming for your classes and variables. Just for the 
sake of consistency. And yeah yeah I know, Python prescribes the underscore instead,
but I'm coming from C++ and Java, and underscores in code just look weird to me.

Please branch your development off from the dev branch, and just do a 
Pull Request with a few lines explaining what your code does.

If you want to see the architecture, have a look at the classes.uxf file in 
the `design` directory. It can be opened with the Open Source 
[UMLet](http://www.umlet.com/) editor.

If you have any questions at all (and you will, because literally none of the 
code is documented right now ;-) ) just drop me a line.
