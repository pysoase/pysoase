Usage
#####

PySoaSE currently only has a command line interface. This section will describe 
the various commands and their options and inputs.

.. note::
        I would be happy to accept any contribution of a graphical frontend, 
        preferably with a portable toolkit like Qt. If you want to write one,
        contact me to discuss the interface you would need.

Overview
========

The general structure of a PySoaSE invocation is as follows::

        $ pysoase [general options] command [command options] subcommand [subcommand options] [inputs]

To get short help information, call ``pysoase --help``. Help for specific 
commands or subcommands is provided with ``pysoase command --help`` and 
``pysoase command subcommand --help``.

In addition, the ``pysoase`` main command accepts the following options:

-o, --out <file>  Redirects result output to <file> instead of printing 
                  it directly (**ATTENTION**: All contents in <file> will be overridden!)
--version         Outputs the current PySoaSE version and exits
--quiet           Suppresses display of progress bars

When a relative path is provided as input, it is relative to the current working 
directory of the console. You may also enter absolute paths.

.. note::

        When a file path contains spaces, it must be surrounded with quotation 
        marks!

PySoaSE provides the following commands:

check
        This is the main reason PySoaSE exists. The check command provides 
        access to syntax and semantics checks. It checks the syntax, namely 
        the correct spelling, order and existence of required lines in any
        modable Sins file, safe, for the moment, meshes. Furthermore, *check* 
        is able to test semantics. This means to check if referenced files 
        (like Buffs referenced in an Ability entity) exist, as well as checking 
        for the existence of entries in files like ``.string`` or ``.brushes``.
        Additional tests are gleaned from the assertion errors the Sins 
        ``Dev .exe`` displays, like checking whether the number of required 
        research stations for a ResearchSubject correspond to their visual tier 
        in the research window. In general, all given files are checked, and 
        the checking continues even when errors are found. The result is a list 
        of Problems sorted by the file they concern. See :ref:`sec_cmd_check`.
show
        This command is used to display a variety of information about a mod 
        and its files. One example is the ability to extract and display all 
        references in any Sins file. See :ref:`sec_cmd_show`.
modify
        This command allows to actually change your mod's files. It provides 
        a way to batch change a number of lines in a number of files, by 
        providing a modification string with identifiers for the lines and 
        changes to be applied. See :ref:`sec_cmd_modify` for details.

.. _sec_dirs:

Directories
===========

Whenever a ``check`` or ``show`` command is executed, PySoaSE might need more 
information then just the files given on the command line. Imagine, for example,
that you would like to check whether your mod's new ``Great Planet.entity`` file 
has errors. You would call PySoaSE like this, assuming your current working 
directory is also your mod's root directory::

        $ pysoase check general "GameInfo\Great Planet.entity"

To check whether all files referenced exist, PySoaSE would need to know where 
to look for things like String files, Brush files or Meshes. 

Your first option here is to provide your mod's root directory via the 
``--mod`` option. The call would then become::

        $ pysoase check --mod . general "GameInfo\Great Planet.entity"

.. warning::

        The quotation marks around "GameInfo\\Great Planet.entity" in the 
        above examples are important! Command line parameters are delimited by 
        a space. Without the quotation marks, PySoaSE would interpret the 
        example command as having two files as parameters, "GameInfo\\Great" 
        and "Planet.entity".

Again, assuming that your current directory is also your mod's root dir. The 
current directory is denoted by the ``.`` special directory entry on Windows as 
well as Unixoid systems. 

To spare you the hassle of writing out your mod directory for every call, 
PySoaSE can also try to guess the directory from your input files. When you 
don't provide the ``--mod`` option, PySoaSE simply assumes that the files you 
provided on the command line as input are in a valid Sins mod directory 
structure. This means that when you give an entity as the input file, PySoaSE 
is going to assume that that entity file resides in your mod's ``GameInfo`` 
directory, and that the parent directory is going to be your mod's root dir.

Thus, PySoaSE would assume that your mod's ``String`` string directory can be 
found when going one level higher from the directory your entity lives in. 

PySoaSE **always** assumes a standard, valid Sins directory structure, whether 
you provide the mod root via the ``--mod`` option or PySoaSE has to guess it.

.. warning::

        PySoaSE can only handle single directory mods at the moment. Mod 
        chainloading, like it is used in the Sins of the Fallen mod, is not 
        yet supported!

.. _sec_vanilla:

Vanilla Sins Files
------------------

Many mods make use of certain assets from the vanilla game, while not adding 
them to the mod directory. PySoaSE supports such usage, thus preventing false 
positives when searching referenced files which don't exist in a mod but are 
an asset from the base game. For the sake of argument, let's assume your 
``Great Planet.entity`` uses the vanilla ``Shield_PlanetSmall.mesh`` file. In 
this case, when checking your entity for problems, PySoaSE would come across 
that mesh, and would need to check whether such a file actually exists. 

As of PySoaSE v1.2.0, no local Sins directory is needed to use PySoaSE. All 
vanilla files are now listed as a file manifest, distributed with PySoaSE. No 
error messages will be generated from vanilla files missing. This change 
was necessary to support PySoaSE in environments where you don't have access to 
a Sins installation.

But, there is an additional problem. What if you use a vanilla Brush? Or the 
vanilla explosions? When PySoaSE checks references to e.g. Brushes, it can't 
simply look if the file exists. It actually needs to parse the file and check 
whether the Brush you referenced actually exists in any Brush file. But that 
check can't be done with the vanilla files, because those files are in a binary 
format in your Sins directory. If you want PySoaSE to check those references too,
you will need the converted vanilla Rebellion reference files. You can either 
convert them yourself, or you can download a full set, e.g. from 
`ModDB <http://www.moddb.com/company/soase-m0dders/downloads/reference-files>`_.

PySoaSE has no way of knowing where you 
store your vanilla reference files, and you have to provide the path with the 
``--rebref`` option, like so::

        $ pysoase check --rebref "C:\path\to\rebellion\reference" general "GameInfo\Great Planet.entity"

PySoaSE won't crash if you use vanilla files and don't give the 
``rebref`` option, but it will output false errors for your mod's files because 
it could not find the referenced vanilla files. To alleviate this problem,
PySoaSE bundles a number of files, notably those which have themselves entries 
which might be referenced elsewhere, like brush files. Those files are installed 
and used automatically. 

Several PySoaSE commands, most notably the ``unref`` command, still need a full 
Sins Rebellion reference directory with all files converted to TXT.

Having to retype the vanilla paths for every invocation of PySoaSE can get old 
rather fast. As those paths are unlikely to change, you can provide them in a 
simple configuration file. For more information, see :ref:`sec_conf`

.. _sec_conf:

Configuration File
==================

Having to retype your reference files paths, which are unlikely to 
change, for every invocation is wasteful. To ease the burden, you can enter 
them into a configuration file stored in your user's home directory. In most 
Unixoid systems, that's the ``/home/username`` directory, while modern Windows 
versions store user files under ``C:\Users\username``. The file's name must be 
``.pysoase``. Yes, you're seeing right, no file extension. The full path to 
the file on my own Windows installation is::

        $ C:\Users\michael\.pysoase

Where ``michael`` is my Windows user name. The file content has the following 
structure::

        [subcommand]
        rebrefdir=C:\path\to\rebellion\reference

where ``subcommand`` might be one of ``check,show,attribg``. Instead of giving 
values for every command or subcommand, you can also include an ``all`` section.
All parameters in that section are used for any subcommand call which has 
that parameter. So, instead of setting your paths per subcommand::

        [check]
        rebrefdir=/home/michael/projects/soase/mods/rebellion_ref/
        [show]
        rebrefdir=/home/michael/projects/soase/mods/rebellion_ref/

You can also just set the paths in the ``all`` section of the config::

        [all]
        rebrefdir=/home/michael/projects/soase/mods/rebellion_ref/

Storing that file in your user's home directory will allow PySoaSE to find 
your vanilla Sins files without having to provide the paths for every 
invocation.

.. _sec_cmd_check:

The ``check`` command
=====================

The ``check`` command is the main feature of PySoaSE. It can be used to find 
a wide array of problems in mods, ranging from having forgotten to add a new 
ship to the manifest to missing textures.The ``show`` command has the triage 
of directory options already mentioned in :ref:`sec_vanilla`:

-m, --mod <path>  Your mod's root directory
--rebref <path>   The path to the Sins Rebellion reference files
--store <directory path>   Store pickled list of problems in this directory

The additional ``--store`` parameter stores all discovered problems as a
*pickled* list of problems. These files will be named with the current date 
and time in UTC. This list of problems can be loaded via the ``pickle`` module
of the Python Standard Library.

All checking commands work only with those file types which are already 
implemented. Please refer to :ref:`sec_progress` for an overview of currently 
implemented file types.

There are the following checking subcommands:

syntax
        Checks whether the given files are syntactically correct. There is a 
        complete definition for all implemented file types against which the 
        files are tested. This definition includes the correct lines in the 
        correct order, the correct naming of the lines, etc. For details, 
        consult :ref:`sec_check_syntax`.
general
        Does a complete check of the given files. This includes checking for 
        syntax errors, checking referenced files/brushes/strings/etc for 
        existence and so forth. This command accepts all implemented files. You 
        can enter any mixture of file types.
        For details, reference :ref:`sec_check_general`.
entities
        This is a convenience interface, allowing the user to check not single 
        specific Entity files, but all Entities with a given type. See 
        :ref:`sec_check_entities` for details.
mod
        Tests an entire mod against all implemented checks, every single file. 
        This is the kind of command you might fire off a week before release 
        to see where you stand. See :ref:`sec_check_mod` for details.

.. _sec_check_syntax:

Check Syntax
------------

All Sins files are well defined, not free form text files. They need to have 
a specific structure, a specific order of lines defined by the file type. Due 
to this fact, a mod's file can easily be checked for syntactical errors, like 
having forgotten a closing quotation mark or a complete line. This is what the 
``check syntax`` subcommand does::

        $ pysoase check [--mod <path>] [--rebref <path>] syntax file1 [file2 [file3...]]

Any path provided can be either relative or absolute. Remember to surround 
paths containing spaces with quotation marks.

When ``check syntax`` is called, PySoaSE tries to parse all files given on the 
command line. Any problems encountered are stored and printed at the end. 
Please note: Due to the parsing mechanics, only the first syntax error 
encountered in a file is registered, so you should call ``check syntax`` again 
after you have corrected an error, to make sure there aren't any others.

.. _sec_check_general:

Check General
-------------

The ``check general`` command conducts full checks on the files provided on the 
command line. This includes syntax checks as well as checking referenced files 
(Entities, Particles, etc.) for existence. The command is invoked like this::

        $ pysoase check [--mod <path>] [--rebref <path>] general file1 [file2 [file3...]]

When the command is invoked, the given files are parsed and then fully checked.
Should problems occur, they will be printed after all files have been checked. 
You can mix file types, calls like this are perfectly valid::

        $ pysoase check general English.str entity.manifest CoolExplosion.particle

Performance can be vastly improved by calling ``check general`` with multiple 
files in one invocation, instead of calling it for every file separately. The 
reason is the way checking has to work, take ``.brushes`` files for example.
When an Entity references a brush, there is no way to know in which specific 
``.brushes`` file that brush can be found, and so all Brush files need to be 
parsed to check said Entity. As Brush files can be rather large, it would be 
practical when they would only be parsed once and then used with multiple files.
Thus, it is advisable to call ``check general`` with multiple files instead of 
once per file. 

.. _sec_check_mod:

Check Mod
---------

The ``check mod`` command runs all implemented tests on all files of a given 
mod. It is a one-shot command to see where there are problems in your mod. The 
command is invoked like this::

        $ pysoase check [--rebref <path>] [--strict] mod <mod directory>

In contrast to other ``check`` subcommands, ``check mod`` does not make use 
of the ``--mod`` parameter. You need to give your mod's root directory as the 
``<mod directory>`` parameter. The ``--strict`` option confines any checks 
of ``.brushes``, ``.entity``, ``.playerThemes``, ``.playerPictures`` and
``.galaxy`` files to those which are in your mod (so no vanilla files)
and their respective manifest. This might be useful to exclude files which 
are still in your mod directories but not currently in use.

This command is especially useful as a one-shot command after all new 
features for a release are added. Executing it as the last step before 
uploading might preclude the need for 0-day patches.

In addition, checking your mod with this command also has performance benefits, 
when you wish to check several subcomponents, like all brushes and all particle 
files. The advantage in speed originates from the fact that any file will only 
be parsed and loaded once, while several invocations of PySoaSE would also 
need to load all necessary files each time.

Component Mode
**************

In addition to files, the ``check general`` command also accepts whole 
directories. When given a directory, PySoaSE will check all files associated 
with that directory. Any directory given must be a valid subdirectory under 
a SoaSE mod. The following directories are currently accepted:

String
        Runs checks on all String files in your mod
Window
        Runs checks on all Brush files as well as player theme files and both 
        the brush and player theme manifests
GameInfo
        Runs checks on all Entities as well as the ``.asteroidDef``, 
        ``DustCloudsDef.renderingDef`` and entity manifest files in your mod
Particle
        Runs checks on all Particle files in your mod
Sound
        Runs checks on the ``.sounddata`` files in your mod
TextureAnimations
        Runs checks on all the texture animations in your mod

So, calling PySoaSE as follows:: 

        $ pysoase check general myMod\Sound\ myMod\String\

would result in all ``.sounddata`` and all ``.str`` files being checked for 
problems.

.. _sec_check_entities:

Check Entities
--------------

The ``check entities`` command is geared towards testing Entities by type, 
instead of specific Entity files. It has the following call signature::

        $ pysoase check [--mod <path>] [--rebref <path>] entities <GameInfoDir> [type [type1 ...]]

Here, ``<GameInfoDir>`` denotes the path to the GameInfo directory from which 
you would like to check Entities. The ``type`` variables which follow denote 
the Entity types to be checked. This must be the type as it is written 
in the ``entityType`` line. Please also refer to :ref:`sec_progress` for an 
overview of Entity classes already implemented. If the type is completely 
omitted, all implemented Entity types are checked.

When ``check entities`` is called, all Entities with the given types found in 
the given ``GameInfo`` directory are checked for problems.

.. _sec_cmd_modify:

The ``modify`` Command
======================

The ``modify`` subcommand can be used to apply batch changes to a mod's files.
It is invoked as follows::

    $ pysoase modify "<MODIFICATION DEFINITION>" file1 [file2 [file3...]]

The ``<MODIFICATION DEFINITION>`` is the central piece of the call. It tells 
PySoaSE which lines need to be changed in which way. The ``file1...`` parameter 
denotes the files those changes should be applied to. Please note: The
``modify`` command does not accept directories as input, only specific files. 
Use console wild cards to add several related files easily. 

.. warning::
  The ``modify`` command only accepts files, not directories!

.. note::
  The modification string *always* needs to be surrounded by quotation marks!
  Otherwise, your console might mangle the input.

It is important to note that PySoaSE applies the modification to any fitting 
line in any of the input files. So, if a line occurs several times in a file, 
the modification will be applied to every one of them. Task as an example 
a ``.str`` file. Say you tell PySoaSE to modify the ``ID`` line. Then the 
``ID`` of every single StringInfo entry in the given *String* file will 
have the given ID.

The Modification String
-----------------------

The modification string has a simple syntax, made up of the **Identifier** and 
the **Change** to be applied to all lines with that identifier::

    "IDENTIFIER:CHANGE"

The identifier has the same meaning for all types of changes, it is the name 
of a line in a Sins file. Say you want to apply a change to the health regen 
rate a *Planet* type entity. Then, your modification string 
would look something like this::

    "healthRegenRate:CHANGE"

PySoaSE uses the identifier to find the line to change. That search is 
*case sensitive*! Always make certain you typed the identifier correctly.

Multiple modifications can be applied in a single call to multiple files. In 
this case, simply separate the modifications in the string with a single 
``;``, e.g::

  "ident1:+12;ident5:-6"

The change to be applied varies with the kind of line you want to change. The 
kind of line you change also dictates which changes can be applied. There are 
the following kinds of lines in Sins as interpreted by PySoaSE.

Strings
*******

Strings are all lines of the form ``identifier "value"``. These might be 
literal strings, like the ``Value`` lines in ``.str`` files, or references,
like the ``buffType`` lines in Ability entities.
Strings can only be set to other values. All modifications to string values 
have the form::

  "IDENTIFIER:='NEW_VALUE'"

Note the single quotes around the new value, they are necessary!

Scalars
*******

These are numerical values of the form ``identifier value`` where value is 
a number, either an integer or a floating point number. Scalar modifications 
have a wide range of possible operations. They can set, add, subtract or 
multiply the value of the given line.

Scalar operations have the following form::

  "IDENTIFIER:OP$VALUE"

(Ignore the ``$``, it is there only as a separator in the documentation.)
``OP`` is the operator, which can be one of ``+``, ``-``, ``x``, ``=`` where 
``x`` is the letter x and means multiplication. ``VALUE`` can be any number, 
floating point or integer.

Boolean
*******

Boolean values are lines of the form ``identifier TRUE | FALSE``. They can 
only be set. 
Boolean modifications have the form::

  "IDENTIFIER:=TRUE|FALSE"

Thus, they can only be set to ``TRUE`` or ``FALSE``

Vector
******

Vectors are values of the form ``identifier [val1,val2,...]``. Modifications 
for vectors can change individual elements in the vector. They have the 
form::

  "IDENTIFIER:[OP1$VALUE1,OP2$VALUE2,...]"

(As with scalar modifications, ignore the ``$``, they are only there for 
readability in the documentation.)
As you can see, each element of a vector can be changed by a Scalar change. 
Thus they can be added to, subtracted from, multiplied or set to a specific 
value. It is important to note that the vector in the modification string 
needs to have as many elements as the target line to be changed.

If you only want to change a specific element in a vector and leave all others 
as they are, you can just leave that element's change empty, e.g.::

  "ident1:[,,,+18]"

would only add 18 to the last element of the line ``ident1`` and leave all 
others as they were.

Examples
--------

The following examples demonstrate a few modifications and their syntax.

Add ``2.0`` to the health regen rate and set asteroid to ``true`` in the 
vanilla ``Desert.entity``::

  $ pysoase modify "healthRegenRate:+2.0;isAsteroid:=TRUE" GameInfo\PlanetDesert.entity
  The following changes were made:
          PlanetDesert.entity:
                  Changed 'isAsteroid' from 'False' to 'True'
          PlanetDesert.entity:
                  Changed 'healthRegenRate' from '6.0' to '8.0'

Move the vanilla ``RESEARCHSUBJECT_ACCELERATED_WAVE_CANNONS`` up by two units::

  $ pysoase modify "pos:[,+2]" GameInfo\RESEARCHSUBJECT_ACCELERATED_WAVE_CANNONS.entity
  The following changes were made:
          RESEARCHSUBJECT_ACCELERATED_WAVE_CANNONS.entity:
                  Changed 'pos' from '(7, 2)' to '(7, 4)'

Set hud icons for vanilla Vasari light and heavy frigates to the 
hud icon of the Advent light frigate::

  $ pysoase modify "hudIcon:='HUDICON_FRIGATE_PSILIGHT'" GameInfo\FrigatePhaseHeavy.entity GameInfo\FrigatePhaseLight.entity
  The following changes were made:
          FrigatePhaseLight.entity:
                  Changed 'hudIcon' from 'HUDICON_FRIGATE_PHASELIGHT' to 'HUDICON_FRIGATE_PSILIGHT'
          FrigatePhaseHeavy.entity:
                  Changed 'hudIcon' from 'HUDICON_FRIGATE_PHASEHEAVY' to 'HUDICON_FRIGATE_PSILIGHT'


.. _sec_cmd_show:

The ``show`` Command
====================

The ``show`` subcommand is intended to show information about a mod. The 
``show`` command has the triage of directory options already mentioned in 
:ref:`sec_vanilla`:

-m, --mod <path>  Your mod's root directory
--rebref <path>   The path to the Sins Rebellion reference files

``show`` provides the following subcommands:

refs
 Shows all references in the files given on the command line, for
 example all Brushes referenced in an Ability Entity or all 
 ResearchSubject Entities referenced as prerequisites in a CapitalShip
 Entity. See section :ref:`sec_show_refs`.
buffchains
 Displays chains of Buff Entities emanating from the Abilities/Buffs 
 given on the command line. See section :ref:`sec_show_buffchains`.
stringdiff
 Displays differences between a mods' String file for a given language and the
 vanilla file for the same language. Outputs the newly added strings as well as
 those changed compared to vanilla.

.. _sec_show_refs:

Show Refs
---------

The ``show refs`` subcommand shows all references in the files provided on the 
command line. Its full invocation looks like this::

        $ pysoase show [--mod <path>] [--rebref <path>] refs [--combined] file1 [file2 [file3...]]

When PySoaSE is invoked in this way, all files provided in the invocation are 
parsed, all references loaded into a list and then printed to the user. 
References here means all files, be they entities, meshes or others, Strings, Brushes,
Explosions and so forth. 

Normally, ``show refs`` shows all references sorted by the file they were found 
in. If you would like to have the references of all files given as input sorted 
by their type (Brush, Entity,...) instead of their file, you can provide the 
``--combined`` flag during invocation.

Here are a few examples, using the Advent fighter squad entities from the vanilla 
reference files. The invocation would look like this, assuming that your current 
working directory is the reference files root directory:

.. code:: bash

        $ pysoase show refs GameInfo\SquadPsiBomber.entity GameInfo\SquadPsiCombat.entity GameInfo\SquadPsiMineLayer

PySoaSE would then produce the following output::

        SquadPsiBomber.entity
                Brush
                        dockIcon "HUDICONDOCK_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                        mainViewIcon "MAINVIEWICON_SQUADPSIBOMBER" (MainViewIcon-Ship.brushes)
                        infoCardIcon "INFOCARDICON_SQUADPSIBOMBER" (InfoCardIcon-Ship.brushes)
                        picture "PICTURE_SQUADPSIBOMBER" (Picture-Ship.brushes)
                        hudIcon "HUDICON_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                        smallHudIcon "HUDICONSMALL_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                        launchIcon "HUDICONLAUNCH_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                Entity File
                        fighterEntityDef "FighterPsiBomber"
                        fighterIllusionEntityDef "FighterPsiBomber"
                Sound Dialogue
                        SoundID "SQUAD_PSIBOMBER_ONGENERALORDERISSUED"
                        SoundID "SQUAD_PSIBOMBER_ONSELECTED"
                        SoundID "SQUAD_PSIBOMBER_ONATTACKORDERISSUED"
                String
                        descriptionStringID "IDS_SQUADPSIBOMBER_DESC"
                        nameStringID "IDS_SQUADPSIBOMBER_NAME"
        SquadPsiCombat.entity
                Brush
                        dockIcon "HUDICONDOCK_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                        mainViewIcon "MAINVIEWICON_SQUADPSICOMBAT" (MainViewIcon-Ship.brushes)
                        infoCardIcon "INFOCARDICON_SQUADPSICOMBAT" (InfoCardIcon-Ship.brushes)
                        picture "PICTURE_SQUADPSICOMBAT" (Picture-Ship.brushes)
                        hudIcon "HUDICON_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                        smallHudIcon "HUDICONSMALL_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                        launchIcon "HUDICONLAUNCH_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                Entity File
                        fighterEntityDef "FighterPsiCombat"
                        fighterIllusionEntityDef "FighterPsiCombat"
                Sound Dialogue
                        SoundID "SQUAD_PSIFIGHTER_ONGENERALORDERISSUED"
                        SoundID "SQUAD_PSIFIGHTER_ONSELECTED"
                        SoundID "SQUAD_PSIFIGHTER_ONATTACKORDERISSUED"
                String
                        descriptionStringID "IDS_SQUADPSICOMBAT_DESC"
                        nameStringID "IDS_SQUADPSICOMBAT_NAME"
        SquadPsiMineLayer.entity
                Brush
                        dockIcon "HUDICONDOCK_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                        mainViewIcon "MAINVIEWICON_SQUADPSIMINELAYER" (MainViewIcon-Ship.brushes)
                        infoCardIcon "INFOCARDICON_SQUADPSIMINELAYER" (InfoCardIcon-Ship.brushes)
                        picture "PICTURE_SQUADPSIMINELAYER" (Picture-Ship.brushes)
                        hudIcon "HUDICON_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                        smallHudIcon "HUDICONSMALL_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                        launchIcon "HUDICONLAUNCH_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                Entity File
                        fighterEntityDef "FighterPsiMineLayer"
                        fighterIllusionEntityDef "FighterPsiMineLayer"
                Sound Dialogue
                        SoundID "SQUAD_PSIFIGHTER_ONGENERALORDERISSUED"
                        SoundID "MINE_PSI_ONSELECTED"
                        SoundID "SQUAD_PSIFIGHTER_ONATTACKORDERISSUED"
                String
                        descriptionStringID "IDS_SQUADPSIMINELAYER_DESC"
                        nameStringID "IDS_SQUADPSIMINELAYER_NAME"

Using the ``combined`` flag, the output would look like this::

        Brush
                launchIcon "HUDICONLAUNCH_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                infoCardIcon "INFOCARDICON_SQUADPSIBOMBER" (InfoCardIcon-Ship.brushes)
                smallHudIcon "HUDICONSMALL_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                picture "PICTURE_SQUADPSIBOMBER" (Picture-Ship.brushes)
                dockIcon "HUDICONDOCK_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                mainViewIcon "MAINVIEWICON_SQUADPSIBOMBER" (MainViewIcon-Ship.brushes)
                hudIcon "HUDICON_SQUADPSIBOMBER" (HUDIcon-Ship.brushes)
                launchIcon "HUDICONLAUNCH_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                infoCardIcon "INFOCARDICON_SQUADPSICOMBAT" (InfoCardIcon-Ship.brushes)
                smallHudIcon "HUDICONSMALL_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                picture "PICTURE_SQUADPSICOMBAT" (Picture-Ship.brushes)
                dockIcon "HUDICONDOCK_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                mainViewIcon "MAINVIEWICON_SQUADPSICOMBAT" (MainViewIcon-Ship.brushes)
                hudIcon "HUDICON_SQUADPSICOMBAT" (HUDIcon-Ship.brushes)
                launchIcon "HUDICONLAUNCH_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                infoCardIcon "INFOCARDICON_SQUADPSIMINELAYER" (InfoCardIcon-Ship.brushes)
                smallHudIcon "HUDICONSMALL_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                picture "PICTURE_SQUADPSIMINELAYER" (Picture-Ship.brushes)
                dockIcon "HUDICONDOCK_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
                mainViewIcon "MAINVIEWICON_SQUADPSIMINELAYER" (MainViewIcon-Ship.brushes)
                hudIcon "HUDICON_SQUADPSIMINELAYER" (HUDIcon-Ship.brushes)
        Entity File
                fighterEntityDef "FighterPsiBomber"
                fighterIllusionEntityDef "FighterPsiBomber"
                fighterEntityDef "FighterPsiCombat"
                fighterIllusionEntityDef "FighterPsiCombat"
                fighterEntityDef "FighterPsiMineLayer"
                fighterIllusionEntityDef "FighterPsiMineLayer"
        Sound Dialogue
                SoundID "SQUAD_PSIBOMBER_ONSELECTED"
                SoundID "SQUAD_PSIBOMBER_ONGENERALORDERISSUED"
                SoundID "SQUAD_PSIBOMBER_ONATTACKORDERISSUED"
                SoundID "SQUAD_PSIFIGHTER_ONSELECTED"
                SoundID "SQUAD_PSIFIGHTER_ONGENERALORDERISSUED"
                SoundID "SQUAD_PSIFIGHTER_ONATTACKORDERISSUED"
                SoundID "MINE_PSI_ONSELECTED"
                SoundID "SQUAD_PSIFIGHTER_ONGENERALORDERISSUED"
                SoundID "SQUAD_PSIFIGHTER_ONATTACKORDERISSUED"
        String
                descriptionStringID "IDS_SQUADPSIBOMBER_DESC"
                nameStringID "IDS_SQUADPSIBOMBER_NAME"
                descriptionStringID "IDS_SQUADPSICOMBAT_DESC"
                nameStringID "IDS_SQUADPSICOMBAT_NAME"
                descriptionStringID "IDS_SQUADPSIMINELAYER_DESC"
                nameStringID "IDS_SQUADPSIMINELAYER_NAME"

As you can see, all references are prepended with their identifier, so you 
know where in the file it occurs. The value in parenthesis after the Brush 
references denotes the Brush file the referenced Brush resides in.

.. _sec_show_buffchains:

Show Buffchains
---------------

The ``show buffchains`` subcommand can be used to display chains of Buffs 
emanating from Abilities/Buffs given on the command line. 

The call has the following format:

.. code:: bash

        $ pysoase show [--mod <path>] [--rebref <path>] buffchains file1 [file2 [file3...]]

See :ref:`sec_dirs` for explanations of ``--mod`` and ``--rebref`` arguments.
``file1...`` are the files used as starting points for the Buff chains to 
display. They may be either Buff Entity files or Ability Entity files. In 
addition, since ``v0.9.0``, all entity types which have abilities, e.g. ``Planet``,
``Squad`` or ``PlanetModuleWeaponDefense`` can also be used as input. PySoaSE 
will then display the chains for all abilities referenced in that entity file.

When invoked, the ``buffchains`` subcommand first parsers all files given on the 
command line and checks all *Actions* (Periodic and Instant, where applicable) 
for all actions which lead to the application of Buffs. Then, all those 
referenced Buffs are also parsed for Actions with Buff application and so forth, 
until a Buff which does not apply additional Buffs is reached for every chain.

.. note::
        The ``buffchains`` command does not work with vanilla files referenced 
        by your mod's Buffs and Abilities, because the integration of vanilla 
        Entity files is not yet complete. At the moment, referencing a vanilla 
        Buff in a Buff chain will simply display a message that a vanilla file 
        is being used.

In the following example, the chain for the vanilla *AbilityUnyieldingWill.entity* 
file is shown:

.. code:: bash

        $ pysoase show buffchains AbilityUnyieldingWill.entity

.. code::

        AbilityUnyieldingWill.entity:
                BuffUnyieldingWillSelf.entity:
                                BuffUnyieldingWill25.entity:
                                        No Buffs Referenced
                                BuffUnyieldingWill50.entity:
                                        No Buffs Referenced
                                BuffUnyieldingWill75.entity:
                                        No Buffs Referenced
                                BuffUnyieldingWillRecourse.entity:
                                                        BuffUnyieldingWillInvis.entity:
                                                                No Buffs Referenced

.. _sec_show_stringdiff:

Show Stringdiff
---------------

The ``show stringdiff`` subcommand can be used to display the differences
between a mods' String file and the vanilla stringfile, outputting the added
and changed strings.

The call has the following format:

.. code:: bash

        $ pysoase show [--mod <path>] [--rebref <path>] stringdiff [-h] [-u] [--outdir OUTDIR] language

See :ref:`sec_dirs` for explanations of ``--mod`` and ``--rebref`` arguments.
The main input parameter is ``language``, giving the language to be compared.

When this command is launched, it loads the String file in the ``String``
directory of the mod given by the ``--mod`` parameter, appending ``.str`` to
the content of the ``language`` parameter as well as the same file from the
vanilla ``String`` directory.
Then, it compares the ``ID`` value for all ``StringInfo`` entries in both files,
composing an *added* list of strings with all ``StringInfo`` entries found in
the mods' file but not the vanilla file. In addition, all ``StringInfo``
objects from both files with the same ``ID`` values but different ``Value``
entries are added to a *changed* list.

After both lists are generated, their contents are written to a file called
``stringdiff_added_<LANGUAGE>.txt`` for the *added* list and
``stringdiff_changed_<LANGUAGE>.txt`` for the *changed* list. These files are
located in the directory provided in the ``--outdir`` parameter or in the
current directory, when ``--outdir`` is not provided.

To ease regular String file updates, *Show Stringdiff* also comes with the
``-u`` flag. If it is provided, PySoaSE expects to find the two aforementioned
files ``stringdiff_added_<LANGUAGE>.txt`` ``stringdiff_changed_<LANGUAGE>.txt``
either in the current directory or the ``--outdir`` directory. When conducting
the comparison, PySoaSE runs as before. The content of both stringdiff files
is overwritten by the results of the new comparison. But in addition to the
standard output of how many additions and changes were found, PySoaSE also
returns all additions and updates not found in the preexisting stringdiff files.

As an example, assume your mod contains an ``English.str`` file with the
following content:

.. code::

        TXT
        NumStrings 4
        StringInfo
                ID "IDS_INFOCARD_LABEL_ABILITYDURATION"
                Value "NewDuration"
        StringInfo
                ID "IDS_INFOCARD_TITLE_CREDITS"
                Value "Credits New"
        StringInfo
                ID "NewString1"
                Value "String New 1"
        StringInfo
                ID "NewString2"
                Value "String New 2"

Then, running the command

.. code:: bash

        $ pysoase show --mod myMod stringdiff --outdir my_stringdiff English

would result in the following output:

.. code::

        Found 2 changed and 2 added Strings in "myMod/String/English.str".
        Output written to my_stringdiff/stringdiff_added_English.txt
        and my_stringdiff/stringdiff_changed_English.txt.

The *added* file ``my_stringdiff/stringdiff_added_English.txt`` would have the
content

.. code::

        StringInfo
                ID "NewString1"
                Value "String New 1"
        StringInfo
                ID "NewString2"
                Value "String New 2"

and the *changed* file ``my_stringdiff/stringdiff_changed_English.txt``
would have this content:

.. code::

        StringInfo
                ID "IDS_INFOCARD_LABEL_ABILITYDURATION"
                Value "NewDuration"
        StringInfo
                ID "IDS_INFOCARD_TITLE_CREDITS"
                Value "Credits New"