Installation and Setup
######################

In this section, the prerequisites and installation procedures will be described.

Prereqisites
============

PySoaSE is written in Python to facilitate usage under any modern operating 
system. Thus, its first requirement is a working Python interpreter, with 
at least version 3.4 support. 

Linux
-----

Linux users should install Python 3.4.x via their distribution's package 
manager (portage,yum,pacman,apt,...).

You also need git, which should be available in your distribution's package 
manager too.

Windows
-------

The following instructions are intended for users who would like to use
the Windows CMD application for their work with PySoaSE. In addition to
the instructions provided below, PySoaSE has also been found to work with the
`Windows Subsystem for Linux (WSL) <https://msdn.microsoft.com/de-de/commandline/wsl/install_guide>`_.
Simply follow the instructions provided for Linux installations.

Python
^^^^^^

For Windows users, a little bit more care is necessary to ensure that PySoaSE 
can be used as comfortably as possible from the command line:

#. Download the latest version of Python (>=3.4) from 
   `the official Python website <https://www.python.org/downloads/windows/>`_.

        * Make sure to choose the right version (x86 or x86_64) for your 
          Windows installation.

#. Start the installation by double clicking on the file you just downloaded
#. On the *Customize Python* page, make sure the following options are set:

        * *pip* set to *Will be installed on local hard drive*
        * *Add Python.exe to Path* set to *Will be installed on local hard drive*


Choosing *pip* installs a standard Python package manager, which is used by 
PySoaSE's setup script to install a few Python dependencies.

Choosing *Add Python.exe to Path* allows you to start python programs from 
the command line without having to type the full path to the Python executable.

Git
^^^

You will also need the source control tool `Git <https://git-scm.com/>`_, which
is also used by a number of Sins mods for their internal development. If you've 
already got a Git installation, you can skip this section.

Probably the easiest version to use for Windows users is 
`TortoiseGit <https://tortoisegit.org/>`_. Just follow their installation 
instructions `here <https://tortoisegit.org/docs/tortoisegit/index.html>`_.

Download
========

PySoaSE is made available as source code via my 
`Gitlab`_ repository.

Linux
-----

Assuming you want to download PySoaSE into a folder ``pysoase`` in your home 
directory, enter the following into your terminal::

        ~$ git clone https://gitlab.com/pysoase/pysoase.git pysoase

Windows/TortoiseGit
-------------------

To download PySoaSE via TortoiseGit, follow these steps:

#. Navigate to the directory where you would like to store the repository, 
   using the Windows Explorer
#. Right click anywhere in the Explorer window
#. Choose *Git Clone* from the context menu
#. Enter  https://gitlab.com/pysoase/pysoase.git in the *URL* field
#. Click the *branch* check box and enter *master* in the field
#. Click *Ok*

.. _sec_install:

Installation
============

PySoaSE uses the *pip* package manager to install dependencies and the software 
itself.

Linux
-----

To install under Linux, change your current directory to the directory where 
you cloned the repository and enter the following command::

        ~$ pip install src/

Windows
-------

To install PySoaSE under Windows, right click the directory where you cloned 
the repository while holding the ``Shift`` key. From the menu, choose *open 
command window here*. 

In the newly opened console, enter the command::

        > pip install src\

This command will first install the following runtime dependencies:

* `pyparsing <http://pyparsing.wikispaces.com/>`_: Used to parse the SoaSE text 
  files
* `sortedcontainers <http://www.grantjenks.com/docs/sortedcontainers/introduction.html>`_:
  Used for several efficient data structures
* `tqdm <https://github.com/tqdm/tqdm>`_: Used to display a progress during 
  long running checks

as well as PySoaSE itself.

To run PySoaSE, just enter ``pysoase`` into any command line prompt. To receive 
usage instructions, enter ``pysoase --help``.

Update
======

To update PySoaSE, pull in and check out the newest version from the `Gitlab`_ repository and repeat the steps 
detailed in :ref:`sec_install`.

.. _Gitlab: https://gitlab.com/pysoase/pysoase
