.. PySoaSE documentation master file

#####################
PySoaSE Documentation
#####################

Content
=======

.. toctree::
   :maxdepth: 2

   Introduction <self>
   Installation and Setup <setup>
   Usage Command Line <usage>
   Continuous Integration and Automated Testing <automation>

Introduction
============

.. note::
        PySoaSE is still in heavy development. There are yet file classes to be 
        implemented. 

PySoaSE is a modding tool for the PC Game `Sins of a Solar Empire
<https://www.sinsofasolarempire.com/>`_. Its main purpose is to provide modders 
with tools to check and verify that their mods are error-free. To that end,
PySoaSE provides tools for syntax checking as well as reference checking. It is 
written in Python, and thus it can be used on all operating systems with a 
Python interpreter supporting at least Python 3.4.

To facilitate these features, PySoaSE provides text file parsers for all types 
of Sins files. To enable consistency checking, like ensuring that a referenced 
Brush actually exists, all Sins file types have been implemented as Python 
classes.

The following example shows the output from checking the Particle files 
of the Dev version of the `SoGE <http://www.moddb.com/mods/sins-of-a-galactic-empire>`_ mod::

        Exhaust_EMPIREHEAVY.particle:
            ERROR: Referenced Emitter '???????????????????????' could not be resolved.
        Exhaust_EMPIREMEDIUM.particle:
            ERROR: Referenced Emitter '???????????????????????' could not be resolved.
        Exhaust_EMPIREVHEAVY.particle:
            ERROR: Referenced Emitter '???????????????????????' could not be resolved.
        Weapon_SW_VenatorBeam_Hit.particle:
            ERROR: Referenced File 'techcapitalbeamcorona3['.dds', '.tga']' could not be resolved.
        Weapon_SW_VenatorBeam_Muzzle.particle:
            ERROR: Referenced File 'techcapitalbeamcorona3['.dds', '.tga']' could not be resolved.

.. note::
        PySoaSE can only be used on the text version of a mods' files. There 
        is no way to use it on the files in binary format.

The program can be downloaded and contributed to via my 
`Gitlab`_ repository.

.. _sec_progress:

Current Progress
================

This section contains an overview of the current implementation progress, 
detailing tests and classes already implemented.

The following SoaSE files are currently implemented:

* ``.asteroidDef``
* ``.brushes``
* ``.explosiondata``
* ``.galaxy``
* ``.galaxyScenarioDef``
* ``.gameeventdata``
* ``.manifest``
* ``.particle``
* ``.playerPictures``
* ``.playerPicturesDef``
* ``.playerThemes``
* ``.playerThemesDef``
* ``.randomeventdefs``
* ``.renderingDef`` (DustCloudsDef)
* ``.skyboxbackdropdata``
* ``.sounddata``
* ``.starscapedata``
* ``.string``
* ``.texanim`` 
* All ``.entity`` file types.

The following file classes are still missing:

* ``Gameplay.constants``
* ``.coronadata``
* ``FarStar.data``
* ``PlanetElevators.data``
* ``.exhaustTrailDef``
* ``.lensflaredata``
* ``.mesh``
* ``.musicdata``
* ``.postprocessdata``
* ``.skyboxstarsdata``
* ``.window``

Also, the following kinds of problems can be found, where applicable:

* Syntax errors, e.g. missing lines, uneven number of quotation marks, misspelled 
  line name, discrepancy for number of entries given at head of list and actual 
  number of entries, wrong type of value given (e.g. string where number is expected), 
  works for all implemented files
* Duplicate entries (``.string`` and ``.brushes`` files)
* Referenced but missing ``.entity`` files (multiple file types)
* Referenced ``.entity`` files which exist, but have the wrong type, e.g. a 
  *Titan* class entity being referenced in another Entity's 
  ``squadTypeEntityDef`` line instead of a *Fighter* type Entity.
* Referenced *Brushes* which could not be found in any ``.brushes`` file (multiple file types)
* Referenced *Strings* which could not be found in any ``.string`` file (multiple file types)
* Referenced ``.particle`` files which could not be found in the ``Particles`` 
  subdirectory (multiple file types)
* Referenced ``.ogg`` files which could not be found in the ``Sound`` 
  subdirectory (``.sounddata`` files)
* Referenced *Sound* entries which could not be found in any of the 
  ``.sounddata`` files (multiple file types)
* Referenced ``.texanim`` files which could not be found in the 
  ``TextureAnimations`` directory (``.particle`` files)
* Missing internal definitions (e.g. templates) in the ``.galaxyScenarioDef`` 
  file
* Referenced ``.tga``/``.dds`` files missing from the ``Textures`` subdirectory 
  (multiple file types)
* Vanilla files being used (this is not considered an error per se, but might 
  be interesting information for modders)

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _Gitlab: https://gitlab.com/pysoase/pysoase
