Changelog
=========

PySoaSE v1.6.0
--------------

New Features:
* `show stringdiff` subcommand to show strings added/modified in a mod's
	String file compared to vanilla [Issue #14](https://gitlab.com/pysoase/pysoase/issues/14)
* Confirmed that PySoaSE also works in the *Windows Subsystem for Linux*

PySoaSE v1.5.3
--------------

Features:
* Added debug output to GitLab automation script

PySoaSE v1.5.2
--------------

Updated PySoaSE to SoaSE v1.9
* Add research modifiers `FriendlyWithRebelNeutrals` and `FriendlyWithPirates`
* Updated bundled reference files

PySoaSE v1.5.1
--------------

Bugfixes:

* Allow `.wav` files as audio files
	* By default, if no file extension is given on the `filename` line, 
		a `.ogg` file extension is assumed
* Completely disabled the `unref` command, because it is not compartible with
	files which can have multiple endings (e.g. audiofiles, with `.wav` and `.ogg`)

PySoaSE v1.5.0
--------------

Features:
* Added line numbers to problem output

Bugfixes:

* Fixed [Issue #12](https://gitlab.com/pysoase/pysoase/issues/12), PySoaSE 
	should not output numbers in scientific notation. Instead, numbers are now 
	output with a fixed point precision of 6.
* Fixed tests for vector modifications

Internal:

* Split `pysoase.common` module into separate submodules
* Ran the entire codebase through PyCharm reformating
* Switched test suite to [pytest](http://doc.pytest.org/en/latest/)
* General code cleanup with PyCharm linter


PySoaSE v1.4.4
--------------

Bugfixes:

* The `modify` command now recurses into list elements
* Vector modifications can now be applied to vectors of any length

PySoaSE v1.4.3
--------------

Bugfixes:

* Fixed problems with missing build artifacts in successful builds

PySoaSE v1.4.2
--------------

Bugfixes:

* Fixed problems with artifact downloads in GitLab CI config
* Fix [Issue #8](https://gitlab.com/pysoase/pysoase/issues/8)

Docs:

* Added short readme for files in `automation` directory

CI:

* Removed some unnecessary output from GitLab CI script

PySoaSE v1.4.1
--------------

Bugfixes:

* Switch from caching to artifacts for GitLab CI script
* Fix [Issue #7](https://gitlab.com/pysoase/pysoase/issues/7)

Docs:

* Added section about Continuous Integration
* Fixed spelling

PySoaSE v1.4.0
--------------

This commit is concerned with improvements to automated runs of PySoaSE,
for example via GitLab CI.

New Features:

* Add `--store` parameter for pickled problem lists
* Add script `evaluate_problems` to compare lists of pickled problems to find 
  newly introduced problems
* Add `automation/` directory with example script for GitLab CI
* Add `--quiet` flag, which suppresses progress bars for a less cluttered 
  output. This resolves [Issue #3](https://gitlab.com/pysoase/pysoase/issues/3).
* Skip checks for `smallHudIcon` lines in *ResearchSubject* and *Ability*
  entities. This resolves [Issue #4](https://gitlab.com/pysoase/pysoase/issues/4).

PySoaSE v1.3.0
--------------

Implemented the `modify` command to make batch changes to mod files.

* Add the `modify` command
* Add modification to all file classes
* Add [docs](http://pysoase.readthedocs.io/en/stable/usage.html#the-modify-command)
  for the modify command
* This release also resolves [Issue #2](https://gitlab.com/pysoase/pysoase/issues/2)

Bugfix:

* Fix bug where `maxLevels` attribute in Ability entities had boolean type 
  instead of numeric type

Also removed obsolete `testing.py` script, which has been wholly replaced by 
actual unit tests.

PySoaSE v1.2.3
--------------

Finish Gitlab Migration.

* Moved changelog to seperate file
* Ported Readme/Changelog to Markdown markup for Gitlab
* Added Gitlab Continuous Integration config to run PySoaSE's test suite 
  automatically
* Added build badge to Readme

Bugfix:

* Fixed missing exit code checks in test suites for PySoaSE commands
* Fixed Bug leading to crash in String file checks

PySoaSE v1.2.2
--------------

Migrating the project to [Gitlab](https://www.gitlab.com) made a number of URL
changes necessary.

PySoaSE v1.2.1
--------------

Bugfix:

* [Issue #6](https://gitlab.com/pysoase/pysoase/issues/6): Fix bug 
  leading to crash when GalDef is missing/malformed

PySoaSE v1.2.0
--------------


This release is mainly concerned with vanilla file handling. Sometimes, 
PySoaSE may be run in an environment without a vanilla Sins installation. In 
those cases, the number of false positives where missing files are concerned 
should still be held to a minimum. This goal is achieved by the following 
changes:

* Vanilla file existence checks are now conducted on a file manifest, 
  instead of an actual vanilla directory
* Important vanilla files in TXT format are now bundled and shipped with 
  PySoaSE
* The `--reb` command line option has been removed
* Add program exit value. If no problems with severity *ERROR* occured, the 
  exit value of PySoaSE will be `0`. Otherwise, it will be `1`. This 
  allows the usage of PySoaSE in scripts.
* Add `--strict` flag to `check mod` subcommand. With this flag, only 
  files in their respective manifests will be checked, not all files found 
  in the mod directory.
* Duplicate string entries starting with `IDS_TAUNT` are no longer reported 
  as duplicates. It seems that these entries are duplicates by design.

PySoaSE v1.1.0
--------------

This release updates PySoaSE to Sins version 1.85. It contains the following 
updates:

* The new `RuinPlanet` instant action for abilities
* Scenario file format 5
* New capital crew parameter for the `ResourcesAndHaveCapitalCrew` cost type
* New `RuinPlanetNotRuinable` game event
* New `GiveExperience` instant action
* New `Path:Smuggler` upgrade definitions for `Planet` entities

PySoaSE v1.0.0
--------------

With this release, the initial *checking* feature is fully implemented with the 
inclusion of the final missing entity classes (*Titan*, *Frigate*,
*StarBase*, *Fighter* and *CapitalShip*).

As this was the initial goal for PySoaSE, I'm calling this version 1.0.0. For 
the next major version, I will work to implement unused file/entry checking for 
everything from Strings to entities.

* Implemented *Titan*, *Frigate*, *StarBase*, *Fighter* and *CapitalShip* entity 
  classes

PySoaSE v0.9.0
--------------

This release is mostly concerned with frontend improvements.

* Config files now have an `all` section, where parameters used for all 
  subcommands can be set.
* PySoaSE now has progress bars for all operations! Fair warning: Don't put 
  to much stock in the time estimates those progress bars put out. They aren't 
  very accurate.
* The `unref` command now outputs the names of unreferenced files in their 
  actual case, not all lower cased
* The `unref` command now sorts unreferenced files by extension as well 
  as filename
* Entity types with abilities can now be given to the `show buffchains` 
  command as input and will then display the chains of all referenced 
  abilities.

PySoaSE v0.8.2
--------------


Bugfix Release:

* Fixed typo in `.gameeventdata` parser, which lead to erroneous parse 
  results

PySoaSE v0.8.1
--------------

Bugfix Release:

* Reference showing: When `show refs` was called with files could either 
  contain references or not, an exception could occur due to getRefs 
  being called on a file which did not have any references
* The `.gameeventdata` and `.starscapedata` file type implementations in 
  v0.8.0 were lacking adapted getRefs methods, leading to textures and 
  brushes not being returned when getRefs was called on those file types.
* In v0.8.0, the newly implemented file types were not added to the data 
  structures mapping file extensions to their file implementation classes. Due 
  to this oversight, these new file types could not be used with the 
  `check general` subcommand.

PySoaSE v0.8.0
--------------

Bugfixes:

* Finally, PySoaSE is now able to do case insensitive file existence checks 
  on case sensitive file systems

Newly implemented files:

* `.gameeventdata`
* `.randomeventdefs`
* `.skyboxbackdropdata`
* `.starscapedata`

PySoaSE v0.7.3
--------------

Bugfix Release:

* Fixed bug leading to errors when malformed files already loaded are checked.
* Removed false reported problem with referenced entities being of the wrong 
  type. When an entity file could not be parsed or was missing completely,
  PySoaSE reported references to said entity as having the wrong type. Now,
  PySoaSE correctly reports that the entity type could not be checked.

PySoaSE v0.7.2
--------------

Bugfix Release:

* Fixed a bug where effect/dialogue sound references are falsely reported
  as missing, although the referenced entries were simply in a different 
  `.sounddata` file

PySoaSE v0.7.1
--------------

This is mearly a code cleanup release, no new features or bug fixes

PySoaSE v0.7.0
--------------

Features added:

* All PlanetModule* entities are now implemented and can be checked

PySoaSE v0.6.0
--------------

Features added:

* Implementation of the `unref` command to find unused files in your mod

PySoaSE v0.5.0
--------------

Features added:

* Implementation of `.galaxy` files
* Added checking of `Galaxy/` subdirectory

PySoaSE v0.4.0
--------------

Features added:

* Vanilla files are now fully integrated
* Added the `check mod` subcommand, which checks an entire mod

Bugs fixed:

* Files with `.` in their names other than as a seperator for the file 
  extension are now handled correctly

PySoaSE v0.3.2
--------------

Hotfix for severity command line option problem

* The `--sev` option for the `check` command produces problems, because 
  handling of it was not readded when command line parameter handling was 
  overhauled in v0.3.0

PySoaSE v0.3.0
--------------

* Added `--out` optional CLI parameter, which can be used to redirect 
  output to a file instead of printing it on the console
* Removed `attribg` subcommand. It was undocumented, and didn't fit internal 
  changes to the handling of the CLI frontend. Perhaps I will update and 
  reintegrate it at a later date
* Improved documentation
* Added documentation for the `check` subcommand

PySoaSE v0.2.1
--------------

Bugfix release:

* Fixed Bug leading to infinite recursion when `show buffchains` is called 
  with a Buff chain containing cycles
* Removed printing of invalid files headline when there were no invalid files 
  provided on the command line

PySoaSE v0.2.0
--------------

The following features were added:

* Add new `show buffchains` subcommand

PySoaSE v0.1.1
--------------

Bugfix release:

* Bug Fix: Fixed bug where `show refs` would not display references held in 
  lists
* Documentation: Added short section on how to update to a new version

PySoaSE v0.1.0
--------------

This is the initial release. Features:

* Show all references in a given file/files
* Check a majority of Sins files for syntactical und semantical errors
