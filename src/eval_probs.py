#!/usr/bin/python3.4
""" Command line launcher for the evaluate_problems script

This script is just an easy way for developers to start the script without 
having to install pysoase.
"""

from pysoase.scripts.evaluate_probs import main

if __name__=="__main__":
    main()
