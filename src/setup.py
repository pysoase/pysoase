""" Setup configuration for pysoase
"""
from setuptools import setup,find_packages

NAME = "PySoaSE"
VERSION = "1.6.0"
DESCRIPTION = ("A modding tool for Sins of a Solar Empire, geared towards "
               "checking files for syntactical errors and missing references.")
URL = "https://gitlab.com/pysoase/pysoase"
AUTHOR = "Michael Meier"
AUTHOR_MAIL = "mmeier1986@gmail.com"
LICENSE = "MIT"

setup(
        name=NAME,
        version=VERSION,
        description=DESCRIPTION,
        url=URL,
        author=AUTHOR,
        author_email=AUTHOR_MAIL,
        license=LICENSE,
        packages=find_packages(exclude=["pysoase.tests**"]),
        install_requires=["pyparsing","sortedcontainers","tqdm"],
        entry_points={
            "console_scripts":["pysoase=pysoase.frontends.cli:entry",
                "eval_probs=pysoase.scripts.evaluate_probs:main"]
        },
        package_data={
            "pysoase":["resources/vanilla.manifest",
                "resources/reference/*.manifest",
                "resources/reference/GameInfo/*",
                "resources/reference/Window/*"]
        }
)
