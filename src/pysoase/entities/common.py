""" Contains parsers and classes common to multiple entities.
"""

import abc
import enum
import os

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.meshes as meshes
import pysoase.mod.ui as ui

"""################################# Types #################################"""

ATTACH_TYPES = ["Ability","Above","Aura","Center","Invalid"]
PLANETTEYPE_FOR_RESEARCH = ["Asteroid","Barren","Desert","Greenhouse",
    "Ice","Invalid","Iron","Moon","Oceanic","Terran","Uncolonizable",
    "Volcanic"]
POSSIBLE_ENTITY_TYPES = ["Ability","Buff","CannonShell","CapitalShip",
    "Cosmetic","Debris","EmpireWindowNode","EntryVehicle","Fighter","Fleet",
    "Formation","Frigate","MissionAttackPlanet","MissionBuildMines",
    "MissionBuildStarBase","MissionCaptureResources","MissionColonize",
    "MissionDefendFlagship","MissionExplore","MissionFosterRelationships",
    "MissionRaid","PipCloud","Planet","PlanetBonus","PlanetConnection",
    "PlanetModuleHangarDefense","PlanetModuleRefinery",
    "PlanetModuleShipFactory","PlanetModuleStandard",
    "PlanetModuleTradePort","PlanetModuleWeaponDefense","Player","Quest",
    "ResourceAsteroid","ResearchSubject","SpaceMine","Squad","Star",
    "StarBase","StarBaseUpgrade","Titan","TitanUpgrade","Trigger","Weapon"]
WEAPON_CLASSES = ["AUTOCANNON","BEAM","CAPITALABILITY","CHAOSBOLT","DART",
    "FLAK","FLASHBEAM","GAUSS","ION","Invalid","LASERPSI","LASERTECH",
    "MISSILE","PHASECANNON","PHASEMISSILE","PLANETBOMBARDMENT","PLASMA",
    "POINTDEFENSELASER","PULSEGUN","PULSEWAVE","RAILGUN","SPIRITBLADE",
    "WAVE"]

"""################################ Parsers ################################"""

"""### Basic Elements ###"""

g_abilities = pp.Group(
        co_parse.genStringAttrib("ability:0")
        +co_parse.genStringAttrib("ability:1")
        +co_parse.genStringAttrib("ability:2")
        +co_parse.genStringAttrib("ability:3")
        +co_parse.genStringAttrib("ability:4")
)("abilities")
g_antimatterRestore = co_parse.genDecimalAttrib("AntiMatterRestoreRate")(
        "antimatterRestore")
g_cost = (co_parse.genDecimalAttrib("credits")("credits")
          +co_parse.genDecimalAttrib("metal")("metal")
          +co_parse.genDecimalAttrib("crystal")("crystal")
)
g_baseAntimatterRestore = co_parse.genDecimalAttrib("baseAntiMatterRestoreRate")(
        "antimatterRestore")
g_baseMaxAntimatter = co_parse.genDecimalAttrib("baseMaxAntiMatter")("antimatterMax")
g_basePrice = pp.Group(
        co_parse.genHeading("basePrice")
        +g_cost
)("price")
g_counterDescString = co_parse.genStringAttrib("counterDescriptionStringID")(
        "counterDesc")
g_debrisLarge = co_parse.genIntAttrib("numRandomDebrisLarge")("debrisLarge")
g_debrisSmall = co_parse.genIntAttrib("numRandomDebrisSmall")("debrisSmall")
g_debrisSpecific = co_parse.genListAttrib("numSpecificDebris",
        co_parse.genStringAttrib("specificDebrisMeshName"))("debrisSpecific")
g_debris = (
    g_debrisLarge
    +g_debrisSmall
    +g_debrisSpecific
)
g_descriptionString = co_parse.genStringAttrib("descriptionStringID")("descString")
g_descriptionStringCap = co_parse.genStringAttrib("DescriptionStringID")("descString")
g_descriptionStringShort = co_parse.genStringAttrib("descStringID")("descString")
g_dlcid = pp.Group(
        co_parse.g_linestart
        +pp.Keyword("dlcId")("identifier")
        +co_parse.g_space
        +pp.Or([pp.Literal("204880"),pp.Literal("228601"),
            pp.Literal("255910")])("val").setParseAction(lambda t:int(t[0]))
        +co_parse.g_eol
)("dlcID")
g_entityType = co_parse.genEnumAttrib("entityType",POSSIBLE_ENTITY_TYPES)(
        "entityType")
g_header = (
    co_parse.g_txt
    +g_entityType
)
g_hyperChargeSnd = co_parse.genStringAttrib("HyperspaceChargingSoundID")(
        "hyperChargingSnd")
g_hyperTravelSnd = co_parse.genStringAttrib("HyperspaceTravelSoundID")(
        "hyperTravelSnd")
g_mass = co_parse.genDecimalAttrib("mass")("mass")
g_maxAntimatter = co_parse.genDecimalAttrib("MaxAntiMatter")("antimatterMax")
g_movement = (
    co_parse.genDecimalAttrib("maxAccelerationLinear")("maxAccelLin")
    +co_parse.genDecimalAttrib("maxAccelerationStrafe")("maxAccelStrafe")
    +co_parse.genDecimalAttrib("maxDecelerationLinear")("maxDecelLin")
    +co_parse.genDecimalAttrib("maxAccelerationAngular")("maxAccelAng")
    +co_parse.genDecimalAttrib("maxDecelerationAngular")("maxDecelAng")
    +co_parse.genDecimalAttrib("maxSpeedLinear")("maxSpeedLin")
    +co_parse.genDecimalAttrib("maxRollRate")("maxRollRate")
    +co_parse.genDecimalAttrib("maxRollAngle")("maxRollAngle")
)
g_nameString = co_parse.genStringAttrib("nameStringID")("nameString")
g_nameStringCap = co_parse.genStringAttrib("NameStringID")("nameString")
g_price = pp.Group(
        co_parse.genHeading("price")
        +g_cost
)("price")
g_shadows = (
    co_parse.genDecimalAttrib("minShadow")("shadowMin")
    +co_parse.genDecimalAttrib("maxShadow")("shadowMax")
)
g_zoomDist = co_parse.genDecimalAttrib("minZoomDistanceMult")("zoomDist")

"""#### Research ###"""

g_researchPrerequesite = pp.Group(
        co_parse.genHeading("ResearchPrerequisite")
        +co_parse.genStringAttrib("Subject")("subject")
        +co_parse.genIntAttrib("Level")("level")
)
g_researchPrerequesites = (
    co_parse.genListAttrib("NumResearchPrerequisites",g_researchPrerequesite)(
            "prereqs")
    +co_parse.genStringAttrib("RequiredFactionNameID")("reqFaction")
    +co_parse.genIntAttrib("RequiredCompletedResearchSubjects")(
            "reqCompSubjects")
)
g_researchPrereqs = pp.Group(
        co_parse.genHeading("researchPrerequisites")
        +g_researchPrerequesites
)("prereqs")
g_prereqs = pp.Group(
        co_parse.genHeading("Prerequisites")
        +g_researchPrerequesites
)("prereqs")

"""#### Icons ###"""

g_hudIcon = co_parse.genStringAttrib("hudIcon")("iconHud")
g_infoCardIcon = co_parse.genStringAttrib("infoCardIcon")("iconInfocard")
g_mainViewIcon = co_parse.genStringAttrib("mainViewIcon")("iconMainView")
g_picture = co_parse.genStringAttrib("picture")("pic")
g_smallHudIcon = co_parse.genStringAttrib("smallHudIcon")("iconHudSmall")
g_uiIcons = (
    g_hudIcon
    +g_smallHudIcon
    +g_infoCardIcon
)

"""### Parser Element Functions ###"""

def genEntityHeader(entType):
    return (
        co_parse.g_txt
        +co_parse.genKeyValPair("entityType",entType)("entityType")
    )

def genFixedLvlAttrib(identifier):
    return pp.Group(
            co_parse.genHeading(identifier)
            +pp.Group(
                    co_parse.genDecimalAttrib("Level:0")
                    +co_parse.genDecimalAttrib("Level:1")
                    +co_parse.genDecimalAttrib("Level:2")
                    +co_parse.genDecimalAttrib("Level:3")
            )("levels")
    )

"""############################### Problems ################################"""

class WrongEntityTypeProb(co_probs.Problem):
    def __init__(self,refname,allowedTypes,**kwargs):
        super().__init__(severity=co_probs.ProblemSeverity.error,**kwargs)
        self.refname = refname
        self.allowedTypes = allowedTypes

    def __eq__(self,other):
        res = super().__eq__(other)
        return (res and (self.refname==other.refname)
                and (self.allowedTypes==other.allowedTypes))

    def toString(self,indention):
        res = super().toString(0)
        res += ("Reference \""+self.refname+"\" has the wrong type. Allowed "
                "types are "+str(self.allowedTypes)+".\n")
        return indentText(res,indention)

"""############################## Attributes ###############################"""

class TypeCheckResult(enum.Enum):
    valid = 1
    invalid = 0
    na = -1

class Cost(co_attribs.Attribute):
    def __init__(self,credits,metal,crystal,**kwargs):
        super().__init__(**kwargs)
        self.credits = co_attribs.AttribNum(**credits)
        self.metal = co_attribs.AttribNum(**metal)
        self.crystal = co_attribs.AttribNum(**crystal)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.credits==other.credits)
                        and (self.metal==other.metal)
                        and (self.crystal==other.crystal))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.credits.toString(1)
        res += self.metal.toString(1)
        res += self.crystal.toString(1)
        return indentText(res,indention)

class EntityRef(co_attribs.FileRef):
    _REFTYPE = "Entity File"
    _invalidTypeMsg = ("The given type \'{}\' is invalid. Valid types are "
                       +str(POSSIBLE_ENTITY_TYPES))

    def __init__(self,identifier,val,types,**kwargs):
        if not types:
            raise ValueError("\'EntityRef.types\' must not be empty.")
        for t in types:
            if t not in POSSIBLE_ENTITY_TYPES:
                raise ValueError(EntityRef._invalidTypeMsg.format(t))
        super().__init__(identifier=identifier,val=val,subdir="GameInfo",
                extension="entity",**kwargs)
        self._types = types

    def __eq__(self,other):
        return super().__eq__(other) and (self.types==other.types)

    @property
    def types(self):
        return self._types

    @types.setter
    def types(self,newtypes):
        if not newtypes:
            raise ValueError("\'EntityRef.types\' must not be empty.")
        for t in newtypes:
            if t not in POSSIBLE_ENTITY_TYPES:
                raise ValueError(EntityRef._invalidTypeMsg.format(t))
        self._types = newtypes

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod)
        if not res and self.ref:
            res.extend(currMod.entities.checkManifest(self.ref))
            checkRes = self.checkType(self._types,currMod)
            if checkRes==TypeCheckResult.invalid:
                res.append(WrongEntityTypeProb(self.ref,self._types,
                        probLine=self.linenum))
            elif checkRes==TypeCheckResult.na:
                res.append(co_probs.BasicProblem(
                        "Entity type for reference "+self.ref
                        +" could not be checked.",
                        severity=co_probs.ProblemSeverity.warn,
                        probLine=self.linenum))
        for prob in res:
            if not prob.probLine:
                prob.probLine = self.linenum
        return res

    def checkType(self,expTypes,currMod):
        if self.ref:
            return currMod.entities.checkEntityType(self.ref,expTypes)
        else:
            return []

class PrereqDef(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,subject,level,**kwargs):
        super().__init__(**kwargs)
        self.subject = EntityRef(types=["ResearchSubject"],**subject)
        self.level = co_attribs.AttribNum(**level)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.subject==other.subject)
                        and (self.level==other.level))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.subject.toString(1)
        res += self.level.toString(1)
        return indentText(res,indention)

class ResearchPrerequisite(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,prereqs,reqFaction,reqCompSubjects,**kwargs):
        super().__init__(**kwargs)
        self.prereqs = co_attribs.AttribList(elemType=PrereqDef,**prereqs)
        self.reqFaction = ui.StringReference(canBeEmpty=True,**reqFaction)
        self.reqCompSubjects = co_attribs.AttribNum(**reqCompSubjects)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.prereqs==other.prereqs)
                        and (self.reqFaction==other.reqFaction)
                        and (self.reqCompSubjects==other.reqCompSubjects))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.prereqs.toString(1)
        res += self.reqFaction.toString(1)
        res += self.reqCompSubjects.toString(1)
        return indentText(res,indention)

class MeshInfo(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,mesh,**kwargs):
        super().__init__(**kwargs)
        self.mesh = meshes.MeshRef(**mesh)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.mesh==other.mesh)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.mesh.toString(1)
        return indentText(res,indention)

"""################################ Files ##################################"""

class Entity(co_basics.IsFile,co_basics.HasReference):
    parser = g_header

    def __init__(self,entityType,**kwargs):
        super().__init__(**kwargs)
        self._etype = co_attribs.AttribEnum(possibleVals=POSSIBLE_ENTITY_TYPES,
                **entityType)

    @property
    def etype(self):
        return self._etype

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.etype==other.etype)

    @abc.abstractmethod
    def toString(self,indention):
        res = super().toString(0)
        res += self.etype.toString(0)
        return indentText(res,indention)

    @classmethod
    def parseType(cls,filepath):
        if not os.path.exists(filepath):
            p = co_files.correctCaseInsensitive(filepath)
            if p:
                filepath = p
            else:
                raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        try:
            with open(filepath,'r',encoding="utf-8-sig") as f:
                f.readline()
                res = g_entityType.parseString(f.readline())
                res = res[0].val
        except pp.ParseException as e:
            res = [
                co_probs.ParseProblem(filepath,e,probFile=filepath)]
        return res

"""############################# Properties ################################"""

class Property(co_basics.Checkable):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    @abc.abstractmethod
    def __eq__(self,other):
        return super().__eq__(other)

class MainViewVisible(Property):
    def __init__(self,zoomDist,shadowMin,shadowMax,**kwargs):
        super().__init__(**kwargs)
        self.zoomDist = co_attribs.AttribNum(**zoomDist)
        self.shadowMin = co_attribs.AttribNum(**shadowMin)
        self.shadowMax = co_attribs.AttribNum(**shadowMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.zoomDist==other.zoomDist)
                        and (self.shadowMin==other.shadowMin)
                        and (self.shadowMax==other.shadowMax))

class UIVisible(Property,co_basics.HasReference):
    def __init__(self,descString,iconHud,iconInfocard,nameString,iconHudSmall,
            **kwargs):
        super().__init__(**kwargs)
        self.descString = ui.StringReference(**descString)
        self.iconHud = ui.BrushRef(**iconHud)
        self.iconInfocard = ui.BrushRef(**iconInfocard)
        self.nameString = ui.StringReference(**nameString)
        self.iconHudSmall = ui.BrushRef(**iconHudSmall)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.descString==other.descString)
                        and (self.iconHud==other.iconHud)
                        and (self.iconInfocard==other.iconInfocard)
                        and (self.nameString==other.nameString)
                        and (self.iconHudSmall==other.iconHudSmall))

class MainViewSelectable(MainViewVisible,UIVisible):
    def __init__(self,iconMainView,pic,**kwargs):
        super().__init__(**kwargs)
        self.iconMainView = ui.BrushRef(**iconMainView)
        self.pic = ui.BrushRef(**pic)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.iconMainView==other.iconMainView)
                        and (self.pic==other.pic))

class HasAbilities(MainViewVisible,co_basics.HasReference):
    def __init__(self,abilities,antimatterRestore,antimatterMax,**kwargs):
        super().__init__(**kwargs)
        self.abilities = tuple(map(
                lambda vals:EntityRef(canBeEmpty=True,types=["Ability"],
                        **vals),
                abilities))
        self.antimatterMax = co_attribs.AttribNum.factory(antimatterMax)
        self.antimatterRestore = co_attribs.AttribNum.factory(antimatterRestore)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.abilities==other.abilities)
                        and (self.antimatterMax==other.antimatterMax)
                        and (self.antimatterRestore==other.antimatterRestore))

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for abil in self.abilities:
            res.extend(abil.check(currMod,recursive))
        return res

    def outputBuffchain(self,currMod,indention):
        res = os.path.basename(self.filepath)+":\n"
        hasChain = False
        for ab in self.abilities:
            ref = ab.ref
            if ref:
                abInst = currMod.entities.getEntity(ref)
                if not isinstance(abInst,list):
                    res += abInst.outputBuffchain(currMod,indention+1)
                else:
                    for prob in abInst:
                        res += prob.toString(indention+1)
                hasChain = True
        if not hasChain:
            res += "\tNo Buffs Referenced\n"
        return indentText(res,indention)
