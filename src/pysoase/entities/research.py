"""Contains parsers, entities and attributs concerning research
"""

import itertools

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe
import pysoase.mod.ui as ui

"""############################### Parser ##################################"""

"""### Types ###"""

ALLIANCE_TYPES = ["AntimatterPact","ArmorPact","BeamWeaponPact","CulturePact",
    "EfficiencyPact","EnergyWeaponCooldownPact","Invalid","MarketPact",
    "MassReductionPact","MetalCrystalPact","PhaseJumpPact",
    "PlanetBombingPact","ResearchPact","ShieldPact","SupplyPact",
    "TacticalSlotsPact","ThrusterPact","TradeIncomePact",
    "WeaponCooldownPact"]
APPLY_BUFF_RESEARCH_BOOL_MODIFIER_TYPES = [
    "ApplyBuffToAllOwnedShipsWhenOwnedPlanetFalls",
    "ApplyBuffToOwnedPlanetAfterLastEnemyShipLeavesOrDies",
    "ApplyBuffToPlanetAfterColonization",
    "ApplyBuffToPlanetAfterPlayerDestroys","ApplyBuffToPlanetsAfterScuttle",
    "ApplyBuffToShipsAfterHyperspace"]
SIMPLE_RESEARCH_BOOL_MODIFIER_TYPES = ["AllowAlliesToUsePhaseGates",
    "AllowGalaxyTravel","AllowMissionOffers","AllowPacts",
    "AllowPirateMissions","AllowResourceDemandAndGifts",
    "AllowWormHoleTravel",
    "CapitalShipsTitansAndStarBasesCountAsPlanetForNoGameLoss",
    "CultureSpreadIsAlwaysVisible","DestabilizationProtection",
    "FriendlyInsurgentShipsSpawnedAtHomePlanetWhenEnemyPlanetLiberated",
    "FriendlyWithPiratesRebelNeutrals","FriendlyWithPirates",
    "FriendlyWithRebelNeutrals","HyperspaceDetectionAnyJumps",
    "HyperspaceDetectionOneJump","HyperspaceDetectionTwoJumps",
    "JumpBlockerImmune","PirateRaidsOneLevelLower",
    "PlanetsDetectedByCulture","RemoveRacialRelationPenalty",
    "ResearchVictory","SpawnCopyOfPirateRaidsAtRandomPlanet",
    "StarBasesCanHyperspace","StarBasesCanHyperspaceOnlyToPhaseNodes",
    "StripPlanetsOnScuttle"]
SIMPLE_RESEARCH_FLOAT_MODIFIER_TYPES = ["AllegianceFromCultureMaxPerc",
    "AntimatterCapacityAdjustment","AntimatterRegenAdjustment",
    "ArmorBaseAdjustment","ArmorBaseAdjustmentWhenInFriendlyGravityWell",
    "BlackMarketBuyPriceAdjustment","BombingDamageAsDamageDealerAdjustment",
    "BombingCooldownDecrease","BombingDamageAsDamageTargetAdjustment",
    "BombingPopulationKilledAsDamageDealerAdjustment",
    "BombingPopulationKilledPercAsDamageTarget",
    "BombingRangeAdjustment","BountyBoughtAdjustment",
    "BountyCollectedAdjustment","CapitalShipAccelerationAdjustment",
    "CapitalShipAndTitanAngularThrustAdjustment",
    "CapitalShipAngularThrustAdjustment",
    "CapitalShipCultureProtectRateAdjustment","CapitalShipMaxSlotLevel",
    "CapitalShipMaxSlots","CapitalShipMaxTrainableLevel",
    "CapitalShipTaxIncome","CapitalShipWeaponTargetPerBankAdjustment",
    "CargoShipArmorBaseAdjustment","CargoShipCapacityAdjustment",
    "CargoShipHullPointsMaxAdjustment","CarrierFightersPerSquadAdjustment",
    "ChanceToConvertFrigatesHyperspacingFromPlanet",
    "ChanceToIgnoreShieldsAsDamageTarget",
    "ChanceToRespawnFrigateAtHomeworldUponDeath",
    "ChanceToTemporarilyCloneEnemyFrigateWhenKilled",
    "ChanceToTemporarilyCloneEnemyFrigateWhenKilledDuration",
    "CivilianLabsPerCapitalShip","CreditCostAdjustment",
    "CreditsGainedPerPointOfBombingDamageDealt",
    "CultureAntimatterRegenAmount",
    "CultureChanceToIgnoreShieldsAsDamageTarget",
    "CultureEnemyAllegianceRateAdjustment","CultureEnemyBuildRateAdjustment",
    "CultureModuleCostAdjustment","CultureResistPercent",
    "CultureShieldMitigationAdjustment","CultureShieldRegenAmount",
    "CultureSpreadDecayAdjustment","CultureSpreadRateAdjustment",
    "CultureSpreadRateBaseAdjustment","CultureWeaponDamageAdjustment",
    "DerivativeCreditsFromPurchasesPercent",
    "DerivativeCreditsFromTradePercent","ExperienceAwardedAdjustment",
    "ExperienceAwardedAdjustmentWhenInFriendlyGravityWell",
    "ExperienceConstantGainLevelCap","ExperienceConstantGainRate",
    "ExtractionRateCrystalAdjustment","ExtractionRateFromResourceFocus",
    "ExtractionRateMetalAdjustment","HangarFightersPerSquadAdjustment",
    "HullPointsMaxAdjustment","HullPointsMaxBonusPerArmorPoint",
    "HullPointsMaxBonusPerArmorPointMax","HullPointsRegenAdjustment",
    "HyperspaceAntimatterCostAdjustment",
    "HyperspaceAntimatterCostAdjustmentWhenGoingToFriendlyGravityWell",
    "HyperspaceBetweenSystemSpeedAdjustment",
    "HyperspaceChargeUpRateAdjustment","HyperspaceExitDistanceAdjustment",
    "HyperspaceInSystemSpeedAdjustment","HyperspaceSpeedPhaseGateAdjustment",
    "IncomePercLost","MassReduction","MilitaryLabsPerCapitalShip",
    "ModuleBuildRateAdjustment","ModuleCostForSimilarTypesAdjustment",
    "OrbitalCannonMaxAdjustment","OrbitalCannonPriceAdjustment",
    "OrbitalCannonSlotCostAdjustment","PlanetHealthRegenRateAdjustment",
    "PlanetSlotsCivilianIncrease","PlanetSlotsShipsAdjustment",
    "PlanetSlotsTacticalIncrease","PopulationGrowthRateAdjustment",
    "PopulationTaxRateAdjustment",
    "PopulationTaxRateAdjustmentWhenEnemyInGravityWell",
    "QuestDeadlineAdjustment","QuestFailureHappinessAdjustment",
    "QuestRewardHappinessAdjustment","RebellionLevel",
    "RefineryShipCapacityAdjustment","RelationshipBonus",
    "ResearchBuildRateAdjustment","ResearchCostAdjustment",
    "ResourceCostAdjustment","SalvageWreckagePercent","ScuttleRateAdjustment",
    "ScuttleValueAdjustment","SelfRelationshipBonus",
    "ShieldMaxMitigationAdjustment","ShieldPointsMaxAdjustment",
    "ShieldPointsRegenAdjustment","ShipBuildRateAdjustment",
    "ShipBuildRateAdjustmentWhenEnemyInGravityWell","ShipMaxSlotLevel",
    "ShipMaxSlots",
    "ShipMaxSlotsWhenOnlyAlliedWithSameRaceSameFactionPlayers",
    "StarBaseArmorAdjustment","StarBaseBuildRateAtEnemyOrbitBodyAdjustment",
    "StarBaseHullPointsMaxAdjustment","StarBaseShieldPointsMaxAdjustment",
    "StarBaseShieldPointsRegenAdjustment","StarBaseUpgradeCostAdjustment",
    "StarBaseUpgradeMaxAdjustment","StarBaseWeaponDamageAdjustment",
    "StarBaseWeaponTargetPerBankAdjustment",
    "StarBasesPerGravityWellAdjustment","TacticalStructureArmorAdjustment",
    "TacticalStructureHullPointsMaxAdjustment",
    "TacticalStructureWeaponDamageAdjustment",
    "TemporaryRelationshipFromGiveCreditsValueAdjustment",
    "TemporaryRelationshipFromGiveCrystalValueAdjustment",
    "TemporaryRelationshipFromGiveMetalValueAdjustment",
    "TemporaryRelationshipFromGiveResourceMaxAdjustment",
    "TitanConstructionCap","TradeGoodsValueAdjustment",
    "TradeGoodsValueAdjustmentWithNonAlliedPlayers",
    "WeaponTargetsPerBankAdjustmentCapitalShipsOrStarbases"]
PLANETUPGRADE_RESEARCH_FLOAT_MODIFIER_TYPES = [
    "PlanetUpgradeBuildRateAdjustment","PlanetUpgradeCostAdjustment"]
PLANETUPGRADE_TYPES = ["ArtifactLevel","CivilianModules","Home","Industry",
    "Infrastructure","Invalid","Population","Social","TacticalModules"]
PLANET_RESEARCH_FLOAT_MODIFIER_TYPES = ["PlanetPopulationCapAdjustment"]
RESEARCH_FIELDS = ["Artifact","Combat","Defense","Diplomacy","Fleet",
    "NonCombat","PactBonus","ResearchUnlock"]
WEAPON_RESEARCH_FLOAT_MODIFIER_TYPES = ["WeaponDamageAdjustment",
    "WeaponDamageAdjustmentForEachAlliedPlayerInGravityWell",
    "WeaponDamageAdjustmentWhenInFriendlyGravityWell",
    "WeaponDamageAdjustmentWhenInGravityWellBeingBombed",
    "WeaponIgnoresShieldsAdjustment","WeaponRangeAdjustment",
    "WeaponRateOfFireAdjustment",
    "WeaponRateOfFireAdjustmentWhenInGravityWellBeingBombed"]

"""### Research Subject ###"""
g_researchWinPos = pp.Group(
        co_parse.g_linestart
        +pp.Keyword("pos")("identifier")
        +co_parse.g_space
        +co_parse.g_pos2("coord")
        +co_parse.g_eol
)("pos")
g_researchWinLoc = pp.Group(
        co_parse.genHeading("researchWindowLocation")
        +co_parse.genIntAttrib("block")("block")
        +g_researchWinPos
)("winloc")
g_baseCost = pp.Group(
        co_parse.genHeading("BaseCost")
        +coe.g_cost
)("baseCost")
g_costIncrease = pp.Group(
        co_parse.genHeading("PerLevelCostIncrease")
        +coe.g_cost
)("costIncrease")
g_colonizeBoolModifier = (
    co_parse.genKeyValPair("modifierType","PlanetIsColonizable")("modType")
    +co_parse.genEnumAttrib("linkedPlanetType",coe.PLANETTEYPE_FOR_RESEARCH)(
            "linkedPlanet")
)
g_pactBoolModifier = (
    co_parse.genKeyValPair("modifierType","UnlockPact")("modType")
    +co_parse.genEnumAttrib("allianceType",ALLIANCE_TYPES)("allianceType")
    +co_parse.genStringAttrib("pactUnlockEntityDefName")("pactUnlockEntity")
)
g_applyBuffBoolModifier = (
    co_parse.genEnumAttrib("modifierType",
            APPLY_BUFF_RESEARCH_BOOL_MODIFIER_TYPES
    )("modType")
    +co_parse.genStringAttrib("buffToApply")("buff")
)
g_factionBoolModifier = (
    co_parse.genEnumAttrib("modifierType",["SetFaction"])("modType")
    +co_parse.genStringAttrib("factionNameID")("faction")
)
g_researchBoolModifier = pp.Group(
        co_parse.genHeading("researchModifier")
        +pp.Or([co_parse.genEnumAttrib("modifierType",
                SIMPLE_RESEARCH_BOOL_MODIFIER_TYPES
        )("modType"),
            g_colonizeBoolModifier,
            g_pactBoolModifier,
            g_applyBuffBoolModifier,
            g_factionBoolModifier
        ])
)
g_floatModValues = (
    co_parse.genDecimalAttrib("baseValue")("baseValue")
    +co_parse.genDecimalAttrib("perLevelValue")("levelValue")
)
g_researchSimpleFloatModifier = (
    co_parse.genEnumAttrib("modifierType",SIMPLE_RESEARCH_FLOAT_MODIFIER_TYPES)(
            "modType")
    +g_floatModValues
)
g_researchPlUpgradeFloatModifier = (
    co_parse.genEnumAttrib("modifierType",
            PLANETUPGRADE_RESEARCH_FLOAT_MODIFIER_TYPES)("modType")
    +g_floatModValues
    +co_parse.genEnumAttrib("linkedPlanetUpgradeType",PLANETUPGRADE_TYPES)(
            "linked")
)
g_researchPlFloatModifier = (
    co_parse.genEnumAttrib("modifierType",PLANET_RESEARCH_FLOAT_MODIFIER_TYPES)(
            "modType")
    +g_floatModValues
    +co_parse.genEnumAttrib("linkedPlanetType",coe.PLANETTEYPE_FOR_RESEARCH)(
            "linked")
)
g_researchWeaponFloatModifier = (
    co_parse.genEnumAttrib("modifierType",WEAPON_RESEARCH_FLOAT_MODIFIER_TYPES)(
            "modType")
    +g_floatModValues
    +co_parse.genEnumAttrib("linkedWeaponClass",coe.WEAPON_CLASSES)(
            "linked")
)
g_researchFloatModifier = pp.Group(
        co_parse.genHeading("researchModifier")
        +pp.Or([
            g_researchSimpleFloatModifier,
            g_researchPlFloatModifier,
            g_researchPlUpgradeFloatModifier,
            g_researchWeaponFloatModifier])
)
g_ResearchSubjectEntity = (
    coe.genEntityHeader("ResearchSubject")
    +coe.g_uiIcons
    +coe.g_nameStringCap
    +coe.g_descriptionStringCap
    +g_researchWinLoc
    +co_parse.genEnumAttrib("ResearchField",RESEARCH_FIELDS)("researchField")
    +coe.g_prereqs
    +co_parse.genIntAttrib("MinimumArtifactLevel")("minArtifact")
    +co_parse.genDecimalAttrib("BaseUpgradeTime")("baseUpgTime")
    +co_parse.genDecimalAttrib("PerLevelUpgradeTime")("lvlUpgTime")
    +g_baseCost
    +g_costIncrease
    +co_parse.genIntAttrib("Tier")("tier")
    +co_parse.genBoolAttrib("onlyWorksIfTierLabsExist")("worksWithLabs")
    +co_parse.genIntAttrib("MaxNumResearchLevels")("maxLevels")
    +co_parse.genDecimalAttrib("priority")("priority")
    +co_parse.genListAttrib("researchBoolModifiers",g_researchBoolModifier)(
            "boolModifiers")
    +co_parse.genListAttrib("researchFloatModifiers",g_researchFloatModifier)(
            "floatModifiers")
    +co_parse.genStringAttrib("artifactPicture")("picArtifact")
    +co_parse.genStringAttrib("uniqueOverlayBrush")("overlayBrush")
    +coe.g_dlcid
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################# Attributes ################################"""

class ResearchWinLoc(co_attribs.Attribute):
    def __init__(self,block,pos,**kwargs):
        super().__init__(**kwargs)
        self.block = co_attribs.AttribNum(**block)
        self.pos = co_attribs.AttribPos2d(**pos)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.block==other.block)
                        and (self.pos==other.pos))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.block.toString(1)
        res += self.pos.toString(1)
        return indentText(res,indention)

class ResearchMod(co_attribs.Attribute):
    _modTypes = list(itertools.chain(APPLY_BUFF_RESEARCH_BOOL_MODIFIER_TYPES,
            SIMPLE_RESEARCH_BOOL_MODIFIER_TYPES,
            SIMPLE_RESEARCH_FLOAT_MODIFIER_TYPES,
            PLANETUPGRADE_RESEARCH_FLOAT_MODIFIER_TYPES,
            PLANET_RESEARCH_FLOAT_MODIFIER_TYPES,
            WEAPON_RESEARCH_FLOAT_MODIFIER_TYPES,
            ["PlanetIsColonizable","UnlockPact","SetFaction"]))

    def __init__(self,modType,**kwargs):
        super().__init__(**kwargs)
        self.modType = co_attribs.AttribEnum(possibleVals=self._modTypes,**modType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.modType==other.modType)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.modType.toString(1)
        return indentText(res,indention)

    @classmethod
    def factory(cls,modType,**kwargs):
        modifierType = modType["val"]
        if modifierType in SIMPLE_RESEARCH_BOOL_MODIFIER_TYPES:
            return ResearchMod(modType=modType,**kwargs)
        elif modifierType=="PlanetIsColonizable":
            return ColonizeMod(modType=modType,**kwargs)
        elif modifierType=="UnlockPact":
            return PactMod(modType=modType,**kwargs)
        elif modifierType in APPLY_BUFF_RESEARCH_BOOL_MODIFIER_TYPES:
            return ApplyBuffMod(modType=modType,**kwargs)
        elif modifierType=="SetFaction":
            return FactionMod(modType=modType,**kwargs)
        elif modifierType in SIMPLE_RESEARCH_FLOAT_MODIFIER_TYPES:
            return FloatMod(modType=modType,**kwargs)
        elif modifierType in LinkedFloatMod.linkedMods:
            return LinkedFloatMod(modType=modType,**kwargs)
        else:
            raise RuntimeError(modifierType+" is an invalid research modifier "
                               +"type.")

class ColonizeMod(ResearchMod):
    def __init__(self,linkedPlanet,**kwargs):
        super().__init__(**kwargs)
        self.linkedPlanet = co_attribs.AttribEnum(
                possibleVals=coe.PLANETTEYPE_FOR_RESEARCH,**linkedPlanet)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.linkedPlanet==other.linkedPlanet)

    def toString(self,indention):
        res = super().toString(0)
        res += self.linkedPlanet.toString(1)
        return indentText(res,indention)

class PactMod(ResearchMod,co_basics.HasReference):
    def __init__(self,allianceType,pactUnlockEntity,**kwargs):
        super().__init__(**kwargs)
        self.allianceType = co_attribs.AttribEnum(possibleVals=ALLIANCE_TYPES,
                **allianceType)
        self.pactUnlockEntity = coe.EntityRef(types=["ResearchSubject"],
                **pactUnlockEntity)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.allianceType==other.allianceType)
                        and (self.pactUnlockEntity==other.pactUnlockEntity))

    def toString(self,indention):
        res = super().toString(0)
        res += self.allianceType.toString(1)
        res += self.pactUnlockEntity.toString(1)
        return indentText(res,indention)

class ApplyBuffMod(ResearchMod,co_basics.HasReference):
    def __init__(self,buff,**kwargs):
        super().__init__(**kwargs)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buff==other.buff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buff.toString(1)
        return indentText(res,indention)

class FactionMod(ResearchMod,co_basics.HasReference):
    def __init__(self,faction,**kwargs):
        super().__init__(**kwargs)
        self.faction = ui.StringReference(**faction)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.faction==other.faction)

    def toString(self,indention):
        res = super().toString(0)
        res += self.faction.toString(1)
        return indentText(res,indention)

class FloatMod(ResearchMod):
    def __init__(self,baseValue,levelValue,**kwargs):
        super().__init__(**kwargs)
        self.baseValue = co_attribs.AttribNum(**baseValue)
        self.levelValue = co_attribs.AttribNum(**levelValue)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.baseValue==other.baseValue)
                        and (self.levelValue==other.levelValue))

    def toString(self,indention):
        res = super().toString(0)
        res += self.baseValue.toString(1)
        res += self.levelValue.toString(1)
        return indentText(res,indention)

class LinkedFloatMod(FloatMod):
    linkedMods = list(itertools.chain(PLANET_RESEARCH_FLOAT_MODIFIER_TYPES,
            PLANETUPGRADE_RESEARCH_FLOAT_MODIFIER_TYPES,
            WEAPON_RESEARCH_FLOAT_MODIFIER_TYPES))

    def __init__(self,linked,**kwargs):
        super().__init__(**kwargs)
        posVals = []
        if self.modType.value in PLANETUPGRADE_RESEARCH_FLOAT_MODIFIER_TYPES:
            posVals = PLANETUPGRADE_TYPES
        elif self.modType.value in PLANET_RESEARCH_FLOAT_MODIFIER_TYPES:
            posVals = coe.PLANETTEYPE_FOR_RESEARCH
        elif self.modType.value in WEAPON_RESEARCH_FLOAT_MODIFIER_TYPES:
            posVals = coe.WEAPON_CLASSES
        else:
            raise RuntimeError("LinkedFloatMod called with invalid modType "
                               +self.modType.value)
        self.linked = co_attribs.AttribEnum(possibleVals=posVals,**linked)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.linked==other.linked)

    def toString(self,indention):
        res = super().toString(0)
        res += self.linked.toString(1)
        return indentText(res,indention)

"""############################ File Classes ###############################"""

class ResearchSubject(coe.Entity,coe.UIVisible,
        co_basics.HasReference):
    parser = g_ResearchSubjectEntity

    def __init__(self,winloc,researchField,prereqs,minArtifact,baseUpgTime,
            lvlUpgTime,baseCost,costIncrease,tier,worksWithLabs,maxLevels,
            priority,boolModifiers,floatModifiers,picArtifact,overlayBrush,
            dlcID,**kwargs):
        super().__init__(**kwargs)
        # smallHudIcon is not actually used by ResearchSubject entities,
        # any problems in that attribute can be safely ignored.
        self.iconHudSmall.ignore = True
        self.iconInfocard.canBeEmpty = True
        self.winloc = ResearchWinLoc(**winloc)
        self.researchField = co_attribs.AttribEnum(possibleVals=RESEARCH_FIELDS,
                **researchField)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)
        self.minArtifact = co_attribs.AttribNum(**minArtifact)
        self.baseUpgTime = co_attribs.AttribNum(**baseUpgTime)
        self.lvlUpgTime = co_attribs.AttribNum(**lvlUpgTime)
        self.baseCost = coe.Cost(**baseCost)
        self.costIncrease = coe.Cost(**costIncrease)
        self.tier = co_attribs.AttribNum(**tier)
        self.worksWithLabs = co_attribs.AttribBool(**worksWithLabs)
        self.maxLevels = co_attribs.AttribNum(**maxLevels)
        self.priority = co_attribs.AttribNum(**priority)
        self.boolModifiers = co_attribs.AttribList(elemType=ResearchMod,factory=True,
                **boolModifiers)
        self.floatModifiers = co_attribs.AttribList(elemType=ResearchMod,factory=True,
                **floatModifiers)
        if self.researchField.value=="Artifact":
            self.picArtifact = ui.BrushRef(**picArtifact)
        else:
            self.picArtifact = ui.BrushRef(canBeEmpty=True,**picArtifact)
        self.overlayBrush = ui.BrushRef(canBeEmpty=True,**overlayBrush)
        self.dlcID = co_attribs.AttribNum(**dlcID)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.winloc==other.winloc)
                        and (self.researchField==other.researchField)
                        and (self.prereqs==other.prereqs)
                        and (self.minArtifact==other.minArtifact)
                        and (self.baseUpgTime==other.baseUpgTime)
                        and (self.lvlUpgTime==other.lvlUpgTime)
                        and (self.baseCost==other.baseCost)
                        and (self.costIncrease==other.costIncrease)
                        and (self.tier==other.tier)
                        and (self.worksWithLabs==other.worksWithLabs)
                        and (self.maxLevels==other.maxLevels)
                        and (self.priority==other.priority)
                        and (self.boolModifiers==other.boolModifiers)
                        and (self.floatModifiers==other.floatModifiers)
                        and (self.picArtifact==other.picArtifact)
                        and (self.overlayBrush==other.overlayBrush)
                        and (self.dlcID==other.dlcID))

    def toString(self,indention):
        res = super().toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.winloc.toString(0)
        res += self.researchField.toString(0)
        res += self.prereqs.toString(0)
        res += self.minArtifact.toString(0)
        res += self.baseUpgTime.toString(0)
        res += self.lvlUpgTime.toString(0)
        res += self.baseCost.toString(0)
        res += self.costIncrease.toString(0)
        res += self.tier.toString(0)
        res += self.worksWithLabs.toString(0)
        res += self.maxLevels.toString(0)
        res += self.priority.toString(0)
        res += self.boolModifiers.toString(0)
        res += self.floatModifiers.toString(0)
        res += self.picArtifact.toString(0)
        res += self.overlayBrush.toString(0)
        res += self.dlcID.toString(0)
        return indentText(res,indention)
