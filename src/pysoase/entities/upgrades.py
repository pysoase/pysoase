""" Contains parsers and classes for starbase and titan upgrades.
"""

import abc

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe

"""################################# Types #################################"""

BOOL_PROPERTY_TYPES = ["CanPreventEnemyColonize",
    "CanPreventPlanetLossByBombing"]
SBUPGRADE_PROPERTY_TYPES = ["BaseTradeIncomeRate","CultureMeshLevel",
    "CultureSpreadRate","EnemyCultureRepelRate","FrigateBuildRate",
    "MaxAllegianceBonus","MaxNumSquads","MaxShieldMitigation","Population",
    "TradePortMeshLevel","WeaponUpgradeLevel"]
SHARED_PROPERTY_TYPES = ["AbilityALevel","AbilityBLevel","AbilityCLevel",
    "AbilityDLevel","AntiMatterRestoreRate","BaseArmorPoints",
    "HullPointRestoreRate","MaxAntiMatterPoints","MaxHullPoints",
    "MaxShieldPoints","ShieldPointRestoreRate","WeaponDamage"]
SBUPGRADE_PROPERTY_TYPES.extend(SHARED_PROPERTY_TYPES)
TIUPGRADE_PROPERTY_TYPES = ["AbilityCooldownRate","AbilityELevel"]
TIUPGRADE_PROPERTY_TYPES.extend(SHARED_PROPERTY_TYPES)

"""################################ Parsers ################################"""

"""### Starbase Upgrades ###"""

g_SBUpgradeProperty = pp.Group(
        co_parse.genHeading("upgradeProperty")
        +co_parse.genEnumAttrib("propertyType",SBUPGRADE_PROPERTY_TYPES)("propType")
        +co_parse.genDecimalAttrib("value")("value"))
g_sbUpgradeProperties = (
    co_parse.genListAttrib("upgradePropertyCount",g_SBUpgradeProperty)(
            "propsFloat")
    +co_parse.genListAttrib("upgradeBoolPropertyCount",
            co_parse.genEnumAttrib("upgradeBoolProperty",BOOL_PROPERTY_TYPES)
    )("propsBool")
)
g_sbUpgradeStage = pp.Group(
        co_parse.genHeading("stage")
        +coe.g_price
        +co_parse.genDecimalAttrib("upgradeTime")("upgTime")
        +coe.g_nameString
        +coe.g_descriptionStringShort
        +coe.g_researchPrereqs
        +coe.g_uiIcons
        +g_sbUpgradeProperties
)
g_StarBaseUpgradeEntity = (
    coe.genEntityHeader("StarBaseUpgrade")
    +co_parse.genListAttrib("stageCount",g_sbUpgradeStage)("stages")
    +co_parse.genDecimalAttrib("xpModifier")("xpMod")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Titan Upgrades ###"""

g_tiUpgradeProperty = pp.Group(
        co_parse.genHeading("upgradeProperty")
        +co_parse.genEnumAttrib("propertyType",TIUPGRADE_PROPERTY_TYPES)("propType")
        +co_parse.genDecimalAttrib("value")("value")
)
g_tiUpgradeProperties = (
    co_parse.genListAttrib("upgradePropertyCount",g_tiUpgradeProperty)(
            "propsFloat")
    +co_parse.genListAttrib("upgradeBoolPropertyCount",
            co_parse.genEnumAttrib("upgradeBoolProperty",BOOL_PROPERTY_TYPES)
    )("propsBool")
)
g_tiUpgradeStage = pp.Group(
        co_parse.genHeading("stage")
        +coe.g_nameString
        +coe.g_descriptionStringShort
        +coe.g_researchPrereqs
        +co_parse.genIntAttrib("levelRequirement")("levelReq")
        +coe.g_uiIcons
        +g_tiUpgradeProperties
)
g_TitanUpgradeEntity = (
    coe.genEntityHeader("TitanUpgrade")
    +co_parse.genListAttrib("stageCount",g_tiUpgradeStage)("stages")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

class FloatUpgradeProperty(co_attribs.Attribute):
    def __init__(self,propType,value,possTypes,**kwargs):
        super().__init__(**kwargs)
        self.propType = co_attribs.AttribEnum(possibleVals=possTypes,**propType)
        self.value = co_attribs.AttribNum(**value)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.propType==other.propType)
                        and (self.value==other.value))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.propType.toString(1)
        res += self.value.toString(1)
        return indentText(res,indention)

class TiFloatProp(FloatUpgradeProperty):
    def __init__(self,**kwargs):
        super().__init__(possTypes=TIUPGRADE_PROPERTY_TYPES,**kwargs)

class SbFloatProp(FloatUpgradeProperty):
    def __init__(self,**kwargs):
        super().__init__(possTypes=SBUPGRADE_PROPERTY_TYPES,**kwargs)

class BasicUpgradeStage(coe.UIVisible,co_attribs.Attribute,
        co_basics.HasReference):
    def __init__(self,prereqs,propsBool,propsFloat,floatPropType,**kwargs):
        super().__init__(**kwargs)
        self.iconInfocard.canBeEmpty = True
        self.prereqs = coe.ResearchPrerequisite(**prereqs)
        self.propsBool = co_attribs.AttribList(elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":BOOL_PROPERTY_TYPES},**propsBool)
        self.propsFloat = co_attribs.AttribList(elemType=floatPropType,**propsFloat)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.prereqs==other.prereqs)
                        and (self.propsBool==other.propsBool)
                        and (self.propsFloat==other.propsFloat))

    @abc.abstractmethod
    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        return indentText(res,indention)

class SbUpgradeStage(BasicUpgradeStage):
    def __init__(self,price,upgTime,**kwargs):
        super().__init__(floatPropType=SbFloatProp,**kwargs)
        self.price = coe.Cost(**price)
        self.upgTime = co_attribs.AttribNum(**upgTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.price==other.price)
                        and (self.upgTime==other.upgTime))

    def toString(self,indention):
        res = super().toString(0)
        res += self.price.toString(1)
        res += self.upgTime.toString(1)
        res += self.nameString.toString(1)
        res += self.descString.toString(1)
        res += self.prereqs.toString(1)
        res += self.iconHud.toString(1)
        res += self.iconHudSmall.toString(1)
        res += self.iconInfocard.toString(1)
        res += self.propsFloat.toString(1)
        res += self.propsBool.toString(1)
        return indentText(res,indention)

class TiUpgradeStage(BasicUpgradeStage):
    def __init__(self,levelReq,**kwargs):
        super().__init__(floatPropType=TiFloatProp,**kwargs)
        self.levelReq = co_attribs.AttribNum(**levelReq)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.levelReq==other.levelReq)

    def toString(self,indention):
        res = super().toString(0)
        res += self.nameString.toString(1)
        res += self.descString.toString(1)
        res += self.prereqs.toString(1)
        res += self.levelReq.toString(1)
        res += self.iconHud.toString(1)
        res += self.iconHudSmall.toString(1)
        res += self.iconInfocard.toString(1)
        res += self.propsFloat.toString(1)
        res += self.propsBool.toString(1)
        return indentText(res,indention)

"""############################## File Classes #############################"""

class TitanUpgrade(coe.Entity,co_basics.HasReference):
    parser = g_TitanUpgradeEntity

    def __init__(self,stages,**kwargs):
        super().__init__(**kwargs)
        self.stages = co_attribs.AttribList(elemType=TiUpgradeStage,**stages)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.stages==other.stages)

    def toString(self,indention):
        res = super().toString(0)
        res += self.stages.toString(0)
        return indentText(res,indention)

class StarbaseUpgrade(coe.Entity,co_basics.HasReference):
    parser = g_StarBaseUpgradeEntity

    def __init__(self,xpMod,stages,**kwargs):
        super().__init__(**kwargs)
        self.stages = co_attribs.AttribList(elemType=SbUpgradeStage,**stages)
        self.xpMod = co_attribs.AttribNum(**xpMod)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.stages==other.stages)
                        and (self.xpMod==other.xpMod))

    def toString(self,indention):
        res = super().toString(0)
        res += self.stages.toString(0)
        res += self.xpMod.toString(0)
        return indentText(res,indention)
