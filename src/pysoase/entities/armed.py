""" Contains parsers and classes common to armed entities.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.mod.audio as audio
import pysoase.mod.meshes as mesh
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""################################# Types #################################"""

ATTACK_TYPES = ["ANTIHEAVY","ANTILIGHT","ANTIMEDIUM","ANTIMODULE",
    "ANTIVERYHEAVY","ANTIVERYLIGHT","CAPITALSHIP","COMPOSITE","CORVETTE",
    "TITAN"]
AUTOATTACK_RANGES = ["None","GravityWell"]
DMG_AFFECTS = ["AFFECTS_ONLY_HULL","AFFECTS_ONLY_SHIELDS",
    "AFFECTS_SHIELDS_AND_HULL"]
DMG_APPLY = ["OVERTIME","BACKLOADED"]
DMG_TYPE = ["PHYSICAL","ENERGY"]
MESH_LVL_UPG_TYPES = ["CultureMeshLevel","TradePortMeshLevel",
    "WeaponUpgradeLevel"]
STATCOUNT_TYPES = ["CapitalShipBattleship","CapitalShipCarrier",
    "CapitalShipUtility0","CapitalShipUtility1","CapitalShipUtility2",
    "FighterBomber","FighterCombat","FighterMineLayer","FrigateAntiFighter",
    "FrigateAntiModule","FrigateCarrier","FrigateColony",
    "FrigateConstructor","FrigateCorvette","FrigateEnvoy","FrigateHeavy",
    "FrigateLight","FrigateLongRange","FrigateMineLayer",
    "FrigateRefineryShip","FrigateScout","FrigateSiege",
    "FrigateSpawnedByAbility","FrigateStarBaseConstructor",
    "FrigateTradeShip","FrigateUtility0","FrigateUtility1","Invalid",
    "ModuleCannon","ModuleCapitalShipFactory","ModuleCombatLab",
    "ModuleCrystalExtractor","ModuleCultureCenter","ModuleFrigateFactory",
    "ModuleHangarDefense","ModuleJumpBlocker","ModuleMetalExtractor",
    "ModuleNonCombatLab","ModulePointDefense","ModuleRefinery",
    "ModuleRepair","ModuleTradePort","ModuleUtility","SquadBomber",
    "SquadCombat","SquadMineLayer","StarBase","Titan"]
WEAPON_TYPES = ["Beam","Projectile","Missile"]
WEAPON_BANK = ["FRONT","BACK","LEFT","RIGHT"]

"""################################ Parsers ################################"""

"""### Basic Elements ###"""

g_baseBuildTime = co_parse.genDecimalAttrib("baseBuildTime")("buildTime")
g_formation = co_parse.genIntAttrib("formationRank")("formationRank")
g_statType = co_parse.genEnumAttrib("statCountType",STATCOUNT_TYPES)("statcount")

"""### Carriers ###"""
g_squadrons = pp.Group(
        pp.Group(
                co_parse.genStringAttrib("squadTypeEntityDef:0")("squadDef")
                +co_parse.genDecimalAttrib("squadAntiMatterCost:0")("antimatter")
        )
        +pp.Group(
                co_parse.genStringAttrib("squadTypeEntityDef:1")("squadDef")
                +co_parse.genDecimalAttrib("squadAntiMatterCost:1")("antimatter")
        )
        +pp.Group(
                co_parse.genStringAttrib("squadTypeEntityDef:2")("squadDef")
                +co_parse.genDecimalAttrib("squadAntiMatterCost:2")("antimatter")
        )
        +pp.Group(
                co_parse.genStringAttrib("squadTypeEntityDef:3")("squadDef")
                +co_parse.genDecimalAttrib("squadAntiMatterCost:3")("antimatter")
        )
)("squads")

"""### Meshes ###"""

g_meshEffects = (
    co_parse.genStringAttrib("meshNameIncreasedEffectName")("meshIncreasedEffect")
    +co_parse.genStringAttrib("meshNameDecreasedEffectName")("meshDecreasedEffect")
)

g_researchCriterion = pp.Group(
        co_parse.genKeyValPair("criteriaType","Research")("conType")
        +pp.Group(
                co_parse.genHeading("prerequisites")
                +coe.g_researchPrerequesites
        )("prereqs")
)
g_lvlRequirement = pp.Group(
        co_parse.genHeading("levelRequirement")
        +co_parse.genEnumAttrib("upgradePropertyType",MESH_LVL_UPG_TYPES)(
                "upgradeType")
        +co_parse.genIntAttrib("upgradeLevel")("upgradeLevel")
)
g_lvlCriterion = pp.Group(
        co_parse.genKeyValPair("criteriaType","StarBaseUpgradeLevel")("conType")
        +co_parse.genListAttrib("levelRequirementCount",
                g_lvlRequirement,
                header="levelRequirements"
        )("reqs")
)
g_meshInfoCriterion = pp.Group(
        co_parse.genHeading("MeshNameInfo")
        +co_parse.genStringAttrib("meshName")("mesh")
        +pp.Or([
            pp.Group(
                    co_parse.genKeyValPair("criteriaType","None")("conType")
            )("criterion"),
            g_lvlCriterion("criterion"),
            g_researchCriterion("criterion")])
)
g_criterionMeshInfos = co_parse.genListAttrib("MeshNameInfoCount",
        g_meshInfoCriterion)(
        "meshes")

"""### Weapons ###"""

g_damageEnums = pp.Group(
        co_parse.genHeading("damageEnums")
        +co_parse.genEnumAttrib("AttackType",ATTACK_TYPES)("attackType")
        +co_parse.genEnumAttrib("DamageAffectType",DMG_AFFECTS)("dmgAffects")
        +co_parse.genEnumAttrib("DamageApplyType",DMG_APPLY)("dmgApply")
        +co_parse.genEnumAttrib("DamageType",DMG_TYPE)("dmgType")
        +co_parse.genEnumAttrib("WeaponClassType",coe.WEAPON_CLASSES)("weaponClass")
)("dmgEnums")
g_fireResearchConstraint = pp.Group(
        co_parse.genKeyValPair("fireConstraintType","Research")("conType")
        +coe.g_researchPrereqs
)

g_fireSBUpgradeConstraint = pp.Group(
        co_parse.genKeyValPair("fireConstraintType","StarBaseUpgradeLevel")(
                "conType")
        +co_parse.genIntAttrib("requiredStarBaseWeaponLevel")("reqLvl")
)
g_fireConstraint = (
    pp.Or([
        pp.Group(
                co_parse.genKeyValPair("fireConstraintType","CanAlwaysFire")(
                        "conType")
        ),
        g_fireResearchConstraint,
        g_fireSBUpgradeConstraint
    ])
)("fireConstraint")
g_sharedWeaponsEff = (
    co_parse.genDecimalAttrib("burstCount")("burstCount")
    +co_parse.genDecimalAttrib("burstDelay")("burstDelay")
    +co_parse.genDecimalAttrib("fireDelay")("fireDelay")
    +co_parse.genStringAttrib("muzzleEffectName")("muzzleEff")
    +co_parse.genDecimalAttrib("muzzleSoundMinRespawnTime")("sndMuzzleRespawn")
    +co_parse.genListAttrib("soundCount",
            co_parse.genStringAttrib("sound"),"muzzleSounds")(
            "sndMuzzle")
    +co_parse.genStringAttrib("hitEffectName")("hitEff")
    +co_parse.genListAttrib("soundCount",
            co_parse.genStringAttrib("sound"),
            "hitHullEffectSounds")("sndHitHull")
    +co_parse.genListAttrib("soundCount",
            co_parse.genStringAttrib("sound"),
            "hitShieldsEffectSounds")("sndHitShields")
)
g_beamWeaponsEffect = (
    co_parse.genKeyValPair("weaponType","Beam")("effectType")
    +g_sharedWeaponsEff
    +co_parse.genListAttrib("soundCount",
            co_parse.genStringAttrib("sound"),
            "beamEffectSounds")("sndBeam")
    +co_parse.genStringAttrib("beamGlowTextureName")("textureGlow")
    +co_parse.genStringAttrib("beamCoreTextureName")("textureCore")
    +co_parse.genDecimalAttrib("beamWidth")("width")
    +co_parse.genColorAttrib("beamGlowColor")("colorGlow")
    +co_parse.genColorAttrib("beamCoreColor")("colorCore")
    +co_parse.genDecimalAttrib("beamTilingRate")("tilingRate")
)
g_missileWeaponEffect = (
    co_parse.genKeyValPair("weaponType","Missile")("effectType")
    +g_sharedWeaponsEff
    +co_parse.genStringAttrib("missileTravelEffectName")("travelEff")
    +co_parse.genDecimalAttrib("missileStartTurningDistance")("turnDist")
    +co_parse.genDecimalAttrib("missileSlowTurnRate")("turnRate")
    +co_parse.genDecimalAttrib("missileMaxSlowTurnTime")("maxTurnTime")
)
g_projectileWeaponsEffect = (
    co_parse.genKeyValPair("weaponType","Projectile")("effectType")
    +g_sharedWeaponsEff
    +co_parse.genStringAttrib("projectileTravelEffectName")("travelEff")
)
g_weaponEffect = pp.Group(
        co_parse.genHeading("WeaponEffects")
        +pp.Or([g_beamWeaponsEffect,g_missileWeaponEffect,
            g_projectileWeaponsEffect])
)("weaponEff")
g_weapon = pp.Group(
        co_parse.genHeading("Weapon")
        +co_parse.genEnumAttrib("WeaponType",WEAPON_TYPES)("weaponType")
        +g_damageEnums
        +co_parse.genDecimalAttrib("DamagePerBank:FRONT")("dmgFront")
        +co_parse.genDecimalAttrib("DamagePerBank:BACK")("dmgBack")
        +co_parse.genDecimalAttrib("DamagePerBank:LEFT")("dmgLeft")
        +co_parse.genDecimalAttrib("DamagePerBank:RIGHT")("dmgRight")
        +co_parse.genDecimalAttrib("Range")("range")
        +co_parse.genDecimalAttrib("PreBuffCooldownTime")("cooldown")
        +co_parse.genBoolAttrib("CanFireAtFighter")("canAttackFighter")
        +co_parse.genBoolAttrib("SynchronizedTargeting")("syncTargeting")
        +co_parse.genDecimalAttrib("PointStaggerDelay")("pointStagger")
        +co_parse.genDecimalAttrib("TravelSpeed")("travelSpeed")
        +co_parse.genDecimalAttrib("Duration")("duration")
        +g_fireConstraint
        +g_weaponEffect
)
g_firingAlignment = pp.Or([
    pp.Group(co_parse.genEnumAttrib("firingAlignmentType",["Default"])(
            "alignmentType")),
    pp.Group(co_parse.genEnumAttrib("firingAlignmentType",["TiltForward"])(
            "alignmentType")+co_parse.genDecimalAttrib("firingTiltAngle")(
            "tiltAngle"))
])("firingAlignment")
g_armed = (
    co_parse.genListAttrib("NumWeapons",g_weapon)("weapons")
    +co_parse.genIntAttrib("m_weaponIndexForRange")("indexForRange")
    +g_firingAlignment
    +co_parse.genIntAttrib("TargetCountPerBank:FRONT")("targetsFront")
    +co_parse.genIntAttrib("TargetCountPerBank:BACK")("targetsBack")
    +co_parse.genIntAttrib("TargetCountPerBank:LEFT")("targetsLeft")
    +co_parse.genIntAttrib("TargetCountPerBank:RIGHT")("targetsRight")
    +co_parse.genBoolAttrib("canOnlyTargetStructures")("onlyTargetStructures")
)
g_attackBehaviour = (
    co_parse.genEnumAttrib("defaultAutoAttackRange",AUTOATTACK_RANGES)(
            "autoattackRange")
    +co_parse.genBoolAttrib("defaultAutoAttackOn")("autoattackOn")
    +co_parse.genBoolAttrib("prefersToFocusFire")("focusFire")
    +co_parse.genBoolAttrib("usesFighterAttack")("usesFighterAttack")
)

"""############################### Attributes ##############################"""

class SharedWeaponEffect(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,burstCount,burstDelay,fireDelay,muzzleEff,
            sndMuzzleRespawn,sndMuzzle,hitEff,sndHitHull,sndHitShields,
            effectType,**kwargs):
        super().__init__(**kwargs)
        self.effectType = co_attribs.AttribEnum(possibleVals=WEAPON_TYPES,**effectType)
        self.burstCount = co_attribs.AttribNum(**burstCount)
        self.burstDelay = co_attribs.AttribNum(**burstDelay)
        self.fireDelay = co_attribs.AttribNum(**fireDelay)
        self.muzzleEff = par.ParticleRef(**muzzleEff)
        self.sndMuzzleRespawn = co_attribs.AttribNum(**sndMuzzleRespawn)
        self.sndMuzzle = co_attribs.AttribList(elemType=audio.SoundRef,**sndMuzzle)
        self.hitEff = par.ParticleRef(**hitEff)
        self.sndHitHull = co_attribs.AttribList(elemType=audio.SoundRef,**sndHitHull)
        self.sndHitShields = co_attribs.AttribList(elemType=audio.SoundRef,
                **sndHitShields)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.burstCount==other.burstCount)
                        and (self.burstDelay==other.burstDelay)
                        and (self.fireDelay==other.fireDelay)
                        and (self.muzzleEff==other.muzzleEff)
                        and (self.sndMuzzleRespawn==other.sndMuzzleRespawn)
                        and (self.sndMuzzle==other.sndMuzzle)
                        and (self.hitEff==other.hitEff)
                        and (self.sndHitHull==other.sndHitHull)
                        and (self.sndHitShields==other.sndHitShields)
                        and (self.effectType==other.effectType))

    @classmethod
    def factory(cls,effectType,**kwargs):
        if effectType["val"]=="Beam":
            return BeamWeaponEffect(effectType=effectType,**kwargs)
        elif effectType["val"]=="Missile":
            return MissileWeaponEffect(effectType=effectType,**kwargs)
        elif effectType["val"]=="Projectile":
            return ProjectileWeaponEff(effectType=effectType,**kwargs)
        else:
            raise RuntimeError("Invalid effectType for weapon effect: "
                               +effectType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.effectType.toString(1)
        res += self.burstCount.toString(1)
        res += self.burstDelay.toString(1)
        res += self.fireDelay.toString(1)
        res += self.muzzleEff.toString(1)
        res += self.sndMuzzleRespawn.toString(1)
        res += self.sndMuzzle.toString(1)
        res += self.hitEff.toString(1)
        res += self.sndHitHull.toString(1)
        res += self.sndHitShields.toString(1)
        return indentText(res,indention)

class ProjectileWeaponEff(SharedWeaponEffect):
    def __init__(self,travelEff,**kwargs):
        super().__init__(**kwargs)
        self.travelEff = par.ParticleRef(**travelEff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.travelEff==other.travelEff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.travelEff.toString(1)
        return indentText(res,indention)

class MissileWeaponEffect(SharedWeaponEffect):
    def __init__(self,travelEff,turnDist,turnRate,maxTurnTime,**kwargs):
        super().__init__(**kwargs)
        self.travelEff = par.ParticleRef(**travelEff)
        self.turnDist = co_attribs.AttribNum(**turnDist)
        self.turnRate = co_attribs.AttribNum(**turnRate)
        self.maxTurnTime = co_attribs.AttribNum(**maxTurnTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.travelEff==other.travelEff)
                        and (self.turnDist==other.turnDist)
                        and (self.turnRate==other.turnRate)
                        and (self.maxTurnTime==other.maxTurnTime))

    def toString(self,indention):
        res = super().toString(0)
        res += self.travelEff.toString(1)
        res += self.turnDist.toString(1)
        res += self.turnRate.toString(1)
        res += self.maxTurnTime.toString(1)
        return indentText(res,indention)

class BeamWeaponEffect(SharedWeaponEffect):
    def __init__(self,sndBeam,textureGlow,textureCore,width,colorGlow,
            colorCore,tilingRate,**kwargs):
        super().__init__(**kwargs)
        self.sndBeam = co_attribs.AttribList(elemType=audio.SoundRef,**sndBeam)
        self.textureGlow = ui.UITextureRef(**textureGlow)
        self.textureCore = ui.UITextureRef(**textureCore)
        self.width = co_attribs.AttribNum(**width)
        self.colorGlow = co_attribs.AttribColor(**colorGlow)
        self.colorCore = co_attribs.AttribColor(**colorCore)
        self.tilingRate = co_attribs.AttribNum(**tilingRate)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.sndBeam==other.sndBeam)
                        and (self.textureGlow==other.textureGlow)
                        and (self.textureCore==other.textureCore)
                        and (self.width==other.width)
                        and (self.colorGlow==other.colorGlow)
                        and (self.colorCore==other.colorCore)
                        and (self.tilingRate==other.tilingRate))

    def toString(self,indention):
        res = super().toString(0)
        res += self.sndBeam.toString(1)
        res += self.textureGlow.toString(1)
        res += self.textureCore.toString(1)
        res += self.width.toString(1)
        res += self.colorGlow.toString(1)
        res += self.colorCore.toString(1)
        res += self.tilingRate.toString(1)
        return indentText(res,indention)

class LvlReq(co_attribs.Attribute):
    def __init__(self,upgradeType,upgradeLevel,**kwargs):
        super().__init__(**kwargs)
        self.upgradeType = co_attribs.AttribEnum(possibleVals=MESH_LVL_UPG_TYPES,
                **upgradeType)
        self.upgradeLevel = co_attribs.AttribNum(**upgradeLevel)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.upgradeType==other.upgradeType)
                        and (self.upgradeLevel==other.upgradeLevel))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.upgradeType.toString(1)
        res += self.upgradeLevel.toString(1)
        return indentText(res,indention)

class MeshConstraint(co_attribs.Attribute):
    _conTypes = ["None","Research","StarBaseUpgradeLevel"]

    def __init__(self,conType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.conType = co_attribs.AttribEnum(possibleVals=self._conTypes,**conType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.conType==other.conType)

    @classmethod
    def factory(cls,conType,**kwargs):
        t = conType["val"]
        if t=="None":
            return MeshConstraint(conType=conType,**kwargs)
        elif t=="Research":
            return MeshResearchConstraint(conType=conType,**kwargs)
        elif t=="StarBaseUpgradeLevel":
            return MeshSBConstraint(conType=conType,**kwargs)
        else:
            raise RuntimeError("Invalid mesh constraint type: "+t)

    def toString(self,indention):
        res = super().toString(0)
        res += self.conType.toString(0)
        return indentText(res,indention)

class MeshResearchConstraint(MeshConstraint,co_basics.HasReference):
    def __init__(self,prereqs,**kwargs):
        super().__init__(**kwargs)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.prereqs==other.prereqs)

    def toString(self,indention):
        res = super().toString(0)
        res += self.prereqs.toString(0)
        return indentText(res,indention)

class MeshSBConstraint(MeshConstraint):
    def __init__(self,reqs,**kwargs):
        super().__init__(**kwargs)
        self.reqs = co_attribs.AttribList(elemType=LvlReq,**reqs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.reqs==other.reqs)

    def toString(self,indention):
        res = super().toString(0)
        res += self.reqs.toString(0)
        return indentText(res,indention)

class MeshInfoConstruct(coe.MeshInfo):
    def __init__(self,criterion,**kwargs):
        super().__init__(**kwargs)
        self.criterion = MeshConstraint.factory(**criterion)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.criterion==other.criterion)

    def toString(self,indention):
        res = super().toString(0)
        res += self.criterion.toString(1)
        return indentText(res,indention)

class FireConstraint(co_attribs.Attribute):
    _conTypes = ["CanAlwaysFire","Research","StarBaseUpgradeLevel"]

    def __init__(self,conType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.conType = co_attribs.AttribEnum(possibleVals=self._conTypes,**conType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.conType==other.conType)

    @classmethod
    def factory(cls,conType,**kwargs):
        t = conType["val"]
        if t=="CanAlwaysFire":
            return FireConstraint(conType=conType,**kwargs)
        elif t=="Research":
            return FireResearchConstraint(conType=conType,**kwargs)
        elif t=="StarBaseUpgradeLevel":
            return FireSBConstraint(conType=conType,**kwargs)
        else:
            raise RuntimeError("Invalid mesh constraint type: "+t)

    def toString(self,indention):
        res = super().toString(0)
        res += self.conType.toString(0)
        return indentText(res,indention)

class FireResearchConstraint(FireConstraint,co_basics.HasReference):
    def __init__(self,prereqs,**kwargs):
        super().__init__(**kwargs)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.prereqs==other.prereqs)

    def toString(self,indention):
        res = super().toString(0)
        res += self.prereqs.toString(0)
        return indentText(res,indention)

class FireSBConstraint(FireConstraint):
    def __init__(self,reqLvl,**kwargs):
        super().__init__(**kwargs)
        self.reqLvl = co_attribs.AttribNum(**reqLvl)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.reqLvl==other.reqLvl)

    def toString(self,indention):
        res = super().toString(0)
        res += self.reqLvl.toString(0)
        return indentText(res,indention)

class DamageEnums(co_attribs.Attribute):
    def __init__(self,attackType,dmgAffects,dmgApply,dmgType,weaponClass,
            **kwargs):
        super().__init__(**kwargs)
        self.attackType = co_attribs.AttribEnum(possibleVals=ATTACK_TYPES,**attackType)
        self.dmgAffects = co_attribs.AttribEnum(possibleVals=DMG_AFFECTS,**dmgAffects)
        self.dmgApply = co_attribs.AttribEnum(possibleVals=DMG_APPLY,**dmgApply)
        self.dmgType = co_attribs.AttribEnum(possibleVals=DMG_TYPE,**dmgType)
        self.weaponClass = co_attribs.AttribEnum(possibleVals=coe.WEAPON_CLASSES,
                **weaponClass)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.attackType==other.attackType)
                        and (self.dmgAffects==other.dmgAffects)
                        and (self.dmgApply==other.dmgApply)
                        and (self.dmgType==other.dmgType)
                        and (self.weaponClass==other.weaponClass))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.attackType.toString(1)
        res += self.dmgAffects.toString(1)
        res += self.dmgApply.toString(1)
        res += self.dmgType.toString(1)
        res += self.weaponClass.toString(1)
        return indentText(res,indention)

class Weapon(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,weaponType,dmgEnums,dmgFront,dmgBack,dmgLeft,dmgRight,
            range,cooldown,canAttackFighter,syncTargeting,pointStagger,
            travelSpeed,duration,fireConstraint,weaponEff,**kwargs):
        super().__init__(**kwargs)
        self.weaponType = co_attribs.AttribEnum(possibleVals=WEAPON_TYPES,**weaponType)
        self.dmgEnums = DamageEnums(**dmgEnums)
        self.dmgFront = co_attribs.AttribNum(**dmgFront)
        self.dmgBack = co_attribs.AttribNum(**dmgBack)
        self.dmgLeft = co_attribs.AttribNum(**dmgLeft)
        self.dmgRight = co_attribs.AttribNum(**dmgRight)
        self.range = co_attribs.AttribNum(**range)
        self.cooldown = co_attribs.AttribNum(**cooldown)
        self.canAttackFighter = co_attribs.AttribBool(**canAttackFighter)
        self.syncTargeting = co_attribs.AttribBool(**syncTargeting)
        self.pointStagger = co_attribs.AttribNum(**pointStagger)
        self.travelSpeed = co_attribs.AttribNum(**travelSpeed)
        self.duration = co_attribs.AttribNum(**duration)
        self.fireConstraint = FireConstraint.factory(**fireConstraint)
        self.weaponEff = SharedWeaponEffect.factory(**weaponEff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.weaponType==other.weaponType)
                        and (self.dmgEnums==other.dmgEnums)
                        and (self.dmgFront==other.dmgFront)
                        and (self.dmgBack==other.dmgBack)
                        and (self.dmgLeft==other.dmgLeft)
                        and (self.dmgRight==other.dmgRight)
                        and (self.range==other.range)
                        and (self.cooldown==other.cooldown)
                        and (self.canAttackFighter==other.canAttackFighter)
                        and (self.syncTargeting==other.syncTargeting)
                        and (self.pointStagger==other.pointStagger)
                        and (self.travelSpeed==other.travelSpeed)
                        and (self.duration==other.duration)
                        and (self.fireConstraint==other.fireConstraint)
                        and (self.weaponEff==other.weaponEff))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.weaponType.toString(1)
        res += self.dmgEnums.toString(1)
        res += self.dmgFront.toString(1)
        res += self.dmgBack.toString(1)
        res += self.dmgLeft.toString(1)
        res += self.dmgRight.toString(1)
        res += self.range.toString(1)
        res += self.cooldown.toString(1)
        res += self.canAttackFighter.toString(1)
        res += self.syncTargeting.toString(1)
        res += self.pointStagger.toString(1)
        res += self.travelSpeed.toString(1)
        res += self.duration.toString(1)
        res += self.fireConstraint.toString(1)
        res += self.weaponEff.toString(1)
        return indentText(res,indention)

class FiringAlignmentDefault(co_attribs.Attribute):
    def __init__(self,alignmentType,**kwargs):
        kwargs["identifier"] = ""
        super().__init__(**kwargs)
        self.alignmentType = co_attribs.AttribEnum(possibleVals=["Default",
            "TiltForward"],**alignmentType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.alignmentType==other.alignmentType)

    @classmethod
    def factory(cls,alignmentType,**kwargs):
        t = alignmentType["val"]
        if t=="Default":
            return FiringAlignmentDefault(alignmentType=alignmentType,**kwargs)
        elif t=="TiltForward":
            return TiltingFiringAlignment(alignmentType=alignmentType,**kwargs)
        else:
            raise RuntimeError("Invalid fire alignment type "+t)

    def toString(self,indention):
        res = super().toString(0)
        res += self.alignmentType.toString(0)
        return indentText(res,indention)

class TiltingFiringAlignment(FiringAlignmentDefault):
    def __init__(self,tiltAngle,**kwargs):
        super().__init__(**kwargs)
        self.tiltAngle = co_attribs.AttribNum(**tiltAngle)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.tiltAngle==other.tiltAngle)

    def toString(self,indention):
        res = super().toString(0)
        res += self.tiltAngle.toString(0)
        return indentText(res,indention)

class Squadron(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,squadDef,antimatter,**kwargs):
        kwargs["identifier"] = ""
        super().__init__(**kwargs)
        self.squadDef = coe.EntityRef(types=["Squad"],**squadDef)
        self.antimatter = co_attribs.AttribNum(**antimatter)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.squadDef==other.squadDef)
                        and (self.antimatter==other.antimatter))

    def toString(self,indention):
        res = super().toString(0)
        res += self.squadDef.toString(0)
        res += self.antimatter.toString(0)
        return indentText(res,indention)

"""############################### Properties ##############################"""

class HasAttackBehavior(coe.MainViewVisible,coe.UIVisible):
    def __init__(self,autoattackRange,autoattackOn,focusFire,usesFighterAttack,
            **kwargs):
        super().__init__(**kwargs)
        self.autoattackRange = co_attribs.AttribEnum(possibleVals=AUTOATTACK_RANGES,
                **autoattackRange)
        self.autoattackOn = co_attribs.AttribBool(**autoattackOn)
        self.focusFire = co_attribs.AttribBool(**focusFire)
        self.usesFighterAttack = co_attribs.AttribBool(**usesFighterAttack)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.autoattackRange==other.autoattackRange)
                        and (self.autoattackOn==other.autoattackOn)
                        and (self.focusFire==other.focusFire)
                        and (self.usesFighterAttack==other.usesFighterAttack))

class Constructable(hit.Shielded,hit.HasShieldVisuals):
    def __init__(self,price,formationRank,meshDecreasedEffect,
            meshIncreasedEffect,meshes,prereqs,statcount,buildTime,**kwargs):
        super().__init__(**kwargs)
        self.price = coe.Cost(**price)
        self.formationRank = co_attribs.AttribNum(**formationRank)
        self.meshDecreasedEffect = mesh.MeshRef(**meshDecreasedEffect)
        self.meshIncreasedEffect = mesh.MeshRef(**meshIncreasedEffect)
        self.meshes = co_attribs.AttribList(elemType=MeshInfoConstruct,**meshes)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)
        self.statcount = co_attribs.AttribEnum(possibleVals=STATCOUNT_TYPES,**statcount)
        self.buildTime = co_attribs.AttribNum(**buildTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.price==other.price)
                        and (self.formationRank==other.formationRank)
                        and (
                        self.meshDecreasedEffect==other.meshDecreasedEffect)
                        and (
                        self.meshIncreasedEffect==other.meshIncreasedEffect)
                        and (self.meshes==other.meshes)
                        and (self.prereqs==other.prereqs)
                        and (self.statcount==other.statcount)
                        and (self.buildTime==other.buildTime))

class Armed(hit.Hitable,HasAttackBehavior):
    def __init__(self,weapons,indexForRange,firingAlignment,targetsFront,
            targetsBack,targetsLeft,targetsRight,onlyTargetStructures,
            **kwargs):
        super().__init__(**kwargs)
        self.weapons = co_attribs.AttribList(elemType=Weapon,**weapons)
        self.indexForRange = co_attribs.AttribNum(**indexForRange)
        self.firingAlignment = FiringAlignmentDefault.factory(**firingAlignment)
        self.targetsFront = co_attribs.AttribNum(**targetsFront)
        self.targetsBack = co_attribs.AttribNum(**targetsBack)
        self.targetsLeft = co_attribs.AttribNum(**targetsLeft)
        self.targetsRight = co_attribs.AttribNum(**targetsRight)
        self.onlyTargetStructures = co_attribs.AttribBool(**onlyTargetStructures)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.weapons==other.weapons)
                        and (self.indexForRange==other.indexForRange)
                        and (self.firingAlignment==other.firingAlignment)
                        and (self.targetsFront==other.targetsFront)
                        and (self.targetsBack==other.targetsBack)
                        and (self.targetsLeft==other.targetsLeft)
                        and (self.targetsRight==other.targetsRight)
                        and (
                        self.onlyTargetStructures==other.onlyTargetStructures))

    def toString(self,indention):
        res = self.weapons.toString(0)
        res += self.indexForRange.toString(0)
        res += self.firingAlignment.toString(0)
        res += self.targetsFront.toString(0)
        res += self.targetsBack.toString(0)
        res += self.targetsLeft.toString(0)
        res += self.targetsRight.toString(0)
        res += self.onlyTargetStructures.toString(0)
        return indentText(res,indention)

class CarriesFighters(Armed):
    def __init__(self,squads,commandPoints,**kwargs):
        super().__init__(**kwargs)
        self.squads = tuple(map(
                lambda vals:Squadron(**vals),
                squads))
        self.commandPoints = co_attribs.AttribNum.factory(commandPoints)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.squads==other.squads)
                        and (self.commandPoints==other.commandPoints))

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for sq in self.squads:
            res.extend(sq.check(currMod,recursive))
        return res
