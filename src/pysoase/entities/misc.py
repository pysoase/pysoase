""" Contains parsers and classes for miscallenious objects like debris.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""################################# Types #################################"""

PIP_TYPES = ["EnemyShips","FriendlyShips","Modules","Planets"]
QUEST_TYPES = ["BombAnyPlanet","GiveResources","KillCapitalShips",
    "KillCivilianStructures","KillShips","KillTacticalStructures","SendEnvoy"]
QUEST_TARGET_TYPES = ["TargetCivilianSlots","TargetCreditIncome",
    "TargetCrystalIncome","TargetDist","TargetHappiness","TargetMetalIncome",
    "TargetShipSlots","TargetTacticalSlots"]

"""################################ Parsers ################################"""

"""### Basic Elements ###"""
"""### Debris Entity ###"""

g_DebrisEntity = (
    coe.genEntityHeader("Debris")
    +co_parse.genDecimalAttrib("averageLifetime")("lifetime")
    +co_parse.genDecimalAttrib("lifetimeVariancePerc")("lifetimeVariance")
    +co_parse.genDecimalAttrib("minLinearSpeed")("speedLinMin")
    +co_parse.genDecimalAttrib("maxLinearSpeed")("speedLinMax")
    +co_parse.genDecimalAttrib("minAngularSpeed")("speedAngMin")
    +co_parse.genDecimalAttrib("maxAngularSpeed")("speedAngMax")
    +co_parse.genDecimalAttrib("dragRate")("dragRate")
    +coe.g_shadows
).ignore(pp.dblSlashComment)

"""### Quest Entity ###"""

g_qReward = pp.Group(
        co_parse.genHeading("reward")
        +coe.g_cost
)("reward")
g_qPriceDemand = pp.Group(
        co_parse.genHeading("priceDemanded")
        +coe.g_cost
)("demand")
g_qConstraint = (
    co_parse.genEnumAttrib("type",QUEST_TARGET_TYPES)("constType")
    +co_parse.genDecimalAttrib("min")("constMin")
    +co_parse.genDecimalAttrib("max")("constMax")
)
g_qReceiverConstraint = pp.Group(
        co_parse.genHeading("ReceiverConstraint")
        +g_qConstraint
)
g_qTargetConstraint = pp.Group(
        co_parse.genHeading("TargetConstraint")
        +g_qConstraint
)
g_QuestEntity = (
    coe.genEntityHeader("Quest")
    +co_parse.genStringAttrib("titleStringId")("titleString")
    +co_parse.genStringAttrib("infoCardDescStringId")("descString")
    +co_parse.genEnumAttrib("questType",QUEST_TYPES)("questType")
    +co_parse.genDecimalAttrib("duration")("duration")
    +co_parse.genDecimalAttrib("relationshipDecayRate")("relDecayRate")
    +co_parse.genDecimalAttrib("happinessChangeForSuccess")("happinessSuccess")
    +co_parse.genDecimalAttrib("happinessChangeForFailure")("happinessFailure")
    +g_qReward
    +g_qPriceDemand
    +co_parse.genDecimalAttrib("valueToKill")("valueKill")
    +co_parse.genListAttrib("numReceiverConstraints",g_qReceiverConstraint)(
            "constraintsReceiver")
    +co_parse.genListAttrib("numTargetConstraints",g_qTargetConstraint)(
            "constraintsTarget")
    +co_parse.genDecimalAttrib("weight")("weight")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Pip Cloud Entity ###"""

g_PipCloudEntity = (
    coe.genEntityHeader("PipCloud")
    +co_parse.genEnumAttrib("pipCloudType",PIP_TYPES)("pipType")
    +co_parse.genStringAttrib("empireWindowPipIcon")("empireWindowIcon")
    +pp.Group(
            co_parse.g_linestart
            +pp.Keyword("mainViewIconAreaOffsetFromCenter")("identifier")
            +co_parse.g_space
            +co_parse.g_pos4("coord")
            +co_parse.g_eol
    )("mainIconOffset")
    +co_parse.genStringAttrib("mainViewOverlay")("mainOverlay")
    +co_parse.genStringAttrib("mainViewOverlayUnderCursor")("overlayUnderCursor")
    +co_parse.genIntAttrib("mainViewIconMaxEntityCount")("maxEntityCount")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Cannon Shell Entity ###"""

g_CannonShellEntity = (
    coe.genEntityHeader("CannonShell")
    +co_parse.genDecimalAttrib("acceleration")("accel")
    +co_parse.genDecimalAttrib("startingSpeed")("startSpeed")
    +co_parse.genDecimalAttrib("maxSpeed")("maxSpeed")
    +co_parse.genStringAttrib("buffToApplyOnImpact")("buff")
    +co_parse.genStringAttrib("planetImpactEffectName")("impactEff")
    +co_parse.genListAttrib("soundCount",
            co_parse.genStringAttrib("sound"),
            "planetImpactSounds")("impactSounds")
    +co_parse.genDecimalAttrib("planetImpactCameraShakeMaxSize")(
            "cameraShakeMax")
    +co_parse.genDecimalAttrib("planetImpactCameraShakeStrength")(
            "cameraShakeStrength")
    +co_parse.genDecimalAttrib("planetImpactCameraShakeDuration")(
            "cameraShakeDuration")
    +co_parse.genStringAttrib("meshName")("mesh")
    +coe.g_mainViewIcon
    +coe.g_picture
    +coe.g_nameString
    +coe.g_descriptionStringShort
    +co_parse.genBoolAttrib("renderMesh")("renderMesh")
    +coe.g_zoomDist
    +coe.g_uiIcons
    +coe.g_shadows
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################## Attributes ###############################"""

class QuestConstraint(co_attribs.Attribute):
    def __init__(self,constType,constMin,constMax,**kwargs):
        super().__init__(**kwargs)
        self.constType = co_attribs.AttribEnum(possibleVals=QUEST_TARGET_TYPES,
                **constType)
        self.constMin = co_attribs.AttribNum(**constMin)
        self.constMax = co_attribs.AttribNum(**constMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.constType==other.constType)
                        and (self.constMin==other.constMin)
                        and (self.constMax==other.constMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.constType.toString(1)
        res += self.constMin.toString(1)
        res += self.constMax.toString(1)
        return indentText(res,indention)

"""############################ Entity Classes #############################"""

class Debris(coe.Entity):
    parser = g_DebrisEntity

    def __init__(self,lifetime,lifetimeVariance,speedLinMin,speedLinMax,
            speedAngMin,speedAngMax,dragRate,shadowMin,shadowMax,**kwargs):
        super().__init__(**kwargs)
        self.lifetime = co_attribs.AttribNum(**lifetime)
        self.lifetimeVariance = co_attribs.AttribNum(**lifetimeVariance)
        self.speedLinMin = co_attribs.AttribNum(**speedLinMin)
        self.speedLinMax = co_attribs.AttribNum(**speedLinMax)
        self.speedAngMin = co_attribs.AttribNum(**speedAngMin)
        self.speedAngMax = co_attribs.AttribNum(**speedAngMax)
        self.dragRate = co_attribs.AttribNum(**dragRate)
        self.shadowMin = co_attribs.AttribNum(**shadowMin)
        self.shadowMax = co_attribs.AttribNum(**shadowMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.lifetime==other.lifetime)
                        and (self.lifetimeVariance==other.lifetimeVariance)
                        and (self.speedLinMin==other.speedLinMin)
                        and (self.speedLinMax==other.speedLinMax)
                        and (self.speedAngMin==other.speedAngMin)
                        and (self.speedAngMax==other.speedAngMax)
                        and (self.dragRate==other.dragRate)
                        and (self.shadowMin==other.shadowMin)
                        and (self.shadowMax==other.shadowMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.lifetime.toString(0)
        res += self.lifetimeVariance.toString(0)
        res += self.speedLinMin.toString(0)
        res += self.speedLinMax.toString(0)
        res += self.speedAngMin.toString(0)
        res += self.speedAngMax.toString(0)
        res += self.dragRate.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)

class PipCloud(coe.Entity):
    parser = g_PipCloudEntity

    def __init__(self,pipType,empireWindowIcon,mainIconOffset,mainOverlay,
            overlayUnderCursor,maxEntityCount,**kwargs):
        super().__init__(**kwargs)
        self.pipType = co_attribs.AttribEnum(possibleVals=PIP_TYPES,**pipType)
        self.empireWindowIcon = ui.BrushRef(**empireWindowIcon)
        self.mainIconOffset = co_attribs.AttribPos4d(**mainIconOffset)
        self.mainOverlay = ui.BrushRef(**mainOverlay)
        self.overlayUnderCursor = ui.BrushRef(**overlayUnderCursor)
        self.maxEntityCount = co_attribs.AttribNum(**maxEntityCount)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.pipType==other.pipType)
                        and (self.empireWindowIcon==other.empireWindowIcon)
                        and (self.mainIconOffset==other.mainIconOffset)
                        and (self.mainOverlay==other.mainOverlay)
                        and (self.overlayUnderCursor==other.overlayUnderCursor)
                        and (self.maxEntityCount==other.maxEntityCount))

    def toString(self,indention):
        res = super().toString(0)
        res += self.pipType.toString(0)
        res += self.empireWindowIcon.toString(0)
        res += self.mainIconOffset.toString(0)
        res += self.mainOverlay.toString(0)
        res += self.overlayUnderCursor.toString(0)
        res += self.maxEntityCount.toString(0)
        return indentText(res,indention)

class CannonShell(coe.MainViewSelectable,coe.Entity):
    parser = g_CannonShellEntity

    def __init__(self,accel,startSpeed,maxSpeed,buff,impactEff,impactSounds,
            cameraShakeMax,cameraShakeStrength,cameraShakeDuration,mesh,
            renderMesh,**kwargs):
        super().__init__(**kwargs)
        self.accel = co_attribs.AttribNum(**accel)
        self.startSpeed = co_attribs.AttribNum(**startSpeed)
        self.maxSpeed = co_attribs.AttribNum(**maxSpeed)
        self.buff = coe.EntityRef(types=["Buff"],**buff)
        self.impactEff = par.ParticleRef(**impactEff)
        self.impactSounds = co_attribs.AttribList(elemType=audio.SoundRef,
                **impactSounds)
        self.cameraShakeMax = co_attribs.AttribNum(**cameraShakeMax)
        self.cameraShakeStrength = co_attribs.AttribNum(**cameraShakeStrength)
        self.cameraShakeDuration = co_attribs.AttribNum(**cameraShakeDuration)
        self.mesh = meshes.MeshRef(**mesh)
        self.renderMesh = co_attribs.AttribBool(**renderMesh)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.accel==other.accel)
                        and (self.startSpeed==other.startSpeed)
                        and (self.maxSpeed==other.maxSpeed)
                        and (self.buff==other.buff)
                        and (self.impactEff==other.impactEff)
                        and (self.impactSounds==other.impactSounds)
                        and (self.cameraShakeMax==other.cameraShakeMax)
                        and (
                        self.cameraShakeStrength==other.cameraShakeStrength)
                        and (
                        self.cameraShakeDuration==other.cameraShakeDuration)
                        and (self.mesh==other.mesh)
                        and (self.renderMesh==other.renderMesh))

    def toString(self,indention):
        res = super().toString(0)
        res += self.accel.toString(0)
        res += self.startSpeed.toString(0)
        res += self.maxSpeed.toString(0)
        res += self.buff.toString(0)
        res += self.impactEff.toString(0)
        res += self.impactSounds.toString(0)
        res += self.cameraShakeMax.toString(0)
        res += self.cameraShakeStrength.toString(0)
        res += self.cameraShakeDuration.toString(0)
        res += self.mesh.toString(0)
        res += self.iconMainView.toString(0)
        res += self.pic.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.renderMesh.toString(0)
        res += self.zoomDist.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)

class Quest(coe.Entity,co_basics.HasReference):
    parser = g_QuestEntity

    def __init__(self,titleString,descString,questType,duration,relDecayRate,
            happinessSuccess,happinessFailure,reward,demand,valueKill,
            constraintsReceiver,constraintsTarget,weight,**kwargs):
        super().__init__(**kwargs)
        self.titleString = ui.StringReference(**titleString)
        self.descString = ui.StringReference(**descString)
        self.questType = co_attribs.AttribEnum(possibleVals=QUEST_TYPES,**questType)
        self.duration = co_attribs.AttribNum(**duration)
        self.relDecayRate = co_attribs.AttribNum(**relDecayRate)
        self.happinessSuccess = co_attribs.AttribNum(**happinessSuccess)
        self.happinessFailure = co_attribs.AttribNum(**happinessFailure)
        self.reward = coe.Cost(**reward)
        self.demand = coe.Cost(**demand)
        self.valueKill = co_attribs.AttribNum(**valueKill)
        self.constraintsReceiver = co_attribs.AttribList(elemType=QuestConstraint,
                **constraintsReceiver)
        self.constraintsTarget = co_attribs.AttribList(elemType=QuestConstraint,
                **constraintsTarget)
        self.weight = co_attribs.AttribNum(**weight)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.titleString==other.titleString)
                        and (self.descString==other.descString)
                        and (self.questType==other.questType)
                        and (self.duration==other.duration)
                        and (self.relDecayRate==other.relDecayRate)
                        and (self.happinessSuccess==other.happinessSuccess)
                        and (self.happinessFailure==other.happinessFailure)
                        and (self.reward==other.reward)
                        and (self.demand==other.demand)
                        and (self.valueKill==other.valueKill)
                        and (self.constraintsTarget==other.constraintsTarget)
                        and (
                        self.constraintsReceiver==other.constraintsReceiver)
                        and (self.weight==other.weight))

    def toString(self,indention):
        res = super().toString(0)
        res += self.titleString.toString(0)
        res += self.descString.toString(0)
        res += self.questType.toString(0)
        res += self.duration.toString(0)
        res += self.relDecayRate.toString(0)
        res += self.happinessSuccess.toString(0)
        res += self.happinessFailure.toString(0)
        res += self.reward.toString(0)
        res += self.demand.toString(0)
        res += self.valueKill.toString(0)
        res += self.constraintsReceiver.toString(0)
        res += self.constraintsTarget.toString(0)
        res += self.weight.toString(0)
        return indentText(res,indention)
