"""Classes, attributes and parsers for Buffs and Abilities 
"""

import collections
import copy
import itertools as it
import os

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.mod.audio as audio
import pysoase.mod.constants as consts
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""################################ Types ##################################"""

ACTION_OVER_TIME_DO_DMG = ["DoDamage","DoDamageToTarget"]
ACTION_OVER_TIME_DMG_PLANET = ["DoDamageToPlanet","DoPercOfMaxDamageToPlanet"]
ACTION_OVER_TIME_SIMPLE = ["DoDamageAndGiveResourceCostPerc",
    "DoModuleConstruction","DrainAntiMatter","EarnResources",
    "RestoreAllegiance","RestoreAntiMatter","RestoreHull",
    "RestorePlanetHealth","RestoreShields","StealPlanetProduction"]
ACTION_OVER_TIME_SIMPLE.extend(ACTION_OVER_TIME_DMG_PLANET)
ACTION_OVER_TIME = copy.copy(ACTION_OVER_TIME_DO_DMG)
ACTION_OVER_TIME.extend(ACTION_OVER_TIME_DMG_PLANET)
ACTION_OVER_TIME.extend(ACTION_OVER_TIME_SIMPLE)
ACTION_OVER_TIME.extend(["Magnetize","DrainAntiMatterAndDoDamage"])
AI_USE_TARGET_BUFF = ["HasBuff"]
AI_USE_TARGET_SIMPLE = ["Any","HasAbilityInCooldown","HasNoWeapons","Invalid",
    "IsBoarded","IsCapitalShipOrStarbase","IsCapitalShipOrStarbaseOrTitan",
    "IsDifferentRoleType","IsNotWeaponDisabled"]
AI_USE_TARGET_VAL = ["AntimatterExceedsAmount","HullDamageExceedsAmount",
    "ShieldDamageExceedsAmount"]
AI_USE_TARGET = copy.copy(AI_USE_TARGET_VAL)
AI_USE_TARGET.extend(AI_USE_TARGET_SIMPLE)
AI_USE_TARGET.extend(AI_USE_TARGET_BUFF)
AI_USE_TIME_DMG_HULL = ["OnlyWhenHullDamageExceeds",
    "OnlyWhenManyTargetsHaveHullDamage"]
AI_USE_TIME_MANY_TARGETS_BANK_RANGE = ["OnlyWhenManyTargetsInRangeOfBank"]
AI_USE_TIME_SIMPLE = ["Anytime","Invalid","NoEnemyShipsInGravityWell",
    "NotInCombat","OnlyIfOwnTitan","OnlyInCombat","OnlyWhenAboutToDie",
    "OnlyWhenAttackTargetIsInFiringSolution",
    "OnlyWhenAttackTargetIsInFiringSolutionAndHasAttackOrder",
    "OnlyWhenBombing","OnlyWhenDisabled","OnlyWhenEnemyFleetWillPrevail",
    "OnlyWhenManyTargetsHaveShieldDamage",
    "OnlyWhenManyTargetsHyperspacingTowardsMyGravityWell",
    "OnlyWhenMovingFar","OnlyWhenOrbitingEnemyOrAlliedPlanet",
    "OnlyWhenOrbitingEnemyPlanet","OnlyWhenTakingDamage","OnlyWithNoOrder",
    "OnlyWithNoOrderOrAutoAttackButNotIfTargettingMine"]
AI_USE_TIME_TARGETS_NOT_BUFFED = ["OnlyWhenManyTargetsInCombatDoNotHaveBuff"]
AI_USE_TIME_VAL = copy.copy(AI_USE_TIME_DMG_HULL)
AI_USE_TIME_VAL.extend(["OnlyWhenDebrisWithinRange",
    "OnlyWhenManyTargetsInRange","OnlyWhenMovingThresholdDistance"])
AI_USE_TIME = copy.copy(AI_USE_TIME_SIMPLE)
AI_USE_TIME.extend(AI_USE_TIME_DMG_HULL)
AI_USE_TIME.extend(AI_USE_TIME_MANY_TARGETS_BANK_RANGE)
AI_USE_TIME.extend(AI_USE_TIME_TARGETS_NOT_BUFFED)
AI_USE_TIME.extend(AI_USE_TIME_VAL)
BUFF_ENTITY_MODIFIERS = ["AbilityCooldownRate","AngularThrust",
    "AntimatterCostOfNonUltimates","AntimatterRestoration",
    "AntimatterRestoreRate","ArmorAsDamageTarget","BonusMaxSquads",
    "ChanceToBeNotOptimallyTargetted","ChanceToHitAsDamageDealer",
    "ChanceToHitAsDamageTarget","ChanceToIgnoreShieldsAsDamageDealer",
    "ChanceToIgnoreShieldsAsDamageTarget","CultureSpread",
    "CultureSpreadModifier","CultureSpreadRateForOwner",
    "DamageAsDamageDealer","DamageAsDamageTarget",
    "DamageAsDamageTargetFromForward","DamageSharing","EnemyCultureSpread",
    "EnergyWeaponCooldown","FightersPerSquadron",
    "GravityWellRangeForHyperspace","HullPointRestoration",
    "HullPointRestoreRate","HyperspaceChargeUpRate","HyperspaceSpeed",
    "LinearMaxSpeed","LinearThrust","MaxHullPoints","MaxShieldPoints",
    "PhysicalDamageAsDamageDealer","PhysicalDamageAsDamageTarget",
    "PlanetBombingCooldown","PlanetBombingDamageAbsorption",
    "PlanetHealthMaxAdjustment","PlanetMaxPopulation",
    "PlanetModuleBuildTime","PlanetPopulationGrowthRate",
    "PlanetProductionStolen","PlanetResourceOutput","PlanetTaxRate",
    "PlanetUpgradeBuildRateAdjustment","PlanetUpgradeCost",
    "PropagateWeaponDamagePerc","RefineryIncome","ResourceExtractionRate",
    "ShieldMitigation","ShieldPointRestoration","ShieldPointRestoreRate",
    "ShipBuildTime","TradeIncome","UnderConstructionRate","WeaponCooldown",
    "WeaponRange"]
BUFF_ENTITY_MODIFIERS_BOOL = ["ActivelyBeingConstructed","Boarded",
    "CanBeCaptured","CancelCannotBeDamaged","CancelPhaseOut",
    "CannotBeColonized","CannotBeDamaged","CannotBeScuttled",
    "DetectIncomingHyperspacingEntities","DisableAbilities",
    "DisableAbilitiesUltimate","DisableAngularEngines",
    "DisableConstruction","DisableFighterLaunch","DisableLinearEngines",
    "DisableModuleFunctionality","DisablePhaseJump","DisableRegeneration",
    "DisableWeapons","DisableWeaponsUltimate",
    "ForceIsDamagedOverlayInHyperspace","HasDestabilizedHyperspaceEffect",
    "HasShieldMeshEffect","HasTauntTarget","ImmuneToNonUltimateDisable",
    "InstantBuildFighters","IsFlagship","IsIllusionShip",
    "IsInNonFriendlyGravityWell","IsInvisible","IsPhaseGateEndPoint",
    "IsResourceFocusActive","JumpBlockerImmune","NextAbilityAmplified",
    "PhaseDodge","PhaseOut","ProxySensor","WeaponsDealNoDamage"]
BUFF_EXCLUSIVITY = ["ExclusivePerPlayer","ExclusiveForAllPlayers",
    "NotExclusive"]
COST_TYPE_AM = ["AntiMatter","AntiMatterAndNotBeAtStar"]
COST_TYPE_AM_HULL = ["HullAndAntimatter"]
COST_TYPE_NONE = ["None"]
COST_TYPE_PASSIVE = ["Passive"]
COST_TYPE_RESOURCES = ["Resources",
    "ResourcesAndMustHaveTitan"]
COST_TYPE_RESOURCE_SUPPLY = ["ResourcesAndMustHaveShipSlots"]
COST_TYPE_RESOURCE_CAPITAL = ["ResourcesAndMustHaveCapitalCrew"]
COST_TYPES = copy.copy(COST_TYPE_AM)
COST_TYPES.extend(COST_TYPE_AM_HULL)
COST_TYPES.extend(COST_TYPE_NONE)
COST_TYPES.extend(COST_TYPE_PASSIVE)
COST_TYPES.extend(COST_TYPE_RESOURCES)
COST_TYPES.extend(COST_TYPE_RESOURCE_SUPPLY)
COST_TYPES.extend(COST_TYPE_RESOURCE_CAPITAL)
EFFECT_ATTACH_TYPES_SIMPLE = ["Invalid","Center","Aura","Above"]
EFFECT_ATTACH_TYPES = copy.copy(EFFECT_ATTACH_TYPES_SIMPLE)
EFFECT_ATTACH_TYPES.append("Ability")
FINISH_COND_NO_LONGER_BUFFED = ["FirstSpawnerNoLongerHasBuff",
    "LastSpawnerNoLongerHasBuff","OwnerNoLongerHasBuff"]
FINISH_COND_SIMPLE = ["AbilityUsed","AllOnDelayInstantActionsDone",
    "AllPeriodicActionsDone","AntiMatterDepleted",
    "AnyOnConditionInstantActionDone","EnterHyperspace","ExitHyperspace",
    "Invalid","NoTarget","OutOfRange","OwnerChanged",
    "OwnerIsFriendlyToFirstSpawner","ShieldFailure",
    "TargetConstructionCannotBeUpdated","TargetIsConstructed",
    "TargetNoLongerActivelyBeingConstructed","TargetOwnerIsFriendly",
    "TargetOwnerIsHostile","TargetOwnerIsNeutral"]
FINISH_COND_NUMERIC = ["FightersKilled","OwnerHullAbovePerc","TimeElapsed"]
FINISH_CONDITIONS = copy.copy(FINISH_COND_SIMPLE)
FINISH_CONDITIONS.extend(FINISH_COND_NO_LONGER_BUFFED)
FINISH_CONDITIONS.extend(FINISH_COND_NUMERIC)
FINISH_CONDITIONS.extend(["DamageTaken","ResearchNotMet",])
INSTANT_ACTION_APPLY_BUFF = ["ApplyBuffToFirstSpawnerNoFilterNoRange",
    "ApplyBuffToTargetNoFilterNoRange","RemoveBuffOfType",
    "ApplyBuffToLastSpawnerNoFilterNoRange"]
INSTANT_ACTION_APPLY_CONE = ["ApplyBuffToTargetsInDirectionalCone"]
INSTANT_ACTION_APPLY_EFF_LVL = ["ApplyArbitraryTargettedBuffToSelf"]
INSTANT_ACTION_APPLY_EFFECT = ["ApplyBuffToSelf",
    "ApplyTargettedBuffToSelfNoFilterNoRange"]
INSTANT_ACTION_APPLY_EFFECT_FILTER = ["ApplyBuffToTargetsLinked",
    "ApplyTargettedBuffToSelfNoRange","ApplyBuffToIncomingHyperspacers",
    "ApplyBuffToLocalOrbitBody","ApplyBuffToTargetNoRange"]
INSTANT_ACTION_APPLY_ENTRY_VEHICLE = ["ApplyBuffToTargetWithEntryVehicles"]
INSTANT_ACTION_APPLY_FILTER = ["ApplyBuffToSelfWithFilter",
    "ApplyBuffToTargetOnWeaponFired"]
INSTANT_ACTION_APPLY_OR_REMOVE = ["ApplyOrRemoveBuffToSelf"]
INSTANT_ACTION_APPLY_TARGET_EFF = ["ApplyBuffToTargetsAtAdjacentOrbitBodies",
    "ApplyBuffToTargetsAtOrbitBody","ApplyBuffToTargetsInColumn",
    "ApplyBuffToTargetsInRadius","ApplyTargettedBuffToSelf",
    "ApplyBuffToTarget"]
INSTANT_ACTION_APPLY_TARGET_RADIUS_TRAVEL = [
    "ApplyBuffToTargetsInRadiusWithTravel"]
INSTANT_ACTION_APPLY_TARGET_WEAP = ["ApplyBuffToLocalOrbitBodyWithTravel",
    "ApplyBuffToTargetWithTravel",
    "ApplyBuffToTargetsInRadiusOfTargetWithTravel",
    "ApplyBuffToTargetsInRadiusWithChainTravel"]
INSTANT_ACTION_APPLY_WEAP = ["ApplyBuffToLastSpawnerWithTravelNoFilterNoRange",
    "ApplyBuffToTargetWithTravelNoFilterNoRange"]
INSTANT_ACTION_CHANGE_PLAYER = ["ChangePlayerIndexToFirstSpawner"]
INSTANT_ACTION_COLONIZE_PLANET = ["ColonizePlanet"]
INSTANT_ACTION_COND_BUFF = ["IfFirstSpawnerDoesNotHaveBuff",
    "IfFirstSpawnerHasBuff","IfOwnerDoesNotHaveBuff","IfOwnerHasBuff"]
INSTANT_ACTION_COND_SIMPLE = ["IfOwnerDoesNotHaveDestabilizationProtection",
    "IfOwnerHasDestabilizationProtection",
    "IfOwnerIsAboutToDie","IfOwnerIsFirstPlayerOwnedStarbaseAtOrbitbody",
    "IfOwnerIsNotFirstPlayerOwnedStarbaseAtOrbitbody"]
INSTANT_ACTION_CONDITIONS = copy.copy(INSTANT_ACTION_COND_BUFF)
INSTANT_ACTION_CONDITIONS.append("IfOwnerHasHullLessThanPerc")
INSTANT_ACTION_CONDITIONS.extend(INSTANT_ACTION_COND_SIMPLE)
INSTANT_ACTION_CONVERT_DMG_AM = ["ConvertDamageToAntiMatter"]
INSTANT_ACTION_CREATE_FRIGATE = ["CreateFrigate"]
INSTANT_ACTION_CREATE_FRIGATE_CLONE = ["CreateClonedFrigate"]
INSTANT_ACTION_CREATE_FRIGATE_TARGET = ["CreateFrigateAtTarget"]
INSTANT_ACTION_CREATE_ILLUSION_FIGHTERS = ["CreateIllusionFighters"]
INSTANT_ACTION_CREATE_SHELL = ["CreateCannonShell"]
INSTANT_ACTION_CREATE_STARBASE = ["CreateStarBase"]
INSTANT_ACTION_CREATE_UNIT = ["CreatePlanetModule","CreateSpaceMine",
    "CreateSquad"]
INSTANT_ACTION_DEBRIS_HULL = ["ConvertNearbyDebrisToHull"]
INSTANT_ACTION_DMG_ENTITY = ["DoDamagePerEntityInRadius"]
INSTANT_ACTION_DO_DMG = ["DoDamage","DoDamagePerLastSpawnerPopulation",
    "DoDamagePercOfCurrentHull"]
INSTANT_ACTION_EFFECT = ["PlayPersistantAttachedEffect","PlayAttachedEffect"]
INSTANT_ACTION_NUM = ["AddPopulationToPlanet","AttractDebris",
    "ConvertFrigateToResources","DoAllegianceChangeToPlanet",
    "DoDamageToPlanet",
    "RemoveAntiMatter","RemoveAntiMatterPerc","RestoreAntiMatter",
    "RestoreHullPoints","RestoreHullPointsPerc","RestoreShieldPoints",
    "RestoreShieldPointsPerc","RetaliateBounty","RetaliateDamage",
    "SpawnResourceExtractors","StealAntiMatterForFirstSpawner",
    "ApplyRelationshipModifierToOwnerOfPlanetInCurrentGravityWell",
    "ApplyForceFromSpawner","ApplyImpulseFromSpawner",
    "ApplyImpulseInSpawnerDirection","GiveExperience",
    "IncreaseOwnerAbilityLevel","InitializeMovementTowardLastSpawner",
    "InitializeRandomMotion","TeleportTowardsMoveTarget",
    "TeleportTowardsTarget","TiltUpVectorInTargetDirection"]
INSTANT_ACTION_NUN_EFFECT = ["PlayDetachedEffectsInRadius",
    "GiveCreditsToPlayer"]
INSTANT_ACTION_PROPAGATE_DMG = [
    "PropagateWeaponDamageReceivedToTargetsInRadius"]
INSTANT_ACTION_SIMPLE = ["ChangePlayerIndexToNeutral","ClearRecordedDamage",
    "DoInterrupt","DoInterruptUltimate","Explore",
    "ForceAttackersToRepickAttackTarget","GainAntimatterEqualToTarget",
    "GainHullEqualToTarget","GainShieldEqualToTarget","Invalid","MakeDead",
    "MatchTargetVelocity","RecordDamage","ResetPhysicsState",
    "ResurrectCapitalShip","SetTauntTargetToLastSpawner",
    "TeleportTowardsArbitraryTarget"]
INSTANT_ACTION_SPAWN_SHIPS = ["SpawnShipsAtPlanet"]
INSTANT_ACTION_STEAL_RESOURCES = ["StealResources"]
INSTANT_ACTION_SQUAD_MINES = ["ConvertSquadMembersToMines"]
INSTANT_ACTION_TRIGGERS_SIMPLE = ["AlwaysPerform","OnAbilityUsed",
    "OnBuffFinish","OnDamageTaken","OnHyperspaceExit","OnOwnerDeath"]
INSTANT_ACTION_TRIGGERS = ["AlwaysPerform","OnAbilityUsed",
    "OnBuffFinish","OnDamageTaken","OnHyperspaceExit","OnOwnerDeath",
    "OnChance","OnCondition","OnDelay","OnWeaponFired"]
INSTANT_ACTION_TYPES = ["AddPopulationToPlanet",
    "ApplyArbitraryTargettedBuffToSelf",
    "ApplyBuffToFirstSpawnerNoFilterNoRange",
    "ApplyBuffToIncomingHyperspacers",
    "ApplyBuffToLastSpawnerNoFilterNoRange",
    "ApplyBuffToLastSpawnerWithTravelNoFilterNoRange",
    "ApplyBuffToLocalOrbitBody","ApplyBuffToLocalOrbitBodyWithTravel",
    "ApplyBuffToSelf","ApplyBuffToSelfWithFilter","ApplyBuffToTarget",
    "ApplyBuffToTargetNoFilterNoRange","ApplyBuffToTargetNoRange",
    "ApplyBuffToTargetOnWeaponFired","ApplyBuffToTargetWithEntryVehicles",
    "ApplyBuffToTargetWithTravel",
    "ApplyBuffToTargetWithTravelNoFilterNoRange",
    "ApplyBuffToTargetsAtAdjacentOrbitBodies",
    "ApplyBuffToTargetsAtOrbitBody",
    "ApplyBuffToTargetsInColumn","ApplyBuffToTargetsInDirectionalCone",
    "ApplyBuffToTargetsInRadius",
    "ApplyBuffToTargetsInRadiusOfTargetWithTravel",
    "ApplyBuffToTargetsInRadiusWithChainTravel",
    "ApplyBuffToTargetsInRadiusWithTravel",
    "ApplyBuffToTargetsLinked","ApplyForceFromSpawner",
    "ApplyImpulseFromSpawner","ApplyImpulseInSpawnerDirection",
    "ApplyOrRemoveBuffToSelf",
    "ApplyRelationshipModifierToOwnerOfPlanetInCurrentGravityWell",
    "ApplyTargettedBuffToSelf","ApplyTargettedBuffToSelfNoFilterNoRange",
    "ApplyTargettedBuffToSelfNoRange","AttractDebris",
    "ChangePlayerIndexToFirstSpawner","ChangePlayerIndexToNeutral",
    "ClearRecordedDamage","ColonizePlanet","ConvertDamageToAntiMatter",
    "ConvertFrigateToResources","ConvertNearbyDebrisToHull",
    "ConvertSquadMembersToMines","CreateCannonShell","CreateClonedFrigate",
    "CreateFrigate","CreateFrigateAtArbitraryTarget",
    "CreateFrigateAtTarget","CreateIllusionFighters","CreatePlanetModule",
    "CreateSpaceMine","CreateSquad","CreateStarBase",
    "DoAllegianceChangeToPlanet","DoDamage","DoDamagePerEntityInRadius",
    "DoDamagePerLastSpawnerPopulation","DoDamagePercOfCurrentHull",
    "DoDamageToPlanet","DoInterrupt","DoInterruptUltimate","Explore",
    "ForceAttackersToRepickAttackTarget","GainAntimatterEqualToTarget",
    "GainHullEqualToTarget","GainShieldEqualToTarget","GiveCreditsToPlayer",
    "GiveExperience",
    "IncreaseOwnerAbilityLevel","InitializeMovementTowardLastSpawner",
    "InitializeRandomMotion","MakeDead","MatchTargetVelocity",
    "PlayAttachedEffect","PlayDetachedEffectsInRadius",
    "PlayPersistantAttachedEffect","PlayPersistantBeamEffect",
    "PropagateWeaponDamageReceivedToTargetsInRadius","RecordDamage",
    "RemoveAntiMatter","RemoveAntiMatterPerc","RemoveBuffOfType",
    "ResetPhysicsState","RestoreAntiMatter","RestoreHullPoints",
    "RestoreHullPointsPerc","RestoreShieldPoints","RestoreShieldPointsPerc",
    "ResurrectCapitalShip","RetaliateBounty","RetaliateDamage","RuinPlanet",
    "SetTauntTargetToLastSpawner","SpawnResourceExtractors",
    "SpawnShipsAtPlanet","StealAntiMatterForFirstSpawner","StealResources",
    "TeleportTowardsArbitraryTarget","TeleportTowardsMoveTarget",
    "TeleportTowardsTarget","TiltUpVectorInTargetDirection"]
INSTANT_ACTION_WEAPON_EFFECT = ["PlayPersistantBeamEffect"]
LVL_SOURCE_INTRINSIC = ["Intrinsic"]
LVL_SOURCE_SIMPLE = ["FixedLevel0"]
LVL_SOURCE_RESEARCH_BASE = ["ResearchWithBase"]
LVL_SOURCE_RESEARCH_NO_BASE = ["ResearchStartsAt0","ResearchWithoutBase"]
LVL_SOURCE_UPGRADE = ["StarBaseUpgradeLevel","TitanUpgradeLevel"]
LVL_SOURCE = copy.copy(LVL_SOURCE_SIMPLE)
LVL_SOURCE.extend(LVL_SOURCE_INTRINSIC)
LVL_SOURCE.extend(LVL_SOURCE_RESEARCH_BASE)
LVL_SOURCE.extend(LVL_SOURCE_RESEARCH_NO_BASE)
LVL_SOURCE.extend(LVL_SOURCE_UPGRADE)
OBJECTS = ["CapitalShip","Corvette","EnvoyFrigate","Fighter","Frigate",
    "PlaceHolderPlanetModule","PlaceHolderSpaceMine","Planet",
    "PlanetModule","SpaceMine","Star","StarBase","Titan"]
ON_REAPPLY_DUPLICATE = ["Invalid","PrioritizeNewBuffs","PrioritizeOldBuffs"]
ORDER_ACKNOWLEDGE = ["ONATTACKORDERISSUED","ONCREATION","ONGENERALORDERISSUED",
    "ONSELECTED","ONSTARTPHASEJUMP","Invalid"]
OWNERSHIPS = ["Allied","AlliedOrEnemy","Enemy","Friendly","NoOwner","Player"]
RESOURCE_TYPES = ["Credits","Metal","Crystal"]
SOURCE_PROPERTIES = ["AbilityALevel","AbilityBLevel","AbilityCLevel",
    "AbilityDLevel","AbilityELevel"]
SPACE_TYPES = ["Normal","Phase"]
SPAWN_SHIPS_TYPE = ["FleetBeacon","PirateRaid"]
STACKING_LIMITS = ["PerPlayer","ForAllPlayers"]
TARGET_CONSTRAINTS = ["CanBeCaptured","CanBeJumpBlocked","CanHaveFighters",
    "CanHaveShields","CanPhaseDodge","CanSpreadWeaponDamage",
    "DoesNotHaveDestabilizationProtection","HasAbilityWithCooldown",
    "HasAntiMatterPool","HasAntiMatterShortage",
    "HasDestabilizationProtection","HasEnergyWeapons","HasHullDamage",
    "HasPhaseMissiles","HasPopulation","HasShieldDamage","HasWeapons",
    "Invalid","IsActivelyBeingConstructed","IsBoarded","IsCargoShip",
    "IsColonizable","IsControllable","IsExplored","IsInFriendlyGravityWell",
    "IsInNonFriendlyGravityWell","IsMovingToLocalOrbitBody",
    "IsNotStarBaseConstructor","IsPetOfTargetter","IsPsi",
    "IsResourceExtractor","IsRuinable","LinkedCargoShip","LinkedSquad",
    "ModuleUnderConstruction","NotActivelyBeingConstructed",
    "NotFlagship","NotInvulnerable","NotIsBoarded","NotIsHomeworld",
    "NotIsIllusionShip","NotIsOccupationPlanet","NotSelf",
    "WithinCurrentSolarSystem"]
WEAP_EFFECT_IMPACT_OFFSET = ["RandomMesh","Center",
    "CenterOffsetBySpatialRadius"]

"""################################# Parser ################################"""

"""### Basic Elements ###"""

g_buffType = co_parse.genStringAttrib("buffType")("buff")
g_dmgAffect = co_parse.genEnumAttrib("damageAffectType",armed.DMG_AFFECTS)(
        "dmgAffect")
g_dmgType = co_parse.genEnumAttrib("damageType",armed.DMG_TYPE)("dmgType")
g_impulse = co_parse.genDecimalAttrib("impulse")("impulse")
g_maxTargets = coe.genFixedLvlAttrib("maxTargetCount")("maxTargets")
g_targetFilter = pp.Group(
        co_parse.genHeading("targetFilter")
        +co_parse.genListAttrib("numOwnerships",
                co_parse.genEnumAttrib("ownership",OWNERSHIPS))("ownerships")
        +co_parse.genListAttrib("numObjects",
                co_parse.genEnumAttrib("object",OBJECTS))(
                "objects")
        +co_parse.genListAttrib("numSpaces",
                co_parse.genEnumAttrib("space",SPACE_TYPES))(
                "spaces")
        +co_parse.genListAttrib("numConstraints",
                co_parse.genEnumAttrib("constraint",
                TARGET_CONSTRAINTS))("constraints")
)("targetFilter")
g_travelSpeed = co_parse.genDecimalAttrib("travelSpeed")("travelSpeed")
g_range = coe.genFixedLvlAttrib("range")("range")

"""### Instant Action Trigger Conditions ###"""

g_actionConditionSimple = pp.Group(
        co_parse.genEnumAttrib("instantActionConditionType",
                INSTANT_ACTION_COND_SIMPLE)("condType")
)
g_actionConditionBuff = pp.Group(
        co_parse.genEnumAttrib("instantActionConditionType",
                INSTANT_ACTION_COND_BUFF)("condType")
        +co_parse.genStringAttrib("conditionBuffType")("buffType")
        +co_parse.genStringAttrib("conditionBuffShortName")("buffShortName")
)
g_actionConditionLvl = pp.Group(
        co_parse.genKeyValPair("instantActionConditionType",
                "IfOwnerHasHullLessThanPerc")("condType")
        +coe.genFixedLvlAttrib("hullPerc")("hullPerc")
)

"""### Instant Action Triggers ###"""

g_actionTriggerSimple = pp.Group(
        co_parse.genEnumAttrib("instantActionTriggerType",
                INSTANT_ACTION_TRIGGERS_SIMPLE)("triggerType")
)
g_actionTriggerChance = pp.Group(
        co_parse.genKeyValPair("instantActionTriggerType","OnChance")("triggerType")
        +coe.genFixedLvlAttrib("buffApplyChance")("applyChance")
)
g_actionTriggerCondition = pp.Group(
        co_parse.genKeyValPair("instantActionTriggerType","OnCondition")(
                "triggerType")
        +pp.Or([g_actionConditionBuff,g_actionConditionLvl,
            g_actionConditionSimple])("cond")
)
g_actionTriggerDelay = pp.Group(
        co_parse.genKeyValPair("instantActionTriggerType","OnDelay")(
                "triggerType")
        +co_parse.genDecimalAttrib("delayTime")("delay")
)
g_actionTriggerWeapFired = pp.Group(
        co_parse.genKeyValPair("instantActionTriggerType","OnWeaponFired")(
                "triggerType")
        +co_parse.genListAttrib("numWeaponClasses",
                co_parse.genEnumAttrib("weaponClassForWeaponPassive",
                        coe.WEAPON_CLASSES)
        )("weaponClasses")
        +coe.genFixedLvlAttrib("passiveWeaponEffectChance")(
                "passiveEffectChance")
)
g_actionTrigger = pp.Or([
    g_actionTriggerChance,
    g_actionTriggerCondition,
    g_actionTriggerDelay,
    g_actionTriggerSimple,
    g_actionTriggerWeapFired
])("trigger")

"""### Attach Effects ###"""
g_attachInfoSimple = (
    co_parse.genEnumAttrib("attachType",EFFECT_ATTACH_TYPES_SIMPLE)("attachType")
)
g_attachInfoAbility = (
    co_parse.genKeyValPair("attachType","Ability")("attachType")
    +co_parse.genIntAttrib("abilityIndex")("index")
)
g_effectAttach = pp.Group(
        co_parse.genHeading("effectAttachInfo")
        +pp.Or([g_attachInfoSimple,g_attachInfoAbility])
)("attachInfo")
g_weapAttach = pp.Group(
        co_parse.genHeading("weaponEffectAttachInfo")
        +pp.Or([g_attachInfoSimple,g_attachInfoAbility])
)("effectAttach")
g_effectInfoContent = (
    g_effectAttach
    +co_parse.genStringAttrib("smallEffectName")("effectSmall")
    +co_parse.genStringAttrib("mediumEffectName")("effectMedium")
    +co_parse.genStringAttrib("largeEffectName")("effectLarge")
    +co_parse.genStringAttrib("soundID")("sound")
)
g_effectInfo = pp.Group(
        co_parse.genHeading("effectInfo")
        +g_effectInfoContent
)("effectInfo")
g_effectInfoEntryVehicle = pp.Group(
        co_parse.genHeading("entryVehicleLaunchInfo")
        +g_effectInfoContent
)("effectInfo")

"""### Weapon Effects ###"""
g_weapEffectSpawnInfoRand = pp.Group(
        co_parse.genHeading("targetCentricEffectPointSpawnInfo")
        +co_parse.genKeyValPair("spawnType","CompletelyRandom")("spawnType")
        +co_parse.genDecimalAttrib("minRadius")("radiusMin")
        +co_parse.genDecimalAttrib("maxRadius")("radiusMax")
)("targetCentricEffect")
g_weapEffectSpawnInfoRandUp = pp.Group(
        co_parse.genHeading("targetCentricEffectPointSpawnInfo")
        +co_parse.genKeyValPair("spawnType","RandomFromUp")("spawnType")
        +co_parse.genDecimalAttrib("minRadius")("radiusMin")
        +co_parse.genDecimalAttrib("maxRadius")("radiusMax")
        +co_parse.genDecimalAttrib("angleVariance")("angleVar")
)("targetCentricEffect")
g_weapEffectOriginTarget = pp.Group(
        co_parse.genKeyValPair("weaponEffectOriginType","Target")("originType")
        +pp.Or([g_weapEffectSpawnInfoRand,g_weapEffectSpawnInfoRandUp])
)
g_weapEffectOriginTargetter = pp.Group(
        co_parse.genKeyValPair("weaponEffectOriginType","Targetter")("originType")
        +g_weapAttach
)
g_weapEffectOrigin = pp.Or([g_weapEffectOriginTarget,
    g_weapEffectOriginTargetter])("origin")
g_hasWeapEffectFalse = co_parse.genBoolAttrib("hasWeaponEffects",
        co_parse.g_false)(
        "hasWeapEffect")
g_weaponEffect = pp.Group(
        co_parse.genHeading("weaponEffectsDef")
        +pp.Or([armed.g_beamWeaponsEffect,armed.g_missileWeaponEffect,
            armed.g_projectileWeaponsEffect])
)("weaponEffect")
g_hasWeapEffectTrue = (
    co_parse.genBoolAttrib("hasWeaponEffects",
            co_parse.g_true)("hasWeapEffect")
    +g_weapEffectOrigin
    +co_parse.genEnumAttrib("weaponEffectImpactOffsetType",
            WEAP_EFFECT_IMPACT_OFFSET)("impactOffsetType")
    +co_parse.genBoolAttrib("canWeaponEffectHitHull")("effectHitsHull")
    +co_parse.genBoolAttrib("canWeaponEffectHitShields")("effectHitsShield")
    +g_weaponEffect
)
g_hasWeapEffect = pp.Or([g_hasWeapEffectTrue,g_hasWeapEffectFalse])

"""### Instant Actions ###"""

g_actionAddPop = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","AddPopulationToPlanet")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("populationAdded")
        )("vals")
)
g_actionAllegianceChange = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","DoAllegianceChangeToPlanet")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("allegianceChange")
        )("vals")
)
g_actionAM = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",["RemoveAntiMatter",
            "RestoreAntiMatter","StealAntiMatterForFirstSpawner"])(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("antiMatter")("antimatter")
        )("vals")
)
g_actionApplyBuff = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",[
            "ApplyBuffToFirstSpawnerNoFilterNoRange",
            "ApplyBuffToTargetNoFilterNoRange",
            "ApplyBuffToLastSpawnerNoFilterNoRange"])("actionType")
        +g_actionTrigger
        +g_buffType
)
g_actionApplyCone = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_APPLY_CONE)("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +co_parse.genEnumAttrib("weaponBank",armed.WEAPON_BANK)("weapBank")
        +pp.Group(
                g_range
                +coe.genFixedLvlAttrib("coneAngle")
                +g_maxTargets
        )("vals")
        +g_effectInfo
)
g_actionApplyEffect = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",INSTANT_ACTION_APPLY_EFFECT)(
                "actionType")
        +g_actionTrigger
        +g_buffType
        +g_effectInfo
)
g_actionApplyEffFilter = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_APPLY_EFFECT_FILTER)("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +g_effectInfo
)
g_actionApplyEntryVehicle = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_APPLY_ENTRY_VEHICLE)("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +g_range
        +co_parse.genStringAttrib("entryVehicleType")("entryVehicle")
        +pp.Group(
                coe.genFixedLvlAttrib("numEntryVehicles")
                +co_parse.genDecimalAttrib("travelTime")
        )("vals")
        +g_effectInfoEntryVehicle
)
g_actionApplyFilter = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",INSTANT_ACTION_APPLY_FILTER)(
                "actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
)
g_actionApplyForce = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","ApplyForceFromSpawner")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                co_parse.genDecimalAttrib("force")
        )("vals")
)
g_actionApplyOrbitBodyTravel = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "ApplyBuffToLocalOrbitBodyWithTravel")("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_travelSpeed
        )("vals")
        +g_hasWeapEffect
)
g_actionApplyOrRemove = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_APPLY_OR_REMOVE)("actionType")
        +g_actionTrigger
        +g_buffType
        +g_effectInfo
        +co_parse.genStringAttrib("toggleStateOnNameStringID")("toggleOnName")
        +co_parse.genStringAttrib("toggleStateOnDescStringID")("toggleOnDesc")
)
g_actionApplyRelModifier = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "ApplyRelationshipModifierToOwnerOfPlanetInCurrentGravityWell")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("relationshipIncrease")
        )("vals")
)
g_actionApplyTargetChainTravel = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "ApplyBuffToTargetsInRadiusWithChainTravel")("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_range
                +g_maxTargets
                +co_parse.genDecimalAttrib("chainDelay")
        )("vals")
        +g_hasWeapEffect
)
g_actionApplyTargetColumn = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","ApplyBuffToTargetsInColumn")(
                "actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                coe.genFixedLvlAttrib("columnRadius")
                +g_maxTargets
        )("vals")
        +g_effectInfo
)
g_actionApplyTargetOrbitBody = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                ["ApplyBuffToTargetsAtAdjacentOrbitBodies",
                    "ApplyBuffToTargetsAtOrbitBody"])("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_maxTargets
        )("vals")
        +g_effectInfo
)
g_actionApplyTargetRadius = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","ApplyBuffToTargetsInRadius")(
                "actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_range
                +g_maxTargets
        )("vals")
        +g_effectInfo
)
g_actionApplyTargetRadiusTravel = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_APPLY_TARGET_RADIUS_TRAVEL)("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +g_range
        +g_maxTargets
        +g_effectInfo
        +g_travelSpeed
        +co_parse.genDecimalAttrib("effectStaggerDelay")("staggerDelay")
        +g_hasWeapEffect
)
g_actionApplyTargetRange = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                ["ApplyTargettedBuffToSelf","ApplyBuffToTarget"])("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_range
        )("vals")
        +g_effectInfo
)
g_actionApplyTargetTravel = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "ApplyBuffToTargetWithTravel")("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_range
                +g_travelSpeed
        )("vals")
        +g_hasWeapEffect
)
g_actionApplyTargetWithinRadiusTravel = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "ApplyBuffToTargetsInRadiusOfTargetWithTravel")("actionType")
        +g_actionTrigger
        +g_buffType
        +g_targetFilter
        +pp.Group(
                g_range
                +g_maxTargets
                +g_travelSpeed
                +co_parse.genDecimalAttrib("effectStaggerDelay")
        )("vals")
        +g_hasWeapEffect
)
g_actionApplyTargettedSelf = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_APPLY_EFF_LVL)("actionType")
        +g_actionTrigger
        +g_buffType
        +pp.Group(
                g_range
        )("vals")
        +g_effectInfo
)
g_actionApplyWeap = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",INSTANT_ACTION_APPLY_WEAP)(
                "actionType")
        +g_actionTrigger
        +g_buffType
        +pp.Group(
                g_travelSpeed
        )("vals")
        +g_hasWeapEffect
)
g_actionAttractDebris = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","AttractDebris")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                g_range
                +g_travelSpeed
        )("vals")
)
g_actionChangePlayer = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CHANGE_PLAYER)("actionType")
        +g_actionTrigger
        +co_parse.genBoolAttrib("isPermanent")("permanent")
        +co_parse.genBoolAttrib("failIfNotEnoughShipSlots")("failIfLackingSupply")
        +pp.Group(
                co_parse.genDecimalAttrib("experiencePercentageToAward")
        )("vals")
)
g_actionColonizePlanet = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_COLONIZE_PLANET)("actionType")
        +g_actionTrigger
        +g_targetFilter
        +g_range
        +co_parse.genDecimalAttrib("delayUntilColonization")("delay")
        +co_parse.genStringAttrib("entryVehicleType")("entryVehicles")
        +coe.genFixedLvlAttrib("numEntryVehicles")("numEntryVehicles")
        +co_parse.genDecimalAttrib("travelTime")("travelTime")
        +g_effectInfoEntryVehicle
        +co_parse.genStringAttrib("afterColonizeBuffType")("afterColonizeBuff")
)
g_actionConvertDmgAM = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CONVERT_DMG_AM)("actionType")
        +g_actionTrigger
        +g_dmgType
        +pp.Group(
                coe.genFixedLvlAttrib("antiMatterFromDamagePerc")
        )("vals")
)
g_actionConvertFrigateResources = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","ConvertFrigateToResources")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("percentageOfCost")
        )("vals")
)
g_actionCreateFrigate = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CREATE_FRIGATE)("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("frigateType")("entity")
        +coe.genFixedLvlAttrib("numFrigates")("numFrigates")
        +co_parse.genBoolAttrib("matchOwnerDamageState")("matchOwnerDmg")
        +pp.Group(
                g_impulse
                +coe.genFixedLvlAttrib("expiryTime")("expiryTime")
        )("vals")
        +co_parse.genStringAttrib("spawnFrigateSoundID")("spawnSound")
        +co_parse.genStringAttrib("postSpawnBuff")("postSpawnBuff")
)
g_actionCreateFrigateClone = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CREATE_FRIGATE_CLONE)("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("numFrigates")
                +g_impulse
                +coe.genFixedLvlAttrib("expiryTime")
        )("vals")
        +co_parse.genStringAttrib("spawnFrigateSoundID")("spawnSound")
)
g_actionCreateFrigateTarget = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CREATE_FRIGATE_TARGET)("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("frigateType")("entity")
        +pp.Group(
                coe.genFixedLvlAttrib("numFrigates")
                +g_impulse
                +coe.genFixedLvlAttrib("expiryTime")
        )("vals")
        +co_parse.genStringAttrib("spawnFrigateSoundID")("spawnSound")
        +g_targetFilter
        +g_range
)
g_actionCreateFrigateTargetArbitrary = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "CreateFrigateAtArbitraryTarget")("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("frigateType")("entity")
        +pp.Group(
                coe.genFixedLvlAttrib("numFrigates")
                +coe.genFixedLvlAttrib("expiryTime")
        )("vals")
        +co_parse.genStringAttrib("spawnFrigateSoundID")("spawnSound")
        +g_range
)
g_actionCreateIllusionFighters = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CREATE_ILLUSION_FIGHTERS)("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("numFighters")
                +coe.genFixedLvlAttrib("expiryTime")
        )("vals")
        +g_effectInfo
)
g_actionCreatePlanetModule = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "CreatePlanetModule")("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("planetModuleType")("entity")
        +co_parse.genStringAttrib("spawnPlanetModuleSoundID")("spawnSound")
)
g_actionCreateShell = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CREATE_SHELL)("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("cannonShellType")("entity")
        +g_effectInfo
)
g_actionCreateStarbase = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_CREATE_STARBASE)("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("starBaseType")("entity")
        +pp.Group(
                coe.genFixedLvlAttrib("placementRadius")
        )("vals")
)
g_actionCreateSpaceMine = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "CreateSpaceMine")("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("spaceMineType")("entity")
        +pp.Group(
                coe.genFixedLvlAttrib("numMines")
                +g_impulse
                +co_parse.genDecimalAttrib("angleVariance")
                +coe.genFixedLvlAttrib("expiryTime")
        )("vals")
        +co_parse.genStringAttrib("spawnMineSoundID")("spawnSound")
)
g_actionCreateSquad = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "CreateSquad")("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("squadType")("entity")
        +pp.Group(
                coe.genFixedLvlAttrib("numSquads")
                +coe.genFixedLvlAttrib("expiryTime")
        )("vals")
        +co_parse.genStringAttrib("spawnSquadSoundID")("spawnSound")
)
g_actionDamagePlanet = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","DoDamageToPlanet")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("damage")
                +coe.genFixedLvlAttrib("populationKilled")
        )("vals")
)
g_actionDebrisToHull = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_DEBRIS_HULL)("actionType")
        +g_actionTrigger
        +pp.Group(
                g_range
                +coe.genFixedLvlAttrib("hullPerDebris")
        )("vals")
        +co_parse.genListAttrib("soundCount",
                co_parse.genStringAttrib("sound"),
                "debrisCleanupSounds")("cleanupSounds")
)
g_actionDoDamage = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","DoDamage")("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("damage")
        )("vals")
        +g_dmgAffect
        +g_dmgType
        +co_parse.genBoolAttrib("isDamageShared")("isShared")
)
g_actionDoDamageLastSpawnerPop = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "DoDamagePerLastSpawnerPopulation")("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("damagePerPopulationPoint")
        )("vals")
        +g_dmgAffect
        +g_dmgType
        +co_parse.genBoolAttrib("isDamageShared")("isShared")
)
g_actionDoDamagePerEntity = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_DMG_ENTITY)("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("damagePerEntity")
        )("vals")
        +g_dmgAffect
        +g_dmgType
        +co_parse.genBoolAttrib("isDamageShared")("isShared")
        +g_targetFilter
        +g_range
        +g_maxTargets
)
g_actionDoDamagePercHull = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "DoDamagePercOfCurrentHull")("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("hullDamagePerc")
        )("vals")
        +g_dmgAffect
        +g_dmgType
        +co_parse.genBoolAttrib("isDamageShared")("isShared")
)
g_actionGiveCredits = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "GiveCreditsToPlayer")("actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("credits")
        )("vals")
        +g_effectInfo
)
g_actionGiveExp = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","GiveExperience")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                co_parse.genDecimalAttrib("experienceToGive")
        )("vals")
)
g_actionImpulse = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",["ApplyImpulseFromSpawner",
            "ApplyImpulseInSpawnerDirection"])("actionType")
        +g_actionTrigger
        +pp.Group(
                g_impulse
        )("vals")
)
g_actionIncreaseAbilityLvl = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","IncreaseOwnerAbilityLevel")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                co_parse.genDecimalAttrib("abilityIndex")
                +coe.genFixedLvlAttrib("abilityLevel")
        )("vals")
)
g_actionMoveLastSpawner = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "InitializeMovementTowardLastSpawner")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                co_parse.genDecimalAttrib("minLinearSpeed")
                +co_parse.genDecimalAttrib("maxLinearSpeed")
        )("vals")
)
g_actionPlayAttachedEffect = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",INSTANT_ACTION_EFFECT)(
                "actionType")
        +g_actionTrigger
        +g_effectInfo
)
g_actionPlayEffectsRadius = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "PlayDetachedEffectsInRadius")("actionType")
        +g_actionTrigger
        +g_effectInfo
        +pp.Group(
                g_range
                +co_parse.genIntAttrib("numEffects")
                +co_parse.genDecimalAttrib("delayBetweenEffects")
        )("vals")
)
g_actionPropagateDmg = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_PROPAGATE_DMG)("actionType")
        +g_actionTrigger
        +g_targetFilter
        +pp.Group(
                g_range
                +g_maxTargets
        )("vals")
)
g_actionRandomMotion = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","InitializeRandomMotion")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                co_parse.genDecimalAttrib("minRandomAngularSpeed")
                +co_parse.genDecimalAttrib("maxRandomAngularSpeed")
                +co_parse.genDecimalAttrib("minRandomLinearSpeed")
                +co_parse.genDecimalAttrib("maxRandomLinearSpeed")
        )("vals")
)
g_actionRemoveBuffType = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RemoveBuffOfType")(
                "actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("buffTypeToRemove")("buff")
)
g_actionRemoveAMPerc = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RemoveAntiMatterPerc")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("antiMatterPerc")
        )("vals")
)
g_actionRestoreHull = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RestoreHullPoints")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("hull")
        )("vals")
)
g_actionRestoreHullPerc = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RestoreHullPointsPerc")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("hullPerc")
        )("vals")
)
g_actionRestoreShield = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RestoreShieldPoints")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("shields")
        )("vals")
)
g_actionRestoreShieldPerc = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RestoreShieldPointsPerc")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("shieldPerc")
        )("vals")
)
g_actionRetaliateBounty = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RetaliateBounty")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("bountyAmount")
        )("vals")
)
g_actionRetaliateDmg = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RetaliateDamage")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("damageRetaliationPerc")
        )("vals")
)
g_actionRuinPlanet = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","RuinPlanet")(
                "actionType")
        +g_actionTrigger
        +g_targetFilter
        +pp.Group(
                g_range
        )("vals")
        +co_parse.genStringAttrib("afterRuinBuffType")("buff")
)
g_actionSimple = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",INSTANT_ACTION_SIMPLE)(
                "actionType")
        +g_actionTrigger
)
g_actionSpawnExtractors = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","SpawnResourceExtractors")(
                "actionType")
        +g_actionTrigger
        +pp.Group(
                coe.genFixedLvlAttrib("numExtractors")
        )("vals")
)
g_actionSpawnShips = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_SPAWN_SHIPS)("actionType")
        +g_actionTrigger
        +co_parse.genListAttrib("spawnShipsLevelCount",consts.g_spawnShips)("ships")
        +pp.Group(
                co_parse.genDecimalAttrib("spawnShipsArrivalDelayTime")
        )("vals")
        +co_parse.genEnumAttrib("spawnShipsHyperspaceSpawnType",SPAWN_SHIPS_TYPE)(
                "hyperSpawnType")
)
g_actionSquadToMines = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_SQUAD_MINES)("actionType")
        +g_actionTrigger
        +co_parse.genStringAttrib("spaceMineType")("entity")
        +pp.Group(
                coe.genFixedLvlAttrib("expiryTime")
        )("vals")
)
g_actionStealResources = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_STEAL_RESOURCES)("actionType")
        +g_actionTrigger
        +co_parse.genEnumAttrib("resourceToSteal",RESOURCE_TYPES)("resource")
        +pp.Group(
                coe.genFixedLvlAttrib("resourceAmount")
        )("vals")
)
g_actionTeleportMoveTarget = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "TeleportTowardsMoveTarget")("actionType")
        +g_actionTrigger
        +pp.Group(
                co_parse.genDecimalAttrib("teleportDistance")
        )("vals")
)
g_actionTeleportTowardsTarget = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType","TeleportTowardsTarget")(
                "actionType")
        +g_actionTrigger
        +pp.Group(co_parse.genDecimalAttrib("teleportStopOffset"))("vals")
)
g_actionTiltVectorTarget = pp.Group(
        co_parse.genKeyValPair("buffInstantActionType",
                "TiltUpVectorInTargetDirection")(
                "actionType")
        +g_actionTrigger
        +pp.Group(co_parse.genDecimalAttrib("angleOffset"))("vals")
)
g_actionWeaponEffect = pp.Group(
        co_parse.genEnumAttrib("buffInstantActionType",
                INSTANT_ACTION_WEAPON_EFFECT)("actionType")
        +g_actionTrigger
        +g_hasWeapEffect
)
g_instantActions = pp.Or([g_actionAddPop,g_actionAllegianceChange,g_actionAM,
    g_actionApplyBuff,g_actionApplyCone,g_actionApplyEffect,
    g_actionApplyEffFilter,g_actionApplyEntryVehicle,g_actionApplyFilter,
    g_actionApplyForce,g_actionApplyOrbitBodyTravel,g_actionApplyOrRemove,
    g_actionApplyRelModifier,g_actionApplyTargetChainTravel,
    g_actionApplyTargetColumn,g_actionApplyTargetOrbitBody,
    g_actionApplyTargetRadius,
    g_actionApplyTargetRadiusTravel,g_actionApplyTargetRange,
    g_actionApplyTargetTravel,g_actionApplyTargetWithinRadiusTravel,
    g_actionApplyTargettedSelf,g_actionApplyWeap,g_actionAttractDebris,
    g_actionChangePlayer,g_actionColonizePlanet,g_actionConvertDmgAM,
    g_actionConvertFrigateResources,g_actionCreateFrigate,
    g_actionCreateFrigateClone,g_actionCreateFrigateTarget,
    g_actionCreateFrigateTargetArbitrary,g_actionCreateIllusionFighters,
    g_actionCreatePlanetModule,g_actionCreateShell,g_actionCreateStarbase,
    g_actionCreateSpaceMine,g_actionCreateSquad,g_actionDamagePlanet,
    g_actionDebrisToHull,g_actionDoDamage,g_actionDoDamageLastSpawnerPop,
    g_actionDoDamagePerEntity,g_actionDoDamagePercHull,g_actionGiveCredits,
    g_actionImpulse,g_actionIncreaseAbilityLvl,g_actionMoveLastSpawner,
    g_actionPlayAttachedEffect,g_actionPlayEffectsRadius,g_actionPropagateDmg,
    g_actionRandomMotion,g_actionRemoveBuffType,g_actionRemoveAMPerc,
    g_actionRestoreHull,g_actionRestoreHullPerc,g_actionRestoreShield,
    g_actionRestoreShieldPerc,g_actionRetaliateBounty,g_actionRetaliateDmg,
    g_actionRuinPlanet,g_actionGiveExp,
    g_actionSimple,g_actionSpawnExtractors,g_actionSpawnShips,
    g_actionSquadToMines,g_actionStealResources,g_actionTeleportMoveTarget,
    g_actionTeleportTowardsTarget,g_actionTiltVectorTarget,
    g_actionWeaponEffect])
g_buffInstantAction = pp.Group(
        co_parse.genHeading("instantAction")
        +pp.ungroup(g_instantActions)
)
g_actionCountInfinite = pp.Group(
        co_parse.genKeyValPair("actionCountType","Infinite")("countType")
)
g_actionCountFinite = pp.Group(
        co_parse.genKeyValPair("actionCountType","Finite")("countType")
        +coe.genFixedLvlAttrib("actionCount")("count")
)
g_buffPeriodicAction = pp.Group(
        co_parse.genHeading("periodicAction")
        +pp.Or([g_actionCountInfinite,g_actionCountFinite])("count")
        +coe.genFixedLvlAttrib("actionIntervalTime")("interval")
        +g_instantActions("instantAction")
)

"""### Over Time Actions ###"""
g_actionOverTimeDoDmg = (
    co_parse.genEnumAttrib("buffOverTimeActionType",ACTION_OVER_TIME_DO_DMG)(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("damageRate")
    )("vals")
    +g_dmgAffect
    +g_dmgType
    +co_parse.genBoolAttrib("isDamageShared")("dmgShared")
)
g_actionOverTimeDmgPlanet = (
    co_parse.genEnumAttrib("buffOverTimeActionType",ACTION_OVER_TIME_DMG_PLANET)(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("damageRate")
            +coe.genFixedLvlAttrib("populationKillRate")
    )("vals")
)
g_actionOverTimeDmgLeechResources = (
    co_parse.genKeyValPair("buffOverTimeActionType",
            "DoDamageAndGiveResourceCostPerc")("actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("resourceGainPerc")
            +coe.genFixedLvlAttrib("damageRate")
    )("vals")
)
g_actionOverTimeModuleConstruct = (
    co_parse.genKeyValPair("buffOverTimeActionType","DoModuleConstruction")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("buildRate")
            +coe.genFixedLvlAttrib("moduleCostFraction")
    )("vals")
)
g_actionOverTimeDrainAM = (
    co_parse.genKeyValPair("buffOverTimeActionType","DrainAntiMatter")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("drainAntiMatterRate")
    )("vals")
)
g_actionOverTimeDmgDrainAM = (
    co_parse.genKeyValPair("buffOverTimeActionType","DrainAntiMatterAndDoDamage")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("drainAntiMatterRate")
            +coe.genFixedLvlAttrib("damageRate")
    )("vals")
    +co_parse.genBoolAttrib("isDamageShared")("dmgShared")
)
g_actionOverTimeEarnResources = (
    co_parse.genKeyValPair("buffOverTimeActionType","EarnResources")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("earnRate")
    )("vals")
)
g_actionOverTimeMagnetize = (
    co_parse.genKeyValPair("buffOverTimeActionType","Magnetize")(
            "actionType")
    +g_targetFilter
    +pp.Group(
            g_range
            +coe.genFixedLvlAttrib("maxNumTargets")
            +coe.genFixedLvlAttrib("damagePerImpact")
            +co_parse.genDecimalAttrib("pullForceMagnitude")
    )("vals")
)
g_actionOverTimeRestoreAllegiance = (
    co_parse.genKeyValPair("buffOverTimeActionType","RestoreAllegiance")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("allegianceRestoreRate")
    )("vals")
)
g_actionOverTimeRestoreAM = (
    co_parse.genKeyValPair("buffOverTimeActionType","RestoreAntiMatter")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("antiMatterRestoreRate")
    )("vals")
)
g_actionOverTimeRestoreHull = (
    co_parse.genKeyValPair("buffOverTimeActionType","RestoreHull")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("hullRestoreRate")
    )("vals")
)
g_actionOverTimeRestorePlanetHealth = (
    co_parse.genKeyValPair("buffOverTimeActionType","RestorePlanetHealth")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("planetHealthRestoreRate")
    )("vals")
)
g_actionOverTimeRestoreShields = (
    co_parse.genKeyValPair("buffOverTimeActionType","RestoreShields")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("shieldRestoreRate")
    )("vals")
)
g_actionOverTimeStealProduction = (
    co_parse.genKeyValPair("buffOverTimeActionType","StealPlanetProduction")(
            "actionType")
    +pp.Group(
            coe.genFixedLvlAttrib("stolenPlanetProductionPerc")
    )("vals")
)
g_buffActionOverTime = pp.Group(
        co_parse.genHeading("overTimeAction")
        +pp.Or([g_actionOverTimeDoDmg,g_actionOverTimeDmgPlanet,
            g_actionOverTimeDmgLeechResources,g_actionOverTimeModuleConstruct,
            g_actionOverTimeDrainAM,g_actionOverTimeDmgDrainAM,
            g_actionOverTimeEarnResources,g_actionOverTimeMagnetize,
            g_actionOverTimeRestoreAllegiance,g_actionOverTimeRestoreAM,
            g_actionOverTimeRestoreHull,g_actionOverTimeRestorePlanetHealth,
            g_actionOverTimeRestoreShields,g_actionOverTimeStealProduction])
)

"""### Finish Conditions ###"""
g_finishCondSimple = (
    co_parse.genEnumAttrib("finishConditionType",FINISH_COND_SIMPLE)("finishType")
)
g_finishCondDmgTaken = (
    co_parse.genKeyValPair("finishConditionType","DamageTaken")("finishType")
    +pp.Group(
            coe.genFixedLvlAttrib("damageNeeded")
    )("vals")
    +co_parse.genEnumAttrib("typeOfDamageNeeded",armed.DMG_AFFECTS)("dmgType")
)
g_finishCondFightersKilled = (
    co_parse.genKeyValPair("finishConditionType","FightersKilled")("finishType")
    +pp.Group(
            coe.genFixedLvlAttrib("fightersKilled")
    )("vals")
)
g_finishCondNoLongerBuffed = (
    co_parse.genEnumAttrib("finishConditionType",FINISH_COND_NO_LONGER_BUFFED)(
            "finishType")
    +co_parse.genStringAttrib("buffTypeToQuery")("buff")
)
g_finishCondHullAbove = (
    co_parse.genKeyValPair("finishConditionType","OwnerHullAbovePerc")(
            "finishType")
    +pp.Group(
            coe.genFixedLvlAttrib("hullPercent")
    )("vals")
)
g_finishCondResearchNotMet = (
    co_parse.genKeyValPair("finishConditionType","ResearchNotMet")("finishType")
    +coe.g_researchPrereqs
)
g_finishCondTimeElapsed = (
    co_parse.genKeyValPair("finishConditionType","TimeElapsed")(
            "finishType")
    +pp.Group(
            coe.genFixedLvlAttrib("time")
    )("vals")
)
g_finishCondition = pp.Group(
        co_parse.genHeading("finishCondition")
        +pp.Or([g_finishCondSimple,g_finishCondDmgTaken,
            g_finishCondFightersKilled,g_finishCondNoLongerBuffed,
            g_finishCondHullAbove,g_finishCondResearchNotMet,
            g_finishCondTimeElapsed])
)

"""### Buff Entity ###"""
g_buffEntityModifier = pp.Group(
        co_parse.genHeading("entityModifier")
        +co_parse.genEnumAttrib("buffEntityModifierType",BUFF_ENTITY_MODIFIERS)(
                "modType")
        +coe.genFixedLvlAttrib("value")("value")
)
g_BuffEntity = (
    coe.genEntityHeader("Buff")
    +co_parse.genEnumAttrib("onReapplyDuplicateType",ON_REAPPLY_DUPLICATE)(
            "onReapply")
    +co_parse.genEnumAttrib("buffStackingLimitType",STACKING_LIMITS)(
            "stackingLimitType")
    +pp.Or([
        pp.Group(
                co_parse.g_linestart
                +pp.Keyword("stackingLimit")("identifier")
                +co_parse.g_space
                +pp.Literal("-1").setParseAction(lambda t:int(t[0]))("val")
                +co_parse.g_eol
        )("stackingLimit"),
        (
            co_parse.genIntAttrib("stackingLimit")("stackingLimit")
            +co_parse.genBoolAttrib("allowFirstSpawnerToStack")(
                    "allowFirstSpawnerStack")
        )
    ])
    +co_parse.genEnumAttrib("buffExclusivityForAIType",BUFF_EXCLUSIVITY)(
            "exclusivityForAI")
    +co_parse.genBoolAttrib("isInterruptable")("interruptable")
    +co_parse.genBoolAttrib("isChannelling")("channeling")
    +co_parse.genListAttrib("numInstantActions",g_buffInstantAction)(
            "instantActions")
    +co_parse.genListAttrib("numPeriodicActions",g_buffPeriodicAction)(
            "periodicActions")
    +co_parse.genListAttrib("numOverTimeActions",g_buffActionOverTime)(
            "actionsOverTime")
    +co_parse.genListAttrib("numEntityModifiers",g_buffEntityModifier)(
            "entityMods")
    +co_parse.genListAttrib("numEntityBoolModifiers",
            co_parse.genEnumAttrib("entityBoolModifier",BUFF_ENTITY_MODIFIERS_BOOL))(
            "entityModsBool")
    +co_parse.genListAttrib("numFinishConditions",g_finishCondition)(
            "finishConditions")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Level Sources ###"""

g_lvlSourceSimple = pp.Group(
        co_parse.genEnumAttrib("levelSourceType",LVL_SOURCE_SIMPLE)("sourceType")
)
g_lvlSourceIntrinsic = pp.Group(
        co_parse.genEnumAttrib("levelSourceType",LVL_SOURCE_INTRINSIC)("sourceType")
        +coe.genFixedLvlAttrib("minExperienceLevelRequired")("minExpForLvl")
)
g_lvlSourceResearch = pp.Group(
        co_parse.genEnumAttrib("levelSourceType",LVL_SOURCE_RESEARCH_NO_BASE)(
                "sourceType")
        +co_parse.genStringAttrib("improveSourceResearchSubject")("subject")
)
g_lvlSourceResearchBase = pp.Group(
        co_parse.genEnumAttrib("levelSourceType",LVL_SOURCE_RESEARCH_BASE)(
                "sourceType")
        +co_parse.genStringAttrib("baseSourceResearchSubject")("base")
        +co_parse.genStringAttrib("improveSourceResearchSubject")("subject")
)
g_lvlSourceUpgrade = pp.Group(
        co_parse.genEnumAttrib("levelSourceType",LVL_SOURCE_UPGRADE)(
                "sourceType")
        +co_parse.genEnumAttrib("sourcePropertyType",SOURCE_PROPERTIES)(
                "sourceProperty")
)
g_lvlSource = pp.Or([g_lvlSourceSimple,g_lvlSourceIntrinsic,
    g_lvlSourceResearch,g_lvlSourceResearchBase,g_lvlSourceUpgrade])(
        "lvlSource")

"""### AI Use Time ###"""

g_aiUseTimeSimple = pp.Group(
        co_parse.genEnumAttrib("aiUseTime",AI_USE_TIME_SIMPLE)("aiUseType")
)
g_aiUseTimeDmgHull = pp.Group(
        co_parse.genEnumAttrib("aiUseTime",AI_USE_TIME_DMG_HULL)("aiUseType")
        +coe.genFixedLvlAttrib("onlyAutoCastWhenTotalHullDamageExceedsAmount"
        )("val")
)
g_aiUseTimeDebris = pp.Group(
        co_parse.genKeyValPair("aiUseTime","OnlyWhenDebrisWithinRange")("aiUseType")
        +co_parse.genDecimalAttrib("onlyAutoCastWhenDebrisWithinRange")("val")
)
g_aiUseTimeTargetsNotBuffed = pp.Group(
        co_parse.genEnumAttrib("aiUseTime",AI_USE_TIME_TARGETS_NOT_BUFFED)(
                "aiUseType")
        +coe.genFixedLvlAttrib("onlyAutoCastWhenTargetCountExceedsAmount")(
                "val")
        +co_parse.genStringAttrib("buffType")("buff")
)
g_aiUseTimeTargetsInRange = pp.Group(
        co_parse.genKeyValPair("aiUseTime","OnlyWhenManyTargetsInRange")("aiUseType")
        +coe.genFixedLvlAttrib("onlyAutoCastWhenTargetCountExceedsAmount")(
                "val")
)
g_aiUseTimeTargetsRangeBank = pp.Group(
        co_parse.genEnumAttrib("aiUseTime",AI_USE_TIME_MANY_TARGETS_BANK_RANGE)(
                "aiUseType")
        +coe.genFixedLvlAttrib("onlyAutoCastWhenTargetCountExceedsAmount")(
                "val")
        +co_parse.genEnumAttrib("onlyAutoCastWhenTargetsInBank",armed.WEAPON_BANK)(
                "bank")
)
g_aiUseTimethresholdDist = pp.Group(
        co_parse.genKeyValPair("aiUseTime","OnlyWhenMovingThresholdDistance")(
                "aiUseType")
        +co_parse.genDecimalAttrib("thresholdDistance")("val")
)
g_aiUseTime = pp.Or([g_aiUseTimeSimple,g_aiUseTimeDmgHull,g_aiUseTimeDebris,
    g_aiUseTimeTargetsNotBuffed,g_aiUseTimeTargetsInRange,
    g_aiUseTimeTargetsRangeBank,g_aiUseTimethresholdDist])("aiUseTime")

"""### AI Use Target ###"""

g_aiUseTargetSimple = pp.Group(
        co_parse.genEnumAttrib("aiUseTargetCondition",AI_USE_TARGET_SIMPLE)(
                "aiUseTargetType")
)
g_aiUseTargetHasBuff = pp.Group(
        co_parse.genEnumAttrib("aiUseTargetCondition",AI_USE_TARGET_BUFF)(
                "aiUseTargetType")
        +co_parse.genStringAttrib("buffType")("buff")
)
g_aiUseTargetAM = pp.Group(
        co_parse.genKeyValPair("aiUseTargetCondition","AntimatterExceedsAmount")(
                "aiUseTargetType")
        +coe.genFixedLvlAttrib(
                "onlyAutoCastWhenTargetAntimatterExceedsAmount")("val")
)
g_aiUseTargetHullDmg = pp.Group(
        co_parse.genKeyValPair("aiUseTargetCondition","HullDamageExceedsAmount")(
                "aiUseTargetType")
        +coe.genFixedLvlAttrib(
                "onlyAutoCastWhenTargetHullDamageExceedsAmount")("val")
)
g_aiUseTargetShieldDmg = pp.Group(
        co_parse.genKeyValPair("aiUseTargetCondition","ShieldDamageExceedsAmount")(
                "aiUseTargetType")
        +coe.genFixedLvlAttrib(
                "onlyAutoCastWhenTargetShieldDamageExceedsAmount")("val")
)
g_aiUseTarget = pp.Or([g_aiUseTargetSimple,g_aiUseTargetHasBuff,
    g_aiUseTargetAM,g_aiUseTargetHullDmg,g_aiUseTargetShieldDmg])("aiUseTarget")

"""### Use Costs ###"""

g_cooldown = coe.genFixedLvlAttrib("cooldownTime")("cooldown")
g_amCost = coe.genFixedLvlAttrib("antiMatterCost")("antimatterCost")
g_orderAcknowledge = co_parse.genEnumAttrib("orderAcknowledgementType",
        ORDER_ACKNOWLEDGE)("orderAcknowledge")
g_resourceCost = pp.Group(
        co_parse.genHeading("resourceCost")
        +coe.g_cost
)("resources")
g_costPassive = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_PASSIVE)("costType")
)
g_costNone = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_NONE)("costType")
        +g_cooldown
        +g_orderAcknowledge
)
g_costAM = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_AM)("costType")
        +g_amCost
        +g_cooldown
        +g_orderAcknowledge
)
g_costAMHull = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_AM_HULL)("costType")
        +coe.genFixedLvlAttrib("hullCost")("hullCost")
        +g_amCost
        +g_cooldown
        +g_orderAcknowledge
)
g_costResources = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_RESOURCES)("costType")
        +g_resourceCost
        +g_cooldown
        +g_orderAcknowledge
)
g_costResourcesSupply = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_RESOURCE_SUPPLY)("costType")
        +g_resourceCost
        +coe.genFixedLvlAttrib("requiredShipSlots")("reqSupply")
        +g_cooldown
        +g_orderAcknowledge
)
g_costResourcesCapital = pp.Group(
        co_parse.genEnumAttrib("useCostType",COST_TYPE_RESOURCE_CAPITAL)("costType")
        +g_resourceCost
        +coe.genFixedLvlAttrib("requiredShipSlots")("reqSupply")
        +coe.genFixedLvlAttrib("requiredCapitalShipSlots")("reqCapCrews")
        +g_cooldown
        +g_orderAcknowledge
)
g_useCost = pp.Or([g_costPassive,g_costNone,g_costAM,g_costAMHull,
    g_costResources,g_costResourcesSupply,g_costResourcesCapital])("cost")

"""### Ability Entity ###"""
g_AbilityEntity = (
    coe.genEntityHeader("Ability")
    +g_instantActions("instantAction")
    +co_parse.genBoolAttrib("needsToFaceTarget")("faceTarget")
    +co_parse.genBoolAttrib("canCollideWithTarget")("canCollideWithTarget")
    +co_parse.genBoolAttrib("moveThruTarget")("moveThruTarget")
    +co_parse.genBoolAttrib("isUltimateAbility")("ultimate")
    +co_parse.genIntAttrib("maxNumLevels")("maxLevels")
    +g_lvlSource
    +g_aiUseTime
    +g_aiUseTarget
    +co_parse.genBoolAttrib("isAutoCastAvailable")("canAutocast")
    +co_parse.genBoolAttrib("isAutoCastOnByDefault")("isAutocastDefault")
    +co_parse.genBoolAttrib("pickRandomPlanetToExploreForAutoCastTarget")(
            "pickRandPlanetExplore")
    +co_parse.genBoolAttrib("ignoreNonCombatShipsForAutoCastTarget")(
            "ignoreCivShips")
    +co_parse.genDecimalAttrib("onlyAutoCastWhenDamageTakenExceedsPerc")(
            "onlyAutocastWhenDmgPerc")
    +g_useCost
    +coe.g_researchPrereqs
    +coe.g_nameString
    +coe.g_descriptionStringShort
    +coe.g_uiIcons
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################## Attributes ###############################"""

class TargetFilter(co_attribs.Attribute):
    def __init__(self,ownerships,objects,spaces,constraints,**kwargs):
        super().__init__(**kwargs)
        self.ownerships = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":OWNERSHIPS},
                **ownerships)
        self.objects = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":OBJECTS},
                **objects)
        self.spaces = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":SPACE_TYPES},
                **spaces)
        self.constraints = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":TARGET_CONSTRAINTS},
                **constraints)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.ownerships==other.ownerships)
                        and (self.objects==other.objects)
                        and (self.spaces==other.spaces)
                        and (self.constraints==other.constraints))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.ownerships.toString(1)
        res += self.objects.toString(1)
        res += self.spaces.toString(1)
        res += self.constraints.toString(1)
        return indentText(res,indention)

class BuffEntityModifier(co_attribs.Attribute):
    def __init__(self,modType,value,**kwargs):
        super().__init__(**kwargs)
        self.modType = co_attribs.AttribEnum(possibleVals=BUFF_ENTITY_MODIFIERS,
                **modType)
        self.value = co_attribs.AttribFixedLevelable(**value)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.modType==other.modType)
                        and (self.value==other.value))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.modType.toString(1)
        res += self.value.toString(1)
        return indentText(res,indention)

"""### Instant Action Conditions ###"""

class InstantActionCondition(co_attribs.Attribute):
    def __init__(self,condType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.condType = co_attribs.AttribEnum(possibleVals=INSTANT_ACTION_CONDITIONS,
                **condType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.condType==other.condType)

    @classmethod
    def factory(cls,condType,**kwargs):
        if condType["val"] in INSTANT_ACTION_COND_SIMPLE:
            return InstantActionCondition(condType=condType,**kwargs)
        elif condType["val"] in INSTANT_ACTION_COND_BUFF:
            return InstantActionCondBuff(condType=condType,**kwargs)
        elif condType["val"]=="IfOwnerHasHullLessThanPerc":
            return InstantActionCondLvl(condType=condType,**kwargs)
        else:
            raise RuntimeError(condType["val"]+" is not a valid instant action"
                               +" condition type.")

    def toString(self,indention):
        res = super().toString(0)
        res += self.condType.toString(0)
        return indentText(res,indention)

class InstantActionCondBuff(InstantActionCondition,
        co_basics.HasReference):
    def __init__(self,buffType,buffShortName,**kwargs):
        super().__init__(**kwargs)
        self.buffType = coe.EntityRef(types=["Buff"],**buffType)
        self.buffShortName = ui.StringReference(**buffShortName)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.buffType==other.buffType)
                        and (self.buffShortName==other.buffShortName))

    def toString(self,indention):
        res = super().toString(0)
        res += self.buffType.toString(0)
        res += self.buffShortName.toString(0)
        return indentText(res,indention)

class InstantActionCondLvl(InstantActionCondition):
    def __init__(self,hullPerc,**kwargs):
        super().__init__(**kwargs)
        self.hullPerc = co_attribs.AttribFixedLevelable(**hullPerc)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.hullPerc==other.hullPerc)

    def toString(self,indention):
        res = super().toString(0)
        res += self.hullPerc.toString(0)
        return indentText(res,indention)

"""### Instant Action Triggers ###"""

class InstantActionTrigger(co_attribs.Attribute):
    def __init__(self,triggerType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.triggerType = co_attribs.AttribEnum(possibleVals=INSTANT_ACTION_TRIGGERS,
                **triggerType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.triggerType==other.triggerType)

    @classmethod
    def factory(cls,triggerType,**kwargs):
        if triggerType["val"] in INSTANT_ACTION_TRIGGERS_SIMPLE:
            return InstantActionTrigger(triggerType=triggerType,**kwargs)
        elif triggerType["val"]=="OnChance":
            return TriggerChance(triggerType=triggerType,**kwargs)
        elif triggerType["val"]=="OnWeaponFired":
            return TriggerWeaponFired(triggerType=triggerType,**kwargs)
        elif triggerType["val"]=="OnCondition":
            return TriggerCond(triggerType=triggerType,**kwargs)
        elif triggerType["val"]=="OnDelay":
            return TriggerDelay(triggerType=triggerType,**kwargs)
        else:
            raise RuntimeError("Unknown Action Trigger "+triggerType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.triggerType.toString(0)
        return indentText(res,indention)

class TriggerChance(InstantActionTrigger):
    def __init__(self,applyChance,**kwargs):
        super().__init__(**kwargs)
        self.applyChance = co_attribs.AttribFixedLevelable(**applyChance)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.applyChance==other.applyChance)

    def toString(self,indention):
        res = super().toString(0)
        res += self.applyChance.toString(0)
        return indentText(res,indention)

class TriggerDelay(InstantActionTrigger):
    def __init__(self,delay,**kwargs):
        super().__init__(**kwargs)
        self.delay = co_attribs.AttribNum(**delay)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.delay==other.delay)

    def toString(self,indention):
        res = super().toString(0)
        res += self.delay.toString(0)
        return indentText(res,indention)

class TriggerWeaponFired(InstantActionTrigger):
    def __init__(self,weaponClasses,passiveEffectChance,**kwargs):
        super().__init__(**kwargs)
        self.weaponClasses = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":coe.WEAPON_CLASSES},
                **weaponClasses)
        self.passiveEffectChance = co_attribs.AttribFixedLevelable(
                **passiveEffectChance)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.weaponClasses==other.weaponClasses)
                        and (
                        self.passiveEffectChance==other.passiveEffectChance))

    def toString(self,indention):
        res = super().toString(0)
        res += self.weaponClasses.toString(0)
        res += self.passiveEffectChance.toString(0)
        return indentText(res,indention)

class TriggerCond(InstantActionTrigger,co_basics.HasReference):
    def __init__(self,cond,**kwargs):
        super().__init__(**kwargs)
        self.cond = InstantActionCondition.factory(**cond)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.cond==other.cond)

    def toString(self,indention):
        res = super().toString(0)
        res += self.cond.toString(0)
        return indentText(res,indention)

"""### Finish Conditions ###"""

class BuffFinishCond(co_attribs.Attribute):
    def __init__(self,finishType,**kwargs):
        super().__init__(**kwargs)
        self.finishType = co_attribs.AttribEnum(possibleVals=FINISH_CONDITIONS,
                **finishType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.finishType==other.finishType)

    @classmethod
    def factory(cls,finishType,**kwargs):
        if finishType["val"] in FINISH_COND_SIMPLE:
            return BuffFinishCond(finishType=finishType,**kwargs)
        elif finishType["val"] in FINISH_COND_NO_LONGER_BUFFED:
            return BuffFinishCondBuff(finishType=finishType,**kwargs)
        elif finishType["val"] in FINISH_COND_NUMERIC:
            return BuffFinishCondSimple(finishType=finishType,**kwargs)
        elif finishType["val"]=="DamageTaken":
            return BuffFinishCondDmg(finishType=finishType,**kwargs)
        elif finishType["val"]=="ResearchNotMet":
            return BuffFinishCondResearch(finishType=finishType,**kwargs)
        else:
            raise RuntimeError("Invalid Finish Condition type: "
                               +finishType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.finishType.toString(1)
        return indentText(res,indention)

class BuffFinishCondBuff(BuffFinishCond,co_basics.HasReference):
    def __init__(self,buff,**kwargs):
        super().__init__(**kwargs)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buff==other.buff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buff.toString(1)
        return indentText(res,indention)

class BuffFinishCondSimple(BuffFinishCond):
    def __init__(self,vals,**kwargs):
        super().__init__(**kwargs)
        self.vals = collections.OrderedDict()
        for elem in [co_attribs.AttribNum.factory(val) for val in vals]:
            self.vals[elem.identifier] = elem

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.vals==other.vals)

    def __getattr__(self,name):
        if name in self.vals:
            return self.vals[name]
        else:
            raise AttributeError("Unknown attribute: "+name)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for val in self.vals.values():
            res.extend(val.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class BuffFinishCondDmg(BuffFinishCondSimple):
    def __init__(self,dmgType,**kwargs):
        super().__init__(**kwargs)
        self.dmgType = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,**dmgType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.dmgType==other.dmgType)

    def toString(self,indention):
        res = super().toString(0)
        res += self.dmgType.toString(1)
        return indentText(res,indention)

class BuffFinishCondResearch(BuffFinishCond,co_basics.HasReference):
    def __init__(self,prereqs,**kwargs):
        super().__init__(**kwargs)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.prereqs==other.prereqs)

    def toString(self,indention):
        res = super().toString(0)
        res += self.prereqs.toString(1)
        return indentText(res,indention)

"""### Attach Effect ###"""

class AttachInfo(co_attribs.Attribute):
    def __init__(self,attachType,**kwargs):
        super().__init__(**kwargs)
        self.attachType = co_attribs.AttribEnum(possibleVals=EFFECT_ATTACH_TYPES,
                **attachType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.attachType==other.attachType)

    @classmethod
    def factory(cls,attachType,**kwargs):
        if attachType["val"] in EFFECT_ATTACH_TYPES_SIMPLE:
            return AttachInfo(attachType=attachType,**kwargs)
        if attachType["val"]=="Ability":
            return AttachInfoAbility(attachType=attachType,**kwargs)
        else:
            raise RuntimeError("Invalid Effect Attach Type: "
                               +attachType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.attachType.toString(1)
        return indentText(res,indention)

class AttachInfoAbility(AttachInfo):
    def __init__(self,index,**kwargs):
        super().__init__(**kwargs)
        self.index = co_attribs.AttribNum(**index)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.index==other.index)

    def toString(self,indention):
        res = super().toString(0)
        res += self.index.toString(1)
        return indentText(res,indention)

class AttachEffect(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,attachInfo,effectSmall,effectMedium,effectLarge,sound,
            **kwargs):
        super().__init__(**kwargs)
        self.attachInfo = AttachInfo.factory(**attachInfo)
        self.effectSmall = par.ParticleRef(**effectSmall)
        self.effectMedium = par.ParticleRef(**effectMedium)
        self.effectLarge = par.ParticleRef(**effectLarge)
        self.sound = audio.SoundRef(canBeEmpty=True,**sound)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.attachInfo==other.attachInfo)
                        and (self.effectSmall==other.effectSmall)
                        and (self.effectMedium==other.effectMedium)
                        and (self.effectLarge==other.effectLarge)
                        and (self.sound==other.sound))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.attachInfo.toString(1)
        res += self.effectSmall.toString(1)
        res += self.effectMedium.toString(1)
        res += self.effectLarge.toString(1)
        res += self.sound.toString(1)
        return indentText(res,indention)

"""### Weapon Effects ###"""

class WeaponEffectSpawnRand(co_attribs.Attribute):
    def __init__(self,spawnType,radiusMin,radiusMax,**kwargs):
        super().__init__(**kwargs)
        self.spawnType = co_attribs.AttribEnum(possibleVals=["CompletelyRandom",
            "RandomFromUp"],**spawnType)
        self.radiusMin = co_attribs.AttribNum(**radiusMin)
        self.radiusMax = co_attribs.AttribNum(**radiusMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.spawnType==other.spawnType)
                        and (self.radiusMin==other.radiusMin)
                        and (self.radiusMax==other.radiusMax))

    @classmethod
    def factory(cls,spawnType,**kwargs):
        if spawnType["val"]=="CompletelyRandom":
            return WeaponEffectSpawnRand(spawnType=spawnType,**kwargs)
        elif spawnType["val"]=="RandomFromUp":
            return WeaponEffectSpawnRandUp(spawnType=spawnType,**kwargs)
        else:
            raise RuntimeError("Unknown Weapon Spawn Type: "+spawnType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.spawnType.toString(1)
        res += self.radiusMin.toString(1)
        res += self.radiusMax.toString(1)
        return indentText(res,indention)

class WeaponEffectSpawnRandUp(WeaponEffectSpawnRand):
    def __init__(self,angleVar,**kwargs):
        super().__init__(**kwargs)
        self.angleVar = co_attribs.AttribNum(**angleVar)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.angleVar==other.angleVar)

    def toString(self,indention):
        res = super().toString(0)
        res += self.angleVar.toString(1)
        return indentText(res,indention)

class WeaponEffectOrigin(co_attribs.Attribute):
    def __init__(self,originType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.originType = co_attribs.AttribEnum(possibleVals=["Target","Targetter"],
                **originType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.originType==other.originType)

    @classmethod
    def factory(cls,originType,**kwargs):
        if originType["val"]=="Target":
            return WeaponEffectOriginTarget(originType=originType,**kwargs)
        elif originType["val"]=="Targetter":
            return WeaponEffectOriginTargetter(originType=originType,**kwargs)
        else:
            raise RuntimeError("Invalid Weapon Effect Origin Type: "
                               +originType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.originType.toString(0)
        return indentText(res,indention)

class WeaponEffectOriginTarget(WeaponEffectOrigin):
    def __init__(self,targetCentricEffect,**kwargs):
        super().__init__(**kwargs)
        self.targetCentricEffect = WeaponEffectSpawnRand.factory(
                **targetCentricEffect)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.targetCentricEffect==other.targetCentricEffect)

    def toString(self,indention):
        res = super().toString(0)
        res += self.targetCentricEffect.toString(0)
        return indentText(res,indention)

class WeaponEffectOriginTargetter(WeaponEffectOrigin):
    def __init__(self,effectAttach,**kwargs):
        super().__init__(**kwargs)
        self.effectAttach = AttachInfo.factory(**effectAttach)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.effectAttach==other.effectAttach)

    def toString(self,indention):
        res = super().toString(0)
        res += self.effectAttach.toString(0)
        return indentText(res,indention)

"""### Action Over Time ###"""

class ActionOverTime(co_attribs.Attribute):
    def __init__(self,actionType,**kwargs):
        super().__init__(**kwargs)
        self.actionType = co_attribs.AttribEnum(possibleVals=ACTION_OVER_TIME,
                **actionType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.actionType==other.actionType)

    @classmethod
    def factory(cls,actionType,**kwargs):
        if actionType["val"] in ACTION_OVER_TIME_SIMPLE:
            return ActionOverTimeSimple(actionType=actionType,**kwargs)
        elif actionType["val"] in ACTION_OVER_TIME_DO_DMG:
            return ActionOverTimeDoDmg(actionType=actionType,**kwargs)
        elif actionType["val"]=="DrainAntiMatterAndDoDamage":
            return ActionOverTimeDoDmgDrainAM(actionType=actionType,**kwargs)
        elif actionType["val"]=="Magnetize":
            return ActionOverTimeMagnetize(actionType=actionType,**kwargs)
        else:
            raise RuntimeError("Invalid Action Type for Overtime Action: "
                               +actionType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.actionType.toString(1)
        return indentText(res,indention)

class ActionOverTimeSimple(ActionOverTime):
    def __init__(self,vals,**kwargs):
        super().__init__(**kwargs)
        self.vals = collections.OrderedDict()
        for elem in [co_attribs.AttribNum.factory(val) for val in vals]:
            self.vals[elem.identifier] = elem

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.vals==other.vals)

    def __getattr__(self,name):
        if name in self.vals:
            return self.vals[name]
        else:
            raise AttributeError("Unknown attribute: "+name)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for val in self.vals.values():
            res.extend(val.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionOverTimeDoDmg(ActionOverTimeSimple):
    def __init__(self,dmgAffect,dmgType,dmgShared,**kwargs):
        super().__init__(**kwargs)
        self.dmgAffect = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,
                **dmgAffect)
        self.dmgType = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,**dmgType)
        self.dmgShared = co_attribs.AttribBool(**dmgShared)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.dmgAffect==other.dmgAffect)
                        and (self.dmgType==other.dmgType)
                        and (self.dmgShared==other.dmgShared))

    def toString(self,indention):
        res = super().toString(0)
        res += self.dmgAffect.toString(1)
        res += self.dmgType.toString(1)
        res += self.dmgShared.toString(1)
        return indentText(res,indention)

class ActionOverTimeDoDmgDrainAM(ActionOverTimeSimple):
    def __init__(self,dmgShared,**kwargs):
        super().__init__(**kwargs)
        self.dmgShared = co_attribs.AttribBool(**dmgShared)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.dmgShared==other.dmgShared)

    def toString(self,indention):
        res = super().toString(0)
        res += self.dmgShared.toString(1)
        return indentText(res,indention)

class ActionOverTimeMagnetize(ActionOverTimeSimple):
    def __init__(self,targetFilter,**kwargs):
        super().__init__(**kwargs)
        self.targetFilter = TargetFilter(**targetFilter)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.targetFilter==other.targetFilter)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.targetFilter.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

"""### Instant Actions ###"""

class InstantAction(co_attribs.Attribute):
    def __init__(self,actionType,trigger,**kwargs):
        super().__init__(**kwargs)
        self.actionType = co_attribs.AttribEnum(possibleVals=INSTANT_ACTION_TYPES,
                **actionType)
        self.trigger = InstantActionTrigger.factory(**trigger)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.actionType==other.actionType)
                        and (self.trigger==other.trigger))

    @classmethod
    def factory(cls,actionType,**kwargs):
        if actionType["val"] in INSTANT_ACTION_SIMPLE:
            return InstantAction(actionType=actionType,**kwargs)
        elif actionType["val"]=="PlayDetachedEffectsInRadius":
            return ActionPlayEffectRadius(actionType=actionType,**kwargs)
        elif actionType["val"]=="GiveCreditsToPlayer":
            return ActionGiveCredits(actionType=actionType,**kwargs)
        elif actionType["val"]=="RuinPlanet":
            return ActionRuinPlanet(actionType=actionType,**kwargs)
        elif actionType["val"]=="CreateFrigateAtArbitraryTarget":
            return ActionCreateFrigateTargetArbitrary(actionType=actionType,
                    **kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_TARGET_RADIUS_TRAVEL:
            return ActionApplyTargetWeap(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_SPAWN_SHIPS:
            return ActionSpawnShips(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_WEAPON_EFFECT:
            return ActionWeaponEffect(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_FRIGATE:
            return ActionCreateFrg(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_FRIGATE_TARGET:
            return ActionCreateFrgTarget(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_FRIGATE_CLONE:
            return ActionCreateClonedFrg(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_STEAL_RESOURCES:
            return ActionStealResources(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CONVERT_DMG_AM:
            return ActionConvertDmgAM(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_PROPAGATE_DMG:
            return ActionPropagateDamage(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_DMG_ENTITY:
            return ActionDoDmgPerEntity(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_SQUAD_MINES:
            return ActionSquadToMines(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_DEBRIS_HULL:
            return ActionDebrisToHull(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CHANGE_PLAYER:
            return ActionChangePlayer(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_STARBASE:
            return ActionCreateStarbase(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_ILLUSION_FIGHTERS:
            return ActionIllusionFighters(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_COLONIZE_PLANET:
            return ActionColonizePlanet(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_CONE:
            return ActionApplyCone(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_ENTRY_VEHICLE:
            return ActionApplyEntryVehicle(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_EFF_LVL:
            return ActionApplyEffNum(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_UNIT:
            return ActionCreateUnit(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_BUFF:
            return ActionApplyBuff(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_EFFECT:
            return ActionWithEffect(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_EFFECT:
            return ActionApplyEffect(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_OR_REMOVE:
            return ActionApplyOrRemove(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_CREATE_SHELL:
            return ActionCreateShell(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_FILTER:
            return ActionApplyFilter(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_EFFECT_FILTER:
            return ActionApplyEffFilter(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_TARGET_WEAP:
            return ActionApplyTargetWeapNum(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_NUM:
            return ActionSimple(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_DO_DMG:
            return ActionDoDamage(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_WEAP:
            return ActionApplyWeap(actionType=actionType,**kwargs)
        elif actionType["val"] in INSTANT_ACTION_APPLY_TARGET_EFF:
            return ActionApplyTargetEff(actionType=actionType,**kwargs)
        else:
            raise RuntimeError("Invalid Instant Action Type: "
                               +actionType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        return indentText(res,indention)

class ActionWeaponEffect(InstantAction,co_basics.HasReference):
    def __init__(self,hasWeapEffect,origin=None,impactOffsetType=None,
            effectHitsHull=None,effectHitsShield=None,weaponEffect=None,
            **kwargs):
        super().__init__(**kwargs)
        self.hasWeapEffect = co_attribs.AttribBool(**hasWeapEffect)
        if self.hasWeapEffect.value:
            self.origin = WeaponEffectOrigin.factory(**origin)
            self.impactOffsetType = co_attribs.AttribEnum(
                    possibleVals=WEAP_EFFECT_IMPACT_OFFSET,
                    **impactOffsetType)
            self.effectHitsHull = co_attribs.AttribBool(**effectHitsHull)
            self.effectHitsShield = co_attribs.AttribBool(**effectHitsShield)
            self.weaponEffect = armed.SharedWeaponEffect.factory(**weaponEffect)

    def __eq__(self,other):
        res = super().__eq__(other)
        if self.hasWeapEffect.value:
            res = res and ((self.hasWeapEffect==other.hasWeapEffect)
                           and (self.origin==other.origin)
                           and (self.impactOffsetType==other.impactOffsetType)
                           and (self.effectHitsHull==other.effectHitsHull)
                           and (self.effectHitsShield==other.effectHitsShield)
                           and (self.weaponEffect==other.weaponEffect))
        else:
            res = res and (self.hasWeapEffect==other.hasWeapEffect)
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.hasWeapEffect.toString(1)
        if self.hasWeapEffect.value:
            res += self.origin.toString(1)
            res += self.impactOffsetType.toString(1)
            res += self.effectHitsHull.toString(1)
            res += self.effectHitsShield.toString(1)
            res += self.weaponEffect.toString(1)
        return indentText(res,indention)

class ActionApplyBuff(InstantAction,co_basics.HasReference):
    def __init__(self,buff,**kwargs):
        super().__init__(**kwargs)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buff==other.buff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buff.toString(1)
        return indentText(res,indention)

class ActionWithEffect(InstantAction,co_basics.HasReference):
    def __init__(self,effectInfo,**kwargs):
        super().__init__(**kwargs)
        self.effectInfo = AttachEffect(**effectInfo)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.effectInfo==other.effectInfo)

    def toString(self,indention):
        res = super().toString(0)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionApplyEffect(ActionApplyBuff,ActionWithEffect):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionApplyOrRemove(ActionApplyEffect):
    def __init__(self,toggleOnName,toggleOnDesc,**kwargs):
        super().__init__(**kwargs)
        self.toggleOnName = ui.StringReference(**toggleOnName)
        self.toggleOnDesc = ui.StringReference(**toggleOnDesc)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.toggleOnName==other.toggleOnName)
                        and (self.toggleOnDesc==other.toggleOnDesc))

    def toString(self,indention):
        res = super().toString(0)
        res += self.toggleOnName.toString(1)
        res += self.toggleOnDesc.toString(1)
        return indentText(res,indention)

class ActionCreateShell(ActionWithEffect):
    def __init__(self,entity,**kwargs):
        super().__init__(**kwargs)
        self.entity = coe.EntityRef(types=["CannonShell"],**entity)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.entity==other.entity)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.entity.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionApplyFilter(ActionApplyBuff):
    def __init__(self,targetFilter,**kwargs):
        super().__init__(**kwargs)
        self.targetFilter = TargetFilter(**targetFilter)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.targetFilter==other.targetFilter)

    def toString(self,indention):
        res = super().toString(0)
        res += self.targetFilter.toString(1)
        return indentText(res,indention)

class ActionApplyEffFilter(ActionApplyFilter,ActionApplyEffect):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.targetFilter.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionSimple(InstantAction):
    def __init__(self,vals,**kwargs):
        super().__init__(**kwargs)
        self.vals = collections.OrderedDict()
        for elem in [co_attribs.AttribNum.factory(val) for val in vals]:
            self.vals[elem.identifier] = elem

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.vals==other.vals)

    def __getattr__(self,name):
        if name in self.vals:
            return self.vals[name]
        else:
            raise AttributeError("Unknown attribute: "+name)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for val in self.vals.values():
            res.extend(val.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionApplyEffNum(ActionSimple,ActionApplyEffect):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionApplyTargetEff(ActionApplyFilter,ActionApplyEffNum):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.targetFilter.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionApplyEntryVehicle(ActionApplyEffFilter,ActionApplyTargetEff):
    def __init__(self,entryVehicle,range,**kwargs):
        super().__init__(**kwargs)
        self.entryVehicle = coe.EntityRef(types=["EntryVehicle"],**entryVehicle)
        self.range = co_attribs.AttribFixedLevelable(**range)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.entryVehicle==other.entryVehicle)
                        and (self.range==other.range))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.targetFilter.toString(1)
        res += self.range.toString(1)
        res += self.entryVehicle.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionApplyCone(ActionApplyTargetEff):
    def __init__(self,weapBank,**kwargs):
        super().__init__(**kwargs)
        self.weapBank = co_attribs.AttribEnum(possibleVals=armed.WEAPON_BANK,
                **weapBank)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.weapBank==other.weapBank)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.targetFilter.toString(1)
        res += self.weapBank.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionGiveCredits(ActionSimple,ActionWithEffect):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionPlayEffectRadius(ActionSimple,ActionWithEffect):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.effectInfo.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionColonizePlanet(ActionWithEffect):
    def __init__(self,targetFilter,range,delay,entryVehicles,
            numEntryVehicles,travelTime,afterColonizeBuff,**kwargs):
        super().__init__(**kwargs)
        self.targetFilter = TargetFilter(**targetFilter)
        self.range = co_attribs.AttribFixedLevelable(**range)
        self.delay = co_attribs.AttribNum(**delay)
        self.entryVehicles = coe.EntityRef(types=["EntryVehicle"],
                **entryVehicles)
        self.numEntryVehicles = co_attribs.AttribFixedLevelable(**numEntryVehicles)
        self.travelTime = co_attribs.AttribNum(**travelTime)
        self.afterColonizeBuff = coe.EntityRef(types=["Buff"],
                **afterColonizeBuff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.targetFilter==other.targetFilter)
                        and (self.range==other.range)
                        and (self.delay==other.delay)
                        and (self.entryVehicles==other.entryVehicles)
                        and (self.numEntryVehicles==other.numEntryVehicles)
                        and (self.travelTime==other.travelTime)
                        and (self.afterColonizeBuff==other.afterColonizeBuff))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.targetFilter.toString(1)
        res += self.range.toString(1)
        res += self.delay.toString(1)
        res += self.entryVehicles.toString(1)
        res += self.numEntryVehicles.toString(1)
        res += self.travelTime.toString(1)
        res += self.effectInfo.toString(1)
        res += self.afterColonizeBuff.toString(1)
        return indentText(res,indention)

class ActionIllusionFighters(ActionSimple,ActionWithEffect):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.effectInfo.toString(1)
        return indentText(res,indention)

class ActionCreateStarbase(ActionSimple,co_basics.HasReference):
    def __init__(self,entity,**kwargs):
        super().__init__(**kwargs)
        self.entity = coe.EntityRef(types=["StarBase"],**entity)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.entity==other.entity)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.entity.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionChangePlayer(ActionSimple):
    def __init__(self,permanent,failIfLackingSupply,**kwargs):
        super().__init__(**kwargs)
        self.permanent = co_attribs.AttribBool(**permanent)
        self.failIfLackingSupply = co_attribs.AttribBool(**failIfLackingSupply)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.permanent==other.permanent)
                        and (
                        self.failIfLackingSupply==other.failIfLackingSupply))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.permanent.toString(1)
        res += self.failIfLackingSupply.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionSquadToMines(ActionSimple,co_basics.HasReference):
    def __init__(self,entity,**kwargs):
        super().__init__(**kwargs)
        self.entity = coe.EntityRef(types=["SpaceMine"],**entity)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.entity==other.entity)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.entity.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionDoDamage(ActionSimple):
    def __init__(self,dmgAffect,dmgType,isShared,**kwargs):
        super().__init__(**kwargs)
        self.dmgAffect = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,
                **dmgAffect)
        self.dmgType = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,
                **dmgType)
        self.isShared = co_attribs.AttribBool(**isShared)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.dmgAffect==other.dmgAffect)
                        and (self.dmgType==other.dmgType)
                        and (self.isShared==other.isShared))

    def toString(self,indention):
        res = super().toString(0)
        res += self.dmgAffect.toString(1)
        res += self.dmgType.toString(1)
        res += self.isShared.toString(1)
        return indentText(res,indention)

class ActionDoDmgPerEntity(ActionDoDamage):
    def __init__(self,targetFilter,range,maxTargets,**kwargs):
        super().__init__(**kwargs)
        self.targetFilter = TargetFilter(**targetFilter)
        self.range = co_attribs.AttribFixedLevelable(**range)
        self.maxTargets = co_attribs.AttribFixedLevelable(**maxTargets)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.targetFilter==other.targetFilter)
                        and (self.range==other.range)
                        and (self.maxTargets==other.maxTargets))

    def toString(self,indention):
        res = super().toString(0)
        res += self.targetFilter.toString(1)
        res += self.range.toString(1)
        res += self.maxTargets.toString(1)
        return indentText(res,indention)

class ActionDebrisToHull(ActionSimple,co_basics.HasReference):
    def __init__(self,cleanupSounds,**kwargs):
        super().__init__(**kwargs)
        self.cleanupSounds = co_attribs.AttribList(elemType=audio.SoundRef,
                **cleanupSounds)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.cleanupSounds==other.cleanupSounds)

    def toString(self,indention):
        res = super().toString(0)
        res += self.cleanupSounds.toString(1)
        return indentText(res,indention)

class ActionPropagateDamage(ActionSimple):
    def __init__(self,targetFilter,**kwargs):
        super().__init__(**kwargs)
        self.targetFilter = TargetFilter(**targetFilter)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.targetFilter==other.targetFilter)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.targetFilter.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionStealResources(ActionSimple):
    def __init__(self,resource,**kwargs):
        super().__init__(**kwargs)
        self.resource = co_attribs.AttribEnum(possibleVals=RESOURCE_TYPES,**resource)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.resource==other.resource)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.resource.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionConvertDmgAM(ActionSimple):
    def __init__(self,dmgType,**kwargs):
        super().__init__(**kwargs)
        self.dmgType = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,**dmgType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.dmgType==other.dmgType)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.dmgType.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        return indentText(res,indention)

class ActionCreateClonedFrg(ActionSimple,co_basics.HasReference):
    def __init__(self,spawnSound,**kwargs):
        super().__init__(**kwargs)
        self.spawnSound = audio.SoundRef(**spawnSound)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.spawnSound==other.spawnSound)

    def toString(self,indention):
        res = super().toString(0)
        res += self.spawnSound.toString(1)
        return indentText(res,indention)

class ActionCreateUnit(ActionCreateClonedFrg,co_basics.HasReference):
    def __init__(self,entity,vals=[],**kwargs):
        super().__init__(vals=vals,**kwargs)
        if self.actionType.value in ["CreateFrigate","CreateFrigateAtTarget",
                "CreateFrigateAtArbitraryTarget"]:
            self.entity = coe.EntityRef(types=["Frigate"],**entity)
        elif self.actionType.value=="CreatePlanetModule":
            self.entity = coe.EntityRef(types=["PlanetModuleHangarDefense",
                "PlanetModuleRefinery","PlanetModuleShipFactory",
                "PlanetModuleStandard","PlanetModuleTradePort",
                "PlanetModuleWeaponDefense"],**entity)
        elif self.actionType.value=="CreateSpaceMine":
            self.entity = coe.EntityRef(types=["SpaceMine"],**entity)
        elif self.actionType.value=="CreateSquad":
            self.entity = coe.EntityRef(types=["Squad"],**entity)
        else:
            raise RuntimeError("Invalid Action Type for ActionCreateUnit: "
                               +self.actionType.value)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.entity==other.entity)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.entity.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.spawnSound.toString(1)
        return indentText(res,indention)

class ActionCreateFrigateTargetArbitrary(ActionCreateUnit):
    def __init__(self,range,**kwargs):
        super().__init__(**kwargs)
        self.range = co_attribs.AttribFixedLevelable(**range)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.range==other.range)

    def toString(self,indention):
        res = super().toString(0)
        res += self.range.toString(1)
        return indentText(res,indention)

class ActionCreateFrg(ActionCreateUnit):
    def __init__(self,numFrigates,matchOwnerDmg,postSpawnBuff,**kwargs):
        super().__init__(**kwargs)
        self.numFrigates = co_attribs.AttribFixedLevelable(**numFrigates)
        self.matchOwnerDmg = co_attribs.AttribBool(**matchOwnerDmg)
        self.postSpawnBuff = coe.EntityRef(types=["Buff"],**postSpawnBuff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.numFrigates==other.numFrigates)
                        and (self.matchOwnerDmg==other.matchOwnerDmg)
                        and (self.postSpawnBuff==other.postSpawnBuff))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.entity.toString(1)
        res += self.numFrigates.toString(1)
        res += self.matchOwnerDmg.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.spawnSound.toString(1)
        res += self.postSpawnBuff.toString(1)
        return indentText(res,indention)

class ActionCreateFrgTarget(ActionCreateUnit):
    def __init__(self,targetFilter,range,**kwargs):
        super().__init__(**kwargs)
        self.targetFilter = TargetFilter(**targetFilter)
        self.range = co_attribs.AttribFixedLevelable(**range)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.targetFilter==other.targetFilter)
                        and (self.range==other.range))

    def toString(self,indention):
        res = super().toString(0)
        res += self.targetFilter.toString(1)
        res += self.range.toString(1)
        return indentText(res,indention)

class ActionApplyWeap(ActionWeaponEffect,ActionSimple,ActionApplyBuff):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.hasWeapEffect.toString(1)
        if self.hasWeapEffect.value:
            res += self.origin.toString(1)
            res += self.impactOffsetType.toString(1)
            res += self.effectHitsHull.toString(1)
            res += self.effectHitsShield.toString(1)
            res += self.weaponEffect.toString(1)
        return indentText(res,indention)

class ActionApplyTargetWeap(ActionApplyEffFilter,ActionWeaponEffect):
    def __init__(self,range,maxTargets,travelSpeed,staggerDelay,**kwargs):
        super().__init__(**kwargs)
        self.range = co_attribs.AttribFixedLevelable(**range)
        self.maxTargets = co_attribs.AttribFixedLevelable(**maxTargets)
        self.travelSpeed = co_attribs.AttribNum(**travelSpeed)
        self.staggerDelay = co_attribs.AttribNum(**staggerDelay)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.range==other.range)
                        and (self.maxTargets==other.maxTargets)
                        and (self.travelSpeed==other.travelSpeed)
                        and (self.staggerDelay==other.staggerDelay))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.targetFilter.toString(1)
        res += self.range.toString(1)
        res += self.maxTargets.toString(1)
        res += self.effectInfo.toString(1)
        res += self.travelSpeed.toString(1)
        res += self.staggerDelay.toString(1)
        res += self.hasWeapEffect.toString(1)
        if self.hasWeapEffect.value:
            res += self.origin.toString(1)
            res += self.impactOffsetType.toString(1)
            res += self.effectHitsHull.toString(1)
            res += self.effectHitsShield.toString(1)
            res += self.weaponEffect.toString(1)
        return indentText(res,indention)

class ActionApplyTargetWeapNum(ActionApplyWeap,ActionApplyFilter):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.buff.toString(1)
        res += self.targetFilter.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.hasWeapEffect.toString(1)
        if self.hasWeapEffect.value:
            res += self.origin.toString(1)
            res += self.impactOffsetType.toString(1)
            res += self.effectHitsHull.toString(1)
            res += self.effectHitsShield.toString(1)
            res += self.weaponEffect.toString(1)
        return indentText(res,indention)

class ActionRuinPlanet(ActionSimple,ActionApplyFilter):
    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.targetFilter.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.buff.toString(1)
        return indentText(res,indention)

class ActionSpawnShips(ActionSimple,co_basics.HasReference):
    def __init__(self,ships,hyperSpawnType,**kwargs):
        super().__init__(**kwargs)
        self.ships = co_attribs.AttribList(elemType=consts.ShipSpawn,**ships)
        self.hyperSpawnType = co_attribs.AttribEnum(possibleVals=SPAWN_SHIPS_TYPE,
                **hyperSpawnType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.ships==other.ships)
                        and (self.hyperSpawnType==other.hyperSpawnType))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.actionType.toString(1)
        res += self.trigger.toString(1)
        res += self.ships.toString(1)
        for val in self.vals.values():
            res += val.toString(1)
        res += self.hyperSpawnType.toString(1)
        return indentText(res,indention)

"""### Periodic Actions ###"""

class ActionCount(co_attribs.Attribute):
    def __init__(self,countType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.countType = co_attribs.AttribEnum(possibleVals=["Infinite","Finite"],
                **countType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.countType==other.countType)

    @classmethod
    def factory(cls,countType,**kwargs):
        if countType["val"]=="Infinite":
            return ActionCount(countType=countType,**kwargs)
        elif countType["val"]=="Finite":
            return ActionCountFinite(countType=countType,**kwargs)
        else:
            raise RuntimeError("Invalid countType for periodic action: "
                               +countType["val"])

    def toString(self,indention):
        res = self.countType.toString(0)
        return indentText(res,indention)

class ActionCountFinite(ActionCount):
    def __init__(self,count,**kwargs):
        super().__init__(**kwargs)
        self.count = co_attribs.AttribFixedLevelable(**count)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.count==other.count)

    def toString(self,indention):
        res = super().toString(0)
        res += self.count.toString(0)
        return indentText(res,indention)

class PeriodicAction(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,count,interval,instantAction,**kwargs):
        super().__init__(**kwargs)
        self.count = ActionCount.factory(**count)
        self.interval = co_attribs.AttribFixedLevelable(**interval)
        self.instantAction = InstantAction.factory(identifier="%remove%",
                **instantAction)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.count==other.count)
                        and (self.interval==other.interval)
                        and (self.instantAction==other.instantAction))

    def toString(self,indention):
        periodicHeader = self.identifier+"\n"
        periodicHeader += self.count.toString(1)
        periodicHeader += self.interval.toString(1)
        actionString = self.instantAction.toString(0)
        res = actionString.replace("%remove%\n",periodicHeader)
        return indentText(res,indention)

"""### Level Sources ###"""

class LevelSource(co_attribs.Attribute):
    def __init__(self,sourceType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.sourceType = co_attribs.AttribEnum(possibleVals=LVL_SOURCE,**sourceType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.sourceType==other.sourceType)

    @classmethod
    def factory(cls,sourceType,**kwargs):
        if sourceType["val"] in LVL_SOURCE_SIMPLE:
            return LevelSource(sourceType=sourceType,**kwargs)
        elif sourceType["val"] in LVL_SOURCE_INTRINSIC:
            return LevelSourceIntrinsic(sourceType=sourceType,**kwargs)
        elif sourceType["val"] in LVL_SOURCE_RESEARCH_NO_BASE:
            return LevelSourceResearch(sourceType=sourceType,**kwargs)
        elif sourceType["val"] in LVL_SOURCE_RESEARCH_BASE:
            return LevelSourceResearchBase(sourceType=sourceType,**kwargs)
        elif sourceType["val"] in LVL_SOURCE_UPGRADE:
            return LevelSourceUpgrade(sourceType=sourceType,**kwargs)
        else:
            raise RuntimeError("Invalid source type for Level Source: "
                               +sourceType["val"])

    def toString(self,indention):
        res = self.sourceType.toString(0)
        return indentText(res,indention)

class LevelSourceIntrinsic(LevelSource):
    def __init__(self,minExpForLvl,**kwargs):
        super().__init__(**kwargs)
        self.minExpForLvl = co_attribs.AttribFixedLevelable(**minExpForLvl)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.minExpForLvl==other.minExpForLvl)

    def toString(self,indention):
        res = super().toString(0)
        res += self.minExpForLvl.toString(0)
        return indentText(res,indention)

class LevelSourceResearch(LevelSource,co_basics.HasReference):
    def __init__(self,subject,**kwargs):
        super().__init__(**kwargs)
        self.subject = coe.EntityRef(types=["ResearchSubject"],**subject)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.subject==other.subject)

    def toString(self,indention):
        res = super().toString(0)
        res += self.subject.toString(0)
        return indentText(res,indention)

class LevelSourceResearchBase(LevelSourceResearch):
    def __init__(self,base,**kwargs):
        super().__init__(**kwargs)
        self.base = coe.EntityRef(types=["ResearchSubject"],**base)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.base==other.base)

    def toString(self,indention):
        res = self.sourceType.toString(0)
        res += self.base.toString(0)
        res += self.subject.toString(0)
        return indentText(res,indention)

class LevelSourceUpgrade(LevelSource):
    def __init__(self,sourceProperty,**kwargs):
        super().__init__(**kwargs)
        self.sourceProperty = co_attribs.AttribEnum(possibleVals=SOURCE_PROPERTIES,
                **sourceProperty)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.sourceProperty==other.sourceProperty)

    def toString(self,indention):
        res = super().toString(0)
        res += self.sourceProperty.toString(0)
        return indentText(res,indention)

"""### AI Use Time ###"""

class AIUseTime(co_attribs.Attribute):
    def __init__(self,aiUseType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.aiUseType = co_attribs.AttribEnum(possibleVals=AI_USE_TIME,**aiUseType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.aiUseType==other.aiUseType)

    @classmethod
    def factory(cls,aiUseType,**kwargs):
        if aiUseType["val"] in AI_USE_TIME_SIMPLE:
            return AIUseTime(aiUseType=aiUseType,**kwargs)
        elif aiUseType["val"] in AI_USE_TIME_VAL:
            return AIUseTimeVal(aiUseType=aiUseType,**kwargs)
        elif aiUseType["val"] in AI_USE_TIME_TARGETS_NOT_BUFFED:
            return AIUseTimeTargetsNotBuffed(aiUseType=aiUseType,**kwargs)
        elif aiUseType["val"] in AI_USE_TIME_MANY_TARGETS_BANK_RANGE:
            return AIUseTimeTargetsRangeBank(aiUseType=aiUseType,**kwargs)
        else:
            raise RuntimeError("Invalid ai use type for Ability:"
                               +aiUseType["val"])

    def toString(self,indention):
        res = self.aiUseType.toString(0)
        return indentText(res,indention)

class AIUseTimeVal(AIUseTime):
    def __init__(self,val,**kwargs):
        super().__init__(**kwargs)
        self.val = co_attribs.AttribNum.factory(val)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.val==other.val)

    def toString(self,indention):
        res = super().toString(0)
        res += self.val.toString(0)
        return indentText(res,indention)

class AIUseTimeTargetsNotBuffed(AIUseTimeVal,co_basics.HasReference):
    def __init__(self,buff,**kwargs):
        super().__init__(**kwargs)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buff==other.buff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buff.toString(0)
        return indentText(res,indention)

class AIUseTimeTargetsRangeBank(AIUseTimeVal):
    def __init__(self,bank,**kwargs):
        super().__init__(**kwargs)
        self.bank = co_attribs.AttribEnum(possibleVals=armed.WEAPON_BANK,**bank)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.bank==other.bank)

    def toString(self,indention):
        res = super().toString(0)
        res += self.bank.toString(0)
        return indentText(res,indention)

"""### AI Use Target ###"""

class AIUseTarget(co_attribs.Attribute):
    def __init__(self,aiUseTargetType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.aiUseTargetType = co_attribs.AttribEnum(possibleVals=AI_USE_TARGET,
                **aiUseTargetType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.aiUseTargetType==other.aiUseTargetType)

    @classmethod
    def factory(cls,aiUseTargetType,**kwargs):
        if aiUseTargetType["val"] in AI_USE_TARGET_SIMPLE:
            return AIUseTarget(aiUseTargetType=aiUseTargetType,**kwargs)
        elif aiUseTargetType["val"] in AI_USE_TARGET_VAL:
            return AIUseTargetVal(aiUseTargetType=aiUseTargetType,**kwargs)
        elif aiUseTargetType["val"] in AI_USE_TARGET_BUFF:
            return AIUseTargetHasBuff(aiUseTargetType=aiUseTargetType,**kwargs)
        else:
            raise RuntimeError("Invalid AI Use Target condition: "
                               +aiUseTargetType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.aiUseTargetType.toString(0)
        return indentText(res,indention)

class AIUseTargetVal(AIUseTarget):
    def __init__(self,val,**kwargs):
        super().__init__(**kwargs)
        self.val = co_attribs.AttribFixedLevelable(**val)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.val==other.val)

    def toString(self,indention):
        res = super().toString(0)
        res += self.val.toString(0)
        return indentText(res,indention)

class AIUseTargetHasBuff(AIUseTarget,co_basics.HasReference):
    def __init__(self,buff,**kwargs):
        super().__init__(**kwargs)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buff==other.buff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buff.toString(0)
        return indentText(res,indention)

"""### Use Costs ###"""

class AbilityUseCost(co_attribs.Attribute):
    def __init__(self,costType,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.costType = co_attribs.AttribEnum(possibleVals=COST_TYPES,**costType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.costType==other.costType)

    @classmethod
    def factory(cls,costType,**kwargs):
        if costType["val"] in COST_TYPE_PASSIVE:
            return AbilityUseCost(costType=costType,**kwargs)
        elif costType["val"] in COST_TYPE_NONE:
            return AbilityUseCostActive(costType=costType,**kwargs)
        elif costType["val"] in COST_TYPE_AM:
            return AbilityUseCostAntimatter(costType=costType,**kwargs)
        elif costType["val"] in COST_TYPE_AM_HULL:
            return AbilityUseCostAMHull(costType=costType,**kwargs)
        elif costType["val"] in COST_TYPE_RESOURCES:
            return AbilityUseCostResources(costType=costType,**kwargs)
        elif costType["val"] in COST_TYPE_RESOURCE_SUPPLY:
            return AbilityUseCostResourcesSupply(costType=costType,**kwargs)
        elif costType["val"] in COST_TYPE_RESOURCE_CAPITAL:
            return AbilityUseCostResourcesCapital(costType=costType,**kwargs)
        else:
            raise RuntimeError("Invalid cost type for ability use cost: "
                               +costType["val"])

    def toString(self,indention):
        res = super().toString(0)
        res += self.costType.toString(0)
        return indentText(res,indention)

class AbilityUseCostActive(AbilityUseCost):
    def __init__(self,cooldown,orderAcknowledge,**kwargs):
        super().__init__(**kwargs)
        self.cooldown = co_attribs.AttribFixedLevelable(**cooldown)
        self.orderAcknowledge = co_attribs.AttribEnum(possibleVals=ORDER_ACKNOWLEDGE,
                **orderAcknowledge)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.cooldown==other.cooldown)
                        and (self.orderAcknowledge==other.orderAcknowledge))

    def toString(self,indention):
        res = super().toString(0)
        res += self.cooldown.toString(0)
        res += self.orderAcknowledge.toString(0)
        return indentText(res,indention)

class AbilityUseCostAntimatter(AbilityUseCostActive):
    def __init__(self,antimatterCost,**kwargs):
        super().__init__(**kwargs)
        self.antimatterCost = co_attribs.AttribFixedLevelable(**antimatterCost)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.antimatterCost==other.antimatterCost)

    def toString(self,indention):
        res = self.costType.toString(0)
        res += self.antimatterCost.toString(0)
        res += self.cooldown.toString(0)
        res += self.orderAcknowledge.toString(0)
        return indentText(res,indention)

class AbilityUseCostAMHull(AbilityUseCostAntimatter):
    def __init__(self,hullCost,**kwargs):
        super().__init__(**kwargs)
        self.hullCost = co_attribs.AttribFixedLevelable(**hullCost)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.hullCost==other.hullCost)

    def toString(self,indention):
        res = self.costType.toString(0)
        res += self.hullCost.toString(0)
        res += self.antimatterCost.toString(0)
        res += self.cooldown.toString(0)
        res += self.orderAcknowledge.toString(0)
        return indentText(res,indention)

class AbilityUseCostResources(AbilityUseCostActive):
    def __init__(self,resources,**kwargs):
        super().__init__(**kwargs)
        self.resources = coe.Cost(**resources)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.resources==other.resources)

    def toString(self,indention):
        res = self.costType.toString(0)
        res += self.resources.toString(0)
        res += self.cooldown.toString(0)
        res += self.orderAcknowledge.toString(0)
        return indentText(res,indention)

class AbilityUseCostResourcesSupply(AbilityUseCostResources):
    def __init__(self,reqSupply,**kwargs):
        super().__init__(**kwargs)
        self.reqSupply = co_attribs.AttribFixedLevelable(**reqSupply)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.reqSupply==other.reqSupply)

    def toString(self,indention):
        res = self.costType.toString(0)
        res += self.resources.toString(0)
        res += self.reqSupply.toString(0)
        res += self.cooldown.toString(0)
        res += self.orderAcknowledge.toString(0)
        return indentText(res,indention)

class AbilityUseCostResourcesCapital(AbilityUseCostResourcesSupply):
    def __init__(self,reqCapCrews,**kwargs):
        super().__init__(**kwargs)
        self.reqCapCrews = co_attribs.AttribFixedLevelable(**reqCapCrews)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.reqCapCrews==other.reqCapCrews)

    def toString(self,indention):
        res = self.costType.toString(0)
        res += self.resources.toString(0)
        res += self.reqSupply.toString(0)
        res += self.reqCapCrews.toString(0)
        res += self.cooldown.toString(0)
        res += self.orderAcknowledge.toString(0)
        return indentText(res,indention)

"""############################# File Classes ##############################"""

class Buff(coe.Entity,co_basics.HasReference):
    parser = g_BuffEntity

    def __init__(self,onReapply,stackingLimitType,stackingLimit,
            exclusivityForAI,interruptable,channeling,instantActions,
            periodicActions,actionsOverTime,entityMods,entityModsBool,
            finishConditions,allowFirstSpawnerStack=None,**kwargs):
        super().__init__(**kwargs)
        self.onReapply = co_attribs.AttribEnum(possibleVals=ON_REAPPLY_DUPLICATE,
                **onReapply)
        self.stackingLimitType = co_attribs.AttribEnum(possibleVals=STACKING_LIMITS,
                **stackingLimitType)
        self.stackingLimit = co_attribs.AttribNum(**stackingLimit)
        if self.stackingLimit.value!=-1:
            self.allowFirstSpawnerStack = co_attribs.AttribBool(
                    **allowFirstSpawnerStack)
        self.exclusivityForAI = co_attribs.AttribEnum(possibleVals=BUFF_EXCLUSIVITY,
                **exclusivityForAI)
        self.interruptable = co_attribs.AttribBool(**interruptable)
        self.channeling = co_attribs.AttribBool(**channeling)
        self.instantActions = co_attribs.AttribList(elemType=InstantAction,
                factory=True,
                **instantActions)
        self.periodicActions = co_attribs.AttribList(elemType=PeriodicAction,
                **periodicActions)
        self.actionsOverTime = co_attribs.AttribList(elemType=ActionOverTime,
                factory=True,
                **actionsOverTime)
        self.entityMods = co_attribs.AttribList(elemType=BuffEntityModifier,
                **entityMods)
        self.entityModsBool = co_attribs.AttribList(elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":BUFF_ENTITY_MODIFIERS_BOOL},
                **entityModsBool)
        self.finishConditions = co_attribs.AttribList(elemType=BuffFinishCond,
                factory=True,
                **finishConditions)

    def __eq__(self,other):
        res = super().__eq__(other)
        res = res and ((self.onReapply==other.onReapply)
                       and (self.stackingLimitType==other.stackingLimitType)
                       and (self.stackingLimit==other.stackingLimit)
                       and (self.exclusivityForAI==other.exclusivityForAI)
                       and (self.interruptable==other.interruptable)
                       and (self.channeling==other.channeling)
                       and (self.instantActions==other.instantActions)
                       and (self.periodicActions==other.periodicActions)
                       and (self.actionsOverTime==other.actionsOverTime)
                       and (self.entityMods==other.entityMods)
                       and (self.entityModsBool==other.entityModsBool)
                       and (self.finishConditions==other.finishConditions))
        if self.stackingLimit.value!=-1 and res:
            res = res and (
                self.allowFirstSpawnerStack==other.allowFirstSpawnerStack)
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.onReapply.toString(0)
        res += self.stackingLimitType.toString(0)
        res += self.stackingLimit.toString(0)
        if self.stackingLimit.value!=-1:
            res += self.allowFirstSpawnerStack.toString(0)
        res += self.exclusivityForAI.toString(0)
        res += self.interruptable.toString(0)
        res += self.channeling.toString(0)
        res += self.instantActions.toString(0)
        res += self.periodicActions.toString(0)
        res += self.actionsOverTime.toString(0)
        res += self.entityMods.toString(0)
        res += self.entityModsBool.toString(0)
        res += self.finishConditions.toString(0)
        return indentText(res,indention)

    def outputBuffchain(self,currMod,indention,seen=set()):
        res = os.path.basename(self.filepath)+":\n"
        if res not in seen:
            seen.add(res)
            refs = []
            for action in it.chain(
                    self.instantActions.elements,
                    [elem.instantAction for elem in
                        self.periodicActions.elements]):
                if isinstance(action,ActionApplyBuff):
                    refs.append(action.buff.ref)
                elif isinstance(action,ActionColonizePlanet):
                    refs.append(action.afterColonizeBuff.ref)
                elif isinstance(action,ActionCreateFrg):
                    refs.append(action.postSpawnBuff.ref)

            ref = None
            probs = []
            for ref in sorted(refs):
                buff = currMod.entities.getEntity(ref)
                if isinstance(buff,list):
                    probs.extend(buff)
                else:
                    res += buff.outputBuffchain(currMod,indention+1,seen)
            prob = None
            for prob in probs:
                res += prob.toString(indention+1)
            if not ref and not prob:
                res += "\tNo Buffs Referenced\n"
        else:
            res += "\tThis Buff has already been displayed in this chain\n"
        return indentText(res,indention)

class Ability(coe.Entity,coe.UIVisible):
    parser = g_AbilityEntity

    def __init__(self,instantAction,faceTarget,canCollideWithTarget,
            moveThruTarget,ultimate,maxLevels,lvlSource,aiUseTime,aiUseTarget,
            canAutocast,isAutocastDefault,pickRandPlanetExplore,ignoreCivShips,
            onlyAutocastWhenDmgPerc,cost,prereqs,**kwargs):
        super().__init__(**kwargs)
        # smallHudIcon is not actually used by Ability entities, any problems 
        # in that attribute can be safely ignored.
        self.iconHudSmall.ignore = True
        self.instantAction = InstantAction.factory(identifier="",
                **instantAction)
        self.faceTarget = co_attribs.AttribBool(**faceTarget)
        self.canCollideWithTarget = co_attribs.AttribBool(**canCollideWithTarget)
        self.moveThruTarget = co_attribs.AttribBool(**moveThruTarget)
        self.ultimate = co_attribs.AttribBool(**ultimate)
        self.maxLevels = co_attribs.AttribNum(**maxLevels)
        self.lvlSource = LevelSource.factory(**lvlSource)
        self.aiUseTime = AIUseTime.factory(**aiUseTime)
        self.aiUseTarget = AIUseTarget.factory(**aiUseTarget)
        self.canAutocast = co_attribs.AttribBool(**canAutocast)
        self.isAutocastDefault = co_attribs.AttribBool(**isAutocastDefault)
        self.pickRandPlanetExplore = co_attribs.AttribBool(**pickRandPlanetExplore)
        self.ignoreCivShips = co_attribs.AttribBool(**ignoreCivShips)
        self.onlyAutocastWhenDmgPerc = co_attribs.AttribNum(**onlyAutocastWhenDmgPerc)
        self.cost = AbilityUseCost.factory(**cost)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.instantAction==other.instantAction)
                        and (self.faceTarget==other.faceTarget)
                        and (
                        self.canCollideWithTarget==other.canCollideWithTarget)
                        and (self.moveThruTarget==other.moveThruTarget)
                        and (self.ultimate==other.ultimate)
                        and (self.maxLevels==other.maxLevels)
                        and (self.lvlSource==other.lvlSource)
                        and (self.aiUseTime==other.aiUseTime)
                        and (self.aiUseTarget==other.aiUseTarget)
                        and (self.canAutocast==other.canAutocast)
                        and (self.isAutocastDefault==other.isAutocastDefault)
                        and (
                        self.pickRandPlanetExplore==other.pickRandPlanetExplore)
                        and (self.ignoreCivShips==other.ignoreCivShips)
                        and (
                        self.onlyAutocastWhenDmgPerc==other.onlyAutocastWhenDmgPerc)
                        and (self.cost==other.cost)
                        and (self.prereqs==other.prereqs))

    def outputBuffchain(self,currMod,indention):
        res = os.path.basename(self.filepath)+":\n"
        action = self.instantAction
        ref = None
        if isinstance(action,ActionApplyBuff):
            ref = action.buff.ref
        elif isinstance(action,ActionColonizePlanet):
            ref = action.afterColonizeBuff.ref
        elif isinstance(action,ActionCreateFrg):
            ref = action.postSpawnBuff.ref

        if ref:
            buff = currMod.entities.getEntity(ref)
            if not isinstance(buff,list):
                res += buff.outputBuffchain(currMod,indention+1)
            else:
                for prob in buff:
                    res += prob.toString(indention+1)
        else:
            res += "\tNo Buffs Referenced\n"
        return indentText(res,indention)

    def toString(self,indention):
        res = super().toString(0)
        res += self.instantAction.toString(0)
        res += self.faceTarget.toString(0)
        res += self.canCollideWithTarget.toString(0)
        res += self.moveThruTarget.toString(0)
        res += self.ultimate.toString(0)
        res += self.maxLevels.toString(0)
        res += self.lvlSource.toString(0)
        res += self.aiUseTime.toString(0)
        res += self.aiUseTarget.toString(0)
        res += self.canAutocast.toString(0)
        res += self.isAutocastDefault.toString(0)
        res += self.pickRandPlanetExplore.toString(0)
        res += self.ignoreCivShips.toString(0)
        res += self.onlyAutocastWhenDmgPerc.toString(0)
        res += self.cost.toString(0)
        res += self.prereqs.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        return indentText(res,indention)
