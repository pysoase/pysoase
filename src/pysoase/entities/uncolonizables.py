""" Contains parsers and classes for Asteroid/DustCloudDef files
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.meshes as mesh
import pysoase.mod.ui as ui

"""################################ Parsers ################################"""

"""Asteroid Def"""
g_meshGroup = pp.Group(
        co_parse.genHeading("meshGroup")
        +co_parse.genStringAttrib("name")("name")
        +co_parse.genListAttrib("meshNameCount",
                co_parse.genStringAttrib("meshName"))(
                "meshes")
)
g_cluster = pp.Group(
        co_parse.genHeading("cluster")
        +co_parse.genStringAttrib("meshGroupName")("meshGroup")
        +co_parse.genIntAttrib("asteroidCount")("asteroidCount")
        +co_parse.genDecimalAttrib("minDistance")("distMin")
        +co_parse.genDecimalAttrib("maxDistance")("distMax")
        +co_parse.genDecimalAttrib("maxZOffset")("offsetMax")
        +co_parse.genDecimalAttrib("minAngle")("angleMin")
        +co_parse.genDecimalAttrib("maxAngle")("angleMax")
        +co_parse.genDecimalAttrib("minAngularSpeed")("angularSpeedMin")
        +co_parse.genDecimalAttrib("maxAngularSpeed")("angularSpeedMax")
)
g_asteroidTemplate = pp.Group(
        co_parse.genHeading("template")
        +co_parse.genStringAttrib("name")("name")
        +co_parse.genListAttrib("clusterCount",g_cluster)("clusters")
)
g_AsteroidDef = (
    co_parse.g_txt
    +co_parse.genListAttrib("meshGroupCount",g_meshGroup)("meshes")
    +co_parse.genListAttrib("templateCount",g_asteroidTemplate)("templates")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""DustCloud Def"""
g_textureGroup = pp.Group(
        co_parse.genHeading("textureGroup")
        +co_parse.genStringAttrib("name")("name")
        +co_parse.genListAttrib("textureNameCount",
                co_parse.genStringAttrib("textureName"))("textures")
)
g_clump = pp.Group(
        co_parse.genHeading("clump")
        +co_parse.genIntAttrib("instanceCount")("instanceCount")
        +co_parse.genStringAttrib("textureGroupName")("textureGroup")
        +co_parse.genDecimalAttrib("minDistance")("distMin")
        +co_parse.genDecimalAttrib("maxDistance")("distMax")
        +co_parse.genDecimalAttrib("radius")("radius")
        +co_parse.genDecimalAttrib("minParticleWidth")("particleWidthMin")
        +co_parse.genDecimalAttrib("maxParticleWidth")("particleWidthMax")
        +co_parse.genIntAttrib("minParticles")("particlesMin")
        +co_parse.genIntAttrib("maxParticles")("particlesMax")
)
g_cloudTemplate = pp.Group(
        co_parse.genHeading("template")
        +co_parse.genStringAttrib("name")("name")
        +co_parse.genListAttrib("clumpCount",g_clump)("clumps")
)
g_DustCloudDef = (
    co_parse.g_txt
    +co_parse.genDecimalAttrib("fullVisibilityDistance")("distVisibility")
    +co_parse.genDecimalAttrib("fullAlphaDistance")("distAlpha")
    +co_parse.genListAttrib("textureGroupCount",g_textureGroup)("textures")
    +co_parse.genListAttrib("templateCount",g_cloudTemplate)("templates")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

"""### Asteroid Def ###"""

class MeshGroup(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,meshes,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.meshes = co_attribs.AttribList(elemType=mesh.MeshRef,**meshes)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.meshes==other.meshes))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.meshes.toString(1)
        return indentText(res,indention)

class MeshGroupRef(co_attribs.FileLocalRef):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and self.ref not in self.parent.meshes:
            res.append(co_probs.MissingRefProblem("Asteroid Mesh Group",self.ref,
                    probLine=self.linenum))
        return res

class AsteroidCluster(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,meshGroup,asteroidCount,distMin,distMax,offsetMax,
            angleMin,angleMax,angularSpeedMin,angularSpeedMax,parent,**kwargs):
        super().__init__(**kwargs)
        self.meshGroup = MeshGroupRef(parent=parent,**meshGroup)
        self.asteroidCount = co_attribs.AttribNum(**asteroidCount)
        self.distMin = co_attribs.AttribNum(**distMin)
        self.distMax = co_attribs.AttribNum(**distMax)
        self.offsetMax = co_attribs.AttribNum(**offsetMax)
        self.angleMin = co_attribs.AttribNum(**angleMin)
        self.angleMax = co_attribs.AttribNum(**angleMax)
        self.angularSpeedMin = co_attribs.AttribNum(**angularSpeedMin)
        self.angularSpeedMax = co_attribs.AttribNum(**angularSpeedMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.meshGroup==other.meshGroup)
                        and (self.asteroidCount==other.asteroidCount)
                        and (self.distMin==other.distMin)
                        and (self.distMax==other.distMax)
                        and (self.offsetMax==other.offsetMax)
                        and (self.angleMin==other.angleMin)
                        and (self.angleMax==other.angleMax)
                        and (self.angularSpeedMin==other.angularSpeedMin)
                        and (self.angularSpeedMax==other.angularSpeedMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.meshGroup.toString(1)
        res += self.asteroidCount.toString(1)
        res += self.distMin.toString(1)
        res += self.distMax.toString(1)
        res += self.offsetMax.toString(1)
        res += self.angleMin.toString(1)
        res += self.angleMax.toString(1)
        res += self.angularSpeedMin.toString(1)
        res += self.angularSpeedMax.toString(1)
        return indentText(res,indention)

class AsteroidTemplate(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,clusters,parent,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.clusters = co_attribs.AttribList(elemType=AsteroidCluster,
                elemArgs={"parent":parent},**clusters)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.clusters==other.clusters))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.clusters.toString(1)
        return indentText(res,indention)

class AsteroidRef(co_attribs.AttribRef):
    _REFTYPE = "Asteroid"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and not isinstance(currMod.entities.asteroids,list):
            if self.ref not in currMod.entities.asteroids.templates:
                res.append(co_probs.MissingRefProblem("Asteroid Template",self.ref,
                        probLine=self.linenum))
        return res

"""### Cloud Def ###"""

class TextureGroup(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,textures,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.textures = co_attribs.AttribList(elemType=ui.UITextureRef,**textures)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.textures==other.textures))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.textures.toString(1)
        return indentText(res,indention)

class TextureGroupRef(co_attribs.FileLocalRef):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and self.ref not in self.parent.textures:
            res.append(co_probs.MissingRefProblem("TextureGroup",self.ref,
                    probLine=self.linenum))
        return res

class CloudClump(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,instanceCount,textureGroup,distMin,distMax,radius,
            particleWidthMin,particleWidthMax,particlesMin,particlesMax,parent,
            **kwargs):
        super().__init__(**kwargs)
        self.instanceCount = co_attribs.AttribNum(**instanceCount)
        self.textureGroup = TextureGroupRef(parent=parent,**textureGroup)
        self.distMin = co_attribs.AttribNum(**distMin)
        self.distMax = co_attribs.AttribNum(**distMax)
        self.radius = co_attribs.AttribNum(**radius)
        self.particleWidthMin = co_attribs.AttribNum(**particleWidthMin)
        self.particleWidthMax = co_attribs.AttribNum(**particleWidthMax)
        self.particlesMin = co_attribs.AttribNum(**particlesMin)
        self.particlesMax = co_attribs.AttribNum(**particlesMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.instanceCount==other.instanceCount)
                        and (self.textureGroup==other.textureGroup)
                        and (self.distMin==other.distMin)
                        and (self.distMax==other.distMax)
                        and (self.radius==other.radius)
                        and (self.particleWidthMin==other.particleWidthMin)
                        and (self.particleWidthMax==other.particleWidthMax)
                        and (self.particlesMin==other.particlesMin)
                        and (self.particlesMax==other.particlesMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.instanceCount.toString(1)
        res += self.textureGroup.toString(1)
        res += self.distMin.toString(1)
        res += self.distMax.toString(1)
        res += self.radius.toString(1)
        res += self.particleWidthMin.toString(1)
        res += self.particleWidthMax.toString(1)
        res += self.particlesMin.toString(1)
        res += self.particlesMax.toString(1)
        return indentText(res,indention)

class CloudTemplate(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,clumps,parent,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.clumps = co_attribs.AttribList(elemType=CloudClump,
                elemArgs={"parent":parent},**clumps)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name),
        self.clumps==other.clumps)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.clumps.toString(1)
        return indentText(res,indention)

class CloudRef(co_attribs.AttribRef):
    _REFTYPE = "Cloud"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and not isinstance(currMod.entities.clouds,list):
            if self.ref not in currMod.entities.clouds.templates:
                res.append(co_probs.MissingRefProblem("Cloud Template",self.ref,
                        probLine=self.linenum))
        return res

"""############################## File Classes #############################"""

class AsteroidDef(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_AsteroidDef

    def __init__(self,meshes,templates,**kwargs):
        super().__init__(**kwargs)
        self._meshes = co_attribs.AttribListKeyed(elemType=MeshGroup,
                keyfunc=lambda val:val.name.value,**meshes)
        self._templates = co_attribs.AttribListKeyed(elemType=AsteroidTemplate,
                keyfunc=lambda val:val.name.value,elemArgs={"parent":self},
                **templates)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.meshes==other.meshes)
                        and (self.templates==other.templates))

    @property
    def meshes(self):
        return self._meshes

    @property
    def templates(self):
        return self._templates

    def toString(self,indention):
        res = super().toString(0)
        res += self._meshes.toString(0)
        res += self._templates.toString(0)
        return indentText(res,indention)

class CloudDef(co_basics.IsFile,co_basics.HasReference):
    parser = g_DustCloudDef

    def __init__(self,distVisibility,distAlpha,textures,templates,**kwargs):
        super().__init__(**kwargs)
        self.distVisibility = co_attribs.AttribNum(**distVisibility)
        self.distAlpha = co_attribs.AttribNum(**distAlpha)
        self._textures = co_attribs.AttribListKeyed(elemType=TextureGroup,
                keyfunc=lambda val:val.name.value,**textures)
        self._templates = co_attribs.AttribListKeyed(elemType=CloudTemplate,
                elemArgs={"parent":self},keyfunc=lambda val:val.name.value,
                **templates)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.distVisibility==other.distVisibility)
                        and (self.distAlpha==other.distAlpha)
                        and (self.textures==other.textures)
                        and (self.templates==other.templates))

    @property
    def textures(self):
        return self._textures

    @property
    def templates(self):
        return self._templates

    def toString(self,indention):
        res = super().toString(indention)
        res += self.distVisibility.toString(0)
        res += self.distAlpha.toString(0)
        res += self.textures.toString(0)
        res += self.templates.toString(0)
        return indentText(res,indention)
