""" Contains parsers and classes for ships and other moving objects.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.mod.audio as audio
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""################################# Types #################################"""

CARGO_TYPES = ["TradeCredits","RefineryResources","Invalid"]
CAPITAL_ROLE_TYPES = ["COLONY","Invalid"]
FRIGATE_ROLE_TYPES = ["AbilityCreated","AntiFighter","AntiMine","AntiModule",
    "Cargo","Carrier","Colony","Corvette","Envoy","Heavy","Invalid",
    "Light","Lrm","MineLayer","ModuleConstructor","ModulePointDefense",
    "ModuleRepair","ModuleUtility","Raider","ResourceCapturer","Scout",
    "Siege","StarBaseConstructor","Support"]

"""################################ Parsers ################################"""

def genVarLvlAttrib(keyword):
    return pp.Group(
            co_parse.genHeading(keyword)
            +co_parse.genDecimalAttrib("StartValue")("startVal")
            +co_parse.genDecimalAttrib("ValueIncreasePerLevel")("perLvlInc")
    )

"""### Basic Elements ###"""

g_antimatterRestoreLvl = genVarLvlAttrib("AntiMatterRestoreRate")(
        "antimatterRestore")
g_armorFromExp = genVarLvlAttrib("ArmorPointsFromExperience")("armorPoints")
g_autoJoin = co_parse.genBoolAttrib("autoJoinFleetDefault")("autoJoinFleet")
g_buildTimeCap = co_parse.genDecimalAttrib("BuildTime")("buildTime")
g_commandPointsLvl = genVarLvlAttrib("CommandPoints")("commandPoints")
g_cultureProtectRate = genVarLvlAttrib("CultureProtectRate")(
        "cultureProtectRate")
g_engineSound = co_parse.genStringAttrib("EngineSoundID")("engineSound")
g_hullPointRestoreLvl = genVarLvlAttrib("HullPointRestoreRate")(
        "hullPointRestore")
g_exhaustSys = co_parse.genStringAttrib("ExhaustParticleSystemName")("exhaustSys")
g_maxAMLvl = genVarLvlAttrib("MaxAntiMatter")("antimatterMax")
g_maxHullPointsLvl = genVarLvlAttrib("MaxHullPoints")("hullPointsMax")
g_maxMitigationLvl = genVarLvlAttrib("maxMitigation")("shieldMitigation")
g_maxShieldPointsLvl = genVarLvlAttrib("MaxShieldPoints")("shieldPointsMax")
g_shieldPointsRestoreLvl = genVarLvlAttrib("ShieldPointRestoreRate")(
        "shieldPointRestore")
g_slotCount = co_parse.genDecimalAttrib("slotCount")("slotCount")
g_weapCooldown = genVarLvlAttrib("weaponCooldownDecreasePerc")(
        "weapCooldownDec")
g_weapDamage = genVarLvlAttrib("weaponDamageIncreasePerc")("weapDmgInc")

"""### Bombing ###"""

g_bombingEffect = pp.Group(
        co_parse.genHeading("bombEffectsDef")
        +pp.Or([armed.g_beamWeaponsEffect,armed.g_missileWeaponEffect,
            armed.g_projectileWeaponsEffect])
)("effect")
g_bombingLvlFalse = (
    co_parse.genBoolAttrib("hasBombingLevels",
            co_parse.g_false)("hasLevels")
    +co_parse.genDecimalAttrib("baseDamage")("baseDamage")
    +co_parse.genDecimalAttrib("basePopulationKilled")("basePopKilled")
)
g_bombingLvlTrue = (
    co_parse.genBoolAttrib("hasBombingLevels",
            co_parse.g_true)("hasLevels")
    +coe.genFixedLvlAttrib("baseDamage")("baseDamage")
    +coe.genFixedLvlAttrib("basePopulationKilled")("basePopKilled")
)
g_bombingLvl = pp.Or([g_bombingLvlTrue,g_bombingLvlFalse])
g_canBombFalse = co_parse.genBoolAttrib("canBomb",
        co_parse.g_false)("canBomb")
g_bombardment = pp.Group(
        g_bombingLvl
        +co_parse.genDecimalAttrib("bombingFreqTime")("bombingFreq")
        +co_parse.genDecimalAttrib("baseRange")("baseRange")
        +co_parse.genDecimalAttrib("bombTransitTime")("transitTime")
        +co_parse.genIntAttrib("bombEffectCount")("effectCount")
        +co_parse.genDecimalAttrib("bombEffectAngleVariance")("effectAngleVar")
        +g_bombingEffect
)("bombardment")
g_canBombTrue = (
    co_parse.genBoolAttrib("canBomb",
            co_parse.g_true)("canBomb")
    +g_bombardment
)
g_canBomb = pp.Or([g_canBombTrue,g_canBombFalse])

"""### Frigate Entity ###"""
g_hasLevelsTrue = (
    co_parse.genBoolAttrib("hasLevels",
            co_parse.g_true)("hasLevels")
    +coe.genFixedLvlAttrib("ExperiencePointsForDestroying")("exp")
    +coe.genFixedLvlAttrib("MaxHullPoints")("hullPointsMax")
    +coe.genFixedLvlAttrib("MaxShieldPoints")("shieldPointsMax")
    +coe.genFixedLvlAttrib("HullPointRestoreRate")("hullPointRestore")
    +coe.genFixedLvlAttrib("ShieldPointRestoreRate")("shieldPointRestore")
    +coe.genFixedLvlAttrib("BaseArmorPoints")("armorPoints")
    +coe.genFixedLvlAttrib("maxMitigation")("shieldMitigation")
)
g_hasLevelsFalse = (
    co_parse.genBoolAttrib("hasLevels",
            co_parse.g_false)("hasLevels")
    +hit.g_expCap
    +hit.g_hullPoints
    +hit.g_maxShield
    +hit.g_hullRestore
    +hit.g_shieldRestore
    +hit.g_baseArmorPoints
    +hit.g_maxMitigation("shieldMitigation")
)
g_hasLevels = pp.Or([g_hasLevelsTrue,g_hasLevelsFalse])
g_hasWeaponLevelsFalse = co_parse.genBoolAttrib("hasWeaponLevels",
        co_parse.g_false)(
        "hasWeapLevels")
g_hasWeaponLevelsTrue = (
    co_parse.genBoolAttrib("hasWeaponLevels",
            co_parse.g_true)("hasWeapLevels")
    +coe.genFixedLvlAttrib("weaponDamageMult")("weapDmgMult")
    +coe.genFixedLvlAttrib("weaponCooldownMult")("weapCooldownMult")
)
g_hasWeaponLevels = pp.Or([g_hasWeaponLevelsTrue,g_hasWeaponLevelsFalse])
g_FrigateEntity = (
    coe.genEntityHeader("Frigate")
    +armed.g_attackBehaviour
    +g_autoJoin
    +g_canBomb
    +co_parse.genListAttrib("typeCount",
            co_parse.genEnumAttrib("frigateRoleType",
            FRIGATE_ROLE_TYPES))("roleTypes")
    +armed.g_statType
    +coe.g_mainViewIcon
    +coe.g_picture
    +coe.g_nameStringCap
    +coe.g_descriptionStringCap
    +coe.g_counterDescString
    +coe.g_basePrice
    +g_slotCount
    +g_buildTimeCap
    +g_hasLevels
    +coe.g_prereqs
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +coe.g_zoomDist
    +armed.g_armed
    +g_hasWeaponLevels
    +coe.g_mass
    +hit.g_shieldVisuals
    +coe.g_movement
    +armed.g_squadrons
    +co_parse.genIntAttrib("maxNumCommandPoints")("commandPoints")
    +hit.g_voiceSounds
    +armed.g_criterionMeshInfos
    +armed.g_meshEffects
    +g_exhaustSys
    +hit.g_explosion
    +coe.g_hyperChargeSnd
    +coe.g_hyperTravelSnd
    +g_engineSound
    +coe.g_abilities
    +coe.g_maxAntimatter
    +coe.g_antimatterRestore
    +co_parse.genEnumAttrib("cargoType",CARGO_TYPES)("cargoType")
    +co_parse.genDecimalAttrib("maxCargoCapacity")("cargoCapacity")
    +armed.g_formation
    +coe.g_shadows
    +co_parse.genDecimalAttrib("allegianceDecreasePerRoundtrip")(
            "allegianceDecPerRoundtrip")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### CapitalShip Entity ###"""

g_CapitalShipEntity = (
    coe.genEntityHeader("CapitalShip")
    +coe.g_abilities
    +armed.g_attackBehaviour
    +g_autoJoin
    +g_canBomb
    +coe.g_mainViewIcon
    +coe.g_basePrice
    +g_slotCount
    +g_buildTimeCap
    +g_maxHullPointsLvl
    +g_maxShieldPointsLvl
    +g_hullPointRestoreLvl
    +g_shieldPointsRestoreLvl
    +g_armorFromExp
    +g_maxMitigationLvl
    +g_maxAMLvl
    +g_antimatterRestoreLvl
    +g_cultureProtectRate
    +coe.g_nameStringCap
    +coe.g_descriptionStringCap
    +coe.g_picture
    +coe.g_prereqs
    +co_parse.genEnumAttrib("roleType",CAPITAL_ROLE_TYPES)("roleType")
    +armed.g_statType
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +coe.g_zoomDist
    +armed.g_armed
    +coe.g_mass
    +hit.g_shieldVisuals
    +coe.g_movement
    +armed.g_squadrons
    +hit.g_voiceSounds
    +armed.g_criterionMeshInfos
    +armed.g_meshEffects
    +g_exhaustSys
    +hit.g_explosion
    +g_commandPointsLvl
    +g_weapCooldown
    +g_weapDamage
    +coe.g_hyperChargeSnd
    +coe.g_hyperTravelSnd
    +g_engineSound
    +armed.g_formation
    +coe.g_shadows
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Titan Entity ###"""
g_TitanEntity = (
    coe.genEntityHeader("Titan")
    +coe.g_abilities
    +armed.g_attackBehaviour
    +g_autoJoin
    +g_canBomb
    +coe.g_mainViewIcon
    +coe.g_basePrice
    +g_slotCount
    +g_buildTimeCap
    +g_maxHullPointsLvl
    +g_maxShieldPointsLvl
    +g_hullPointRestoreLvl
    +g_shieldPointsRestoreLvl
    +g_armorFromExp
    +g_maxMitigationLvl
    +g_maxAMLvl
    +g_antimatterRestoreLvl
    +g_cultureProtectRate
    +coe.g_nameStringCap
    +coe.g_descriptionStringCap
    +coe.g_picture
    +coe.g_prereqs
    +armed.g_statType
    +co_parse.genListAttrib("UpgradeTypeCount",
            co_parse.genStringAttrib("UpgradeType"))("upgrades")
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +coe.g_zoomDist
    +armed.g_armed
    +coe.g_mass
    +hit.g_shieldVisuals
    +coe.g_movement
    +armed.g_squadrons
    +hit.g_voiceSounds
    +armed.g_criterionMeshInfos
    +armed.g_meshEffects
    +g_exhaustSys
    +hit.g_explosion
    +g_commandPointsLvl
    +g_weapCooldown
    +g_weapDamage
    +coe.g_hyperChargeSnd
    +coe.g_hyperTravelSnd
    +g_engineSound
    +armed.g_formation
    +coe.g_shadows
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

class Bombardment(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,hasLevels,baseDamage,basePopKilled,bombingFreq,baseRange,
            transitTime,effectCount,effectAngleVar,effect,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.hasLevels = co_attribs.AttribBool(**hasLevels)
        self.baseDamage = co_attribs.AttribNum.factory(baseDamage)
        self.basePopKilled = co_attribs.AttribNum.factory(basePopKilled)
        self.bombingFreq = co_attribs.AttribNum(**bombingFreq)
        self.baseRange = co_attribs.AttribNum(**baseRange)
        self.transitTime = co_attribs.AttribNum(**transitTime)
        self.effectCount = co_attribs.AttribNum(**effectCount)
        self.effectAngleVar = co_attribs.AttribNum(**effectAngleVar)
        self.effect = armed.SharedWeaponEffect.factory(**effect)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.hasLevels==other.hasLevels)
                        and (self.baseDamage==other.baseDamage)
                        and (self.basePopKilled==other.basePopKilled)
                        and (self.bombingFreq==other.bombingFreq)
                        and (self.baseRange==other.baseRange)
                        and (self.transitTime==other.transitTime)
                        and (self.effectCount==other.effectCount)
                        and (self.effectAngleVar==other.effectAngleVar)
                        and (self.effect==other.effect))

    def toString(self,indention):
        res = self.hasLevels.toString(0)
        res += self.baseDamage.toString(0)
        res += self.basePopKilled.toString(0)
        res += self.bombingFreq.toString(0)
        res += self.baseRange.toString(0)
        res += self.transitTime.toString(0)
        res += self.effectCount.toString(0)
        res += self.effectAngleVar.toString(0)
        res += self.effect.toString(0)
        return indentText(res,indention)

"""############################### Properties ##############################"""

class Moving(coe.MainViewVisible):
    def __init__(self,exhaustSys,mass,engineSound,maxAccelLin,maxAccelStrafe,
            maxDecelLin,maxAccelAng,maxDecelAng,maxSpeedLin,maxRollRate,
            maxRollAngle,**kwargs):
        super().__init__(**kwargs)
        self.exhaustSys = par.ParticleRef(**exhaustSys)
        self.engineSound = audio.SoundRef(**engineSound)
        self.mass = co_attribs.AttribNum(**mass)
        self.maxAccelLin = co_attribs.AttribNum(**maxAccelLin)
        self.maxAccelStrafe = co_attribs.AttribNum(**maxAccelStrafe)
        self.maxDecelLin = co_attribs.AttribNum(**maxDecelLin)
        self.maxAccelAng = co_attribs.AttribNum(**maxAccelAng)
        self.maxDecelAng = co_attribs.AttribNum(**maxDecelAng)
        self.maxSpeedLin = co_attribs.AttribNum(**maxSpeedLin)
        self.maxRollRate = co_attribs.AttribNum(**maxRollRate)
        self.maxRollAngle = co_attribs.AttribNum(**maxRollAngle)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.exhaustSys==other.exhaustSys)
                        and (self.engineSound==other.engineSound)
                        and (self.mass==other.mass)
                        and (self.maxAccelLin==other.maxAccelLin)
                        and (self.maxAccelStrafe==other.maxAccelStrafe)
                        and (self.maxDecelLin==other.maxDecelLin)
                        and (self.maxAccelAng==other.maxAccelAng)
                        and (self.maxDecelAng==other.maxDecelAng)
                        and (self.maxSpeedLin==other.maxSpeedLin)
                        and (self.maxRollRate==other.maxRollRate)
                        and (self.maxRollAngle==other.maxRollAngle))

class Ship(armed.Constructable,armed.CarriesFighters,Moving):
    def __init__(self,autoJoinFleet,canBomb,hyperChargingSnd,
            hyperTravelSnd,slotCount,bombardment=None,**kwargs):
        super().__init__(**kwargs)
        self.autoJoinFleet = co_attribs.AttribBool(**autoJoinFleet)
        self.canBomb = co_attribs.AttribBool(**canBomb)
        if self.canBomb.value:
            self.bombardment = Bombardment(**bombardment)
        else:
            self.bombardment = None
        self.hyperChargingSnd = audio.SoundRef(**hyperChargingSnd)
        self.hyperTravelSnd = audio.SoundRef(**hyperTravelSnd)
        self.slotCount = co_attribs.AttribNum(**slotCount)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.autoJoinFleet==other.autoJoinFleet)
                        and (self.canBomb==other.canBomb)
                        and (self.bombardment==other.bombardment)
                        and (self.hyperChargingSnd==other.hyperChargingSnd)
                        and (self.hyperTravelSnd==other.hyperTravelSnd)
                        and (self.slotCount==other.slotCount))

"""################################# Files #################################"""

class Frigate(coe.Entity,Ship):
    parser = g_FrigateEntity

    def __init__(self,roleTypes,counterDesc,hasLevels,hasWeapLevels,
            cargoType,cargoCapacity,allegianceDecPerRoundtrip,
            weapDmgMult=None,weapCooldownMult=None,**kwargs):
        super().__init__(**kwargs)
        self.roleTypes = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":FRIGATE_ROLE_TYPES},
                **roleTypes)
        self.counterDesc = ui.StringReference(**counterDesc)
        self.hasLevels = co_attribs.AttribBool(**hasLevels)
        self.hasWeapLevels = co_attribs.AttribBool(**hasWeapLevels)
        if self.hasWeapLevels.value:
            self.weapDmgMult = co_attribs.AttribFixedLevelable(**weapDmgMult)
            self.weapCooldownMult = co_attribs.AttribFixedLevelable(
                    **weapCooldownMult)
        else:
            self.weapDmgMult = None
            self.weapCooldownMult = None
        self.cargoType = co_attribs.AttribEnum(possibleVals=CARGO_TYPES,**cargoType)
        self.cargoCapacity = co_attribs.AttribNum(**cargoCapacity)
        self.allegianceDecPerRoundtrip = co_attribs.AttribNum(
                **allegianceDecPerRoundtrip)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.roleTypes==other.roleTypes)
                        and (self.counterDesc==other.counterDesc)
                        and (self.hasLevels==other.hasLevels)
                        and (self.hasWeapLevels==other.hasWeapLevels)
                        and (self.weapDmgMult==other.weapDmgMult)
                        and (self.weapCooldownMult==other.weapCooldownMult)
                        and (self.cargoType==other.cargoType)
                        and (self.cargoCapacity==other.cargoCapacity)
                        and (
                        self.allegianceDecPerRoundtrip==other.allegianceDecPerRoundtrip))

    def toString(self,indention):
        res = super().toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += self.autoJoinFleet.toString(0)
        res += self.canBomb.toString(0)
        if self.canBomb.value:
            res += self.bombardment.toString(0)
        res += self.roleTypes.toString(0)
        res += self.statcount.toString(0)
        res += self.iconMainView.toString(0)
        res += self.pic.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.counterDesc.toString(0)
        res += self.price.toString(0)
        res += self.slotCount.toString(0)
        res += self.buildTime.toString(0)
        res += self.hasLevels.toString(0)
        res += self.exp.toString(0)
        res += self.hullPointsMax.toString(0)
        res += self.shieldPointsMax.toString(0)
        res += self.hullPointRestore.toString(0)
        res += self.shieldPointRestore.toString(0)
        res += self.armorPoints.toString(0)
        res += self.shieldMitigation.toString(0)
        res += self.prereqs.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.zoomDist.toString(0)
        res += armed.Armed.toString(self,0)
        res += self.hasWeapLevels.toString(0)
        if self.hasWeapLevels.value:
            res += self.weapDmgMult.toString(0)
            res += self.weapCooldownMult.toString(0)
        res += self.mass.toString(0)
        res += self.shieldMesh.toString(0)
        res += self.shieldRender.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        for sq in self.squads:
            res += sq.toString(0)
        res += self.commandPoints.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.meshes.toString(0)
        res += self.meshIncreasedEffect.toString(0)
        res += self.meshDecreasedEffect.toString(0)
        res += self.exhaustSys.toString(0)
        res += self.explosion.toString(0)
        res += self.hyperChargingSnd.toString(0)
        res += self.hyperTravelSnd.toString(0)
        res += self.engineSound.toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.antimatterMax.toString(0)
        res += self.antimatterRestore.toString(0)
        res += self.cargoType.toString(0)
        res += self.cargoCapacity.toString(0)
        res += self.formationRank.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        res += self.allegianceDecPerRoundtrip.toString(0)
        return indentText(res,indention)

class CapitalShip(coe.Entity,Ship):
    parser = g_CapitalShipEntity

    def __init__(self,cultureProtectRate,roleType,weapCooldownDec,weapDmgInc,
            **kwargs):
        super().__init__(exp={"identifier":"","val":""},**kwargs)
        # The experience value for capital ships is determined by their 
        # level and defined in the Gameplay.constants file
        self.exp = None
        self.cultureProtectRate = co_attribs.AttribVarLevelable(**cultureProtectRate)
        self.roleType = co_attribs.AttribEnum(possibleVals=CAPITAL_ROLE_TYPES,
                **roleType)
        self.weapCooldownDec = co_attribs.AttribVarLevelable(**weapCooldownDec)
        self.weapDmgInc = co_attribs.AttribVarLevelable(**weapDmgInc)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.cultureProtectRate==other.cultureProtectRate)
                        and (self.roleType==other.roleType)
                        and (self.weapCooldownDec==other.weapCooldownDec)
                        and (self.weapDmgInc==other.weapDmgInc))

    def toString(self,indention):
        res = super().toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += self.autoJoinFleet.toString(0)
        res += self.canBomb.toString(0)
        if self.canBomb.value:
            res += self.bombardment.toString(0)
        res += self.iconMainView.toString(0)
        res += self.price.toString(0)
        res += self.slotCount.toString(0)
        res += self.buildTime.toString(0)
        res += self.hullPointsMax.toString(0)
        res += self.shieldPointsMax.toString(0)
        res += self.hullPointRestore.toString(0)
        res += self.shieldPointRestore.toString(0)
        res += self.armorPoints.toString(0)
        res += self.shieldMitigation.toString(0)
        res += self.antimatterMax.toString(0)
        res += self.antimatterRestore.toString(0)
        res += self.cultureProtectRate.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.pic.toString(0)
        res += self.prereqs.toString(0)
        res += self.roleType.toString(0)
        res += self.statcount.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.zoomDist.toString(0)
        res += armed.Armed.toString(self,0)
        res += self.mass.toString(0)
        res += self.shieldMesh.toString(0)
        res += self.shieldRender.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        for sq in self.squads:
            res += sq.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.meshes.toString(0)
        res += self.meshIncreasedEffect.toString(0)
        res += self.meshDecreasedEffect.toString(0)
        res += self.exhaustSys.toString(0)
        res += self.explosion.toString(0)
        res += self.commandPoints.toString(0)
        res += self.weapCooldownDec.toString(0)
        res += self.weapDmgInc.toString(0)
        res += self.hyperChargingSnd.toString(0)
        res += self.hyperTravelSnd.toString(0)
        res += self.engineSound.toString(0)
        res += self.formationRank.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)

class Titan(coe.Entity,Ship):
    parser = g_TitanEntity

    def __init__(self,cultureProtectRate,weapCooldownDec,weapDmgInc,upgrades,
            **kwargs):
        super().__init__(exp={"identifier":"","val":""},**kwargs)
        # The experience value for capital ships is determined by their 
        # level and defined in the Gameplay.constants file
        self.exp = None
        self.upgrades = co_attribs.AttribList(elemType=coe.EntityRef,
                elemArgs={"types":["TitanUpgrade"]},
                **upgrades)
        self.cultureProtectRate = co_attribs.AttribVarLevelable(**cultureProtectRate)
        self.weapCooldownDec = co_attribs.AttribVarLevelable(**weapCooldownDec)
        self.weapDmgInc = co_attribs.AttribVarLevelable(**weapDmgInc)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.cultureProtectRate==other.cultureProtectRate)
                        and (self.upgrades==other.upgrades)
                        and (self.weapCooldownDec==other.weapCooldownDec)
                        and (self.weapDmgInc==other.weapDmgInc))

    def toString(self,indention):
        res = super().toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += self.autoJoinFleet.toString(0)
        res += self.canBomb.toString(0)
        if self.canBomb.value:
            res += self.bombardment.toString(0)
        res += self.iconMainView.toString(0)
        res += self.price.toString(0)
        res += self.slotCount.toString(0)
        res += self.buildTime.toString(0)
        res += self.hullPointsMax.toString(0)
        res += self.shieldPointsMax.toString(0)
        res += self.hullPointRestore.toString(0)
        res += self.shieldPointRestore.toString(0)
        res += self.armorPoints.toString(0)
        res += self.shieldMitigation.toString(0)
        res += self.antimatterMax.toString(0)
        res += self.antimatterRestore.toString(0)
        res += self.cultureProtectRate.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.pic.toString(0)
        res += self.prereqs.toString(0)
        res += self.statcount.toString(0)
        res += self.upgrades.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.zoomDist.toString(0)
        res += armed.Armed.toString(self,0)
        res += self.mass.toString(0)
        res += self.shieldMesh.toString(0)
        res += self.shieldRender.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        for sq in self.squads:
            res += sq.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.meshes.toString(0)
        res += self.meshIncreasedEffect.toString(0)
        res += self.meshDecreasedEffect.toString(0)
        res += self.exhaustSys.toString(0)
        res += self.explosion.toString(0)
        res += self.commandPoints.toString(0)
        res += self.weapCooldownDec.toString(0)
        res += self.weapDmgInc.toString(0)
        res += self.hyperChargingSnd.toString(0)
        res += self.hyperTravelSnd.toString(0)
        res += self.engineSound.toString(0)
        res += self.formationRank.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)
