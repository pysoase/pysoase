"""Contains parsers and classes for astronomic objects.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.entities.uncolonizables as uncol
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""############################### Parser ##################################"""

"""### Basic Elements ###"""

g_meshInfo = pp.Group(
        co_parse.genHeading("meshInfo")
        +co_parse.genStringAttrib("meshName")("mesh")
        +co_parse.genBoolAttrib("shouldPrefer")("prefer")
)
g_celestialBody = (
    co_parse.genDecimalAttrib("moveAreaRadius")("moveArea")
    +co_parse.genDecimalAttrib("hyperspaceExitRadius")("hyperspaceExit")
    +co_parse.genBoolAttrib("isWormhole")("wormhole")
    +co_parse.genIntAttrib("maxStarBaseCountPerPlayer")("maxStarbases")
    +co_parse.genIntAttrib("maxSpaceMineCountPerPlayer")("maxMines")
)

"""### Planet Bonus Entity ###"""

g_PlanetBonusEntity = (
    coe.genEntityHeader("PlanetBonus")
    +co_parse.genIntAttrib("minUpgradeLevelNeeded")("minUpgLvl")
    +co_parse.genIntAttrib("maxUpgradeLevelNeeded")("maxUpgLvl")
    +coe.g_nameString
    +coe.g_descriptionStringShort
    +pp.Group(
            co_parse.genDecimalAttrib("floatBonus:AdditiveCultureSpreadPerc")(
                    "addCultSpread")
            +co_parse.genDecimalAttrib("floatBonus:AdditivePopulation")("addPop")
            +co_parse.genDecimalAttrib("floatBonus:AdditivePlanetHealth")(
                    "addHealth")
            +co_parse.genDecimalAttrib("floatBonus:AdditiveSlotsCivilian")(
                    "addSlotsCiv")
            +co_parse.genDecimalAttrib("floatBonus:AdditiveSlotsTactical")(
                    "addSlotsTac")
            +co_parse.genDecimalAttrib("floatBonus:AdditiveTaxIncome")("addTax")
            +co_parse.genDecimalAttrib("floatBonus:AdditiveTradeIncomePerc")(
                    "addTrade")
            +co_parse.genDecimalAttrib(
                    "floatBonus:BombingDamageAsDamageTargetAdjustment")(
                    "bombDamageAdj")
            +co_parse.genDecimalAttrib("floatBonus:MaxAllegiancePercBonus")(
                    "maxAllegiance")
            +co_parse.genDecimalAttrib("floatBonus:MetalIncomePerc")("metalIncome")
            +co_parse.genDecimalAttrib("floatBonus:CrystalIncomePerc")(
                    "crystalIncome")
            +co_parse.genDecimalAttrib("floatBonus:GravityWellRadiusPerc")(
                    "gravWellRadius")
            +co_parse.genDecimalAttrib("floatBonus:ModuleBuildCostPerc")(
                    "moduleCost")
            +co_parse.genDecimalAttrib("floatBonus:ModuleBuildRatePerc")(
                    "moduleBuildRate")
            +co_parse.genDecimalAttrib("floatBonus:PlanetUpgradeBuildCostPerc")(
                    "upgradeCost")
            +co_parse.genDecimalAttrib("floatBonus:PlanetUpgradeBuildRatePerc")(
                    "upgradeRate")
            +co_parse.genDecimalAttrib("floatBonus:PopulationGrowthPerc")(
                    "popGrowth")
            +co_parse.genDecimalAttrib("floatBonus:ShipBuildCostAdjustment")(
                    "shipCost")
            +co_parse.genDecimalAttrib(
                    "floatBonus:WeaponDamageAtGravityWellMultiplier")(
                    "weapon_damage"))("floatBoni")
    +pp.Group(
            co_parse.genIntAttrib("intBonus:MaxSpaceMines")("maxMines")
            +co_parse.genIntAttrib("intBonus:MaxStarbases")("maxStarbases")
            +co_parse.genIntAttrib("intBonus:ModuleConstructors")(
                    "numModConstructors")
            +co_parse.genIntAttrib("intBonus:SpacePonies")("spacePonies")
    )("intBoni")
    +coe.g_dlcid
    +coe.g_uiIcons
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Star Entity ###"""

g_StarEntity = (
    coe.genEntityHeader("Star")
    +co_parse.genColorAttrib("coronaStartColor")("coronaStartColor")
    +co_parse.genColorAttrib("coronaEndColor")("coronaEndColor")
    +co_parse.genDecimalAttrib("coronaStartColorDistance")("coronaStartColorDist")
    +co_parse.genDecimalAttrib("coronaEndColorDistance")("coronaEndColorDist")
    +co_parse.genStringAttrib("farIcon")("iconFar")
    +co_parse.genListAttrib("coloredSkyboxMeshInfoCount",g_meshInfo)(
            "coloredSkyboxes")
    +co_parse.genListAttrib("deepSpaceSkyboxMeshInfoCount",g_meshInfo)(
            "deepSpaceSkyboxes")
    +co_parse.genStringAttrib("iconTextureName")("textureIcon")
    +co_parse.genIntAttrib("dlcID")("dlcID")
    +coe.g_abilities
    +coe.g_uiIcons
    +coe.g_zoomDist
    +g_celestialBody
    +coe.g_mainViewIcon
    +coe.g_picture
    +co_parse.genStringAttrib("MeshName")("mesh")
    +co_parse.genDecimalAttrib("MaxMeshVisibilityDistanceToCamera")(
            "maxVisibilityDist")
    +coe.g_shadows
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Planet Entity ###"""

g_plMeshInfo = pp.Group(
        co_parse.genHeading("meshInfo")
        +co_parse.genStringAttrib("typeNameStringID")("nameString")
        +co_parse.genStringAttrib("asteroidTemplate")("templateAsteroid")
        +co_parse.genStringAttrib("dustCloudTemplate")("templateDustCloud")
        +co_parse.genStringAttrib("meshName")("mesh")
        +co_parse.genColorAttrib("cloudColor")("colorCloud")
        +co_parse.genDecimalAttrib("nullMeshRadius")("nullMeshRadius")
        +co_parse.genStringAttrib("nullMeshParticleEffect")("nullMeshEff")
        +coe.g_uiIcons
        +coe.g_mainViewIcon
        +co_parse.genStringAttrib("undetectedMainViewIcon")("iconMainUndetected")
        +coe.g_picture
        +co_parse.genBoolAttrib("shouldPrefer")("prefer")
)
g_asteroidSetup = (
    co_parse.genDecimalAttrib("minCount")("countMin")
    +co_parse.genDecimalAttrib("maxCount")("countMax")
    +co_parse.genDecimalAttrib("extractionRate")("rateExtract")
    +co_parse.genDecimalAttrib("refineryRate")("rateRefinery")
    +co_parse.genIntAttrib("maxRefineryCount")("refineriesMax")
)
g_resourceSetup = pp.Group(
        co_parse.genHeading("planetResourceSetupInfo")
        +co_parse.genDecimalAttrib("asteroidSpawnAngleVariance")("asteroidSpawnAngle")
        +co_parse.genDecimalAttrib("totalMaxResourceAsteroids")("maxAsteroids")
        +pp.Group(
                co_parse.genHeading("metalResourceAsteroidSetup")
                +g_asteroidSetup
        )("asteroidsMetal")
        +pp.Group(
                co_parse.genHeading("crystalResourceAsteroidSetup")
                +g_asteroidSetup
        )("asteroidsCrystal")
        +pp.Group(
                co_parse.genHeading("neutralMetalResourceAsteroidSetup")
                +g_asteroidSetup
        )("asteroidsMetalNeutral")
        +pp.Group(
                co_parse.genHeading("neutralCrystalResourceAsteroidSetup")
                +g_asteroidSetup
        )("asteroidsCrystalNeutral")
)("resourceSetup")
g_plStage = pp.Group(
        co_parse.genHeading("stage")
        +coe.g_price
        +co_parse.genDecimalAttrib("upgradeTime")("upgTime")
)
g_popStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("maxPopulation")("popMax")
        +co_parse.genDecimalAttrib("populationGrowthRate")("popGrowth")
        +co_parse.genDecimalAttrib("developmentTaxPenalty")("taxPenalty")
)
g_civStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("maxModuleSlotCount:Civilian")("maxSlots")
        +co_parse.genIntAttrib("maxModuleConstructorCount")("maxConstructors")
)
g_tacStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("maxModuleSlotCount:Tactical")("maxSlots")
        +co_parse.genIntAttrib("maxModuleConstructorCount")("maxConstructors")
)
g_homeStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genBoolAttrib("isHomePlanet")("isHome")
        +co_parse.genDecimalAttrib("homePlanetTaxRateBonus")("taxBonus")
        +co_parse.genDecimalAttrib("homePlanetMetalIncomeBonus")("metalBonus")
        +co_parse.genDecimalAttrib("homePlanetCrystalIncomeBonus")("crystalBonus")
)
g_infraStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("maxHealth")("maxHealth")
)
g_socStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("maxPopulation")("popMax")
        +co_parse.genDecimalAttrib("populationGrowthRate")("popGrowth")
        +co_parse.genDecimalAttrib("tradeIncomeModifier")("tradeMod")
        +co_parse.genDecimalAttrib("shipBuildRateModifier")("shipBuildMod")
        +co_parse.genDecimalAttrib("cultureSpreadRateModifier")("cultureSpreadMod")
)
g_industryStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("maxPopulation")("popMax")
        +co_parse.genDecimalAttrib("tradeIncomeModifier")("tradeMod")
        +co_parse.genDecimalAttrib("shipBuildRateModifier")("shipBuildMod")
        +co_parse.genDecimalAttrib("cultureSpreadRateModifier")("cultureSpreadMod")
)
g_smugglerStage = pp.Group(
        pp.ungroup(g_plStage)
        +co_parse.genDecimalAttrib("corruptionPercent")("corruption")
        +co_parse.genDecimalAttrib("smugglingPercent")("smuggling")
)
g_plUpgrades = pp.Group(
        co_parse.genHeading("planetUpgradeDef")
        +co_parse.genListAttrib("stageCount",g_popStage,"path:Population")(
                "population")
        +co_parse.genListAttrib("stageCount",g_civStage,"path:CivilianModules")(
                "civilian")
        +co_parse.genListAttrib("stageCount",g_tacStage,"path:TacticalModules")(
                "tactical")
        +co_parse.genListAttrib("stageCount",g_homeStage,"path:Home")(
                "home")
        +co_parse.genListAttrib("stageCount",g_plStage,"path:ArtifactLevel")(
                "artifact")
        +co_parse.genListAttrib("stageCount",g_infraStage,"path:Infrastructure")(
                "infrastructure")
        +co_parse.genListAttrib("stageCount",g_socStage,"path:Social")(
                "social")
        +co_parse.genListAttrib("stageCount",g_industryStage,"path:Industry")(
                "industry")
        +co_parse.genListAttrib("stageCount",g_smugglerStage,"path:Smuggler")(
                "smuggler")
)("upgrades")
g_skyboxScalar = pp.Group(
        co_parse.genHeading("skyboxScalars")
        +co_parse.genDecimalAttrib("diffuseScalar")("diffuse")
        +co_parse.genDecimalAttrib("ambientScalar")("ambient")
)
g_plRuin = pp.Group(
        co_parse.genHeading("planetRuinDef")
        +co_parse.genStringAttrib("ruinPlanetType")("ruinType")
        +co_parse.genEnumAttrib("attachType",coe.ATTACH_TYPES)("attachType")
        +co_parse.genStringAttrib("effectName")("effect")
        +co_parse.genDecimalAttrib("startTime")("timeStart")
        +co_parse.genStringAttrib("soundID")("sound")
        +co_parse.genDecimalAttrib("creditsOnStrippedToTheCore")("gainedCredits")
        +co_parse.genDecimalAttrib("metalOnStrippedToTheCore")("gainedMetal")
        +co_parse.genDecimalAttrib("crystalOnStrippedToTheCore")("gainedCrystal")
        +co_parse.genDecimalAttrib("extraScuttleTimeOnStrip")("timeScuttle")
)("ruin")
g_PlanetEntity = (
    coe.genEntityHeader("Planet")
    +co_parse.genListAttrib("meshInfoCount",g_plMeshInfo)("meshes")
    +coe.g_zoomDist
    +co_parse.genColorAttrib("glowColor")("colorGlow")
    +co_parse.genColorAttrib("ringColor")("colorRing")
    +co_parse.genStringAttrib("cloudLayerTextureName")("textureCloud")
    +g_resourceSetup
    +co_parse.genBoolAttrib("isAsteroid")("isAsteroid")
    +co_parse.genDecimalAttrib("healthRegenRate")("healthRegen")
    +g_plUpgrades
    +co_parse.genDecimalAttrib("ringsChance")("chanceRings")
    +co_parse.genBoolAttrib("isColonizable")("isColonizable")
    +co_parse.genEnumAttrib("planetTypeForResearch",
            coe.PLANETTEYPE_FOR_RESEARCH)("researchType")
    +co_parse.genListAttrib("skyboxScalarsCount",g_skyboxScalar)("skyboxScalars")
    +co_parse.genListAttrib("requiredPlanetBonusesCount",
            co_parse.genStringAttrib("bonus"))(
            "planetBonusesReq")
    +co_parse.genListAttrib("possibleRandomPlanetBonusesCount",
            co_parse.genStringAttrib("bonus"))("planetBonusesRand")
    +co_parse.genStringAttrib("ambienceSoundID")("sndAmbience")
    +co_parse.genBoolAttrib("renderAsVolcanic")("renderVolcanic")
    +g_plRuin
    +coe.g_dlcid
    +hit.g_shieldVisuals
    +g_celestialBody
    +coe.g_shadows
    +coe.g_abilities
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""########################### Attribute Classes ###########################"""

class MeshInfoStar(coe.MeshInfo):
    def __init__(self,prefer,**kwargs):
        super().__init__(**kwargs)
        self.prefer = co_attribs.AttribBool(**prefer)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.prefer==other.prefer)

    def toString(self,indention):
        res = super().toString(0)
        res += self.prefer.toString(1)
        return indentText(res,indention)

class MeshInfoPlanet(MeshInfoStar,coe.MainViewSelectable):
    def __init__(self,templateAsteroid,templateDustCloud,colorCloud,
            nullMeshRadius,nullMeshEff,iconMainUndetected,**kwargs):
        descString = {"identifier":"","val":""}
        zoomDist = {"identifier":"","val":0.0}
        shadowMin = {"identifier":"","val":0.0}
        shadowMax = {"identifier":"","val":0.0}
        super().__init__(
                zoomDist=zoomDist,
                descString=descString,
                shadowMin=shadowMin,
                shadowMax=shadowMax,
                **kwargs)
        self.descString = None
        self.mesh.canBeEmpty = True
        self.templateAsteroid = uncol.AsteroidRef(**templateAsteroid)
        self.templateDustCloud = uncol.CloudRef(**templateDustCloud)
        self.colorCloud = co_attribs.AttribColor(**colorCloud)
        self.nullMeshRadius = co_attribs.AttribNum(**nullMeshRadius)
        if self.mesh.ref:
            self.nullMeshEff = par.ParticleRef(canBeEmpty=True,**nullMeshEff)
        else:
            self.nullMeshEff = par.ParticleRef(**nullMeshEff)
        self.iconMainUndetected = ui.BrushRef(**iconMainUndetected)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.templateAsteroid==other.templateAsteroid)
                        and (self.templateDustCloud==other.templateDustCloud)
                        and (self.colorCloud==other.colorCloud)
                        and (self.nullMeshRadius==other.nullMeshRadius)
                        and (self.nullMeshEff==other.nullMeshEff)
                        and (self.iconMainUndetected==other.iconMainUndetected))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.nameString.toString(1)
        res += self.templateAsteroid.toString(1)
        res += self.templateDustCloud.toString(1)
        res += self.mesh.toString(1)
        res += self.colorCloud.toString(1)
        res += self.nullMeshRadius.toString(1)
        res += self.nullMeshEff.toString(1)
        res += self.iconHud.toString(1)
        res += self.iconHudSmall.toString(1)
        res += self.iconInfocard.toString(1)
        res += self.iconMainView.toString(1)
        res += self.iconMainUndetected.toString(1)
        res += self.pic.toString(1)
        res += self.prefer.toString(1)
        return indentText(res,indention)

class AsteroidSetup(co_attribs.Attribute):
    def __init__(self,countMin,countMax,rateExtract,rateRefinery,refineriesMax,
            **kwargs):
        super().__init__(**kwargs)
        self.countMin = co_attribs.AttribNum(**countMin)
        self.countMax = co_attribs.AttribNum(**countMax)
        self.rateExtract = co_attribs.AttribNum(**rateExtract)
        self.rateRefinery = co_attribs.AttribNum(**rateRefinery)
        self.refineriesMax = co_attribs.AttribNum(**refineriesMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.countMin==other.countMin)
                        and (self.countMax==other.countMax)
                        and (self.rateExtract==other.rateExtract)
                        and (self.rateRefinery==other.rateRefinery)
                        and (self.refineriesMax==other.refineriesMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.countMin.toString(1)
        res += self.countMax.toString(1)
        res += self.rateExtract.toString(1)
        res += self.rateRefinery.toString(1)
        res += self.refineriesMax.toString(1)
        return indentText(res,indention)

class ResourceSetup(co_attribs.Attribute):
    def __init__(self,asteroidSpawnAngle,maxAsteroids,asteroidsMetal,
            asteroidsCrystal,asteroidsMetalNeutral,asteroidsCrystalNeutral,
            **kwargs):
        super().__init__(**kwargs)
        self.asteroidSpawnAngle = co_attribs.AttribNum(**asteroidSpawnAngle)
        self.maxAsteroids = co_attribs.AttribNum(**maxAsteroids)
        self.asteroidsMetal = AsteroidSetup(**asteroidsMetal)
        self.asteroidsCrystal = AsteroidSetup(**asteroidsCrystal)
        self.asteroidsMetalNeutral = AsteroidSetup(**asteroidsMetalNeutral)
        self.asteroidsCrystalNeutral = AsteroidSetup(**asteroidsCrystalNeutral)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.asteroidSpawnAngle==other.asteroidSpawnAngle)
                        and (self.maxAsteroids==other.maxAsteroids)
                        and (self.asteroidsMetal==other.asteroidsMetal)
                        and (self.asteroidsCrystal==other.asteroidsCrystal)
                        and (
                        self.asteroidsMetalNeutral==other.asteroidsMetalNeutral)
                        and (
                        self.asteroidsCrystalNeutral==other.asteroidsCrystalNeutral))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.asteroidSpawnAngle.toString(1)
        res += self.maxAsteroids.toString(1)
        res += self.asteroidsMetal.toString(1)
        res += self.asteroidsCrystal.toString(1)
        res += self.asteroidsMetalNeutral.toString(1)
        res += self.asteroidsCrystalNeutral.toString(1)
        return indentText(res,indention)

class PlanetUpgradeStage(co_attribs.Attribute):
    def __init__(self,price,upgTime,**kwargs):
        super().__init__(**kwargs)
        self.price = coe.Cost(**price)
        self.upgTime = co_attribs.AttribNum(**upgTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.price==other.price)
                        and (self.upgTime==other.upgTime))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.price.toString(1)
        res += self.upgTime.toString(1)
        return indentText(res,indention)

class PlanetPopStage(PlanetUpgradeStage):
    def __init__(self,popMax,popGrowth,taxPenalty,**kwargs):
        super().__init__(**kwargs)
        self.popMax = co_attribs.AttribNum(**popMax)
        self.popGrowth = co_attribs.AttribNum(**popGrowth)
        self.taxPenalty = co_attribs.AttribNum(**taxPenalty)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.popMax==other.popMax)
                        and (self.popGrowth==other.popGrowth)
                        and (self.taxPenalty==other.taxPenalty))

    def toString(self,indention):
        res = super().toString(0)
        res += self.popMax.toString(1)
        res += self.popGrowth.toString(1)
        res += self.taxPenalty.toString(1)
        return indentText(res,indention)

class PlanetInfrastructureStage(PlanetUpgradeStage):
    def __init__(self,maxHealth,**kwargs):
        super().__init__(**kwargs)
        self.maxHealth = co_attribs.AttribNum(**maxHealth)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.maxHealth==other.maxHealth)

    def toString(self,indention):
        res = super().toString(0)
        res += self.maxHealth.toString(1)
        return indentText(res,indention)

class PlanetIndustrialStage(PlanetUpgradeStage):
    def __init__(self,popMax,tradeMod,shipBuildMod,cultureSpreadMod,**kwargs):
        super().__init__(**kwargs)
        self.popMax = co_attribs.AttribNum(**popMax)
        self.tradeMod = co_attribs.AttribNum(**tradeMod)
        self.shipBuildMod = co_attribs.AttribNum(**shipBuildMod)
        self.cultureSpreadMod = co_attribs.AttribNum(**cultureSpreadMod)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.popMax==other.popMax)
                        and (self.tradeMod==other.tradeMod)
                        and (self.shipBuildMod==other.shipBuildMod)
                        and (self.cultureSpreadMod==other.cultureSpreadMod))

    def toString(self,indention):
        res = super().toString(0)
        res += self.popMax.toString(1)
        res += self.tradeMod.toString(1)
        res += self.shipBuildMod.toString(1)
        res += self.cultureSpreadMod.toString(1)
        return indentText(res,indention)

class PlanetSocialStage(PlanetIndustrialStage):
    def __init__(self,popGrowth,**kwargs):
        super().__init__(**kwargs)
        self.popGrowth = co_attribs.AttribNum(**popGrowth)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.popGrowth==other.popGrowth)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.price.toString(1)
        res += self.upgTime.toString(1)
        res += self.popMax.toString(1)
        res += self.popGrowth.toString(1)
        res += self.tradeMod.toString(1)
        res += self.shipBuildMod.toString(1)
        res += self.cultureSpreadMod.toString(1)
        return indentText(res,indention)

class PlanetModuleStage(PlanetUpgradeStage):
    def __init__(self,maxSlots,maxConstructors,**kwargs):
        super().__init__(**kwargs)
        self.maxSlots = co_attribs.AttribNum(**maxSlots)
        self.maxConstructors = co_attribs.AttribNum(**maxConstructors)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.maxSlots==other.maxSlots)
                        and (self.maxConstructors==other.maxConstructors))

    def toString(self,indention):
        res = super().toString(0)
        res += self.maxSlots.toString(1)
        res += self.maxConstructors.toString(1)
        return indentText(res,indention)

class PlanetHomeStage(PlanetUpgradeStage):
    def __init__(self,isHome,taxBonus,metalBonus,crystalBonus,**kwargs):
        super().__init__(**kwargs)
        self.isHome = co_attribs.AttribBool(**isHome)
        self.taxBonus = co_attribs.AttribNum(**taxBonus)
        self.metalBonus = co_attribs.AttribNum(**metalBonus)
        self.crystalBonus = co_attribs.AttribNum(**crystalBonus)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.isHome==other.isHome)
                        and (self.taxBonus==other.taxBonus)
                        and (self.metalBonus==other.metalBonus)
                        and (self.crystalBonus==other.crystalBonus))

    def toString(self,indention):
        res = super().toString(0)
        res += self.isHome.toString(1)
        res += self.taxBonus.toString(1)
        res += self.metalBonus.toString(1)
        res += self.crystalBonus.toString(1)
        return indentText(res,indention)

class PlanetSmugglerStage(PlanetUpgradeStage):
    def __init__(self,corruption,smuggling,**kwargs):
        super().__init__(**kwargs)
        self.corruption = co_attribs.AttribNum(**corruption)
        self.smuggling = co_attribs.AttribNum(**smuggling)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.corruption==other.corruption)
                        and (self.smuggling==other.smuggling))

    def toString(self,indention):
        res = super().toString(0)
        res += self.corruption.toString(1)
        res += self.smuggling.toString(1)
        return indentText(res,indention)

class PlanetUpgrades(co_attribs.Attribute):
    def __init__(self,population,civilian,tactical,home,artifact,
            infrastructure,social,industry,smuggler,**kwargs):
        super().__init__(**kwargs)
        self.population = co_attribs.AttribList(elemType=PlanetPopStage,**population)
        self.civilian = co_attribs.AttribList(elemType=PlanetModuleStage,**civilian)
        self.tactical = co_attribs.AttribList(elemType=PlanetModuleStage,**tactical)
        self.home = co_attribs.AttribList(elemType=PlanetHomeStage,**home)
        self.artifact = co_attribs.AttribList(elemType=PlanetUpgradeStage,**artifact)
        self.infrastructure = co_attribs.AttribList(elemType=PlanetInfrastructureStage,
                **infrastructure)
        self.social = co_attribs.AttribList(elemType=PlanetSocialStage,**social)
        self.industry = co_attribs.AttribList(elemType=PlanetIndustrialStage,
                **industry)
        self.smuggler = co_attribs.AttribList(elemType=PlanetSmugglerStage,
                **smuggler)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.population==other.population)
                        and (self.civilian==other.civilian)
                        and (self.tactical==other.tactical)
                        and (self.home==other.home)
                        and (self.artifact==other.artifact)
                        and (self.infrastructure==other.infrastructure)
                        and (self.social==other.social)
                        and (self.smuggler==other.smuggler)
                        and (self.industry==other.industry))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.population.toString(1)
        res += self.civilian.toString(1)
        res += self.tactical.toString(1)
        res += self.home.toString(1)
        res += self.artifact.toString(1)
        res += self.infrastructure.toString(1)
        res += self.social.toString(1)
        res += self.industry.toString(1)
        res += self.smuggler.toString(1)
        return indentText(res,indention)

class SkyboxScalar(co_attribs.Attribute):
    def __init__(self,diffuse,ambient,**kwargs):
        super().__init__(**kwargs)
        self.diffuse = co_attribs.AttribNum(**diffuse)
        self.ambient = co_attribs.AttribNum(**ambient)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.diffuse==other.diffuse)
                        and (self.ambient==other.ambient))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.diffuse.toString(1)
        res += self.ambient.toString(1)
        return indentText(res,indention)

class PlanetRuin(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,ruinType,attachType,effect,timeStart,sound,gainedCredits,
            gainedMetal,gainedCrystal,timeScuttle,**kwargs):
        super().__init__(**kwargs)
        self.ruinType = coe.EntityRef(types=["Planet"],**ruinType)
        self.attachType = co_attribs.AttribEnum(possibleVals=coe.ATTACH_TYPES,
                **attachType)
        self.effect = par.ParticleRef(**effect)
        self.timeStart = co_attribs.AttribNum(**timeStart)
        self.sound = audio.SoundRef(**sound)
        self.gainedCredits = co_attribs.AttribNum(**gainedCredits)
        self.gainedMetal = co_attribs.AttribNum(**gainedMetal)
        self.gainedCrystal = co_attribs.AttribNum(**gainedCrystal)
        self.timeScuttle = co_attribs.AttribNum(**timeScuttle)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.ruinType==other.ruinType)
                        and (self.attachType==other.attachType)
                        and (self.effect==other.effect)
                        and (self.timeStart==other.timeStart)
                        and (self.sound==other.sound)
                        and (self.gainedCredits==other.gainedCredits)
                        and (self.gainedMetal==other.gainedMetal)
                        and (self.gainedCrystal==other.gainedCrystal)
                        and (self.timeScuttle==other.timeScuttle))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.ruinType.toString(1)
        res += self.attachType.toString(1)
        res += self.effect.toString(1)
        res += self.timeStart.toString(1)
        res += self.sound.toString(1)
        res += self.gainedCredits.toString(1)
        res += self.gainedMetal.toString(1)
        res += self.gainedCrystal.toString(1)
        res += self.timeScuttle.toString(1)
        return indentText(res,indention)

"""############################## Properties ###############################"""

class CelestialBody(coe.HasAbilities):
    def __init__(self,moveArea,hyperspaceExit,wormhole,maxStarbases,maxMines,
            **kwargs):
        antimatterRestore = {"identifier":"","val":0.0}
        antimatterMax = {"identifier":"","val":0.0}
        super().__init__(antimatterMax=antimatterMax,
                antimatterRestore=antimatterRestore,**kwargs)
        self.moveArea = co_attribs.AttribNum(**moveArea)
        self.hyperspaceExit = co_attribs.AttribNum(**hyperspaceExit)
        self.wormhole = co_attribs.AttribBool(**wormhole)
        self.maxStarbases = co_attribs.AttribNum(**maxStarbases)
        self.maxMines = co_attribs.AttribNum(**maxMines)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.moveArea==other.moveArea)
                        and (self.hyperspaceExit==other.hyperspaceExit)
                        and (self.wormhole==other.wormhole)
                        and (self.maxStarbases==other.maxStarbases)
                        and (self.maxMines==other.maxMines))

"""############################ File Classes ###############################"""

class PlanetBonus(coe.Entity,coe.UIVisible):
    parser = g_PlanetBonusEntity

    def __init__(self,minUpgLvl,maxUpgLvl,floatBoni,intBoni,dlcID,**kwargs):
        super().__init__(**kwargs)
        self.minUpgLvl = co_attribs.AttribNum(**minUpgLvl)
        self.maxUpgLvl = co_attribs.AttribNum(**maxUpgLvl)
        self.dlcID = co_attribs.AttribNum(**dlcID)
        self.floatBoni = [co_attribs.AttribNum(**bonus) for bonus in floatBoni]
        self.intBoni = [co_attribs.AttribNum(**bonus) for bonus in intBoni]

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive=False)
        if self.minUpgLvl.value>self.maxUpgLvl.value:
            res.append(
                    co_probs.BasicProblem(severity=co_probs.ProblemSeverity.error,
                    desc="minUpgradeLevelNeeded ("+str(
                            self.minUpgLvl.value)+") > "
                         +"maxUpgradeLevelNeeded ("+str(
                            self.maxUpgLvl.value)+")",
                    probLine=self.minUpgLvl.linenum))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.minUpgLvl.toString(0)
        res += self.maxUpgLvl.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        for bonus in self.floatBoni:
            res += bonus.toString(0)
        for bonus in self.intBoni:
            res += bonus.toString(0)
        res += self.dlcID.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        return indentText(res,indention)

class Star(CelestialBody,coe.MainViewSelectable,coe.Entity):
    parser = g_StarEntity

    def __init__(self,coronaStartColor,coronaEndColor,coronaStartColorDist,
            coronaEndColorDist,iconFar,coloredSkyboxes,deepSpaceSkyboxes,
            textureIcon,dlcID,mesh,maxVisibilityDist,**kwargs):
        descString = {"identifier":"","val":""}
        nameString = {"identifier":"","val":""}
        super().__init__(descString=descString,nameString=nameString,**kwargs)
        self.nameString = None
        self.descString = None
        self.coronaStartColor = co_attribs.AttribColor(**coronaStartColor)
        self.coronaEndColor = co_attribs.AttribColor(**coronaEndColor)
        self.coronaStartColorDist = co_attribs.AttribNum(**coronaStartColorDist)
        self.coronaEndColorDist = co_attribs.AttribNum(**coronaEndColorDist)
        self.iconFar = ui.BrushRef(**iconFar)
        self.coloredSkyboxes = co_attribs.AttribList(elemType=MeshInfoStar,
                **coloredSkyboxes)
        self.deepSpaceSkyboxes = co_attribs.AttribList(elemType=MeshInfoStar,
                **deepSpaceSkyboxes)
        self.textureIcon = ui.UITextureRef(**textureIcon)
        self.dlcID = co_attribs.AttribNum(**dlcID)
        self.mesh = meshes.MeshRef(**mesh)
        self.maxVisibilityDist = co_attribs.AttribNum(**maxVisibilityDist)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.coronaStartColor==other.coronaStartColor)
                        and (self.coronaEndColor==other.coronaEndColor)
                        and (
                        self.coronaStartColorDist==other.coronaStartColorDist)
                        and (self.coronaEndColorDist==other.coronaEndColorDist)
                        and (self.iconFar==other.iconFar)
                        and (self.coloredSkyboxes==other.coloredSkyboxes)
                        and (self.deepSpaceSkyboxes==other.deepSpaceSkyboxes)
                        and (self.textureIcon==other.textureIcon)
                        and (self.dlcID==other.dlcID)
                        and (self.mesh==other.mesh)
                        and (self.maxVisibilityDist==other.maxVisibilityDist))

    def toString(self,indention):
        res = super().toString(0)
        res += self.coronaStartColor.toString(0)
        res += self.coronaEndColor.toString(0)
        res += self.coronaStartColorDist.toString(0)
        res += self.coronaEndColorDist.toString(0)
        res += self.iconFar.toString(0)
        res += self.coloredSkyboxes.toString(0)
        res += self.deepSpaceSkyboxes.toString(0)
        res += self.textureIcon.toString(0)
        res += self.dlcID.toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.zoomDist.toString(0)
        res += self.moveArea.toString(0)
        res += self.hyperspaceExit.toString(0)
        res += self.wormhole.toString(0)
        res += self.maxStarbases.toString(0)
        res += self.maxMines.toString(0)
        res += self.iconMainView.toString(0)
        res += self.pic.toString(0)
        res += self.mesh.toString(0)
        res += self.maxVisibilityDist.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)

class Planet(coe.Entity,CelestialBody,hit.HasShieldVisuals):
    parser = g_PlanetEntity

    def __init__(self,meshes,colorGlow,colorRing,textureCloud,resourceSetup,
            isAsteroid,healthRegen,upgrades,chanceRings,isColonizable,
            researchType,skyboxScalars,planetBonusesReq,planetBonusesRand,
            sndAmbience,renderVolcanic,ruin,dlcID,**kwargs):
        super().__init__(**kwargs)
        self.meshes = co_attribs.AttribList(elemType=MeshInfoPlanet,**meshes)
        self.colorGlow = co_attribs.AttribColor(**colorGlow)
        self.colorRing = co_attribs.AttribColor(**colorRing)
        self.textureCloud = ui.UITextureRef(canBeEmpty=True,**textureCloud)
        self.resourceSetup = ResourceSetup(**resourceSetup)
        self.isAsteroid = co_attribs.AttribBool(**isAsteroid)
        self.healthRegen = co_attribs.AttribNum(**healthRegen)
        self.upgrades = PlanetUpgrades(**upgrades)
        self.chanceRings = co_attribs.AttribNum(**chanceRings)
        self.isColonizable = co_attribs.AttribBool(**isColonizable)
        self.researchType = co_attribs.AttribEnum(
                possibleVals=coe.PLANETTEYPE_FOR_RESEARCH,
                **researchType)
        self.skyboxScalars = co_attribs.AttribList(elemType=SkyboxScalar,
                **skyboxScalars)
        self.planetBonusesReq = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["PlanetBonus"]},
                **planetBonusesReq)
        self.planetBonusesRand = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["PlanetBonus"]},
                **planetBonusesRand
        )
        self.sndAmbience = audio.SoundRef(canBeEmpty=True,**sndAmbience)
        self.renderVolcanic = co_attribs.AttribBool(**renderVolcanic)
        self.ruin = PlanetRuin(**ruin)
        self.dlcID = co_attribs.AttribNum(**dlcID)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.meshes==other.meshes)
                        and (self.colorGlow==other.colorGlow)
                        and (self.colorRing==other.colorRing)
                        and (self.textureCloud==other.textureCloud)
                        and (self.resourceSetup==other.resourceSetup)
                        and (self.isAsteroid==other.isAsteroid)
                        and (self.healthRegen==other.healthRegen)
                        and (self.upgrades==other.upgrades)
                        and (self.chanceRings==other.chanceRings)
                        and (self.isColonizable==other.isColonizable)
                        and (self.researchType==other.researchType)
                        and (self.skyboxScalars==other.skyboxScalars)
                        and (self.planetBonusesReq==other.planetBonusesReq)
                        and (self.planetBonusesRand==other.planetBonusesRand)
                        and (self.sndAmbience==other.sndAmbience)
                        and (self.renderVolcanic==other.renderVolcanic)
                        and (self.ruin==other.ruin)
                        and (self.dlcID==other.dlcID))

    def toString(self,indention):
        res = super().toString(0)
        res += self.meshes.toString(0)
        res += self.zoomDist.toString(0)
        res += self.colorGlow.toString(0)
        res += self.colorRing.toString(0)
        res += self.textureCloud.toString(0)
        res += self.resourceSetup.toString(0)
        res += self.isAsteroid.toString(0)
        res += self.healthRegen.toString(0)
        res += self.upgrades.toString(0)
        res += self.chanceRings.toString(0)
        res += self.isColonizable.toString(0)
        res += self.researchType.toString(0)
        res += self.skyboxScalars.toString(0)
        res += self.planetBonusesReq.toString(0)
        res += self.planetBonusesRand.toString(0)
        res += self.sndAmbience.toString(0)
        res += self.renderVolcanic.toString(0)
        res += self.ruin.toString(0)
        res += self.dlcID.toString(0)
        res += self.shieldMesh.toString(0)
        res += self.shieldRender.toString(0)
        res += self.moveArea.toString(0)
        res += self.hyperspaceExit.toString(0)
        res += self.wormhole.toString(0)
        res += self.maxStarbases.toString(0)
        res += self.maxMines.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        return indentText(res,indention)
