"""This module contains common parser elements and classes for entities.
"""

import itertools as it
import os.path

import pyparsing as pp
import tqdm

import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
import pysoase.common.problems as co_probs
import pysoase.entities.buffs_abilities as abilities
import pysoase.entities.common as coe
import pysoase.entities.misc as misc
import pysoase.entities.orbitals as orb
import pysoase.entities.orbitbodies as orbitbo
import pysoase.entities.player as player
import pysoase.entities.research as research
import pysoase.entities.ships as ships
import pysoase.entities.smallcraft as scraft
import pysoase.entities.uncolonizables as uncol
import pysoase.entities.upgrades as upgrades

"""######################### Attribute Extraction ##########################"""

g_attribute = pp.Word(pp.alphanums+"_"+":")+pp.restOfLine().suppress()
g_attribute_simple = (pp.LineStart().leaveWhitespace()
                      +pp.Word(pp.alphanums+"_"+":")
                      +pp.restOfLine().suppress())

def extract_attributes(entity_file,simple):
    if not simple:
        g_line = coe.g_header.setResultsName("type") | g_attribute
    else:
        g_line = coe.g_header.setResultsName("type") | g_attribute_simple
    with open(entity_file,'r',encoding="utf-8-sig") as f:
        res = g_line.searchString(f.read())
    entity_type = res[0][0][1]
    res = list(it.chain.from_iterable(res[1:]))
    res.append("entityType")
    return entity_type,set(res)

"""############################## ModEntities ##############################"""

class ModEntities(co_basics.ModComponent):
    ENT_TO_CLASS = {
        "Ability":abilities.Ability,
        "Buff":abilities.Buff,
        "CannonShell":misc.CannonShell,
        "CapitalShip":ships.CapitalShip,
        "Debris":misc.Debris,
        "EntryVehicle":scraft.EntryVehicle,
        "Fighter":scraft.Fighter,
        "Frigate":ships.Frigate,
        "PipCloud":misc.PipCloud,
        "Planet":orbitbo.Planet,
        "PlanetBonus":orbitbo.PlanetBonus,
        "PlanetModuleHangarDefense":orb.PlanetModuleHangarDefense,
        "PlanetModuleStandard":orb.PlanetModuleStandard,
        "PlanetModuleShipFactory":orb.PlanetModuleFactory,
        "PlanetModuleRefinery":orb.PlanetModuleRefinery,
        "PlanetModuleTradePort":orb.PlanetModuleTradePort,
        "PlanetModuleWeaponDefense":orb.PlanetModuleWeaponDefense,
        "Player":player.Player,
        "Quest":misc.Quest,
        "ResearchSubject":research.ResearchSubject,
        "ResourceAsteroid":orb.ResourceAsteroid,
        "SpaceMine":scraft.SpaceMine,
        "Squad":scraft.Squad,
        "Star":orbitbo.Star,
        "StarBase":orb.Starbase,
        "StarBaseUpgrade":upgrades.StarbaseUpgrade,
        "Titan":ships.Titan,
        "TitanUpgrade":upgrades.TitanUpgrade
    }

    def __init__(self,currMod):
        super().__init__(currMod,"GameInfo","entity")
        self._asteroids = None
        self._clouds = None
        self._manifest = None
        self._entTypes = {key:list() for key in coe.POSSIBLE_ENTITY_TYPES}
        for entFile in self.compFilesMod:
            etype = coe.Entity.parseType(os.path.join(
                    self.mod.moddir,"GameInfo",entFile))
            if not isinstance(etype,list):
                self._entTypes[etype].append(entFile)

    @property
    def asteroids(self):
        if self._asteroids is None:
            inst,probs = self.loadFile(self.subName,
                    "AsteroidDef.asteroidDef",
                    uncol.AsteroidDef)
            if inst is None:
                self._asteroids = probs
            else:
                self._asteroids = inst
        return self._asteroids

    @property
    def clouds(self):
        if self._clouds is None:
            inst,probs = self.loadFile(self.subName,
                    "DustCloudsDef.renderingDef",
                    uncol.CloudDef)
            if inst is None:
                self._clouds = probs
            else:
                self._clouds = inst
        return self._clouds

    @property
    def manifest(self):
        if self._manifest is None:
            import pysoase.mod.manifest as manifest
            inst,probs = self.loadFile("","entity.manifest",
                    manifest.ManifestEntity)
            if inst is None:
                self._manifest = probs
            else:
                self._manifest = inst
        return self._manifest

    def checkEntity(self,name):
        probs = self.loadEntity(name)
        if not probs:
            probs.extend(self.compFilesMod[name].check(self.mod))
        probs.extend(self.checkManifest(name))
        return probs

    def checkEntityType(self,name,expectedTypes):
        if name not in self.compFilesMod:
            return coe.TypeCheckResult.na
        elif self.compFilesMod[name] is None:
            fsource,probs = self._checkFileExistence(self.subName,name)
            path = None
            if fsource==co_files.FileSource.mod:
                path = os.path.join(self.mod.moddir,self.subName,name)
            elif fsource==co_files.FileSource.rebref:
                path = os.path.join(self.mod.rebrefdir,self.subName,name)
            if path:
                etype = coe.Entity.parseType(path)
                if isinstance(etype,list):
                    return coe.TypeCheckResult.na
            else:
                return coe.TypeCheckResult.na
        elif isinstance(self.compFilesMod[name],list):
            return coe.TypeCheckResult.na
        else:
            etype = self.compFilesMod[name].etype.value
        return (coe.TypeCheckResult.valid if etype in expectedTypes
        else coe.TypeCheckResult.invalid)

    def checkManifest(self,entry):
        if isinstance(self.manifest,list):
            return self.manifest
        else:
            return self.manifest.checkEntry(entry)

    def check(self,strict=False):
        probs = []
        if isinstance(self.manifest,list):
            probs.extend(self.manifest)
        else:
            probs.extend(self.manifest.check(self.mod))

        if isinstance(self.asteroids,list):
            probs.extend(self.asteroids)
        else:
            probs.extend(self.asteroids.check(self.mod))

        if isinstance(self.clouds,list):
            probs.extend(self.clouds)
        else:
            probs.extend(self.clouds.check(self.mod))

        if strict and not isinstance(self.manifest,list):
            manList = {entry.ref.lower()
                for entry in self.manifest.entries.elements}
            entList = [ent for ent in self.compFilesMod
                if ent.lower() in manList]
            with tqdm.tqdm(disable=self.mod.quiet,total=len(entList),
                    desc="Checking Entities in Manifest") as pbarEnts:
                for ent in entList:
                    probs.extend(self.checkEntity(ent))
                    pbarEnts.update()
        else:
            probs.extend(self.checkType(self._entTypes))

        return probs

    def checkFile(self,filepath):
        res = super().checkFile(filepath)
        checkInst = None
        if os.path.basename(filepath)=="entity.manifest":
            checkInst = self.manifest
        elif os.path.basename(filepath)=="AsteroidDef.asteroidDef":
            checkInst = self.asteroids
        elif os.path.basename(filepath)=="DustCloudsDef.renderingDef":
            checkInst = self.clouds
        else:
            name,ext = os.path.splitext(filepath)
            if ext==".entity":
                checkInst = self.checkEntity(os.path.basename(filepath))
            else:
                raise RuntimeError("Files of type "+filepath
                                   +"are not handled by ModThemes.")
        if isinstance(checkInst,list):
            # There was a parsing problem with the given file, return it
            res.extend(checkInst)
        else:
            # There was no problem with the parsing, conduct the check
            res.extend(checkInst.check(self.mod))
        return res

    def checkType(self,entityTypes):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(entityTypes),
                desc="Checking entity types") as pbarTypes:
            for etype in entityTypes:
                with tqdm.tqdm(disable=self.mod.quiet,
                        total=len(self._entTypes[etype]),
                        desc="Checking "+etype+" entities",
                        leave=False) as pbarEnts:
                    for ent in self._entTypes[etype]:
                        probs.extend(self.checkEntity(ent))
                        pbarEnts.update()
                pbarTypes.update()
        return probs

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def getEntity(self,name):
        filename,ext = os.path.splitext(name)
        if not ext:
            name += ".entity"
        elif ext!=".entity":
            raise RuntimeError(ext+" is not a valid entity file ending.")
        self.loadEntity(name)
        return self.compFilesMod[name]

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Loading all entities") as pbar:
            for ent in self.compFilesMod:
                probs.extend(self.loadEntity(ent))
                pbar.update()
        return probs

    def loadEntity(self,name):
        probs = []
        if name not in self.compFilesMod or self.compFilesMod[name] is None:
            fsource,probs = self._checkFileExistence(self.subName,name)
            path = None
            inst = None
            entType = None
            if fsource==co_files.FileSource.mod:
                path = os.path.join(self.mod.moddir,self.subName,name)
            elif fsource==co_files.FileSource.rebref:
                path = os.path.join(self.mod.rebrefdir,self.subName,name)
            if path:
                path = co_files.correctCaseInsensitive(path)
                entType = coe.Entity.parseType(path)
                if isinstance(entType,list):
                    probs.extend(entType)
                    self.compFilesMod[name] = entType
                else:
                    if entType in self.ENT_TO_CLASS:
                        inst = self.ENT_TO_CLASS[entType].createInstance(path)
                        if isinstance(inst,list):
                            probs.extend(inst)
                            inst = None
                    else:
                        probs.append(co_probs.BasicProblem(
                                "Invalid Entity type "+entType,
                                severity=co_probs.ProblemSeverity.error,
                                probFile=os.path.basename(path)))
            if inst:
                self.compFilesMod[name] = inst
                if fsource==co_files.FileSource.rebref:
                    self._entTypes[entType].append(os.path.basename(
                            inst.filepath))
            else:
                self.compFilesMod[name] = probs
        elif isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name])
        return probs
