""" Contains parsers and classes for Sins entities which can be attacked.
"""
import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.visuals as vis

"""################################# Types #################################"""

ARMOR_TYPES = ["CapitalShip","Heavy","Light","Medium","Module","Pirate",
    "Titan","VeryHeavy","VeryLight"]

"""################################ Parsers ################################"""

"""### Basic Elements ###"""

g_explosion = co_parse.genStringAttrib("ExplosionName")("explosion")
g_voiceSounds = (
    co_parse.genListAttrib("NumSoundsFor:ONATTACKORDERISSUED",
            co_parse.genStringAttrib("SoundID"))("sndAttackOrder")
    +co_parse.genListAttrib("NumSoundsFor:ONCREATION",
            co_parse.genStringAttrib("SoundID"))("sndCreation")
    +co_parse.genListAttrib("NumSoundsFor:ONGENERALORDERISSUED",
            co_parse.genStringAttrib("SoundID"))("sndGeneralOrder")
    +co_parse.genListAttrib("NumSoundsFor:ONSELECTED",
            co_parse.genStringAttrib("SoundID"))("sndSelected")
    +co_parse.genListAttrib("NumSoundsFor:ONSTARTPHASEJUMP",
            co_parse.genStringAttrib("SoundID"))("sndPhaseJump")
)

"""### Hull & Shields ###"""

g_baseArmorPoints = co_parse.genDecimalAttrib("BaseArmorPoints")("armorPoints")
g_hullPoints = co_parse.genDecimalAttrib("MaxHullPoints")("hullPointsMax")
g_hullRestore = co_parse.genDecimalAttrib(
        "HullPointRestoreRate")("hullPointRestore")
g_maxMitigation = co_parse.genDecimalAttrib("maxMitigation")("maxMitigation")
g_maxShield = co_parse.genDecimalAttrib("MaxShieldPoints")("shieldPointsMax")
g_maxShieldMitigation = co_parse.genDecimalAttrib("maxShieldMitigation")(
        "shieldMitigation")
g_shieldRestore = co_parse.genDecimalAttrib("ShieldPointRestoreRate")(
        "shieldPointRestore")
g_exp = co_parse.genDecimalAttrib("experiencePointsForDestroying")("exp")
g_expCap = co_parse.genDecimalAttrib("ExperiencePointsForDestroying")("exp")
g_armorType = co_parse.genEnumAttrib("armorType",ARMOR_TYPES)("armorType")
g_shieldVisuals = (
    co_parse.genStringAttrib("ShieldMeshName")("shieldMesh")
    +co_parse.genBoolAttrib("renderShield")("shieldRender")
)

"""############################## Properties ###############################"""

class HasShieldVisuals(coe.HasAbilities):
    def __init__(self,shieldMesh,shieldRender,**kwargs):
        super().__init__(**kwargs)
        self.shieldRender = co_attribs.AttribBool(**shieldRender)
        if self.shieldRender.value:
            self.shieldMesh = meshes.MeshRef(**shieldMesh)
        else:
            self.shieldMesh = meshes.MeshRef(canBeEmpty=True,**shieldMesh)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.shieldMesh==other.shieldMesh)
                        and (self.shieldRender==other.shieldRender))

class Controllable(coe.MainViewSelectable,coe.HasAbilities):
    def __init__(self,sndAttackOrder,sndCreation,sndGeneralOrder,sndSelected,
            sndPhaseJump,**kwargs):
        super().__init__(**kwargs)
        self.sndAttackOrder = co_attribs.AttribList(elemType=audio.DialogueRef,
                **sndAttackOrder)
        self.sndCreation = co_attribs.AttribList(elemType=audio.DialogueRef,
                **sndCreation)
        self.sndGeneralOrder = co_attribs.AttribList(elemType=audio.DialogueRef,
                **sndGeneralOrder)
        self.sndSelected = co_attribs.AttribList(elemType=audio.DialogueRef,
                **sndSelected)
        self.sndPhaseJump = co_attribs.AttribList(elemType=audio.DialogueRef,
                **sndPhaseJump)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.sndAttackOrder==other.sndAttackOrder)
                        and (self.sndCreation==other.sndCreation)
                        and (self.sndGeneralOrder==other.sndGeneralOrder)
                        and (self.sndSelected==other.sndSelected)
                        and (self.sndPhaseJump==other.sndPhaseJump))

class Hitable(coe.MainViewVisible,coe.UIVisible):
    def __init__(self,armorType,armorPoints,explosion,exp,hullPointRestore,
            hullPointsMax,debrisLarge,debrisSmall,debrisSpecific,**kwargs):
        super().__init__(**kwargs)
        self.armorType = co_attribs.AttribEnum(possibleVals=ARMOR_TYPES,**armorType)
        self.armorPoints = co_attribs.AttribNum.factory(armorPoints)
        self.explosion = vis.ExplosionRef(**explosion)
        self.exp = co_attribs.AttribNum.factory(exp)
        self.hullPointRestore = co_attribs.AttribNum.factory(hullPointRestore)
        self.hullPointsMax = co_attribs.AttribNum.factory(hullPointsMax)
        self.debrisLarge = co_attribs.AttribNum(**debrisLarge)
        self.debrisSmall = co_attribs.AttribNum(**debrisSmall)
        self.debrisSpecific = co_attribs.AttribList(
                elemType=meshes.MeshRef,
                **debrisSpecific
        )

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.armorType==other.armorType)
                        and (self.armorPoints==other.armorPoints)
                        and (self.explosion==other.explosion)
                        and (self.exp==other.exp)
                        and (self.hullPointRestore==other.hullPointRestore)
                        and (self.hullPointsMax==other.hullPointsMax)
                        and (self.debrisLarge==other.debrisLarge)
                        and (self.debrisSmall==other.debrisSmall)
                        and (self.debrisSpecific==other.debrisSpecific))

class Shielded(Hitable,Controllable):
    def __init__(self,shieldPointsMax,shieldPointRestore,shieldMitigation,
            **kwargs):
        super().__init__(**kwargs)
        self.shieldPointsMax = co_attribs.AttribNum.factory(shieldPointsMax)
        self.shieldPointRestore = co_attribs.AttribNum.factory(shieldPointRestore)
        self.shieldMitigation = co_attribs.AttribNum.factory(shieldMitigation)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.shieldPointsMax==other.shieldPointsMax)
                        and (self.shieldPointRestore==other.shieldPointRestore)
                        and (self.shieldMitigation==other.shieldMitigation))
