""" Contains parsers and classes pertaining to small craft like fighters.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.entities.ships as ships
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""################################ Types ##################################"""

SQUADRON_ROLES = ["Bomber","Combat","MineLayer"]

"""################################ Parser #################################"""

"""### Basic Elements ###"""

g_armorPoints = co_parse.genDecimalAttrib("armorPoints")("armorPoints")
g_exp = co_parse.genDecimalAttrib("experiencePointsForDestroying")("exp")
g_hullPoints = co_parse.genDecimalAttrib("maxHullPoints")("hullPointsMax")
g_hullRestore = co_parse.genDecimalAttrib("hullPointRestoreRate")("hullPointRestore")

"""### Entry Vehicle Entity ###"""

g_EntryVehicleEntity = (
    coe.genEntityHeader("EntryVehicle")
    +co_parse.genStringAttrib("AtmosphereEntryEffectName")("atmoEntryEff")
    +co_parse.genBoolAttrib("HasSurfaceImpactEffect")("hasSurfImpact")
    +co_parse.genStringAttrib("SurfaceImpactEffectName")("surfImpactEff")
    +co_parse.genStringAttrib("SurfaceImpactSoundID")("surfImpactSnd")
    +co_parse.genStringAttrib("LaunchSoundID")("launchSnd")
    +coe.g_mass
    +coe.g_movement
    +co_parse.genStringAttrib("MeshName")("mesh")
    +co_parse.genStringAttrib("ExhaustParticleSystemName")("exhaustSys")
    +co_parse.genStringAttrib("ExhaustTrailTextureName")("exhaustTexture")
    +co_parse.genDecimalAttrib("ExhaustTrailWidth")("exhaustWidth")
    +co_parse.genStringAttrib("AtmosphereEntrySoundID")("atmoEntrySnd")
    +co_parse.genStringAttrib("SpaceTravelSoundID")("engineSound")
    +coe.g_shadows
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Fighter Entity ###"""
g_FighterEntity = (
    coe.genEntityHeader("Fighter")
    +armed.g_attackBehaviour
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +coe.g_zoomDist
    +coe.g_nameString
    +co_parse.genStringAttrib("counterDescriptionStringID")("descString")
    +armed.g_statType
    +co_parse.genDecimalAttrib("formationOffsetDistance")("formationOffset")
    +coe.g_mass
    +coe.g_movement
    +armed.g_armed
    +co_parse.genStringAttrib("meshName")("mesh")
    +co_parse.genStringAttrib("exhaustParticleSystemName")("exhaustSys")
    +hit.g_explosion
    +co_parse.genStringAttrib("exhaustTrailTextureName")("exhaustTexture")
    +co_parse.genDecimalAttrib("exhaustTrailWidth")("exhaustWidth")
    +co_parse.genStringAttrib("engineSoundID")("engineSound")
    +g_hullPoints
    +g_hullRestore
    +g_armorPoints
    +hit.g_maxMitigation
    +hit.g_exp
    +coe.g_shadows
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### SpaceMine ###"""

g_SpaceMineEntity = (
    coe.genEntityHeader("SpaceMine")
    +coe.g_abilities
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +coe.g_picture
    +coe.g_mainViewIcon
    +coe.g_zoomDist
    +co_parse.genStringAttrib("MeshName")("mesh")
    +co_parse.genDecimalAttrib("spawnActivationDelay")("spawnDelay")
    +co_parse.genDecimalAttrib("targetStartFollowRange")("followRangeStart")
    +co_parse.genDecimalAttrib("targetMaxFollowRange")("followRangeMax")
    +coe.g_nameString
    +pp.Group(
            co_parse.genHeading("scuttleValue")
            +coe.g_cost
    )("scuttleValue")
    +co_parse.genDecimalAttrib("baseScuttleTime")("scuttleTime")
    +hit.g_explosion
    +coe.g_shadows
    +coe.g_mass
    +coe.g_movement
    +hit.g_voiceSounds
    +g_hullPoints
    +g_hullRestore
    +co_parse.genDecimalAttrib("maxShieldPoints")("shieldPointsMax")
    +co_parse.genDecimalAttrib("shieldPointRestoreRate")("shieldPointRestore")
    +hit.g_maxShieldMitigation
    +g_armorPoints
    +hit.g_exp
    +coe.g_baseMaxAntimatter
    +coe.g_baseAntimatterRestore
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Squad Entity ###"""

g_SquadEntity = (
    coe.genEntityHeader("Squad")
    +armed.g_attackBehaviour
    +coe.g_uiIcons
    +co_parse.genStringAttrib("fighterEntityDef")("fighterDef")
    +co_parse.genStringAttrib("fighterIllusionEntityDef")("fighterIllusionDef")
    +co_parse.genDecimalAttrib("scuttleTime")("scuttleTime")
    +co_parse.genDecimalAttrib("fighterConstructionTime")("fighterBuildTime")
    +co_parse.genDecimalAttrib("decayWithoutOwnerRate")("decayWithoutOwner")
    +co_parse.genIntAttrib("baseMaxNumFighters")("numFighters")
    +co_parse.genEnumAttrib("role",SQUADRON_ROLES)("role")
    +armed.g_statType
    +coe.g_mainViewIcon
    +coe.g_picture
    +co_parse.genStringAttrib("launchIcon")("launchIcon")
    +co_parse.genStringAttrib("dockIcon")("dockIcon")
    +coe.g_nameString
    +coe.g_descriptionString
    +coe.g_researchPrereqs
    +hit.g_voiceSounds
    +coe.g_shadows
    +coe.g_abilities
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################## Properties ###############################"""

class Smallcraft(ships.Moving):
    def __init__(self,mesh,exhaustTexture,exhaustWidth,**kwargs):
        super().__init__(**kwargs)
        self.mesh = meshes.MeshRef(**mesh)
        self.exhaustTexture = ui.UITextureRef(**exhaustTexture)
        self.exhaustWidth = co_attribs.AttribNum(**exhaustWidth)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.mesh==other.mesh)
                        and (self.exhaustTexture==other.exhaustTexture)
                        and (self.exhaustWidth==other.exhaustWidth))

"""############################# File Classes ##############################"""

class EntryVehicle(coe.Entity,Smallcraft):
    parser = g_EntryVehicleEntity

    def __init__(self,atmoEntryEff,hasSurfImpact,surfImpactEff,surfImpactSnd,
            launchSnd,atmoEntrySnd,**kwargs):
        super().__init__(zoomDist={"identifier":"","val":0.0},**kwargs)
        self.atmoEntryEff = par.ParticleRef(**atmoEntryEff)
        self.hasSurfImpact = co_attribs.AttribBool(**hasSurfImpact)
        self.surfImpactEff = par.ParticleRef(**surfImpactEff)
        self.surfImpactSnd = audio.SoundRef(**surfImpactSnd)
        self.launchSnd = audio.SoundRef(**launchSnd)
        self.atmoEntrySnd = audio.SoundRef(**atmoEntrySnd)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.atmoEntryEff==other.atmoEntryEff)
                        and (self.hasSurfImpact==other.hasSurfImpact)
                        and (self.surfImpactEff==other.surfImpactEff)
                        and (self.surfImpactSnd==other.surfImpactSnd)
                        and (self.launchSnd==other.launchSnd)
                        and (self.atmoEntrySnd==other.atmoEntrySnd))

    def toString(self,indention):
        res = super().toString(0)
        res += self.atmoEntryEff.toString(0)
        res += self.hasSurfImpact.toString(0)
        res += self.surfImpactEff.toString(0)
        res += self.surfImpactSnd.toString(0)
        res += self.launchSnd.toString(0)
        res += self.mass.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        res += self.mesh.toString(0)
        res += self.exhaustSys.toString(0)
        res += self.exhaustTexture.toString(0)
        res += self.exhaustWidth.toString(0)
        res += self.atmoEntrySnd.toString(0)
        res += self.engineSound.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)

class Squad(coe.Entity,hit.Controllable,armed.HasAttackBehavior):
    parser = g_SquadEntity

    def __init__(self,fighterDef,fighterIllusionDef,scuttleTime,
            fighterBuildTime,decayWithoutOwner,numFighters,role,statcount,
            launchIcon,dockIcon,prereqs,**kwargs):
        super().__init__(
                zoomDist={"identifier":"","val":0.0},
                antimatterMax={"identifier":"","val":0.0},
                antimatterRestore={"identifier":"","val":0.0},
                **kwargs)
        self.fighterDef = coe.EntityRef(types=["Fighter"],**fighterDef)
        self.fighterIllusionDef = coe.EntityRef(types=["Fighter"],
                **fighterIllusionDef)
        self.scuttleTime = co_attribs.AttribNum(**scuttleTime)
        self.fighterBuildTime = co_attribs.AttribNum(**fighterBuildTime)
        self.decayWithoutOwner = co_attribs.AttribNum(**decayWithoutOwner)
        self.numFighters = co_attribs.AttribNum(**numFighters)
        self.role = co_attribs.AttribEnum(possibleVals=SQUADRON_ROLES,**role)
        self.statcount = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,
                **statcount)
        self.launchIcon = ui.BrushRef(**launchIcon)
        self.dockIcon = ui.BrushRef(**dockIcon)
        self.prereqs = coe.ResearchPrerequisite(**prereqs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.fighterDef==other.fighterDef)
                        and (self.fighterIllusionDef==other.fighterIllusionDef)
                        and (self.scuttleTime==other.scuttleTime)
                        and (self.fighterBuildTime==other.fighterBuildTime)
                        and (self.decayWithoutOwner==other.decayWithoutOwner)
                        and (self.numFighters==other.numFighters)
                        and (self.role==other.role)
                        and (self.statcount==other.statcount)
                        and (self.launchIcon==other.launchIcon)
                        and (self.dockIcon==other.dockIcon)
                        and (self.prereqs==other.prereqs))

    def toString(self,indention):
        res = super().toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.fighterDef.toString(0)
        res += self.fighterIllusionDef.toString(0)
        res += self.scuttleTime.toString(0)
        res += self.fighterBuildTime.toString(0)
        res += self.decayWithoutOwner.toString(0)
        res += self.numFighters.toString(0)
        res += self.role.toString(0)
        res += self.statcount.toString(0)
        res += self.iconMainView.toString(0)
        res += self.pic.toString(0)
        res += self.launchIcon.toString(0)
        res += self.dockIcon.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.prereqs.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        return indentText(res,indention)

class SpaceMine(coe.Entity,ships.Moving,hit.Shielded):
    parser = g_SpaceMineEntity

    def __init__(self,mesh,spawnDelay,followRangeStart,followRangeMax,
            scuttleValue,scuttleTime,**kwargs):
        super().__init__(exhaustSys={"identifier":"","val":""},
                descString={"identifier":"","val":""},
                engineSound={"identifier":"","val":""},**kwargs)
        self.mesh = meshes.MeshRef(**mesh)
        self.spawnDelay = co_attribs.AttribNum(**spawnDelay)
        self.followRangeStart = co_attribs.AttribNum(**followRangeStart)
        self.followRangeMax = co_attribs.AttribNum(**followRangeMax)
        self.scuttleValue = coe.Cost(**scuttleValue)
        self.scuttleTime = co_attribs.AttribNum(**scuttleTime)
        # These two attributes, inherited from ships.Moving, are not used 
        # by the SpaceMine entity
        self.descString = None
        self.exhaustSys = None
        self.engineSound = None

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.mesh==other.mesh)
                        and (self.spawnDelay==other.spawnDelay)
                        and (self.followRangeStart==other.followRangeStart)
                        and (self.followRangeMax==other.followRangeMax)
                        and (self.scuttleValue==other.scuttleValue)
                        and (self.scuttleTime==other.scuttleTime))

    def toString(self,indention):
        res = super().toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.pic.toString(0)
        res += self.iconMainView.toString(0)
        res += self.zoomDist.toString(0)
        res += self.mesh.toString(0)
        res += self.spawnDelay.toString(0)
        res += self.followRangeStart.toString(0)
        res += self.followRangeMax.toString(0)
        res += self.nameString.toString(0)
        res += self.scuttleValue.toString(0)
        res += self.scuttleTime.toString(0)
        res += self.explosion.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        res += self.mass.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.hullPointsMax.toString(0)
        res += self.hullPointRestore.toString(0)
        res += self.shieldPointsMax.toString(0)
        res += self.shieldPointRestore.toString(0)
        res += self.shieldMitigation.toString(0)
        res += self.armorPoints.toString(0)
        res += self.exp.toString(0)
        res += self.antimatterMax.toString(0)
        res += self.antimatterRestore.toString(0)
        return indentText(res,indention)

class Fighter(coe.Entity,Smallcraft,armed.Armed):
    parser = g_FighterEntity

    def __init__(self,formationOffset,statcount,maxMitigation,**kwargs):
        super().__init__(**kwargs)
        self.formationOffset = co_attribs.AttribNum(**formationOffset)
        self.statcount = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,
                **statcount)
        self.maxMitigation = co_attribs.AttribNum(**maxMitigation)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.formationOffset==other.formationOffset)
                        and (self.statcount==other.statcount)
                        and (self.maxMitigation==other.maxMitigation))

    def toString(self,indention):
        res = super().toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.zoomDist.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.statcount.toString(0)
        res += self.formationOffset.toString(0)
        res += self.mass.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        res += armed.Armed.toString(self,0)
        res += self.mesh.toString(0)
        res += self.exhaustSys.toString(0)
        res += self.explosion.toString(0)
        res += self.exhaustTexture.toString(0)
        res += self.exhaustWidth.toString(0)
        res += self.engineSound.toString(0)
        res += self.hullPointsMax.toString(0)
        res += self.hullPointRestore.toString(0)
        res += self.armorPoints.toString(0)
        res += self.maxMitigation.toString(0)
        res += self.exp.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        return indentText(res,indention)
