"""Contains parsers and classes to handle Player entities.
"""

import collections

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui

"""################################# Parser ################################"""

g_planetModuleInfo = pp.Group(
        co_parse.genHeading("planetModuleInfo")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),"Page:0")(
                "page0")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),"Page:1")(
                "page1")
)("planetModules")
g_titanInfo = pp.Group(
        co_parse.genHeading("titanInfo")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),"Page:0")(
                "page0")
)("titans")
g_capShipInfo = pp.Group(
        co_parse.genHeading("capitalShipInfo")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),"Page:0")(
                "page0")
)("capitals")
g_frigateInfo = pp.Group(
        co_parse.genHeading("frigateInfo")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),"Page:0")(
                "page0")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),"Page:1")(
                "page1")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),
                "NotOnPage")(
                "noPage")
)("frigates")
g_playerBuildables = pp.Group(
        co_parse.genHeading("entities")
        +g_planetModuleInfo
        +g_titanInfo
        +g_capShipInfo
        +g_frigateInfo
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),
                "researchInfo")(
                "research")
        +co_parse.genListAttrib("count",
                co_parse.genStringAttrib("entityDefName"),
                "starBaseInfo")(
                "starbases")
        +co_parse.genStringAttrib("flagship")("flagship")
)("buildables")
g_debrisData = pp.Group(
        co_parse.genHeading("debrisData")
        +co_parse.genListAttrib("numRandomMeshNamesLarge",
                co_parse.genStringAttrib("randomMeshName"))("meshesLarge")
        +co_parse.genListAttrib("numRandomMeshNamesSmall",
                co_parse.genStringAttrib("randomMeshName"))("meshesSmall")
)("debrisData")
g_hyperspaceData = pp.Group(
        co_parse.genHeading("hyperspaceData")
        +pp.Group(
                co_parse.genStringAttrib("frigateChargeUpEffectName")("chargeUp")
                +co_parse.genStringAttrib("frigateChargeUpEffectNameBetweenStars")(
                        "chargeUpBetweenStars")
                +co_parse.genStringAttrib("frigateChargeUpEffectNameDestabilized")(
                        "chargeUpDestabilized")
                +co_parse.genStringAttrib("frigateChargeUpEffectSoundID")(
                        "chargeUpSound")
                +co_parse.genStringAttrib("frigateTravelEffectName")("travel")
                +co_parse.genStringAttrib("frigateTravelEffectNameBetweenStars")(
                        "travelBetweenStars")
                +co_parse.genStringAttrib("frigateTravelEffectNameDestabilized")(
                        "travelDestabilized")
                +co_parse.genStringAttrib("frigateTravelEffectSoundID")("travelSound")
                +co_parse.genStringAttrib("frigateExitEffectName")("exit")
                +co_parse.genStringAttrib("frigateExitEffectSoundID")("exitSound")
        )("frigates")
        +pp.Group(
                co_parse.genStringAttrib("capitalShipChargeUpEffectName")("chargeUp")
                +co_parse.genStringAttrib(
                        "capitalShipChargeUpEffectNameBetweenStars")(
                        "chargeUpBetweenStars")
                +co_parse.genStringAttrib(
                        "capitalShipChargeUpEffectNameDestabilized")(
                        "chargeUpDestabilized")
                +co_parse.genStringAttrib("capitalShipChargeUpEffectSoundID")(
                        "chargeUpSound")
                +co_parse.genStringAttrib("capitalShipTravelEffectName")("travel")
                +co_parse.genStringAttrib("capitalShipTravelEffectNameBetweenStars")(
                        "travelBetweenStars")
                +co_parse.genStringAttrib("capitalShipTravelEffectNameDestabilized")(
                        "travelDestabilized")
                +co_parse.genStringAttrib("capitalShipTravelEffectSoundID")(
                        "travelSound")
                +co_parse.genStringAttrib("capitalShipExitEffectName")("exit")
                +co_parse.genStringAttrib("capitalShipExitEffectSoundID")("exitSound")
        )("capitals")
        +pp.Group(
                co_parse.genStringAttrib("titanChargeUpEffectName")("chargeUp")
                +co_parse.genStringAttrib("titanChargeUpEffectNameBetweenStars")(
                        "chargeUpBetweenStars")
                +co_parse.genStringAttrib("titanChargeUpEffectNameDestabilized")(
                        "chargeUpDestabilized")
                +co_parse.genStringAttrib("titanChargeUpEffectSoundID")(
                        "chargeUpSound")
                +co_parse.genStringAttrib("titanTravelEffectName")("travel")
                +co_parse.genStringAttrib("titanTravelEffectNameBetweenStars")(
                        "travelBetweenStars")
                +co_parse.genStringAttrib("titanTravelEffectNameDestabilized")(
                        "travelDestabilized")
                +co_parse.genStringAttrib("titanTravelEffectSoundID")("travelSound")
                +co_parse.genStringAttrib("titanExitEffectName")("exit")
                +co_parse.genStringAttrib("titanExitEffectSoundID")("exitSound")
        )("titans")
        +co_parse.genDecimalAttrib("baseHyperspaceSpeed")("baseHyperSpeed")
        +co_parse.genDecimalAttrib("hyperspaceSpeedBetweenStarsMultiplier")(
                "speedBetweenStarsMult")
        +co_parse.genDecimalAttrib("hyperspaceSpeedToFriendlyOrbitBodiesMultiplier")(
                "speedFriendlyGravWellMult")
        +co_parse.genDecimalAttrib("baseHyperspaceAntiMatterCost")(
                "baseHyperspaceAMCost")
        +co_parse.genDecimalAttrib("baseHyperspaceChargeUpTime")(
                "baseHyperChargeupTime")
)("hyperspaceData")
g_shieldData = pp.Group(
        co_parse.genHeading("shieldData")
        +co_parse.genDecimalAttrib("shieldAbsorbGrowthPerDamage")("absorbPerDmg")
        +co_parse.genDecimalAttrib("shieldAbsorbDecayRate")("absorbDecay")
        +co_parse.genDecimalAttrib("shieldAbsorbBaseMin")("absorbMin")
        +co_parse.genColorAttrib("shieldColor")("color")
)("shieldData")
g_gameEventData = pp.Group(
        co_parse.genHeading("gameEventData")
        +pp.Group(
                co_parse.genStringAttrib("GameEventSound:AllianceCeaseFireBroken")
                +co_parse.genStringAttrib("GameEventSound:AllianceCeaseFireFormed")
                +co_parse.genStringAttrib("GameEventSound:AllianceCeaseFireOffered")
                +co_parse.genStringAttrib("GameEventSound:AllianceOtherBroken")
                +co_parse.genStringAttrib("GameEventSound:AllianceOtherFormed")
                +co_parse.genStringAttrib("GameEventSound:AllianceOtherOffered")
                +co_parse.genStringAttrib("GameEventSound:AllyBackUpArrived")
                +co_parse.genStringAttrib("GameEventSound:AllyBackUpEnRoute")
                +co_parse.genStringAttrib("GameEventSound:AllyTitanQueued")
                +co_parse.genStringAttrib("GameEventSound:AllyTitanCompleted")
                +co_parse.genStringAttrib("GameEventSound:EnemyTitanQueued")
                +co_parse.genStringAttrib("GameEventSound:EnemyTitanCompleted")
                +co_parse.genStringAttrib("GameEventSound:AllyTitanLost")
                +co_parse.genStringAttrib("GameEventSound:AllyCapitalShipLost")
                +co_parse.genStringAttrib("GameEventSound:AllyFleetArrived")
                +co_parse.genStringAttrib("GameEventSound:AllyPlanetLost")
                +co_parse.genStringAttrib("GameEventSound:AllyPlanetUnderAttack")
                +co_parse.genStringAttrib("GameEventSound:AllyRequestAttackPlanet")
                +co_parse.genStringAttrib("GameEventSound:AllyRequestDefendPlanet")
                +co_parse.genStringAttrib("GameEventSound:AllyUnitUnderAttack")
                +co_parse.genStringAttrib("GameEventSound:BountyAllDepleted")
                +co_parse.genStringAttrib("GameEventSound:BountyIncreasedOther")
                +co_parse.genStringAttrib("GameEventSound:BountyIncreasedPlayer")
                +co_parse.genStringAttrib("GameEventSound:BuyNoCommand")
                +co_parse.genStringAttrib("GameEventSound:BuyNoCredits")
                +co_parse.genStringAttrib("GameEventSound:BuyNoMetal")
                +co_parse.genStringAttrib("GameEventSound:BuyNoCrystal")
                +co_parse.genStringAttrib("GameEventSound:BuyNoFrigateFactory")
                +co_parse.genStringAttrib("GameEventSound:BuyNoCapitalShipFactory")
                +co_parse.genStringAttrib("GameEventSound:BuyNoTitanFactory")
                +co_parse.genStringAttrib("GameEventSound:BuyNoAvailableShipSlots")
                +co_parse.genStringAttrib(
                        "GameEventSound:BuyNoAvailableCapitalShipSlots")
                +co_parse.genStringAttrib("GameEventSound:BuyAlreadyHasTitan")
                +co_parse.genStringAttrib("GameEventSound:CannonShellDetected")
                +co_parse.genStringAttrib("GameEventSound:RandomEventDetected")
                +co_parse.genStringAttrib("GameEventSound:CannonUnderConstruction")
                +co_parse.genStringAttrib("GameEventSound:ColonizePlanetAlreadyOwned")
                +co_parse.genStringAttrib(
                        "GameEventSound:ColonizePlanetNotColonizable")
                +co_parse.genStringAttrib(
                        "GameEventSound:ColonizePlanetAlreadyBeingColonized")
                +co_parse.genStringAttrib(
                        "GameEventSound:ColonizeNeedPrerequisiteResearch")
                +co_parse.genStringAttrib(
                        "GameEventSound:ColonizePlanetHasTakeoverCulture")
                +co_parse.genStringAttrib("GameEventSound:ColonizePlanetHasDebuff")
                +co_parse.genStringAttrib(
                        "GameEventSound:ColonizePlanetBlockedByEnemyStarBase")
                +co_parse.genStringAttrib("GameEventSound:RuinPlanetNotRuinable")
                +co_parse.genStringAttrib("GameEventSound:PirateFleetArrived")
                +co_parse.genStringAttrib("GameEventSound:PirateRaidImminent")
                +co_parse.genStringAttrib("GameEventSound:PirateRaidLaunched")
                +co_parse.genStringAttrib("GameEventSound:HostileFleetArrived")
                +co_parse.genStringAttrib("GameEventSound:HostileFleetEnRoute")
                +co_parse.genStringAttrib("GameEventSound:HostilePlanetDestroyed")
                +co_parse.genStringAttrib("GameEventSound:ModuleComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:MoveCannotTravelBetweenPlanets")
                +co_parse.genStringAttrib("GameEventSound:NoValidPath")
                +co_parse.genStringAttrib("GameEventSound:NoValidPathObserved")
                +co_parse.genStringAttrib("GameEventSound:NoValidPathBetweenStars")
                +co_parse.genStringAttrib("GameEventSound:NoValidPathDueToBuff")
                +co_parse.genStringAttrib("GameEventSound:Ping")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlanetUpgradeAlreadyMaxLevel")
                +co_parse.genStringAttrib("GameEventSound:PlanetUpgradeComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlanetUpgradeMaxLevelQueued")
                +co_parse.genStringAttrib("GameEventSound:NewPlanetDiscovered")
                +co_parse.genStringAttrib("GameEventSound:ArtifactDiscovered")
                +co_parse.genStringAttrib("GameEventSound:ArtifactDiscoveredOther")
                +co_parse.genStringAttrib("GameEventSound:BonusDiscovered")
                +co_parse.genStringAttrib("GameEventSound:NothingDiscovered")
                +co_parse.genStringAttrib(
                        "GameEventSound:LaunchSquadsDisabledByDebuff")
                +co_parse.genStringAttrib("GameEventSound:PlayerBackUpArrived")
                +co_parse.genStringAttrib("GameEventSound:PlayerTitanCreated")
                +co_parse.genStringAttrib("GameEventSound:PlayerTitanLost")
                +co_parse.genStringAttrib("GameEventSound:PlayerCapitalShipCreated")
                +co_parse.genStringAttrib("GameEventSound:PlayerCapitalShipLost")
                +co_parse.genStringAttrib("GameEventSound:PlayerStarbaseLost")
                +co_parse.genStringAttrib("GameEventSound:PlayerFleetArrived")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerFleetArrivedToFleetBeacon")
                +co_parse.genStringAttrib("GameEventSound:PlayerFleetDeparted")
                +co_parse.genStringAttrib("GameEventSound:PlayerFrigateCreated")
                +co_parse.genStringAttrib("GameEventSound:PlayerPlanetColonized")
                +co_parse.genStringAttrib("GameEventSound:PlayerCapitalPlanetChanged")
                +co_parse.genStringAttrib("GameEventSound:PlayerCapitalPlanetLost")
                +co_parse.genStringAttrib("GameEventSound:AllyCapitalPlanetLost")
                +co_parse.genStringAttrib("GameEventSound:EnemyCapitalPlanetLost")
                +co_parse.genStringAttrib("GameEventSound:PlayerPlanetLost")
                +co_parse.genStringAttrib("GameEventSound:PlayerPlanetLostToCulture")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerPlanetSoonLostToCulture")
                +co_parse.genStringAttrib("GameEventSound:PlayerPlanetUnderAttack")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerReceivedResourcesFromAlly")
                +co_parse.genStringAttrib("GameEventSound:PlayerUnitUnderAttack")
                +co_parse.genStringAttrib("GameEventSound:PlayerFlagshipUnderAttack")
                +co_parse.genStringAttrib("GameEventSound:PlayerFlagshipShieldsDown")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerFlagshipHullSeverelyDamaged")
                +co_parse.genStringAttrib("GameEventSound:PlayerFlagshipDestroyed")
                +co_parse.genStringAttrib("GameEventSound:AllyFlagshipDestroyed")
                +co_parse.genStringAttrib("GameEventSound:EnemyFlagshipDestroyed")
                +co_parse.genStringAttrib("GameEventSound:PlayerTitanShieldsDown")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerTitanHullSeverelyDamaged")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerCapitalShipShieldsDown")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerCapitalShipHullSeverelyDamaged")
                +co_parse.genStringAttrib("GameEventSound:AchievementCompleted")
                +co_parse.genStringAttrib("GameEventSound:QuestAdded")
                +co_parse.genStringAttrib("GameEventSound:QuestCompleted")
                +co_parse.genStringAttrib("GameEventSound:QuestFailed")
                +co_parse.genStringAttrib("GameEventSound:QuestEnded")
                +co_parse.genStringAttrib("GameEventSound:QuestRejected")
                +co_parse.genStringAttrib("GameEventSound:PlayerQuestAdded")
                +co_parse.genStringAttrib("GameEventSound:PlayerQuestCompleted")
                +co_parse.genStringAttrib("GameEventSound:PlayerQuestFailed")
                +co_parse.genStringAttrib("GameEventSound:DiplomaticVictoryHalfway")
                +co_parse.genStringAttrib("GameEventSound:DiplomaticVictoryComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:AllyDiplomaticVictoryHalfway")
                +co_parse.genStringAttrib(
                        "GameEventSound:AllyDiplomaticVictoryComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyDiplomaticVictoryHalfway")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyDiplomaticVictoryComplete")
                +co_parse.genStringAttrib("GameEventSound:OccupationVictoryStarted")
                +co_parse.genStringAttrib("GameEventSound:OccupationVictoryHalfway")
                +co_parse.genStringAttrib("GameEventSound:OccupationVictoryComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:AllyOccupationVictoryStarted")
                +co_parse.genStringAttrib(
                        "GameEventSound:AllyOccupationVictoryHalfway")
                +co_parse.genStringAttrib(
                        "GameEventSound:AllyOccupationVictoryComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyOccupationVictoryStarted")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyOccupationVictoryHalfway")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyOccupationVictoryComplete")
                +co_parse.genStringAttrib("GameEventSound:ResearchVictoryStarted")
                +co_parse.genStringAttrib("GameEventSound:ResearchVictoryHalfway")
                +co_parse.genStringAttrib("GameEventSound:ResearchVictoryComplete")
                +co_parse.genStringAttrib("GameEventSound:ResearchComplete")
                +co_parse.genStringAttrib("GameEventSound:AllyResearchVictoryStarted")
                +co_parse.genStringAttrib("GameEventSound:AllyResearchVictoryHalfway")
                +co_parse.genStringAttrib(
                        "GameEventSound:AllyResearchVictoryComplete")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyResearchVictoryStarted")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyResearchVictoryHalfway")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnemyResearchVictoryComplete")
                +co_parse.genStringAttrib("GameEventSound:ResearchAlreadyMaxLevel")
                +co_parse.genStringAttrib("GameEventSound:ResearchMaxLevelQueued")
                +co_parse.genStringAttrib(
                        "GameEventSound:ResearchNotEnoughPointsInTier")
                +co_parse.genStringAttrib("GameEventSound:ResearchNeedMoreLabModules")
                +co_parse.genStringAttrib("GameEventSound:ResearchNeedPrerequisite")
                +co_parse.genStringAttrib("GameEventSound:MoreModuleSlotsNeeded")
                +co_parse.genStringAttrib("GameEventSound:SpaceMineLimitReached")
                +co_parse.genStringAttrib(
                        "GameEventSound:StarBaseUpgradeNotEnoughUpgradePoints")
                +co_parse.genStringAttrib(
                        "GameEventSound:StarBaseUpgradeAlreadyMaxLevel")
                +co_parse.genStringAttrib(
                        "GameEventSound:StarBaseUpgradeMaxLevelQueued")
                +co_parse.genStringAttrib(
                        "GameEventSound:TitanUpgradeNotEnoughUpgradePoints")
                +co_parse.genStringAttrib(
                        "GameEventSound:TitanUpgradeAlreadyMaxLevel")
                +co_parse.genStringAttrib(
                        "GameEventSound:InvalidModulePlacementPosition")
                +co_parse.genStringAttrib(
                        "GameEventSound:AbilityUpgradeAlreadyMaxLevel")
                +co_parse.genStringAttrib(
                        "GameEventSound:AbilityUpgradeRequiresHigherLevel")
                +co_parse.genStringAttrib(
                        "GameEventSound:AbilityUpgradeNoUnspentPoints")
                +co_parse.genStringAttrib("GameEventSound:ShipUpgradeAlreadyMaxLevel")
                +co_parse.genStringAttrib(
                        "GameEventSound:ShipUpgradeAlreadyQueuedMaxLevel")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityCooldownNotExpired")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityDisabledByDebuff")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityNoAntimatter")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityNoUndockedSquadMembers")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityNotEnoughRoomForStarBase")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityCannotUseAbitraryLocationOnEntity")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityTooManyStarBases")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityMineIsNotActivated")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityTargetNotInRange")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityNoValidObjectType")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityPhaseSpaceTarget")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityNormalSpaceTarget")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintCanBeCaptured")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintCanHaveFighters")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintCanHaveShields")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintCanSpreadWeaponDamage")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintCanPhaseDodge")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasAbilityWithCooldown")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasAntimatter")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasPhaseMissiles")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasPopulation")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasEnergyWeapons")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasHullDamage")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasShieldDamage")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasAntiMatterShortage")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintHasWeapons")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsCargoShip")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsExplored")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsResourceExtractor")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsMovingToLocalOrbitBody")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsPsi")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintLinkedCargoShip")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintLinkedSquad")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintNotSelf")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintWithinSolarSystem")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintModuleUnderConstruction")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintModuleIsActivelyBeingConstructed")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintModuleNotActivelyBeingConstructed")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsNotHomeworld")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintNotInvulnerable")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsInFriendlyGravityWell")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetConstraintIsInNonFriendlyGravityWell")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityUnmetOwnerLinked")
                +co_parse.genStringAttrib("GameEventSound:UseAbilityUnmetOwnerPlayer")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetOwnerFriendly")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetOwnerHostile")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetOwnerNoOwner")
                +co_parse.genStringAttrib(
                        "GameEventSound:UseAbilityUnmetOwnerAlliedOrEnemy")
                +co_parse.genStringAttrib("GameEventSound:GameStart")
                +co_parse.genStringAttrib("GameEventSound:GameWin")
                +co_parse.genStringAttrib("GameEventSound:AllyGameWin")
                +co_parse.genStringAttrib("GameEventSound:CapitalShipLevelUp")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidWithNoReason")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsNotDamagableDueToBuffs")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsNotEnemy")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsCantFireAtFighters")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsCantMoveAndTargetNotInRange")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsCantMoveToTarget")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsCanOnlyAttackStructures")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsMustBeInSameOrbitWell")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsNoWeaponsToBombPlanet")
                +co_parse.genStringAttrib(
                        "GameEventSound:AttackTargetInvalidAsPlanetCantBeBombed")
                +co_parse.genStringAttrib("GameEventSound:ScuttleShipStarted")
                +co_parse.genStringAttrib("GameEventSound:ScuttleShipStopped")
                +co_parse.genStringAttrib("GameEventSound:ScuttlePlanetStarted")
                +co_parse.genStringAttrib("GameEventSound:ScuttlePlanetStopped")
                +co_parse.genStringAttrib("GameEventSound:MarketBoom")
                +co_parse.genStringAttrib("GameEventSound:MarketCrash")
                +co_parse.genStringAttrib(
                        "GameEventSound:PlayerCultureFlippedEnemyPlanet")
                +co_parse.genStringAttrib(
                        "GameEventSound:EnvoyTargetPlanetIsNowInvalid")
                +co_parse.genStringAttrib("GameEventSound:OffensiveMissionDiscovered")
                +co_parse.genStringAttrib("GameEventSound:MissionDiscovered")
        )("sounds")
)("gameEventData")
g_planetElevatorData = pp.Group(
        co_parse.genHeading("planetElevatorData")
        +co_parse.genStringAttrib("elevatorMeshName:TaxCenter")("taxCenter")
        +co_parse.genStringAttrib("elevatorMeshName:CultureCenter")(
                "cultureCenter")
        +co_parse.genStringAttrib("elevatorMeshName:MetalExtractor")(
                "metalExtractor")
        +co_parse.genStringAttrib("elevatorMeshName:CrystalExtractor")(
                "crystalExtractor")
        +co_parse.genStringAttrib("elevatorMeshName:Bunker")("bunker")
        +co_parse.genStringAttrib("elevatorMeshName:GoodsFactory")(
                "goodsFactory")
        +co_parse.genStringAttrib("elevatorMeshName:ProductionCenter")(
                "productionCenter")
)("planetElevatorData")
g_musicThemeData = pp.Group(
        co_parse.genHeading("musicThemeData")
        +co_parse.genListAttrib("startupMusicThemeCount",
                co_parse.genStringAttrib("musicTheme"))(
                "startup")
        +co_parse.genListAttrib("neutralMusicThemeCount",
                co_parse.genStringAttrib("musicTheme"))(
                "neutral")
        +co_parse.genListAttrib("scareThemeCount",
                co_parse.genStringAttrib("musicTheme"))(
                "scare")
        +co_parse.genListAttrib("announcementCount",
                co_parse.genStringAttrib("musicTheme"))(
                "announcement")
)("musicThemes")
g_hudSkinData = pp.Group(
        co_parse.genHeading("hudSkinData")
        +co_parse.genStringAttrib("loadScreenCharacterBrush")(
                "loadScreenCharacters")
        +co_parse.genListAttrib("fleetIconCount",
                co_parse.genStringAttrib("fleetIcon"))(
                "fleetIcons")
        +co_parse.genEnumAttrib("Bandbox",["BANDBOX"])("bandbox")
        +co_parse.genEnumAttrib("FogOfWar",["FogOfWar"])("fogOfWar")
        +pp.Group(
                co_parse.genStringAttrib("ActionGridSlotBackdrop")
                +co_parse.genStringAttrib("BottomWindowBackdrop")
                +co_parse.genStringAttrib("BottomWindowOverlay")
                +co_parse.genStringAttrib("TopWindowBackdrop")
                +co_parse.genStringAttrib("TopWindowOverlay")
                +co_parse.genStringAttrib("InfoCardAttachedToBottomBackdrop")
                +co_parse.genStringAttrib("InfoCardAttachedToScreenEdgeBackdrop")
                +co_parse.genStringAttrib("TopWindowLeftCapLowRes")
                +co_parse.genStringAttrib("TopWindowLeftCapHighRes")
                +co_parse.genStringAttrib("TopWindowRightCapLowRes")
                +co_parse.genStringAttrib("TopWindowRightCapHighRes")
                +co_parse.genStringAttrib("EmpireWindowBackdrop")
                +co_parse.genStringAttrib("EmpireWindowHighResBackdrop")
                +co_parse.genStringAttrib("QuickMarketBackdrop")
                +co_parse.genStringAttrib("QuickMarketBackdropNoPirates")
                +co_parse.genStringAttrib("UnknownPlanetHUDIcon")
                +co_parse.genStringAttrib("UnknownPlanetHUDSmallIcon")
                +co_parse.genStringAttrib("UnknownPlanetInfoCardIcon")
                +co_parse.genStringAttrib("UnknownPlanetMainViewIcon")
                +co_parse.genStringAttrib("UnknownPlanetPicture")
                +co_parse.genStringAttrib("AutoAttackRangeStateMixed")
                +co_parse.genStringAttrib("AutoAttackRangeStateAllNone")
                +co_parse.genStringAttrib("AutoAttackRangeStateAllLocalArea")
                +co_parse.genStringAttrib("AutoAttackRangeStateAllGravityWell")
                +co_parse.genStringAttrib("CohesionRangeStateMixed")
                +co_parse.genStringAttrib("CohesionRangeStateAllNone")
                +co_parse.genStringAttrib("CohesionRangeStateAllNear")
                +co_parse.genStringAttrib("CohesionRangeStateAllFar")
                +co_parse.genStringAttrib("GroupMoveStateNone")
                +co_parse.genStringAttrib("GroupMoveStateSome")
                +co_parse.genStringAttrib("GroupMoveStateAll")
                +co_parse.genStringAttrib("ZoomInstantContextNear")
                +co_parse.genStringAttrib("ZoomInstantContextFar")
                +co_parse.genStringAttrib("GameEventCategoryPing")
                +co_parse.genStringAttrib("GameEventCategoryProduction")
                +co_parse.genStringAttrib("GameEventCategoryAlliance")
                +co_parse.genStringAttrib("GameEventCategoryThreat")
                +co_parse.genStringAttrib("HUDIconQuest")
                +co_parse.genStringAttrib("HUDIconHappiness")
                +co_parse.genStringAttrib("HUDIconOverlayNotEnoughCredits")
                +co_parse.genStringAttrib("HUDIconOverlayNotEnoughMetal")
                +co_parse.genStringAttrib("HUDIconOverlayNotEnoughCrystal")
                +co_parse.genStringAttrib("HUDIconOverlayNotResearched")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetUpgradePathLastStageUpgraded")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetModuleNoAvailableResourceAsteroids")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetModuleNoTacticalSlotsAvailable")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetModuleNoCivilianSlotsAvailable")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetBuildShipNoFrigateFactories")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetBuildShipNoCapitalShipFactories")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetBuildShipNotEnoughShipSlots")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlots")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints")
                +co_parse.genStringAttrib(
                        "HUDIconOverlayStarBaseUpgradePathLastStageUpgraded")
                +co_parse.genStringAttrib("HUDIconOverlayInFleet")
                +co_parse.genStringAttrib("AllianceHUDIconTrade")
                +co_parse.genStringAttrib("AllianceHUDIconExternalVision")
                +co_parse.genStringAttrib("AllianceHUDIconInternalVision")
                +co_parse.genStringAttrib("AllianceHUDIconCeaseFire")
                +co_parse.genStringAttrib("AllianceHUDIconPeaceTreaty")
                +co_parse.genStringAttrib("AllianceHUDIconResearchPact")
                +co_parse.genStringAttrib("AllianceHUDIconEfficiencyPact")
                +co_parse.genStringAttrib("AllianceHUDIconAntimatterPact")
                +co_parse.genStringAttrib("AllianceHUDIconArmorPact")
                +co_parse.genStringAttrib("AllianceHUDIconBeamWeaponPact")
                +co_parse.genStringAttrib("AllianceHUDIconCulturePact")
                +co_parse.genStringAttrib("AllianceHUDIconMetalCrystlPact")
                +co_parse.genStringAttrib("AllianceHUDIconThrusterPact")
                +co_parse.genStringAttrib("AllianceHUDIconPhaseJumpPact")
                +co_parse.genStringAttrib("AllianceHUDIconPlanetBombingPact")
                +co_parse.genStringAttrib("AllianceHUDIconShieldPact")
                +co_parse.genStringAttrib("AllianceHUDIconMarketPact")
                +co_parse.genStringAttrib("AllianceHUDIconEnergyWeaponCooldownPact")
                +co_parse.genStringAttrib("AllianceHUDIconMassReductionPact")
                +co_parse.genStringAttrib("AllianceHUDIconSupplyPact")
                +co_parse.genStringAttrib("AllianceHUDIconTacticalSlotsPact")
                +co_parse.genStringAttrib("AllianceHUDIconWeaponCooldownPact")
                +co_parse.genStringAttrib("AllianceHUDIconTradeIncomePact")
                +co_parse.genStringAttrib("ResourceHUDIconCredits")
                +co_parse.genStringAttrib("ResourceHUDIconMetal")
                +co_parse.genStringAttrib("ResourceHUDIconCrystal")
                +co_parse.genStringAttrib("ResourceInfoCardIconCredits")
                +co_parse.genStringAttrib("ResourceInfoCardIconMetal")
                +co_parse.genStringAttrib("ResourceInfoCardIconCrystal")
                +co_parse.genStringAttrib("ResearchFieldButtonIconCombat")
                +co_parse.genStringAttrib("ResearchFieldButtonIconDefense")
                +co_parse.genStringAttrib("ResearchFieldButtonIconNonCombat")
                +co_parse.genStringAttrib("ResearchFieldButtonIconDiplomacy")
                +co_parse.genStringAttrib("ResearchFieldButtonIconFleet")
                +co_parse.genStringAttrib("ResearchFieldButtonIconArtifact")
                +co_parse.genStringAttrib("QuestStatusIconBombAnyPlanet")
                +co_parse.genStringAttrib("QuestStatusIconKillShips")
                +co_parse.genStringAttrib("QuestStatusIconKillCapitalShips")
                +co_parse.genStringAttrib("QuestStatusIconKillCivilianStructures")
                +co_parse.genStringAttrib("QuestStatusIconKillTacticalStructures")
                +co_parse.genStringAttrib("QuestStatusIconSendEnvoy")
                +co_parse.genStringAttrib("OrderInfoCardIconNone")
                +co_parse.genStringAttrib("OrderInfoCardIconAttack")
                +co_parse.genStringAttrib("OrderInfoCardIconMove")
                +co_parse.genStringAttrib("OrderInfoCardIconAbility")
                +co_parse.genStringAttrib("OrderInfoCardIconRetreat")
                +co_parse.genStringAttrib("InfoCardIconAutoCast")
                +co_parse.genStringAttrib("InfoCardIconCombinedPlanetIncome")
                +co_parse.genStringAttrib("InfoCardIconCombinedStarBaseIncome")
                +co_parse.genStringAttrib("InfoCardIconFleetUpkeep")
                +co_parse.genStringAttrib("InfoCardIconCapitalShip")
                +co_parse.genStringAttrib("InfoCardIconFrigate")
                +co_parse.genStringAttrib("InfoCardIconTradeShip")
                +co_parse.genStringAttrib("InfoCardIconRefineryShip")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel0")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel1")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel2")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel3")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel4")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel5")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel6")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel7")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel8")
                +co_parse.genStringAttrib("MainViewIconOverlayCapitalShipLevel9")
                +co_parse.genStringAttrib("EmpireWindowFindIconOwnedColonyShips")
                +co_parse.genStringAttrib("EmpireWindowFindIconOwnedScoutShips")
                +co_parse.genStringAttrib("EmpireWindowFindIconOwnedCapitalShips")
                +co_parse.genStringAttrib("EmpireWindowFindIconOwnedStarbases")
                +co_parse.genStringAttrib("EmpireWindowFindIconOwnedEnvoys")
                +co_parse.genStringAttrib(
                        "EmpireWindowFindIconOwnedPlanetsWithResourceAsteroids")
                +co_parse.genStringAttrib(
                        "EmpireWindowFindIconOwnedPlanetsWithFactories")
                +co_parse.genStringAttrib(
                        "EmpireWindowFindIconOwnedPlanetsWithUpgrades")
                +co_parse.genStringAttrib(
                        "EmpireWindowFindIconOwnedPlanetsWithCivilianSlots")
                +co_parse.genStringAttrib("EmpireWindowFindIconPlanetsWithArtifacts")
                +co_parse.genStringAttrib("UserActionIconAttack")
                +co_parse.genStringAttrib("UserActionIconStop")
                +co_parse.genStringAttrib("UserActionIconCreateFleet")
                +co_parse.genStringAttrib("UserActionIconDisbandFleet")
                +co_parse.genStringAttrib("UserActionIconRetreat")
                +co_parse.genStringAttrib("UserActionIconSetRallyPoint")
                +co_parse.genStringAttrib("UserActionIconOpenModulePage0")
                +co_parse.genStringAttrib("UserActionIconOpenModulePage1")
                +co_parse.genStringAttrib("UserActionIconCloseModulePage0")
                +co_parse.genStringAttrib("UserActionIconCloseModulePage1")
                +co_parse.genStringAttrib("UserActionIconOpenPlanetTitanManagement")
                +co_parse.genStringAttrib("UserActionIconClosePlanetTitanManagement")
                +co_parse.genStringAttrib(
                        "UserActionIconOpenPlanetCapitalShipManagement")
                +co_parse.genStringAttrib(
                        "UserActionIconClosePlanetCapitalShipManagement")
                +co_parse.genStringAttrib(
                        "UserActionIconOpenPlanetFrigateManagement0")
                +co_parse.genStringAttrib(
                        "UserActionIconOpenPlanetFrigateManagement1")
                +co_parse.genStringAttrib(
                        "UserActionIconClosePlanetFrigateManagement0")
                +co_parse.genStringAttrib(
                        "UserActionIconClosePlanetFrigateManagement1")
                +co_parse.genStringAttrib("UserActionIconOpenSquadManagement")
                +co_parse.genStringAttrib("UserActionIconCloseSquadManagement")
                +co_parse.genStringAttrib("UserActionIconOpenShipAbilityManagement")
                +co_parse.genStringAttrib("UserActionIconCloseShipAbilityManagement")
                +co_parse.genStringAttrib("UserActionIconOpenShipTacticsManagement")
                +co_parse.genStringAttrib("UserActionIconCloseShipTacticsManagement")
                +co_parse.genStringAttrib("UserActionIconPurchaseCapitalShipLevel")
                +co_parse.genStringAttrib("UserActionIconOpenPlanetUpgradeManagement")
                +co_parse.genStringAttrib(
                        "UserActionIconClosePlanetUpgradeManagement")
                +co_parse.genStringAttrib(
                        "UserActionIconOpenStarBaseUpgradeManagement")
                +co_parse.genStringAttrib(
                        "UserActionIconCloseStarBaseUpgradeManagement")
                +co_parse.genStringAttrib("UserActionIconUpgradePlanetPopulation")
                +co_parse.genStringAttrib("UserActionIconUpdatePlanetInfrastructure")
                +co_parse.genStringAttrib("UserActionIconUpgradePlanetArtifactLevel")
                +co_parse.genStringAttrib("UserActionIconUpgradePlanetHome")
                +co_parse.genStringAttrib(
                        "UserActionIconUpgradePlanetCivilianModules")
                +co_parse.genStringAttrib(
                        "UserActionIconUpgradePlanetTacticalModules")
                +co_parse.genStringAttrib("UserActionIconUpgradePlanetSocial")
                +co_parse.genStringAttrib("UserActionIconUpgradePlanetIndustry")
                +co_parse.genStringAttrib("UserActionIconUpgradePlanetSmuggler")
                +co_parse.genStringAttrib(
                        "UserActionIconBuildResourceAsteroidExtractor")
                +co_parse.genStringAttrib("UserActionIconPing")
                +co_parse.genStringAttrib("UserActionIconPingAllyAttack")
                +co_parse.genStringAttrib("UserActionIconPingAllyDefend")
                +co_parse.genStringAttrib("UserActionIconPingAllyStop")
                +co_parse.genStringAttrib("UserActionIconToggleZoomToCursor")
                +co_parse.genStringAttrib("UserActionIconToggleScuttle")
                +co_parse.genStringAttrib("UserActionIconOpenRenameWindow")
                +co_parse.genStringAttrib("UserActionIconChangeAutoAttackRange")
                +co_parse.genStringAttrib("UserActionIconChangeCohesionRange")
                +co_parse.genStringAttrib("UserActionIconOpenEscapeMenuScreen")
                +co_parse.genStringAttrib("UserActionIconOpenNPCScreen")
                +co_parse.genStringAttrib("UserActionIconOpenRelationshipsScreen")
                +co_parse.genStringAttrib("UserActionIconOpenResearchScreen")
                +co_parse.genStringAttrib("UserActionIconOpenPlayerScreen")
                +co_parse.genStringAttrib("UserActionIconToggleEmpireWindowStacked")
                +co_parse.genStringAttrib(
                        "UserActionIconToggleIsAutoPlaceModuleActive")
                +co_parse.genStringAttrib("UserActionIconIncreaseGameSpeed")
                +co_parse.genStringAttrib("UserActionIconDecreaseGameSpeed")
        )("hudIcons")
)("hudSkinData")
g_researchBlock = pp.Group(
        co_parse.genHeading("block")
        +pp.Group(
                co_parse.g_linestart
                +pp.Keyword("area")("identifier")
                +co_parse.g_space
                +co_parse.g_pos4("coord")
                +co_parse.g_eol
        )("area")
        +co_parse.genStringAttrib("backdrop")("backdrop")
        +co_parse.genStringAttrib("tierIndicatorBackdrop")("indicatorBackdrop")
        +co_parse.genStringAttrib("title")("title")
        +co_parse.genStringAttrib("picture")("picture")
)
g_researchData = pp.Group(
        co_parse.genHeading("researchScreenData")
        +co_parse.genListAttrib("blockCount",g_researchBlock,"Combat")("combat")
        +co_parse.genListAttrib("blockCount",g_researchBlock,"Defense")("defense")
        +co_parse.genListAttrib("blockCount",g_researchBlock,"NonCombat")("nonCombat")
        +co_parse.genListAttrib("blockCount",g_researchBlock,"Diplomacy")("diplomacy")
        +co_parse.genListAttrib("blockCount",g_researchBlock,"Artifact")("artifact")
        +co_parse.genListAttrib("blockCount",g_researchBlock,"Fleet")("fleet")
        +pp.Group(
                co_parse.genStringAttrib("capitalShipSlotLevelPicture-0")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-1")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-2")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-3")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-4")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-5")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-6")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-7")
                +co_parse.genStringAttrib("capitalShipSlotLevelPicture-8")
        )("capShipSlotIcons")
        +pp.Group(
                co_parse.genStringAttrib("shipSlotLevelPicture-0")
                +co_parse.genStringAttrib("shipSlotLevelPicture-1")
                +co_parse.genStringAttrib("shipSlotLevelPicture-2")
                +co_parse.genStringAttrib("shipSlotLevelPicture-3")
                +co_parse.genStringAttrib("shipSlotLevelPicture-4")
                +co_parse.genStringAttrib("shipSlotLevelPicture-5")
                +co_parse.genStringAttrib("shipSlotLevelPicture-6")
                +co_parse.genStringAttrib("shipSlotLevelPicture-7")
                +co_parse.genStringAttrib("shipSlotLevelPicture-8")
        )("shipSlotIcons")
)("research")
g_gameOverData = pp.Group(
        co_parse.genHeading("gameOverWindowData")
        +co_parse.genStringAttrib("hasWonPicture")("picHasWon")
        +co_parse.genStringAttrib("hasLostPicture")("picHasLost")
        +co_parse.genStringAttrib("themeBackdropBrush")("themeBackdrop")
        +pp.Group(
                co_parse.genStringAttrib("hasWonMilitaryStatusFormatStringId")
                +co_parse.genStringAttrib("hasLostMilitaryStatusFormatStringId")
                +co_parse.genStringAttrib("hasWonFlagshipStatusFormatStringId")
                +co_parse.genStringAttrib("hasLostFlagshipStatusFormatStringId")
                +co_parse.genStringAttrib("hasWonHomeworldStatusFormatStringId")
                +co_parse.genStringAttrib("hasLostHomeworldStatusFormatStringId")
                +co_parse.genStringAttrib("hasWonDiplomaticStatusFormatStringId")
                +co_parse.genStringAttrib("hasLostDiplomaticStatusFormatStringId")
                +co_parse.genStringAttrib("hasWonResearchStatusFormatStringId")
                +co_parse.genStringAttrib("hasLostResearchStatusFormatStringId")
                +co_parse.genStringAttrib("hasWonOccupationStatusFormatStringId")
                +co_parse.genStringAttrib("hasLostOccupationStatusFormatStringId")
        )("formatStrings")
        +pp.Group(
                co_parse.genStringAttrib("hasWonMilitaryPhraseStringId")
                +co_parse.genStringAttrib("hasLostMilitaryPhraseStringId")
                +co_parse.genStringAttrib("hasWonFlagshipPhraseStringId")
                +co_parse.genStringAttrib("hasLostFlagshipPhraseStringId")
                +co_parse.genStringAttrib("hasWonHomeworldPhraseStringId")
                +co_parse.genStringAttrib("hasLostHomeworldPhraseStringId")
                +co_parse.genStringAttrib("hasWonDiplomaticPhraseStringId")
                +co_parse.genStringAttrib("hasLostDiplomaticPhraseStringId")
                +co_parse.genStringAttrib("hasWonResearchPhraseStringId")
                +co_parse.genStringAttrib("hasLostResearchPhraseStringId")
                +co_parse.genStringAttrib("hasWonOccupationPhraseStringId")
                +co_parse.genStringAttrib("hasLostOccupationPhraseStringId")
        )("phraseStrings")
)("gameOver")
g_factionRelationMod = pp.Group(
        co_parse.genHeading("factionRelationsModifier")
        +co_parse.genStringAttrib("factionNameID")("factionName")
        +co_parse.genStringAttrib("otherFactionNameID")("otherFactionName")
        +co_parse.genDecimalAttrib("modifierMin")("modMin")
        +co_parse.genDecimalAttrib("modifierMax")("modMax")
)
g_raceRelationMod = pp.Group(
        co_parse.genHeading("raceRelationsModifier")
        +co_parse.genStringAttrib("raceNameID")("raceName")
        +co_parse.genDecimalAttrib("modifierMin")("modMin")
        +co_parse.genDecimalAttrib("modifierMax")("modMax")
        +co_parse.genListAttrib("factionRelationsModCount",g_factionRelationMod)(
                "factionRelations")
)
g_PlayerEntity = (
        coe.genEntityHeader("Player")
        +co_parse.genStringAttrib("raceNameStringID")("raceName")
        +co_parse.genBoolAttrib("canColonize")("canColonize")
        +co_parse.genBoolAttrib("isPsi")("isPsi")
        +co_parse.genBoolAttrib("isSelectable")("selectable")
        +co_parse.genIntAttrib("selectablePriorty")("selectPriority")
        +co_parse.genStringAttrib("randomCapitalShipNamePrefix")("randCapNamePrefix")
        +co_parse.genStringAttrib("randomStarBaseNamePrefix")("randStarbaseNamePrefix")
    +g_playerBuildables
    +g_debrisData
    +g_hyperspaceData
    +g_shieldData
    +g_gameEventData
    +g_planetElevatorData
    +g_musicThemeData
    +g_hudSkinData
    +g_researchData
    +g_gameOverData
        +co_parse.genListAttrib("nameCount",
            co_parse.genStringAttrib("name"),"aiNameData")(
            "aiNames")
        +co_parse.genStringAttrib("raceNameParsePrefix")("raceNameParsePrefix")
        +co_parse.genStringAttrib("raceNameParsePrefixFallback")("raceNameParseFallback")
        +co_parse.genListAttrib("numResearchFields",
            co_parse.genStringAttrib("field"))(
            "researchFields")
        +co_parse.genStringAttrib("playerThemeGroupName")("playerTheme")
        +co_parse.genStringAttrib("playerPictureGroupName")("playerPicture")
        +co_parse.genListAttrib("raceRelationsModCount",g_raceRelationMod)("relations")
        +co_parse.genStringAttrib("defaultFactionNameID")("defaultFactionName")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

class BuildPages(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,types,page0,page1=None,noPage=None,**kwargs):
        super().__init__(**kwargs)
        self.page0 = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":types},
                **page0
        )
        if page1:
            self.page1 = co_attribs.AttribList(
                    elemType=coe.EntityRef,
                    elemArgs={"types":types},
                    **page1
            )
        else:
            self.page1 = None
        if noPage:
            self.noPage = co_attribs.AttribList(
                    elemType=coe.EntityRef,
                    elemArgs={"types":types},
                    **noPage
            )
        else:
            self.noPage = None

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.page0==other.page0)
                        and (self.page1==other.page1)
                        and (self.noPage==other.noPage))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.page0.toString(1)
        if self.page1:
            res += self.page1.toString(1)
        if self.noPage:
            res += self.noPage.toString(1)
        return indentText(res,indention)

class PlayerBuildables(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,planetModules,titans,capitals,frigates,research,
            starbases,flagship,**kwargs):
        super().__init__(**kwargs)
        self.planetModules = BuildPages(
                types=["PlanetModuleHangarDefense","PlanetModuleRefinery",
                    "PlanetModuleShipFactory","PlanetModuleStandard",
                    "PlanetModuleTradePort","PlanetModuleWeaponDefense"],
                **planetModules)
        self.titans = BuildPages(types=["Titan"],**titans)
        self.capitals = BuildPages(types=["CapitalShip"],**capitals)
        self.frigates = BuildPages(types=["Frigate"],**frigates)
        self.research = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["ResearchSubject"]},
                **research)
        self.starbases = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["StarBase"]},
                **starbases)
        self.flagship = coe.EntityRef(types=["Titan","Frigate","CapitalShip"],
                **flagship)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.planetModules==other.planetModules)
                        and (self.titans==other.titans)
                        and (self.capitals==other.capitals)
                        and (self.frigates==other.frigates)
                        and (self.research==other.research)
                        and (self.starbases==other.starbases)
                        and (self.flagship==other.flagship))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.planetModules.toString(1)
        res += self.titans.toString(1)
        res += self.capitals.toString(1)
        res += self.frigates.toString(1)
        res += self.research.toString(1)
        res += self.starbases.toString(1)
        res += self.flagship.toString(1)
        return indentText(res,indention)

class DebrisData(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,meshesLarge,meshesSmall,**kwargs):
        super().__init__(**kwargs)
        self.meshesLarge = co_attribs.AttribList(elemType=meshes.MeshRef,**meshesLarge)
        self.meshesSmall = co_attribs.AttribList(elemType=meshes.MeshRef,**meshesSmall)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.meshesLarge==other.meshesLarge)
                        and (self.meshesSmall==other.meshesSmall))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.meshesLarge.toString(1)
        res += self.meshesSmall.toString(1)
        return indentText(res,indention)

class HyperspaceEffects(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,chargeUp,chargeUpBetweenStars,chargeUpDestabilized,
            chargeUpSound,travel,travelBetweenStars,travelDestabilized,
            travelSound,exit,exitSound,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.chargeUp = par.ParticleRef(**chargeUp)
        self.chargeUpBetweenStars = par.ParticleRef(**chargeUpBetweenStars)
        self.chargeUpDestabilized = par.ParticleRef(**chargeUpDestabilized)
        self.chargeUpSound = audio.SoundRef(**chargeUpSound)
        self.travel = par.ParticleRef(**travel)
        self.travelBetweenStars = par.ParticleRef(**travelBetweenStars)
        self.travelDestabilized = par.ParticleRef(**travelDestabilized)
        self.travelSound = audio.SoundRef(**travelSound)
        self.exit = par.ParticleRef(**exit)
        self.exitSound = audio.SoundRef(**exitSound)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.chargeUp==other.chargeUp)
                        and (
                        self.chargeUpBetweenStars==other.chargeUpBetweenStars)
                        and (
                        self.chargeUpDestabilized==other.chargeUpDestabilized)
                        and (self.chargeUpSound==other.chargeUpSound)
                        and (self.travel==other.travel)
                        and (self.travelBetweenStars==other.travelBetweenStars)
                        and (self.travelDestabilized==other.travelDestabilized)
                        and (self.travelSound==other.travelSound)
                        and (self.exit==other.exit)
                        and (self.exitSound==other.exitSound))

    def toString(self,indention):
        res = self.chargeUp.toString(0)
        res += self.chargeUpBetweenStars.toString(0)
        res += self.chargeUpDestabilized.toString(0)
        res += self.chargeUpSound.toString(0)
        res += self.travel.toString(0)
        res += self.travelBetweenStars.toString(0)
        res += self.travelDestabilized.toString(0)
        res += self.travelSound.toString(0)
        res += self.exit.toString(0)
        res += self.exitSound.toString(0)
        return indentText(res,indention)

class HyperspaceData(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,frigates,capitals,titans,baseHyperSpeed,
            speedBetweenStarsMult,speedFriendlyGravWellMult,
            baseHyperspaceAMCost,baseHyperChargeupTime,**kwargs):
        super().__init__(**kwargs)
        self.frigates = HyperspaceEffects(**frigates)
        self.capitals = HyperspaceEffects(**capitals)
        self.titans = HyperspaceEffects(**titans)
        self.baseHyperSpeed = co_attribs.AttribNum(**baseHyperSpeed)
        self.speedBetweenStarsMult = co_attribs.AttribNum(**speedBetweenStarsMult)
        self.speedFriendlyGravWellMult = co_attribs.AttribNum(
                **speedFriendlyGravWellMult)
        self.baseHyperspaceAMCost = co_attribs.AttribNum(**baseHyperspaceAMCost)
        self.baseHyperChargeupTime = co_attribs.AttribNum(**baseHyperChargeupTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.frigates==other.frigates)
                        and (self.capitals==other.capitals)
                        and (self.titans==other.titans)
                        and (self.baseHyperSpeed==other.baseHyperSpeed)
                        and (
                        self.speedBetweenStarsMult==other.speedBetweenStarsMult)
                        and (
                        self.speedFriendlyGravWellMult==other.speedFriendlyGravWellMult)
                        and (
                        self.baseHyperspaceAMCost==other.baseHyperspaceAMCost)
                        and (
                        self.baseHyperChargeupTime==other.baseHyperChargeupTime))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.frigates.toString(1)
        res += self.capitals.toString(1)
        res += self.titans.toString(1)
        res += self.baseHyperSpeed.toString(1)
        res += self.speedBetweenStarsMult.toString(1)
        res += self.speedFriendlyGravWellMult.toString(1)
        res += self.baseHyperspaceAMCost.toString(1)
        res += self.baseHyperChargeupTime.toString(1)
        return indentText(res,indention)

class ShieldData(co_attribs.Attribute):
    def __init__(self,absorbPerDmg,absorbDecay,absorbMin,color,**kwargs):
        super().__init__(**kwargs)
        self.absorbPerDmg = co_attribs.AttribNum(**absorbPerDmg)
        self.absorbDecay = co_attribs.AttribNum(**absorbDecay)
        self.absorbMin = co_attribs.AttribNum(**absorbMin)
        self.color = co_attribs.AttribColor(**color)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.absorbPerDmg==other.absorbPerDmg)
                        and (self.absorbDecay==other.absorbDecay)
                        and (self.absorbMin==other.absorbMin)
                        and (self.color==other.color))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.absorbPerDmg.toString(1)
        res += self.absorbDecay.toString(1)
        res += self.absorbMin.toString(1)
        res += self.color.toString(1)
        return indentText(res,indention)

class GameEventData(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,sounds,**kwargs):
        super().__init__(**kwargs)
        self.sounds = collections.OrderedDict()
        for elem in [audio.DialogueRef(**val) for val in sounds]:
            self.sounds[elem.identifier.split(":")[1]] = elem

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.sounds==other.sounds)

    def __getattr__(self,name):
        if name in self.sounds:
            return self.sounds[name]
        else:
            raise AttributeError("Unknown attribute: "+name)

    def getRefs(self):
        res = super().getRefs()
        res.extend(self.sounds.values())
        return res

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for val in self.sounds.values():
            res.extend(val.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        for val in self.sounds.values():
            res += val.toString(1)
        return indentText(res,indention)

class PlanetElevatorData(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,taxCenter,cultureCenter,metalExtractor,crystalExtractor,
            bunker,goodsFactory,productionCenter,**kwargs):
        super().__init__(**kwargs)
        self.taxCenter = meshes.MeshRef(**taxCenter)
        self.cultureCenter = meshes.MeshRef(**cultureCenter)
        self.metalExtractor = meshes.MeshRef(**metalExtractor)
        self.crystalExtractor = meshes.MeshRef(**crystalExtractor)
        self.bunker = meshes.MeshRef(**bunker)
        self.goodsFactory = meshes.MeshRef(**goodsFactory)
        self.productionCenter = meshes.MeshRef(**productionCenter)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.taxCenter==other.taxCenter)
                        and (self.cultureCenter==other.cultureCenter)
                        and (self.metalExtractor==other.metalExtractor)
                        and (self.crystalExtractor==other.crystalExtractor)
                        and (self.bunker==other.bunker)
                        and (self.goodsFactory==other.goodsFactory)
                        and (self.productionCenter==other.productionCenter))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.taxCenter.toString(1)
        res += self.cultureCenter.toString(1)
        res += self.metalExtractor.toString(1)
        res += self.crystalExtractor.toString(1)
        res += self.bunker.toString(1)
        res += self.goodsFactory.toString(1)
        res += self.productionCenter.toString(1)
        return indentText(res,indention)

class MusicThemesData(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,startup,neutral,scare,announcement,**kwargs):
        super().__init__(**kwargs)
        self.startup = co_attribs.AttribList(elemType=audio.MusicRef,**startup)
        self.neutral = co_attribs.AttribList(elemType=audio.MusicRef,**neutral)
        self.scare = co_attribs.AttribList(elemType=audio.MusicRef,**scare)
        self.announcement = co_attribs.AttribList(elemType=audio.MusicRef,
                **announcement)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.startup==other.startup)
                        and (self.neutral==other.neutral)
                        and (self.scare==other.scare)
                        and (self.announcement==other.announcement))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.startup.toString(1)
        res += self.neutral.toString(1)
        res += self.scare.toString(1)
        res += self.announcement.toString(1)
        return indentText(res,indention)

class HudSkinData(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,loadScreenCharacters,fleetIcons,bandbox,fogOfWar,
            hudIcons,**kwargs):
        super().__init__(**kwargs)
        self.loadScreenCharacters = ui.BrushRef(**loadScreenCharacters)
        self.fleetIcons = co_attribs.AttribList(elemType=ui.BrushRef,**fleetIcons)
        self.bandbox = co_attribs.AttribEnum(possibleVals=["BANDBOX"],**bandbox)
        self.fogOfWar = co_attribs.AttribEnum(possibleVals=["FogOfWar"],**fogOfWar)
        self.hudIcons = collections.OrderedDict()
        for elem in [ui.BrushRef(**val) for val in hudIcons]:
            self.hudIcons[elem.identifier] = elem

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.loadScreenCharacters==other.loadScreenCharacters)
                        and (self.fleetIcons==other.fleetIcons)
                        and (self.bandbox==other.bandbox)
                        and (self.fogOfWar==other.fogOfWar)
                        and (self.hudIcons==other.hudIcons))

    def __getattr__(self,name):
        if name in self.hudIcons:
            return self.hudIcons[name]
        else:
            raise AttributeError("Unknown attribute: "+name)

    def getRefs(self):
        res = super().getRefs()
        res.extend(self.hudIcons.values())
        return res

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for val in self.hudIcons.values():
            res.extend(val.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.loadScreenCharacters.toString(1)
        res += self.fleetIcons.toString(1)
        res += self.bandbox.toString(1)
        res += self.fogOfWar.toString(1)
        for val in self.hudIcons.values():
            res += val.toString(1)
        return indentText(res,indention)

class ResearchBlock(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,area,backdrop,indicatorBackdrop,title,picture,**kwargs):
        super().__init__(**kwargs)
        self.area = co_attribs.AttribPos4d(**area)
        self.backdrop = ui.BrushRef(**backdrop)
        self.indicatorBackdrop = ui.BrushRef(**indicatorBackdrop)
        self.title = ui.StringReference(**title)
        self.picture = ui.BrushRef(**picture)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.area==other.area)
                        and (self.backdrop==other.backdrop)
                        and (self.indicatorBackdrop==other.indicatorBackdrop)
                        and (self.title==other.title)
                        and (self.picture==other.picture))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.area.toString(1)
        res += self.backdrop.toString(1)
        res += self.indicatorBackdrop.toString(1)
        res += self.title.toString(1)
        res += self.picture.toString(1)
        return indentText(res,indention)

class ResearchData(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,combat,defense,nonCombat,diplomacy,artifact,fleet,
            capShipSlotIcons,shipSlotIcons,**kwargs):
        super().__init__(**kwargs)
        self.combat = co_attribs.AttribList(elemType=ResearchBlock,**combat)
        self.defense = co_attribs.AttribList(elemType=ResearchBlock,**defense)
        self.nonCombat = co_attribs.AttribList(elemType=ResearchBlock,**nonCombat)
        self.diplomacy = co_attribs.AttribList(elemType=ResearchBlock,**diplomacy)
        self.artifact = co_attribs.AttribList(elemType=ResearchBlock,**artifact)
        self.fleet = co_attribs.AttribList(elemType=ResearchBlock,**fleet)
        self.capShipSlotIcons = tuple(map(
                lambda vals:ui.BrushRef(**vals),
                capShipSlotIcons))
        self.shipSlotIcons = tuple(map(
                lambda vals:ui.BrushRef(**vals),
                shipSlotIcons))

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.combat==other.combat)
                        and (self.defense==other.defense)
                        and (self.nonCombat==other.nonCombat)
                        and (self.diplomacy==other.diplomacy)
                        and (self.artifact==other.artifact)
                        and (self.fleet==other.fleet)
                        and (self.capShipSlotIcons==other.capShipSlotIcons)
                        and (self.shipSlotIcons==other.shipSlotIcons))

    def getRefs(self):
        res = super().getRefs()
        res.extend(self.capShipSlotIcons)
        res.extend(self.shipSlotIcons)
        return res

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for elem in self.capShipSlotIcons:
            res.extend(elem.check(currMod,recursive))
        for elem in self.shipSlotIcons:
            res.extend(elem.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.combat.toString(1)
        res += self.defense.toString(1)
        res += self.nonCombat.toString(1)
        res += self.diplomacy.toString(1)
        res += self.artifact.toString(1)
        res += self.fleet.toString(1)
        for elem in self.capShipSlotIcons:
            res += elem.toString(1)
        for elem in self.shipSlotIcons:
            res += elem.toString(1)
        return indentText(res,indention)

class FactionRelationMod(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,factionName,otherFactionName,modMin,modMax,**kwargs):
        super().__init__(**kwargs)
        self.factionName = ui.StringReference(**factionName)
        self.otherFactionName = ui.StringReference(**otherFactionName)
        self.modMin = co_attribs.AttribNum(**modMin)
        self.modMax = co_attribs.AttribNum(**modMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.factionName==other.factionName)
                        and (self.otherFactionName==other.otherFactionName)
                        and (self.modMin==other.modMin)
                        and (self.modMax==other.modMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.factionName.toString(1)
        res += self.otherFactionName.toString(1)
        res += self.modMin.toString(1)
        res += self.modMax.toString(1)
        return indentText(res,indention)

class RaceRelationMod(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,raceName,modMin,modMax,factionRelations,**kwargs):
        super().__init__(**kwargs)
        self.raceName = ui.StringReference(**raceName)
        self.modMin = co_attribs.AttribNum(**modMin)
        self.modMax = co_attribs.AttribNum(**modMax)
        self.factionRelations = co_attribs.AttribList(elemType=FactionRelationMod,
                **factionRelations)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.raceName==other.raceName)
                        and (self.modMin==other.modMin)
                        and (self.modMax==other.modMax)
                        and (self.factionRelations==other.factionRelations))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.raceName.toString(1)
        res += self.modMin.toString(1)
        res += self.modMax.toString(1)
        res += self.factionRelations.toString(1)
        return indentText(res,indention)

class GameOverData(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,picHasWon,picHasLost,themeBackdrop,formatStrings,
            phraseStrings,**kwargs):
        super().__init__(**kwargs)
        self.picHasWon = ui.BrushRef(**picHasWon)
        self.picHasLost = ui.BrushRef(**picHasLost)
        self.themeBackdrop = ui.BrushRef(**themeBackdrop)
        self.formatStrings = collections.OrderedDict()
        for elem in [ui.StringReference(**val) for val in formatStrings]:
            self.formatStrings[elem.identifier] = elem
        self.phraseStrings = collections.OrderedDict()
        for elem in [ui.StringReference(**val) for val in phraseStrings]:
            self.phraseStrings[elem.identifier] = elem

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.picHasWon==other.picHasWon)
                        and (self.picHasLost==other.picHasLost)
                        and (self.themeBackdrop==other.themeBackdrop)
                        and (self.formatStrings==other.formatStrings)
                        and (self.phraseStrings==other.phraseStrings))

    def __getattr__(self,name):
        if name in self.formatStrings:
            return self.formatStrings[name]
        elif name in self.phraseStrings:
            return self.phraseStrings[name]
        else:
            raise AttributeError("Unknown attribute: "+name)

    def getRefs(self):
        res = super().getRefs()
        res.extend(self.formatStrings.values())
        res.extend(self.phraseStrings.values())
        return res

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for val in self.formatStrings.values():
            res.extend(val.check(currMod,recursive))
        for val in self.phraseStrings.values():
            res.extend(val.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.picHasWon.toString(1)
        res += self.picHasLost.toString(1)
        res += self.themeBackdrop.toString(1)
        for val in self.formatStrings.values():
            res += val.toString(1)
        for val in self.phraseStrings.values():
            res += val.toString(1)
        return indentText(res,indention)

class Player(coe.Entity,co_basics.HasReference):
    parser = g_PlayerEntity

    def __init__(self,raceName,canColonize,isPsi,selectable,selectPriority,
            randCapNamePrefix,randStarbaseNamePrefix,buildables,debrisData,
            hyperspaceData,shieldData,gameEventData,planetElevatorData,
            musicThemes,hudSkinData,research,gameOver,aiNames,
            raceNameParsePrefix,raceNameParseFallback,researchFields,
            playerTheme,playerPicture,relations,defaultFactionName,**kwargs):
        super().__init__(**kwargs)
        self.raceName = ui.StringReference(**raceName)
        self.canColonize = co_attribs.AttribBool(**canColonize)
        self.isPsi = co_attribs.AttribBool(**isPsi)
        self.selectable = co_attribs.AttribBool(**selectable)
        self.selectPriority = co_attribs.AttribNum(**selectPriority)
        self.randCapNamePrefix = co_attribs.AttribString(**randCapNamePrefix)
        self.randStarbaseNamePrefix = co_attribs.AttribString(
                **randStarbaseNamePrefix)
        self.buildables = PlayerBuildables(**buildables)
        self.debrisData = DebrisData(**debrisData)
        self.hyperspaceData = HyperspaceData(**hyperspaceData)
        self.shieldData = ShieldData(**shieldData)
        self.gameEventData = GameEventData(**gameEventData)
        self.planetElevatorData = PlanetElevatorData(**planetElevatorData)
        self.musicThemes = MusicThemesData(**musicThemes)
        self.hudSkinData = HudSkinData(**hudSkinData)
        self.research = ResearchData(**research)
        self.gameOver = GameOverData(**gameOver)
        self.aiNames = co_attribs.AttribList(elemType=ui.StringReference,**aiNames)
        self.raceNameParsePrefix = co_attribs.AttribString(**raceNameParsePrefix)
        self.raceNameParseFallback = co_attribs.AttribString(**raceNameParseFallback)
        self.researchFields = co_attribs.AttribList(elemType=ui.StringReference,
                **researchFields)
        self.playerTheme = themes.PlayerThemeRef(**playerTheme)
        self.playerPicture = themes.PlayerPictureRef(**playerPicture)
        self.relations = co_attribs.AttribList(elemType=RaceRelationMod,**relations)
        self.defaultFactionName = ui.StringReference(**defaultFactionName)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.raceName==other.raceName)
                        and (self.canColonize==other.canColonize)
                        and (self.isPsi==other.isPsi)
                        and (self.selectable==other.selectable)
                        and (self.selectPriority==other.selectPriority)
                        and (self.randCapNamePrefix==other.randCapNamePrefix)
                        and (
                        self.randStarbaseNamePrefix==other.randStarbaseNamePrefix)
                        and (self.buildables==other.buildables)
                        and (self.debrisData==other.debrisData)
                        and (self.hyperspaceData==other.hyperspaceData)
                        and (self.shieldData==other.shieldData)
                        and (self.gameEventData==other.gameEventData)
                        and (self.planetElevatorData==other.planetElevatorData)
                        and (self.musicThemes==other.musicThemes)
                        and (self.hudSkinData==other.hudSkinData)
                        and (self.research==other.research)
                        and (self.gameOver==other.gameOver)
                        and (self.aiNames==other.aiNames)
                        and (
                        self.raceNameParsePrefix==other.raceNameParsePrefix)
                        and (
                        self.raceNameParseFallback==other.raceNameParseFallback)
                        and (self.researchFields==other.researchFields)
                        and (self.playerTheme==other.playerTheme)
                        and (self.playerPicture==other.playerPicture)
                        and (self.relations==other.relations)
                        and (self.defaultFactionName==other.defaultFactionName))

    def toString(self,indention):
        res = super().toString(0)
        res += self.raceName.toString(0)
        res += self.canColonize.toString(0)
        res += self.isPsi.toString(0)
        res += self.selectable.toString(0)
        res += self.selectPriority.toString(0)
        res += self.randCapNamePrefix.toString(0)
        res += self.randStarbaseNamePrefix.toString(0)
        res += self.buildables.toString(0)
        res += self.debrisData.toString(0)
        res += self.hyperspaceData.toString(0)
        res += self.shieldData.toString(0)
        res += self.gameEventData.toString(0)
        res += self.planetElevatorData.toString(0)
        res += self.musicThemes.toString(0)
        res += self.hudSkinData.toString(0)
        res += self.research.toString(0)
        res += self.gameOver.toString(0)
        res += self.aiNames.toString(0)
        res += self.raceNameParsePrefix.toString(0)
        res += self.raceNameParseFallback.toString(0)
        res += self.researchFields.toString(0)
        res += self.playerTheme.toString(0)
        res += self.playerPicture.toString(0)
        res += self.relations.toString(0)
        res += self.defaultFactionName.toString(0)
        return indentText(res,indention)
