"""Containts parsers and classes to handle all PlanetModule entities.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.entities.ships as ships
import pysoase.mod.audio as audio
import pysoase.mod.meshes as mesh
import pysoase.mod.particles as particles

"""################################# Types #################################"""

EXTRACTION_TYPE = ["Crystal","Metal","Invalid"]
MODULE_SLOT_TYPES = ["Civilian","Tactical"]
MODULE_ROLE_TYPES = ["ANTIMATTERRECHARGER","CANNON","CAPITALSHIPFACTORY",
    "CRYSTALEXTRACTOR","CULTURE","FRIGATEFACTORY","HANGARDEFENSE",
    "JUMPBLOCKER","METALEXTRACTOR","NANOWEAPONJAMMER","PHASEGATE",
    "REFINERY","REPAIRPLATFORM","RESEARCHCOMBAT","RESEARCHNONCOMBAT",
    "SHIELDGENERATOR","SPACEMINE","TITANFACTORY","TRADEPORT",
    "WEAPONDEFENSE"]
MODULE_ROTATE_TYPE = ["None","AwayFromOrbitBody","TowardsOrbitBody"]
RESOURCE_TYPES = ["Crystal","Metal"]

"""################################ Parsers ################################"""

"""### Basic Elements ###"""

g_buildEffect = co_parse.genStringAttrib("buildEffectName")("buildEffect")
g_placement = (
    co_parse.genDecimalAttrib("nearPlacementDistOffset")("nearPlacement")
    +co_parse.genDecimalAttrib("farPlacementDistOffset")("farPlacement")
)
g_angSpeed = co_parse.genDecimalAttrib("angularSpeed")("speedAngular")
g_rotation = (
    co_parse.genDecimalAttrib("angularSpeed")("speedAngular")
    +co_parse.genStringAttrib("rotateSoundName")("sndRotate")
    +co_parse.genEnumAttrib("rotateFacingType",MODULE_ROTATE_TYPE)("rotateType")
    +co_parse.genBoolAttrib("rotateConstantly")("rotateConst")
)

"""### Planet Modules ###"""

g_PlanetModule = (
    coe.g_zoomDist
    +coe.g_abilities
    +coe.g_maxAntimatter
    +coe.g_antimatterRestore
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +co_parse.genEnumAttrib("planetUpgradeSlotType",MODULE_SLOT_TYPES)("slotType")
    +co_parse.genDecimalAttrib("planetUpgradeSlotCount")("slotCount")
    +coe.g_basePrice
    +coe.g_nameStringCap
    +coe.g_descriptionStringCap
    +co_parse.genEnumAttrib("planetModuleRoleType",MODULE_ROLE_TYPES)("roleType")
    +armed.g_statType
    +coe.g_prereqs
    +hit.g_hullPoints
    +hit.g_hullRestore
    +hit.g_baseArmorPoints
    +hit.g_maxShield
    +hit.g_shieldRestore
    +hit.g_maxShieldMitigation
    +hit.g_expCap
    +co_parse.genEnumAttrib("resourceExtractionType",EXTRACTION_TYPE)(
            "extractType")
    +co_parse.genDecimalAttrib("cultureSpreadRate")("cultureRate")
    +co_parse.genBoolAttrib("isAffectedBySimilarModuleCostResearch")(
            "affectedByCostResearch")
    +co_parse.genDecimalAttrib("placementRadius")("placeRadius")
    +g_placement
    +co_parse.genIntAttrib("spawnCount")("spawnCount")
    +co_parse.genStringAttrib("swapMineType")("swapMineType")
    +g_rotation
    +hit.g_shieldVisuals
    +hit.g_voiceSounds
    +armed.g_criterionMeshInfos
    +armed.g_meshEffects
    +hit.g_explosion
    +coe.g_mainViewIcon
    +coe.g_picture
    +coe.g_shadows
    +armed.g_formation
    +armed.g_baseBuildTime
    +g_buildEffect
)

g_PlanetModuleStandardEntity = (
    coe.genEntityHeader("PlanetModuleStandard")
    +g_PlanetModule
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_PlanetModuleShipFactoryEntity = (
    coe.genEntityHeader("PlanetModuleShipFactory")
    +g_PlanetModule
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_PlanetModuleCommercial = (
    co_parse.genStringAttrib("cargoShipType")("shipType")
    +co_parse.genIntAttrib("maxCargoShipCount")("maxShips")
    +co_parse.genDecimalAttrib("cargoShipRespawnTime")("shipRespawnTime")
).ignore(pp.dblSlashComment)

g_PlanetModuleTradePortEntity = (
    coe.genEntityHeader("PlanetModuleTradePort")
    +g_PlanetModule
    +g_PlanetModuleCommercial
    +co_parse.genDecimalAttrib("baseTradeIncomeRate")("baseTradeIncome")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_PlanetModuleRefineryEntity = (
    coe.genEntityHeader("PlanetModuleRefinery")
    +g_PlanetModule
    +g_PlanetModuleCommercial
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)
g_PlanetModuleWeaponDefenseEntity = (
    coe.genEntityHeader("PlanetModuleWeaponDefense")
    +g_PlanetModule
    +armed.g_attackBehaviour
    +armed.g_armed
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)
g_PlanetModuleHangarDefenseEntity = (
    coe.genEntityHeader("PlanetModuleHangarDefense")
    +g_PlanetModule
    +armed.g_squadrons
    +armed.g_armed
    +co_parse.genIntAttrib("baseCommandPoints")("commandPoints")
    +armed.g_attackBehaviour
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Asteroid ###"""

g_ResourceAsteroidEntity = (
    coe.genEntityHeader("ResourceAsteroid")
    +co_parse.genEnumAttrib("resource",RESOURCE_TYPES)("resource")
    +coe.g_uiIcons
    +coe.g_mainViewIcon
    +coe.g_picture
    +co_parse.genListAttrib("meshNameCount",
            co_parse.genStringAttrib("meshName"))("meshes")
    +coe.g_nameString
    +coe.g_descriptionStringShort
    +g_placement
    +coe.g_shadows
    +coe.g_zoomDist
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Starbase ###"""
g_StarBaseEntity = (
    coe.genEntityHeader("StarBase")
    +coe.g_abilities
    +armed.g_attackBehaviour
    +coe.g_debris
    +hit.g_armorType
    +coe.g_uiIcons
    +coe.g_zoomDist
    +armed.g_armed
    +hit.g_shieldVisuals
    +armed.g_squadrons
    +coe.g_mainViewIcon
    +hit.g_exp
    +coe.g_nameString
    +coe.g_picture
    +armed.g_statType
    +co_parse.genStringAttrib("creationSourceAbility")("createAbility")
    +co_parse.genIntAttrib("maxUpgradeLevelCount")("maxUpgradeLvl")
    +co_parse.genListAttrib("UpgradeTypeCount",
            co_parse.genStringAttrib("UpgradeType"))("upgrades")
    +armed.g_criterionMeshInfos
    +armed.g_meshEffects
    +hit.g_voiceSounds
    +hit.g_explosion
    +armed.g_formation
    +coe.g_shadows
    +g_PlanetModuleCommercial
    +armed.g_baseBuildTime
    +g_buildEffect
    +g_rotation
    +coe.g_movement
    +coe.g_mass
    +coe.g_hyperChargeSnd
    +coe.g_hyperTravelSnd
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Properties ##############################"""

class GravityWellObject(coe.MainViewSelectable):
    def __init__(self,nearPlacement,farPlacement,**kwargs):
        super().__init__(**kwargs)
        self.nearPlacement = co_attribs.AttribNum(**nearPlacement)
        self.farPlacement = co_attribs.AttribNum(**farPlacement)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.nearPlacement==other.nearPlacement)
                        and (self.farPlacement==other.farPlacement))

class PlanetModule(armed.Constructable,GravityWellObject):
    def __init__(self,slotType,slotCount,roleType,extractType,cultureRate,
            affectedByCostResearch,placeRadius,spawnCount,swapMineType,
            speedAngular,buildEffect,rotateConst,rotateType,sndRotate,**kwargs):
        super().__init__(**kwargs)
        self.slotType = co_attribs.AttribEnum(possibleVals=MODULE_SLOT_TYPES,
                **slotType)
        self.slotCount = co_attribs.AttribNum(**slotCount)
        self.roleType = co_attribs.AttribEnum(possibleVals=MODULE_ROLE_TYPES,
                **roleType)
        self.extractType = co_attribs.AttribEnum(possibleVals=EXTRACTION_TYPE,
                **extractType)
        self.cultureRate = co_attribs.AttribNum(**cultureRate)
        self.affectedByCostResearch = co_attribs.AttribBool(**affectedByCostResearch)
        self.placeRadius = co_attribs.AttribNum(**placeRadius)
        self.spawnCount = co_attribs.AttribNum(**spawnCount)
        self.swapMineType = coe.EntityRef(types=["SpaceMine"],**swapMineType)
        self.speedAngular = co_attribs.AttribNum(**speedAngular)
        self.buildEffect = particles.ParticleRef(**buildEffect)
        self.rotateConst = co_attribs.AttribBool(**rotateConst)
        self.rotateType = co_attribs.AttribEnum(possibleVals=MODULE_ROTATE_TYPE,
                **rotateType)
        self.sndRotate = audio.SoundRef(**sndRotate)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.slotType==other.slotType)
                        and (self.slotCount==other.slotCount)
                        and (self.roleType==other.roleType)
                        and (self.extractType==other.extractType)
                        and (self.cultureRate==other.cultureRate)
                        and (
                        self.affectedByCostResearch==other.affectedByCostResearch)
                        and (self.placeRadius==other.placeRadius)
                        and (self.spawnCount==other.spawnCount)
                        and (self.swapMineType==other.swapMineType)
                        and (self.speedAngular==other.speedAngular)
                        and (self.buildEffect==other.buildEffect)
                        and (self.rotateConst==other.rotateConst)
                        and (self.sndRotate==other.sndRotate))

    def toString(self,indention):
        res = ""
        res += self.zoomDist.toString(0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.antimatterMax.toString(0)
        res += self.antimatterRestore.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.slotType.toString(0)
        res += self.slotCount.toString(0)
        res += self.price.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.roleType.toString(0)
        res += self.statcount.toString(0)
        res += self.prereqs.toString(0)
        res += self.hullPointsMax.toString(0)
        res += self.hullPointRestore.toString(0)
        res += self.armorPoints.toString(0)
        res += self.shieldPointsMax.toString(0)
        res += self.shieldPointRestore.toString(0)
        res += self.shieldMitigation.toString(0)
        res += self.exp.toString(0)
        res += self.extractType.toString(0)
        res += self.cultureRate.toString(0)
        res += self.affectedByCostResearch.toString(0)
        res += self.placeRadius.toString(0)
        res += self.nearPlacement.toString(0)
        res += self.farPlacement.toString(0)
        res += self.spawnCount.toString(0)
        res += self.swapMineType.toString(0)
        res += self.speedAngular.toString(0)
        res += self.sndRotate.toString(0)
        res += self.rotateType.toString(0)
        res += self.rotateConst.toString(0)
        res += self.shieldMesh.toString(0)
        res += self.shieldRender.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.meshes.toString(0)
        res += self.meshIncreasedEffect.toString(0)
        res += self.meshDecreasedEffect.toString(0)
        res += self.explosion.toString(0)
        res += self.iconMainView.toString(0)
        res += self.pic.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        res += self.formationRank.toString(0)
        res += self.buildTime.toString(0)
        res += self.buildEffect.toString(0)
        return indentText(res,indention)

class PlanetModuleCommercial(PlanetModule):
    def __init__(self,shipType,maxShips,shipRespawnTime,**kwargs):
        super().__init__(**kwargs)
        self.shipType = coe.EntityRef(types=["Frigate"],**shipType)
        self.maxShips = co_attribs.AttribNum(**maxShips)
        self.shipRespawnTime = co_attribs.AttribNum(**shipRespawnTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.shipType==other.shipType)
                        and (self.maxShips==other.maxShips)
                        and (self.shipRespawnTime==other.shipRespawnTime))

    def toString(self,indention):
        res = super().toString(0)
        res += self.shipType.toString(0)
        res += self.maxShips.toString(0)
        res += self.shipRespawnTime.toString(0)
        return indentText(res,indention)

"""############################## File Classes #############################"""

class ResourceAsteroid(coe.Entity,GravityWellObject):
    parser = g_ResourceAsteroidEntity

    def __init__(self,resource,meshes,**kwargs):
        super().__init__(**kwargs)
        self.resource = co_attribs.AttribEnum(possibleVals=RESOURCE_TYPES,**resource)
        self.meshes = co_attribs.AttribList(elemType=mesh.MeshRef,**meshes)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.resource==other.resource)
                        and (self.meshes==other.meshes))

    def toString(self,indention):
        res = super().toString(0)
        res += self.resource.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.iconMainView.toString(0)
        res += self.pic.toString(0)
        res += self.meshes.toString(0)
        res += self.nameString.toString(0)
        res += self.descString.toString(0)
        res += self.nearPlacement.toString(0)
        res += self.farPlacement.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        res += self.zoomDist.toString(0)
        return indentText(res,indention)

class PlanetModuleStandard(coe.Entity,PlanetModule):
    parser = g_PlanetModuleStandardEntity

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res

    def toString(self,indention):
        res = coe.Entity.toString(self,0)
        res += PlanetModule.toString(self,0)
        return indentText(res,indention)

class PlanetModuleFactory(PlanetModuleStandard):
    parser = g_PlanetModuleShipFactoryEntity

class PlanetModuleRefinery(coe.Entity,PlanetModuleCommercial):
    parser = g_PlanetModuleRefineryEntity

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res

    def toString(self,indention):
        res = coe.Entity.toString(self,0)
        res += PlanetModuleCommercial.toString(self,0)
        return indentText(res,indention)

class PlanetModuleTradePort(coe.Entity,PlanetModuleCommercial):
    parser = g_PlanetModuleTradePortEntity

    def __init__(self,baseTradeIncome,**kwargs):
        super().__init__(**kwargs)
        self.baseTradeIncome = co_attribs.AttribNum(**baseTradeIncome)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.baseTradeIncome==other.baseTradeIncome)

    def toString(self,indention):
        res = coe.Entity.toString(self,0)
        res += PlanetModuleCommercial.toString(self,0)
        res += self.baseTradeIncome.toString(0)
        return indentText(res,indention)

class PlanetModuleWeaponDefense(coe.Entity,PlanetModule,armed.Armed):
    parser = g_PlanetModuleWeaponDefenseEntity

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res

    def toString(self,indention):
        res = coe.Entity.toString(self,0)
        res += PlanetModule.toString(self,0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += armed.Armed.toString(self,0)
        return indentText(res,indention)

class PlanetModuleHangarDefense(coe.Entity,PlanetModule,armed.CarriesFighters):
    parser = g_PlanetModuleHangarDefenseEntity

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res

    def toString(self,indention):
        res = coe.Entity.toString(self,0)
        res += PlanetModule.toString(self,0)
        for sq in self.squads:
            res += sq.toString(0)
        res += armed.Armed.toString(self,0)
        res += self.commandPoints.toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        return indentText(res,indention)

class Starbase(coe.Entity,ships.Moving,armed.CarriesFighters,hit.Shielded,
        hit.HasShieldVisuals):
    parser = g_StarBaseEntity

    def __init__(self,statcount,createAbility,maxUpgradeLvl,upgrades,
            meshes,meshIncreasedEffect,meshDecreasedEffect,formationRank,
            shipType,maxShips,shipRespawnTime,buildTime,buildEffect,
            speedAngular,sndRotate,rotateType,rotateConst,hyperTravelSnd,
            hyperChargingSnd,**kwargs):
        dummy = {"identifier":"","val":""}
        kwargs["armorPoints"] = dummy
        kwargs["hullPointRestore"] = dummy
        kwargs["hullPointsMax"] = dummy
        kwargs["shieldPointsMax"] = dummy
        kwargs["shieldPointRestore"] = dummy
        kwargs["shieldMitigation"] = dummy
        kwargs["antimatterMax"] = dummy
        kwargs["antimatterRestore"] = dummy
        kwargs["commandPoints"] = dummy
        kwargs["descString"] = dummy
        kwargs["exhaustSys"] = dummy
        kwargs["engineSound"] = dummy
        super().__init__(**kwargs)
        self.armorPoints = None
        self.hullPointsMax = None
        self.hullPointRestore = None
        self.shieldPointsMax = None
        self.shieldPointRestore = None
        self.shieldMitigation = None
        self.antimatterMax = None
        self.antimatterRestore = None
        self.commandPoints = None
        self.descString = None
        self.exhaustSys = None
        self.engineSound = None
        self.statcount = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,
                **statcount)
        self.createAbility = coe.EntityRef(types=["Ability"],**createAbility)
        self.maxUpgradeLvl = co_attribs.AttribNum(**maxUpgradeLvl)
        self.upgrades = co_attribs.AttribList(elemType=coe.EntityRef,
                elemArgs={"types":["StarBaseUpgrade"]},
                **upgrades)
        self.meshes = co_attribs.AttribList(elemType=armed.MeshInfoConstruct,**meshes)
        self.formationRank = co_attribs.AttribNum(**formationRank)
        self.meshDecreasedEffect = mesh.MeshRef(**meshDecreasedEffect)
        self.meshIncreasedEffect = mesh.MeshRef(**meshIncreasedEffect)
        self.shipType = coe.EntityRef(types=["Frigate"],**shipType)
        self.maxShips = co_attribs.AttribNum(**maxShips)
        self.shipRespawnTime = co_attribs.AttribNum(**shipRespawnTime)
        self.buildTime = co_attribs.AttribNum(**buildTime)
        self.buildEffect = particles.ParticleRef(**buildEffect)
        self.speedAngular = co_attribs.AttribNum(**speedAngular)
        self.rotateConst = co_attribs.AttribBool(**rotateConst)
        self.rotateType = co_attribs.AttribEnum(possibleVals=MODULE_ROTATE_TYPE,
                **rotateType)
        self.sndRotate = audio.SoundRef(**sndRotate)
        self.hyperChargingSnd = audio.SoundRef(**hyperChargingSnd)
        self.hyperTravelSnd = audio.SoundRef(**hyperTravelSnd)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.statcount==other.statcount)
                        and (self.createAbility==other.createAbility)
                        and (self.maxUpgradeLvl==other.maxUpgradeLvl)
                        and (self.upgrades==other.upgrades)
                        and (self.meshes==other.meshes)
                        and (self.formationRank==other.formationRank)
                        and (
                        self.meshDecreasedEffect==other.meshDecreasedEffect)
                        and (
                        self.meshIncreasedEffect==other.meshIncreasedEffect)
                        and (self.shipType==other.shipType)
                        and (self.maxShips==other.maxShips)
                        and (self.shipRespawnTime==other.shipRespawnTime)
                        and (self.buildTime==other.buildTime)
                        and (self.buildEffect==other.buildEffect)
                        and (self.speedAngular==other.speedAngular)
                        and (self.rotateConst==other.rotateConst)
                        and (self.rotateType==other.rotateType)
                        and (self.sndRotate==other.sndRotate)
                        and (self.hyperChargingSnd==other.hyperChargingSnd)
                        and (self.hyperTravelSnd==other.hyperTravelSnd))

    def toString(self,indention):
        res = coe.Entity.toString(self,0)
        for ab in self.abilities:
            res += ab.toString(0)
        res += self.autoattackRange.toString(0)
        res += self.autoattackOn.toString(0)
        res += self.focusFire.toString(0)
        res += self.usesFighterAttack.toString(0)
        res += self.debrisLarge.toString(0)
        res += self.debrisSmall.toString(0)
        res += self.debrisSpecific.toString(0)
        res += self.armorType.toString(0)
        res += self.iconHud.toString(0)
        res += self.iconHudSmall.toString(0)
        res += self.iconInfocard.toString(0)
        res += self.zoomDist.toString(0)
        res += armed.Armed.toString(self,0)
        res += self.shieldMesh.toString(0)
        res += self.shieldRender.toString(0)
        for sq in self.squads:
            res += sq.toString(0)
        res += self.iconMainView.toString(0)
        res += self.exp.toString(0)
        res += self.nameString.toString(0)
        res += self.pic.toString(0)
        res += self.statcount.toString(0)
        res += self.createAbility.toString(0)
        res += self.maxUpgradeLvl.toString(0)
        res += self.upgrades.toString(0)
        res += self.meshes.toString(0)
        res += self.meshIncreasedEffect.toString(0)
        res += self.meshDecreasedEffect.toString(0)
        res += self.sndAttackOrder.toString(0)
        res += self.sndCreation.toString(0)
        res += self.sndGeneralOrder.toString(0)
        res += self.sndSelected.toString(0)
        res += self.sndPhaseJump.toString(0)
        res += self.explosion.toString(0)
        res += self.formationRank.toString(0)
        res += self.shadowMin.toString(0)
        res += self.shadowMax.toString(0)
        res += self.shipType.toString(0)
        res += self.maxShips.toString(0)
        res += self.shipRespawnTime.toString(0)
        res += self.buildTime.toString(0)
        res += self.buildEffect.toString(0)
        res += self.speedAngular.toString(0)
        res += self.sndRotate.toString(0)
        res += self.rotateType.toString(0)
        res += self.rotateConst.toString(0)
        res += self.maxAccelLin.toString(0)
        res += self.maxAccelStrafe.toString(0)
        res += self.maxDecelLin.toString(0)
        res += self.maxAccelAng.toString(0)
        res += self.maxDecelAng.toString(0)
        res += self.maxSpeedLin.toString(0)
        res += self.maxRollRate.toString(0)
        res += self.maxRollAngle.toString(0)
        res += self.mass.toString(0)
        res += self.hyperChargingSnd.toString(0)
        res += self.hyperTravelSnd.toString(0)
        return indentText(res,indention)
