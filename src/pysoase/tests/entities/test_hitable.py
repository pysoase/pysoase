""" Tests for the pysoase.entities.hitable module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.visuals as vis
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_ecommon import HasAbilitiesFixtures, \
    MainViewSelectableFixture, \
    MainViewVisibleFixtures, \
    UIVisibleFixtures

testdata = os.path.join("entities","hitable")

"""######################## Tests HasShieldVisuals #########################"""

class HasShieldVisualsFixtures(HasAbilitiesFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "ShieldMeshName \"testshield\"\n"
            +"renderShield TRUE\n"
        )
        cls.shieldMesh = {"identifier":"ShieldMeshName","val":"testshield"}
        cls.shieldRender = {"identifier":"renderShield","val":True}

class HasShieldVisualsTests(HasShieldVisualsFixtures,unit.TestCase):
    desc = "Tests HasShieldVisuals:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = hit.HasShieldVisuals(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_zoomDist
            +coe.g_shadows
            +coe.g_abilities
            +coe.g_antimatterRestore
            +coe.g_maxAntimatter
            +hit.g_shieldVisuals
        )
        parseRes = parser.parseString(self.parseString)
        res = hit.HasShieldVisuals(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribShieldMesh(self):
        """ Test whether shieldMesh attribute is created correctly
        """
        exp = meshes.MeshRef(**self.shieldMesh)
        self.assertEqual(self.inst.shieldMesh,exp)

    def testCreationAttribShieldRender(self):
        """ Test whether shieldMesh attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.shieldRender)
        self.assertEqual(self.inst.shieldRender,exp)

    def testCreationAttribShieldMeshEmpty(self):
        """ Test whether shieldMesh can be empty with renderShield FALSE
        """
        shieldRenderFalse = {"identifier":"renderShield","val":False}
        currInst = hit.HasShieldVisuals(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                shieldMesh=self.shieldMesh,
                shieldRender=shieldRenderFalse
        )
        exp = meshes.MeshRef(canBeEmpty=True,**self.shieldMesh)
        self.assertEqual(currInst.shieldMesh,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        patchers = []
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
            patchers.append(patcher)

        with mock.patch.object(self.inst.shieldMesh,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

        for p in patchers:
            p.stop()

    def testCheckShieldMesh(self):
        """ Test whether problems in shieldMesh attribute are returned
        """
        patchers = []
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
            patchers.append(patcher)

        with mock.patch.object(self.inst.shieldMesh,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

        for p in patchers:
            p.stop()

"""########################## Tests Controllable ###########################"""

class ControllableFixture(MainViewSelectableFixture,HasAbilitiesFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"mainViewIcon \"mainviewicon\"\n"
            +"picture \"picture\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"ability:0 \"ab0\"\n"
            +"ability:1 \"ab1\"\n"
            +"ability:2 \"ab2\"\n"
            +"ability:3 \"ab3\"\n"
            +"ability:4 \"ab4\"\n"
            +"AntiMatterRestoreRate 50.0\n"
            +"MaxAntiMatter 100.0\n"
            +"NumSoundsFor:ONATTACKORDERISSUED 1\n"
            +"SoundID \"attackorder\"\n"
            +"NumSoundsFor:ONCREATION 1\n"
            +"SoundID \"creation\"\n"
            +"NumSoundsFor:ONGENERALORDERISSUED 1\n"
            +"SoundID \"generalorder\"\n"
            +"NumSoundsFor:ONSELECTED 1\n"
            +"SoundID \"selected\"\n"
            +"NumSoundsFor:ONSTARTPHASEJUMP 1\n"
            +"SoundID \"phasejump\"\n"
        )
        cls.sndAttackOrder = {
            "counter":{"identifier":"NumSoundsFor:ONATTACKORDERISSUED",
                "val":1},
            "elements":[
                {"identifier":"SoundID","val":"attackorder"}
            ]
        }
        cls.sndCreation = {
            "counter":{"identifier":"NumSoundsFor:ONCREATION",
                "val":1},
            "elements":[
                {"identifier":"SoundID","val":"creation"}
            ]
        }
        cls.sndGeneralOrder = {
            "counter":{"identifier":"NumSoundsFor:ONGENERALORDERISSUED",
                "val":1},
            "elements":[
                {"identifier":"SoundID","val":"generalorder"}
            ]
        }
        cls.sndSelected = {
            "counter":{"identifier":"NumSoundsFor:ONSELECTED",
                "val":1},
            "elements":[
                {"identifier":"SoundID","val":"selected"}
            ]
        }
        cls.sndPhaseJump = {
            "counter":{"identifier":"NumSoundsFor:ONSTARTPHASEJUMP",
                "val":1},
            "elements":[
                {"identifier":"SoundID","val":"phasejump"}
            ]
        }

class ControllableTests(ControllableFixture,unit.TestCase):
    desc = "Tests Controllable:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []
        cls.mmod.audio.checkEffect.return_value = []

    def setUp(self):
        self.inst = hit.Controllable(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump
        )
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_mainViewIcon
            +coe.g_picture
            +coe.g_zoomDist
            +coe.g_shadows
            +coe.g_abilities
            +coe.g_antimatterRestore
            +coe.g_maxAntimatter
            +hit.g_voiceSounds
        )
        parseRes = parser.parseString(self.parseString)
        res = hit.Controllable(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSndAttackOrder(self):
        """ Test whether sndAttackOrder attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.DialogueRef,**self.sndAttackOrder)
        self.assertEqual(self.inst.sndAttackOrder,exp)

    def testCreationAttribSndCreation(self):
        """ Test whether sndCreation attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.DialogueRef,**self.sndCreation)
        self.assertEqual(self.inst.sndCreation,exp)

    def testCreationAttribSndGeneralOrder(self):
        """ Test whether sndGeneralOrder attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.DialogueRef,**self.sndGeneralOrder)
        self.assertEqual(self.inst.sndGeneralOrder,exp)

    def testCreationAttribSndSelected(self):
        """ Test whether sndSelected attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.DialogueRef,**self.sndSelected)
        self.assertEqual(self.inst.sndSelected,exp)

    def testCreationAttribSndPhaseJump(self):
        """ Test whether sndPhaseJump attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.DialogueRef,**self.sndPhaseJump)
        self.assertEqual(self.inst.sndPhaseJump,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckSndAttackOrder(self):
        """ Test whether check returns problems from sndAttackOrder attrib
        """
        with mock.patch.object(self.inst.sndAttackOrder,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckSndCreation(self):
        """ Test whether check returns problems from sndCreation attrib
        """
        with mock.patch.object(self.inst.sndCreation,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckSndGeneralOrder(self):
        """ Test whether check returns problems from sndGeneralOrder attrib
        """
        with mock.patch.object(self.inst.sndGeneralOrder,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckSndSelected(self):
        """ Test whether check returns problems from sndSelected attrib
        """
        with mock.patch.object(self.inst.sndSelected,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckSndPhaseJump(self):
        """ Test whether check returns problems from sndPhaseJump attrib
        """
        with mock.patch.object(self.inst.sndPhaseJump,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

"""############################# Tests Hitable #############################"""

class HitableFixtures(UIVisibleFixtures,MainViewVisibleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"armorType \"Titan\"\n"
            +"BaseArmorPoints 12.0\n"
            +"ExplosionName \"testexplosion\"\n"
            +"experiencePointsForDestroying 10.0\n"
            +"HullPointRestoreRate 1.0\n"
            +"MaxHullPoints 2.0\n"
            +"numRandomDebrisLarge 1.0\n"
            +"numRandomDebrisSmall 1.0\n"
            +"numSpecificDebris 1\n"
            +"\tspecificDebrisMeshName \"debris\"\n"
        )
        cls.armorType = {"identifier":"armorType","val":"Titan"}
        cls.armorPoints = {"identifier":"BaseArmorPoints","val":12.0}
        cls.explosion = {"identifier":"ExplosionName","val":"testexplosion"}
        cls.exp = {"identifier":"experiencePointsForDestroying","val":10.0}
        cls.expCap = {"identifier":"ExperiencePointsForDestroying","val":10.0}
        cls.hullPointRestore = {"identifier":"HullPointRestoreRate",
            "val":1.0}
        cls.hullPointRestoreLvl = {
            "identifier":"HullPointRestoreRate",
            "startVal":{"identifier":"StartValue","val":1.8},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.12}
        }
        cls.hullPointsMaxLvl = {
            "identifier":"MaxHullPoints",
            "startVal":{"identifier":"StartValue","val":3180.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":138.0}
        }
        cls.armorPointsLvl = {
            "identifier":"ArmorPointsFromExperience",
            "startVal":{"identifier":"StartValue","val":6.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.48}
        }
        cls.hullPointsMax = {"identifier":"MaxHullPoints","val":2.0}
        cls.debrisLarge = {"identifier":"numRandomDebrisLarge","val":1.0}
        cls.debrisSmall = {"identifier":"numRandomDebrisSmall","val":1.0}
        cls.debrisSpecific = {
            "counter":{"identifier":"numSpecificDebris","val":1},
            "elements":[
                {"identifier":"specificDebrisMeshName","val":"debris"}
            ]
        }

class HitableTests(HitableFixtures,unit.TestCase):
    desc = "Tests Hitable:"

    def setUp(self):
        self.inst = hit.Hitable(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific
        )
        self.mmod = tco.genMockMod("./")
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExplosion = patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDebris = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_zoomDist
            +coe.g_shadows
            +hit.g_armorType
            +hit.g_baseArmorPoints
            +hit.g_explosion
            +hit.g_exp
            +hit.g_hullRestore
            +hit.g_hullPoints
            +coe.g_debris
        )
        parseRes = parser.parseString(self.parseString)
        inst1 = hit.Hitable(**parseRes)
        self.assertEqual(inst1,self.inst)

    def testCreationAttribArmorType(self):
        """ Test whether armorType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=hit.ARMOR_TYPES,**self.armorType)
        self.assertEqual(self.inst.armorType,exp)

    def testCreationAttribArmorPoints(self):
        """ Test whether armorPoints attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.armorPoints)
        self.assertEqual(self.inst.armorPoints,exp)

    def testCreationAttribExplosion(self):
        """ Test whether explosion attrib is created correctly
        """
        exp = vis.ExplosionRef(**self.explosion)
        self.assertEqual(self.inst.explosion,exp)

    def testCreationAttribExp(self):
        """ Test whether exp attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.exp)
        self.assertEqual(self.inst.exp,exp)

    def testCreationAttribHullPointRestore(self):
        """ Test whether hullPointRestore attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.hullPointRestore)
        self.assertEqual(self.inst.hullPointRestore,exp)

    def testCreationAttribHullPointsMax(self):
        """ Test whether hullPointsMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.hullPointsMax)
        self.assertEqual(self.inst.hullPointsMax,exp)

    def testCreationAttribDebrisLarge(self):
        """ Test whether debrisLarge attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.debrisLarge)
        self.assertEqual(self.inst.debrisLarge,exp)

    def testCreationAttribDebrisSmall(self):
        """ Test whether debrisSmall attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.debrisSmall)
        self.assertEqual(self.inst.debrisSmall,exp)

    def testCreationAttribDebrisSpecific(self):
        """ Test whether debrisSpecific attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=meshes.MeshRef,**self.debrisSpecific)
        self.assertEqual(self.inst.debrisSpecific,exp)

    def testCheck(self):
        """ Test whether check returns empty list when there are no problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckExplosion(self):
        """ Test whether check returns problems from explosion attrib
        """
        self.mockExplosion.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckDebrisSpecific(self):
        """ Test whether check returns problems from debrisSpecific attrib
        """
        self.mockDebris.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

"""############################# Tests Shielded ############################"""

class ShieldedFixtures(ControllableFixture,HitableFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"armorType \"Titan\"\n"
            +"BaseArmorPoints 12.0\n"
            +"ExplosionName \"testexplosion\"\n"
            +"experiencePointsForDestroying 10.0\n"
            +"HullPointRestoreRate 1.0\n"
            +"MaxHullPoints 2.0\n"
            +"numRandomDebrisLarge 1.0\n"
            +"numRandomDebrisSmall 1.0\n"
            +"numSpecificDebris 1\n"
            +"\tspecificDebrisMeshName \"debris\"\n"
            +"mainViewIcon \"mainviewicon\"\n"
            +"picture \"picture\"\n"
            +"ability:0 \"ab0\"\n"
            +"ability:1 \"ab1\"\n"
            +"ability:2 \"ab2\"\n"
            +"ability:3 \"ab3\"\n"
            +"ability:4 \"ab4\"\n"
            +"AntiMatterRestoreRate 50.0\n"
            +"MaxAntiMatter 100.0\n"
            +"NumSoundsFor:ONATTACKORDERISSUED 1\n"
            +"SoundID \"attackorder\"\n"
            +"NumSoundsFor:ONCREATION 1\n"
            +"SoundID \"creation\"\n"
            +"NumSoundsFor:ONGENERALORDERISSUED 1\n"
            +"SoundID \"generalorder\"\n"
            +"NumSoundsFor:ONSELECTED 1\n"
            +"SoundID \"selected\"\n"
            +"NumSoundsFor:ONSTARTPHASEJUMP 1\n"
            +"SoundID \"phasejump\"\n"
            +"MaxShieldPoints 12.0\n"
            +"ShieldPointRestoreRate 17.0\n"
            +"maxShieldMitigation 20.0\n"
        )
        cls.shieldPointsMax = {"identifier":"MaxShieldPoints","val":12.0}
        cls.shieldPointRestore = {"identifier":"ShieldPointRestoreRate",
            "val":17.0}
        cls.shieldMitigation = {"identifier":"maxShieldMitigation","val":20.0}
        cls.shieldPointsMaxLvl = {
            "identifier":"MaxShieldPoints",
            "startVal":{"identifier":"StartValue","val":1710.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":198.0}
        }
        cls.shieldPointRestoreLvl = {
            "identifier":"ShieldPointRestoreRate",
            "startVal":{"identifier":"StartValue","val":3.6},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.24}
        }
        cls.maxMitigationLvl = {
            "identifier":"maxMitigation",
            "startVal":{"identifier":"StartValue","val":0.65},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.01}
        }

class ShieldedTests(ShieldedFixtures,unit.TestCase):
    desc = "Tests Shielded:"

    def setUp(self):
        self.mmod = tco.genMockMod("./")
        self.inst = hit.Shielded(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation
        )
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_zoomDist
            +coe.g_shadows
            +hit.g_armorType
            +hit.g_baseArmorPoints
            +hit.g_explosion
            +hit.g_exp
            +hit.g_hullRestore
            +hit.g_hullPoints
            +coe.g_debris
            +coe.g_mainViewIcon
            +coe.g_picture
            +coe.g_abilities
            +coe.g_antimatterRestore
            +coe.g_maxAntimatter
            +hit.g_voiceSounds
            +hit.g_maxShield
            +hit.g_shieldRestore
            +hit.g_maxShieldMitigation
        )
        parseRes = parser.parseString(self.parseString)
        res = hit.Shielded(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribShieldPointsMax(self):
        """ Test whether shieldPointsMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.shieldPointsMax)
        self.assertEqual(self.inst.shieldPointsMax,exp)

    def testCreationAttribShieldPointRestore(self):
        """ Test whether shieldPointRestore attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.shieldPointRestore)
        self.assertEqual(self.inst.shieldPointRestore,exp)

    def testCreationAttribShieldMitigation(self):
        """ Test whether shieldMitigation attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.shieldMitigation)
        self.assertEqual(self.inst.shieldMitigation,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExplosion = patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDebris = patcher.start()
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
