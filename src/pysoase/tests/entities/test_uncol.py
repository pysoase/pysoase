""" Unit tests for the pysoase.entities.uncolonizables module.
"""

import os
import unittest as unit
import unittest.mock as mock

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.uncolonizables as uncol
import pysoase.mod.meshes as meshes
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("entities","uncol")

"""############################ Tests MeshGroup ############################"""

class MeshGroupTests(unit.TestCase):
    desc = "Tests MeshGroup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "meshGroup\n"
            +"\tname \"Testname\"\n"
            +"\tmeshNameCount 2\n"
            +"\tmeshName \"mesh1\"\n"
            +"\tmeshName \"mesh2\"\n"
        )
        cls.identifier = "meshGroup"
        cls.name = {"identifier":"name","val":"Testname"}
        cls.meshes = {
            "counter":{"identifier":"meshNameCount","val":2},
            "elements":[
                {"identifier":"meshName","val":"mesh1"},
                {"identifier":"meshName","val":"mesh2"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = uncol.MeshGroup(
                identifier=self.identifier,
                name=self.name,
                meshes=self.meshes
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = uncol.g_meshGroup.parseString(self.parseString)[0]
        res = uncol.MeshGroup(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribMeshes(self):
        """ Test whether meshes attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=meshes.MeshRef,**self.meshes)
        self.assertEqual(self.inst.meshes,exp)

    def testCheckNoProbs(self):
        """ Test whether check correctly returns empty list of problems
        """
        with mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether check correctly returns problems in meshes
        """
        with mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests MeshGroupRef ##########################"""

class MeshGroupRefTests(unit.TestCase):
    desc = "Tests MeshGroupRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.val = "testref"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.mockAstDef = mock.create_autospec(uncol.AsteroidDef,spec_set=True)
        self.inst = uncol.MeshGroupRef(identifier=self.identifier,
                val=self.val,
                parent=self.mockAstDef)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = uncol.MeshGroupRef(parent=self.mockAstDef,**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckNoProb(self):
        """ Test whether check returns problem with existing mesh group
        """
        self.mockAstDef.meshes.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
        self.mockAstDef.meshes.__contains__.assert_called_once_with(self.val)

    def testCheckMissing(self):
        """ Test whether check returns problem with missing mesh group
        """
        self.mockAstDef.meshes.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.mockAstDef.meshes.__contains__.assert_called_once_with(self.val)

"""########################## Tests AsteroidCluster ########################"""

class AsteroidClusterTests(unit.TestCase):
    desc = "Tests AsteroidCluster:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "cluster\n"
            +"\tmeshGroupName \"Testgroup\"\n"
            +"\tasteroidCount 125\n"
            +"\tminDistance 0.000000\n"
            +"\tmaxDistance 0.500000\n"
            +"\tmaxZOffset 0.900000\n"
            +"\tminAngle 0\n"
            +"\tmaxAngle 75\n"
            +"\tminAngularSpeed 0.200000\n"
            +"\tmaxAngularSpeed 0.900000\n"
        )
        cls.identifier = "cluster"
        cls.asteroidCount = {"identifier":"asteroidCount","val":125}
        cls.meshGroup = {"identifier":"meshGroupName","val":"Testgroup"}
        cls.distMin = {"identifier":"minDistance","val":0.0}
        cls.distMax = {"identifier":"maxDistance","val":0.5}
        cls.offsetMax = {"identifier":"maxZOffset","val":0.9}
        cls.angleMin = {"identifier":"minAngle","val":0}
        cls.angleMax = {"identifier":"maxAngle","val":75}
        cls.angularSpeedMin = {"identifier":"minAngularSpeed","val":0.2}
        cls.angularSpeedMax = {"identifier":"maxAngularSpeed","val":0.9}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.parent = mock.create_autospec(uncol.AsteroidDef,spec_set=True)
        self.inst = uncol.AsteroidCluster(
                identifier=self.identifier,
                parent=self.parent,
                asteroidCount=self.asteroidCount,
                meshGroup=self.meshGroup,
                distMin=self.distMin,
                distMax=self.distMax,
                offsetMax=self.offsetMax,
                angleMin=self.angleMin,
                angleMax=self.angleMax,
                angularSpeedMin=self.angularSpeedMin,
                angularSpeedMax=self.angularSpeedMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = uncol.g_cluster.parseString(self.parseString)[0]
        res = uncol.AsteroidCluster(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMeshGroup(self):
        """ Test whether meshGroup attribute is constructed correctly
        """
        exp = uncol.MeshGroupRef(parent=self.parent,**self.meshGroup)
        self.assertEqual(self.inst.meshGroup,exp)

    def testCreationAttribAsteroidCount(self):
        """ Test whether asteroidCount attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.asteroidCount)
        self.assertEqual(self.inst.asteroidCount,exp)

    def testCreationAttribDistMin(self):
        """ Test whether distMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.distMin)
        self.assertEqual(self.inst.distMin,exp)

    def testCreationAttribDistMax(self):
        """ Test whether distMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.distMax)
        self.assertEqual(self.inst.distMax,exp)

    def testCreationAttribOffsetMax(self):
        """ Test whether offsetMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.offsetMax)
        self.assertEqual(self.inst.offsetMax,exp)

    def testCreationAttribAngleMin(self):
        """ Test whether angleMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.angleMin)
        self.assertEqual(self.inst.angleMin,exp)

    def testCreationAttribAngleMax(self):
        """ Test whether angleMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.angleMax)
        self.assertEqual(self.inst.angleMax,exp)

    def testCreationAttribAngularSpeedMin(self):
        """ Test whether angularSpeedMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.angularSpeedMin)
        self.assertEqual(self.inst.angularSpeedMin,exp)

    def testCreationAttribAngularSpeedMax(self):
        """ Test whether angularSpeedMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.angularSpeedMax)
        self.assertEqual(self.inst.angularSpeedMax,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list of probs for good values
        """
        self.parent.meshes.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        self.parent.meshes.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

"""########################## Tests AsteroidTemplate #######################"""

class AsteroidTemplateTests(unit.TestCase):
    desc = "Tests AsteroidTemplate:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "template\n"
            +"\tname \"Shattered\"\n"
            +"\tclusterCount 1\n"
            +"\tcluster\n"
            +"\t\tmeshGroupName \"Testgroup\"\n"
            +"\t\tasteroidCount 125\n"
            +"\t\tminDistance 0.000000\n"
            +"\t\tmaxDistance 0.500000\n"
            +"\t\tmaxZOffset 0.900000\n"
            +"\t\tminAngle 0\n"
            +"\t\tmaxAngle 75\n"
            +"\t\tminAngularSpeed 0.200000\n"
            +"\t\tmaxAngularSpeed 0.900000\n"
        )
        cls.identifier = "template"
        cls.name = {"identifier":"name","val":"Shattered"}
        cls.clusters = {
            "counter":{"identifier":"clusterCount","val":1},
            "elements":[
                {
                    "identifier":"cluster",
                    "asteroidCount":{"identifier":"asteroidCount","val":125},
                    "meshGroup":{"identifier":"meshGroupName",
                        "val":"Testgroup"},
                    "distMin":{"identifier":"minDistance","val":0.0},
                    "distMax":{"identifier":"maxDistance","val":0.5},
                    "offsetMax":{"identifier":"maxZOffset","val":0.9},
                    "angleMin":{"identifier":"minAngle","val":0},
                    "angleMax":{"identifier":"maxAngle","val":75},
                    "angularSpeedMin":{"identifier":"minAngularSpeed",
                        "val":0.2},
                    "angularSpeedMax":{"identifier":"maxAngularSpeed",
                        "val":0.9}
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.parent = mock.create_autospec(uncol.AsteroidDef,spec_set=True)
        self.inst = uncol.AsteroidTemplate(
                parent=self.parent,
                identifier=self.identifier,
                name=self.name,
                clusters=self.clusters
        )

    def testCreationParse(self):
        """ Test whether results are created correctly from parse results
        """
        parseRes = uncol.g_asteroidTemplate.parseString(self.parseString)[0]
        res = uncol.AsteroidTemplate(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribClusters(self):
        """ Test whether clusters attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=uncol.AsteroidCluster,
                elemArgs={"parent":self.parent},**self.clusters)
        self.assertEqual(self.inst.clusters,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        with mock.patch.object(self.inst.clusters,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether check returns problems correctly
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.clusters,"check",autospec=True,
                spec_set=True,return_value=probs):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,probs)

"""############################ Tests AsteroidDef ##########################"""

class AsteroidDefTests(unit.TestCase):
    desc = "Tests AsteroidDef:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "AsteroidDef.asteroidDef"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.meshes = {
            "counter":{"identifier":"meshGroupCount","val":1},
            "elements":[
                {
                    "identifier":"meshGroup",
                    "name":{"identifier":"name","val":"Shattered"},
                    "meshes":{
                        "counter":{"identifier":"meshNameCount","val":1},
                        "elements":[
                            {"identifier":"meshName","val":"Testmesh"}
                        ]
                    }
                }
            ]
        }
        cls.templates = {
            "counter":{"identifier":"templateCount","val":1},
            "elements":[
                {
                    "identifier":"template",
                    "name":{"identifier":"name","val":"Shattered"},
                    "clusters":{
                        "counter":{"identifier":"clusterCount","val":1},
                        "elements":[
                            {
                                "identifier":"cluster",
                                "meshGroup":{"identifier":"meshGroupName",
                                    "val":"Shattered"},
                                "asteroidCount":{
                                    "identifier":"asteroidCount",
                                    "val":125},
                                "distMin":{"identifier":"minDistance",
                                    "val":0.0},
                                "distMax":{"identifier":"maxDistance",
                                    "val":0.5},
                                "offsetMax":{"identifier":"maxZOffset",
                                    "val":0.9},
                                "angleMin":{"identifier":"minAngle",
                                    "val":0},
                                "angleMax":{"identifier":"maxAngle",
                                    "val":75},
                                "angularSpeedMin":{
                                    "identifier":"minAngularSpeed",
                                    "val":0.2},
                                "angularSpeedMax":{
                                    "identifier":"maxAngularSpeed",
                                    "val":0.9}
                            }
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = uncol.AsteroidDef(
                filepath=self.filepath,
                meshes=self.meshes,
                templates=self.templates
        )

    def testCreationParse(self):
        """ Test whether instance is created crrectly from parse result
        """
        res = uncol.AsteroidDef.createInstance(self.filepath)
        self.assertIsInstance(res,uncol.AsteroidDef)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether createInstance returns correct parse problem
        """
        res = uncol.AsteroidDef.createInstance(
                "AsteroidDefMalformed.asteroidDef")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribMeshes(self):
        """ Test whether meshes attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(elemType=uncol.MeshGroup,
                keyfunc=lambda val:val.name.value,**self.meshes)
        self.assertEqual(self.inst.meshes,exp)

    def testCreationAttribTemplates(self):
        """ Test whether templates attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(elemType=uncol.AsteroidTemplate,
                keyfunc=lambda val:val.name.value,elemArgs={"parent":self.inst},
                **self.templates)
        self.assertEqual(self.inst.templates,exp)

    def testCheckNoProb(self):
        """ Test whether check without problems returns empty list
        """
        with mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_with(self.mmod,False)

    def testCheckProb(self):
        """ Test whether check with problems returns correct problems
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            mocked.assert_called_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################ Tests AsteroidRef ##########################"""

class AsteroidRefTests(unit.TestCase):
    desc = "Tests AsteroidRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"Testref\"\n"
        cls.identifier = "ident"
        cls.val = "Testref"

    def setUp(self):
        self.inst = uncol.AsteroidRef(
                identifier=self.identifier,
                val=self.val)
        self.mmod = tco.genMockMod("./")
        self.mockAst = mock.create_autospec(uncol.AsteroidDef,spec_set=True)
        self.mockAst.templates.__contains__.return_value = True
        self.mmod.entities.asteroids = self.mockAst

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = uncol.AsteroidRef(**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckExists(self):
        """ Test whether check returns no problem for existing template
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
        self.mockAst.templates.__contains__.assert_called_once_with(self.val)

    def testCheckMissing(self):
        """ Test whether check returns problem for missing template
        """
        self.mockAst.templates.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.mockAst.templates.__contains__.assert_called_once_with(self.val)

"""############################ Tests TextureGroup #########################"""

class TextureGroupTests(unit.TestCase):
    desc = "Tests TextureGroup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "textureGroup\n"
            +"\tname \"Default\"\n"
            +"\ttextureNameCount 1\n"
            +"\ttextureName \"Testtext\"\n"
        )
        cls.identifier = "textureGroup"
        cls.name = {"identifier":"name","val":"Default"}
        cls.textures = {
            "counter":{"identifier":"textureNameCount","val":1},
            "elements":[
                {"identifier":"textureName","val":"Testtext"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = uncol.TextureGroup(
                identifier=self.identifier,
                name=self.name,
                textures=self.textures
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = uncol.g_textureGroup.parseString(self.parseString)[0]
        res = uncol.TextureGroup(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribTextures(self):
        """ Test whether textures attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=ui.UITextureRef,**self.textures)
        self.assertEqual(self.inst.textures,exp)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        with mock.patch.object(self.inst.textures,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns problems correctly
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.textures,"check",autospec=True,
                spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,probs)
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests TextureGroupRef ########################"""

class TextureGroupRefTests(unit.TestCase):
    desc = "Tests TextureGroup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.val = "testref"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.parent = mock.create_autospec(uncol.CloudDef,
                spec_set=True)
        self.inst = uncol.TextureGroupRef(
                identifier=self.identifier,
                val=self.val,
                parent=self.parent
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = uncol.TextureGroupRef(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckExists(self):
        """ Test whether check returns empty list of problems for existing ref
        """
        self.parent.textures.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
        self.parent.textures.__contains__.assert_called_once_with(self.val)

    def testCheckMissing(self):
        """ Test whether check returns problem with missing reference
        """
        self.parent.textures.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.parent.textures.__contains__.assert_called_once_with(self.val)

"""############################ Tests CloudClump ###########################"""

class CloudClumpTests(unit.TestCase):
    desc = "Tests CloudClump:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "clump\n"
            +"\tinstanceCount 3\n"
            +"\ttextureGroupName \"testgroup\"\n"
            +"\tminDistance 0.200000\n"
            +"\tmaxDistance 0.900000\n"
            +"\tradius 10000\n"
            +"\tminParticleWidth 5000\n"
            +"\tmaxParticleWidth 50000\n"
            +"\tminParticles 1\n"
            +"\tmaxParticles 3\n"
        )
        cls.identifier = "clump"
        cls.instanceCount = {"identifier":"instanceCount","val":3}
        cls.textureGroup = {"identifier":"textureGroupName","val":"testgroup"}
        cls.distMin = {"identifier":"minDistance","val":0.2}
        cls.distMax = {"identifier":"maxDistance","val":0.9}
        cls.radius = {"identifier":"radius","val":10000}
        cls.particleWidthMin = {"identifier":"minParticleWidth","val":5000}
        cls.particleWidthMax = {"identifier":"maxParticleWidth","val":50000}
        cls.particlesMin = {"identifier":"minParticles","val":1}
        cls.particlesMax = {"identifier":"maxParticles","val":3}
        cls.parent = mock.create_autospec(uncol.CloudDef,spec_set=True)
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = uncol.CloudClump(
                identifier=self.identifier,
                parent=self.parent,
                instanceCount=self.instanceCount,
                textureGroup=self.textureGroup,
                distMin=self.distMin,
                distMax=self.distMax,
                radius=self.radius,
                particleWidthMin=self.particleWidthMin,
                particleWidthMax=self.particleWidthMax,
                particlesMin=self.particlesMin,
                particlesMax=self.particlesMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly form parse results
        """
        parseRes = uncol.g_clump.parseString(self.parseString)[0]
        res = uncol.CloudClump(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribInstanceCount(self):
        """ Test whether instanceCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.instanceCount)
        self.assertEqual(self.inst.instanceCount,exp)

    def testCreationAttribTextureGroup(self):
        """ Test whether textureGroup attribute is created correctly
        """
        exp = uncol.TextureGroupRef(parent=self.parent,**self.textureGroup)
        self.assertEqual(self.inst.textureGroup,exp)

    def testCreationAttribDistMin(self):
        """ Test whether distMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.distMin)
        self.assertEqual(self.inst.distMin,exp)

    def testCreationAttribDistMax(self):
        """ Test whether distMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.distMax)
        self.assertEqual(self.inst.distMax,exp)

    def testCreationAttribRadius(self):
        """ Test whether radius attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.radius)
        self.assertEqual(self.inst.radius,exp)

    def testCreationAttribParticleWidthMin(self):
        """ Test whether particleWidthMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.particleWidthMin)
        self.assertEqual(self.inst.particleWidthMin,exp)

    def testCreationAttribParticleWidthMax(self):
        """ Test whether particleWidthMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.particleWidthMax)
        self.assertEqual(self.inst.particleWidthMax,exp)

    def testCreationAttribParticlesMin(self):
        """ Test whether particlesMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.particlesMin)
        self.assertEqual(self.inst.particlesMin,exp)

    def testCreationAttribParticlesMax(self):
        """ Test whether particlesMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.particlesMax)
        self.assertEqual(self.inst.particlesMax,exp)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        self.parent.textures.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        self.parent.textures.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests CloudTemplate #########################"""

class CloudTemplateTests(unit.TestCase):
    desc = "Tests CloudTemplate:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "template\n"
            +"\tname \"Shattered\"\n"
            +"\tclumpCount 1\n"
            +"\tclump\n"
            +"\t\tinstanceCount 3\n"
            +"\t\ttextureGroupName \"testgroup\"\n"
            +"\t\tminDistance 0.200000\n"
            +"\t\tmaxDistance 0.900000\n"
            +"\t\tradius 10000\n"
            +"\t\tminParticleWidth 5000\n"
            +"\t\tmaxParticleWidth 50000\n"
            +"\t\tminParticles 1\n"
            +"\t\tmaxParticles 3\n"
        )
        cls.identifier = "template"
        cls.name = {"identifier":"name","val":"Shattered"}
        cls.clumps = {
            "counter":{"identifier":"clumpCount","val":1},
            "elements":[
                {
                    "identifier":"clump",
                    "instanceCount":{"identifier":"instanceCount","val":3},
                    "textureGroup":{"identifier":"textureGroupName",
                        "val":"testgroup"},
                    "distMin":{"identifier":"minDistance","val":0.2},
                    "distMax":{"identifier":"maxDistance","val":0.9},
                    "radius":{"identifier":"radius","val":10000},
                    "particleWidthMin":{"identifier":"minParticleWidth",
                        "val":5000},
                    "particleWidthMax":{"identifier":"maxParticleWidth",
                        "val":50000},
                    "particlesMin":{"identifier":"minParticles",
                        "val":1},
                    "particlesMax":{"identifier":"maxParticles",
                        "val":3}
                }
            ]
        }
        cls.parent = mock.create_autospec(uncol.CloudDef,spec_set=True)
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = uncol.CloudTemplate(
                identifier=self.identifier,
                parent=self.parent,
                name=self.name,
                clumps=self.clumps
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = uncol.g_cloudTemplate.parseString(self.parseString)[0]
        res = uncol.CloudTemplate(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribClumps(self):
        """ Test whether clumps attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=uncol.CloudClump,
                elemArgs={"parent":self.parent},**self.clumps)
        self.assertEqual(self.inst.clumps,exp)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        self.parent.textures.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        self.parent.textures.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests CloudDef ############################"""

class CloudDefTests(unit.TestCase):
    desc = "Tests CloudDef:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "DustCloudsDef.renderingDef"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.distVisibility = {"identifier":"fullVisibilityDistance","val":2.0}
        cls.distAlpha = {"identifier":"fullAlphaDistance","val":10.0}
        cls.textures = {
            "counter":{"identifier":"textureGroupCount","val":1},
            "elements":[
                {
                    "identifier":"textureGroup",
                    "name":{"identifier":"name","val":"Default"},
                    "textures":{
                        "counter":{"identifier":"textureNameCount",
                            "val":1},
                        "elements":[
                            {"identifier":"textureName",
                                "val":"smoke1"}
                        ]
                    }
                }
            ]
        }
        cls.templates = {
            "counter":{"identifier":"templateCount","val":1},
            "elements":[
                {
                    "identifier":"template",
                    "name":{"identifier":"name","val":"Sparse"},
                    "clumps":{
                        "counter":{"identifier":"clumpCount","val":1},
                        "elements":[
                            {
                                "identifier":"clump",
                                "instanceCount":{
                                    "identifier":"instanceCount",
                                    "val":3},
                                "textureGroup":{
                                    "identifier":"textureGroupName",
                                    "val":"Default"},
                                "distMin":{"identifier":"minDistance",
                                    "val":0.6},
                                "distMax":{"identifier":"maxDistance",
                                    "val":4.0},
                                "radius":{"identifier":"radius",
                                    "val":60000},
                                "particleWidthMin":{
                                    "identifier":"minParticleWidth",
                                    "val":5000},
                                "particleWidthMax":{
                                    "identifier":"maxParticleWidth",
                                    "val":50000},
                                "particlesMin":{
                                    "identifier":"minParticles",
                                    "val":2},
                                "particlesMax":{
                                    "identifier":"maxParticles",
                                    "val":4}
                            }
                        ]
                    }

                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = uncol.CloudDef(
                filepath=self.filepath,
                distVisibility=self.distVisibility,
                distAlpha=self.distAlpha,
                textures=self.textures,
                templates=self.templates
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = uncol.CloudDef.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether malformed file produces ParseProblem
        """
        res = uncol.CloudDef.createInstance(
                "DustCloudsDefMalformed.renderingDef")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribDistVisibility(self):
        """ Test whether distVisibility attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.distVisibility)
        self.assertEqual(self.inst.distVisibility,exp)

    def testCreationAttribDistAlpha(self):
        """ Test whether distAlpha attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.distAlpha)
        self.assertEqual(self.inst.distAlpha,exp)

    def testCreationAttribTextures(self):
        """ Test whether textures attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(elemType=uncol.TextureGroup,
                keyfunc=lambda val:val.name.value,**self.textures)
        self.assertEqual(self.inst.textures,exp)

    def testCreationAttribTemplates(self):
        """ Test whether templates attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(elemType=uncol.CloudTemplate,
                elemArgs={"parent":self.inst},keyfunc=lambda val:val.name.value,
                **self.templates)
        self.assertEqual(self.inst.templates,exp)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        with mock.patch.object(self.inst.textures,"check",autospec=True,
                return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        prob = mock.MagicMock(probFile=None)
        prob.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.textures,"check",autospec=True,
                return_value=[prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[prob])
            mocked.assert_called_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests CloudRef ###########################"""

class CloudRefTests(unit.TestCase):
    desc = "Tests CloudRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"Testref\"\n"
        cls.identifier = "ident"
        cls.val = "Testref"

    def setUp(self):
        self.inst = uncol.CloudRef(
                identifier=self.identifier,
                val=self.val)
        self.mmod = tco.genMockMod("./")
        self.mockCloud = mock.create_autospec(uncol.CloudDef,spec_set=True)
        self.mockCloud.templates.__contains__.return_value = True
        self.mmod.entities.clouds = self.mockCloud

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = uncol.CloudRef(**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckExists(self):
        """ Test whether check returns no problem for existing template
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
        self.mockCloud.templates.__contains__.assert_called_once_with(self.val)

    def testCheckMissing(self):
        """ Test whether check returns problem for missing template
        """
        self.mockCloud.templates.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.mockCloud.templates.__contains__.assert_called_once_with(self.val)
