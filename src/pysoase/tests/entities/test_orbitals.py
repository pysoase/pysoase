""" Tests for the pysoase.entities.orbitals module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.problems as co_probs
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.orbitals as orb
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as particles
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_armed import ConstructableFixtures, \
    ArmedFixtures,CarriesFightersFixtures
from pysoase.tests.entities.test_ecommon import MainViewSelectableFixture
from pysoase.tests.entities.test_hitable import ShieldedFixtures
from pysoase.tests.entities.test_ships import MovingFixtures

testdata = os.path.join("entities","orbitals")

"""######################## Tests GravityWellObject ########################"""

class GravityWellObjectFixtures(MainViewSelectableFixture):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            cls.parseString
            +"nearPlacementDistOffset 1.0\n"
            +"farPlacementDistOffset 2.0\n"
        )
        cls.nearPlacement = {"identifier":"nearPlacementDistOffset","val":1.0}
        cls.farPlacement = {"identifier":"farPlacementDistOffset","val":2.0}

class GravityWellObjectTests(GravityWellObjectFixtures,unit.TestCase):
    desc = "Tests GravityWellObject:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = orb.GravityWellObject(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_mainViewIcon
            +coe.g_picture
            +coe.g_zoomDist
            +coe.g_shadows
            +orb.g_placement
        )
        parseRes = parser.parseString(self.parseString)
        res = orb.GravityWellObject(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribNearPlacement(self):
        """ Test whether nearPlacement attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nearPlacement)
        self.assertEqual(self.inst.nearPlacement,exp)

    def testCreationAttribFarPlacement(self):
        """ Test whether farPlacement attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.farPlacement)
        self.assertEqual(self.inst.farPlacement,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

"""######################## Tests ResourceAsteroid #########################"""

class ResourceAsteroidTests(GravityWellObjectFixtures,unit.TestCase):
    desc = "Tests ResourceAsteroid:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "ResourceAsteroid.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"ResourceAsteroid"}
        cls.resource = {"identifier":"resource","val":"Metal"}
        cls.meshes = {
            "counter":{"identifier":"meshNameCount","val":1},
            "elements":[
                {"identifier":"meshName","val":"mesh"}
            ]
        }
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = orb.ResourceAsteroid(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                resource=self.resource,
                meshes=self.meshes
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.ResourceAsteroid.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.ResourceAsteroid.createInstance(
                "ResourceAsteroidMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribResource(self):
        """ Test whether resource attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=orb.RESOURCE_TYPES,**self.resource)
        self.assertEqual(self.inst.resource,exp)

    def testCreationAttribMeshes(self):
        """ Test whether meshes attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=meshes.MeshRef,**self.meshes)
        self.assertEqual(self.inst.meshes,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckMeshes(self):
        """ Test whether check returns problems from meshes attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests PlanetModule ###########################"""

class PlanetModuleFixtures(ConstructableFixtures,GravityWellObjectFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModule.property"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.slotType = {"identifier":"planetUpgradeSlotType","val":"Tactical"}
        cls.slotCount = {"identifier":"planetUpgradeSlotCount","val":2.0}
        cls.roleType = {"identifier":"planetModuleRoleType","val":"JUMPBLOCKER"}
        cls.extractType = {"identifier":"resourceExtractionType",
            "val":"Invalid"}
        cls.cultureRate = {"identifier":"cultureSpreadRate","val":0.0}
        cls.affectedByCostResearch = {
            "identifier":"isAffectedBySimilarModuleCostResearch",
            "val":False}
        cls.placeRadius = {"identifier":"placementRadius","val":350.0}
        cls.spawnCount = {"identifier":"spawnCount","val":1}
        cls.swapMineType = {"identifier":"swapMineType","val":"minetype"}
        cls.speedAngular = {"identifier":"angularSpeed","val":0.0}
        cls.buildEffect = {"identifier":"buildEffectName","val":"buildeff"}
        cls.rotateConst = {"identifier":"rotateConstantly","val":False}
        cls.rotateType = {"identifier":"rotateFacingType","val":"None"}
        cls.sndRotate = {"identifier":"rotateSoundName","val":"sndrotate"}

    def genMocks(self):
        patchers = []
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
            patchers.append(patcher)
        patcher = mock.patch.object(self.inst.shieldMesh,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshDecreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshIncreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshes,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.swapMineType,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockSwapMine = patcher.start()
        patcher = mock.patch.object(self.inst.buildEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockBuildEffect = patcher.start()
        patcher = mock.patch.object(self.inst.sndRotate,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockSndRotate = patcher.start()
        self.addCleanup(mock.patch.stopall)

class PlanetModuleTests(PlanetModuleFixtures,unit.TestCase):
    desc = "Tests PlanetModules:"

    def setUp(self):
        self.inst = orb.PlanetModule(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate
        )
        self.genMocks()
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orb.g_PlanetModule.parseString(self.parseString)
        res = orb.PlanetModule(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSlotType(self):
        """ Test whether slotType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=orb.MODULE_SLOT_TYPES,**self.slotType)
        self.assertEqual(self.inst.slotType,exp)

    def testCreationAttribSlotCount(self):
        """ Test whether slotCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.slotCount)
        self.assertEqual(self.inst.slotCount,exp)

    def testCreationAttribRoleType(self):
        """ Test whether slotCount attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=orb.MODULE_ROLE_TYPES,**self.roleType)
        self.assertEqual(self.inst.roleType,exp)

    def testCreationAttribExtractType(self):
        """ Test whether extractType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=orb.EXTRACTION_TYPE,
                **self.extractType)
        self.assertEqual(self.inst.extractType,exp)

    def testCreationAttribCultureRate(self):
        """ Test whether cultureRate attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.cultureRate)
        self.assertEqual(self.inst.cultureRate,exp)

    def testCreationAttribAffectedByCostResearch(self):
        """ Test whether affectedByCostResearch attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.affectedByCostResearch)
        self.assertEqual(self.inst.affectedByCostResearch,exp)

    def testCreationAttribPlaceRadius(self):
        """ Test whether placeRadius attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.placeRadius)
        self.assertEqual(self.inst.placeRadius,exp)

    def testCreationAttribSpawnCount(self):
        """ Test whether spawnCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnCount)
        self.assertEqual(self.inst.spawnCount,exp)

    def testCreationAttribSwapMineType(self):
        """ Test whether swapMineType attrib is created correctly
        """
        exp = coe.EntityRef(types=["SpaceMine"],**self.swapMineType)
        self.assertEqual(self.inst.swapMineType,exp)

    def testCreationAttribSpeedAngular(self):
        """ Test whether speedAngular attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedAngular)
        self.assertEqual(self.inst.speedAngular,exp)

    def testCreationAttribBuildEffect(self):
        """ Test whether buildEffect attrib is created correctly
        """
        exp = particles.ParticleRef(**self.buildEffect)
        self.assertEqual(self.inst.buildEffect,exp)

    def testCreationAttribRotateConst(self):
        """ Test whether rotateConst attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.rotateConst)
        self.assertEqual(self.inst.rotateConst,exp)

    def testCreationAttribRotateType(self):
        """ Test whether rotateType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=orb.MODULE_ROTATE_TYPE,
                **self.rotateType)
        self.assertEqual(self.inst.rotateType,exp)

    def testCreationAttribSndRotate(self):
        """ Test whether sndRotate attrib is created correctly
        """
        exp = audio.SoundRef(**self.sndRotate)
        self.assertEqual(self.inst.sndRotate,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests PlanetModuleStandard ######################"""

class PlanetModuleStandardTests(PlanetModuleFixtures,unit.TestCase):
    desc = "Tests PlanetModuleStandard:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleStandard.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType",
            "val":"PlanetModuleStandard"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orb.PlanetModuleStandard(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate
        )
        self.genMocks()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.PlanetModuleStandard.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.PlanetModuleStandard.createInstance(
                "PlanetModuleStandardMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests PlanetModuleFactory #######################"""

class PlanetModuleFactoryTests(PlanetModuleFixtures,unit.TestCase):
    desc = "Tests PlanetModuleFactory:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleFactory.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType",
            "val":"PlanetModuleShipFactory"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orb.PlanetModuleFactory(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate
        )
        self.genMocks()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.PlanetModuleFactory.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.PlanetModuleStandard.createInstance(
                "PlanetModuleFactoryMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests PlanetModuleCommercial ######################"""

class PlanetModuleCommecialFixtures(PlanetModuleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleCommercial.property"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.shipType = {"identifier":"cargoShipType","val":"cargoship"}
        cls.maxShips = {"identifier":"maxCargoShipCount","val":6}
        cls.shipRespawnTime = {"identifier":"cargoShipRespawnTime","val":0.5}

    def genMocks(self):
        super().genMocks()
        patcher = mock.patch.object(self.inst.shipType,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockShipType = patcher.start()

class PlanetModuleCommercialTests(PlanetModuleCommecialFixtures,unit.TestCase):
    desc = "Tests PlanetModuleCommercial:"

    def setUp(self):
        self.inst = orb.PlanetModuleCommercial(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate,
                shipType=self.shipType,
                maxShips=self.maxShips,
                shipRespawnTime=self.shipRespawnTime
        )
        self.genMocks()
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = (orb.g_PlanetModule
                    +orb.g_PlanetModuleCommercial).parseString(self.parseString)
        res = orb.PlanetModuleCommercial(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribShipType(self):
        """ Test whether shipType attrib is created correctly
        """
        exp = coe.EntityRef(types=["Frigate"],**self.shipType)
        self.assertEqual(self.inst.shipType,exp)

    def testCreationAttribMaxShips(self):
        """ Test whether maxShips attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxShips)
        self.assertEqual(self.inst.maxShips,exp)

    def testCreationAttribShipRespawnTime(self):
        """ Test whether shipRespawnTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.shipRespawnTime)
        self.assertEqual(self.inst.shipRespawnTime,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckShipType(self):
        """ Test whether check returns problems from shipType attrib
        """
        self.mockShipType.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests PlanetModuleRefinery ######################"""

class PlanetModuleRefineryTests(PlanetModuleCommecialFixtures,unit.TestCase):
    desc = "Tests PlanetModuleRefinery:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleRefinery.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType",
            "val":"PlanetModuleRefinery"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orb.PlanetModuleRefinery(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate,
                shipType=self.shipType,
                maxShips=self.maxShips,
                shipRespawnTime=self.shipRespawnTime
        )
        self.genMocks()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.PlanetModuleRefinery.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.PlanetModuleRefinery.createInstance(
                "PlanetModuleRefineryMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests PlanetModuleTradePort #####################"""

class PlanetModuleTradePortTests(PlanetModuleCommecialFixtures,unit.TestCase):
    desc = "Tests PlanetModuleTradePort:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleTradePort.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType",
            "val":"PlanetModuleTradePort"}
        cls.baseTradeIncome = {"identifier":"baseTradeIncomeRate","val":2.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orb.PlanetModuleTradePort(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate,
                shipType=self.shipType,
                maxShips=self.maxShips,
                shipRespawnTime=self.shipRespawnTime,
                baseTradeIncome=self.baseTradeIncome
        )
        self.genMocks()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.PlanetModuleTradePort.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.PlanetModuleTradePort.createInstance(
                "PlanetModuleTradePortMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribBaseTradeIncome(self):
        """ Test whether baseTradeIncome attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.baseTradeIncome)
        self.assertEqual(self.inst.baseTradeIncome,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests PlanetModuleWeaponDefense ###################"""

class PlanetModuleWeaponDefenseTests(PlanetModuleFixtures,ArmedFixtures,
        unit.TestCase):
    desc = "Tests PlanetModuleWeaponDefense:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleWeaponDefense.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType",
            "val":"PlanetModuleWeaponDefense"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orb.PlanetModuleWeaponDefense(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures
        )
        self.genMocks()
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockWeapons = patcher.start()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.PlanetModuleWeaponDefense.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.PlanetModuleWeaponDefense.createInstance(
                "PlanetModuleWeaponDefenseMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests PlanetModuleHangarDefense ###################"""

class PlanetModuleHangarDefenseTests(PlanetModuleFixtures,
        CarriesFightersFixtures,
        unit.TestCase):
    desc = "Tests PlanetModuleHangarDefense:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "PlanetModuleHangarDefense.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType",
            "val":"PlanetModuleHangarDefense"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orb.PlanetModuleHangarDefense(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                nearPlacement=self.nearPlacement,
                farPlacement=self.farPlacement,
                slotType=self.slotType,
                slotCount=self.slotCount,
                roleType=self.roleType,
                extractType=self.extractType,
                cultureRate=self.cultureRate,
                affectedByCostResearch=self.affectedByCostResearch,
                placeRadius=self.placeRadius,
                spawnCount=self.spawnCount,
                swapMineType=self.swapMineType,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                commandPoints=self.commandPoints
        )
        self.genMocks()
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockWeapons = patcher.start()
        for ab in self.inst.squads:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.PlanetModuleHangarDefense.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether create instance returns parse problems
        """
        res = orb.PlanetModuleHangarDefense.createInstance(
                "PlanetModuleHangarDefenseMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests Starbase ############################"""

class StarbaseTests(MovingFixtures,CarriesFightersFixtures,ShieldedFixtures,
        unit.TestCase):
    desc = "Tests Starbase:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Starbase.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"StarBase"}
        cls.statcount = {"identifier":"statCountType",
            "val":"ModulePointDefense"}
        cls.createAbility = {"identifier":"creationSourceAbility",
            "val":"createab"}
        cls.maxUpgradeLvl = {"identifier":"maxUpgradeLevelCount","val":2}
        cls.upgrades = {
            "counter":{"identifier":"UpgradeTypeCount","val":1},
            "elements":[
                {"identifier":"UpgradeType","val":"testupgrade"}
            ]
        }
        cls.meshes = {
            "counter":{"identifier":"MeshNameInfoCount","val":1},
            "elements":[
                {
                    "identifier":"MeshNameInfo",
                    "mesh":{"identifier":"meshName","val":"testmesh"},
                    "criterion":{
                        "conType":{"identifier":"criteriaType","val":"None"}
                    }
                }
            ]
        }
        cls.formationRank = {"identifier":"formationRank","val":2}
        cls.meshIncreasedEffect = {"identifier":"meshNameIncreasedEffectName",
            "val":"incmesh"}
        cls.meshDecreasedEffect = {"identifier":"meshNameDecreasedEffectName",
            "val":"decmesh"}
        cls.shipType = {"identifier":"cargoShipType","val":"cargoship"}
        cls.maxShips = {"identifier":"maxCargoShipCount","val":6}
        cls.shipRespawnTime = {"identifier":"cargoShipRespawnTime","val":0.5}
        cls.buildTime = {"identifier":"baseBuildTime","val":6.0}
        cls.buildEffect = {"identifier":"buildEffectName","val":"buildeff"}
        cls.speedAngular = {"identifier":"angularSpeed","val":0.0}
        cls.rotateConst = {"identifier":"rotateConstantly","val":False}
        cls.rotateType = {"identifier":"rotateFacingType","val":"None"}
        cls.sndRotate = {"identifier":"rotateSoundName","val":"sndrotate"}
        cls.hyperChargingSnd = {"identifier":"HyperspaceChargingSoundID",
            "val":"hypercharge"}
        cls.hyperTravelSnd = {"identifier":"HyperspaceTravelSoundID",
            "val":"hypertravel"}
        cls.shieldMesh = {"identifier":"ShieldMeshName","val":"testshield"}
        cls.shieldRender = {"identifier":"renderShield","val":True}
        cls.mass = {"identifier":"mass","val":10.0}
        cls.exhaustSys = {"identifier":"ExhaustParticleSystemName",
            "val":"exhaustsys"}
        cls.engineSound = {"identifier":"EngineSoundID","val":"sndengine"}
        cls.engineSoundS = {"identifier":"engineSoundID","val":"sndengine"}
        cls.maxAccelLin = {"identifier":"maxAccelerationLinear","val":1.0}
        cls.maxAccelStrafe = {"identifier":"maxAccelerationStrafe","val":2.0}
        cls.maxDecelLin = {"identifier":"maxDecelerationLinear","val":3.0}
        cls.maxAccelAng = {"identifier":"maxAccelerationAngular","val":4.0}
        cls.maxDecelAng = {"identifier":"maxDecelerationAngular","val":5.0}
        cls.maxSpeedLin = {"identifier":"maxSpeedLinear","val":6.0}
        cls.maxRollRate = {"identifier":"maxRollRate","val":7.0}
        cls.maxRollAngle = {"identifier":"maxRollAngle","val":8.0}

    def setUp(self):
        self.inst = orb.Starbase(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                explosion=self.explosion,
                exp=self.exp,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                statcount=self.statcount,
                buildTime=self.buildTime,
                speedAngular=self.speedAngular,
                buildEffect=self.buildEffect,
                rotateConst=self.rotateConst,
                rotateType=self.rotateType,
                sndRotate=self.sndRotate,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                createAbility=self.createAbility,
                maxUpgradeLvl=self.maxUpgradeLvl,
                upgrades=self.upgrades,
                shipType=self.shipType,
                maxShips=self.maxShips,
                shipRespawnTime=self.shipRespawnTime,
                hyperChargingSnd=self.hyperChargingSnd,
                hyperTravelSnd=self.hyperTravelSnd
        )
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockWeapons = patcher.start()
        for ab in self.inst.squads:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        patcher = mock.patch.object(self.inst.shipType,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockShipType = patcher.start()
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        patcher = mock.patch.object(self.inst.shieldMesh,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshDecreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshIncreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshes,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.buildEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockBuildEffect = patcher.start()
        patcher = mock.patch.object(self.inst.sndRotate,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockSndRotate = patcher.start()
        patcher = mock.patch.object(self.inst.createAbility,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockCreationAb = patcher.start()
        patcher = mock.patch.object(self.inst.upgrades,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockUpgrades = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orb.Starbase.createInstance(self.filepath)
        if isinstance(res,list):
            for prob in res:
                print(prob.toString(0))
        self.assertEqual(res,self.inst)

    def testCreationAttribCreateAbility(self):
        """ Test whether createAbility attrib is created correctly
        """
        exp = coe.EntityRef(types=["Ability"],**self.createAbility)
        self.assertEqual(self.inst.createAbility,exp)

    def testCreationAttribStatcount(self):
        """ Test whether statcount attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,**self.statcount)
        self.assertEqual(self.inst.statcount,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)
