""" Unit tests for the pysoase.entities.component module.
"""

import os
import unittest.mock as mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.filehandling as co_files
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.component as comp
import pysoase.entities.uncolonizables as uncol
import pysoase.mod.manifest as manifest
import pysoase.tests.conftest as tco

testdata = os.path.join("entities","comp")
testmodule = "pysoase.entities.component"

"""########################## Tests ModEntities ############################"""

@pytest.fixture()
def mockAst():
    return mock.create_autospec(uncol.AsteroidDef,spec_set=True)

@pytest.fixture()
def mockCloud():
    return mock.create_autospec(uncol.CloudDef,spec_set=True)

class TestModEntitiesCreation:
    desc = "Test ModEntities creation:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.modpath = os.path.abspath("./")
        cls.rebrefpath = os.path.abspath("./comprebref")
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebrefpath)

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.inst = comp.ModEntities(self.mmod)

    def testCreationEntities(self):
        """ Test whether initial discovery of entities works correctly
        """
        initialEntities = ["test1.entity","test2.entity"]
        assert all(elem in self.inst.compFilesMod for elem in initialEntities)

    def testCreationEntTypes(self):
        """ Test whether the entity type mapping is initialized correctly
        """
        assert len(self.inst._entTypes["Debris"]) == 2
        for t in self.inst._entTypes:
            if t!="Debris":
                assert len(self.inst._entTypes[t]) == 0

    @pytest.fixture()
    def mockManifest(self):
        manMock = mock.create_autospec(manifest.ManifestEntity,
                spec_set=True)
        manMock.createInstance.return_value = mock.sentinel.man
        moduleMock = mock.MagicMock(ManifestEntity=manMock)
        packageMock = mock.MagicMock()
        packageMock.mod.manifest = moduleMock
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            yield manMock,mocked

    def testCreationManifest(self,mockManifest):
        """ Test whether manifest is created correctly
        """
        manMock,mocked = mockManifest
        res = self.inst.manifest
        mocked.assert_called_once_with("","entity.manifest",
                manifest.ManifestEntity)
        assert res == manMock

    def testCreationManifestLazy(self,mockManifest):
        """ Test whether manifest lazy loadind works
        """
        manMock,mocked = mockManifest
        res = self.inst.manifest
        mocked.assert_called_once_with("","entity.manifest",
                manifest.ManifestEntity)
        assert res == manMock
        res1 = self.inst.manifest
        assert mocked.call_count == 1
        assert res == res1

    def testCreationManifestMissing(self):
        """ Test whether missing manifest is loaded from bundled files
        """
        currMod = tco.genMockMod("./comp_empty")
        currInst = comp.ModEntities(currMod)
        res = currInst.manifest
        assert isinstance(res,manifest.ManifestEntity)

    @pytest.fixture()
    def mockLoadAsteroid(self,mockAst):
        mockCreate = mock.create_autospec(uncol.AsteroidDef.createInstance,
                spec_set=True,return_value=mockAst)
        with mock.patch(
                "pysoase.entities.uncolonizables.AsteroidDef.createInstance",
                new=mockCreate) as mocked:
            yield mockAst,mocked

    def testCreationAsteroid(self,mockLoadAsteroid):
        """ Test whether AsteroidDef is created correctly
        """
        astMock,mocked = mockLoadAsteroid
        res = self.inst.asteroids
        assert res == astMock
        mocked.assert_called_once_with(os.path.join(self.modpath,
                "GameInfo","AsteroidDef.asteroidDef"))

    def testCreationAsteroidReb(self,mockLoadAsteroid):
        """ Test whether AsteroidDef is created correctly from vanilla file
        """
        currMod = tco.genMockMod("./comp_empty",self.rebrefpath)
        inst = comp.ModEntities(currMod)
        astMock,mocked = mockLoadAsteroid
        res = inst.asteroids
        assert res == astMock
        mocked.assert_called_once_with(os.path.join(self.rebrefpath,
                "GameInfo","AsteroidDef.asteroidDef"))

    def testCreationAsteroidMissing(self):
        """ Test whether missing AsteroidDef is loaded from bundled files
        """
        currMod = tco.genMockMod("./comp_empty")
        inst = comp.ModEntities(currMod)
        res = inst.asteroids
        assert isinstance(res,uncol.AsteroidDef)

    @pytest.fixture()
    def mockLoadCloud(self,mockCloud):
        mockCreate = mock.create_autospec(uncol.CloudDef.createInstance,
                spec_set=True,return_value=mockCloud)
        with mock.patch(
                "pysoase.entities.uncolonizables.CloudDef.createInstance",
                new=mockCreate) as mocked:
            yield mockCloud,mocked

    def testCreationClouds(self,mockLoadCloud):
        """ Test whether CloudDef is created correctly
        """
        cloudMock,mocked = mockLoadCloud
        res = self.inst.clouds
        assert res == cloudMock
        mocked.assert_called_once_with(os.path.join(self.modpath,
                "GameInfo","DustCloudsDef.renderingDef"))

    def testCreationCloudsReb(self,mockLoadCloud):
        """ Test whether CloudDef is created correctly from vanilla file
        """
        currMod = tco.genMockMod("./comp_empty",self.rebrefpath)
        inst = comp.ModEntities(currMod)
        cloudMock,mocked = mockLoadCloud
        res = inst.clouds
        assert res == cloudMock
        mocked.assert_called_once_with(os.path.join(self.rebrefpath,
                "GameInfo","DustCloudsDef.renderingDef"))

    def testCreationCloudsMissing(self):
        """ Test whether missing CloudsDef is loaded from bundled files
        """
        currMod = tco.genMockMod("./comp_empty")
        inst = comp.ModEntities(currMod)
        res = inst.clouds
        assert isinstance(res,uncol.CloudDef)

class TestModEntitiesLoading:
    desc = "Test ModEntities loading:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.modpath = os.path.abspath("./compprob")
        cls.rebpath = os.path.abspath("./comprebref")
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebpath)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.mockType = mock.create_autospec(coe.Entity,spec_set=True)
        patcherType = mock.patch.dict(comp.ModEntities.ENT_TO_CLASS,{
            "Debris":self.mockType
        })
        patcherType.start()
        self.mockParse = mock.create_autospec(coe.Entity.parseType,
                spec_set=True,return_value="Debris")
        patcherParse = mock.patch(
                "pysoase.entities.component.coe.Entity.parseType",
                new=self.mockParse)
        patcherParse.start()
        self.inst = comp.ModEntities(self.mmod)
        yield
        patcherType.stop()
        patcherParse.stop()

    def testLoadEntity(self):
        """ Test whether correct entity is loaded properly
        """
        self.mockType.createInstance.return_value = self.mockType
        res = self.inst.loadEntity("good.entity")
        assert res == []
        assert self.inst.compFilesMod["good.entity"] == self.mockType
        self.mockType.createInstance.assert_called_once_with(os.path.join(
                self.modpath,"GameInfo","good.entity"))

    def testLoadEntityMissing(self):
        """ Test whether missing entity returns appropriate problem
        """
        res = self.inst.loadEntity("missing.entity")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        assert len(self.inst.compFilesMod["missing.entity"]) == 1
        assert isinstance(self.inst.compFilesMod["missing.entity"][0],
                co_probs.MissingRefProblem)

    def testLoadEntityMalformedType(self):
        """ Test whether malformed type line returns correct problem
        """
        self.mockParse.return_value = [mock.sentinel.typeprob]
        res = self.inst.loadEntity("malformed_type.entity")
        assert len(res) == 1
        assert res[0] == mock.sentinel.typeprob
        assert len(self.inst.compFilesMod["malformed_type.entity"]) == 1
        assert (self.inst.compFilesMod["malformed_type.entity"][0]
                == mock.sentinel.typeprob)

    def testLoadEntityMalformed(self):
        """ Test whether malformed entity returns correct problem
        """
        self.mockType.createInstance.return_value = [mock.sentinel.typeprob]
        res = self.inst.loadEntity("malformed.entity")
        assert len(res) == 1
        assert res[0] == mock.sentinel.typeprob
        assert len(self.inst.compFilesMod["malformed.entity"]) == 1
        assert (self.inst.compFilesMod["malformed.entity"][0]
                == mock.sentinel.typeprob)

    def testGetEntity(self):
        """ Test whether getEntity returns correct entity
        """
        self.mockType.createInstance.return_value = self.mockType
        res = self.inst.getEntity("good.entity")
        assert isinstance(res,coe.Entity)

    def testGetEntityMissingFileExt(self):
        """ Test whether getEntity returns correct entity with no file extension
        """
        self.mockType.createInstance.return_value = self.mockType
        res = self.inst.getEntity("good")
        assert isinstance(res,coe.Entity)

    def testGetEntityWrongExtension(self):
        """ Test whether getEntity raises error with wrong file extension
        """
        with pytest.raises(RuntimeError):
            self.inst.getEntity("test.manifest")

    def testLoadAll(self,mockTqdm):
        """ Test whether loadAll loads all entities
        """
        currMod = tco.genMockMod("./")
        currInst = comp.ModEntities(currMod)
        calls = [mock.call(os.path.join("./","GameInfo","test2.entity")),
            mock.call(os.path.join("./","GameInfo","test1.entity"))]
        self.mockType.createInstance.return_value = self.mockType
        res = currInst.loadAll()
        assert res == []
        assert self.mockType.createInstance.call_count == 2
        self.mockType.createInstance.assert_has_calls(calls,any_order=True)
        assert len(currInst.compFilesMod) == 2
        assert currInst.compFilesMod["test1.entity"] == self.mockType
        assert currInst.compFilesMod["test2.entity"] == self.mockType

class TestModEntitiesChecking:
    desc = "Test ModEntities checking:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        modpath = os.path.abspath("./compcheck")
        rebpath = os.path.abspath("./comprebref")
        cls.mmod = tco.genMockMod(modpath,rebpath)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = comp.ModEntities(self.mmod)

    @pytest.fixture()
    def mockEntities(self):
        self.mockType = mock.create_autospec(coe.Entity,spec_set=True)
        patcherType = mock.patch.dict(comp.ModEntities.ENT_TO_CLASS,{
            "Debris":self.mockType
        })
        patcherType.start()
        self.mockParse = mock.create_autospec(coe.Entity.parseType,
                spec_set=True,return_value="Debris")
        patcherParse = mock.patch(
                "pysoase.entities.component.coe.Entity.parseType",
                new=self.mockParse)
        patcherParse.start()
        yield
        patcherType.stop()
        patcherParse.stop()

    def testCheckType(self,mockEntities):
        """ Test whether checking with correct entity works
        """
        exp = ["Debris"]
        res = self.inst.checkEntityType("correct.entity",exp)
        assert res == coe.TypeCheckResult.valid

    def testCheckTypePreloaded(self,mockEntities):
        """ Test whether checking with correct preloaded entity works
        """
        exp = ["Debris"]
        mockEtype = mock.create_autospec(co_attribs.AttribEnum,value="Debris")
        self.mockType.etype = mockEtype
        self.inst.compFilesMod["correct.entity"] = self.mockType
        assert (self.inst.checkEntityType("correct.entity",exp)
                == coe.TypeCheckResult.valid)

    def testCheckTypePreloadedWrongType(self,mockEntities):
        """ Test whether checking with incorrect preloaded entity works
        """
        exp = ["Debris"]
        mockEtype = mock.create_autospec(co_attribs.AttribEnum,value="Frigate")
        self.mockType.etype = mockEtype
        self.inst.compFilesMod["correct.entity"] = self.mockType
        assert (self.inst.checkEntityType("correct.entity",exp)
                == coe.TypeCheckResult.invalid)

    def testCheckTypeMissing(self,mockEntities):
        """ Test whether checking with missing entity works
        """
        exp = ["Debris"]
        assert (self.inst.checkEntityType("missing.entity",exp)
                == coe.TypeCheckResult.na)

    def testCheckTypeReb(self,mockEntities):
        """ Test whether checking with vanilla entity works
        """
        exp = ["Debris"]
        assert (self.inst.checkEntityType("Debris.entity",exp)
                == coe.TypeCheckResult.na)

    def testCheckTypeParseFail(self,mockEntities):
        """ Test whether checking with failed type parse returns false
        """
        exp = ["Debris"]
        self.mockParse.return_value = [mock.sentinel.prob]
        assert (self.inst.checkEntityType("correct.entity",exp)
                == coe.TypeCheckResult.na)

    def testCheckTypeMalformedFile(self,mockEntities):
        """ Test whether checking with malformed entity works
        """
        self.inst.compFilesMod["malformed.entity"] = [mock.sentinel.prob]
        exp = ["Debris"]
        assert (self.inst.checkEntityType("malformed.entity",exp)
                == coe.TypeCheckResult.na)

    @pytest.fixture()
    def mockManifest(self):
        entries = mock.MagicMock(
                elements=[mock.MagicMock(ref="correct.entity")])
        self.mockMan = mock.MagicMock(entries=entries)
        self.mockMan.mock_add_spec(manifest.ManifestEntity,spec_set=True)
        patcher = mock.patch.object(self.inst,"_manifest",self.mockMan)
        patcher.start()
        yield
        patcher.stop()

    def testCheckManifestIn(self,mockManifest):
        """ Test whether manifest checking with present entry works
        """
        self.mockMan.checkEntry.return_value = []
        res = self.inst.checkManifest("test.entity")
        assert res == []
        self.mockMan.checkEntry.assert_called_once_with("test.entity")

    def testCheckManifestNotIn(self,mockManifest):
        """ Test whether manifest checking with missing entry works
        """
        self.mockMan.checkEntry.return_value = [mock.sentinel.prob]
        res = self.inst.checkManifest("test.entity")
        assert res == [mock.sentinel.prob]
        self.mockMan.checkEntry.assert_called_once_with("test.entity")

    def testCheckManifestMissing(self):
        """ Test whether manifest checking with missing manifest works
        """
        with mock.patch.object(self.inst,"_manifest",[mock.sentinel.prob]):
            res = self.inst.checkManifest("test.entity")
            assert res == [mock.sentinel.prob]

    def testCheckEntityLoadProbs(self,mockEntities,mockManifest):
        """ Test whether checkEntity correctly returns load problems
        """
        self.mockType.createInstance.return_value = [mock.sentinel.prob]
        res = self.inst.checkEntity("malformed.entity")
        assert res == [mock.sentinel.prob]

    def testCheckEntityPreloadLoadProbs(self,mockEntities,mockManifest):
        """ Test whether checkEntity correctly returns preload load problems
        """
        self.mockType.createInstance.return_value = [mock.sentinel.prob]
        self.inst.loadEntity("malformed.entity")
        res = self.inst.checkEntity("malformed.entity")
        assert res == [mock.sentinel.prob]

    def testCheckEntityProbs(self,mockEntities,mockManifest):
        """ Test whether checkEntity correctly returns entity problems
        """
        mockEnt = mock.create_autospec(coe.Entity,spec_set=True)
        mockEnt.check.return_value = [mock.sentinel.prob]
        self.mockType.createInstance.return_value = mockEnt
        res = self.inst.checkEntity("malformed.entity")
        assert res == [mock.sentinel.prob]
        mockEnt.check.assert_called_once_with(self.mmod)

    def testCheckEntity(self,mockEntities,mockManifest):
        """ Test whether checkEntity correctly returns empty problem list
        """
        mockEnt = mock.create_autospec(coe.Entity,spec_set=True)
        mockEnt.check.return_value = []
        self.mockType.createInstance.return_value = mockEnt
        res = self.inst.checkEntity("correct.entity")
        assert res == []
        mockEnt.check.assert_called_once_with(self.mmod)

    @pytest.fixture()
    def astFixture(self,mockAst):
        patcher = mock.patch.object(self.inst,"_asteroids",mockAst)
        patcher.start()
        self.mockAst = mockAst
        yield
        patcher.stop()

    @pytest.fixture()
    def cloudFixture(self,mockCloud):
        patcher = mock.patch.object(self.inst,"_clouds",mockCloud)
        patcher.start()
        self.mockCloud = mockCloud
        yield
        patcher.stop()

    @pytest.fixture()
    def checkAllFixture(self,mockTqdm,mockEntities,mockManifest,astFixture,
            cloudFixture):
        yield

    def testCheckAllManifest(self,checkAllFixture):
        """ Test whether check checks manifest
        """
        self.inst.entities = {}
        self.mockMan.check.return_value = [mock.sentinel.prob]
        res = self.inst.check()
        self.mockMan.check.assert_called_once_with(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckAllAsteroids(self,checkAllFixture):
        """ Test whether check checks asteroids
        """
        self.inst.entities = {}
        self.mockAst.check.return_value = [mock.sentinel.prob]
        res = self.inst.check()
        self.mockAst.check.assert_called_once_with(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckAllAsteroidsMalformed(self,checkAllFixture):
        """ Test whether check handles malformed AsteroidDef correctly
        """
        self.inst.entities = {}
        self.inst._asteroids = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]

    def testCheckAllClouds(self,checkAllFixture):
        """ Test whether check checks clouds
        """
        self.inst.entities = {}
        self.mockCloud.check.return_value = [mock.sentinel.prob]
        res = self.inst.check()
        self.mockCloud.check.assert_called_once_with(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckAllCloudsMalformed(self,checkAllFixture):
        """ Test whether check handles malformed CloudDef correctly
        """
        self.inst.entities = {}
        self.inst._clouds = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]

    def testCheckAllManifestMalformed(self,checkAllFixture):
        """ Test whether check handles malformed manifest correctly
        """
        self.inst.entities = {}
        self.inst._entTypes = {}
        self.inst._manifest = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]

    def testCheckAllEntities(self,checkAllFixture):
        """ Test whether check checks all entities
        """
        mockEnt = mock.create_autospec(coe.Entity,spec_set=True)
        mockEnt.check.return_value = [mock.sentinel.prob]
        self.mockMan.check.return_value = []
        self.mockType.createInstance.return_value = mockEnt
        self.inst._entTypes["Debris"] = ["correct.entity","malformed.entity"]
        res = self.inst.check()
        assert mockEnt.check.call_count == 2
        assert res == [mock.sentinel.prob,mock.sentinel.prob]

    def testCheckAllEntitiesStrict(self,checkAllFixture):
        """ Test whether strict check checks entities in manifest only
        """
        mockEnt = mock.create_autospec(coe.Entity,spec_set=True)
        mockEnt.check.return_value = [mock.sentinel.prob]
        self.mockMan.check.return_value = []
        self.mockType.createInstance.return_value = mockEnt
        res = self.inst.check(strict=True)
        assert mockEnt.check.call_count == 1
        assert res == [mock.sentinel.prob]

    def testCheckFileAsteroids(self,astFixture):
        """ Test whether checkFile checks AsteroidDef correctly
        """
        self.mockAst.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkFile("AsteroidDef.asteroidDef")
        self.mockAst.check.assert_called_once_with(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckFileAsteroidMalformed(self):
        """ Test whether checkFile handles malformed AsteroidDef correctly
        """
        self.inst._asteroids = [mock.sentinel.prob]
        res = self.inst.checkFile("AsteroidDef.asteroidDef")
        assert res == [mock.sentinel.prob]

    def testCheckFileClouds(self,cloudFixture):
        """ Test whether checkFile checks CloudDef correctly
        """
        self.mockCloud.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkFile("DustCloudsDef.renderingDef")
        self.mockCloud.check.assert_called_once_with(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckFileCloudsMalformed(self):
        """ Test whether checkFile handles malformed CloudDef correctly
        """
        self.inst._clouds = [mock.sentinel.prob]
        res = self.inst.checkFile("DustCloudsDef.renderingDef")
        assert res == [mock.sentinel.prob]

    def testCheckFileManifest(self,mockManifest):
        """ Test whether checkFile checks manifest correctly
        """
        self.mockMan.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkFile("entity.manifest")
        self.mockMan.check.assert_called_once_with(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckFileManifestMalformed(self):
        """ Test whether checkFile handles malformed manifest correctly
        """
        self.inst._manifest = [mock.sentinel.prob]
        res = self.inst.checkFile("entity.manifest")
        assert res == [mock.sentinel.prob]

    def testCheckFileEntity(self,mockEntities):
        """ Test whether checkFile works for entities
        """
        mockEnt = mock.create_autospec(coe.Entity,spec_set=True)
        mockEnt.check.return_value = []
        self.inst.compFilesMod["correct.entity"] = mockEnt
        self.inst.checkFile("correct.entity")
        mockEnt.check.assert_called_once_with(self.mmod)

    def testCheckFileInvalid(self):
        """ Test whether checkFile raises error with invalid filetype
        """
        with pytest.raises(RuntimeError):
            self.inst.checkFile("NPC.playerThemes")

    def testCheckEntType(self,checkAllFixture):
        """ Test whether checkType checks the correct types
        """
        mockEnt1 = mock.create_autospec(coe.Entity,spec_set=True)
        mockEnt1.check.return_value = [mock.sentinel.prob]
        mockEnt2 = mock.create_autospec(coe.Entity,spec_set=True)
        self.mockType.createInstance.return_value = self.mockType
        self.inst.compFilesMod["ent1.entity"] = mockEnt1
        self.inst.compFilesMod["ent2.entity"] = mockEnt2
        self.inst._entTypes["Frigate"] = ["ent1.entity"]
        self.inst._entTypes["Planet"] = ["ent2.entity"]
        with mock.patch.object(self.inst,"_checkFileExistence",
                return_value=(co_files.FileSource.mod,[])):
            res = self.inst.checkType(["Frigate","Planet"])
            assert res == [mock.sentinel.prob]
            mockEnt1.check.assert_called_once_with(self.mmod)
            mockEnt2.check.assert_called_once_with(self.mmod)
            assert all(e is None for e in self.inst._entTypes["Debris"])
