""" Tests for the pysoase.entities.player module.
"""

import os
import unittest as unit
from unittest import mock

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.player as player
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("entities","player")

"""########################### Tests BuildPages ############################"""

class BuildPagesTitanTests(unit.TestCase):
    desc = "Tests BuildPages with only page0:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "titanInfo\n"
            +"\tPage:0\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"testtitan\"\n"
        )
        cls.identifier = "titanInfo"
        cls.page0 = {
            "identifier":"Page:0",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"testtitan"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.BuildPages(
                identifier=self.identifier,
                types=["Titan"],
                page0=self.page0
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_titanInfo.parseString(self.parseString)[0]
        res = player.BuildPages(types=["Titan"],**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPage0(self):
        """ Test whether page0 attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["Titan"]},
                **self.page0
        )
        self.assertEqual(self.inst.page0,exp)

    def testCreationAttribPage1(self):
        """ Test whether page1 attrib is None
        """
        self.assertEqual(self.inst.page1,None)

    def testCreationAttribNoPage(self):
        """ Test whether noPage attrib is None
        """
        self.assertEqual(self.inst.noPage,None)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        with mock.patch.object(self.inst.page0,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckPage0(self):
        """ Test whether check returns problems from page0 attrib
        """
        with mock.patch.object(self.inst.page0,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BuildPagesModuleTests(unit.TestCase):
    desc = "Tests BuildPages with page0,page1:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetModuleInfo\n"
            +"\tPage:0\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"mod1\"\n"
            +"\tPage:1\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"mod2\"\n"
        )
        cls.identifier = "planetModuleInfo"
        cls.page0 = {
            "identifier":"Page:0",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"mod1"}
            ]
        }
        cls.page1 = {
            "identifier":"Page:1",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"mod2"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.BuildPages(
                identifier=self.identifier,
                types=["PlanetModuleStandard"],
                page0=self.page0,
                page1=self.page1
        )
        patcher = mock.patch.object(self.inst.page0,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockP0 = patcher.start()
        patcher = mock.patch.object(self.inst.page1,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockP1 = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_planetModuleInfo.parseString(self.parseString)[0]
        res = player.BuildPages(types=["PlanetModuleStandard"],**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPage0(self):
        """ Test whether page0 attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["PlanetModuleStandard"]},
                **self.page0
        )
        self.assertEqual(self.inst.page0,exp)

    def testCreationAttribPage1(self):
        """ Test whether page1 attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["PlanetModuleStandard"]},
                **self.page1
        )
        self.assertEqual(self.inst.page1,exp)

    def testCreationAttribNoPage(self):
        """ Test whether noPage attrib is None
        """
        self.assertEqual(self.inst.noPage,None)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckPage1(self):
        """ Test whether check returns problems from page1 attrib
        """
        self.mockP1.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockP1.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BuildPagesFrigatesTests(unit.TestCase):
    desc = "Tests BuildPages with page0,page1,noPage:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "frigateInfo\n"
            +"\tPage:0\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"frig1\"\n"
            +"\tPage:1\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"frig2\"\n"
            +"\tNotOnPage\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"frig3\"\n"
        )
        cls.identifier = "frigateInfo"
        cls.page0 = {
            "identifier":"Page:0",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"frig1"}
            ]
        }
        cls.page1 = {
            "identifier":"Page:1",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"frig2"}
            ]
        }
        cls.noPage = {
            "identifier":"NotOnPage",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"frig3"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.BuildPages(
                identifier=self.identifier,
                types=["Frigate"],
                page0=self.page0,
                page1=self.page1,
                noPage=self.noPage
        )
        patcher = mock.patch.object(self.inst.page0,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockP0 = patcher.start()
        patcher = mock.patch.object(self.inst.page1,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockP1 = patcher.start()
        patcher = mock.patch.object(self.inst.noPage,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockNo = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_frigateInfo.parseString(self.parseString)[0]
        res = player.BuildPages(types=["Frigate"],**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPage0(self):
        """ Test whether page0 attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["Frigate"]},
                **self.page0
        )
        self.assertEqual(self.inst.page0,exp)

    def testCreationAttribPage1(self):
        """ Test whether page1 attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["Frigate"]},
                **self.page1
        )
        self.assertEqual(self.inst.page1,exp)

    def testCreationAttribNoPage(self):
        """ Test whether noPage attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["Frigate"]},
                **self.noPage
        )
        self.assertEqual(self.inst.noPage,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckNoPage(self):
        """ Test whether check returns problems from noPage attrib
        """
        self.mockNo.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockNo.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests PlayerBuildables #########################"""

class PlayerBuildablesTests(unit.TestCase):
    desc = "Tests PlayerBuildables:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "entities\n"
            +"\tplanetModuleInfo\n"
            +"\t\tPage:0\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"mod1\"\n"
            +"\t\tPage:1\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"mod2\"\n"
            +"\ttitanInfo\n"
            +"\t\tPage:0\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"testtitan\"\n"
            +"\tcapitalShipInfo\n"
            +"\t\tPage:0\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"capital\"\n"
            +"\tfrigateInfo\n"
            +"\t\tPage:0\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"frig1\"\n"
            +"\t\tPage:1\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"frig2\"\n"
            +"\t\tNotOnPage\n"
            +"\t\t\tcount 1\n"
            +"\t\t\tentityDefName \"frig3\"\n"
            +"\tresearchInfo\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"research\"\n"
            +"\tstarBaseInfo\n"
            +"\t\tcount 1\n"
            +"\t\tentityDefName \"starbase\"\n"
            +"\tflagship \"flagship\"\n"
        )
        cls.identifier = "entities"
        cls.planetModules = {
            "identifier":"planetModuleInfo",
            "page0":{
                "identifier":"Page:0",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"mod1"}
                ]
            },
            "page1":{
                "identifier":"Page:1",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"mod2"}
                ]
            }
        }
        cls.titans = {
            "identifier":"titanInfo",
            "page0":{
                "identifier":"Page:0",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"testtitan"}
                ]
            }
        }
        cls.capitals = {
            "identifier":"capitalShipInfo",
            "page0":{
                "identifier":"Page:0",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"capital"}
                ]
            }
        }
        cls.frigates = {
            "identifier":"frigateInfo",
            "page0":{
                "identifier":"Page:0",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"frig1"}
                ]
            },
            "page1":{
                "identifier":"Page:1",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"frig2"}
                ]
            },
            "noPage":{
                "identifier":"NotOnPage",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"frig3"}
                ]
            }
        }
        cls.research = {
            "identifier":"researchInfo",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"research"}
            ]
        }
        cls.starbases = {
            "identifier":"starBaseInfo",
            "counter":{"identifier":"count","val":1},
            "elements":[
                {"identifier":"entityDefName","val":"starbase"}
            ]
        }
        cls.flagship = {"identifier":"flagship","val":"flagship"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.PlayerBuildables(
                identifier=self.identifier,
                planetModules=self.planetModules,
                titans=self.titans,
                capitals=self.capitals,
                frigates=self.frigates,
                research=self.research,
                starbases=self.starbases,
                flagship=self.flagship
        )
        patcher = mock.patch.object(self.inst.planetModules,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockModules = patcher.start()
        patcher = mock.patch.object(self.inst.titans,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTitans = patcher.start()
        patcher = mock.patch.object(self.inst.capitals,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockCapitals = patcher.start()
        patcher = mock.patch.object(self.inst.frigates,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockFrigates = patcher.start()
        patcher = mock.patch.object(self.inst.research,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockResearch = patcher.start()
        patcher = mock.patch.object(self.inst.starbases,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockStarbases = patcher.start()
        patcher = mock.patch.object(self.inst.flagship,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockFlagship = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_playerBuildables.parseString(self.parseString)[0]
        res = player.PlayerBuildables(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPlanetModules(self):
        """ Test whether planetModules attrib is created correctly
        """
        exp = player.BuildPages(
                types=["PlanetModuleHangarDefense","PlanetModuleRefinery",
                    "PlanetModuleShipFactory","PlanetModuleStandard",
                    "PlanetModuleTradePort","PlanetModuleWeaponDefense"],
                **self.planetModules
        )
        self.assertEqual(self.inst.planetModules,exp)

    def testCreationAttribTitans(self):
        """ Test whether titans attrib is created correctly
        """
        exp = player.BuildPages(
                types=["Titan"],
                **self.titans
        )
        self.assertEqual(self.inst.titans,exp)

    def testCreationAttribCapitals(self):
        """ Test whether capitals attrib is created correctly
        """
        exp = player.BuildPages(
                types=["CapitalShip"],
                **self.capitals
        )
        self.assertEqual(self.inst.capitals,exp)

    def testCreationAttribFrigates(self):
        """ Test whether frigates attrib is created correctly
        """
        exp = player.BuildPages(
                types=["Frigate"],
                **self.frigates
        )
        self.assertEqual(self.inst.frigates,exp)

    def testCreationAttribResearch(self):
        """ Test whether research attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["ResearchSubject"]},
                **self.research
        )
        self.assertEqual(self.inst.research,exp)

    def testCreationAttribStarbases(self):
        """ Test whether starbases attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["StarBase"]},
                **self.starbases
        )
        self.assertEqual(self.inst.starbases,exp)

    def testCreationAttribFlagship(self):
        """ Test whether flagship attrib is created correctly
        """
        exp = coe.EntityRef(types=["Titan","Frigate","CapitalShip"],
                **self.flagship)
        self.assertEqual(self.inst.flagship,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckModules(self):
        """ Test whether check returns problems from planetModules attrib
        """
        self.mockModules.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockModules.assert_called_once_with(self.mmod,False)

    def testCheckTitans(self):
        """ Test whether check returns problems from titans attrib
        """
        self.mockTitans.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTitans.assert_called_once_with(self.mmod,False)

    def testCheckCapitals(self):
        """ Test whether check returns problems from capitals attrib
        """
        self.mockCapitals.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockCapitals.assert_called_once_with(self.mmod,False)

    def testCheckFrigates(self):
        """ Test whether check returns problems from frigates attrib
        """
        self.mockFrigates.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockFrigates.assert_called_once_with(self.mmod,False)

    def testCheckResearch(self):
        """ Test whether check returns problems from research attrib
        """
        self.mockResearch.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockResearch.assert_called_once_with(self.mmod,False)

    def testCheckStarbases(self):
        """ Test whether check returns problems from starbases attrib
        """
        self.mockStarbases.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockStarbases.assert_called_once_with(self.mmod,False)

    def testCheckFlagship(self):
        """ Test whether check returns problems from flagship attrib
        """
        self.mockFlagship.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockFlagship.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests DebrisData ############################"""

class DebrisDataTests(unit.TestCase):
    desc = "Tests DebrisData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "debrisData\n"
            +"\tnumRandomMeshNamesLarge 1\n"
            +"\trandomMeshName \"debris1\"\n"
            +"\tnumRandomMeshNamesSmall 1\n"
            +"\trandomMeshName \"debris2\"\n"
        )
        cls.identifier = "debrisData"
        cls.meshesLarge = {
            "counter":{"identifier":"numRandomMeshNamesLarge","val":1},
            "elements":[
                {"identifier":"randomMeshName","val":"debris1"}
            ]
        }
        cls.meshesSmall = {
            "counter":{"identifier":"numRandomMeshNamesSmall","val":1},
            "elements":[
                {"identifier":"randomMeshName","val":"debris2"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.DebrisData(
                identifier=self.identifier,
                meshesLarge=self.meshesLarge,
                meshesSmall=self.meshesSmall
        )
        patcher = mock.patch.object(self.inst.meshesLarge,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockLarge = patcher.start()
        patcher = mock.patch.object(self.inst.meshesSmall,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockSmall = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_debrisData.parseString(self.parseString)[0]
        res = player.DebrisData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMeshesLarge(self):
        """ Test whether meshesLarge attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=meshes.MeshRef,**self.meshesLarge)
        self.assertEqual(self.inst.meshesLarge,exp)

    def testCreationAttribMeshesSmall(self):
        """ Test whether meshesSmall attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=meshes.MeshRef,**self.meshesSmall)
        self.assertEqual(self.inst.meshesSmall,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckLarge(self):
        """ Test whether check returns problems from meshesLarge attrib
        """
        self.mockLarge.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckSmall(self):
        """ Test whether check returns problems from meshesSmall attrib
        """
        self.mockSmall.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests HyperspaceEffects #########################"""

class HyperspaceEffectsTests(unit.TestCase):
    desc = "Tests HyperspaceEffects:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "frigateChargeUpEffectName \"frigchargeup\"\n"
            "frigateChargeUpEffectNameBetweenStars \"frigchargeupstars\"\n"
            "frigateChargeUpEffectNameDestabilized \"frigchargeupdestabilize\"\n"
            "frigateChargeUpEffectSoundID \"frigchargeupsound\"\n"
            "frigateTravelEffectName \"frigtravel\"\n"
            "frigateTravelEffectNameBetweenStars \"frigtravelstars\"\n"
            "frigateTravelEffectNameDestabilized \"frigtraveldestabilized\"\n"
            "frigateTravelEffectSoundID \"frigtravelsound\"\n"
            "frigateExitEffectName \"frigexit\"\n"
            "frigateExitEffectSoundID \"frigexitsound\"\n"
        )
        cls.chargeUp = {"identifier":"frigateChargeUpEffectName",
            "val":"frigchargeup"}
        cls.chargeUpBetweenStars = {
            "identifier":"frigateChargeUpEffectNameBetweenStars",
            "val":"frigchargeupstars"}
        cls.chargeUpDestabilized = {
            "identifier":"frigateChargeUpEffectNameDestabilized",
            "val":"frigchargeupdestabilize"}
        cls.chargeUpSound = {
            "identifier":"frigateChargeUpEffectSoundID",
            "val":"frigchargeupsound"
        }
        cls.travel = {"identifier":"frigateTravelEffectName",
            "val":"frigtravel"}
        cls.travelBetweenStars = {
            "identifier":"frigateTravelEffectNameBetweenStars",
            "val":"frigtravelstars"}
        cls.travelDestabilized = {
            "identifier":"frigateTravelEffectNameDestabilized",
            "val":"frigtraveldestabilized"}
        cls.travelSound = {"identifier":"frigateTravelEffectSoundID",
            "val":"frigtravelsound"}
        cls.exit = {"identifier":"frigateExitEffectName","val":"frigexit"}
        cls.exitSound = {"identifier":"frigateExitEffectSoundID",
            "val":"frigexitsound"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.HyperspaceEffects(
                chargeUp=self.chargeUp,
                chargeUpBetweenStars=self.chargeUpBetweenStars,
                chargeUpDestabilized=self.chargeUpDestabilized,
                chargeUpSound=self.chargeUpSound,
                travel=self.travel,
                travelBetweenStars=self.travelBetweenStars,
                travelDestabilized=self.travelDestabilized,
                travelSound=self.travelSound,
                exit=self.exit,
                exitSound=self.exitSound
        )
        patcher = mock.patch.object(self.inst.chargeUp,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockChargeUp = patcher.start()
        patcher = mock.patch.object(self.inst.chargeUpBetweenStars,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockChargeUpBetweenStars = patcher.start()
        patcher = mock.patch.object(self.inst.chargeUpDestabilized,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockChargeUpDestabilized = patcher.start()
        patcher = mock.patch.object(self.inst.chargeUpSound,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockChargeUpSound = patcher.start()
        patcher = mock.patch.object(self.inst.travel,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTravel = patcher.start()
        patcher = mock.patch.object(self.inst.travelBetweenStars,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTravelBetweenStars = patcher.start()
        patcher = mock.patch.object(self.inst.travelDestabilized,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTravelDestabilized = patcher.start()
        patcher = mock.patch.object(self.inst.travelSound,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTravelSound = patcher.start()
        patcher = mock.patch.object(self.inst.exit,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExit = patcher.start()
        patcher = mock.patch.object(self.inst.exitSound,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExitSound = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = pp.Group(
                co_parse.genStringAttrib("frigateChargeUpEffectName")("chargeUp")
                +co_parse.genStringAttrib("frigateChargeUpEffectNameBetweenStars")(
                        "chargeUpBetweenStars")
                +co_parse.genStringAttrib("frigateChargeUpEffectNameDestabilized")(
                        "chargeUpDestabilized")
                +co_parse.genStringAttrib("frigateChargeUpEffectSoundID")(
                        "chargeUpSound")
                +co_parse.genStringAttrib("frigateTravelEffectName")("travel")
                +co_parse.genStringAttrib("frigateTravelEffectNameBetweenStars")(
                        "travelBetweenStars")
                +co_parse.genStringAttrib("frigateTravelEffectNameDestabilized")(
                        "travelDestabilized")
                +co_parse.genStringAttrib("frigateTravelEffectSoundID")("travelSound")
                +co_parse.genStringAttrib("frigateExitEffectName")("exit")
                +co_parse.genStringAttrib("frigateExitEffectSoundID")("exitSound")
        )
        parseRes = parser.parseString(self.parseString)[0]
        res = player.HyperspaceEffects(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribChargeUp(self):
        """ Test whether chargeUp attrib is created correctly
        """
        exp = par.ParticleRef(**self.chargeUp)
        self.assertEqual(self.inst.chargeUp,exp)

    def testCreationAttribChargeUpBetweenStars(self):
        """ Test whether chargeUpBetweenStars attrib is created correctly
        """
        exp = par.ParticleRef(**self.chargeUpBetweenStars)
        self.assertEqual(self.inst.chargeUpBetweenStars,exp)

    def testCreationAttribChargeUpDestabilized(self):
        """ Test whether chargeUpDestabilized attrib is created correctly
        """
        exp = par.ParticleRef(**self.chargeUpDestabilized)
        self.assertEqual(self.inst.chargeUpDestabilized,exp)

    def testCreationAttribChargeUpSound(self):
        """ Test whether chargeUpSound attrib is created correctly
        """
        exp = audio.SoundRef(**self.chargeUpSound)
        self.assertEqual(self.inst.chargeUpSound,exp)

    def testCreationAttribTravel(self):
        """ Test whether travel attrib is created correctly
        """
        exp = par.ParticleRef(**self.travel)
        self.assertEqual(self.inst.travel,exp)

    def testCreationAttribTravelBetweenStars(self):
        """ Test whether travelBetweenStars attrib is created correctly
        """
        exp = par.ParticleRef(**self.travelBetweenStars)
        self.assertEqual(self.inst.travelBetweenStars,exp)

    def testCreationAttribTravelDestabilized(self):
        """ Test whether travelDestabilized attrib is created correctly
        """
        exp = par.ParticleRef(**self.travelDestabilized)
        self.assertEqual(self.inst.travelDestabilized,exp)

    def testCreationAttribTravelSound(self):
        """ Test whether travelSound attrib is created correctly
        """
        exp = audio.SoundRef(**self.travelSound)
        self.assertEqual(self.inst.travelSound,exp)

    def testCreationAttribExit(self):
        """ Test whether exit attrib is created correctly
        """
        exp = par.ParticleRef(**self.exit)
        self.assertEqual(self.inst.exit,exp)

    def testCreationAttribExitSound(self):
        """ Test whether exitSound attrib is created correctly
        """
        exp = audio.SoundRef(**self.exitSound)
        self.assertEqual(self.inst.exitSound,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckChargeUp(self):
        """ Test whether check returns problems from chargeUp attrib
        """
        self.mockChargeUp.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockChargeUp.assert_called_once_with(self.mmod,False)

    def testCheckChargeUpBetweenStars(self):
        """ Test whether check returns problems from chargeUpBetweenStars attrib
        """
        self.mockChargeUpBetweenStars.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockChargeUpBetweenStars.assert_called_once_with(self.mmod,False)

    def testCheckChargeUpDestabilized(self):
        """ Test whether check returns problems from chargeUpDestabilized attrib
        """
        self.mockChargeUpDestabilized.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockChargeUpDestabilized.assert_called_once_with(self.mmod,False)

    def testCheckChargeUpSound(self):
        """ Test whether check returns problems from chargeUpSound attrib
        """
        self.mockChargeUpSound.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockChargeUpSound.assert_called_once_with(self.mmod,False)

    def testCheckTravel(self):
        """ Test whether check returns problems from travel attrib
        """
        self.mockTravel.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTravel.assert_called_once_with(self.mmod,False)

    def testCheckTravelBetweenStars(self):
        """ Test whether check returns problems from travelBetweenStars attrib
        """
        self.mockTravelBetweenStars.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTravelBetweenStars.assert_called_once_with(self.mmod,False)

    def testCheckTravelDestabilized(self):
        """ Test whether check returns problems from travelDestabilized attrib
        """
        self.mockTravelDestabilized.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTravelDestabilized.assert_called_once_with(self.mmod,False)

    def testCheckTravelSound(self):
        """ Test whether check returns problems from travelSound attrib
        """
        self.mockTravelSound.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTravelSound.assert_called_once_with(self.mmod,False)

    def testCheckExit(self):
        """ Test whether check returns problems from exit attrib
        """
        self.mockExit.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockExit.assert_called_once_with(self.mmod,False)

    def testCheckExitSound(self):
        """ Test whether check returns problems from exitSound attrib
        """
        self.mockExitSound.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockExitSound.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests HyperspaceData ###########################"""

class HyperspaceDataTests(unit.TestCase):
    desc = "Tests HyperspaceData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "hyperspaceData\n"
            +"\tfrigateChargeUpEffectName \"frigchargeup\"\n"
            +"\tfrigateChargeUpEffectNameBetweenStars \"frigchargeupstars\"\n"
            +"\tfrigateChargeUpEffectNameDestabilized \"frigchargeupdestabilize\"\n"
            +"\tfrigateChargeUpEffectSoundID \"frigchargeupsound\"\n"
            +"\tfrigateTravelEffectName \"frigtravel\"\n"
            +"\tfrigateTravelEffectNameBetweenStars \"frigtravelstars\"\n"
            +"\tfrigateTravelEffectNameDestabilized \"frigtraveldestabilized\"\n"
            +"\tfrigateTravelEffectSoundID \"frigtravelsound\"\n"
            +"\tfrigateExitEffectName \"frigexit\"\n"
            +"\tfrigateExitEffectSoundID \"frigexitsound\"\n"
            +"\tcapitalShipChargeUpEffectName \"capchargeup\"\n"
            +"\tcapitalShipChargeUpEffectNameBetweenStars \"capchargeupstars\"\n"
            +"\tcapitalShipChargeUpEffectNameDestabilized \"capchargeupdestabilize\"\n"
            +"\tcapitalShipChargeUpEffectSoundID \"capchargeupsound\"\n"
            +"\tcapitalShipTravelEffectName \"captravel\"\n"
            +"\tcapitalShipTravelEffectNameBetweenStars \"captravelstars\"\n"
            +"\tcapitalShipTravelEffectNameDestabilized \"captraveldestabilize\"\n"
            +"\tcapitalShipTravelEffectSoundID \"captravelsound\"\n"
            +"\tcapitalShipExitEffectName \"capexit\"\n"
            +"\tcapitalShipExitEffectSoundID \"capexitsound\"\n"
            +"\ttitanChargeUpEffectName \"titanchargeup\"\n"
            +"\ttitanChargeUpEffectNameBetweenStars \"titanchargeupstars\"\n"
            +"\ttitanChargeUpEffectNameDestabilized \"titanchargeupdestabilize\"\n"
            +"\ttitanChargeUpEffectSoundID \"titanchargeupsound\"\n"
            +"\ttitanTravelEffectName \"titantravel\"\n"
            +"\ttitanTravelEffectNameBetweenStars \"titantravelstars\"\n"
            +"\ttitanTravelEffectNameDestabilized \"titantraveldestabilize\"\n"
            +"\ttitanTravelEffectSoundID \"titantravelsound\"\n"
            +"\ttitanExitEffectName \"titanexit\"\n"
            +"\ttitanExitEffectSoundID \"titanexitsound\"\n"
            +"\tbaseHyperspaceSpeed 29500.000000\n"
            +"\thyperspaceSpeedBetweenStarsMultiplier 24.000000\n"
            +"\thyperspaceSpeedToFriendlyOrbitBodiesMultiplier 1.000000\n"
            +"\tbaseHyperspaceAntiMatterCost 100.000000\n"
            +"\tbaseHyperspaceChargeUpTime 7.000000\n"
        )
        cls.identifier = "hyperspaceData"
        cls.frigates = {
            "chargeUp":{"identifier":"frigateChargeUpEffectName",
                "val":"frigchargeup"},
            "chargeUpBetweenStars":{
                "identifier":"frigateChargeUpEffectNameBetweenStars",
                "val":"frigchargeupstars"},
            "chargeUpDestabilized":{
                "identifier":"frigateChargeUpEffectNameDestabilized",
                "val":"frigchargeupdestabilize"},
            "chargeUpSound":{
                "identifier":"frigateChargeUpEffectSoundID",
                "val":"frigchargeupsound"
            },
            "travel":{"identifier":"frigateTravelEffectName",
                "val":"frigtravel"},
            "travelBetweenStars":{
                "identifier":"frigateTravelEffectNameBetweenStars",
                "val":"frigtravelstars"},
            "travelDestabilized":{
                "identifier":"frigateTravelEffectNameDestabilized",
                "val":"frigtraveldestabilized"},
            "travelSound":{"identifier":"frigateTravelEffectSoundID",
                "val":"frigtravelsound"},
            "exit":{"identifier":"frigateExitEffectName","val":"frigexit"},
            "exitSound":{"identifier":"frigateExitEffectSoundID",
                "val":"frigexitsound"}
        }
        cls.capitals = {
            "chargeUp":{"identifier":"capitalShipChargeUpEffectName",
                "val":"capchargeup"},
            "chargeUpBetweenStars":{
                "identifier":"capitalShipChargeUpEffectNameBetweenStars",
                "val":"capchargeupstars"},
            "chargeUpDestabilized":{
                "identifier":"capitalShipChargeUpEffectNameDestabilized",
                "val":"capchargeupdestabilize"},
            "chargeUpSound":{
                "identifier":"capitalShipChargeUpEffectSoundID",
                "val":"capchargeupsound"
            },
            "travel":{"identifier":"capitalShipTravelEffectName",
                "val":"captravel"},
            "travelBetweenStars":{
                "identifier":"capitalShipTravelEffectNameBetweenStars",
                "val":"captravelstars"},
            "travelDestabilized":{
                "identifier":"capitalShipTravelEffectNameDestabilized",
                "val":"captraveldestabilize"},
            "travelSound":{"identifier":"capitalShipTravelEffectSoundID",
                "val":"captravelsound"},
            "exit":{"identifier":"capitalShipExitEffectName","val":"capexit"},
            "exitSound":{"identifier":"capitalShipExitEffectSoundID",
                "val":"capexitsound"}
        }
        cls.titans = {
            "chargeUp":{"identifier":"titanChargeUpEffectName",
                "val":"titanchargeup"},
            "chargeUpBetweenStars":{
                "identifier":"titanChargeUpEffectNameBetweenStars",
                "val":"titanchargeupstars"},
            "chargeUpDestabilized":{
                "identifier":"titanChargeUpEffectNameDestabilized",
                "val":"titanchargeupdestabilize"},
            "chargeUpSound":{
                "identifier":"titanChargeUpEffectSoundID",
                "val":"titanchargeupsound"
            },
            "travel":{"identifier":"titanTravelEffectName",
                "val":"titantravel"},
            "travelBetweenStars":{
                "identifier":"titanTravelEffectNameBetweenStars",
                "val":"titantravelstars"},
            "travelDestabilized":{
                "identifier":"titanTravelEffectNameDestabilized",
                "val":"titantraveldestabilize"},
            "travelSound":{"identifier":"titanTravelEffectSoundID",
                "val":"titantravelsound"},
            "exit":{"identifier":"titanExitEffectName","val":"titanexit"},
            "exitSound":{"identifier":"titanExitEffectSoundID",
                "val":"titanexitsound"}
        }
        cls.baseHyperSpeed = {"identifier":"baseHyperspaceSpeed","val":29500.0}
        cls.speedBetweenStarsMult = {
            "identifier":"hyperspaceSpeedBetweenStarsMultiplier",
            "val":24.0}
        cls.speedFriendlyGravWellMult = {
            "identifier":"hyperspaceSpeedToFriendlyOrbitBodiesMultiplier",
            "val":1.0}
        cls.baseHyperspaceAMCost = {"identifier":"baseHyperspaceAntiMatterCost",
            "val":100.0}
        cls.baseHyperChargeupTime = {"identifier":"baseHyperspaceChargeUpTime",
            "val":7.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.HyperspaceData(
                identifier=self.identifier,
                frigates=self.frigates,
                capitals=self.capitals,
                titans=self.titans,
                baseHyperSpeed=self.baseHyperSpeed,
                speedBetweenStarsMult=self.speedBetweenStarsMult,
                speedFriendlyGravWellMult=self.speedFriendlyGravWellMult,
                baseHyperspaceAMCost=self.baseHyperspaceAMCost,
                baseHyperChargeupTime=self.baseHyperChargeupTime
        )
        patcher = mock.patch.object(self.inst.frigates,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockFrigates = patcher.start()
        patcher = mock.patch.object(self.inst.capitals,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockCapitals = patcher.start()
        patcher = mock.patch.object(self.inst.titans,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockTitans = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_hyperspaceData.parseString(self.parseString)[0]
        res = player.HyperspaceData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribFrigates(self):
        """ Test whether frigates attribute is created correctly
        """
        exp = player.HyperspaceEffects(**self.frigates)
        self.assertEqual(self.inst.frigates,exp)

    def testCreationAttribCapitals(self):
        """ Test whether capitals attribute is created correctly
        """
        exp = player.HyperspaceEffects(**self.capitals)
        self.assertEqual(self.inst.capitals,exp)

    def testCreationAttribTitans(self):
        """ Test whether titans attribute is created correctly
        """
        exp = player.HyperspaceEffects(**self.titans)
        self.assertEqual(self.inst.titans,exp)

    def testCreationAttribBaseHyperSpeed(self):
        """ Test whether baseHyperSpeed attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.baseHyperSpeed)
        self.assertEqual(self.inst.baseHyperSpeed,exp)

    def testCreationAttribSpeedBetweenStarsMult(self):
        """ Test whether speedBetweenStarsMult attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedBetweenStarsMult)
        self.assertEqual(self.inst.speedBetweenStarsMult,exp)

    def testCreationAttribSpeedFriendlyGravWellMult(self):
        """ Test whether speedFriendlyGravWellMult attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedFriendlyGravWellMult)
        self.assertEqual(self.inst.speedFriendlyGravWellMult,exp)

    def testCreationAttribBaseHyperspaceAMCost(self):
        """ Test whether baseHyperspaceAMCost attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.baseHyperspaceAMCost)
        self.assertEqual(self.inst.baseHyperspaceAMCost,exp)

    def testCreationAttribBaseHyperChargeupTime(self):
        """ Test whether baseHyperChargeupTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.baseHyperChargeupTime)
        self.assertEqual(self.inst.baseHyperChargeupTime,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckFrigates(self):
        """ Test whether check returns problems from frigates attrib
        """
        self.mockFrigates.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockFrigates.assert_called_once_with(self.mmod,False)

    def testCheckCapitals(self):
        """ Test whether check returns problems from capitals attrib
        """
        self.mockCapitals.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockCapitals.assert_called_once_with(self.mmod,False)

    def testCheckTitans(self):
        """ Test whether check returns problems from titans attrib
        """
        self.mockTitans.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTitans.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests ShieldData #############################"""

class ShieldDataTests(unit.TestCase):
    desc = "Tests ShieldData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "shieldData\n"
            +"\tshieldAbsorbGrowthPerDamage 0.001000\n"
            +"\tshieldAbsorbDecayRate 0.012500\n"
            +"\tshieldAbsorbBaseMin 0.150000\n"
            +"\tshieldColor ffffc705\n"
        )
        cls.identifier = "shieldData"
        cls.absorbPerDmg = {"identifier":"shieldAbsorbGrowthPerDamage",
            "val":0.001}
        cls.absorbDecay = {"identifier":"shieldAbsorbDecayRate","val":0.0125}
        cls.absorbMin = {"identifier":"shieldAbsorbBaseMin","val":0.15}
        cls.color = {"identifier":"shieldColor","val":"ffffc705"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.ShieldData(
                identifier=self.identifier,
                absorbPerDmg=self.absorbPerDmg,
                absorbDecay=self.absorbDecay,
                absorbMin=self.absorbMin,
                color=self.color
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_shieldData.parseString(self.parseString)[0]
        res = player.ShieldData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAbsorbPerDmg(self):
        """ Test whether absorbPerDmg attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.absorbPerDmg)
        self.assertEqual(self.inst.absorbPerDmg,exp)

    def testCreationAttribAbsorbDecay(self):
        """ Test whether absorbDecay attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.absorbDecay)
        self.assertEqual(self.inst.absorbDecay,exp)

    def testCreationAttribAbsorbMin(self):
        """ Test whether absorbMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.absorbMin)
        self.assertEqual(self.inst.absorbMin,exp)

    def testCreationAttribColor(self):
        """ Test whether color attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.color)
        self.assertEqual(self.inst.color,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests GameEventData ###########################"""

class GameEventDataTests(unit.TestCase):
    desc = "Tests GameEventData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "gameEventData\n"
            "\tGameEventSound:AllianceCeaseFireBroken \"AllianceCeaseFireBroken\"\n"
            "\tGameEventSound:AllianceCeaseFireFormed \"GameEventSound:AllianceCeaseFireFormed\"\n"
            "\tGameEventSound:AllianceCeaseFireOffered \"GameEventSound:AllianceCeaseFireOffered\"\n"
            "\tGameEventSound:AllianceOtherBroken \"GameEventSound:AllianceOtherBroken\"\n"
            "\tGameEventSound:AllianceOtherFormed \"GameEventSound:AllianceOtherFormed\"\n"
            "\tGameEventSound:AllianceOtherOffered \"GameEventSound:AllianceOtherOffered\"\n"
            "\tGameEventSound:AllyBackUpArrived \"GameEventSound:AllyBackUpArrived\"\n"
            "\tGameEventSound:AllyBackUpEnRoute \"GameEventSound:AllyBackUpEnRoute\"\n"
            "\tGameEventSound:AllyTitanQueued \"GameEventSound:AllyTitanQueued\"\n"
            "\tGameEventSound:AllyTitanCompleted \"GameEventSound:AllyTitanCompleted\"\n"
            "\tGameEventSound:EnemyTitanQueued \"GameEventSound:EnemyTitanQueued\"\n"
            "\tGameEventSound:EnemyTitanCompleted \"GameEventSound:EnemyTitanCompleted\"\n"
            "\tGameEventSound:AllyTitanLost \"GameEventSound:AllyTitanLost\"\n"
            "\tGameEventSound:AllyCapitalShipLost \"EVENTTECH_ALLYCAPITALSHIPLOST\"\n"
            "\tGameEventSound:AllyFleetArrived \"EVENTTECH_ALLYFLEETARRIVED\"\n"
            "\tGameEventSound:AllyPlanetLost \"EVENTTECH_ALLYPLANETLOST\"\n"
            "\tGameEventSound:AllyPlanetUnderAttack \"EVENTTECH_ALLYPLANETUNDERATTACK\"\n"
            "\tGameEventSound:AllyRequestAttackPlanet \"EVENTTECH_ALLYREQUESTATTACK\"\n"
            "\tGameEventSound:AllyRequestDefendPlanet \"EVENTTECH_ALLYREQUESTDEFEND\"\n"
            "\tGameEventSound:AllyUnitUnderAttack \"EVENTTECH_ALLYUNITUNDERATTACK\"\n"
            "\tGameEventSound:BountyAllDepleted \"EVENTTECH_BOUNTYALLDEPLETED\"\n"
            "\tGameEventSound:BountyIncreasedOther \"EVENTTECH_BOUNTYINCREASEDOTHER\"\n"
            "\tGameEventSound:BountyIncreasedPlayer \"EVENTTECH_BOUNTYINCREASEDPLAYER\"\n"
            "\tGameEventSound:BuyNoCommand \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:BuyNoCredits \"EVENTTECH_BUYNOCREDITS\"\n"
            "\tGameEventSound:BuyNoMetal \"EVENTTECH_BUYNOMETAL\"\n"
            "\tGameEventSound:BuyNoCrystal \"EVENTTECH_BUYNOCRYSTAL\"\n"
            "\tGameEventSound:BuyNoFrigateFactory \"EVENTTECH_BUYNOFRIGATEFACTORY\"\n"
            "\tGameEventSound:BuyNoCapitalShipFactory \"EVENTTECH_BUYNOCAPITALSHIPFACTORY\"\n"
            "\tGameEventSound:BuyNoTitanFactory \"EVENTTECH_BUYNOTITANFACTORY\"\n"
            "\tGameEventSound:BuyNoAvailableShipSlots \"EVENTTECH_RESEARCHNEEDPREREQUISITE\"\n"
            "\tGameEventSound:BuyNoAvailableCapitalShipSlots \"EVENTTECH_RESEARCHNEEDPREREQUISITE\"\n"
            "\tGameEventSound:BuyAlreadyHasTitan \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:CannonShellDetected \"EVENTTECH_CANNONSHELLDETECTED\"\n"
            "\tGameEventSound:RandomEventDetected \"Effect_RandomEventDetected\"\n"
            "\tGameEventSound:CannonUnderConstruction \"UI_COMMON_CANNONBUILT\"\n"
            "\tGameEventSound:ColonizePlanetAlreadyOwned \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ColonizePlanetNotColonizable \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ColonizePlanetAlreadyBeingColonized \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ColonizeNeedPrerequisiteResearch \"EVENTTECH_RESEARCHNEEDPREREQUISITE\"\n"
            "\tGameEventSound:ColonizePlanetHasTakeoverCulture \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ColonizePlanetHasDebuff \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ColonizePlanetBlockedByEnemyStarBase \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:RuinPlanetNotRuinable \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:PirateFleetArrived \"EVENTTECH_PIRATEFLEETARRIVED\"\n"
            "\tGameEventSound:PirateRaidImminent \"EVENTTECH_PIRATERAIDIMMINENT\"\n"
            "\tGameEventSound:PirateRaidLaunched \"EVENTTECH_PIRATERAIDLAUNCHED\"\n"
            "\tGameEventSound:HostileFleetArrived \"EVENTTECH_HOSTILEFLEETARRIVED\"\n"
            "\tGameEventSound:HostileFleetEnRoute \"EVENTTECH_HOSTILEFLEETENROUTE\"\n"
            "\tGameEventSound:HostilePlanetDestroyed \"EVENTTECH_HOSTILEPLANETDESTROYED\"\n"
            "\tGameEventSound:ModuleComplete \"EVENTTECH_MODULECOMPLETE\"\n"
            "\tGameEventSound:MoveCannotTravelBetweenPlanets \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:NoValidPath \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:NoValidPathObserved \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:NoValidPathBetweenStars \"EVENTTECH_RESEARCHNEEDPREREQUISITE\"\n"
            "\tGameEventSound:NoValidPathDueToBuff \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:Ping \"UI_COMMON_PING\"\n"
            "\tGameEventSound:PlanetUpgradeAlreadyMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:PlanetUpgradeComplete \"EVENTTECH_PLANETUPGRADECOMPLETE\"\n"
            "\tGameEventSound:PlanetUpgradeMaxLevelQueued \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:NewPlanetDiscovered \"EVENTTECH_NEWPLANETDISCOVERED\"\n"
            "\tGameEventSound:ArtifactDiscovered \"EVENTTECH_ARTIFACTDISCOVERED\"\n"
            "\tGameEventSound:ArtifactDiscoveredOther \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:BonusDiscovered \"EVENTTECH_BONUSDISCOVERED\"\n"
            "\tGameEventSound:NothingDiscovered \"EVENTTECH_NOTHINGDISCOVERED\"\n"
            "\tGameEventSound:LaunchSquadsDisabledByDebuff \"LaunchSquadsDisabledByDebuff\"\n"
            "\tGameEventSound:PlayerBackUpArrived \"EVENTTECH_PLAYERBACKUPARRIVED\"\n"
            "\tGameEventSound:PlayerTitanCreated \"PlayerTitanCreated\"\n"
            "\tGameEventSound:PlayerTitanLost \"EVENTTECH_REBEL_PLAYERTITANLOST_0\"\n"
            "\tGameEventSound:PlayerCapitalShipCreated \"PlayerCapitalShipCreated\"\n"
            "\tGameEventSound:PlayerCapitalShipLost \"EVENTTECH_PLAYERCAPITALSHIPLOST\"\n"
            "\tGameEventSound:PlayerStarbaseLost \"PlayerStarbaseLost\"\n"
            "\tGameEventSound:PlayerFleetArrived \"EVENTTECH_PLAYERFLEETARRIVED\"\n"
            "\tGameEventSound:PlayerFleetArrivedToFleetBeacon \"PlayerFleetArrivedToFleetBeacon\"\n"
            "\tGameEventSound:PlayerFleetDeparted \"PlayerFleetDeparted\"\n"
            "\tGameEventSound:PlayerFrigateCreated \"PlayerFrigateCreated\"\n"
            "\tGameEventSound:PlayerPlanetColonized \"EVENTTECH_PLAYERPLANETCOLONIZED\"\n"
            "\tGameEventSound:PlayerCapitalPlanetChanged \"EVENTTECH_PLAYERCAPITALPLANETCHANGED\"\n"
            "\tGameEventSound:PlayerCapitalPlanetLost \"EVENTTECH_PLAYERCAPITALPLANETDESTROYED\"\n"
            "\tGameEventSound:AllyCapitalPlanetLost \"EVENTTECH_ALLYCAPITALPLANETDESTROYED\"\n"
            "\tGameEventSound:EnemyCapitalPlanetLost \"EVENTTECH_ENEMYCAPITALPLANETDESTROYED\"\n"
            "\tGameEventSound:PlayerPlanetLost \"EVENTTECH_PLAYERPLANETLOST\"\n"
            "\tGameEventSound:PlayerPlanetLostToCulture \"EVENTTECH_PLAYERPLANETLOST\"\n"
            "\tGameEventSound:PlayerPlanetSoonLostToCulture \"EVENTTECH_PLAYERPLANETSOONLOSTTOCULTURE\"\n"
            "\tGameEventSound:PlayerPlanetUnderAttack \"EVENTTECH_PLAYERPLANETUNDERATTACK\"\n"
            "\tGameEventSound:PlayerReceivedResourcesFromAlly \"EFFECT_COINDROP\"\n"
            "\tGameEventSound:PlayerUnitUnderAttack \"EVENTTECH_PLAYERUNITUNDERATTACK\"\n"
            "\tGameEventSound:PlayerFlagshipUnderAttack \"EVENTTECH_PLAYERFLAGSHIPUNDERATTACK\"\n"
            "\tGameEventSound:PlayerFlagshipShieldsDown \"EVENTTECH_PLAYERFLAGSHIPSHIELDSDOWN\"\n"
            "\tGameEventSound:PlayerFlagshipHullSeverelyDamaged \"EVENTTECH_PLAYERFLAGSHIPHULLSEVERELYDAMAGED\"\n"
            "\tGameEventSound:PlayerFlagshipDestroyed \"EVENTTECH_PLAYERFLAGSHIPDESTROYED\"\n"
            "\tGameEventSound:AllyFlagshipDestroyed \"EVENTTECH_ALLYFLAGSHIPDESTROYED\"\n"
            "\tGameEventSound:EnemyFlagshipDestroyed \"EVENTTECH_ENEMYFLAGSHIPDESTROYED\"\n"
            "\tGameEventSound:PlayerTitanShieldsDown \"EVENTTECH_REBEL_PLAYERTITANSHIELDSDOWN_0\"\n"
            "\tGameEventSound:PlayerTitanHullSeverelyDamaged \"EVENTTECH_REBEL_TITANHULLSERVERDAMAGED_0\"\n"
            "\tGameEventSound:PlayerCapitalShipShieldsDown \"EVENTTECH_PLAYERCAPITALSHIPSHIELDSDOWN\"\n"
            "\tGameEventSound:PlayerCapitalShipHullSeverelyDamaged \"EVENTTECH_PLAYERCAPITALSHIPHULLSEVERELYDAMAGED\"\n"
            "\tGameEventSound:AchievementCompleted \"AchievementCompleted\"\n"
            "\tGameEventSound:QuestAdded \"EVENTTECH_QUESTADDED\"\n"
            "\tGameEventSound:QuestCompleted \"EVENTTECH_QUESTCOMPLETED\"\n"
            "\tGameEventSound:QuestFailed \"EVENTTECH_QUESTFAILED\"\n"
            "\tGameEventSound:QuestEnded \"QuestEnded\"\n"
            "\tGameEventSound:QuestRejected \"QuestRejected\"\n"
            "\tGameEventSound:PlayerQuestAdded \"PlayerQuestAdded\"\n"
            "\tGameEventSound:PlayerQuestCompleted \"PlayerQuestCompleted\"\n"
            "\tGameEventSound:PlayerQuestFailed \"PlayerQuestFailed\"\n"
            "\tGameEventSound:DiplomaticVictoryHalfway \"DiplomaticVictoryHalfway\"\n"
            "\tGameEventSound:DiplomaticVictoryComplete \"DiplomaticVictoryComplete\"\n"
            "\tGameEventSound:AllyDiplomaticVictoryHalfway \"AllyDiplomaticVictoryHalfway\"\n"
            "\tGameEventSound:AllyDiplomaticVictoryComplete \"AllyDiplomaticVictoryComplete\"\n"
            "\tGameEventSound:EnemyDiplomaticVictoryHalfway \"EnemyDiplomaticVictoryHalfway\"\n"
            "\tGameEventSound:EnemyDiplomaticVictoryComplete \"EnemyDiplomaticVictoryComplete\"\n"
            "\tGameEventSound:OccupationVictoryStarted \"OccupationVictoryStarted\"\n"
            "\tGameEventSound:OccupationVictoryHalfway \"OccupationVictoryHalfway\"\n"
            "\tGameEventSound:OccupationVictoryComplete \"OccupationVictoryComplete\"\n"
            "\tGameEventSound:AllyOccupationVictoryStarted \"AllyOccupationVictoryStarted\"\n"
            "\tGameEventSound:AllyOccupationVictoryHalfway \"AllyOccupationVictoryHalfway\"\n"
            "\tGameEventSound:AllyOccupationVictoryComplete \"AllyOccupationVictoryComplete\"\n"
            "\tGameEventSound:EnemyOccupationVictoryStarted \"EnemyOccupationVictoryStarted\"\n"
            "\tGameEventSound:EnemyOccupationVictoryHalfway \"EnemyOccupationVictoryHalfway\"\n"
            "\tGameEventSound:EnemyOccupationVictoryComplete \"EnemyOccupationVictoryComplete\"\n"
            "\tGameEventSound:ResearchVictoryStarted \"ResearchVictoryStarted\"\n"
            "\tGameEventSound:ResearchVictoryHalfway \"ResearchVictoryHalfway\"\n"
            "\tGameEventSound:ResearchVictoryComplete \"ResearchVictoryComplete\"\n"
            "\tGameEventSound:ResearchComplete \"EVENTTECH_RESEARCHCOMPLETE\"\n"
            "\tGameEventSound:AllyResearchVictoryStarted \"AllyResearchVictoryStarted\"\n"
            "\tGameEventSound:AllyResearchVictoryHalfway \"AllyResearchVictoryHalfway\"\n"
            "\tGameEventSound:AllyResearchVictoryComplete \"AllyResearchVictoryComplete\"\n"
            "\tGameEventSound:EnemyResearchVictoryStarted \"EnemyResearchVictoryStarted\"\n"
            "\tGameEventSound:EnemyResearchVictoryHalfway \"EnemyResearchVictoryHalfway\"\n"
            "\tGameEventSound:EnemyResearchVictoryComplete \"EnemyResearchVictoryComplete\"\n"
            "\tGameEventSound:ResearchAlreadyMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ResearchMaxLevelQueued \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ResearchNotEnoughPointsInTier \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ResearchNeedMoreLabModules \"EVENTTECH_RESEARCHNEEDMORELABMODULES\"\n"
            "\tGameEventSound:ResearchNeedPrerequisite \"EVENTTECH_RESEARCHNEEDPREREQUISITE\"\n"
            "\tGameEventSound:MoreModuleSlotsNeeded \"EVENTTECH_MOREMODULESLOTSNEEDED\"\n"
            "\tGameEventSound:SpaceMineLimitReached \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:StarBaseUpgradeNotEnoughUpgradePoints \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:StarBaseUpgradeAlreadyMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:StarBaseUpgradeMaxLevelQueued \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:TitanUpgradeNotEnoughUpgradePoints \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:TitanUpgradeAlreadyMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:InvalidModulePlacementPosition \"EVENTTECH_INVALIDMODULEPLACEMENTPOSITION\"\n"
            "\tGameEventSound:AbilityUpgradeAlreadyMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AbilityUpgradeRequiresHigherLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AbilityUpgradeNoUnspentPoints \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ShipUpgradeAlreadyMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ShipUpgradeAlreadyQueuedMaxLevel \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityCooldownNotExpired \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityDisabledByDebuff \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityNoAntimatter \"EVENTTECH_USEABILITYNOANTIMATTER\"\n"
            "\tGameEventSound:UseAbilityNoUndockedSquadMembers \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityNotEnoughRoomForStarBase \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityCannotUseAbitraryLocationOnEntity \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityTooManyStarBases \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityMineIsNotActivated \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityTargetNotInRange \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityNoValidObjectType \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityPhaseSpaceTarget \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityNormalSpaceTarget \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintCanBeCaptured \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintCanHaveFighters \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintCanHaveShields \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintCanSpreadWeaponDamage \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintCanPhaseDodge \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasAbilityWithCooldown \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasAntimatter \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasPhaseMissiles \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasPopulation \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasEnergyWeapons \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasHullDamage \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasShieldDamage \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasAntiMatterShortage \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintHasWeapons \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsCargoShip \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsExplored \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsResourceExtractor \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsMovingToLocalOrbitBody \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsPsi \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintLinkedCargoShip \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintLinkedSquad \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintNotSelf \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintWithinSolarSystem \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintModuleUnderConstruction \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintModuleIsActivelyBeingConstructed \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintModuleNotActivelyBeingConstructed \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsNotHomeworld \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintNotInvulnerable \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsInFriendlyGravityWell \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetConstraintIsInNonFriendlyGravityWell \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetOwnerLinked \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetOwnerPlayer \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetOwnerFriendly \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetOwnerHostile \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetOwnerNoOwner \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:UseAbilityUnmetOwnerAlliedOrEnemy \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:GameStart \"GameStart\"\n"
            "\tGameEventSound:GameWin \"EVENTTECH_GAMEWIN\"\n"
            "\tGameEventSound:AllyGameWin \"EVENTTECH_ALLIEDGAMEWIN\"\n"
            "\tGameEventSound:CapitalShipLevelUp \"CapitalShipLevelUp\"\n"
            "\tGameEventSound:AttackTargetInvalidWithNoReason \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsNotDamagableDueToBuffs \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsNotEnemy \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsCantFireAtFighters \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsCantMoveAndTargetNotInRange \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsCantMoveToTarget \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsCanOnlyAttackStructures \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsMustBeInSameOrbitWell \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsNoWeaponsToBombPlanet \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:AttackTargetInvalidAsPlanetCantBeBombed \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ScuttleShipStarted \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ScuttleShipStopped \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ScuttlePlanetStarted \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:ScuttlePlanetStopped \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:MarketBoom \"MarketBoom\"\n"
            "\tGameEventSound:MarketCrash \"MarketCrash\"\n"
            "\tGameEventSound:PlayerCultureFlippedEnemyPlanet \"PlayerCultureFlippedEnemyPlanet\"\n"
            "\tGameEventSound:EnvoyTargetPlanetIsNowInvalid \"UI_COMMON_DEFAULTERROR\"\n"
            "\tGameEventSound:OffensiveMissionDiscovered \"OffensiveMissionDiscovered\"\n"
            "\tGameEventSound:MissionDiscovered \"MissionDiscovered\"\n"
        )
        cls.identifier = "gameEventData"
        cls.sounds = [
            {"identifier":"GameEventSound:AllianceCeaseFireBroken",
                "val":"AllianceCeaseFireBroken"},
            {"identifier":"GameEventSound:AllianceCeaseFireFormed",
                "val":"GameEventSound:AllianceCeaseFireFormed"},
            {"identifier":"GameEventSound:AllianceCeaseFireOffered",
                "val":"GameEventSound:AllianceCeaseFireOffered"},
            {"identifier":"GameEventSound:AllianceOtherBroken",
                "val":"GameEventSound:AllianceOtherBroken"},
            {"identifier":"GameEventSound:AllianceOtherFormed",
                "val":"GameEventSound:AllianceOtherFormed"},
            {"identifier":"GameEventSound:AllianceOtherOffered",
                "val":"GameEventSound:AllianceOtherOffered"},
            {"identifier":"GameEventSound:AllyBackUpArrived",
                "val":"GameEventSound:AllyBackUpArrived"},
            {"identifier":"GameEventSound:AllyBackUpEnRoute",
                "val":"GameEventSound:AllyBackUpEnRoute"},
            {"identifier":"GameEventSound:AllyTitanQueued",
                "val":"GameEventSound:AllyTitanQueued"},
            {"identifier":"GameEventSound:AllyTitanCompleted",
                "val":"GameEventSound:AllyTitanCompleted"},
            {"identifier":"GameEventSound:EnemyTitanQueued",
                "val":"GameEventSound:EnemyTitanQueued"},
            {"identifier":"GameEventSound:EnemyTitanCompleted",
                "val":"GameEventSound:EnemyTitanCompleted"},
            {"identifier":"GameEventSound:AllyTitanLost",
                "val":"GameEventSound:AllyTitanLost"},
            {"identifier":"GameEventSound:AllyCapitalShipLost",
                "val":"EVENTTECH_ALLYCAPITALSHIPLOST"},
            {"identifier":"GameEventSound:AllyFleetArrived",
                "val":"EVENTTECH_ALLYFLEETARRIVED"},
            {"identifier":"GameEventSound:AllyPlanetLost",
                "val":"EVENTTECH_ALLYPLANETLOST"},
            {"identifier":"GameEventSound:AllyPlanetUnderAttack",
                "val":"EVENTTECH_ALLYPLANETUNDERATTACK"},
            {"identifier":"GameEventSound:AllyRequestAttackPlanet",
                "val":"EVENTTECH_ALLYREQUESTATTACK"},
            {"identifier":"GameEventSound:AllyRequestDefendPlanet",
                "val":"EVENTTECH_ALLYREQUESTDEFEND"},
            {"identifier":"GameEventSound:AllyUnitUnderAttack",
                "val":"EVENTTECH_ALLYUNITUNDERATTACK"},
            {"identifier":"GameEventSound:BountyAllDepleted",
                "val":"EVENTTECH_BOUNTYALLDEPLETED"},
            {"identifier":"GameEventSound:BountyIncreasedOther",
                "val":"EVENTTECH_BOUNTYINCREASEDOTHER"},
            {"identifier":"GameEventSound:BountyIncreasedPlayer",
                "val":"EVENTTECH_BOUNTYINCREASEDPLAYER"},
            {"identifier":"GameEventSound:BuyNoCommand",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:BuyNoCredits",
                "val":"EVENTTECH_BUYNOCREDITS"},
            {"identifier":"GameEventSound:BuyNoMetal",
                "val":"EVENTTECH_BUYNOMETAL"},
            {"identifier":"GameEventSound:BuyNoCrystal",
                "val":"EVENTTECH_BUYNOCRYSTAL"},
            {"identifier":"GameEventSound:BuyNoFrigateFactory",
                "val":"EVENTTECH_BUYNOFRIGATEFACTORY"},
            {"identifier":"GameEventSound:BuyNoCapitalShipFactory",
                "val":"EVENTTECH_BUYNOCAPITALSHIPFACTORY"},
            {"identifier":"GameEventSound:BuyNoTitanFactory",
                "val":"EVENTTECH_BUYNOTITANFACTORY"},
            {"identifier":"GameEventSound:BuyNoAvailableShipSlots",
                "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
            {"identifier":"GameEventSound:BuyNoAvailableCapitalShipSlots",
                "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
            {"identifier":"GameEventSound:BuyAlreadyHasTitan",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:CannonShellDetected",
                "val":"EVENTTECH_CANNONSHELLDETECTED"},
            {"identifier":"GameEventSound:RandomEventDetected",
                "val":"Effect_RandomEventDetected"},
            {"identifier":"GameEventSound:CannonUnderConstruction",
                "val":"UI_COMMON_CANNONBUILT"},
            {"identifier":"GameEventSound:ColonizePlanetAlreadyOwned",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ColonizePlanetNotColonizable",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ColonizePlanetAlreadyBeingColonized",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ColonizeNeedPrerequisiteResearch",
                "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
            {"identifier":"GameEventSound:ColonizePlanetHasTakeoverCulture",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ColonizePlanetHasDebuff",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ColonizePlanetBlockedByEnemyStarBase",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:RuinPlanetNotRuinable",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:PirateFleetArrived",
                "val":"EVENTTECH_PIRATEFLEETARRIVED"},
            {"identifier":"GameEventSound:PirateFleetArrived",
                "val":"EVENTTECH_PIRATEFLEETARRIVED"},
            {"identifier":"GameEventSound:PirateRaidImminent",
                "val":"EVENTTECH_PIRATERAIDIMMINENT"},
            {"identifier":"GameEventSound:PirateRaidLaunched",
                "val":"EVENTTECH_PIRATERAIDLAUNCHED"},
            {"identifier":"GameEventSound:HostileFleetArrived",
                "val":"EVENTTECH_HOSTILEFLEETARRIVED"},
            {"identifier":"GameEventSound:HostileFleetEnRoute",
                "val":"EVENTTECH_HOSTILEFLEETENROUTE"},
            {"identifier":"GameEventSound:HostilePlanetDestroyed",
                "val":"EVENTTECH_HOSTILEPLANETDESTROYED"},
            {"identifier":"GameEventSound:ModuleComplete",
                "val":"EVENTTECH_MODULECOMPLETE"},
            {"identifier":"GameEventSound:MoveCannotTravelBetweenPlanets",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:NoValidPath",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:NoValidPathObserved",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:NoValidPathBetweenStars",
                "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
            {"identifier":"GameEventSound:NoValidPathDueToBuff",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:Ping","val":"UI_COMMON_PING"},
            {"identifier":"GameEventSound:PlanetUpgradeAlreadyMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:PlanetUpgradeComplete",
                "val":"EVENTTECH_PLANETUPGRADECOMPLETE"},
            {"identifier":"GameEventSound:PlanetUpgradeMaxLevelQueued",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:NewPlanetDiscovered",
                "val":"EVENTTECH_NEWPLANETDISCOVERED"},
            {"identifier":"GameEventSound:ArtifactDiscovered",
                "val":"EVENTTECH_ARTIFACTDISCOVERED"},
            {"identifier":"GameEventSound:ArtifactDiscoveredOther",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:BonusDiscovered",
                "val":"EVENTTECH_BONUSDISCOVERED"},
            {"identifier":"GameEventSound:NothingDiscovered",
                "val":"EVENTTECH_NOTHINGDISCOVERED"},
            {"identifier":"GameEventSound:LaunchSquadsDisabledByDebuff",
                "val":"LaunchSquadsDisabledByDebuff"},
            {"identifier":"GameEventSound:PlayerBackUpArrived",
                "val":"EVENTTECH_PLAYERBACKUPARRIVED"},
            {"identifier":"GameEventSound:PlayerTitanCreated",
                "val":"PlayerTitanCreated"},
            {"identifier":"GameEventSound:PlayerTitanLost",
                "val":"EVENTTECH_REBEL_PLAYERTITANLOST_0"},
            {"identifier":"GameEventSound:PlayerCapitalShipCreated",
                "val":"PlayerCapitalShipCreated"},
            {"identifier":"GameEventSound:PlayerCapitalShipLost",
                "val":"EVENTTECH_PLAYERCAPITALSHIPLOST"},
            {"identifier":"GameEventSound:PlayerStarbaseLost",
                "val":"PlayerStarbaseLost"},
            {"identifier":"GameEventSound:PlayerFleetArrived",
                "val":"EVENTTECH_PLAYERFLEETARRIVED"},
            {"identifier":"GameEventSound:PlayerFleetArrivedToFleetBeacon",
                "val":"PlayerFleetArrivedToFleetBeacon"},
            {"identifier":"GameEventSound:PlayerFleetDeparted",
                "val":"PlayerFleetDeparted"},
            {"identifier":"GameEventSound:PlayerFrigateCreated",
                "val":"PlayerFrigateCreated"},
            {"identifier":"GameEventSound:PlayerPlanetColonized",
                "val":"EVENTTECH_PLAYERPLANETCOLONIZED"},
            {"identifier":"GameEventSound:PlayerCapitalPlanetChanged",
                "val":"EVENTTECH_PLAYERCAPITALPLANETCHANGED"},
            {"identifier":"GameEventSound:PlayerCapitalPlanetLost",
                "val":"EVENTTECH_PLAYERCAPITALPLANETDESTROYED"},
            {"identifier":"GameEventSound:AllyCapitalPlanetLost",
                "val":"EVENTTECH_ALLYCAPITALPLANETDESTROYED"},
            {"identifier":"GameEventSound:EnemyCapitalPlanetLost",
                "val":"EVENTTECH_ENEMYCAPITALPLANETDESTROYED"},
            {"identifier":"GameEventSound:PlayerPlanetLost",
                "val":"EVENTTECH_PLAYERPLANETLOST"},
            {"identifier":"GameEventSound:PlayerPlanetLostToCulture",
                "val":"EVENTTECH_PLAYERPLANETLOST"},
            {"identifier":"GameEventSound:PlayerPlanetSoonLostToCulture",
                "val":"EVENTTECH_PLAYERPLANETSOONLOSTTOCULTURE"},
            {"identifier":"GameEventSound:PlayerPlanetUnderAttack",
                "val":"EVENTTECH_PLAYERPLANETUNDERATTACK"},
            {"identifier":"GameEventSound:PlayerReceivedResourcesFromAlly",
                "val":"EFFECT_COINDROP"},
            {"identifier":"GameEventSound:PlayerUnitUnderAttack",
                "val":"EVENTTECH_PLAYERUNITUNDERATTACK"},
            {"identifier":"GameEventSound:PlayerFlagshipUnderAttack",
                "val":"EVENTTECH_PLAYERFLAGSHIPUNDERATTACK"},
            {"identifier":"GameEventSound:PlayerFlagshipShieldsDown",
                "val":"EVENTTECH_PLAYERFLAGSHIPSHIELDSDOWN"},
            {"identifier":"GameEventSound:PlayerFlagshipHullSeverelyDamaged",
                "val":"EVENTTECH_PLAYERFLAGSHIPHULLSEVERELYDAMAGED"},
            {"identifier":"GameEventSound:PlayerFlagshipDestroyed",
                "val":"EVENTTECH_PLAYERFLAGSHIPDESTROYED"},
            {"identifier":"GameEventSound:AllyFlagshipDestroyed",
                "val":"EVENTTECH_ALLYFLAGSHIPDESTROYED"},
            {"identifier":"GameEventSound:EnemyFlagshipDestroyed",
                "val":"EVENTTECH_ENEMYFLAGSHIPDESTROYED"},
            {"identifier":"GameEventSound:PlayerTitanShieldsDown",
                "val":"EVENTTECH_REBEL_PLAYERTITANSHIELDSDOWN_0"},
            {"identifier":"GameEventSound:PlayerTitanHullSeverelyDamaged",
                "val":"EVENTTECH_REBEL_TITANHULLSERVERDAMAGED_0"},
            {"identifier":"GameEventSound:PlayerCapitalShipShieldsDown",
                "val":"EVENTTECH_PLAYERCAPITALSHIPSHIELDSDOWN"},
            {"identifier":"GameEventSound:PlayerCapitalShipHullSeverelyDamaged",
                "val":"EVENTTECH_PLAYERCAPITALSHIPHULLSEVERELYDAMAGED"},
            {"identifier":"GameEventSound:AchievementCompleted",
                "val":"AchievementCompleted"},
            {"identifier":"GameEventSound:QuestAdded",
                "val":"EVENTTECH_QUESTADDED"},
            {"identifier":"GameEventSound:QuestCompleted",
                "val":"EVENTTECH_QUESTCOMPLETED"},
            {"identifier":"GameEventSound:QuestFailed",
                "val":"EVENTTECH_QUESTFAILED"},
            {"identifier":"GameEventSound:QuestEnded","val":"QuestEnded"},
            {"identifier":"GameEventSound:QuestRejected",
                "val":"QuestRejected"},
            {"identifier":"GameEventSound:PlayerQuestAdded",
                "val":"PlayerQuestAdded"},
            {"identifier":"GameEventSound:PlayerQuestCompleted",
                "val":"PlayerQuestCompleted"},
            {"identifier":"GameEventSound:PlayerQuestFailed",
                "val":"PlayerQuestFailed"},
            {"identifier":"GameEventSound:DiplomaticVictoryHalfway",
                "val":"DiplomaticVictoryHalfway"},
            {"identifier":"GameEventSound:DiplomaticVictoryComplete",
                "val":"DiplomaticVictoryComplete"},
            {"identifier":"GameEventSound:AllyDiplomaticVictoryHalfway",
                "val":"AllyDiplomaticVictoryHalfway"},
            {"identifier":"GameEventSound:AllyDiplomaticVictoryComplete",
                "val":"AllyDiplomaticVictoryComplete"},
            {"identifier":"GameEventSound:EnemyDiplomaticVictoryHalfway",
                "val":"EnemyDiplomaticVictoryHalfway"},
            {"identifier":"GameEventSound:EnemyDiplomaticVictoryComplete",
                "val":"EnemyDiplomaticVictoryComplete"},
            {"identifier":"GameEventSound:OccupationVictoryStarted",
                "val":"OccupationVictoryStarted"},
            {"identifier":"GameEventSound:OccupationVictoryHalfway",
                "val":"OccupationVictoryHalfway"},
            {"identifier":"GameEventSound:OccupationVictoryComplete",
                "val":"OccupationVictoryComplete"},
            {"identifier":"GameEventSound:AllyOccupationVictoryStarted",
                "val":"AllyOccupationVictoryStarted"},
            {"identifier":"GameEventSound:AllyOccupationVictoryHalfway",
                "val":"AllyOccupationVictoryHalfway"},
            {"identifier":"GameEventSound:AllyOccupationVictoryComplete",
                "val":"AllyOccupationVictoryComplete"},
            {"identifier":"GameEventSound:EnemyOccupationVictoryStarted",
                "val":"EnemyOccupationVictoryStarted"},
            {"identifier":"GameEventSound:EnemyOccupationVictoryHalfway",
                "val":"EnemyOccupationVictoryHalfway"},
            {"identifier":"GameEventSound:EnemyOccupationVictoryComplete",
                "val":"EnemyOccupationVictoryComplete"},
            {"identifier":"GameEventSound:ResearchVictoryStarted",
                "val":"ResearchVictoryStarted"},
            {"identifier":"GameEventSound:ResearchVictoryHalfway",
                "val":"ResearchVictoryHalfway"},
            {"identifier":"GameEventSound:ResearchVictoryComplete",
                "val":"ResearchVictoryComplete"},
            {"identifier":"GameEventSound:ResearchComplete",
                "val":"EVENTTECH_RESEARCHCOMPLETE"},
            {"identifier":"GameEventSound:AllyResearchVictoryStarted",
                "val":"AllyResearchVictoryStarted"},
            {"identifier":"GameEventSound:AllyResearchVictoryHalfway",
                "val":"AllyResearchVictoryHalfway"},
            {"identifier":"GameEventSound:AllyResearchVictoryComplete",
                "val":"AllyResearchVictoryComplete"},
            {"identifier":"GameEventSound:EnemyResearchVictoryStarted",
                "val":"EnemyResearchVictoryStarted"},
            {"identifier":"GameEventSound:EnemyResearchVictoryHalfway",
                "val":"EnemyResearchVictoryHalfway"},
            {"identifier":"GameEventSound:EnemyResearchVictoryComplete",
                "val":"EnemyResearchVictoryComplete"},
            {"identifier":"GameEventSound:ResearchAlreadyMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ResearchMaxLevelQueued",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ResearchNotEnoughPointsInTier",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ResearchNeedMoreLabModules",
                "val":"EVENTTECH_RESEARCHNEEDMORELABMODULES"},
            {"identifier":"GameEventSound:ResearchNeedPrerequisite",
                "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
            {"identifier":"GameEventSound:MoreModuleSlotsNeeded",
                "val":"EVENTTECH_MOREMODULESLOTSNEEDED"},
            {"identifier":"GameEventSound:SpaceMineLimitReached",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:StarBaseUpgradeNotEnoughUpgradePoints",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:StarBaseUpgradeAlreadyMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:StarBaseUpgradeMaxLevelQueued",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:TitanUpgradeNotEnoughUpgradePoints",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:TitanUpgradeAlreadyMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:InvalidModulePlacementPosition",
                "val":"EVENTTECH_INVALIDMODULEPLACEMENTPOSITION"},
            {"identifier":"GameEventSound:AbilityUpgradeAlreadyMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:AbilityUpgradeRequiresHigherLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:AbilityUpgradeNoUnspentPoints",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ShipUpgradeAlreadyMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ShipUpgradeAlreadyQueuedMaxLevel",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityCooldownNotExpired",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityDisabledByDebuff",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityNoAntimatter",
                "val":"EVENTTECH_USEABILITYNOANTIMATTER"},
            {"identifier":"GameEventSound:UseAbilityNoUndockedSquadMembers",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityNotEnoughRoomForStarBase",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityCannotUseAbitraryLocationOnEntity",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityTooManyStarBases",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityMineIsNotActivated",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityTargetNotInRange",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityNoValidObjectType",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityPhaseSpaceTarget",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityNormalSpaceTarget",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintCanBeCaptured",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintCanHaveFighters",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintCanHaveShields",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintCanSpreadWeaponDamage",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintCanPhaseDodge",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasAbilityWithCooldown",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasAntimatter",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasPhaseMissiles",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasPopulation",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasEnergyWeapons",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasHullDamage",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasShieldDamage",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintHasAntiMatterShortage",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetConstraintHasWeapons",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetConstraintIsCargoShip",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetConstraintIsExplored",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintIsResourceExtractor",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintIsMovingToLocalOrbitBody",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetConstraintIsPsi",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintLinkedCargoShip",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetConstraintLinkedSquad",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetConstraintNotSelf",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintWithinSolarSystem",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintModuleUnderConstruction",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintModuleIsActivelyBeingConstructed",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintModuleNotActivelyBeingConstructed",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintIsNotHomeworld",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintNotInvulnerable",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintIsInFriendlyGravityWell",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:UseAbilityUnmetConstraintIsInNonFriendlyGravityWell",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetOwnerLinked",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetOwnerPlayer",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetOwnerFriendly",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetOwnerHostile",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetOwnerNoOwner",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:UseAbilityUnmetOwnerAlliedOrEnemy",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:GameStart","val":"GameStart"},
            {"identifier":"GameEventSound:GameWin","val":"EVENTTECH_GAMEWIN"},
            {"identifier":"GameEventSound:AllyGameWin",
                "val":"EVENTTECH_ALLIEDGAMEWIN"},
            {"identifier":"GameEventSound:CapitalShipLevelUp",
                "val":"CapitalShipLevelUp"},
            {"identifier":"GameEventSound:AttackTargetInvalidWithNoReason",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsNotDamagableDueToBuffs",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:AttackTargetInvalidAsNotEnemy",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsCantFireAtFighters",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsCantMoveAndTargetNotInRange",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsCantMoveToTarget",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsCanOnlyAttackStructures",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsMustBeInSameOrbitWell",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsNoWeaponsToBombPlanet",
                "val":"UI_COMMON_DEFAULTERROR"},
            {
                "identifier":"GameEventSound:AttackTargetInvalidAsPlanetCantBeBombed",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ScuttleShipStarted",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ScuttleShipStopped",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ScuttlePlanetStarted",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:ScuttlePlanetStopped",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:MarketBoom","val":"MarketBoom"},
            {"identifier":"GameEventSound:MarketCrash","val":"MarketCrash"},
            {"identifier":"GameEventSound:PlayerCultureFlippedEnemyPlanet",
                "val":"PlayerCultureFlippedEnemyPlanet"},
            {"identifier":"GameEventSound:EnvoyTargetPlanetIsNowInvalid",
                "val":"UI_COMMON_DEFAULTERROR"},
            {"identifier":"GameEventSound:OffensiveMissionDiscovered",
                "val":"OffensiveMissionDiscovered"},
            {"identifier":"GameEventSound:MissionDiscovered",
                "val":"MissionDiscovered"}
        ]

    def setUp(self):
        self.inst = player.GameEventData(
                identifier=self.identifier,
                sounds=self.sounds
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.audio.checkEffect.return_value = []
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_gameEventData.parseString(self.parseString)[0]
        res = player.GameEventData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMarketBoom(self):
        """ Test whether MarketBoom attrib is created correctly
        """
        exp = audio.DialogueRef(identifier="GameEventSound:MarketBoom",
                val="MarketBoom")
        self.assertEqual(self.inst.MarketBoom,exp)

    def testCreationAttribBuyNoCredits(self):
        """ Test whether BuyNoCredits attrib is created correctly
        """
        exp = audio.DialogueRef(identifier="GameEventSound:BuyNoCredits",
                val="EVENTTECH_BUYNOCREDITS")
        self.assertEqual(self.inst.BuyNoCredits,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckGameStart(self):
        """ Test whether GameStart attrib is checked
        """
        res = self.inst.check(self.mmod)
        self.mmod.audio.checkEffect.assert_any_call("GameStart",False)

    def testCheckCannonShellDetected(self):
        """ Test whether CannonShellDetected attrib is checked
        """
        res = self.inst.check(self.mmod)
        self.mmod.audio.checkEffect.assert_any_call(
                "EVENTTECH_CANNONSHELLDETECTED",False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests PlanetElevatorData ########################"""

class PlanetElevatorDataTests(unit.TestCase):
    desc = "Tests PlanetElevatorData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetElevatorData\n"
            +"\televatorMeshName:TaxCenter \"taxcenter\"\n"
            +"\televatorMeshName:CultureCenter \"culturecenter\"\n"
            +"\televatorMeshName:MetalExtractor \"metalextractor\"\n"
            +"\televatorMeshName:CrystalExtractor \"crystalextractor\"\n"
            +"\televatorMeshName:Bunker \"bunker\"\n"
            +"\televatorMeshName:GoodsFactory \"goodsfactory\"\n"
            +"\televatorMeshName:ProductionCenter \"prodcenter\"\n"
        )
        cls.identifier = "planetElevatorData"
        cls.taxCenter = {"identifier":"elevatorMeshName:TaxCenter",
            "val":"taxcenter"}
        cls.cultureCenter = {"identifier":"elevatorMeshName:CultureCenter",
            "val":"culturecenter"}
        cls.metalExtractor = {"identifier":"elevatorMeshName:MetalExtractor",
            "val":"metalextractor"}
        cls.crystalExtractor = {
            "identifier":"elevatorMeshName:CrystalExtractor",
            "val":"crystalextractor"}
        cls.bunker = {"identifier":"elevatorMeshName:Bunker","val":"bunker"}
        cls.goodsFactory = {"identifier":"elevatorMeshName:GoodsFactory",
            "val":"goodsfactory"}
        cls.productionCenter = {
            "identifier":"elevatorMeshName:ProductionCenter",
            "val":"prodcenter"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.PlanetElevatorData(
                identifier=self.identifier,
                taxCenter=self.taxCenter,
                cultureCenter=self.cultureCenter,
                metalExtractor=self.metalExtractor,
                crystalExtractor=self.crystalExtractor,
                bunker=self.bunker,
                goodsFactory=self.goodsFactory,
                productionCenter=self.productionCenter
        )
        patcher = mock.patch.object(self.inst.taxCenter,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockTax = patcher.start()
        patcher = mock.patch.object(self.inst.cultureCenter,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockCulture = patcher.start()
        patcher = mock.patch.object(self.inst.metalExtractor,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockMetal = patcher.start()
        patcher = mock.patch.object(self.inst.crystalExtractor,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockCrystal = patcher.start()
        patcher = mock.patch.object(self.inst.bunker,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockBunker = patcher.start()
        patcher = mock.patch.object(self.inst.goodsFactory,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockGoods = patcher.start()
        patcher = mock.patch.object(self.inst.productionCenter,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockProd = patcher.start()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_planetElevatorData.parseString(self.parseString)[0]
        res = player.PlanetElevatorData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTaxCenter(self):
        """ Test whether taxCenter attrib is created correctly
        """
        exp = meshes.MeshRef(**self.taxCenter)
        self.assertEqual(self.inst.taxCenter,exp)

    def testCreationAttribCultureCenter(self):
        """ Test whether cultureCenter attrib is created correctly
        """
        exp = meshes.MeshRef(**self.cultureCenter)
        self.assertEqual(self.inst.cultureCenter,exp)

    def testCreationAttribMetalExtractor(self):
        """ Test whether metalExtractor attrib is created correctly
        """
        exp = meshes.MeshRef(**self.metalExtractor)
        self.assertEqual(self.inst.metalExtractor,exp)

    def testCreationAttribCrystalExtractor(self):
        """ Test whether crystalExtractor attrib is created correctly
        """
        exp = meshes.MeshRef(**self.crystalExtractor)
        self.assertEqual(self.inst.crystalExtractor,exp)

    def testCreationAttribBunker(self):
        """ Test whether bunker attrib is created correctly
        """
        exp = meshes.MeshRef(**self.bunker)
        self.assertEqual(self.inst.bunker,exp)

    def testCreationAttribGoodsFactory(self):
        """ Test whether goodsFactory attrib is created correctly
        """
        exp = meshes.MeshRef(**self.goodsFactory)
        self.assertEqual(self.inst.goodsFactory,exp)

    def testCreationAttribProductionCenter(self):
        """ Test whether productionCenter attrib is created correctly
        """
        exp = meshes.MeshRef(**self.productionCenter)
        self.assertEqual(self.inst.productionCenter,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckTaxCenter(self):
        """ Test whether problems from taxCenter attrib are returned correctly
        """
        self.mockTax.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTax.assert_called_once_with(self.mmod,False)

    def testCheckCultureCenter(self):
        """ Test whether problems from cultureCenter attrib are returned
        """
        self.mockCulture.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockCulture.assert_called_once_with(self.mmod,False)

    def testCheckMetalExtractor(self):
        """ Test whether problems from metalExtractor attrib are returned
        """
        self.mockMetal.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockMetal.assert_called_once_with(self.mmod,False)

    def testCheckCrystalExtractor(self):
        """ Test whether problems from crystalExtractor attrib are returned
        """
        self.mockCrystal.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockCrystal.assert_called_once_with(self.mmod,False)

    def testCheckBunker(self):
        """ Test whether problems from bunker attrib are returned
        """
        self.mockBunker.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockBunker.assert_called_once_with(self.mmod,False)

    def testCheckGoodsFactory(self):
        """ Test whether problems from goodsFactory attrib are returned
        """
        self.mockGoods.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockGoods.assert_called_once_with(self.mmod,False)

    def testCheckProductionCenter(self):
        """ Test whether problems from productionCenter attrib are returned
        """
        self.mockProd.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockProd.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests MusicThemesData #########################"""

class MusicThemesDataTests(unit.TestCase):
    desc = "Tests MusicThemesData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "musicThemeData\n"
            +"\tstartupMusicThemeCount 1\n"
            +"\tmusicTheme \"mstartup\"\n"
            +"\tneutralMusicThemeCount 1\n"
            +"\tmusicTheme \"mneutral\"\n"
            +"\tscareThemeCount 1\n"
            +"\tmusicTheme \"mscare\"\n"
            +"\tannouncementCount 1\n"
            +"\tmusicTheme \"mannounce\"\n"
        )
        cls.identifier = "musicThemeData"
        cls.startup = {
            "counter":{"identifier":"startupMusicThemeCount","val":1},
            "elements":[
                {"identifier":"musicTheme","val":"mstartup"}
            ]
        }
        cls.neutral = {
            "counter":{"identifier":"neutralMusicThemeCount","val":1},
            "elements":[
                {"identifier":"musicTheme","val":"mneutral"}
            ]
        }
        cls.scare = {
            "counter":{"identifier":"scareThemeCount","val":1},
            "elements":[
                {"identifier":"musicTheme","val":"mscare"}
            ]
        }
        cls.announcement = {
            "counter":{"identifier":"announcementCount","val":1},
            "elements":[
                {"identifier":"musicTheme","val":"mannounce"}
            ]
        }

    def setUp(self):
        self.inst = player.MusicThemesData(
                identifier=self.identifier,
                startup=self.startup,
                neutral=self.neutral,
                scare=self.scare,
                announcement=self.announcement
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.audio.checkMusic.return_value = []

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_musicThemeData.parseString(self.parseString)[0]
        res = player.MusicThemesData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribStartup(self):
        """ Test whether startup attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.MusicRef,**self.startup)
        self.assertEqual(self.inst.startup,exp)

    def testCreationAttribNeutral(self):
        """ Test whether neutral attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.MusicRef,**self.neutral)
        self.assertEqual(self.inst.neutral,exp)

    def testCreationAttribScare(self):
        """ Test whether scare attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.MusicRef,**self.scare)
        self.assertEqual(self.inst.scare,exp)

    def testCreationAttribAnnouncement(self):
        """ Test whether announcement attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.MusicRef,**self.announcement)
        self.assertEqual(self.inst.announcement,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckStartup(self):
        """ Test whether check returns problems in startup attrib
        """
        with mock.patch.object(self.inst.startup,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckNeutral(self):
        """ Test whether check returns problems in neutral attrib
        """
        with mock.patch.object(self.inst.neutral,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckScare(self):
        """ Test whether check returns problems in scare attrib
        """
        with mock.patch.object(self.inst.scare,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckAnnouncement(self):
        """ Test whether check returns problems in announcement attrib
        """
        with mock.patch.object(self.inst.announcement,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests HudSkinData ###########################"""

class HudSkinDataTests(unit.TestCase):
    desc = "Tests HudSkinData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "hudSkinData\n"
            +"\tloadScreenCharacterBrush \"LoadScreenCharacterTech\"\n"
            +"\tfleetIconCount 1\n"
            +"\tfleetIcon \"fleeticon\"\n"
            +"\tBandbox \"BANDBOX\"\n"
            +"\tFogOfWar \"FogOfWar\"\n"
            +"\tActionGridSlotBackdrop \"ActionGridSlotBackdropTech\"\n"
            +"\tBottomWindowBackdrop \"BottomWindowBackdropTech\"\n"
            +"\tBottomWindowOverlay \"BottomWindowOverlayTech\"\n"
            +"\tTopWindowBackdrop \"TopWindowBackdropTech\"\n"
            +"\tTopWindowOverlay \"TopWindowOverlayTech\"\n"
            +"\tInfoCardAttachedToBottomBackdrop \"InfoCardAttachedToBottomBackdropTech\"\n"
            +"\tInfoCardAttachedToScreenEdgeBackdrop \"InfoCardAttachedToScreenEdgeBackdropTech\"\n"
            +"\tTopWindowLeftCapLowRes \"TopWindowLeftCapLowResTech\"\n"
            +"\tTopWindowLeftCapHighRes \"TopWindowLeftCapHighResTech\"\n"
            +"\tTopWindowRightCapLowRes \"TopWindowRightCapLowResTech\"\n"
            +"\tTopWindowRightCapHighRes \"TopWindowRightCapHighResTech\"\n"
            +"\tEmpireWindowBackdrop \"EmpireWindowOptionsWindowBackdropTech\"\n"
            +"\tEmpireWindowHighResBackdrop \"EmpireWindowOptionsWindowHighResBackdropTech\"\n"
            +"\tQuickMarketBackdrop \"QuickMarketBackdropTech\"\n"
            +"\tQuickMarketBackdropNoPirates \"QuickMarketBackdropNoPiratesTech\"\n"
            +"\tUnknownPlanetHUDIcon \"HUDICON_PLANET_UNKNOWN\"\n"
            +"\tUnknownPlanetHUDSmallIcon \"HUDICONSMALL_PLANET_UNKNOWN\"\n"
            +"\tUnknownPlanetInfoCardIcon \"INFOCARDICON_PLANET_UNKNOWN\"\n"
            +"\tUnknownPlanetMainViewIcon \"MAINVIEWICON_PLANET_UNKNOWN\"\n"
            +"\tUnknownPlanetPicture \"PICTURE_PLANET_UNKNOWN\"\n"
            +"\tAutoAttackRangeStateMixed \"ACTIONICON_CAPITALSHIP_ATTACKRANGENONE\"\n"
            +"\tAutoAttackRangeStateAllNone \"ACTIONICON_CAPITALSHIP_ATTACKRANGENONE\"\n"
            +"\tAutoAttackRangeStateAllLocalArea \"ACTIONICON_CAPITALSHIP_ATTACKRANGELOCALAREA\"\n"
            +"\tAutoAttackRangeStateAllGravityWell \"ACTIONICON_CAPITALSHIP_ATTACKRANGEGRAVITYWELL\"\n"
            +"\tCohesionRangeStateMixed \"ACTIONICON_CAPITALSHIP_COHESIONRANGENONE\"\n"
            +"\tCohesionRangeStateAllNone \"ACTIONICON_CAPITALSHIP_COHESIONRANGENONE\"\n"
            +"\tCohesionRangeStateAllNear \"ACTIONICON_CAPITALSHIP_COHESIONRANGENEAR\"\n"
            +"\tCohesionRangeStateAllFar \"ACTIONICON_CAPITALSHIP_COHESIONRANGEFAR\"\n"
            +"\tGroupMoveStateNone \"ACTIONICON_CAPITALSHIP_SINGLEMOVE\"\n"
            +"\tGroupMoveStateSome \"ACTIONICON_CAPITALSHIP_PARTIALGROUPMOVE\"\n"
            +"\tGroupMoveStateAll \"ACTIONICON_CAPITALSHIP_WHOLEGROUPMOVE\"\n"
            +"\tZoomInstantContextNear \"ActionIconCameraZoomInstantWithZoomNearContext\"\n"
            +"\tZoomInstantContextFar \"ActionIconCameraZoomInstantWithZoomFarContext\"\n"
            +"\tGameEventCategoryPing \"GameEventCategoryInfoCardIconPing\"\n"
            +"\tGameEventCategoryProduction \"GameEventCategoryInfoCardIconProduction\"\n"
            +"\tGameEventCategoryAlliance \"GameEventCategoryInfoCardIconAlliance\"\n"
            +"\tGameEventCategoryThreat \"GameEventCategoryInfoCardIconThreat\"\n"
            +"\tHUDIconQuest \"HUDIconQuest\"\n"
            +"\tHUDIconHappiness \"HUDIconHappinessGained\"\n"
            +"\tHUDIconOverlayNotEnoughCredits \"HUDIconOverlayNotEnoughCredits\"\n"
            +"\tHUDIconOverlayNotEnoughMetal \"HUDIconOverlayNotEnoughMetal\"\n"
            +"\tHUDIconOverlayNotEnoughCrystal \"HUDIconOverlayNotEnoughCrystal\"\n"
            +"\tHUDIconOverlayNotResearched \"HUDIconOverlayNotResearched\"\n"
            +"\tHUDIconOverlayPlanetUpgradePathLastStageUpgraded \"HUDIconOverlayPlanetUpgradePathLastStageUpgraded\"\n"
            +"\tHUDIconOverlayPlanetModuleNoAvailableResourceAsteroids \"HUDIconOverlayPlanetModuleNoAvailableResourceAsteroids\"\n"
            +"\tHUDIconOverlayPlanetModuleNoTacticalSlotsAvailable \"HUDIconOverlayPlanetModuleNoTacticalSlotsAvailable\"\n"
            +"\tHUDIconOverlayPlanetModuleNoCivilianSlotsAvailable \"HUDIconOverlayPlanetModuleNoCivilianSlotsAvailable\"\n"
            +"\tHUDIconOverlayPlanetBuildShipNoFrigateFactories \"HUDIconOverlayPlanetBuildShipNoFrigateFactories\"\n"
            +"\tHUDIconOverlayPlanetBuildShipNoCapitalShipFactories \"HUDIconOverlayPlanetBuildShipNoCapitalShipFactories\"\n"
            +"\tHUDIconOverlayPlanetBuildShipNotEnoughShipSlots \"HUDIconOverlayPlanetBuildShipNotEnoughShipSlotsTech\"\n"
            +"\tHUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlots \"HUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlotsTech\"\n"
            +"\tHUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints \"HUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints\"\n"
            +"\tHUDIconOverlayStarBaseUpgradePathLastStageUpgraded \"HUDIconOverlayStarBaseUpgradePathLastStageUpgraded\"\n"
            +"\tHUDIconOverlayInFleet \"HUDIconOverlayInFleet\"\n"
            +"\tAllianceHUDIconTrade \"HUDIconAllianceTypeTrade\"\n"
            +"\tAllianceHUDIconExternalVision \"HUDIconAllianceTypeExternalVision\"\n"
            +"\tAllianceHUDIconInternalVision \"HUDIconAllianceTypeInternalVision\"\n"
            +"\tAllianceHUDIconCeaseFire \"HUDIconAllianceTypeCeaseFire\"\n"
            +"\tAllianceHUDIconPeaceTreaty \"HUDIconAllianceTypePeaceTreaty\"\n"
            +"\tAllianceHUDIconResearchPact \"PlayerScreenOfferResearchPact\"\n"
            +"\tAllianceHUDIconEfficiencyPact \"PlayerScreenOfferEfficiencyPact\"\n"
            +"\tAllianceHUDIconAntimatterPact \"PlayerScreenOfferAntimatterPact\"\n"
            +"\tAllianceHUDIconArmorPact \"PlayerScreenOfferArmorPact\"\n"
            +"\tAllianceHUDIconBeamWeaponPact \"PlayerScreenOfferBeamPact\"\n"
            +"\tAllianceHUDIconCulturePact \"PlayerScreenOfferCulturePact\"\n"
            +"\tAllianceHUDIconMetalCrystlPact \"PlayerScreenOfferMetalCrystalPact\"\n"
            +"\tAllianceHUDIconThrusterPact \"PlayerScreenOfferThrusterPact\"\n"
            +"\tAllianceHUDIconPhaseJumpPact \"PlayerScreenOfferPhaseJumpPact\"\n"
            +"\tAllianceHUDIconPlanetBombingPact \"PlayerScreenOfferPlanetBombingPact\"\n"
            +"\tAllianceHUDIconShieldPact \"PlayerScreenOfferShieldPact\"\n"
            +"\tAllianceHUDIconMarketPact \"PlayerScreenOfferMarketPact\"\n"
            +"\tAllianceHUDIconEnergyWeaponCooldownPact \"PlayerScreenOfferEnergyWeaponCooldownPact\"\n"
            +"\tAllianceHUDIconMassReductionPact \"PlayerScreenOfferMassReductionPact\"\n"
            +"\tAllianceHUDIconSupplyPact \"PlayerScreenOfferSupplyPact\"\n"
            +"\tAllianceHUDIconTacticalSlotsPact \"PlayerScreenOfferTacticalSlotsPact\"\n"
            +"\tAllianceHUDIconWeaponCooldownPact \"PlayerScreenOfferWeaponCooldownPact\"\n"
            +"\tAllianceHUDIconTradeIncomePact \"PlayerScreenOfferTradeIncomePact\"\n"
            +"\tResourceHUDIconCredits \"HUDIconCredits\"\n"
            +"\tResourceHUDIconMetal \"HUDIconMetal\"\n"
            +"\tResourceHUDIconCrystal \"HUDIconCrystal\"\n"
            +"\tResourceInfoCardIconCredits \"InfoCardIconCredits\"\n"
            +"\tResourceInfoCardIconMetal \"InfoCardIconMetal\"\n"
            +"\tResourceInfoCardIconCrystal \"InfoCardIconCrystal\"\n"
            +"\tResearchFieldButtonIconCombat \"ResearchScreenTabButtonCombatTech\"\n"
            +"\tResearchFieldButtonIconDefense \"ResearchScreenTabButtonDefenseTech\"\n"
            +"\tResearchFieldButtonIconNonCombat \"ResearchScreenTabButtonNonCombatTech\"\n"
            +"\tResearchFieldButtonIconDiplomacy \"ResearchScreenTabButtonDiplomacyTech\"\n"
            +"\tResearchFieldButtonIconFleet \"ResearchScreenTabButtonFleetTech\"\n"
            +"\tResearchFieldButtonIconArtifact \"ResearchScreenTabButtonArtifact\"\n"
            +"\tQuestStatusIconBombAnyPlanet \"HUDIconQuestBombAnyPlanet\"\n"
            +"\tQuestStatusIconKillShips \"HUDIconQuestKillShips\"\n"
            +"\tQuestStatusIconKillCapitalShips \"HUDIconQuestKillCapitalShips\"\n"
            +"\tQuestStatusIconKillCivilianStructures \"HUDIconQuestKillCivilianStructures\"\n"
            +"\tQuestStatusIconKillTacticalStructures \"HUDIconQuestKillTacticalStructures\"\n"
            +"\tQuestStatusIconSendEnvoy \"HUDIconQuestSendEnvoy\"\n"
            +"\tOrderInfoCardIconNone \"OrderInfoCardIconNone\"\n"
            +"\tOrderInfoCardIconAttack \"InfoCardIconOrderAttack\"\n"
            +"\tOrderInfoCardIconMove \"InfoCardIconOrderMove\"\n"
            +"\tOrderInfoCardIconAbility \"InfoCardIconOrderAbility\"\n"
            +"\tOrderInfoCardIconRetreat \"InfoCardIconOrderRetreat\"\n"
            +"\tInfoCardIconAutoCast \"InfoCardIconAutoCast\"\n"
            +"\tInfoCardIconCombinedPlanetIncome \"InfoCardIconCombinedPlanetIncome\"\n"
            +"\tInfoCardIconCombinedStarBaseIncome \"InfoCardIconCombinedStarBaseIncome\"\n"
            +"\tInfoCardIconFleetUpkeep \"InfoCardIconFleetUpkeepTech\"\n"
            +"\tInfoCardIconCapitalShip \"INFOCARDICON_CAPITALSHIP_TECHBATTLESHIP\"\n"
            +"\tInfoCardIconFrigate \"INFOCARDICON_FRIGATE_TECHSCOUT\"\n"
            +"\tInfoCardIconTradeShip \"INFOCARDICON_TRADESHIP_TECH\"\n"
            +"\tInfoCardIconRefineryShip \"INFOCARDICON_REFINERYSHIP_TECH\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel0 \"MainViewIconOverlayCapitalShipLevel0\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel1 \"MainViewIconOverlayCapitalShipLevel1\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel2 \"MainViewIconOverlayCapitalShipLevel2\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel3 \"MainViewIconOverlayCapitalShipLevel3\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel4 \"MainViewIconOverlayCapitalShipLevel4\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel5 \"MainViewIconOverlayCapitalShipLevel5\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel6 \"MainViewIconOverlayCapitalShipLevel6\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel7 \"MainViewIconOverlayCapitalShipLevel7\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel8 \"MainViewIconOverlayCapitalShipLevel8\"\n"
            +"\tMainViewIconOverlayCapitalShipLevel9 \"MainViewIconOverlayCapitalShipLevel9\"\n"
            +"\tEmpireWindowFindIconOwnedColonyShips \"EmpireWindowFindIconOwnedColonyShipsTech\"\n"
            +"\tEmpireWindowFindIconOwnedScoutShips \"EmpireWindowFindIconOwnedScoutShipsTech\"\n"
            +"\tEmpireWindowFindIconOwnedCapitalShips \"EmpireWindowFindIconOwnedCapitalShipsTech\"\n"
            +"\tEmpireWindowFindIconOwnedStarbases \"EmpireWindowFindIconOwnedStarbasesTech\"\n"
            +"\tEmpireWindowFindIconOwnedEnvoys \"EmpireWindowFindIconOwnedEnvoysTech\"\n"
            +"\tEmpireWindowFindIconOwnedPlanetsWithResourceAsteroids \"EmpireWindowFindIconOwnedPlanetsWithResourceAsteroidsTech\"\n"
            +"\tEmpireWindowFindIconOwnedPlanetsWithFactories \"EmpireWindowFindIconOwnedPlanetsWithFactoriesTech\"\n"
            +"\tEmpireWindowFindIconOwnedPlanetsWithUpgrades \"EmpireWindowFindIconOwnedPlanetsWithUpgradesTech\"\n"
            +"\tEmpireWindowFindIconOwnedPlanetsWithCivilianSlots \"EmpireWindowFindIconOwnedPlanetsWithCivilianSlotsTech\"\n"
            +"\tEmpireWindowFindIconPlanetsWithArtifacts \"EmpireWindowFindIconPlanetsWithArtifactsTech\"\n"
            +"\tUserActionIconAttack \"ACTIONICON_CAPITALSHIP_ATTACK\"\n"
            +"\tUserActionIconStop \"ACTIONICON_CAPITALSHIP_STOP\"\n"
            +"\tUserActionIconCreateFleet \"ActionIconCreateFleet\"\n"
            +"\tUserActionIconDisbandFleet \"ActionIconDisbandFleet\"\n"
            +"\tUserActionIconRetreat \"ActionIconRetreat\"\n"
            +"\tUserActionIconSetRallyPoint \"ACTIONICON_SETRALLYPOINT\"\n"
            +"\tUserActionIconOpenModulePage0 \"ACTIONICON_PLANET_OPENPLANETMODULEMANAGEMENT_ORBIT0\"\n"
            +"\tUserActionIconOpenModulePage1 \"ACTIONICON_PLANET_OPENPLANETMODULEMANAGEMENT_ORBIT1\"\n"
            +"\tUserActionIconCloseModulePage0 \"ACTIONICON_PLANET_CLOSEPLANETMODULEMANAGEMENT_ORBIT0\"\n"
            +"\tUserActionIconCloseModulePage1 \"ACTIONICON_PLANET_CLOSEPLANETMODULEMANAGEMENT_ORBIT1\"\n"
            +"\tUserActionIconOpenPlanetTitanManagement \"ACTIONICON_PLANET_OPENTITANMANAGEMENT_TECHREBEL\"\n"
            +"\tUserActionIconClosePlanetTitanManagement \"ACTIONICON_PLANET_CLOSECAPITALSHIPMANAGEMENT\"\n"
            +"\tUserActionIconOpenPlanetCapitalShipManagement \"ACTIONICON_PLANET_OPENCAPITALSHIPMANAGEMENT\"\n"
            +"\tUserActionIconClosePlanetCapitalShipManagement \"ACTIONICON_PLANET_CLOSECAPITALSHIPMANAGEMENT\"\n"
            +"\tUserActionIconOpenPlanetFrigateManagement0 \"ACTIONICON_PLANET_OPENFRIGATEMANAGEMENT0\"\n"
            +"\tUserActionIconOpenPlanetFrigateManagement1 \"ACTIONICON_PLANET_OPENFRIGATEMANAGEMENT1\"\n"
            +"\tUserActionIconClosePlanetFrigateManagement0 \"ACTIONICON_PLANET_CLOSEFRIGATEMANAGEMENT0\"\n"
            +"\tUserActionIconClosePlanetFrigateManagement1 \"ACTIONICON_PLANET_CLOSEFRIGATEMANAGEMENT1\"\n"
            +"\tUserActionIconOpenSquadManagement \"ACTIONICON_CAPITALSHIP_OPENSQUADRONMANAGEMENT\"\n"
            +"\tUserActionIconCloseSquadManagement \"ACTIONICON_CAPITALSHIP_CLOSESQUADRONMANAGEMENT\"\n"
            +"\tUserActionIconOpenShipAbilityManagement \"ACTIONICON_CAPITALSHIP_OPENCAPITALABILITYMANAGEMENT\"\n"
            +"\tUserActionIconCloseShipAbilityManagement \"ACTIONICON_CAPITALSHIP_CLOSECAPITALABILITYMANAGEMENT\"\n"
            +"\tUserActionIconOpenShipTacticsManagement \"ACTIONICON_CAPITALSHIP_OPENMANAGETACTICS\"\n"
            +"\tUserActionIconCloseShipTacticsManagement \"ACTIONICON_CAPITALSHIP_CLOSEMANAGETACTICS\"\n"
            +"\tUserActionIconPurchaseCapitalShipLevel \"ACTIONICON_CAPITALSHIP_TOGGLEPURCHASENEXTLEVEL\"\n"
            +"\tUserActionIconOpenPlanetUpgradeManagement \"ActionIconPlanetOpenUpgradeManagement\"\n"
            +"\tUserActionIconClosePlanetUpgradeManagement \"ActionIconPlanetCloseUpgradeManagement\"\n"
            +"\tUserActionIconOpenStarBaseUpgradeManagement \"ACTIONICON_PLANET_OPENSTARBASEMANAGEMENT\"\n"
            +"\tUserActionIconCloseStarBaseUpgradeManagement \"ActionIconStarBaseCloseUpgradeManagement\"\n"
            +"\tUserActionIconUpgradePlanetPopulation \"ActionIconPlanetUpgradePopulation\"\n"
            +"\tUserActionIconUpdatePlanetInfrastructure \"ActionIconPlanetUpgradeInfrastructure\"\n"
            +"\tUserActionIconUpgradePlanetArtifactLevel \"ActionIconPlanetUpgradeArtifactLevel\"\n"
            +"\tUserActionIconUpgradePlanetHome \"ActionIconPlanetUpgradeHome\"\n"
            +"\tUserActionIconUpgradePlanetCivilianModules \"ActionIconPlanetUpgradeCivilianModules\"\n"
            +"\tUserActionIconUpgradePlanetTacticalModules \"ActionIconPlanetUpgradeTacticalModules\"\n"
            +"\tUserActionIconUpgradePlanetSocial \"ActionIconPlanetUpgradeSocial\"\n"
            +"\tUserActionIconUpgradePlanetIndustry \"ActionIconPlanetUpgradeIndustry\"\n"
            +"\tUserActionIconUpgradePlanetSmuggler \"ActionIconPlanetUpgradeSmuggler\"\n"
            +"\tUserActionIconBuildResourceAsteroidExtractor \"ActionIconBuildResourceAsteroidExtractor\"\n"
            +"\tUserActionIconPing \"BottomWindowButtonPing\"\n"
            +"\tUserActionIconPingAllyAttack \"ActionIconPingAllyAttack\"\n"
            +"\tUserActionIconPingAllyDefend \"ActionIconPingAllyDefend\"\n"
            +"\tUserActionIconPingAllyStop \"ActionIconPingAllyStop\"\n"
            +"\tUserActionIconToggleZoomToCursor \"ACTIONICON_TOGGLEZOOMTOCURSOR\"\n"
            +"\tUserActionIconToggleScuttle \"ACTIONICON_TOGGLESCUTTLE\"\n"
            +"\tUserActionIconOpenRenameWindow \"ACTIONICON_OPENRENAMEWINDOW\"\n"
            +"\tUserActionIconChangeAutoAttackRange \"ACTIONICON_CAPITALSHIP_CHANGEAUTOATTACKRANGE\"\n"
            +"\tUserActionIconChangeCohesionRange \"ACTIONICON_CAPITALSHIP_CHANGECOHESIONRANGE\"\n"
            +"\tUserActionIconOpenEscapeMenuScreen \"ActionIconOpenEscapeMenuScreen\"\n"
            +"\tUserActionIconOpenNPCScreen \"ActionIconOpenNPCScreen\"\n"
            +"\tUserActionIconOpenRelationshipsScreen \"ActionIconOpenRelationshipsScreen\"\n"
            +"\tUserActionIconOpenResearchScreen \"ActionIconOpenResearchScreenEnabled\"\n"
            +"\tUserActionIconOpenPlayerScreen \"ActionIconOpenPlayerScreen\"\n"
            +"\tUserActionIconToggleEmpireWindowStacked \"ActionIconToggleEmpireWindowStacked\"\n"
            +"\tUserActionIconToggleIsAutoPlaceModuleActive \"ActionIconToggleIsAutoPlaceModuleActive\"\n"
            +"\tUserActionIconIncreaseGameSpeed \"ActionIconIncreaseGameSpeed\"\n"
            +"\tUserActionIconDecreaseGameSpeed \"ActionIconDecreaseGameSpeed\"\n"
        )
        cls.identifier = "hudSkinData"
        cls.loadScreenCharacters = {"identifier":"loadScreenCharacterBrush",
            "val":"LoadScreenCharacterTech"}
        cls.fleetIcons = {
            "counter":{"identifier":"fleetIconCount","val":1},
            "elements":[
                {"identifier":"fleetIcon","val":"fleeticon"}
            ]
        }
        cls.bandbox = {"identifier":"Bandbox","val":"BANDBOX"}
        cls.fogOfWar = {"identifier":"FogOfWar","val":"FogOfWar"}
        cls.hudIcons = [
            {"identifier":"ActionGridSlotBackdrop",
                "val":"ActionGridSlotBackdropTech"},
            {"identifier":"BottomWindowBackdrop",
                "val":"BottomWindowBackdropTech"},
            {"identifier":"BottomWindowOverlay",
                "val":"BottomWindowOverlayTech"},
            {"identifier":"TopWindowBackdrop","val":"TopWindowBackdropTech"},
            {"identifier":"TopWindowOverlay","val":"TopWindowOverlayTech"},
            {"identifier":"InfoCardAttachedToBottomBackdrop",
                "val":"InfoCardAttachedToBottomBackdropTech"},
            {"identifier":"InfoCardAttachedToScreenEdgeBackdrop",
                "val":"InfoCardAttachedToScreenEdgeBackdropTech"},
            {"identifier":"TopWindowLeftCapLowRes",
                "val":"TopWindowLeftCapLowResTech"},
            {"identifier":"TopWindowLeftCapHighRes",
                "val":"TopWindowLeftCapHighResTech"},
            {"identifier":"TopWindowRightCapLowRes",
                "val":"TopWindowRightCapLowResTech"},
            {"identifier":"TopWindowRightCapHighRes",
                "val":"TopWindowRightCapHighResTech"},
            {"identifier":"EmpireWindowBackdrop",
                "val":"EmpireWindowOptionsWindowBackdropTech"},
            {"identifier":"EmpireWindowHighResBackdrop",
                "val":"EmpireWindowOptionsWindowHighResBackdropTech"},
            {"identifier":"QuickMarketBackdrop",
                "val":"QuickMarketBackdropTech"},
            {"identifier":"QuickMarketBackdropNoPirates",
                "val":"QuickMarketBackdropNoPiratesTech"},
            {"identifier":"UnknownPlanetHUDIcon",
                "val":"HUDICON_PLANET_UNKNOWN"},
            {"identifier":"UnknownPlanetHUDSmallIcon",
                "val":"HUDICONSMALL_PLANET_UNKNOWN"},
            {"identifier":"UnknownPlanetInfoCardIcon",
                "val":"INFOCARDICON_PLANET_UNKNOWN"},
            {"identifier":"UnknownPlanetMainViewIcon",
                "val":"MAINVIEWICON_PLANET_UNKNOWN"},
            {"identifier":"UnknownPlanetPicture",
                "val":"PICTURE_PLANET_UNKNOWN"},
            {"identifier":"AutoAttackRangeStateMixed",
                "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGENONE"},
            {"identifier":"AutoAttackRangeStateAllNone",
                "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGENONE"},
            {"identifier":"AutoAttackRangeStateAllLocalArea",
                "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGELOCALAREA"},
            {"identifier":"AutoAttackRangeStateAllGravityWell",
                "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGEGRAVITYWELL"},
            {"identifier":"CohesionRangeStateMixed",
                "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGENONE"},
            {"identifier":"CohesionRangeStateAllNone",
                "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGENONE"},
            {"identifier":"CohesionRangeStateAllNear",
                "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGENEAR"},
            {"identifier":"CohesionRangeStateAllFar",
                "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGEFAR"},
            {"identifier":"GroupMoveStateNone",
                "val":"ACTIONICON_CAPITALSHIP_SINGLEMOVE"},
            {"identifier":"GroupMoveStateSome",
                "val":"ACTIONICON_CAPITALSHIP_PARTIALGROUPMOVE"},
            {"identifier":"GroupMoveStateAll",
                "val":"ACTIONICON_CAPITALSHIP_WHOLEGROUPMOVE"},
            {"identifier":"ZoomInstantContextNear",
                "val":"ActionIconCameraZoomInstantWithZoomNearContext"},
            {"identifier":"ZoomInstantContextFar",
                "val":"ActionIconCameraZoomInstantWithZoomFarContext"},
            {"identifier":"GameEventCategoryPing",
                "val":"GameEventCategoryInfoCardIconPing"},
            {"identifier":"GameEventCategoryProduction",
                "val":"GameEventCategoryInfoCardIconProduction"},
            {"identifier":"GameEventCategoryAlliance",
                "val":"GameEventCategoryInfoCardIconAlliance"},
            {"identifier":"GameEventCategoryThreat",
                "val":"GameEventCategoryInfoCardIconThreat"},
            {"identifier":"HUDIconQuest","val":"HUDIconQuest"},
            {"identifier":"HUDIconHappiness","val":"HUDIconHappinessGained"},
            {"identifier":"HUDIconOverlayNotEnoughCredits",
                "val":"HUDIconOverlayNotEnoughCredits"},
            {"identifier":"HUDIconOverlayNotEnoughMetal",
                "val":"HUDIconOverlayNotEnoughMetal"},
            {"identifier":"HUDIconOverlayNotEnoughCrystal",
                "val":"HUDIconOverlayNotEnoughCrystal"},
            {"identifier":"HUDIconOverlayNotResearched",
                "val":"HUDIconOverlayNotResearched"},
            {"identifier":"HUDIconOverlayPlanetUpgradePathLastStageUpgraded",
                "val":"HUDIconOverlayPlanetUpgradePathLastStageUpgraded"},
            {
                "identifier":"HUDIconOverlayPlanetModuleNoAvailableResourceAsteroids",
                "val":"HUDIconOverlayPlanetModuleNoAvailableResourceAsteroids"},
            {"identifier":"HUDIconOverlayPlanetModuleNoTacticalSlotsAvailable",
                "val":"HUDIconOverlayPlanetModuleNoTacticalSlotsAvailable"},
            {"identifier":"HUDIconOverlayPlanetModuleNoCivilianSlotsAvailable",
                "val":"HUDIconOverlayPlanetModuleNoCivilianSlotsAvailable"},
            {"identifier":"HUDIconOverlayPlanetBuildShipNoFrigateFactories",
                "val":"HUDIconOverlayPlanetBuildShipNoFrigateFactories"},
            {"identifier":"HUDIconOverlayPlanetBuildShipNoCapitalShipFactories",
                "val":"HUDIconOverlayPlanetBuildShipNoCapitalShipFactories"},
            {"identifier":"HUDIconOverlayPlanetBuildShipNotEnoughShipSlots",
                "val":"HUDIconOverlayPlanetBuildShipNotEnoughShipSlotsTech"},
            {
                "identifier":"HUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlots",
                "val":"HUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlotsTech"},
            {"identifier":"HUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints",
                "val":"HUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints"},
            {"identifier":"HUDIconOverlayStarBaseUpgradePathLastStageUpgraded",
                "val":"HUDIconOverlayStarBaseUpgradePathLastStageUpgraded"},
            {"identifier":"HUDIconOverlayInFleet",
                "val":"HUDIconOverlayInFleet"},
            {"identifier":"AllianceHUDIconTrade",
                "val":"HUDIconAllianceTypeTrade"},
            {"identifier":"AllianceHUDIconExternalVision",
                "val":"HUDIconAllianceTypeExternalVision"},
            {"identifier":"AllianceHUDIconInternalVision",
                "val":"HUDIconAllianceTypeInternalVision"},
            {"identifier":"AllianceHUDIconCeaseFire",
                "val":"HUDIconAllianceTypeCeaseFire"},
            {"identifier":"AllianceHUDIconPeaceTreaty",
                "val":"HUDIconAllianceTypePeaceTreaty"},
            {"identifier":"AllianceHUDIconResearchPact",
                "val":"PlayerScreenOfferResearchPact"},
            {"identifier":"AllianceHUDIconEfficiencyPact",
                "val":"PlayerScreenOfferEfficiencyPact"},
            {"identifier":"AllianceHUDIconAntimatterPact",
                "val":"PlayerScreenOfferAntimatterPact"},
            {"identifier":"AllianceHUDIconArmorPact",
                "val":"PlayerScreenOfferArmorPact"},
            {"identifier":"AllianceHUDIconBeamWeaponPact",
                "val":"PlayerScreenOfferBeamPact"},
            {"identifier":"AllianceHUDIconCulturePact",
                "val":"PlayerScreenOfferCulturePact"},
            {"identifier":"AllianceHUDIconMetalCrystlPact",
                "val":"PlayerScreenOfferMetalCrystalPact"},
            {"identifier":"AllianceHUDIconThrusterPact",
                "val":"PlayerScreenOfferThrusterPact"},
            {"identifier":"AllianceHUDIconPhaseJumpPact",
                "val":"PlayerScreenOfferPhaseJumpPact"},
            {"identifier":"AllianceHUDIconPlanetBombingPact",
                "val":"PlayerScreenOfferPlanetBombingPact"},
            {"identifier":"AllianceHUDIconShieldPact",
                "val":"PlayerScreenOfferShieldPact"},
            {"identifier":"AllianceHUDIconMarketPact",
                "val":"PlayerScreenOfferMarketPact"},
            {"identifier":"AllianceHUDIconEnergyWeaponCooldownPact",
                "val":"PlayerScreenOfferEnergyWeaponCooldownPact"},
            {"identifier":"AllianceHUDIconMassReductionPact",
                "val":"PlayerScreenOfferMassReductionPact"},
            {"identifier":"AllianceHUDIconSupplyPact",
                "val":"PlayerScreenOfferSupplyPact"},
            {"identifier":"AllianceHUDIconTacticalSlotsPact",
                "val":"PlayerScreenOfferTacticalSlotsPact"},
            {"identifier":"AllianceHUDIconWeaponCooldownPact",
                "val":"PlayerScreenOfferWeaponCooldownPact"},
            {"identifier":"AllianceHUDIconTradeIncomePact",
                "val":"PlayerScreenOfferTradeIncomePact"},
            {"identifier":"ResourceHUDIconCredits","val":"HUDIconCredits"},
            {"identifier":"ResourceHUDIconMetal","val":"HUDIconMetal"},
            {"identifier":"ResourceHUDIconCrystal","val":"HUDIconCrystal"},
            {"identifier":"ResourceInfoCardIconCredits",
                "val":"InfoCardIconCredits"},
            {"identifier":"ResourceInfoCardIconMetal",
                "val":"InfoCardIconMetal"},
            {"identifier":"ResourceInfoCardIconCrystal",
                "val":"InfoCardIconCrystal"},
            {"identifier":"ResearchFieldButtonIconCombat",
                "val":"ResearchScreenTabButtonCombatTech"},
            {"identifier":"ResearchFieldButtonIconDefense",
                "val":"ResearchScreenTabButtonDefenseTech"},
            {"identifier":"ResearchFieldButtonIconNonCombat",
                "val":"ResearchScreenTabButtonNonCombatTech"},
            {"identifier":"ResearchFieldButtonIconDiplomacy",
                "val":"ResearchScreenTabButtonDiplomacyTech"},
            {"identifier":"ResearchFieldButtonIconFleet",
                "val":"ResearchScreenTabButtonFleetTech"},
            {"identifier":"ResearchFieldButtonIconArtifact",
                "val":"ResearchScreenTabButtonArtifact"},
            {"identifier":"QuestStatusIconBombAnyPlanet",
                "val":"HUDIconQuestBombAnyPlanet"},
            {"identifier":"QuestStatusIconKillShips",
                "val":"HUDIconQuestKillShips"},
            {"identifier":"QuestStatusIconKillCapitalShips",
                "val":"HUDIconQuestKillCapitalShips"},
            {"identifier":"QuestStatusIconKillCivilianStructures",
                "val":"HUDIconQuestKillCivilianStructures"},
            {"identifier":"QuestStatusIconKillTacticalStructures",
                "val":"HUDIconQuestKillTacticalStructures"},
            {"identifier":"QuestStatusIconSendEnvoy",
                "val":"HUDIconQuestSendEnvoy"},
            {"identifier":"OrderInfoCardIconNone",
                "val":"OrderInfoCardIconNone"},
            {"identifier":"OrderInfoCardIconAttack",
                "val":"InfoCardIconOrderAttack"},
            {"identifier":"OrderInfoCardIconMove",
                "val":"InfoCardIconOrderMove"},
            {"identifier":"OrderInfoCardIconAbility",
                "val":"InfoCardIconOrderAbility"},
            {"identifier":"OrderInfoCardIconRetreat",
                "val":"InfoCardIconOrderRetreat"},
            {"identifier":"InfoCardIconAutoCast","val":"InfoCardIconAutoCast"},
            {"identifier":"InfoCardIconCombinedPlanetIncome",
                "val":"InfoCardIconCombinedPlanetIncome"},
            {"identifier":"InfoCardIconCombinedStarBaseIncome",
                "val":"InfoCardIconCombinedStarBaseIncome"},
            {"identifier":"InfoCardIconFleetUpkeep",
                "val":"InfoCardIconFleetUpkeepTech"},
            {"identifier":"InfoCardIconCapitalShip",
                "val":"INFOCARDICON_CAPITALSHIP_TECHBATTLESHIP"},
            {"identifier":"InfoCardIconFrigate",
                "val":"INFOCARDICON_FRIGATE_TECHSCOUT"},
            {"identifier":"InfoCardIconTradeShip",
                "val":"INFOCARDICON_TRADESHIP_TECH"},
            {"identifier":"InfoCardIconRefineryShip",
                "val":"INFOCARDICON_REFINERYSHIP_TECH"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel0",
                "val":"MainViewIconOverlayCapitalShipLevel0"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel1",
                "val":"MainViewIconOverlayCapitalShipLevel1"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel2",
                "val":"MainViewIconOverlayCapitalShipLevel2"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel3",
                "val":"MainViewIconOverlayCapitalShipLevel3"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel4",
                "val":"MainViewIconOverlayCapitalShipLevel4"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel5",
                "val":"MainViewIconOverlayCapitalShipLevel5"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel6",
                "val":"MainViewIconOverlayCapitalShipLevel6"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel7",
                "val":"MainViewIconOverlayCapitalShipLevel7"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel8",
                "val":"MainViewIconOverlayCapitalShipLevel8"},
            {"identifier":"MainViewIconOverlayCapitalShipLevel9",
                "val":"MainViewIconOverlayCapitalShipLevel9"},
            {"identifier":"EmpireWindowFindIconOwnedColonyShips",
                "val":"EmpireWindowFindIconOwnedColonyShipsTech"},
            {"identifier":"EmpireWindowFindIconOwnedScoutShips",
                "val":"EmpireWindowFindIconOwnedScoutShipsTech"},
            {"identifier":"EmpireWindowFindIconOwnedCapitalShips",
                "val":"EmpireWindowFindIconOwnedCapitalShipsTech"},
            {"identifier":"EmpireWindowFindIconOwnedStarbases",
                "val":"EmpireWindowFindIconOwnedStarbasesTech"},
            {"identifier":"EmpireWindowFindIconOwnedEnvoys",
                "val":"EmpireWindowFindIconOwnedEnvoysTech"},
            {
                "identifier":"EmpireWindowFindIconOwnedPlanetsWithResourceAsteroids",
                "val":"EmpireWindowFindIconOwnedPlanetsWithResourceAsteroidsTech"},
            {"identifier":"EmpireWindowFindIconOwnedPlanetsWithFactories",
                "val":"EmpireWindowFindIconOwnedPlanetsWithFactoriesTech"},
            {"identifier":"EmpireWindowFindIconOwnedPlanetsWithUpgrades",
                "val":"EmpireWindowFindIconOwnedPlanetsWithUpgradesTech"},
            {"identifier":"EmpireWindowFindIconOwnedPlanetsWithCivilianSlots",
                "val":"EmpireWindowFindIconOwnedPlanetsWithCivilianSlotsTech"},
            {"identifier":"EmpireWindowFindIconPlanetsWithArtifacts",
                "val":"EmpireWindowFindIconPlanetsWithArtifactsTech"},
            {"identifier":"UserActionIconAttack",
                "val":"ACTIONICON_CAPITALSHIP_ATTACK"},
            {"identifier":"UserActionIconStop",
                "val":"ACTIONICON_CAPITALSHIP_STOP"},
            {"identifier":"UserActionIconCreateFleet",
                "val":"ActionIconCreateFleet"},
            {"identifier":"UserActionIconDisbandFleet",
                "val":"ActionIconDisbandFleet"},
            {"identifier":"UserActionIconRetreat","val":"ActionIconRetreat"},
            {"identifier":"UserActionIconSetRallyPoint",
                "val":"ACTIONICON_SETRALLYPOINT"},
            {"identifier":"UserActionIconOpenModulePage0",
                "val":"ACTIONICON_PLANET_OPENPLANETMODULEMANAGEMENT_ORBIT0"},
            {"identifier":"UserActionIconOpenModulePage1",
                "val":"ACTIONICON_PLANET_OPENPLANETMODULEMANAGEMENT_ORBIT1"},
            {"identifier":"UserActionIconCloseModulePage0",
                "val":"ACTIONICON_PLANET_CLOSEPLANETMODULEMANAGEMENT_ORBIT0"},
            {"identifier":"UserActionIconCloseModulePage1",
                "val":"ACTIONICON_PLANET_CLOSEPLANETMODULEMANAGEMENT_ORBIT1"},
            {"identifier":"UserActionIconOpenPlanetTitanManagement",
                "val":"ACTIONICON_PLANET_OPENTITANMANAGEMENT_TECHREBEL"},
            {"identifier":"UserActionIconClosePlanetTitanManagement",
                "val":"ACTIONICON_PLANET_CLOSECAPITALSHIPMANAGEMENT"},
            {"identifier":"UserActionIconOpenPlanetCapitalShipManagement",
                "val":"ACTIONICON_PLANET_OPENCAPITALSHIPMANAGEMENT"},
            {"identifier":"UserActionIconClosePlanetCapitalShipManagement",
                "val":"ACTIONICON_PLANET_CLOSECAPITALSHIPMANAGEMENT"},
            {"identifier":"UserActionIconOpenPlanetFrigateManagement0",
                "val":"ACTIONICON_PLANET_OPENFRIGATEMANAGEMENT0"},
            {"identifier":"UserActionIconOpenPlanetFrigateManagement1",
                "val":"ACTIONICON_PLANET_OPENFRIGATEMANAGEMENT1"},
            {"identifier":"UserActionIconClosePlanetFrigateManagement0",
                "val":"ACTIONICON_PLANET_CLOSEFRIGATEMANAGEMENT0"},
            {"identifier":"UserActionIconClosePlanetFrigateManagement1",
                "val":"ACTIONICON_PLANET_CLOSEFRIGATEMANAGEMENT1"},
            {"identifier":"UserActionIconOpenSquadManagement",
                "val":"ACTIONICON_CAPITALSHIP_OPENSQUADRONMANAGEMENT"},
            {"identifier":"UserActionIconCloseSquadManagement",
                "val":"ACTIONICON_CAPITALSHIP_CLOSESQUADRONMANAGEMENT"},
            {"identifier":"UserActionIconOpenShipAbilityManagement",
                "val":"ACTIONICON_CAPITALSHIP_OPENCAPITALABILITYMANAGEMENT"},
            {"identifier":"UserActionIconCloseShipAbilityManagement",
                "val":"ACTIONICON_CAPITALSHIP_CLOSECAPITALABILITYMANAGEMENT"},
            {"identifier":"UserActionIconOpenShipTacticsManagement",
                "val":"ACTIONICON_CAPITALSHIP_OPENMANAGETACTICS"},
            {"identifier":"UserActionIconCloseShipTacticsManagement",
                "val":"ACTIONICON_CAPITALSHIP_CLOSEMANAGETACTICS"},
            {"identifier":"UserActionIconPurchaseCapitalShipLevel",
                "val":"ACTIONICON_CAPITALSHIP_TOGGLEPURCHASENEXTLEVEL"},
            {"identifier":"UserActionIconOpenPlanetUpgradeManagement",
                "val":"ActionIconPlanetOpenUpgradeManagement"},
            {"identifier":"UserActionIconClosePlanetUpgradeManagement",
                "val":"ActionIconPlanetCloseUpgradeManagement"},
            {"identifier":"UserActionIconOpenStarBaseUpgradeManagement",
                "val":"ACTIONICON_PLANET_OPENSTARBASEMANAGEMENT"},
            {"identifier":"UserActionIconCloseStarBaseUpgradeManagement",
                "val":"ActionIconStarBaseCloseUpgradeManagement"},
            {"identifier":"UserActionIconUpgradePlanetPopulation",
                "val":"ActionIconPlanetUpgradePopulation"},
            {"identifier":"UserActionIconUpdatePlanetInfrastructure",
                "val":"ActionIconPlanetUpgradeInfrastructure"},
            {"identifier":"UserActionIconUpgradePlanetArtifactLevel",
                "val":"ActionIconPlanetUpgradeArtifactLevel"},
            {"identifier":"UserActionIconUpgradePlanetHome",
                "val":"ActionIconPlanetUpgradeHome"},
            {"identifier":"UserActionIconUpgradePlanetCivilianModules",
                "val":"ActionIconPlanetUpgradeCivilianModules"},
            {"identifier":"UserActionIconUpgradePlanetTacticalModules",
                "val":"ActionIconPlanetUpgradeTacticalModules"},
            {"identifier":"UserActionIconUpgradePlanetSocial",
                "val":"ActionIconPlanetUpgradeSocial"},
            {"identifier":"UserActionIconUpgradePlanetIndustry",
                "val":"ActionIconPlanetUpgradeIndustry"},
            {"identifier":"UserActionIconUpgradePlanetSmuggler",
                "val":"ActionIconPlanetUpgradeSmuggler"},
            {"identifier":"UserActionIconBuildResourceAsteroidExtractor",
                "val":"ActionIconBuildResourceAsteroidExtractor"},
            {"identifier":"UserActionIconPing","val":"BottomWindowButtonPing"},
            {"identifier":"UserActionIconPingAllyAttack",
                "val":"ActionIconPingAllyAttack"},
            {"identifier":"UserActionIconPingAllyDefend",
                "val":"ActionIconPingAllyDefend"},
            {"identifier":"UserActionIconPingAllyStop",
                "val":"ActionIconPingAllyStop"},
            {"identifier":"UserActionIconToggleZoomToCursor",
                "val":"ACTIONICON_TOGGLEZOOMTOCURSOR"},
            {"identifier":"UserActionIconToggleScuttle",
                "val":"ACTIONICON_TOGGLESCUTTLE"},
            {"identifier":"UserActionIconOpenRenameWindow",
                "val":"ACTIONICON_OPENRENAMEWINDOW"},
            {"identifier":"UserActionIconChangeAutoAttackRange",
                "val":"ACTIONICON_CAPITALSHIP_CHANGEAUTOATTACKRANGE"},
            {"identifier":"UserActionIconChangeCohesionRange",
                "val":"ACTIONICON_CAPITALSHIP_CHANGECOHESIONRANGE"},
            {"identifier":"UserActionIconOpenEscapeMenuScreen",
                "val":"ActionIconOpenEscapeMenuScreen"},
            {"identifier":"UserActionIconOpenNPCScreen",
                "val":"ActionIconOpenNPCScreen"},
            {"identifier":"UserActionIconOpenRelationshipsScreen",
                "val":"ActionIconOpenRelationshipsScreen"},
            {"identifier":"UserActionIconOpenResearchScreen",
                "val":"ActionIconOpenResearchScreenEnabled"},
            {"identifier":"UserActionIconOpenPlayerScreen",
                "val":"ActionIconOpenPlayerScreen"},
            {"identifier":"UserActionIconToggleEmpireWindowStacked",
                "val":"ActionIconToggleEmpireWindowStacked"},
            {"identifier":"UserActionIconToggleIsAutoPlaceModuleActive",
                "val":"ActionIconToggleIsAutoPlaceModuleActive"},
            {"identifier":"UserActionIconIncreaseGameSpeed",
                "val":"ActionIconIncreaseGameSpeed"},
            {"identifier":"UserActionIconDecreaseGameSpeed",
                "val":"ActionIconDecreaseGameSpeed"},
        ]

    def setUp(self):
        self.inst = player.HudSkinData(
                identifier=self.identifier,
                loadScreenCharacters=self.loadScreenCharacters,
                fleetIcons=self.fleetIcons,
                bandbox=self.bandbox,
                fogOfWar=self.fogOfWar,
                hudIcons=self.hudIcons
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.brushes.checkBrush.return_value = []
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_hudSkinData.parseString(self.parseString)[0]
        res = player.HudSkinData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribLoadScreenCharacters(self):
        """ Test whether loadScreenCharacters attrib is created correctly
        """
        exp = ui.BrushRef(**self.loadScreenCharacters)
        self.assertEqual(self.inst.loadScreenCharacters,exp)

    def testCreationAttribFleetIcons(self):
        """ Test whether fleetIcons attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=ui.BrushRef,**self.fleetIcons)
        self.assertEqual(self.inst.fleetIcons,exp)

    def testCreationAttribBandbox(self):
        """ Test whether bandbox attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["BANDBOX"],**self.bandbox)
        self.assertEqual(self.inst.bandbox,exp)

    def testCreationAttribFogOfWar(self):
        """ Test whether fogOfWar attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["FogOfWar"],**self.fogOfWar)
        self.assertEqual(self.inst.fogOfWar,exp)

    def testCreationAttribUserActionIconOpenNPCScreen(self):
        """ Test whether UserActionIconOpenNPCScreen attrib is created correctly
        """
        exp = ui.BrushRef(identifier="UserActionIconOpenNPCScreen",
                val="ActionIconOpenNPCScreen")
        self.assertEqual(self.inst.UserActionIconOpenNPCScreen,exp)

    def testCreationAttribUnknownPlanetPicture(self):
        """ Test whether UnknownPlanetPicture attrib is created correctly
        """
        exp = ui.BrushRef(identifier="UnknownPlanetPicture",
                val="PICTURE_PLANET_UNKNOWN")
        self.assertEqual(self.inst.UnknownPlanetPicture,exp)

    def testUnknownAttrib(self):
        """ Test whether invalid attrib raises error
        """
        with self.assertRaises(AttributeError):
            self.inst.invalidAttrib

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckGameEventCategoryPing(self):
        """ Test whether GameEventCategoryPing attrib is checked
        """
        self.inst.check(self.mmod)
        self.mmod.brushes.checkBrush.assert_any_call(
                "GameEventCategoryInfoCardIconPing")

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests ResearchBlock ##########################"""

class ResearchBlockTests(unit.TestCase):
    desc = "Tests ResearchBlock:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "block\n"
            +"\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\tbackdrop \"backdrop\"\n"
            +"\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\ttitle \"title\"\n"
            +"\tpicture \"picture\"\n"
        )
        cls.identifier = "block"
        cls.area = {"identifier":"area","coord":[50,69,690,192]}
        cls.backdrop = {"identifier":"backdrop","val":"backdrop"}
        cls.indicatorBackdrop = {"identifier":"tierIndicatorBackdrop",
            "val":"indicatorBackdrop"}
        cls.title = {"identifier":"title","val":"title"}
        cls.picture = {"identifier":"picture","val":"picture"}
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = player.ResearchBlock(
                identifier=self.identifier,
                area=self.area,
                backdrop=self.backdrop,
                indicatorBackdrop=self.indicatorBackdrop,
                title=self.title,
                picture=self.picture
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_researchBlock.parseString(self.parseString)[0]
        res = player.ResearchBlock(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribArea(self):
        """ Test whether area attribute is created correctly
        """
        exp = co_attribs.AttribPos4d(**self.area)
        self.assertEqual(self.inst.area,exp)

    def testCreationAttribBackdrop(self):
        """ Test whether backdrop attribute is created correctly
        """
        exp = ui.BrushRef(**self.backdrop)
        self.assertEqual(self.inst.backdrop,exp)

    def testCreationAttribIndicatorBackdrop(self):
        """ Test whether indicatorBackdrop attribute is created correctly
        """
        exp = ui.BrushRef(**self.indicatorBackdrop)
        self.assertEqual(self.inst.indicatorBackdrop,exp)

    def testCreationAttribTitle(self):
        """ Test whether title attribute is created correctly
        """
        exp = ui.StringReference(**self.title)
        self.assertEqual(self.inst.title,exp)

    def testCreationAttribPicture(self):
        """ Test whether picture attribute is created correctly
        """
        exp = ui.BrushRef(**self.picture)
        self.assertEqual(self.inst.picture,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckBackdrop(self):
        """ Test whether check returns problems from backdrop attrib
        """
        with mock.patch.object(self.inst.backdrop,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckIndicatorBackdrop(self):
        """ Test whether check returns problems from indicatorBackdrop attrib
        """
        with mock.patch.object(self.inst.indicatorBackdrop,"check",
                autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckTitle(self):
        """ Test whether check returns problems from title attrib
        """
        with mock.patch.object(self.inst.title,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckPicture(self):
        """ Test whether check returns problems from picture attrib
        """
        with mock.patch.object(self.inst.picture,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests ResearchData ###########################"""

class ResearchDataTests(unit.TestCase):
    desc = "Tests ResearchData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchScreenData\n"
            +"\tCombat\n"
            +"\t\tblockCount 1\n"
            +"\t\tblock\n"
            +"\t\t\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\t\t\tbackdrop \"backdrop\"\n"
            +"\t\t\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\t\t\ttitle \"title\"\n"
            +"\t\t\tpicture \"picture\"\n"
            +"\tDefense\n"
            +"\t\tblockCount 1\n"
            +"\t\tblock\n"
            +"\t\t\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\t\t\tbackdrop \"backdrop\"\n"
            +"\t\t\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\t\t\ttitle \"title\"\n"
            +"\t\t\tpicture \"picture\"\n"
            +"\tNonCombat\n"
            +"\t\tblockCount 1\n"
            +"\t\tblock\n"
            +"\t\t\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\t\t\tbackdrop \"backdrop\"\n"
            +"\t\t\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\t\t\ttitle \"title\"\n"
            +"\t\t\tpicture \"picture\"\n"
            +"\tDiplomacy\n"
            +"\t\tblockCount 1\n"
            +"\t\tblock\n"
            +"\t\t\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\t\t\tbackdrop \"backdrop\"\n"
            +"\t\t\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\t\t\ttitle \"title\"\n"
            +"\t\t\tpicture \"picture\"\n"
            +"\tArtifact\n"
            +"\t\tblockCount 1\n"
            +"\t\tblock\n"
            +"\t\t\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\t\t\tbackdrop \"backdrop\"\n"
            +"\t\t\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\t\t\ttitle \"title\"\n"
            +"\t\t\tpicture \"picture\"\n"
            +"\tFleet\n"
            +"\t\tblockCount 1\n"
            +"\t\tblock\n"
            +"\t\t\tarea [ 50 , 69 , 690 , 192 ]\n"
            +"\t\t\tbackdrop \"backdrop\"\n"
            +"\t\t\ttierIndicatorBackdrop \"indicatorBackdrop\"\n"
            +"\t\t\ttitle \"title\"\n"
            +"\t\t\tpicture \"picture\"\n"
            +"\tcapitalShipSlotLevelPicture-0 \"capslot0\"\n"
            +"\tcapitalShipSlotLevelPicture-1 \"capslot1\"\n"
            +"\tcapitalShipSlotLevelPicture-2 \"capslot2\"\n"
            +"\tcapitalShipSlotLevelPicture-3 \"capslot3\"\n"
            +"\tcapitalShipSlotLevelPicture-4 \"capslot4\"\n"
            +"\tcapitalShipSlotLevelPicture-5 \"capslot5\"\n"
            +"\tcapitalShipSlotLevelPicture-6 \"capslot6\"\n"
            +"\tcapitalShipSlotLevelPicture-7 \"capslot7\"\n"
            +"\tcapitalShipSlotLevelPicture-8 \"capslot8\"\n"
            +"\tshipSlotLevelPicture-0 \"slot0\"\n"
            +"\tshipSlotLevelPicture-1 \"slot1\"\n"
            +"\tshipSlotLevelPicture-2 \"slot2\"\n"
            +"\tshipSlotLevelPicture-3 \"slot3\"\n"
            +"\tshipSlotLevelPicture-4 \"slot4\"\n"
            +"\tshipSlotLevelPicture-5 \"slot5\"\n"
            +"\tshipSlotLevelPicture-6 \"slot6\"\n"
            +"\tshipSlotLevelPicture-7 \"slot7\"\n"
            +"\tshipSlotLevelPicture-8 \"slot8\"\n"
        )
        cls.identifier = "researchScreenData"
        cls.combat = {
            "identifier":"Combat",
            "counter":{"identifier":"blockCount","val":1},
            "elements":[
                {
                    "identifier":"block",
                    "area":{"identifier":"area","coord":[50,69,690,192]},
                    "backdrop":{"identifier":"backdrop","val":"backdrop"},
                    "indicatorBackdrop":{"identifier":"tierIndicatorBackdrop",
                        "val":"indicatorBackdrop"},
                    "title":{"identifier":"title","val":"title"},
                    "picture":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.defense = {
            "identifier":"Defense",
            "counter":{"identifier":"blockCount","val":1},
            "elements":[
                {
                    "identifier":"block",
                    "area":{"identifier":"area","coord":[50,69,690,192]},
                    "backdrop":{"identifier":"backdrop","val":"backdrop"},
                    "indicatorBackdrop":{"identifier":"tierIndicatorBackdrop",
                        "val":"indicatorBackdrop"},
                    "title":{"identifier":"title","val":"title"},
                    "picture":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.nonCombat = {
            "identifier":"NonCombat",
            "counter":{"identifier":"blockCount","val":1},
            "elements":[
                {
                    "identifier":"block",
                    "area":{"identifier":"area","coord":[50,69,690,192]},
                    "backdrop":{"identifier":"backdrop","val":"backdrop"},
                    "indicatorBackdrop":{"identifier":"tierIndicatorBackdrop",
                        "val":"indicatorBackdrop"},
                    "title":{"identifier":"title","val":"title"},
                    "picture":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.diplomacy = {
            "identifier":"Diplomacy",
            "counter":{"identifier":"blockCount","val":1},
            "elements":[
                {
                    "identifier":"block",
                    "area":{"identifier":"area","coord":[50,69,690,192]},
                    "backdrop":{"identifier":"backdrop","val":"backdrop"},
                    "indicatorBackdrop":{"identifier":"tierIndicatorBackdrop",
                        "val":"indicatorBackdrop"},
                    "title":{"identifier":"title","val":"title"},
                    "picture":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.artifact = {
            "identifier":"Artifact",
            "counter":{"identifier":"blockCount","val":1},
            "elements":[
                {
                    "identifier":"block",
                    "area":{"identifier":"area","coord":[50,69,690,192]},
                    "backdrop":{"identifier":"backdrop","val":"backdrop"},
                    "indicatorBackdrop":{"identifier":"tierIndicatorBackdrop",
                        "val":"indicatorBackdrop"},
                    "title":{"identifier":"title","val":"title"},
                    "picture":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.fleet = {
            "identifier":"Fleet",
            "counter":{"identifier":"blockCount","val":1},
            "elements":[
                {
                    "identifier":"block",
                    "area":{"identifier":"area","coord":[50,69,690,192]},
                    "backdrop":{"identifier":"backdrop","val":"backdrop"},
                    "indicatorBackdrop":{"identifier":"tierIndicatorBackdrop",
                        "val":"indicatorBackdrop"},
                    "title":{"identifier":"title","val":"title"},
                    "picture":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.capShipSlotIcons = [
            {"identifier":"capitalShipSlotLevelPicture-0","val":"capslot0"},
            {"identifier":"capitalShipSlotLevelPicture-1","val":"capslot1"},
            {"identifier":"capitalShipSlotLevelPicture-2","val":"capslot2"},
            {"identifier":"capitalShipSlotLevelPicture-3","val":"capslot3"},
            {"identifier":"capitalShipSlotLevelPicture-4","val":"capslot4"},
            {"identifier":"capitalShipSlotLevelPicture-5","val":"capslot5"},
            {"identifier":"capitalShipSlotLevelPicture-6","val":"capslot6"},
            {"identifier":"capitalShipSlotLevelPicture-7","val":"capslot7"},
            {"identifier":"capitalShipSlotLevelPicture-8","val":"capslot8"}
        ]
        cls.shipSlotIcons = [
            {"identifier":"shipSlotLevelPicture-0","val":"slot0"},
            {"identifier":"shipSlotLevelPicture-1","val":"slot1"},
            {"identifier":"shipSlotLevelPicture-2","val":"slot2"},
            {"identifier":"shipSlotLevelPicture-3","val":"slot3"},
            {"identifier":"shipSlotLevelPicture-4","val":"slot4"},
            {"identifier":"shipSlotLevelPicture-5","val":"slot5"},
            {"identifier":"shipSlotLevelPicture-6","val":"slot6"},
            {"identifier":"shipSlotLevelPicture-7","val":"slot7"},
            {"identifier":"shipSlotLevelPicture-8","val":"slot8"}
        ]
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []

    def setUp(self):
        self.inst = player.ResearchData(
                identifier=self.identifier,
                combat=self.combat,
                defense=self.defense,
                nonCombat=self.nonCombat,
                diplomacy=self.diplomacy,
                artifact=self.artifact,
                fleet=self.fleet,
                capShipSlotIcons=self.capShipSlotIcons,
                shipSlotIcons=self.shipSlotIcons
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_researchData.parseString(self.parseString)[0]
        res = player.ResearchData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCombat(self):
        """ Test whether combat attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.ResearchBlock,**self.combat)
        self.assertEqual(self.inst.combat,exp)

    def testCreationAttribDefense(self):
        """ Test whether defense attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.ResearchBlock,**self.defense)
        self.assertEqual(self.inst.defense,exp)

    def testCreationAttribNonCombat(self):
        """ Test whether nonCombat attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.ResearchBlock,**self.nonCombat)
        self.assertEqual(self.inst.nonCombat,exp)

    def testCreationAttribDiplomacy(self):
        """ Test whether diplomacy attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.ResearchBlock,**self.diplomacy)
        self.assertEqual(self.inst.diplomacy,exp)

    def testCreationAttribArtifact(self):
        """ Test whether artifact attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.ResearchBlock,**self.artifact)
        self.assertEqual(self.inst.artifact,exp)

    def testCreationAttribFleet(self):
        """ Test whether fleet attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.ResearchBlock,**self.fleet)
        self.assertEqual(self.inst.fleet,exp)

    def testCreationAttribCapShipSlotIcons(self):
        """ Test whether capShipSlotIcons attrib is created correctly
        """
        exp = (
            ui.BrushRef(**self.capShipSlotIcons[0]),
            ui.BrushRef(**self.capShipSlotIcons[1]),
            ui.BrushRef(**self.capShipSlotIcons[2]),
            ui.BrushRef(**self.capShipSlotIcons[3]),
            ui.BrushRef(**self.capShipSlotIcons[4]),
            ui.BrushRef(**self.capShipSlotIcons[5]),
            ui.BrushRef(**self.capShipSlotIcons[6]),
            ui.BrushRef(**self.capShipSlotIcons[7]),
            ui.BrushRef(**self.capShipSlotIcons[8])
        )
        self.assertEqual(self.inst.capShipSlotIcons,exp)

    def testCreationAttribShipSlotIcons(self):
        """ Test whether shipSlotIcons attrib is created correctly
        """
        exp = (
            ui.BrushRef(**self.shipSlotIcons[0]),
            ui.BrushRef(**self.shipSlotIcons[1]),
            ui.BrushRef(**self.shipSlotIcons[2]),
            ui.BrushRef(**self.shipSlotIcons[3]),
            ui.BrushRef(**self.shipSlotIcons[4]),
            ui.BrushRef(**self.shipSlotIcons[5]),
            ui.BrushRef(**self.shipSlotIcons[6]),
            ui.BrushRef(**self.shipSlotIcons[7]),
            ui.BrushRef(**self.shipSlotIcons[8])
        )
        self.assertEqual(self.inst.shipSlotIcons,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckCombat(self):
        """ Test whether check returns problems from combat attrib
        """
        with mock.patch.object(self.inst.combat,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckDefense(self):
        """ Test whether check returns problems from defense attrib
        """
        with mock.patch.object(self.inst.defense,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckNonCombat(self):
        """ Test whether check returns problems from nonCombat attrib
        """
        with mock.patch.object(self.inst.nonCombat,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckDiplomacy(self):
        """ Test whether check returns problems from diplomacy attrib
        """
        with mock.patch.object(self.inst.diplomacy,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckArtifact(self):
        """ Test whether check returns problems from artifact attrib
        """
        with mock.patch.object(self.inst.artifact,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckFleet(self):
        """ Test whether check returns problems from fleet attrib
        """
        with mock.patch.object(self.inst.fleet,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckCapShipSlotIcons(self):
        """ Test whether capShipSlotIcons are checked
        """
        res = self.inst.check(self.mmod)
        self.mmod.brushes.checkBrush.assert_any_call("capslot3")

    def testCheckShipSlotIcons(self):
        """ Test whether shipSlotIcons are checked
        """
        self.inst.check(self.mmod)
        self.mmod.brushes.checkBrush.assert_any_call("slot3")

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests FactionRelationMod ########################"""

class FactionRelationModTests(unit.TestCase):
    desc = "Tests FactionRelationMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "factionRelationsModifier\n"
            +"\tfactionNameID \"faction\"\n"
            +"\totherFactionNameID \"otherfaction\"\n"
            +"\tmodifierMin 0.250000\n"
            +"\tmodifierMax 0.500000\n"
        )
        cls.identifier = "factionRelationsModifier"
        cls.factionName = {"identifier":"factionNameID","val":"faction"}
        cls.otherFactionName = {"identifier":"otherFactionNameID",
            "val":"otherfaction"}
        cls.modMin = {"identifier":"modifierMin","val":0.25}
        cls.modMax = {"identifier":"modifierMax","val":0.5}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.FactionRelationMod(
                identifier=self.identifier,
                factionName=self.factionName,
                otherFactionName=self.otherFactionName,
                modMin=self.modMin,
                modMax=self.modMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_factionRelationMod.parseString(self.parseString)[0]
        res = player.FactionRelationMod(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribFactionName(self):
        """ Test whether factionName attrib is created correctly
        """
        exp = ui.StringReference(**self.factionName)
        self.assertEqual(self.inst.factionName,exp)

    def testCreationAttribOtherFactionName(self):
        """ Test whether otherFactionName attrib is created correctly
        """
        exp = ui.StringReference(**self.otherFactionName)
        self.assertEqual(self.inst.otherFactionName,exp)

    def testCreationAttribModMin(self):
        """ Test whether modMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.modMin)
        self.assertEqual(self.inst.modMin,exp)

    def testCreationAttribModMax(self):
        """ Test whether modMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.modMax)
        self.assertEqual(self.inst.modMax,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckFactionName(self):
        """ Test whether check returns problems from factionName attrib
        """
        with mock.patch.object(self.inst.factionName,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckOtherFactionName(self):
        """ Test whether check returns problems from otherFactionName attrib
        """
        with mock.patch.object(self.inst.otherFactionName,"check",
                autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests RaceRelationMod ##########################"""

class RaceRelationModTests(unit.TestCase):
    desc = "Tests RaceRelationMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "raceRelationsModifier\n"
            +"\traceNameID \"race\"\n"
            +"\tmodifierMin 0.100000\n"
            +"\tmodifierMax 1.000000\n"
            +"\tfactionRelationsModCount 1\n"
            +"\tfactionRelationsModifier\n"
            +"\t\tfactionNameID \"faction\"\n"
            +"\t\totherFactionNameID \"otherfaction\"\n"
            +"\t\tmodifierMin 0.250000\n"
            +"\t\tmodifierMax 0.500000\n"
        )
        cls.identifier = "raceRelationsModifier"
        cls.raceName = {"identifier":"raceNameID","val":"race"}
        cls.modMin = {"identifier":"modifierMin","val":0.1}
        cls.modMax = {"identifier":"modifierMax","val":1.0}
        cls.factionRelations = {
            "counter":{"identifier":"factionRelationsModCount","val":1},
            "elements":[
                {
                    "identifier":"factionRelationsModifier",
                    "factionName":{"identifier":"factionNameID",
                        "val":"faction"},
                    "otherFactionName":{"identifier":"otherFactionNameID",
                        "val":"otherfaction"},
                    "modMin":{"identifier":"modifierMin","val":0.25},
                    "modMax":{"identifier":"modifierMax","val":0.5}
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = player.RaceRelationMod(
                identifier=self.identifier,
                raceName=self.raceName,
                modMin=self.modMin,
                modMax=self.modMax,
                factionRelations=self.factionRelations
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_raceRelationMod.parseString(self.parseString)[0]
        res = player.RaceRelationMod(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRaceName(self):
        """ Test whether raceName attribute is created correctly
        """
        exp = ui.StringReference(**self.raceName)
        self.assertEqual(self.inst.raceName,exp)

    def testCreationAttribModMin(self):
        """ Test whether modMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.modMin)
        self.assertEqual(self.inst.modMin,exp)

    def testCreationAttribModMax(self):
        """ Test whether modMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.modMax)
        self.assertEqual(self.inst.modMax,exp)

    def testCreationAttribFactionRelations(self):
        """ Test whether factionRelations attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.FactionRelationMod,
                **self.factionRelations)
        self.assertEqual(self.inst.factionRelations,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckRaceName(self):
        """ Test whether check returns problems from raceName attrib
        """
        with mock.patch.object(self.inst.raceName,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckFactionRelations(self):
        """ Test whether check returns problems from factionRelations attrib
        """
        with mock.patch.object(self.inst.factionRelations,"check",
                autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests GameOverData ###########################"""

class GameOverDataTests(unit.TestCase):
    desc = "Tests GameOverData:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "gameOverWindowData\n"
            +"\thasWonPicture \"wonpic\"\n"
            +"\thasLostPicture \"lostpic\"\n"
            +"\tthemeBackdropBrush \"backdrop\"\n"
            +"\thasWonMilitaryStatusFormatStringId \"fmtwonmilitary\"\n"
            +"\thasLostMilitaryStatusFormatStringId \"fmtlostmilitary\"\n"
            +"\thasWonFlagshipStatusFormatStringId \"fmtwonflagship\"\n"
            +"\thasLostFlagshipStatusFormatStringId \"fmtlostflagship\"\n"
            +"\thasWonHomeworldStatusFormatStringId \"fmtwonhomeworld\"\n"
            +"\thasLostHomeworldStatusFormatStringId \"fmtlosthomeworld\"\n"
            +"\thasWonDiplomaticStatusFormatStringId \"fmtwondiplomatic\"\n"
            +"\thasLostDiplomaticStatusFormatStringId \"fmtlostdiplomatic\"\n"
            +"\thasWonResearchStatusFormatStringId \"fmtwonresearch\"\n"
            +"\thasLostResearchStatusFormatStringId \"fmtlostresearch\"\n"
            +"\thasWonOccupationStatusFormatStringId \"fmtwonoccupation\"\n"
            +"\thasLostOccupationStatusFormatStringId \"fmtlostoccupation\"\n"
            +"\thasWonMilitaryPhraseStringId \"phrasewonmilitary\"\n"
            +"\thasLostMilitaryPhraseStringId \"phraselostmilitary\"\n"
            +"\thasWonFlagshipPhraseStringId \"phrasewonflagship\"\n"
            +"\thasLostFlagshipPhraseStringId \"phraselostflagship\"\n"
            +"\thasWonHomeworldPhraseStringId \"phrasewonhomeworld\"\n"
            +"\thasLostHomeworldPhraseStringId \"phraselosthomeworld\"\n"
            +"\thasWonDiplomaticPhraseStringId \"phrasewondiplomatic\"\n"
            +"\thasLostDiplomaticPhraseStringId \"phraselostdiplomatic\"\n"
            +"\thasWonResearchPhraseStringId \"phrasewonrsearch\"\n"
            +"\thasLostResearchPhraseStringId \"phraselostresearch\"\n"
            +"\thasWonOccupationPhraseStringId \"phrasewonoccupation\"\n"
            +"\thasLostOccupationPhraseStringId \"phraselostoccupation\"\n"
        )
        cls.identifier = "gameOverWindowData"
        cls.picHasWon = {"identifier":"hasWonPicture","val":"wonpic"}
        cls.picHasLost = {"identifier":"hasLostPicture","val":"lostpic"}
        cls.themeBackdrop = {"identifier":"themeBackdropBrush",
            "val":"backdrop"}
        cls.formatStrings = [
            {"identifier":"hasWonMilitaryStatusFormatStringId",
                "val":"fmtwonmilitary"},
            {"identifier":"hasLostMilitaryStatusFormatStringId",
                "val":"fmtlostmilitary"},
            {"identifier":"hasWonFlagshipStatusFormatStringId",
                "val":"fmtwonflagship"},
            {"identifier":"hasLostFlagshipStatusFormatStringId",
                "val":"fmtlostflagship"},
            {"identifier":"hasWonHomeworldStatusFormatStringId",
                "val":"fmtwonhomeworld"},
            {"identifier":"hasLostHomeworldStatusFormatStringId",
                "val":"fmtlosthomeworld"},
            {"identifier":"hasWonDiplomaticStatusFormatStringId",
                "val":"fmtwondiplomatic"},
            {"identifier":"hasLostDiplomaticStatusFormatStringId",
                "val":"fmtlostdiplomatic"},
            {"identifier":"hasWonResearchStatusFormatStringId",
                "val":"fmtwonresearch"},
            {"identifier":"hasLostResearchStatusFormatStringId",
                "val":"fmtlostresearch"},
            {"identifier":"hasWonOccupationStatusFormatStringId",
                "val":"fmtwonoccupation"},
            {"identifier":"hasLostOccupationStatusFormatStringId",
                "val":"fmtlostoccupation"}
        ]
        cls.phraseStrings = [
            {"identifier":"hasWonMilitaryPhraseStringId",
                "val":"phrasewonmilitary"},
            {"identifier":"hasLostMilitaryPhraseStringId",
                "val":"phraselostmilitary"},
            {"identifier":"hasWonFlagshipPhraseStringId",
                "val":"phrasewonflagship"},
            {"identifier":"hasLostFlagshipPhraseStringId",
                "val":"phraselostflagship"},
            {"identifier":"hasWonHomeworldPhraseStringId",
                "val":"phrasewonhomeworld"},
            {"identifier":"hasLostHomeworldPhraseStringId",
                "val":"phraselosthomeworld"},
            {"identifier":"hasWonDiplomaticPhraseStringId",
                "val":"phrasewondiplomatic"},
            {"identifier":"hasLostDiplomaticPhraseStringId",
                "val":"phraselostdiplomatic"},
            {"identifier":"hasWonResearchPhraseStringId",
                "val":"phrasewonrsearch"},
            {"identifier":"hasLostResearchPhraseStringId",
                "val":"phraselostresearch"},
            {"identifier":"hasWonOccupationPhraseStringId",
                "val":"phrasewonoccupation"},
            {"identifier":"hasLostOccupationPhraseStringId",
                "val":"phraselostoccupation"}
        ]
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = player.GameOverData(
                identifier=self.identifier,
                picHasWon=self.picHasWon,
                picHasLost=self.picHasLost,
                themeBackdrop=self.themeBackdrop,
                formatStrings=self.formatStrings,
                phraseStrings=self.phraseStrings
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = player.g_gameOverData.parseString(self.parseString)[0]
        res = player.GameOverData(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPicHasWon(self):
        """ Test whether picHasWon attribute is created correctly
        """
        exp = ui.BrushRef(**self.picHasWon)
        self.assertEqual(self.inst.picHasWon,exp)

    def testCreationAttribPicHasLost(self):
        """ Test whether picHasLost attribute is created correctly
        """
        exp = ui.BrushRef(**self.picHasLost)
        self.assertEqual(self.inst.picHasLost,exp)

    def testCreationAttribThemeBackdrop(self):
        """ Test whether themeBackdrop attribute is created correctly
        """
        exp = ui.BrushRef(**self.themeBackdrop)
        self.assertEqual(self.inst.themeBackdrop,exp)

    def testCreationAttribFormatString(self):
        """ Test whether format strings attributes are created correctly
        """
        exp = ui.StringReference(**self.formatStrings[3])
        self.assertEqual(self.inst.hasLostFlagshipStatusFormatStringId,exp)

    def testCreationAttribPhraseString(self):
        """ Test whether phrase strings attributes are created correctly
        """
        exp = ui.StringReference(**self.phraseStrings[3])
        self.assertEqual(self.inst.hasLostFlagshipPhraseStringId,exp)

    def testUnknownString(self):
        """ Test whether unknown format string attrib raises error
        """
        with self.assertRaises(AttributeError):
            self.inst.Unknown

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckPicHasWon(self):
        """ Test whether check returns problems from picHasWon attrib
        """
        with mock.patch.object(self.inst.picHasWon,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckPicHasLost(self):
        """ Test whether check returns problems from picHasLost attrib
        """
        with mock.patch.object(self.inst.picHasLost,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckThemeBackdrop(self):
        """ Test whether check returns problems from themeBackdrop attrib
        """
        with mock.patch.object(self.inst.themeBackdrop,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckFormatStrings(self):
        """ Test whether check returns problems from formatStrings attrib
        """
        with mock.patch.object(self.inst.formatStrings[
            "hasLostFlagshipStatusFormatStringId"],"check",
                autospec=True,spec_set=True,
                return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckPhraseStrings(self):
        """ Test whether check returns problems from phraseStrings attrib
        """
        with mock.patch.object(self.inst.phraseStrings[
            "hasLostFlagshipPhraseStringId"],"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests Player Entity ##########################"""

class PlayerFixtures(unit.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.filepath = "Player.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Player"}
        cls.raceName = {"identifier":"raceNameStringID","val":"racename"}
        cls.canColonize = {"identifier":"canColonize","val":True}
        cls.isPsi = {"identifier":"isPsi","val":False}
        cls.selectable = {"identifier":"isSelectable","val":True}
        cls.selectPriority = {"identifier":"selectablePriorty","val":2}
        cls.randCapNamePrefix = {"identifier":"randomCapitalShipNamePrefix",
            "val":"capprefix"}
        cls.randStarbaseNamePrefix = {"identifier":"randomStarBaseNamePrefix",
            "val":"randstarbaseprefix"}
        cls.buildables = {
            "identifier":"entities",
            "planetModules":{
                "identifier":"planetModuleInfo",
                "page0":{
                    "identifier":"Page:0",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"mod1"}
                    ]
                },
                "page1":{
                    "identifier":"Page:1",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"mod2"}
                    ]
                }
            },
            "titans":{
                "identifier":"titanInfo",
                "page0":{
                    "identifier":"Page:0",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"testtitan"}
                    ]
                }
            },
            "capitals":{
                "identifier":"capitalShipInfo",
                "page0":{
                    "identifier":"Page:0",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"capital"}
                    ]
                }
            },
            "frigates":{
                "identifier":"frigateInfo",
                "page0":{
                    "identifier":"Page:0",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"frig1"}
                    ]
                },
                "page1":{
                    "identifier":"Page:1",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"frig2"}
                    ]
                },
                "noPage":{
                    "identifier":"NotOnPage",
                    "counter":{"identifier":"count","val":1},
                    "elements":[
                        {"identifier":"entityDefName","val":"frig3"}
                    ]
                }
            },
            "research":{
                "identifier":"researchInfo",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"research"}
                ]
            },
            "starbases":{
                "identifier":"starBaseInfo",
                "counter":{"identifier":"count","val":1},
                "elements":[
                    {"identifier":"entityDefName","val":"starbase"}
                ]
            },
            "flagship":{"identifier":"flagship","val":"flagship"}
        }
        cls.debrisData = {
            "identifier":"debrisData",
            "meshesLarge":{
                "counter":{"identifier":"numRandomMeshNamesLarge","val":1},
                "elements":[
                    {"identifier":"randomMeshName","val":"debris1"}
                ]
            },
            "meshesSmall":{
                "counter":{"identifier":"numRandomMeshNamesSmall","val":1},
                "elements":[
                    {"identifier":"randomMeshName","val":"debris2"}
                ]
            }
        }
        cls.hyperspaceData = {
            "identifier":"hyperspaceData",
            "frigates":{
                "chargeUp":{"identifier":"frigateChargeUpEffectName",
                    "val":"frigchargeup"},
                "chargeUpBetweenStars":{
                    "identifier":"frigateChargeUpEffectNameBetweenStars",
                    "val":"frigchargeupstars"},
                "chargeUpDestabilized":{
                    "identifier":"frigateChargeUpEffectNameDestabilized",
                    "val":"frigchargeupdestabilize"},
                "chargeUpSound":{
                    "identifier":"frigateChargeUpEffectSoundID",
                    "val":"frigchargeupsound"
                },
                "travel":{"identifier":"frigateTravelEffectName",
                    "val":"frigtravel"},
                "travelBetweenStars":{
                    "identifier":"frigateTravelEffectNameBetweenStars",
                    "val":"frigtravelstars"},
                "travelDestabilized":{
                    "identifier":"frigateTravelEffectNameDestabilized",
                    "val":"frigtraveldestabilized"},
                "travelSound":{"identifier":"frigateTravelEffectSoundID",
                    "val":"frigtravelsound"},
                "exit":{"identifier":"frigateExitEffectName","val":"frigexit"},
                "exitSound":{"identifier":"frigateExitEffectSoundID",
                    "val":"frigexitsound"}
            },
            "capitals":{
                "chargeUp":{"identifier":"capitalShipChargeUpEffectName",
                    "val":"capchargeup"},
                "chargeUpBetweenStars":{
                    "identifier":"capitalShipChargeUpEffectNameBetweenStars",
                    "val":"capchargeupstars"},
                "chargeUpDestabilized":{
                    "identifier":"capitalShipChargeUpEffectNameDestabilized",
                    "val":"capchargeupdestabilize"},
                "chargeUpSound":{
                    "identifier":"capitalShipChargeUpEffectSoundID",
                    "val":"capchargeupsound"
                },
                "travel":{"identifier":"capitalShipTravelEffectName",
                    "val":"captravel"},
                "travelBetweenStars":{
                    "identifier":"capitalShipTravelEffectNameBetweenStars",
                    "val":"captravelstars"},
                "travelDestabilized":{
                    "identifier":"capitalShipTravelEffectNameDestabilized",
                    "val":"captraveldestabilize"},
                "travelSound":{"identifier":"capitalShipTravelEffectSoundID",
                    "val":"captravelsound"},
                "exit":{"identifier":"capitalShipExitEffectName",
                    "val":"capexit"},
                "exitSound":{"identifier":"capitalShipExitEffectSoundID",
                    "val":"capexitsound"}
            },
            "titans":{
                "chargeUp":{"identifier":"titanChargeUpEffectName",
                    "val":"titanchargeup"},
                "chargeUpBetweenStars":{
                    "identifier":"titanChargeUpEffectNameBetweenStars",
                    "val":"titanchargeupstars"},
                "chargeUpDestabilized":{
                    "identifier":"titanChargeUpEffectNameDestabilized",
                    "val":"titanchargeupdestabilize"},
                "chargeUpSound":{
                    "identifier":"titanChargeUpEffectSoundID",
                    "val":"titanchargeupsound"
                },
                "travel":{"identifier":"titanTravelEffectName",
                    "val":"titantravel"},
                "travelBetweenStars":{
                    "identifier":"titanTravelEffectNameBetweenStars",
                    "val":"titantravelstars"},
                "travelDestabilized":{
                    "identifier":"titanTravelEffectNameDestabilized",
                    "val":"titantraveldestabilize"},
                "travelSound":{"identifier":"titanTravelEffectSoundID",
                    "val":"titantravelsound"},
                "exit":{"identifier":"titanExitEffectName","val":"titanexit"},
                "exitSound":{"identifier":"titanExitEffectSoundID",
                    "val":"titanexitsound"}
            },
            "baseHyperSpeed":{"identifier":"baseHyperspaceSpeed",
                "val":29500.0},
            "speedBetweenStarsMult":{
                "identifier":"hyperspaceSpeedBetweenStarsMultiplier",
                "val":24.0},
            "speedFriendlyGravWellMult":{
                "identifier":"hyperspaceSpeedToFriendlyOrbitBodiesMultiplier",
                "val":1.0},
            "baseHyperspaceAMCost":{"identifier":"baseHyperspaceAntiMatterCost",
                "val":100.0},
            "baseHyperChargeupTime":{"identifier":"baseHyperspaceChargeUpTime",
                "val":7.0}
        }
        cls.shieldData = {
            "identifier":"shieldData",
            "absorbPerDmg":{"identifier":"shieldAbsorbGrowthPerDamage",
                "val":0.001},
            "absorbDecay":{"identifier":"shieldAbsorbDecayRate","val":0.0125},
            "absorbMin":{"identifier":"shieldAbsorbBaseMin","val":0.15},
            "color":{"identifier":"shieldColor","val":"ffffc705"}
        }
        cls.gameEventData = {
            "identifier":"gameEventData",
            "sounds":[
                {"identifier":"GameEventSound:AllianceCeaseFireBroken",
                    "val":"AllianceCeaseFireBroken"},
                {"identifier":"GameEventSound:AllianceCeaseFireFormed",
                    "val":"GameEventSound:AllianceCeaseFireFormed"},
                {"identifier":"GameEventSound:AllianceCeaseFireOffered",
                    "val":"GameEventSound:AllianceCeaseFireOffered"},
                {"identifier":"GameEventSound:AllianceOtherBroken",
                    "val":"GameEventSound:AllianceOtherBroken"},
                {"identifier":"GameEventSound:AllianceOtherFormed",
                    "val":"GameEventSound:AllianceOtherFormed"},
                {"identifier":"GameEventSound:AllianceOtherOffered",
                    "val":"GameEventSound:AllianceOtherOffered"},
                {"identifier":"GameEventSound:AllyBackUpArrived",
                    "val":"GameEventSound:AllyBackUpArrived"},
                {"identifier":"GameEventSound:AllyBackUpEnRoute",
                    "val":"GameEventSound:AllyBackUpEnRoute"},
                {"identifier":"GameEventSound:AllyTitanQueued",
                    "val":"GameEventSound:AllyTitanQueued"},
                {"identifier":"GameEventSound:AllyTitanCompleted",
                    "val":"GameEventSound:AllyTitanCompleted"},
                {"identifier":"GameEventSound:EnemyTitanQueued",
                    "val":"GameEventSound:EnemyTitanQueued"},
                {"identifier":"GameEventSound:EnemyTitanCompleted",
                    "val":"GameEventSound:EnemyTitanCompleted"},
                {"identifier":"GameEventSound:AllyTitanLost",
                    "val":"GameEventSound:AllyTitanLost"},
                {"identifier":"GameEventSound:AllyCapitalShipLost",
                    "val":"EVENTTECH_ALLYCAPITALSHIPLOST"},
                {"identifier":"GameEventSound:AllyFleetArrived",
                    "val":"EVENTTECH_ALLYFLEETARRIVED"},
                {"identifier":"GameEventSound:AllyPlanetLost",
                    "val":"EVENTTECH_ALLYPLANETLOST"},
                {"identifier":"GameEventSound:AllyPlanetUnderAttack",
                    "val":"EVENTTECH_ALLYPLANETUNDERATTACK"},
                {"identifier":"GameEventSound:AllyRequestAttackPlanet",
                    "val":"EVENTTECH_ALLYREQUESTATTACK"},
                {"identifier":"GameEventSound:AllyRequestDefendPlanet",
                    "val":"EVENTTECH_ALLYREQUESTDEFEND"},
                {"identifier":"GameEventSound:AllyUnitUnderAttack",
                    "val":"EVENTTECH_ALLYUNITUNDERATTACK"},
                {"identifier":"GameEventSound:BountyAllDepleted",
                    "val":"EVENTTECH_BOUNTYALLDEPLETED"},
                {"identifier":"GameEventSound:BountyIncreasedOther",
                    "val":"EVENTTECH_BOUNTYINCREASEDOTHER"},
                {"identifier":"GameEventSound:BountyIncreasedPlayer",
                    "val":"EVENTTECH_BOUNTYINCREASEDPLAYER"},
                {"identifier":"GameEventSound:BuyNoCommand",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:BuyNoCredits",
                    "val":"EVENTTECH_BUYNOCREDITS"},
                {"identifier":"GameEventSound:BuyNoMetal",
                    "val":"EVENTTECH_BUYNOMETAL"},
                {"identifier":"GameEventSound:BuyNoCrystal",
                    "val":"EVENTTECH_BUYNOCRYSTAL"},
                {"identifier":"GameEventSound:BuyNoFrigateFactory",
                    "val":"EVENTTECH_BUYNOFRIGATEFACTORY"},
                {"identifier":"GameEventSound:BuyNoCapitalShipFactory",
                    "val":"EVENTTECH_BUYNOCAPITALSHIPFACTORY"},
                {"identifier":"GameEventSound:BuyNoTitanFactory",
                    "val":"EVENTTECH_BUYNOTITANFACTORY"},
                {"identifier":"GameEventSound:BuyNoAvailableShipSlots",
                    "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
                {"identifier":"GameEventSound:BuyNoAvailableCapitalShipSlots",
                    "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
                {"identifier":"GameEventSound:BuyAlreadyHasTitan",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:CannonShellDetected",
                    "val":"EVENTTECH_CANNONSHELLDETECTED"},
                {"identifier":"GameEventSound:RandomEventDetected",
                    "val":"Effect_RandomEventDetected"},
                {"identifier":"GameEventSound:CannonUnderConstruction",
                    "val":"UI_COMMON_CANNONBUILT"},
                {"identifier":"GameEventSound:ColonizePlanetAlreadyOwned",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ColonizePlanetNotColonizable",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:ColonizePlanetAlreadyBeingColonized",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ColonizeNeedPrerequisiteResearch",
                    "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
                {"identifier":"GameEventSound:ColonizePlanetHasTakeoverCulture",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ColonizePlanetHasDebuff",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:ColonizePlanetBlockedByEnemyStarBase",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:RuinPlanetNotRuinable",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:PirateFleetArrived",
                    "val":"EVENTTECH_PIRATEFLEETARRIVED"},
                {"identifier":"GameEventSound:PirateRaidImminent",
                    "val":"EVENTTECH_PIRATERAIDIMMINENT"},
                {"identifier":"GameEventSound:PirateRaidLaunched",
                    "val":"EVENTTECH_PIRATERAIDLAUNCHED"},
                {"identifier":"GameEventSound:HostileFleetArrived",
                    "val":"EVENTTECH_HOSTILEFLEETARRIVED"},
                {"identifier":"GameEventSound:HostileFleetEnRoute",
                    "val":"EVENTTECH_HOSTILEFLEETENROUTE"},
                {"identifier":"GameEventSound:HostilePlanetDestroyed",
                    "val":"EVENTTECH_HOSTILEPLANETDESTROYED"},
                {"identifier":"GameEventSound:ModuleComplete",
                    "val":"EVENTTECH_MODULECOMPLETE"},
                {"identifier":"GameEventSound:MoveCannotTravelBetweenPlanets",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:NoValidPath",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:NoValidPathObserved",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:NoValidPathBetweenStars",
                    "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
                {"identifier":"GameEventSound:NoValidPathDueToBuff",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:Ping","val":"UI_COMMON_PING"},
                {"identifier":"GameEventSound:PlanetUpgradeAlreadyMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:PlanetUpgradeComplete",
                    "val":"EVENTTECH_PLANETUPGRADECOMPLETE"},
                {"identifier":"GameEventSound:PlanetUpgradeMaxLevelQueued",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:NewPlanetDiscovered",
                    "val":"EVENTTECH_NEWPLANETDISCOVERED"},
                {"identifier":"GameEventSound:ArtifactDiscovered",
                    "val":"EVENTTECH_ARTIFACTDISCOVERED"},
                {"identifier":"GameEventSound:ArtifactDiscoveredOther",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:BonusDiscovered",
                    "val":"EVENTTECH_BONUSDISCOVERED"},
                {"identifier":"GameEventSound:NothingDiscovered",
                    "val":"EVENTTECH_NOTHINGDISCOVERED"},
                {"identifier":"GameEventSound:LaunchSquadsDisabledByDebuff",
                    "val":"LaunchSquadsDisabledByDebuff"},
                {"identifier":"GameEventSound:PlayerBackUpArrived",
                    "val":"EVENTTECH_PLAYERBACKUPARRIVED"},
                {"identifier":"GameEventSound:PlayerTitanCreated",
                    "val":"PlayerTitanCreated"},
                {"identifier":"GameEventSound:PlayerTitanLost",
                    "val":"EVENTTECH_REBEL_PLAYERTITANLOST_0"},
                {"identifier":"GameEventSound:PlayerCapitalShipCreated",
                    "val":"PlayerCapitalShipCreated"},
                {"identifier":"GameEventSound:PlayerCapitalShipLost",
                    "val":"EVENTTECH_PLAYERCAPITALSHIPLOST"},
                {"identifier":"GameEventSound:PlayerStarbaseLost",
                    "val":"PlayerStarbaseLost"},
                {"identifier":"GameEventSound:PlayerFleetArrived",
                    "val":"EVENTTECH_PLAYERFLEETARRIVED"},
                {"identifier":"GameEventSound:PlayerFleetArrivedToFleetBeacon",
                    "val":"PlayerFleetArrivedToFleetBeacon"},
                {"identifier":"GameEventSound:PlayerFleetDeparted",
                    "val":"PlayerFleetDeparted"},
                {"identifier":"GameEventSound:PlayerFrigateCreated",
                    "val":"PlayerFrigateCreated"},
                {"identifier":"GameEventSound:PlayerPlanetColonized",
                    "val":"EVENTTECH_PLAYERPLANETCOLONIZED"},
                {"identifier":"GameEventSound:PlayerCapitalPlanetChanged",
                    "val":"EVENTTECH_PLAYERCAPITALPLANETCHANGED"},
                {"identifier":"GameEventSound:PlayerCapitalPlanetLost",
                    "val":"EVENTTECH_PLAYERCAPITALPLANETDESTROYED"},
                {"identifier":"GameEventSound:AllyCapitalPlanetLost",
                    "val":"EVENTTECH_ALLYCAPITALPLANETDESTROYED"},
                {"identifier":"GameEventSound:EnemyCapitalPlanetLost",
                    "val":"EVENTTECH_ENEMYCAPITALPLANETDESTROYED"},
                {"identifier":"GameEventSound:PlayerPlanetLost",
                    "val":"EVENTTECH_PLAYERPLANETLOST"},
                {"identifier":"GameEventSound:PlayerPlanetLostToCulture",
                    "val":"EVENTTECH_PLAYERPLANETLOST"},
                {"identifier":"GameEventSound:PlayerPlanetSoonLostToCulture",
                    "val":"EVENTTECH_PLAYERPLANETSOONLOSTTOCULTURE"},
                {"identifier":"GameEventSound:PlayerPlanetUnderAttack",
                    "val":"EVENTTECH_PLAYERPLANETUNDERATTACK"},
                {"identifier":"GameEventSound:PlayerReceivedResourcesFromAlly",
                    "val":"EFFECT_COINDROP"},
                {"identifier":"GameEventSound:PlayerUnitUnderAttack",
                    "val":"EVENTTECH_PLAYERUNITUNDERATTACK"},
                {"identifier":"GameEventSound:PlayerFlagshipUnderAttack",
                    "val":"EVENTTECH_PLAYERFLAGSHIPUNDERATTACK"},
                {"identifier":"GameEventSound:PlayerFlagshipShieldsDown",
                    "val":"EVENTTECH_PLAYERFLAGSHIPSHIELDSDOWN"},
                {
                    "identifier":"GameEventSound:PlayerFlagshipHullSeverelyDamaged",
                    "val":"EVENTTECH_PLAYERFLAGSHIPHULLSEVERELYDAMAGED"},
                {"identifier":"GameEventSound:PlayerFlagshipDestroyed",
                    "val":"EVENTTECH_PLAYERFLAGSHIPDESTROYED"},
                {"identifier":"GameEventSound:AllyFlagshipDestroyed",
                    "val":"EVENTTECH_ALLYFLAGSHIPDESTROYED"},
                {"identifier":"GameEventSound:EnemyFlagshipDestroyed",
                    "val":"EVENTTECH_ENEMYFLAGSHIPDESTROYED"},
                {"identifier":"GameEventSound:PlayerTitanShieldsDown",
                    "val":"EVENTTECH_REBEL_PLAYERTITANSHIELDSDOWN_0"},
                {"identifier":"GameEventSound:PlayerTitanHullSeverelyDamaged",
                    "val":"EVENTTECH_REBEL_TITANHULLSERVERDAMAGED_0"},
                {"identifier":"GameEventSound:PlayerCapitalShipShieldsDown",
                    "val":"EVENTTECH_PLAYERCAPITALSHIPSHIELDSDOWN"},
                {
                    "identifier":"GameEventSound:PlayerCapitalShipHullSeverelyDamaged",
                    "val":"EVENTTECH_PLAYERCAPITALSHIPHULLSEVERELYDAMAGED"},
                {"identifier":"GameEventSound:AchievementCompleted",
                    "val":"AchievementCompleted"},
                {"identifier":"GameEventSound:QuestAdded",
                    "val":"EVENTTECH_QUESTADDED"},
                {"identifier":"GameEventSound:QuestCompleted",
                    "val":"EVENTTECH_QUESTCOMPLETED"},
                {"identifier":"GameEventSound:QuestFailed",
                    "val":"EVENTTECH_QUESTFAILED"},
                {"identifier":"GameEventSound:QuestEnded","val":"QuestEnded"},
                {"identifier":"GameEventSound:QuestRejected",
                    "val":"QuestRejected"},
                {"identifier":"GameEventSound:PlayerQuestAdded",
                    "val":"PlayerQuestAdded"},
                {"identifier":"GameEventSound:PlayerQuestCompleted",
                    "val":"PlayerQuestCompleted"},
                {"identifier":"GameEventSound:PlayerQuestFailed",
                    "val":"PlayerQuestFailed"},
                {"identifier":"GameEventSound:DiplomaticVictoryHalfway",
                    "val":"DiplomaticVictoryHalfway"},
                {"identifier":"GameEventSound:DiplomaticVictoryComplete",
                    "val":"DiplomaticVictoryComplete"},
                {"identifier":"GameEventSound:AllyDiplomaticVictoryHalfway",
                    "val":"AllyDiplomaticVictoryHalfway"},
                {"identifier":"GameEventSound:AllyDiplomaticVictoryComplete",
                    "val":"AllyDiplomaticVictoryComplete"},
                {"identifier":"GameEventSound:EnemyDiplomaticVictoryHalfway",
                    "val":"EnemyDiplomaticVictoryHalfway"},
                {"identifier":"GameEventSound:EnemyDiplomaticVictoryComplete",
                    "val":"EnemyDiplomaticVictoryComplete"},
                {"identifier":"GameEventSound:OccupationVictoryStarted",
                    "val":"OccupationVictoryStarted"},
                {"identifier":"GameEventSound:OccupationVictoryHalfway",
                    "val":"OccupationVictoryHalfway"},
                {"identifier":"GameEventSound:OccupationVictoryComplete",
                    "val":"OccupationVictoryComplete"},
                {"identifier":"GameEventSound:AllyOccupationVictoryStarted",
                    "val":"AllyOccupationVictoryStarted"},
                {"identifier":"GameEventSound:AllyOccupationVictoryHalfway",
                    "val":"AllyOccupationVictoryHalfway"},
                {"identifier":"GameEventSound:AllyOccupationVictoryComplete",
                    "val":"AllyOccupationVictoryComplete"},
                {"identifier":"GameEventSound:EnemyOccupationVictoryStarted",
                    "val":"EnemyOccupationVictoryStarted"},
                {"identifier":"GameEventSound:EnemyOccupationVictoryHalfway",
                    "val":"EnemyOccupationVictoryHalfway"},
                {"identifier":"GameEventSound:EnemyOccupationVictoryComplete",
                    "val":"EnemyOccupationVictoryComplete"},
                {"identifier":"GameEventSound:ResearchVictoryStarted",
                    "val":"ResearchVictoryStarted"},
                {"identifier":"GameEventSound:ResearchVictoryHalfway",
                    "val":"ResearchVictoryHalfway"},
                {"identifier":"GameEventSound:ResearchVictoryComplete",
                    "val":"ResearchVictoryComplete"},
                {"identifier":"GameEventSound:ResearchComplete",
                    "val":"EVENTTECH_RESEARCHCOMPLETE"},
                {"identifier":"GameEventSound:AllyResearchVictoryStarted",
                    "val":"AllyResearchVictoryStarted"},
                {"identifier":"GameEventSound:AllyResearchVictoryHalfway",
                    "val":"AllyResearchVictoryHalfway"},
                {"identifier":"GameEventSound:AllyResearchVictoryComplete",
                    "val":"AllyResearchVictoryComplete"},
                {"identifier":"GameEventSound:EnemyResearchVictoryStarted",
                    "val":"EnemyResearchVictoryStarted"},
                {"identifier":"GameEventSound:EnemyResearchVictoryHalfway",
                    "val":"EnemyResearchVictoryHalfway"},
                {"identifier":"GameEventSound:EnemyResearchVictoryComplete",
                    "val":"EnemyResearchVictoryComplete"},
                {"identifier":"GameEventSound:ResearchAlreadyMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ResearchMaxLevelQueued",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ResearchNotEnoughPointsInTier",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ResearchNeedMoreLabModules",
                    "val":"EVENTTECH_RESEARCHNEEDMORELABMODULES"},
                {"identifier":"GameEventSound:ResearchNeedPrerequisite",
                    "val":"EVENTTECH_RESEARCHNEEDPREREQUISITE"},
                {"identifier":"GameEventSound:MoreModuleSlotsNeeded",
                    "val":"EVENTTECH_MOREMODULESLOTSNEEDED"},
                {"identifier":"GameEventSound:SpaceMineLimitReached",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:StarBaseUpgradeNotEnoughUpgradePoints",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:StarBaseUpgradeAlreadyMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:StarBaseUpgradeMaxLevelQueued",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:TitanUpgradeNotEnoughUpgradePoints",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:TitanUpgradeAlreadyMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:InvalidModulePlacementPosition",
                    "val":"EVENTTECH_INVALIDMODULEPLACEMENTPOSITION"},
                {"identifier":"GameEventSound:AbilityUpgradeAlreadyMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AbilityUpgradeRequiresHigherLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:AbilityUpgradeNoUnspentPoints",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ShipUpgradeAlreadyMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ShipUpgradeAlreadyQueuedMaxLevel",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityCooldownNotExpired",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityDisabledByDebuff",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityNoAntimatter",
                    "val":"EVENTTECH_USEABILITYNOANTIMATTER"},
                {"identifier":"GameEventSound:UseAbilityNoUndockedSquadMembers",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityNotEnoughRoomForStarBase",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityCannotUseAbitraryLocationOnEntity",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityTooManyStarBases",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityMineIsNotActivated",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityTargetNotInRange",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityNoValidObjectType",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityPhaseSpaceTarget",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityNormalSpaceTarget",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintCanBeCaptured",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintCanHaveFighters",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintCanHaveShields",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintCanSpreadWeaponDamage",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintCanPhaseDodge",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasAbilityWithCooldown",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasAntimatter",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasPhaseMissiles",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasPopulation",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasEnergyWeapons",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasHullDamage",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasShieldDamage",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasAntiMatterShortage",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintHasWeapons",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsCargoShip",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsExplored",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsResourceExtractor",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsMovingToLocalOrbitBody",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetConstraintIsPsi",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintLinkedCargoShip",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintLinkedSquad",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetConstraintNotSelf",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintWithinSolarSystem",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintModuleUnderConstruction",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintModuleIsActivelyBeingConstructed",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintModuleNotActivelyBeingConstructed",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsNotHomeworld",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintNotInvulnerable",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsInFriendlyGravityWell",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetConstraintIsInNonFriendlyGravityWell",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetOwnerLinked",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetOwnerPlayer",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetOwnerFriendly",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetOwnerHostile",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:UseAbilityUnmetOwnerNoOwner",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:UseAbilityUnmetOwnerAlliedOrEnemy",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:GameStart","val":"GameStart"},
                {"identifier":"GameEventSound:GameWin",
                    "val":"EVENTTECH_GAMEWIN"},
                {"identifier":"GameEventSound:AllyGameWin",
                    "val":"EVENTTECH_ALLIEDGAMEWIN"},
                {"identifier":"GameEventSound:CapitalShipLevelUp",
                    "val":"CapitalShipLevelUp"},
                {"identifier":"GameEventSound:AttackTargetInvalidWithNoReason",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsNotDamagableDueToBuffs",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:AttackTargetInvalidAsNotEnemy",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsCantFireAtFighters",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsCantMoveAndTargetNotInRange",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsCantMoveToTarget",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsCanOnlyAttackStructures",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsMustBeInSameOrbitWell",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsNoWeaponsToBombPlanet",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {
                    "identifier":"GameEventSound:AttackTargetInvalidAsPlanetCantBeBombed",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ScuttleShipStarted",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ScuttleShipStopped",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ScuttlePlanetStarted",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:ScuttlePlanetStopped",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:MarketBoom","val":"MarketBoom"},
                {"identifier":"GameEventSound:MarketCrash",
                    "val":"MarketCrash"},
                {"identifier":"GameEventSound:PlayerCultureFlippedEnemyPlanet",
                    "val":"PlayerCultureFlippedEnemyPlanet"},
                {"identifier":"GameEventSound:EnvoyTargetPlanetIsNowInvalid",
                    "val":"UI_COMMON_DEFAULTERROR"},
                {"identifier":"GameEventSound:OffensiveMissionDiscovered",
                    "val":"OffensiveMissionDiscovered"},
                {"identifier":"GameEventSound:MissionDiscovered",
                    "val":"MissionDiscovered"}
            ]
        }
        cls.planetElevatorData = {
            "identifier":"planetElevatorData",
            "taxCenter":{"identifier":"elevatorMeshName:TaxCenter",
                "val":"taxcenter"},
            "cultureCenter":{"identifier":"elevatorMeshName:CultureCenter",
                "val":"culturecenter"},
            "metalExtractor":{"identifier":"elevatorMeshName:MetalExtractor",
                "val":"metalextractor"},
            "crystalExtractor":{
                "identifier":"elevatorMeshName:CrystalExtractor",
                "val":"crystalextractor"},
            "bunker":{"identifier":"elevatorMeshName:Bunker","val":"bunker"},
            "goodsFactory":{"identifier":"elevatorMeshName:GoodsFactory",
                "val":"goodsfactory"},
            "productionCenter":{
                "identifier":"elevatorMeshName:ProductionCenter",
                "val":"prodcenter"}
        }
        cls.musicThemes = {
            "identifier":"musicThemeData",
            "startup":{
                "counter":{"identifier":"startupMusicThemeCount","val":1},
                "elements":[
                    {"identifier":"musicTheme","val":"mstartup"}
                ]
            },
            "neutral":{
                "counter":{"identifier":"neutralMusicThemeCount","val":1},
                "elements":[
                    {"identifier":"musicTheme","val":"mneutral"}
                ]
            },
            "scare":{
                "counter":{"identifier":"scareThemeCount","val":1},
                "elements":[
                    {"identifier":"musicTheme","val":"mscare"}
                ]
            },
            "announcement":{
                "counter":{"identifier":"announcementCount","val":1},
                "elements":[
                    {"identifier":"musicTheme","val":"mannounce"}
                ]
            }
        }
        cls.hudSkinData = {
            "identifier":"hudSkinData",
            "loadScreenCharacters":{"identifier":"loadScreenCharacterBrush",
                "val":"LoadScreenCharacterTech"},
            "fleetIcons":{
                "counter":{"identifier":"fleetIconCount","val":1},
                "elements":[
                    {"identifier":"fleetIcon","val":"fleeticon"}
                ]
            },
            "bandbox":{"identifier":"Bandbox","val":"BANDBOX"},
            "fogOfWar":{"identifier":"FogOfWar","val":"FogOfWar"},
            "hudIcons":[
                {"identifier":"ActionGridSlotBackdrop",
                    "val":"ActionGridSlotBackdropTech"},
                {"identifier":"BottomWindowBackdrop",
                    "val":"BottomWindowBackdropTech"},
                {"identifier":"BottomWindowOverlay",
                    "val":"BottomWindowOverlayTech"},
                {"identifier":"TopWindowBackdrop",
                    "val":"TopWindowBackdropTech"},
                {"identifier":"TopWindowOverlay","val":"TopWindowOverlayTech"},
                {"identifier":"InfoCardAttachedToBottomBackdrop",
                    "val":"InfoCardAttachedToBottomBackdropTech"},
                {"identifier":"InfoCardAttachedToScreenEdgeBackdrop",
                    "val":"InfoCardAttachedToScreenEdgeBackdropTech"},
                {"identifier":"TopWindowLeftCapLowRes",
                    "val":"TopWindowLeftCapLowResTech"},
                {"identifier":"TopWindowLeftCapHighRes",
                    "val":"TopWindowLeftCapHighResTech"},
                {"identifier":"TopWindowRightCapLowRes",
                    "val":"TopWindowRightCapLowResTech"},
                {"identifier":"TopWindowRightCapHighRes",
                    "val":"TopWindowRightCapHighResTech"},
                {"identifier":"EmpireWindowBackdrop",
                    "val":"EmpireWindowOptionsWindowBackdropTech"},
                {"identifier":"EmpireWindowHighResBackdrop",
                    "val":"EmpireWindowOptionsWindowHighResBackdropTech"},
                {"identifier":"QuickMarketBackdrop",
                    "val":"QuickMarketBackdropTech"},
                {"identifier":"QuickMarketBackdropNoPirates",
                    "val":"QuickMarketBackdropNoPiratesTech"},
                {"identifier":"UnknownPlanetHUDIcon",
                    "val":"HUDICON_PLANET_UNKNOWN"},
                {"identifier":"UnknownPlanetHUDSmallIcon",
                    "val":"HUDICONSMALL_PLANET_UNKNOWN"},
                {"identifier":"UnknownPlanetInfoCardIcon",
                    "val":"INFOCARDICON_PLANET_UNKNOWN"},
                {"identifier":"UnknownPlanetMainViewIcon",
                    "val":"MAINVIEWICON_PLANET_UNKNOWN"},
                {"identifier":"UnknownPlanetPicture",
                    "val":"PICTURE_PLANET_UNKNOWN"},
                {"identifier":"AutoAttackRangeStateMixed",
                    "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGENONE"},
                {"identifier":"AutoAttackRangeStateAllNone",
                    "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGENONE"},
                {"identifier":"AutoAttackRangeStateAllLocalArea",
                    "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGELOCALAREA"},
                {"identifier":"AutoAttackRangeStateAllGravityWell",
                    "val":"ACTIONICON_CAPITALSHIP_ATTACKRANGEGRAVITYWELL"},
                {"identifier":"CohesionRangeStateMixed",
                    "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGENONE"},
                {"identifier":"CohesionRangeStateAllNone",
                    "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGENONE"},
                {"identifier":"CohesionRangeStateAllNear",
                    "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGENEAR"},
                {"identifier":"CohesionRangeStateAllFar",
                    "val":"ACTIONICON_CAPITALSHIP_COHESIONRANGEFAR"},
                {"identifier":"GroupMoveStateNone",
                    "val":"ACTIONICON_CAPITALSHIP_SINGLEMOVE"},
                {"identifier":"GroupMoveStateSome",
                    "val":"ACTIONICON_CAPITALSHIP_PARTIALGROUPMOVE"},
                {"identifier":"GroupMoveStateAll",
                    "val":"ACTIONICON_CAPITALSHIP_WHOLEGROUPMOVE"},
                {"identifier":"ZoomInstantContextNear",
                    "val":"ActionIconCameraZoomInstantWithZoomNearContext"},
                {"identifier":"ZoomInstantContextFar",
                    "val":"ActionIconCameraZoomInstantWithZoomFarContext"},
                {"identifier":"GameEventCategoryPing",
                    "val":"GameEventCategoryInfoCardIconPing"},
                {"identifier":"GameEventCategoryProduction",
                    "val":"GameEventCategoryInfoCardIconProduction"},
                {"identifier":"GameEventCategoryAlliance",
                    "val":"GameEventCategoryInfoCardIconAlliance"},
                {"identifier":"GameEventCategoryThreat",
                    "val":"GameEventCategoryInfoCardIconThreat"},
                {"identifier":"HUDIconQuest","val":"HUDIconQuest"},
                {"identifier":"HUDIconHappiness",
                    "val":"HUDIconHappinessGained"},
                {"identifier":"HUDIconOverlayNotEnoughCredits",
                    "val":"HUDIconOverlayNotEnoughCredits"},
                {"identifier":"HUDIconOverlayNotEnoughMetal",
                    "val":"HUDIconOverlayNotEnoughMetal"},
                {"identifier":"HUDIconOverlayNotEnoughCrystal",
                    "val":"HUDIconOverlayNotEnoughCrystal"},
                {"identifier":"HUDIconOverlayNotResearched",
                    "val":"HUDIconOverlayNotResearched"},
                {
                    "identifier":"HUDIconOverlayPlanetUpgradePathLastStageUpgraded",
                    "val":"HUDIconOverlayPlanetUpgradePathLastStageUpgraded"},
                {
                    "identifier":"HUDIconOverlayPlanetModuleNoAvailableResourceAsteroids",
                    "val":"HUDIconOverlayPlanetModuleNoAvailableResourceAsteroids"},
                {
                    "identifier":"HUDIconOverlayPlanetModuleNoTacticalSlotsAvailable",
                    "val":"HUDIconOverlayPlanetModuleNoTacticalSlotsAvailable"},
                {
                    "identifier":"HUDIconOverlayPlanetModuleNoCivilianSlotsAvailable",
                    "val":"HUDIconOverlayPlanetModuleNoCivilianSlotsAvailable"},
                {"identifier":"HUDIconOverlayPlanetBuildShipNoFrigateFactories",
                    "val":"HUDIconOverlayPlanetBuildShipNoFrigateFactories"},
                {
                    "identifier":"HUDIconOverlayPlanetBuildShipNoCapitalShipFactories",
                    "val":"HUDIconOverlayPlanetBuildShipNoCapitalShipFactories"},
                {"identifier":"HUDIconOverlayPlanetBuildShipNotEnoughShipSlots",
                    "val":"HUDIconOverlayPlanetBuildShipNotEnoughShipSlotsTech"},
                {
                    "identifier":"HUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlots",
                    "val":"HUDIconOverlayPlanetBuildShipNotEnoughCapitalShipSlotsTech"},
                {
                    "identifier":"HUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints",
                    "val":"HUDIconOverlayStarBaseUpgradeNotEnoughUpgradePoints"},
                {
                    "identifier":"HUDIconOverlayStarBaseUpgradePathLastStageUpgraded",
                    "val":"HUDIconOverlayStarBaseUpgradePathLastStageUpgraded"},
                {"identifier":"HUDIconOverlayInFleet",
                    "val":"HUDIconOverlayInFleet"},
                {"identifier":"AllianceHUDIconTrade",
                    "val":"HUDIconAllianceTypeTrade"},
                {"identifier":"AllianceHUDIconExternalVision",
                    "val":"HUDIconAllianceTypeExternalVision"},
                {"identifier":"AllianceHUDIconInternalVision",
                    "val":"HUDIconAllianceTypeInternalVision"},
                {"identifier":"AllianceHUDIconCeaseFire",
                    "val":"HUDIconAllianceTypeCeaseFire"},
                {"identifier":"AllianceHUDIconPeaceTreaty",
                    "val":"HUDIconAllianceTypePeaceTreaty"},
                {"identifier":"AllianceHUDIconResearchPact",
                    "val":"PlayerScreenOfferResearchPact"},
                {"identifier":"AllianceHUDIconEfficiencyPact",
                    "val":"PlayerScreenOfferEfficiencyPact"},
                {"identifier":"AllianceHUDIconAntimatterPact",
                    "val":"PlayerScreenOfferAntimatterPact"},
                {"identifier":"AllianceHUDIconArmorPact",
                    "val":"PlayerScreenOfferArmorPact"},
                {"identifier":"AllianceHUDIconBeamWeaponPact",
                    "val":"PlayerScreenOfferBeamPact"},
                {"identifier":"AllianceHUDIconCulturePact",
                    "val":"PlayerScreenOfferCulturePact"},
                {"identifier":"AllianceHUDIconMetalCrystlPact",
                    "val":"PlayerScreenOfferMetalCrystalPact"},
                {"identifier":"AllianceHUDIconThrusterPact",
                    "val":"PlayerScreenOfferThrusterPact"},
                {"identifier":"AllianceHUDIconPhaseJumpPact",
                    "val":"PlayerScreenOfferPhaseJumpPact"},
                {"identifier":"AllianceHUDIconPlanetBombingPact",
                    "val":"PlayerScreenOfferPlanetBombingPact"},
                {"identifier":"AllianceHUDIconShieldPact",
                    "val":"PlayerScreenOfferShieldPact"},
                {"identifier":"AllianceHUDIconMarketPact",
                    "val":"PlayerScreenOfferMarketPact"},
                {"identifier":"AllianceHUDIconEnergyWeaponCooldownPact",
                    "val":"PlayerScreenOfferEnergyWeaponCooldownPact"},
                {"identifier":"AllianceHUDIconMassReductionPact",
                    "val":"PlayerScreenOfferMassReductionPact"},
                {"identifier":"AllianceHUDIconSupplyPact",
                    "val":"PlayerScreenOfferSupplyPact"},
                {"identifier":"AllianceHUDIconTacticalSlotsPact",
                    "val":"PlayerScreenOfferTacticalSlotsPact"},
                {"identifier":"AllianceHUDIconWeaponCooldownPact",
                    "val":"PlayerScreenOfferWeaponCooldownPact"},
                {"identifier":"AllianceHUDIconTradeIncomePact",
                    "val":"PlayerScreenOfferTradeIncomePact"},
                {"identifier":"ResourceHUDIconCredits","val":"HUDIconCredits"},
                {"identifier":"ResourceHUDIconMetal","val":"HUDIconMetal"},
                {"identifier":"ResourceHUDIconCrystal","val":"HUDIconCrystal"},
                {"identifier":"ResourceInfoCardIconCredits",
                    "val":"InfoCardIconCredits"},
                {"identifier":"ResourceInfoCardIconMetal",
                    "val":"InfoCardIconMetal"},
                {"identifier":"ResourceInfoCardIconCrystal",
                    "val":"InfoCardIconCrystal"},
                {"identifier":"ResearchFieldButtonIconCombat",
                    "val":"ResearchScreenTabButtonCombatTech"},
                {"identifier":"ResearchFieldButtonIconDefense",
                    "val":"ResearchScreenTabButtonDefenseTech"},
                {"identifier":"ResearchFieldButtonIconNonCombat",
                    "val":"ResearchScreenTabButtonNonCombatTech"},
                {"identifier":"ResearchFieldButtonIconDiplomacy",
                    "val":"ResearchScreenTabButtonDiplomacyTech"},
                {"identifier":"ResearchFieldButtonIconFleet",
                    "val":"ResearchScreenTabButtonFleetTech"},
                {"identifier":"ResearchFieldButtonIconArtifact",
                    "val":"ResearchScreenTabButtonArtifact"},
                {"identifier":"QuestStatusIconBombAnyPlanet",
                    "val":"HUDIconQuestBombAnyPlanet"},
                {"identifier":"QuestStatusIconKillShips",
                    "val":"HUDIconQuestKillShips"},
                {"identifier":"QuestStatusIconKillCapitalShips",
                    "val":"HUDIconQuestKillCapitalShips"},
                {"identifier":"QuestStatusIconKillCivilianStructures",
                    "val":"HUDIconQuestKillCivilianStructures"},
                {"identifier":"QuestStatusIconKillTacticalStructures",
                    "val":"HUDIconQuestKillTacticalStructures"},
                {"identifier":"QuestStatusIconSendEnvoy",
                    "val":"HUDIconQuestSendEnvoy"},
                {"identifier":"OrderInfoCardIconNone",
                    "val":"OrderInfoCardIconNone"},
                {"identifier":"OrderInfoCardIconAttack",
                    "val":"InfoCardIconOrderAttack"},
                {"identifier":"OrderInfoCardIconMove",
                    "val":"InfoCardIconOrderMove"},
                {"identifier":"OrderInfoCardIconAbility",
                    "val":"InfoCardIconOrderAbility"},
                {"identifier":"OrderInfoCardIconRetreat",
                    "val":"InfoCardIconOrderRetreat"},
                {"identifier":"InfoCardIconAutoCast",
                    "val":"InfoCardIconAutoCast"},
                {"identifier":"InfoCardIconCombinedPlanetIncome",
                    "val":"InfoCardIconCombinedPlanetIncome"},
                {"identifier":"InfoCardIconCombinedStarBaseIncome",
                    "val":"InfoCardIconCombinedStarBaseIncome"},
                {"identifier":"InfoCardIconFleetUpkeep",
                    "val":"InfoCardIconFleetUpkeepTech"},
                {"identifier":"InfoCardIconCapitalShip",
                    "val":"INFOCARDICON_CAPITALSHIP_TECHBATTLESHIP"},
                {"identifier":"InfoCardIconFrigate",
                    "val":"INFOCARDICON_FRIGATE_TECHSCOUT"},
                {"identifier":"InfoCardIconTradeShip",
                    "val":"INFOCARDICON_TRADESHIP_TECH"},
                {"identifier":"InfoCardIconRefineryShip",
                    "val":"INFOCARDICON_REFINERYSHIP_TECH"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel0",
                    "val":"MainViewIconOverlayCapitalShipLevel0"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel1",
                    "val":"MainViewIconOverlayCapitalShipLevel1"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel2",
                    "val":"MainViewIconOverlayCapitalShipLevel2"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel3",
                    "val":"MainViewIconOverlayCapitalShipLevel3"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel4",
                    "val":"MainViewIconOverlayCapitalShipLevel4"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel5",
                    "val":"MainViewIconOverlayCapitalShipLevel5"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel6",
                    "val":"MainViewIconOverlayCapitalShipLevel6"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel7",
                    "val":"MainViewIconOverlayCapitalShipLevel7"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel8",
                    "val":"MainViewIconOverlayCapitalShipLevel8"},
                {"identifier":"MainViewIconOverlayCapitalShipLevel9",
                    "val":"MainViewIconOverlayCapitalShipLevel9"},
                {"identifier":"EmpireWindowFindIconOwnedColonyShips",
                    "val":"EmpireWindowFindIconOwnedColonyShipsTech"},
                {"identifier":"EmpireWindowFindIconOwnedScoutShips",
                    "val":"EmpireWindowFindIconOwnedScoutShipsTech"},
                {"identifier":"EmpireWindowFindIconOwnedCapitalShips",
                    "val":"EmpireWindowFindIconOwnedCapitalShipsTech"},
                {"identifier":"EmpireWindowFindIconOwnedStarbases",
                    "val":"EmpireWindowFindIconOwnedStarbasesTech"},
                {"identifier":"EmpireWindowFindIconOwnedEnvoys",
                    "val":"EmpireWindowFindIconOwnedEnvoysTech"},
                {
                    "identifier":"EmpireWindowFindIconOwnedPlanetsWithResourceAsteroids",
                    "val":"EmpireWindowFindIconOwnedPlanetsWithResourceAsteroidsTech"},
                {"identifier":"EmpireWindowFindIconOwnedPlanetsWithFactories",
                    "val":"EmpireWindowFindIconOwnedPlanetsWithFactoriesTech"},
                {"identifier":"EmpireWindowFindIconOwnedPlanetsWithUpgrades",
                    "val":"EmpireWindowFindIconOwnedPlanetsWithUpgradesTech"},
                {
                    "identifier":"EmpireWindowFindIconOwnedPlanetsWithCivilianSlots",
                    "val":"EmpireWindowFindIconOwnedPlanetsWithCivilianSlotsTech"},
                {"identifier":"EmpireWindowFindIconPlanetsWithArtifacts",
                    "val":"EmpireWindowFindIconPlanetsWithArtifactsTech"},
                {"identifier":"UserActionIconAttack",
                    "val":"ACTIONICON_CAPITALSHIP_ATTACK"},
                {"identifier":"UserActionIconStop",
                    "val":"ACTIONICON_CAPITALSHIP_STOP"},
                {"identifier":"UserActionIconCreateFleet",
                    "val":"ActionIconCreateFleet"},
                {"identifier":"UserActionIconDisbandFleet",
                    "val":"ActionIconDisbandFleet"},
                {"identifier":"UserActionIconRetreat",
                    "val":"ActionIconRetreat"},
                {"identifier":"UserActionIconSetRallyPoint",
                    "val":"ACTIONICON_SETRALLYPOINT"},
                {"identifier":"UserActionIconOpenModulePage0",
                    "val":"ACTIONICON_PLANET_OPENPLANETMODULEMANAGEMENT_ORBIT0"},
                {"identifier":"UserActionIconOpenModulePage1",
                    "val":"ACTIONICON_PLANET_OPENPLANETMODULEMANAGEMENT_ORBIT1"},
                {"identifier":"UserActionIconCloseModulePage0",
                    "val":"ACTIONICON_PLANET_CLOSEPLANETMODULEMANAGEMENT_ORBIT0"},
                {"identifier":"UserActionIconCloseModulePage1",
                    "val":"ACTIONICON_PLANET_CLOSEPLANETMODULEMANAGEMENT_ORBIT1"},
                {"identifier":"UserActionIconOpenPlanetTitanManagement",
                    "val":"ACTIONICON_PLANET_OPENTITANMANAGEMENT_TECHREBEL"},
                {"identifier":"UserActionIconClosePlanetTitanManagement",
                    "val":"ACTIONICON_PLANET_CLOSECAPITALSHIPMANAGEMENT"},
                {"identifier":"UserActionIconOpenPlanetCapitalShipManagement",
                    "val":"ACTIONICON_PLANET_OPENCAPITALSHIPMANAGEMENT"},
                {"identifier":"UserActionIconClosePlanetCapitalShipManagement",
                    "val":"ACTIONICON_PLANET_CLOSECAPITALSHIPMANAGEMENT"},
                {"identifier":"UserActionIconOpenPlanetFrigateManagement0",
                    "val":"ACTIONICON_PLANET_OPENFRIGATEMANAGEMENT0"},
                {"identifier":"UserActionIconOpenPlanetFrigateManagement1",
                    "val":"ACTIONICON_PLANET_OPENFRIGATEMANAGEMENT1"},
                {"identifier":"UserActionIconClosePlanetFrigateManagement0",
                    "val":"ACTIONICON_PLANET_CLOSEFRIGATEMANAGEMENT0"},
                {"identifier":"UserActionIconClosePlanetFrigateManagement1",
                    "val":"ACTIONICON_PLANET_CLOSEFRIGATEMANAGEMENT1"},
                {"identifier":"UserActionIconOpenSquadManagement",
                    "val":"ACTIONICON_CAPITALSHIP_OPENSQUADRONMANAGEMENT"},
                {"identifier":"UserActionIconCloseSquadManagement",
                    "val":"ACTIONICON_CAPITALSHIP_CLOSESQUADRONMANAGEMENT"},
                {"identifier":"UserActionIconOpenShipAbilityManagement",
                    "val":"ACTIONICON_CAPITALSHIP_OPENCAPITALABILITYMANAGEMENT"},
                {"identifier":"UserActionIconCloseShipAbilityManagement",
                    "val":"ACTIONICON_CAPITALSHIP_CLOSECAPITALABILITYMANAGEMENT"},
                {"identifier":"UserActionIconOpenShipTacticsManagement",
                    "val":"ACTIONICON_CAPITALSHIP_OPENMANAGETACTICS"},
                {"identifier":"UserActionIconCloseShipTacticsManagement",
                    "val":"ACTIONICON_CAPITALSHIP_CLOSEMANAGETACTICS"},
                {"identifier":"UserActionIconPurchaseCapitalShipLevel",
                    "val":"ACTIONICON_CAPITALSHIP_TOGGLEPURCHASENEXTLEVEL"},
                {"identifier":"UserActionIconOpenPlanetUpgradeManagement",
                    "val":"ActionIconPlanetOpenUpgradeManagement"},
                {"identifier":"UserActionIconClosePlanetUpgradeManagement",
                    "val":"ActionIconPlanetCloseUpgradeManagement"},
                {"identifier":"UserActionIconOpenStarBaseUpgradeManagement",
                    "val":"ACTIONICON_PLANET_OPENSTARBASEMANAGEMENT"},
                {"identifier":"UserActionIconCloseStarBaseUpgradeManagement",
                    "val":"ActionIconStarBaseCloseUpgradeManagement"},
                {"identifier":"UserActionIconUpgradePlanetPopulation",
                    "val":"ActionIconPlanetUpgradePopulation"},
                {"identifier":"UserActionIconUpdatePlanetInfrastructure",
                    "val":"ActionIconPlanetUpgradeInfrastructure"},
                {"identifier":"UserActionIconUpgradePlanetArtifactLevel",
                    "val":"ActionIconPlanetUpgradeArtifactLevel"},
                {"identifier":"UserActionIconUpgradePlanetHome",
                    "val":"ActionIconPlanetUpgradeHome"},
                {"identifier":"UserActionIconUpgradePlanetCivilianModules",
                    "val":"ActionIconPlanetUpgradeCivilianModules"},
                {"identifier":"UserActionIconUpgradePlanetTacticalModules",
                    "val":"ActionIconPlanetUpgradeTacticalModules"},
                {"identifier":"UserActionIconUpgradePlanetSocial",
                    "val":"ActionIconPlanetUpgradeSocial"},
                {"identifier":"UserActionIconUpgradePlanetIndustry",
                    "val":"ActionIconPlanetUpgradeIndustry"},
                {"identifier":"UserActionIconUpgradePlanetSmuggler",
                    "val":"ActionIconPlanetUpgradeSmuggler"},
                {"identifier":"UserActionIconBuildResourceAsteroidExtractor",
                    "val":"ActionIconBuildResourceAsteroidExtractor"},
                {"identifier":"UserActionIconPing",
                    "val":"BottomWindowButtonPing"},
                {"identifier":"UserActionIconPingAllyAttack",
                    "val":"ActionIconPingAllyAttack"},
                {"identifier":"UserActionIconPingAllyDefend",
                    "val":"ActionIconPingAllyDefend"},
                {"identifier":"UserActionIconPingAllyStop",
                    "val":"ActionIconPingAllyStop"},
                {"identifier":"UserActionIconToggleZoomToCursor",
                    "val":"ACTIONICON_TOGGLEZOOMTOCURSOR"},
                {"identifier":"UserActionIconToggleScuttle",
                    "val":"ACTIONICON_TOGGLESCUTTLE"},
                {"identifier":"UserActionIconOpenRenameWindow",
                    "val":"ACTIONICON_OPENRENAMEWINDOW"},
                {"identifier":"UserActionIconChangeAutoAttackRange",
                    "val":"ACTIONICON_CAPITALSHIP_CHANGEAUTOATTACKRANGE"},
                {"identifier":"UserActionIconChangeCohesionRange",
                    "val":"ACTIONICON_CAPITALSHIP_CHANGECOHESIONRANGE"},
                {"identifier":"UserActionIconOpenEscapeMenuScreen",
                    "val":"ActionIconOpenEscapeMenuScreen"},
                {"identifier":"UserActionIconOpenNPCScreen",
                    "val":"ActionIconOpenNPCScreen"},
                {"identifier":"UserActionIconOpenRelationshipsScreen",
                    "val":"ActionIconOpenRelationshipsScreen"},
                {"identifier":"UserActionIconOpenResearchScreen",
                    "val":"ActionIconOpenResearchScreenEnabled"},
                {"identifier":"UserActionIconOpenPlayerScreen",
                    "val":"ActionIconOpenPlayerScreen"},
                {"identifier":"UserActionIconToggleEmpireWindowStacked",
                    "val":"ActionIconToggleEmpireWindowStacked"},
                {"identifier":"UserActionIconToggleIsAutoPlaceModuleActive",
                    "val":"ActionIconToggleIsAutoPlaceModuleActive"},
                {"identifier":"UserActionIconIncreaseGameSpeed",
                    "val":"ActionIconIncreaseGameSpeed"},
                {"identifier":"UserActionIconDecreaseGameSpeed",
                    "val":"ActionIconDecreaseGameSpeed"},
            ]
        }
        cls.research = {
            "identifier":"researchScreenData",
            "combat":{
                "identifier":"Combat",
                "counter":{"identifier":"blockCount","val":1},
                "elements":[
                    {
                        "identifier":"block",
                        "area":{"identifier":"area",
                            "coord":[50,69,690,192]},
                        "backdrop":{"identifier":"backdrop","val":"backdrop"},
                        "indicatorBackdrop":{
                            "identifier":"tierIndicatorBackdrop",
                            "val":"indicatorBackdrop"},
                        "title":{"identifier":"title","val":"title"},
                        "picture":{"identifier":"picture","val":"picture"}
                    }
                ]
            },
            "defense":{
                "identifier":"Defense",
                "counter":{"identifier":"blockCount","val":1},
                "elements":[
                    {
                        "identifier":"block",
                        "area":{"identifier":"area",
                            "coord":[50,69,690,192]},
                        "backdrop":{"identifier":"backdrop","val":"backdrop"},
                        "indicatorBackdrop":{
                            "identifier":"tierIndicatorBackdrop",
                            "val":"indicatorBackdrop"},
                        "title":{"identifier":"title","val":"title"},
                        "picture":{"identifier":"picture","val":"picture"}
                    }
                ]
            },
            "nonCombat":{
                "identifier":"NonCombat",
                "counter":{"identifier":"blockCount","val":1},
                "elements":[
                    {
                        "identifier":"block",
                        "area":{"identifier":"area",
                            "coord":[50,69,690,192]},
                        "backdrop":{"identifier":"backdrop","val":"backdrop"},
                        "indicatorBackdrop":{
                            "identifier":"tierIndicatorBackdrop",
                            "val":"indicatorBackdrop"},
                        "title":{"identifier":"title","val":"title"},
                        "picture":{"identifier":"picture","val":"picture"}
                    }
                ]
            },
            "diplomacy":{
                "identifier":"Diplomacy",
                "counter":{"identifier":"blockCount","val":1},
                "elements":[
                    {
                        "identifier":"block",
                        "area":{"identifier":"area",
                            "coord":[50,69,690,192]},
                        "backdrop":{"identifier":"backdrop","val":"backdrop"},
                        "indicatorBackdrop":{
                            "identifier":"tierIndicatorBackdrop",
                            "val":"indicatorBackdrop"},
                        "title":{"identifier":"title","val":"title"},
                        "picture":{"identifier":"picture","val":"picture"}
                    }
                ]
            },
            "artifact":{
                "identifier":"Artifact",
                "counter":{"identifier":"blockCount","val":1},
                "elements":[
                    {
                        "identifier":"block",
                        "area":{"identifier":"area",
                            "coord":[50,69,690,192]},
                        "backdrop":{"identifier":"backdrop","val":"backdrop"},
                        "indicatorBackdrop":{
                            "identifier":"tierIndicatorBackdrop",
                            "val":"indicatorBackdrop"},
                        "title":{"identifier":"title","val":"title"},
                        "picture":{"identifier":"picture","val":"picture"}
                    }
                ]
            },
            "fleet":{
                "identifier":"Fleet",
                "counter":{"identifier":"blockCount","val":1},
                "elements":[
                    {
                        "identifier":"block",
                        "area":{"identifier":"area",
                            "coord":[50,69,690,192]},
                        "backdrop":{"identifier":"backdrop","val":"backdrop"},
                        "indicatorBackdrop":{
                            "identifier":"tierIndicatorBackdrop",
                            "val":"indicatorBackdrop"},
                        "title":{"identifier":"title","val":"title"},
                        "picture":{"identifier":"picture","val":"picture"}
                    }
                ]
            },
            "capShipSlotIcons":[
                {"identifier":"capitalShipSlotLevelPicture-0",
                    "val":"capslot0"},
                {"identifier":"capitalShipSlotLevelPicture-1",
                    "val":"capslot1"},
                {"identifier":"capitalShipSlotLevelPicture-2",
                    "val":"capslot2"},
                {"identifier":"capitalShipSlotLevelPicture-3",
                    "val":"capslot3"},
                {"identifier":"capitalShipSlotLevelPicture-4",
                    "val":"capslot4"},
                {"identifier":"capitalShipSlotLevelPicture-5",
                    "val":"capslot5"},
                {"identifier":"capitalShipSlotLevelPicture-6",
                    "val":"capslot6"},
                {"identifier":"capitalShipSlotLevelPicture-7",
                    "val":"capslot7"},
                {"identifier":"capitalShipSlotLevelPicture-8","val":"capslot8"}
            ],
            "shipSlotIcons":[
                {"identifier":"shipSlotLevelPicture-0","val":"slot0"},
                {"identifier":"shipSlotLevelPicture-1","val":"slot1"},
                {"identifier":"shipSlotLevelPicture-2","val":"slot2"},
                {"identifier":"shipSlotLevelPicture-3","val":"slot3"},
                {"identifier":"shipSlotLevelPicture-4","val":"slot4"},
                {"identifier":"shipSlotLevelPicture-5","val":"slot5"},
                {"identifier":"shipSlotLevelPicture-6","val":"slot6"},
                {"identifier":"shipSlotLevelPicture-7","val":"slot7"},
                {"identifier":"shipSlotLevelPicture-8","val":"slot8"}
            ]
        }
        cls.gameOver = {
            "identifier":"gameOverWindowData",
            "picHasWon":{"identifier":"hasWonPicture","val":"wonpic"},
            "picHasLost":{"identifier":"hasLostPicture","val":"lostpic"},
            "themeBackdrop":{"identifier":"themeBackdropBrush",
                "val":"backdrop"},
            "formatStrings":[
                {"identifier":"hasWonMilitaryStatusFormatStringId",
                    "val":"fmtwonmilitary"},
                {"identifier":"hasLostMilitaryStatusFormatStringId",
                    "val":"fmtlostmilitary"},
                {"identifier":"hasWonFlagshipStatusFormatStringId",
                    "val":"fmtwonflagship"},
                {"identifier":"hasLostFlagshipStatusFormatStringId",
                    "val":"fmtlostflagship"},
                {"identifier":"hasWonHomeworldStatusFormatStringId",
                    "val":"fmtwonhomeworld"},
                {"identifier":"hasLostHomeworldStatusFormatStringId",
                    "val":"fmtlosthomeworld"},
                {"identifier":"hasWonDiplomaticStatusFormatStringId",
                    "val":"fmtwondiplomatic"},
                {"identifier":"hasLostDiplomaticStatusFormatStringId",
                    "val":"fmtlostdiplomatic"},
                {"identifier":"hasWonResearchStatusFormatStringId",
                    "val":"fmtwonresearch"},
                {"identifier":"hasLostResearchStatusFormatStringId",
                    "val":"fmtlostresearch"},
                {"identifier":"hasWonOccupationStatusFormatStringId",
                    "val":"fmtwonoccupation"},
                {"identifier":"hasLostOccupationStatusFormatStringId",
                    "val":"fmtlostoccupation"}
            ],
            "phraseStrings":[
                {"identifier":"hasWonMilitaryPhraseStringId",
                    "val":"phrasewonmilitary"},
                {"identifier":"hasLostMilitaryPhraseStringId",
                    "val":"phraselostmilitary"},
                {"identifier":"hasWonFlagshipPhraseStringId",
                    "val":"phrasewonflagship"},
                {"identifier":"hasLostFlagshipPhraseStringId",
                    "val":"phraselostflagship"},
                {"identifier":"hasWonHomeworldPhraseStringId",
                    "val":"phrasewonhomeworld"},
                {"identifier":"hasLostHomeworldPhraseStringId",
                    "val":"phraselosthomeworld"},
                {"identifier":"hasWonDiplomaticPhraseStringId",
                    "val":"phrasewondiplomatic"},
                {"identifier":"hasLostDiplomaticPhraseStringId",
                    "val":"phraselostdiplomatic"},
                {"identifier":"hasWonResearchPhraseStringId",
                    "val":"phrasewonrsearch"},
                {"identifier":"hasLostResearchPhraseStringId",
                    "val":"phraselostresearch"},
                {"identifier":"hasWonOccupationPhraseStringId",
                    "val":"phrasewonoccupation"},
                {"identifier":"hasLostOccupationPhraseStringId",
                    "val":"phraselostoccupation"}
            ]
        }
        cls.aiNames = {
            "identifier":"aiNameData",
            "counter":{"identifier":"nameCount","val":1},
            "elements":[
                {"identifier":"name","val":"ainame"}
            ]
        }
        cls.raceNameParsePrefix = {"identifier":"raceNameParsePrefix",
            "val":"prefix"}
        cls.raceNameParseFallback = {
            "identifier":"raceNameParsePrefixFallback",
            "val":"fallback"}
        cls.researchFields = {
            "counter":{"identifier":"numResearchFields","val":1},
            "elements":[
                {"identifier":"field","val":"topic"}
            ]
        }
        cls.playerTheme = {"identifier":"playerThemeGroupName",
            "val":"theme"}
        cls.playerPicture = {"identifier":"playerPictureGroupName",
            "val":"picture"}
        cls.relations = {
            "counter":{"identifier":"raceRelationsModCount","val":1},
            "elements":[
                {
                    "identifier":"raceRelationsModifier",
                    "raceName":{"identifier":"raceNameID","val":"race"},
                    "modMin":{"identifier":"modifierMin","val":0.1},
                    "modMax":{"identifier":"modifierMax","val":1.0},
                    "factionRelations":{
                        "counter":{"identifier":"factionRelationsModCount",
                            "val":1},
                        "elements":[
                            {
                                "identifier":"factionRelationsModifier",
                                "factionName":{"identifier":"factionNameID",
                                    "val":"faction"},
                                "otherFactionName":{
                                    "identifier":"otherFactionNameID",
                                    "val":"otherfaction"},
                                "modMin":{"identifier":"modifierMin",
                                    "val":0.25},
                                "modMax":{"identifier":"modifierMax","val":0.5}
                            }
                        ]
                    }
                }
            ]
        }
        cls.defaultFactionName = {"identifier":"defaultFactionNameID",
            "val":"defname"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = player.Player(
                filepath=self.filepath,
                entityType=self.entityType,
                raceName=self.raceName,
                canColonize=self.canColonize,
                isPsi=self.isPsi,
                selectable=self.selectable,
                selectPriority=self.selectPriority,
                randCapNamePrefix=self.randCapNamePrefix,
                randStarbaseNamePrefix=self.randStarbaseNamePrefix,
                buildables=self.buildables,
                debrisData=self.debrisData,
                hyperspaceData=self.hyperspaceData,
                shieldData=self.shieldData,
                gameEventData=self.gameEventData,
                planetElevatorData=self.planetElevatorData,
                musicThemes=self.musicThemes,
                hudSkinData=self.hudSkinData,
                research=self.research,
                gameOver=self.gameOver,
                aiNames=self.aiNames,
                raceNameParsePrefix=self.raceNameParsePrefix,
                raceNameParseFallback=self.raceNameParseFallback,
                researchFields=self.researchFields,
                playerTheme=self.playerTheme,
                playerPicture=self.playerPicture,
                relations=self.relations,
                defaultFactionName=self.defaultFactionName
        )
        self.maxDiff = None

class PlayerTests(PlayerFixtures,unit.TestCase):
    desc = "Creation Tests Player:"

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = player.Player.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether problem is returned for malformed entity
        """
        res = player.Player.createInstance("PlayerMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribRaceName(self):
        """ Test whether raceName attribute is created correctly
        """
        exp = ui.StringReference(**self.raceName)
        self.assertEqual(self.inst.raceName,exp)

    def testCreationAttribCanColonize(self):
        """ Test whether canColonize attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.canColonize)
        self.assertEqual(self.inst.canColonize,exp)

    def testCreationAttribIsPsi(self):
        """ Test whether isPsi attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isPsi)
        self.assertEqual(self.inst.isPsi,exp)

    def testCreationAttribSelectable(self):
        """ Test whether selectable attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.selectable)
        self.assertEqual(self.inst.selectable,exp)

    def testCreationAttribSelectPriority(self):
        """ Test whether selectPriority attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.selectPriority)
        self.assertEqual(self.inst.selectPriority,exp)

    def testCreationAttribRandCapNamePrefix(self):
        """ Test whether randCapNamePrefix attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.randCapNamePrefix)
        self.assertEqual(self.inst.randCapNamePrefix,exp)

    def testCreationAttribRandStarbaseNamePrefix(self):
        """ Test whether randStarbaseNamePrefix attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.randStarbaseNamePrefix)
        self.assertEqual(self.inst.randStarbaseNamePrefix,exp)

    def testCreationAttribBuildables(self):
        """ Test whether buildables attribute is created correctly
        """
        exp = player.PlayerBuildables(**self.buildables)
        self.assertEqual(self.inst.buildables,exp)

    def testCreationAttribDebrisData(self):
        """ Test whether debrisData attribute is created correctly
        """
        exp = player.DebrisData(**self.debrisData)
        self.assertEqual(self.inst.debrisData,exp)

    def testCreationAttribHyperspaceData(self):
        """ Test whether hyperspaceData attribute is created correctly
        """
        exp = player.HyperspaceData(**self.hyperspaceData)
        self.assertEqual(self.inst.hyperspaceData,exp)

    def testCreationAttribShieldData(self):
        """ Test whether shieldData attribute is created correctly
        """
        exp = player.ShieldData(**self.shieldData)
        self.assertEqual(self.inst.shieldData,exp)

    def testCreationAttribGameEventData(self):
        """ Test whether gameEventData attribute is created correctly
        """
        exp = player.GameEventData(**self.gameEventData)
        self.assertEqual(self.inst.gameEventData,exp)

    def testCreationAttribPlanetElevatorData(self):
        """ Test whether planetElevatorData attribute is created correctly
        """
        exp = player.PlanetElevatorData(**self.planetElevatorData)
        self.assertEqual(self.inst.planetElevatorData,exp)

    def testCreationAttribMusicThemes(self):
        """ Test whether musicThemes attribute is created correctly
        """
        exp = player.MusicThemesData(**self.musicThemes)
        self.assertEqual(self.inst.musicThemes,exp)

    def testCreationAttribHudSkinData(self):
        """ Test whether hudSkinData attribute is created correctly
        """
        exp = player.HudSkinData(**self.hudSkinData)
        self.assertEqual(self.inst.hudSkinData,exp)

    def testCreationAttribResearch(self):
        """ Test whether research attribute is created correctly
        """
        exp = player.ResearchData(**self.research)
        self.assertEqual(self.inst.research,exp)

    def testCreationAttribGameOver(self):
        """ Test whether gameOver attribute is created correctly
        """
        exp = player.GameOverData(**self.gameOver)
        self.assertEqual(self.inst.gameOver,exp)

    def testCreationAttribAINames(self):
        """ Test whether aiNames attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=ui.StringReference,**self.aiNames)
        self.assertEqual(self.inst.aiNames,exp)

    def testCreationAttribRaceNameParsePrefix(self):
        """ Test whether raceNameParsePrefix attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.raceNameParsePrefix)
        self.assertEqual(self.inst.raceNameParsePrefix,exp)

    def testCreationAttribRaceNameParseFallback(self):
        """ Test whether raceNameParseFallback attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.raceNameParseFallback)
        self.assertEqual(self.inst.raceNameParseFallback,exp)

    def testCreationAttribResearchFields(self):
        """ Test whether researchFields attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=ui.StringReference,**self.researchFields)
        self.assertEqual(self.inst.researchFields,exp)

    def testCreationAttribPlayerTheme(self):
        """ Test whether playerTheme attribute is created correctly
        """
        exp = themes.PlayerThemeRef(**self.playerTheme)
        self.assertEqual(self.inst.playerTheme,exp)

    def testCreationAttribPlayerPicture(self):
        """ Test whether playerPicture attribute is created correctly
        """
        exp = themes.PlayerPictureRef(**self.playerPicture)
        self.assertEqual(self.inst.playerPicture,exp)

    def testCreationAttribRelations(self):
        """ Test whether relations attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=player.RaceRelationMod,**self.relations)
        self.assertEqual(self.inst.relations,exp)

    def testCreationAttribDefaultFactionName(self):
        """ Test whether defaultFactionName attribute is created correctly
        """
        exp = ui.StringReference(**self.defaultFactionName)
        self.assertEqual(self.inst.defaultFactionName,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlayerChecking(PlayerFixtures,unit.TestCase):
    desc = "Test Player checking:"

    def setUp(self):
        super().setUp()
        patcher = mock.patch.object(self.inst.raceName,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockRaceName = patcher.start()
        patcher = mock.patch.object(self.inst.buildables,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockBuildables = patcher.start()
        patcher = mock.patch.object(self.inst.debrisData,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDebris = patcher.start()
        patcher = mock.patch.object(self.inst.hyperspaceData,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockHyper = patcher.start()
        patcher = mock.patch.object(self.inst.shieldData,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockShield = patcher.start()
        patcher = mock.patch.object(self.inst.gameEventData,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockEvents = patcher.start()
        patcher = mock.patch.object(self.inst.planetElevatorData,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockElevators = patcher.start()
        patcher = mock.patch.object(self.inst.musicThemes,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockMusic = patcher.start()
        patcher = mock.patch.object(self.inst.hudSkinData,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockHud = patcher.start()
        patcher = mock.patch.object(self.inst.research,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockResearch = patcher.start()
        patcher = mock.patch.object(self.inst.gameOver,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockGameOver = patcher.start()
        patcher = mock.patch.object(self.inst.aiNames,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockai = patcher.start()
        patcher = mock.patch.object(self.inst.researchFields,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockFields = patcher.start()
        patcher = mock.patch.object(self.inst.playerTheme,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTheme = patcher.start()
        patcher = mock.patch.object(self.inst.playerPicture,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockPicture = patcher.start()
        patcher = mock.patch.object(self.inst.relations,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockRelations = patcher.start()
        patcher = mock.patch.object(self.inst.defaultFactionName,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockFactionName = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
