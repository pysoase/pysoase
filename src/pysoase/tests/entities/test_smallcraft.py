""" Tests for the pysoase.entities.smallcraft module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.ships as ships
import pysoase.entities.smallcraft as scraft
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_armed import HasAttackBehaviorFixtures, \
    ArmedFixtures
from pysoase.tests.entities.test_hitable import ControllableFixture, \
    ShieldedFixtures
from pysoase.tests.entities.test_ships import MovingFixtures

testdata = os.path.join("entities","smallcraft")

"""########################### Tests Smallcraft ############################"""

class SmallcraftFixture(MovingFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            cls.parseString
            +"MeshName \"mesh\"\n"
            +"ExhaustTrailTextureName \"exhausttexture\"\n"
            +"ExhaustTrailWidth 15.5\n"
        )
        cls.meshCap = {"identifier":"MeshName","val":"mesh"}
        cls.exhaustTextureCap = {"identifier":"ExhaustTrailTextureName",
            "val":"exhausttexture"}
        cls.exhaustWidthCap = {"identifier":"ExhaustTrailWidth","val":15.5}
        cls.mmod = tco.genMockMod("./")

class SmallcraftTests(SmallcraftFixture,unit.TestCase):
    desc = "Tests Smallcraft:"

    def setUp(self):
        self.inst = scraft.Smallcraft(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                mesh=self.meshCap,
                exhaustTexture=self.exhaustTextureCap,
                exhaustWidth=self.exhaustWidthCap
        )
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        patcher = mock.patch.object(self.inst.exhaustTexture,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockExhaustTexture = patcher.start()
        patcher = mock.patch.object(self.inst.exhaustSys,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.engineSound,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_zoomDist
            +coe.g_shadows
            +coe.g_mass
            +coe.g_movement
            +ships.g_engineSound
            +ships.g_exhaustSys
            +co_parse.genStringAttrib("MeshName")("mesh")
            +co_parse.genStringAttrib("ExhaustTrailTextureName")("exhaustTexture")
            +co_parse.genDecimalAttrib("ExhaustTrailWidth")("exhaustWidth")
        )
        parseRes = parser.parseString(self.parseString)
        res = scraft.Smallcraft(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMesh(self):
        """ Test whether mesh attrib is created correctly
        """
        exp = meshes.MeshRef(**self.meshCap)
        self.assertEqual(self.inst.mesh,exp)

    def testCreationAttribExhaustTexture(self):
        """ Test whether exhaustTexture attrib is created correctly
        """
        exp = ui.UITextureRef(**self.exhaustTextureCap)
        self.assertEqual(self.inst.exhaustTexture,exp)

    def testCreationAttribExhaustWidth(self):
        """ Test whether exhaustWidth attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.exhaustWidthCap)
        self.assertEqual(self.inst.exhaustWidth,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMesh(self):
        """ Test whether check returns problems from mesh attrib
        """
        self.mockMesh.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockMesh.assert_called_once_with(self.mmod,False)

    def testCheckExhaustTexture(self):
        """ Test whether check returns problems from exhaustTexture attrib
        """
        self.mockExhaustTexture.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockExhaustTexture.assert_called_once_with(self.mmod,False)

"""########################## Tests EntryVehicle ###########################"""

class EntryVehicleTests(SmallcraftFixture,unit.TestCase):
    desc = "Tests EntryVehicle:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "EntryVehicle.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"EntryVehicle"}
        cls.atmoEntryEff = {"identifier":"AtmosphereEntryEffectName",
            "val":"atmoeff"}
        cls.hasSurfImpact = {"identifier":"HasSurfaceImpactEffect","val":True}
        cls.surfImpactEff = {"identifier":"SurfaceImpactEffectName",
            "val":"surfimpacteff"}
        cls.surfImpactSnd = {"identifier":"SurfaceImpactSoundID",
            "val":"surfimpactsnd"}
        cls.launchSnd = {"identifier":"LaunchSoundID","val":"launchsnd"}
        cls.atmoEntrySnd = {"identifier":"AtmosphereEntrySoundID",
            "val":"atmoentrysnd"}
        cls.engineSound["identifier"] = "SpaceTravelSoundID"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scraft.EntryVehicle(
                filepath=self.filepath,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                mesh=self.meshCap,
                exhaustTexture=self.exhaustTextureCap,
                exhaustWidth=self.exhaustWidthCap,
                entityType=self.entityType,
                atmoEntryEff=self.atmoEntryEff,
                hasSurfImpact=self.hasSurfImpact,
                surfImpactEff=self.surfImpactEff,
                surfImpactSnd=self.surfImpactSnd,
                launchSnd=self.launchSnd,
                atmoEntrySnd=self.atmoEntrySnd
        )
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.exhaustTexture,"check",
                autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.exhaustSys,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.engineSound,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.atmoEntryEff,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockAtmoEntryEff = patcher.start()
        patcher = mock.patch.object(self.inst.surfImpactEff,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockSurfImpactEff = patcher.start()
        patcher = mock.patch.object(self.inst.surfImpactSnd,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockSurfImpactSnd = patcher.start()
        patcher = mock.patch.object(self.inst.launchSnd,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockLaunchSnd = patcher.start()
        patcher = mock.patch.object(self.inst.atmoEntrySnd,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockAtmoEntrySnd = patcher.start()
        self.mockProb = mock.MagicMock(probFile=None)
        self.mockProb.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = scraft.EntryVehicle.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problems in malformed files are reported
        """
        res = scraft.EntryVehicle.createInstance("EntryVehicleMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribAtmoEntryEff(self):
        """ Test whether atmoEntryEff attrib is created correctly
        """
        exp = par.ParticleRef(**self.atmoEntryEff)
        self.assertEqual(self.inst.atmoEntryEff,exp)

    def testCreationAttribHasSurfImpact(self):
        """ Test whether hasSurfImpact attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.hasSurfImpact)
        self.assertEqual(self.inst.hasSurfImpact,exp)

    def testCreationAttribsurfImpactEff(self):
        """ Test whether surfImpactEff attrib is created correctly
        """
        exp = par.ParticleRef(**self.surfImpactEff)
        self.assertEqual(self.inst.surfImpactEff,exp)

    def testCreationAttribSurfImpactSnd(self):
        """ Test whether surfImpactSnd attrib is created correctly
        """
        exp = audio.SoundRef(**self.surfImpactSnd)
        self.assertEqual(self.inst.surfImpactSnd,exp)

    def testCreationAttribLaunchSnd(self):
        """ Test whether launchSnd attrib is created correctly
        """
        exp = audio.SoundRef(**self.launchSnd)
        self.assertEqual(self.inst.launchSnd,exp)

    def testCreationAttribAtmoEntrySnd(self):
        """ Test whether atmoEntrySnd attrib is created correctly
        """
        exp = audio.SoundRef(**self.atmoEntrySnd)
        self.assertEqual(self.inst.atmoEntrySnd,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckAtmoEntryEff(self):
        """ Test whether check returns problems from atmoEntryEff attrib
        """
        self.mockAtmoEntryEff.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockAtmoEntryEff.assert_called_once_with(self.mmod,False)

    def testCheckSurfImpactEff(self):
        """ Test whether check returns problems from surfImpactEff attrib
        """
        self.mockSurfImpactEff.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockSurfImpactEff.assert_called_once_with(self.mmod,False)

    def testCheckSurfImpactSnd(self):
        """ Test whether check returns problems from surfImpactSnd attrib
        """
        self.mockSurfImpactSnd.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockSurfImpactSnd.assert_called_once_with(self.mmod,False)

    def testCheckLaunchSnd(self):
        """ Test whether check returns problems from launchSnd attrib
        """
        self.mockLaunchSnd.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockLaunchSnd.assert_called_once_with(self.mmod,False)

    def testCheckAtmoEntrySnd(self):
        """ Test whether check returns problems from atmoEntrySnd attrib
        """
        self.mockAtmoEntrySnd.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockAtmoEntrySnd.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Squad ##############################"""

class SquadTests(ControllableFixture,HasAttackBehaviorFixtures,unit.TestCase):
    desc = "Tests Squadron:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Squad.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Squad"}
        cls.fighterDef = {"identifier":"fighterEntityDef","val":"entfighter"}
        cls.fighterIllusionDef = {"identifier":"fighterIllusionEntityDef",
            "val":"illusion"}
        cls.scuttleTime = {"identifier":"scuttleTime","val":15.0}
        cls.fighterBuildTime = {"identifier":"fighterConstructionTime","val":33}
        cls.decayWithoutOwner = {"identifier":"decayWithoutOwnerRate","val":0.1}
        cls.numFighters = {"identifier":"baseMaxNumFighters","val":3}
        cls.role = {"identifier":"role","val":"Bomber"}
        cls.statcount = {"identifier":"statCountType","val":"SquadBomber"}
        cls.launchIcon = {"identifier":"launchIcon","val":"launchicon"}
        cls.dockIcon = {"identifier":"dockIcon","val":"dockicon"}
        cls.prereqs = {
            "identifier":"researchPrerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"research1"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"faction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":0}
        }
        cls.mmod = tco.genMockMod("./")
        cls.mmod.strings.checkString.return_value = []
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.audio.checkEffect.return_value = []

    def setUp(self):
        self.inst = scraft.Squad(
                filepath=self.filepath,
                entityType=self.entityType,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringLong,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                fighterDef=self.fighterDef,
                fighterIllusionDef=self.fighterIllusionDef,
                scuttleTime=self.scuttleTime,
                fighterBuildTime=self.fighterBuildTime,
                decayWithoutOwner=self.decayWithoutOwner,
                numFighters=self.numFighters,
                role=self.role,
                statcount=self.statcount,
                launchIcon=self.launchIcon,
                dockIcon=self.dockIcon,
                prereqs=self.prereqs
        )
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        patcher = mock.patch.object(self.inst.fighterDef,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockFighter = patcher.start()
        patcher = mock.patch.object(self.inst.fighterIllusionDef,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockFighterIllusion = patcher.start()
        patcher = mock.patch.object(self.inst.prereqs,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockResearch = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = scraft.Squad.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problems are returned from malformed file
        """
        res = scraft.Squad.createInstance("SquadMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribFighterDef(self):
        """ Test whether fighterDef attribute is created correctly
        """
        exp = coe.EntityRef(types=["Fighter"],**self.fighterDef)
        self.assertEqual(self.inst.fighterDef,exp)

    def testCreationAttribFighterIllusionDef(self):
        """ Test whether fighterIllusionDef attribute is created correctly
        """
        exp = coe.EntityRef(types=["Fighter"],**self.fighterIllusionDef)
        self.assertEqual(self.inst.fighterIllusionDef,exp)

    def testCreationAttribScuttleTime(self):
        """ Test whether scuttleTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.scuttleTime)
        self.assertEqual(self.inst.scuttleTime,exp)

    def testCreationAttribFighterBuildTime(self):
        """ Test whether fighterBuildTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.fighterBuildTime)
        self.assertEqual(self.inst.fighterBuildTime,exp)

    def testCreationAttribDecayWithoutOwner(self):
        """ Test whether decayWithoutOwner attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.decayWithoutOwner)
        self.assertEqual(self.inst.decayWithoutOwner,exp)

    def testCreationAttribNumFighters(self):
        """ Test whether numFighters attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.numFighters)
        self.assertEqual(self.inst.numFighters,exp)

    def testCreationAttribRole(self):
        """ Test whether role attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=scraft.SQUADRON_ROLES,
                **self.role)
        self.assertEqual(self.inst.role,exp)

    def testCreationAttribStatCount(self):
        """ Test whether statcount attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,
                **self.statcount)
        self.assertEqual(self.inst.statcount,exp)

    def testCreationAttribLaunchIcon(self):
        """ Test whether launchIcon attribute is created correctly
        """
        exp = ui.BrushRef(**self.launchIcon)
        self.assertEqual(self.inst.launchIcon,exp)

    def testCreationAttribDockIcon(self):
        """ Test whether dockIcon attribute is created correctly
        """
        exp = ui.BrushRef(**self.dockIcon)
        self.assertEqual(self.inst.dockIcon,exp)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################ Tests SpaceMine ############################"""

class SpaceMineTests(MovingFixtures,ShieldedFixtures,unit.TestCase):
    desc = "Tests SpaceMine:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "SpaceMine.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"SpaceMine"}
        cls.mesh = {"identifier":"MeshName","val":"mesh"}
        cls.spawnDelay = {"identifier":"spawnActivationDelay","val":30.0}
        cls.followRangeStart = {"identifier":"targetStartFollowRange",
            "val":0.0}
        cls.followRangeMax = {"identifier":"targetMaxFollowRange","val":0.0}
        cls.scuttleValue = {
            "identifier":"scuttleValue",
            "credits":{"identifier":"credits","val":100},
            "metal":{"identifier":"metal","val":200},
            "crystal":{"identifier":"crystal","val":300}
        }
        cls.scuttleTime = {"identifier":"baseScuttleTime","val":6.0}
        cls.exp = {"identifier":"experiencePointsForDestroying","val":10.0}
        cls.hullPointsMax["identifier"] = "maxHullPoints"
        cls.hullPointRestore["identifier"] = "hullPointRestoreRate"
        cls.shieldPointsMax["identifier"] = "maxShieldPoints"
        cls.shieldPointRestore["identifier"] = "shieldPointRestoreRate"
        cls.shieldMitigation["identifier"] = "maxShieldMitigation"
        cls.armorPoints["identifier"] = "armorPoints"
        cls.antimatterMax["identifier"] = "baseMaxAntiMatter"
        cls.antimatterRestore["identifier"] = "baseAntiMatterRestoreRate"
        cls.mmod = tco.genMockMod("./")
        cls.mmod.strings.checkString.return_value = []
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.audio.checkEffect.return_value = []

    def setUp(self):
        self.inst = scraft.SpaceMine(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                mass=self.mass,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                mesh=self.mesh,
                spawnDelay=self.spawnDelay,
                followRangeStart=self.followRangeStart,
                followRangeMax=self.followRangeMax,
                scuttleValue=self.scuttleValue,
                scuttleTime=self.scuttleTime,
        )
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = scraft.SpaceMine.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationAttribMesh(self):
        """ Test whether mesh attrib is created correctly
        """
        exp = meshes.MeshRef(**self.mesh)
        self.assertEqual(self.inst.mesh,exp)

    def testCreationAttribSpawnDelay(self):
        """ Test whether spawnDelay attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnDelay)
        self.assertEqual(self.inst.spawnDelay,exp)

    def testCreationAttribFollowRangeStart(self):
        """ Test whether followRangeStart attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.followRangeStart)
        self.assertEqual(self.inst.followRangeStart,exp)

    def testCreationAttribFollowRangeMax(self):
        """ Test whether followRangeMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.followRangeMax)
        self.assertEqual(self.inst.followRangeMax,exp)

    def testCreationAttribScuttleValue(self):
        """ Test whether scuttleValue attrib is created correctly
        """
        exp = coe.Cost(**self.scuttleValue)
        self.assertEqual(self.inst.scuttleValue,exp)

    def testCreationAttribScuttleTime(self):
        """ Test whether scuttleTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.scuttleTime)
        self.assertEqual(self.inst.scuttleTime,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        for prob in res:
            print(prob.toString(0))
        self.assertEqual(res,[])

    def testCheckMesh(self):
        """ Test whether check returns problems from mesh correctly
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mockMesh.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mockProb])
        self.mockMesh.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests Fighter #############################"""

class FighterTests(SmallcraftFixture,ArmedFixtures,unit.TestCase):
    desc = "Tests Fighter:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Fighter.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Fighter"}
        cls.formationOffset = {"identifier":"formationOffsetDistance",
            "val":50.0}
        cls.statcount = {"identifier":"statCountType","val":"FighterCombat"}
        cls.maxMitigation = {"identifier":"maxMitigation","val":0.6}
        cls.exp = {"identifier":"experiencePointsForDestroying","val":10.0}
        cls.hullPointsMax["identifier"] = "maxHullPoints"
        cls.hullPointRestore["identifier"] = "hullPointRestoreRate"
        cls.armorPoints["identifier"] = "armorPoints"
        cls.descString["identifier"] = "counterDescriptionStringID"
        cls.mesh = {"identifier":"meshName","val":"mesh"}
        cls.exhaustSys["identifier"] = "exhaustParticleSystemName"
        cls.exhaustTextureCap["identifier"] = "exhaustTrailTextureName"
        cls.exhaustWidthCap["identifier"] = "exhaustTrailWidth"
        cls.engineSound["identifier"] = "engineSoundID"
        cls.mmod = tco.genMockMod("./")
        cls.mmod.strings.checkString.return_value = []
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.audio.checkEffect.return_value = []

    def setUp(self):
        self.inst = scraft.Fighter(
                filepath=self.filepath,
                entityType=self.entityType,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                mesh=self.mesh,
                exhaustTexture=self.exhaustTextureCap,
                exhaustWidth=self.exhaustWidthCap,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                formationOffset=self.formationOffset,
                statcount=self.statcount,
                maxMitigation=self.maxMitigation
        )
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        patcher = mock.patch.object(self.inst.exhaustTexture,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockExhaustTexture = patcher.start()
        patcher = mock.patch.object(self.inst.exhaustSys,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.engineSound,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockWeapons = patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExplosion = patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDebris = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = scraft.Fighter.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationAttribFormationOffset(self):
        """ Test whether formationOffset attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.formationOffset)
        self.assertEqual(self.inst.formationOffset,exp)

    def testCreationAttribStatcount(self):
        """ Test whether statcount attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,**self.statcount)
        self.assertEqual(self.inst.statcount,exp)

    def testCreationAttribMaxMitigation(self):
        """ Test whether maxMitigation attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxMitigation)
        self.assertEqual(self.inst.maxMitigation,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)
