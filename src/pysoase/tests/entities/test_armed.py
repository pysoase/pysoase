""" Tests for the pysoase.entities.armed module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_ecommon import MainViewVisibleFixtures, \
    UIVisibleFixtures
from pysoase.tests.entities.test_hitable import ShieldedFixtures, \
    HasShieldVisualsFixtures,HitableFixtures

testdata = os.path.join("entities","armed")

"""####################### Tests SharedWeaponEffect ########################"""

class SharedWeaponEffectFixture(object):
    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "\tburstCount 1\n"
            +"\tburstDelay 0.000000\n"
            +"\tfireDelay 0.000000\n"
            +"\tmuzzleEffectName \"effmuzzle\"\n"
            +"\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\tmuzzleSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndmuzzle\"\n"
            +"\thitEffectName \"effhit\"\n"
            +"\thitHullEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndhullhit\"\n"
            +"\thitShieldsEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndshieldhit\"\n"
        )
        cls.identifier = "WeaponEffects"
        cls.effectType = {"identifier":"weaponType","val":"Projectile"}
        cls.burstCount = {"identifier":"burstCount","val":1}
        cls.burstDelay = {"identifier":"burstDelay","val":0.0}
        cls.fireDelay = {"identifier":"fireDelay","val":0.0}
        cls.muzzleEff = {"identifier":"muzzleEffectName","val":"effmuzzle"}
        cls.sndMuzzleRespawn = {"identifier":"muzzleSoundMinRespawnTime",
            "val":0.1}
        cls.sndMuzzle = {
            "identifier":"muzzleSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"sndmuzzle"}
            ]
        }
        cls.hitEff = {"identifier":"hitEffectName","val":"effhit"}
        cls.sndHitHull = {
            "identifier":"hitHullEffectSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"sndhullhit"}
            ]
        }
        cls.sndHitShields = {
            "identifier":"hitShieldsEffectSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"sndshieldhit"}
            ]
        }

class SharedWeaponEffectTests(SharedWeaponEffectFixture,unit.TestCase):
    desc = "Tests SharedWeaponEffect:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "WeaponEffects\n"
            +"\tweaponType \"Projectile\"\n"
            +cls.parseString
        )
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.SharedWeaponEffect(
                identifier=self.identifier,
                effectType=self.effectType,
                burstCount=self.burstCount,
                burstDelay=self.burstDelay,
                fireDelay=self.fireDelay,
                muzzleEff=self.muzzleEff,
                sndMuzzleRespawn=self.sndMuzzleRespawn,
                sndMuzzle=self.sndMuzzle,
                hitEff=self.hitEff,
                sndHitHull=self.sndHitHull,
                sndHitShields=self.sndHitShields
        )
        patcher = mock.patch.object(self.inst.muzzleEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mMuzzleEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndMuzzle,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndMuzzle = patcher.start()
        patcher = mock.patch.object(self.inst.hitEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mHitEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitHull,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitHull = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitShields,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitShields = patcher.start()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genHeading("WeaponEffects")
            +co_parse.genKeyValPair("weaponType","Projectile")("effectType")
            +armed.g_sharedWeaponsEff
        )
        parseRes = parser.parseString(self.parseString)
        res = armed.SharedWeaponEffect(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid effectType
        """
        invalid = {"identifier":"weaponType","val":"foo"}
        with self.assertRaises(RuntimeError):
            armed.SharedWeaponEffect.factory(effectType=invalid)

    def testCreationAttribEffectType(self):
        """ Test whether effectType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.WEAPON_TYPES,**self.effectType)
        self.assertEqual(self.inst.effectType,exp)

    def testCreationAttribBurstCount(self):
        """ Test whether burstCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.burstCount)
        self.assertEqual(self.inst.burstCount,exp)

    def testCreationAttribBurstDelay(self):
        """ Test whether burstDelay attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.burstDelay)
        self.assertEqual(self.inst.burstDelay,exp)

    def testCreationAttribFireDelay(self):
        """ Test whether fireDelay attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.fireDelay)
        self.assertEqual(self.inst.fireDelay,exp)

    def testCreationAttribMuzzleEff(self):
        """ Test whether muzzleEff attrib is created correctly
        """
        exp = par.ParticleRef(**self.muzzleEff)
        self.assertEqual(self.inst.muzzleEff,exp)

    def testCreationAttribSndMuzzleRespawn(self):
        """ Test whether sndMuzzleRespawn attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.sndMuzzleRespawn)
        self.assertEqual(self.inst.sndMuzzleRespawn,exp)

    def testCreationAttribSndMuzzle(self):
        """ Test whether sndMuzzle attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.sndMuzzle)
        self.assertEqual(self.inst.sndMuzzle,exp)

    def testCreationAttribSndHitHull(self):
        """ Test whether sndHitHull attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.sndHitHull)
        self.assertEqual(self.inst.sndHitHull,exp)

    def testCreationAttribSndHitShields(self):
        """ Test whether sndHitShields attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.sndHitShields)
        self.assertEqual(self.inst.sndHitShields,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMuzzleEff(self):
        """ Test whether problems in muzzleEff attrib are returned correctly
        """
        self.mMuzzleEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckSndMuzzle(self):
        """ Test whether problems in sndMuzzle attrib are returned correctly
        """
        self.mSndMuzzle.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckSndHitHull(self):
        """ Test whether problems in sndHitHull attrib are returned correctly
        """
        self.mSndHitHull.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckSndHitShields(self):
        """ Test whether problems in sndHitShields attrib are returned correctly
        """
        self.mSndHitShields.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ProjectileWeaponEffectTests(SharedWeaponEffectFixture,unit.TestCase):
    desc = "Tests ProjectileWeaponEff:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "WeaponEffects\n"
            +"\tweaponType \"Projectile\"\n"
            +cls.parseString
            +"\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.mmod = tco.genMockMod("./")
        cls.travelEff = {"identifier":"projectileTravelEffectName",
            "val":"traveleff"}

    def setUp(self):
        self.inst = armed.ProjectileWeaponEff(
                identifier=self.identifier,
                effectType=self.effectType,
                burstCount=self.burstCount,
                burstDelay=self.burstDelay,
                fireDelay=self.fireDelay,
                muzzleEff=self.muzzleEff,
                sndMuzzleRespawn=self.sndMuzzleRespawn,
                sndMuzzle=self.sndMuzzle,
                hitEff=self.hitEff,
                sndHitHull=self.sndHitHull,
                sndHitShields=self.sndHitShields,
                travelEff=self.travelEff
        )
        patcher = mock.patch.object(self.inst.muzzleEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mMuzzleEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndMuzzle,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndMuzzle = patcher.start()
        patcher = mock.patch.object(self.inst.hitEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mHitEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitHull,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitHull = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitShields,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitShields = patcher.start()
        patcher = mock.patch.object(self.inst.travelEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mTravelEff = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_weaponEffect.parseString(self.parseString)[0]
        res = armed.SharedWeaponEffect.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTravelEff(self):
        """ Test whether travelEff attrib is created correctly
        """
        exp = par.ParticleRef(**self.travelEff)
        self.assertEqual(self.inst.travelEff,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMuzzleEff(self):
        """ Test whether problems in muzzleEff attrib are returned correctly
        """
        self.mTravelEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class MissileWeaponEffectTests(SharedWeaponEffectFixture,unit.TestCase):
    desc = "Tests MissileWeaponEffect:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "WeaponEffects\n"
            +"\tweaponType \"Missile\"\n"
            +cls.parseString
            +"\tmissileTravelEffectName \"traveleff\"\n"
            +"\tmissileStartTurningDistance 50.000000\n"
            +"\tmissileSlowTurnRate 0.400000\n"
            +"\tmissileMaxSlowTurnTime 10.000000\n"
        )
        cls.effectType["val"] = "Missile"
        cls.mmod = tco.genMockMod("./")
        cls.travelEff = {"identifier":"missileTravelEffectName",
            "val":"traveleff"}
        cls.turnDist = {"identifier":"missileStartTurningDistance","val":50.0}
        cls.turnRate = {"identifier":"missileSlowTurnRate","val":0.4}
        cls.maxTurnTime = {"identifier":"missileMaxSlowTurnTime","val":10.0}

    def setUp(self):
        self.inst = armed.MissileWeaponEffect(
                identifier=self.identifier,
                effectType=self.effectType,
                burstCount=self.burstCount,
                burstDelay=self.burstDelay,
                fireDelay=self.fireDelay,
                muzzleEff=self.muzzleEff,
                sndMuzzleRespawn=self.sndMuzzleRespawn,
                sndMuzzle=self.sndMuzzle,
                hitEff=self.hitEff,
                sndHitHull=self.sndHitHull,
                sndHitShields=self.sndHitShields,
                travelEff=self.travelEff,
                turnDist=self.turnDist,
                turnRate=self.turnRate,
                maxTurnTime=self.maxTurnTime
        )
        patcher = mock.patch.object(self.inst.muzzleEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mMuzzleEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndMuzzle,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndMuzzle = patcher.start()
        patcher = mock.patch.object(self.inst.hitEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mHitEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitHull,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitHull = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitShields,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitShields = patcher.start()
        patcher = mock.patch.object(self.inst.travelEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mTravelEff = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_weaponEffect.parseString(self.parseString)[0]
        res = armed.SharedWeaponEffect.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTravelEff(self):
        """ Test whether travelEff attrib is created correctly
        """
        exp = par.ParticleRef(**self.travelEff)
        self.assertEqual(self.inst.travelEff,exp)

    def testCreationAttribTurnDist(self):
        """ Test whether turnDist attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.turnDist)
        self.assertEqual(self.inst.turnDist,exp)

    def testCreationAttribTurnRate(self):
        """ Test whether turnRate attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.turnRate)
        self.assertEqual(self.inst.turnRate,exp)

    def testCreationAttribMaxTurnTime(self):
        """ Test whether turnRate attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxTurnTime)
        self.assertEqual(self.inst.maxTurnTime,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckTravelEff(self):
        """ Test whether problems in travelEff attrib are returned correctly
        """
        self.mTravelEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BeamWeaponEffectTests(SharedWeaponEffectFixture,unit.TestCase):
    desc = "Tests BeamWeaponEffect:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "WeaponEffects\n"
            +"\tweaponType \"Beam\"\n"
            +cls.parseString
            +"\tbeamEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"beamsound\"\n"
            +"\tbeamGlowTextureName \"beamglow\"\n"
            +"\tbeamCoreTextureName \"beamcore\"\n"
            +"\tbeamWidth 5.000000\n"
            +"\tbeamGlowColor ffffffff\n"
            +"\tbeamCoreColor ffffffff\n"
            +"\tbeamTilingRate 3.000000\n"
        )
        cls.effectType["val"] = "Beam"
        cls.sndBeam = {
            "identifier":"beamEffectSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"beamsound"}
            ]
        }
        cls.textureGlow = {"identifier":"beamGlowTextureName","val":"beamglow"}
        cls.textureCore = {"identifier":"beamCoreTextureName","val":"beamcore"}
        cls.width = {"identifier":"beamWidth","val":5.0}
        cls.colorGlow = {"identifier":"beamGlowColor","val":"ffffffff"}
        cls.colorCore = {"identifier":"beamCoreColor","val":"ffffffff"}
        cls.tilingRate = {"identifier":"beamTilingRate","val":3.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.BeamWeaponEffect(
                identifier=self.identifier,
                effectType=self.effectType,
                burstCount=self.burstCount,
                burstDelay=self.burstDelay,
                fireDelay=self.fireDelay,
                muzzleEff=self.muzzleEff,
                sndMuzzleRespawn=self.sndMuzzleRespawn,
                sndMuzzle=self.sndMuzzle,
                hitEff=self.hitEff,
                sndHitHull=self.sndHitHull,
                sndHitShields=self.sndHitShields,
                sndBeam=self.sndBeam,
                textureGlow=self.textureGlow,
                textureCore=self.textureCore,
                width=self.width,
                colorGlow=self.colorGlow,
                colorCore=self.colorCore,
                tilingRate=self.tilingRate
        )
        patcher = mock.patch.object(self.inst.muzzleEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mMuzzleEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndMuzzle,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndMuzzle = patcher.start()
        patcher = mock.patch.object(self.inst.hitEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mHitEff = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitHull,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitHull = patcher.start()
        patcher = mock.patch.object(self.inst.sndHitShields,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndHitShields = patcher.start()
        patcher = mock.patch.object(self.inst.sndBeam,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mSndBeam = patcher.start()
        patcher = mock.patch.object(self.inst.textureGlow,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mTextureGlow = patcher.start()
        patcher = mock.patch.object(self.inst.textureCore,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mTextureCore = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_weaponEffect.parseString(self.parseString)[0]
        res = armed.SharedWeaponEffect.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSndBeam(self):
        """ Test whether sndBeam attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.sndBeam)
        self.assertEqual(self.inst.sndBeam,exp)

    def testCreationAttribTextureGlow(self):
        """ Test whether textureGlow attrib is created correctly
        """
        exp = ui.UITextureRef(**self.textureGlow)
        self.assertEqual(self.inst.textureGlow,exp)

    def testCreationAttribTextureCore(self):
        """ Test whether textureCore attrib is created correctly
        """
        exp = ui.UITextureRef(**self.textureCore)
        self.assertEqual(self.inst.textureCore,exp)

    def testCreationAttribWidth(self):
        """ Test whether width attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.width)
        self.assertEqual(self.inst.width,exp)

    def testCreationAttribColorGlow(self):
        """ Test whether width colorGlow is created correctly
        """
        exp = co_attribs.AttribColor(**self.colorGlow)
        self.assertEqual(self.inst.colorGlow,exp)

    def testCreationAttribColorCore(self):
        """ Test whether width colorCore is created correctly
        """
        exp = co_attribs.AttribColor(**self.colorCore)
        self.assertEqual(self.inst.colorCore,exp)

    def testCreationAttribTilingRate(self):
        """ Test whether width tilingRate is created correctly
        """
        exp = co_attribs.AttribNum(**self.tilingRate)
        self.assertEqual(self.inst.tilingRate,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testChecksndBeam(self):
        """ Test whether problems in sndBeam attrib are returned correctly
        """
        self.mSndBeam.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckTextureGlow(self):
        """ Test whether problems in textureGlow attrib are returned correctly
        """
        self.mTextureGlow.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckTextureCore(self):
        """ Test whether problems in textureCore attrib are returned correctly
        """
        self.mTextureCore.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests HasAttackBehavior #########################"""

class HasAttackBehaviorFixtures(UIVisibleFixtures,MainViewVisibleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"defaultAutoAttackRange \"None\"\n"
            +"defaultAutoAttackOn TRUE\n"
            +"prefersToFocusFire TRUE\n"
            +"usesFighterAttack FALSE\n"
        )
        cls.autoattackRange = {"identifier":"defaultAutoAttackRange",
            "val":"None"}
        cls.autoattackOn = {"identifier":"defaultAutoAttackOn","val":True}
        cls.focusFire = {"identifier":"prefersToFocusFire","val":True}
        cls.usesFighterAttack = {"identifier":"usesFighterAttack","val":False}

class HasAttackBehaviorTests(HasAttackBehaviorFixtures,unit.TestCase):
    desc = "Tests HasAttackBehavior:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = armed.HasAttackBehavior(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_zoomDist
            +coe.g_shadows
            +coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +armed.g_attackBehaviour
        )
        parseRes = parser.parseString(self.parseString)
        res = armed.HasAttackBehavior(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAutoattackRange(self):
        """ Test whether autoattackRange attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.AUTOATTACK_RANGES,
                **self.autoattackRange)
        self.assertEqual(self.inst.autoattackRange,exp)

    def testCreationAttribAutoattackOn(self):
        """ Test whether autoattackOn attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.autoattackOn)
        self.assertEqual(self.inst.autoattackOn,exp)

    def testCreationAttribFocusFire(self):
        """ Test whether focusFire attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.focusFire)
        self.assertEqual(self.inst.focusFire,exp)

    def testCreationAttribUsesFighterAttack(self):
        """ Test whether usesFighterAttack attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.usesFighterAttack)
        self.assertEqual(self.inst.usesFighterAttack,exp)

"""############################# Tests LvlReq ##############################"""

class LvlReqTests(unit.TestCase):
    desc = "Tests LvlReq:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "levelRequirement\n"
            +"\tupgradePropertyType \"CultureMeshLevel\"\n"
            +"\tupgradeLevel 2\n"
        )
        cls.mmod = tco.genMockMod("./")
        cls.identifier = "levelRequirement"
        cls.upgradeType = {"identifier":"upgradePropertyType",
            "val":"CultureMeshLevel"}
        cls.upgradeLevel = {"identifier":"upgradeLevel","val":2}

    def setUp(self):
        self.inst = armed.LvlReq(
                identifier=self.identifier,
                upgradeType=self.upgradeType,
                upgradeLevel=self.upgradeLevel
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_lvlRequirement.parseString(self.parseString)[0]
        res = armed.LvlReq(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribUpgradeType(self):
        """ Test whether upgradeType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.MESH_LVL_UPG_TYPES,
                **self.upgradeType)
        self.assertEqual(self.inst.upgradeType,exp)

    def testCreationAttribUpgradeLevel(self):
        """ Test whether upgradeLevel attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.upgradeLevel)
        self.assertEqual(self.inst.upgradeLevel,exp)

    def testCheck(self):
        """ Test whether check returns an empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests MeshConstraint ##########################"""

class MeshConstraintTests(unit.TestCase):
    desc = "Tests MeshConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "criteriaType \"None\"\n"
        )
        cls.conType = {"identifier":"criteriaType","val":"None"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.MeshConstraint(
                conType=self.conType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genKeyValPair("criteriaType","None")("conType"
        ).parseString(self.parseString)
        res = armed.MeshConstraint.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether call of factory with invalid type raises error
        """
        invalid = {"identifier":"foo","val":"INVALID"}
        with self.assertRaises(RuntimeError):
            res = armed.MeshConstraint.factory(conType=invalid)

    def testCreationAttribConType(self):
        """ Test whether conType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.MeshConstraint._conTypes,
                **self.conType)
        self.assertEqual(self.inst.conType,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests MeshResearchConstraint ######################"""

class MeshResearchConstraintTests(unit.TestCase):
    desc = "Tests MeshResearchConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "criteriaType \"Research\"\n"
            +"prerequisites\n"
            +"\tNumResearchPrerequisites 1\n"
            +"\tResearchPrerequisite\n"
            +"\t\tSubject \"Testsubject\"\n"
            +"\t\tLevel 2\n"
            +"\tRequiredFactionNameID \"Testfaction\"\n"
            +"\tRequiredCompletedResearchSubjects 2\n"
        )
        cls.conType = {"identifier":"criteriaType","val":"Research"}
        cls.prereqs = {
            "identifier":"prerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"Testsubject"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"Testfaction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":2}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.MeshResearchConstraint(
                conType=self.conType,
                prereqs=self.prereqs
        )
        patcher = mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockPrereqs = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_researchCriterion.parseString(self.parseString)[0]
        res = armed.MeshConstraint.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckPrereqs(self):
        """ Test whether check returns problems from prereqs attrib
        """
        self.mockPrereqs.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests MeshSBConstraint #########################"""

class MeshSBConstraintTests(unit.TestCase):
    desc = "Tests MeshSBConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "criteriaType \"StarBaseUpgradeLevel\"\n"
            +"levelRequirements\n"
            +"\tlevelRequirementCount 1\n"
            +"\tlevelRequirement\n"
            +"\t\tupgradePropertyType \"CultureMeshLevel\"\n"
            +"\t\tupgradeLevel 2\n"
        )
        cls.mmod = tco.genMockMod("./")
        cls.conType = {"identifier":"criteriaType","val":"StarBaseUpgradeLevel"}
        cls.reqs = {
            "identifier":"levelRequirements",
            "counter":{"identifier":"levelRequirementCount","val":1},
            "elements":[
                {
                    "identifier":"levelRequirement",
                    "upgradeType":{"identifier":"upgradePropertyType",
                        "val":"CultureMeshLevel"},
                    "upgradeLevel":{"identifier":"upgradeLevel","val":2}
                }
            ]
        }

    def setUp(self):
        self.inst = armed.MeshSBConstraint(
                conType=self.conType,
                reqs=self.reqs
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_lvlCriterion.parseString(self.parseString)[0]
        res = armed.MeshConstraint.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribReqs(self):
        """ Test whether reqs attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=armed.LvlReq,
                **self.reqs
        )
        self.assertEqual(self.inst.reqs,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests MeshInfoConstruct ########################"""

class MeshInfoConstructTests(unit.TestCase):
    desc = "Tests MeshInfoConstruct:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "MeshNameInfo\n"
            +"\tmeshName \"testmesh\"\n"
            +"\tcriteriaType \"None\"\n"
        )
        cls.identifier = "MeshNameInfo"
        cls.mesh = {"identifier":"meshName","val":"testmesh"}
        cls.criterion = {
            "conType":{"identifier":"criteriaType","val":"None"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.MeshInfoConstruct(
                identifier=self.identifier,
                mesh=self.mesh,
                criterion=self.criterion
        )
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_meshInfoCriterion.parseString(self.parseString)[0]
        res = armed.MeshInfoConstruct(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribConType(self):
        """ Test whether conType attrib is created correctly
        """
        exp = armed.MeshConstraint(**self.criterion)
        self.assertEqual(self.inst.criterion,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests Constructable ##########################"""

class ConstructableFixtures(ShieldedFixtures,HasShieldVisualsFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"armorType \"Titan\"\n"
            +"BaseArmorPoints 12.0\n"
            +"ExplosionName \"testexplosion\"\n"
            +"experiencePointsForDestroying 10.0\n"
            +"HullPointRestoreRate 1.0\n"
            +"MaxHullPoints 2.0\n"
            +"numRandomDebrisLarge 1.0\n"
            +"numRandomDebrisSmall 1.0\n"
            +"numSpecificDebris 1\n"
            +"\tspecificDebrisMeshName \"debris\"\n"
            +"mainViewIcon \"mainviewicon\"\n"
            +"picture \"picture\"\n"
            +"ability:0 \"ab0\"\n"
            +"ability:1 \"ab1\"\n"
            +"ability:2 \"ab2\"\n"
            +"ability:3 \"ab3\"\n"
            +"ability:4 \"ab4\"\n"
            +"AntiMatterRestoreRate 50.0\n"
            +"MaxAntiMatter 100.0\n"
            +"NumSoundsFor:ONATTACKORDERISSUED 1\n"
            +"SoundID \"attackorder\"\n"
            +"NumSoundsFor:ONCREATION 1\n"
            +"SoundID \"creation\"\n"
            +"NumSoundsFor:ONGENERALORDERISSUED 1\n"
            +"SoundID \"generalorder\"\n"
            +"NumSoundsFor:ONSELECTED 1\n"
            +"SoundID \"selected\"\n"
            +"NumSoundsFor:ONSTARTPHASEJUMP 1\n"
            +"SoundID \"phasejump\"\n"
            +"MaxShieldPoints 12.0\n"
            +"ShieldPointRestoreRate 17.0\n"
            +"maxShieldMitigation 20.0\n"
            +"ShieldMeshName \"testshield\"\n"
            +"renderShield TRUE\n"
            +"basePrice\n"
            +"\tcredits 100\n"
            +"\tmetal 200\n"
            +"\tcrystal 300\n"
            +"formationRank 2\n"
            +"meshNameIncreasedEffectName \"incmesh\"\n"
            +"meshNameDecreasedEffectName \"decmesh\"\n"
            +"MeshNameInfoCount 1\n"
            +"MeshNameInfo\n"
            +"\tmeshName \"testmesh\"\n"
            +"\tcriteriaType \"None\"\n"
            +"Prerequisites\n"
            +"\tNumResearchPrerequisites 1\n"
            +"\tResearchPrerequisite\n"
            +"\t\tSubject \"Testsubject\"\n"
            +"\t\tLevel 2\n"
            +"\tRequiredFactionNameID \"Testfaction\"\n"
            +"\tRequiredCompletedResearchSubjects 2\n"
            +"statCountType \"ModulePointDefense\"\n"
            +"baseBuildTime 6.0\n"
        )
        cls.price = {
            "identifier":"basePrice",
            "credits":{"identifier":"credits","val":100},
            "metal":{"identifier":"metal","val":200},
            "crystal":{"identifier":"crystal","val":300}
        }
        cls.formationRank = {"identifier":"formationRank","val":2}
        cls.meshIncreasedEffect = {"identifier":"meshNameIncreasedEffectName",
            "val":"incmesh"}
        cls.meshDecreasedEffect = {"identifier":"meshNameDecreasedEffectName",
            "val":"decmesh"}
        cls.meshes = {
            "counter":{"identifier":"MeshNameInfoCount","val":1},
            "elements":[
                {
                    "identifier":"MeshNameInfo",
                    "mesh":{"identifier":"meshName","val":"testmesh"},
                    "criterion":{
                        "conType":{"identifier":"criteriaType","val":"None"}
                    }
                }
            ]
        }
        cls.prereqs = {
            "identifier":"Prerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"Testsubject"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"Testfaction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":2}
        }
        cls.statcount = {"identifier":"statCountType",
            "val":"ModulePointDefense"}
        cls.buildTime = {"identifier":"baseBuildTime","val":6.0}
        cls.mmod = tco.genMockMod("./")

class ConstructableTests(ConstructableFixtures,unit.TestCase):
    desc = "Tests Constructable"

    def setUp(self):
        self.inst = armed.Constructable(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime
        )
        patchers = []
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
            patchers.append(patcher)
        patcher = mock.patch.object(self.inst.shieldMesh,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshDecreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDecEff = patcher.start()
        patcher = mock.patch.object(self.inst.meshIncreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockIncEff = patcher.start()
        patcher = mock.patch.object(self.inst.meshes,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockMeshes = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_zoomDist
            +coe.g_shadows
            +hit.g_armorType
            +hit.g_baseArmorPoints
            +hit.g_explosion
            +hit.g_exp
            +hit.g_hullRestore
            +hit.g_hullPoints
            +coe.g_debris
            +coe.g_mainViewIcon
            +coe.g_picture
            +coe.g_abilities
            +coe.g_antimatterRestore
            +coe.g_maxAntimatter
            +hit.g_voiceSounds
            +hit.g_maxShield
            +hit.g_shieldRestore
            +hit.g_maxShieldMitigation
            +hit.g_shieldVisuals
            +coe.g_basePrice
            +armed.g_formation
            +armed.g_meshEffects
            +armed.g_criterionMeshInfos
            +coe.g_prereqs
            +armed.g_statType
            +armed.g_baseBuildTime
        )
        parseRes = parser.parseString(self.parseString)
        res = armed.Constructable(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrice(self):
        """ Test whether price attrib is created correctly
        """
        exp = coe.Cost(**self.price)
        self.assertEqual(self.inst.price,exp)

    def testCreationAttribFormationRank(self):
        """ Test whether formationRank attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.formationRank)
        self.assertEqual(self.inst.formationRank,exp)

    def testCreationAttribMeshDecreasedEffect(self):
        """ Test whether meshDecreaseEffect attrib is created correctly
        """
        exp = meshes.MeshRef(**self.meshDecreasedEffect)
        self.assertEqual(self.inst.meshDecreasedEffect,exp)

    def testCreationAttribMeshIncreasedEffect(self):
        """ Test whether meshIncreasedEffect attrib is created correctly
        """
        exp = meshes.MeshRef(**self.meshIncreasedEffect)
        self.assertEqual(self.inst.meshIncreasedEffect,exp)

    def testCreationAttribMeshes(self):
        """ Test whether meshes attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=armed.MeshInfoConstruct,
                **self.meshes)
        self.assertEqual(self.inst.meshes,exp)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attrib is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCreationAttribStatcount(self):
        """ Test whether statcount attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.STATCOUNT_TYPES,**self.statcount)
        self.assertEqual(self.inst.statcount,exp)

    def testCreationAttribBuildTime(self):
        """ Test whether buildTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.buildTime)
        self.assertEqual(self.inst.buildTime,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMeshDecreasedEffect(self):
        """ Test whether check returns problems from meshDecreaseEffect attrib
        """
        self.mockDecEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckMeshIncreasedEffect(self):
        """ Test whether check returns problems from meshIncreasedEffect attrib
        """
        self.mockIncEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckMeshes(self):
        """ Test whether check returns problems from meshes attrib
        """
        self.mockMeshes.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

"""######################### Tests FireConstraint ##########################"""

class FireConstraintTests(unit.TestCase):
    desc = "Tests FireConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "fireConstraintType \"CanAlwaysFire\"\n"
        )
        cls.conType = {"identifier":"fireConstraintType","val":"CanAlwaysFire"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.FireConstraint(
                conType=self.conType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genKeyValPair("fireConstraintType","CanAlwaysFire")(
                "conType"
        ).parseString(self.parseString)
        res = armed.FireConstraint.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether call of factory with invalid type raises error
        """
        invalid = {"identifier":"foo","val":"INVALID"}
        with self.assertRaises(RuntimeError):
            res = armed.FireConstraint.factory(conType=invalid)

    def testCreationAttribConType(self):
        """ Test whether conType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.FireConstraint._conTypes,
                **self.conType)
        self.assertEqual(self.inst.conType,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests FireResearchConstraint ######################"""

class FireResearchConstraintTests(unit.TestCase):
    desc = "Tests FireResearchConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "fireConstraintType \"Research\"\n"
            +"researchPrerequisites\n"
            +"\tNumResearchPrerequisites 1\n"
            +"\tResearchPrerequisite\n"
            +"\t\tSubject \"Testsubject\"\n"
            +"\t\tLevel 2\n"
            +"\tRequiredFactionNameID \"Testfaction\"\n"
            +"\tRequiredCompletedResearchSubjects 2\n"
        )
        cls.conType = {"identifier":"fireConstraintType","val":"Research"}
        cls.prereqs = {
            "identifier":"researchPrerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"Testsubject"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"Testfaction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":2}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.FireResearchConstraint(
                conType=self.conType,
                prereqs=self.prereqs
        )
        patcher = mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockPrereqs = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_fireResearchConstraint.parseString(self.parseString)[
            0]
        res = armed.FireConstraint.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckPrereqs(self):
        """ Test whether check returns problems from prereqs attrib
        """
        self.mockPrereqs.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests FireSBConstraint #########################"""

class FireSBConstraintTests(unit.TestCase):
    desc = "Tests FireSBConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "fireConstraintType \"StarBaseUpgradeLevel\"\n"
            +"requiredStarBaseWeaponLevel 2\n"
        )
        cls.mmod = tco.genMockMod("./")
        cls.conType = {"identifier":"fireConstraintType",
            "val":"StarBaseUpgradeLevel"}
        cls.reqLvl = {"identifier":"requiredStarBaseWeaponLevel","val":2}

    def setUp(self):
        self.inst = armed.FireSBConstraint(
                conType=self.conType,
                reqLvl=self.reqLvl
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_fireSBUpgradeConstraint.parseString(
                self.parseString)[0]
        res = armed.FireConstraint.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribReqLvl(self):
        """ Test whether reqLvl attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.reqLvl)
        self.assertEqual(self.inst.reqLvl,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests DamageEnums ###########################"""

class DamageEnumsTests(unit.TestCase):
    desc = "Tests DamageEnums:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "damageEnums\n"
            +"\tAttackType \"TITAN\"\n"
            +"\tDamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\tDamageApplyType \"OVERTIME\"\n"
            +"\tDamageType \"PHYSICAL\"\n"
            +"\tWeaponClassType \"BEAM\"\n"
        )
        cls.identifier = "damageEnums"
        cls.attackType = {"identifier":"AttackType","val":"TITAN"}
        cls.dmgAffects = {"identifier":"DamageAffectType",
            "val":"AFFECTS_ONLY_HULL"}
        cls.dmgApply = {"identifier":"DamageApplyType","val":"OVERTIME"}
        cls.dmgType = {"identifier":"DamageType","val":"PHYSICAL"}
        cls.weaponClass = {"identifier":"WeaponClassType","val":"BEAM"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.DamageEnums(
                identifier=self.identifier,
                attackType=self.attackType,
                dmgAffects=self.dmgAffects,
                dmgApply=self.dmgApply,
                dmgType=self.dmgType,
                weaponClass=self.weaponClass
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_damageEnums.parseString(self.parseString)[0]
        res = armed.DamageEnums(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAttackType(self):
        """ Test whether attackType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.ATTACK_TYPES,**self.attackType)
        self.assertEqual(self.inst.attackType,exp)

    def testCreationAttribDmgAffects(self):
        """ Test whether dmgAffects attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,**self.dmgAffects)
        self.assertEqual(self.inst.dmgAffects,exp)

    def testCreationAttribDmgApply(self):
        """ Test whether dmgApply attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_APPLY,**self.dmgApply)
        self.assertEqual(self.inst.dmgApply,exp)

    def testCreationAttribDmgType(self):
        """ Test whether dmgType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,**self.dmgType)
        self.assertEqual(self.inst.dmgType,exp)

    def testCreationAttribWeaponClass(self):
        """ Test whether weaponClass attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.WEAPON_CLASSES,**self.weaponClass)
        self.assertEqual(self.inst.weaponClass,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests Weapon ##############################"""

class WeaponTests(unit.TestCase):
    desc = "Tests Weapon:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "Weapon\n"
            +"\tWeaponType \"Beam\"\n"
            +"\tdamageEnums\n"
            +"\t\tAttackType \"TITAN\"\n"
            +"\t\tDamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\t\tDamageApplyType \"OVERTIME\"\n"
            +"\t\tDamageType \"PHYSICAL\"\n"
            +"\t\tWeaponClassType \"BEAM\"\n"
            +"\tDamagePerBank:FRONT 1.000000\n"
            +"\tDamagePerBank:BACK 1.000000\n"
            +"\tDamagePerBank:LEFT 1.000000\n"
            +"\tDamagePerBank:RIGHT 1.000000\n"
            +"\tRange 10.000000\n"
            +"\tPreBuffCooldownTime 0.500000\n"
            +"\tCanFireAtFighter TRUE\n"
            +"\tSynchronizedTargeting TRUE\n"
            +"\tPointStaggerDelay 4.000000\n"
            +"\tTravelSpeed 0.500000\n"
            +"\tDuration 7.000000\n"
            +"\tfireConstraintType \"CanAlwaysFire\"\n"
            +"\tWeaponEffects\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.000000\n"
            +"\t\tfireDelay 0.000000\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.identifier = "Weapon"
        cls.weaponType = {"identifier":"WeaponType","val":"Beam"}
        cls.dmgEnums = {
            "identifier":"damageEnums",
            "attackType":{"identifier":"AttackType","val":"TITAN"},
            "dmgAffects":{"identifier":"DamageAffectType",
                "val":"AFFECTS_ONLY_HULL"},
            "dmgApply":{"identifier":"DamageApplyType","val":"OVERTIME"},
            "dmgType":{"identifier":"DamageType","val":"PHYSICAL"},
            "weaponClass":{"identifier":"WeaponClassType","val":"BEAM"}
        }
        cls.dmgFront = {"identifier":"DamagePerBank:FRONT","val":1.0}
        cls.dmgBack = {"identifier":"DamagePerBank:BACK","val":1.0}
        cls.dmgLeft = {"identifier":"DamagePerBank:LEFT","val":1.0}
        cls.dmgRight = {"identifier":"DamagePerBank:RIGHT","val":1.0}
        cls.range = {"identifier":"Range","val":10.0}
        cls.cooldown = {"identifier":"PreBuffCooldownTime","val":0.5}
        cls.canAttackFighter = {"identifier":"CanFireAtFighter","val":True}
        cls.syncTargeting = {"identifier":"SynchronizedTargeting","val":True}
        cls.pointStagger = {"identifier":"PointStaggerDelay","val":4.0}
        cls.travelSpeed = {"identifier":"TravelSpeed","val":0.5}
        cls.duration = {"identifier":"Duration","val":7.0}
        cls.fireConstraint = {
            "conType":{"identifier":"fireConstraintType",
                "val":"CanAlwaysFire"}
        }
        cls.weaponEff = {
            "identifier":"WeaponEffects",
            "effectType":{"identifier":"weaponType","val":"Projectile"},
            "burstCount":{"identifier":"burstCount","val":1},
            "burstDelay":{"identifier":"burstDelay","val":0.0},
            "fireDelay":{"identifier":"fireDelay","val":0.0},
            "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
            "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                "val":0.1},
            "sndMuzzle":{
                "identifier":"muzzleSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndmuzzle"}
                ]
            },
            "hitEff":{"identifier":"hitEffectName","val":"effhit"},
            "sndHitHull":{
                "identifier":"hitHullEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndhullhit"}
                ]
            },
            "sndHitShields":{
                "identifier":"hitShieldsEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndshieldhit"}
                ]
            },
            "travelEff":{"identifier":"projectileTravelEffectName",
                "val":"traveleff"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.Weapon(
                identifier=self.identifier,
                weaponType=self.weaponType,
                dmgEnums=self.dmgEnums,
                dmgFront=self.dmgFront,
                dmgBack=self.dmgBack,
                dmgLeft=self.dmgLeft,
                dmgRight=self.dmgRight,
                range=self.range,
                cooldown=self.cooldown,
                canAttackFighter=self.canAttackFighter,
                syncTargeting=self.syncTargeting,
                pointStagger=self.pointStagger,
                travelSpeed=self.travelSpeed,
                duration=self.duration,
                fireConstraint=self.fireConstraint,
                weaponEff=self.weaponEff
        )
        patcher = mock.patch.object(self.inst.weaponEff,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockWeapEff = patcher.start()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_weapon.parseString(self.parseString)[0]
        res = armed.Weapon(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribWeaponType(self):
        """ Test whether weaponType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.WEAPON_TYPES,**self.weaponType)
        self.assertEqual(self.inst.weaponType,exp)

    def testCreationAttribDmgEnums(self):
        """ Test whether dmgEnums attrib is created correctly
        """
        exp = armed.DamageEnums(**self.dmgEnums)
        self.assertEqual(self.inst.dmgEnums,exp)

    def testCreationAttribdmgFront(self):
        """ Test whether dmgFront attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.dmgFront)
        self.assertEqual(self.inst.dmgFront,exp)

    def testCreationAttribdmgBack(self):
        """ Test whether dmgBack attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.dmgBack)
        self.assertEqual(self.inst.dmgBack,exp)

    def testCreationAttribdmgLeft(self):
        """ Test whether dmgLeft attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.dmgLeft)
        self.assertEqual(self.inst.dmgLeft,exp)

    def testCreationAttribdmgRight(self):
        """ Test whether dmgRight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.dmgRight)
        self.assertEqual(self.inst.dmgRight,exp)

    def testCreationAttribRange(self):
        """ Test whether range attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.range)
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribCooldown(self):
        """ Test whether cooldown attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.cooldown)
        self.assertEqual(self.inst.cooldown,exp)

    def testCreationAttribCanAttackFighters(self):
        """ Test whether canAttackFighter attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.canAttackFighter)
        self.assertEqual(self.inst.canAttackFighter,exp)

    def testCreationAttribSyncTargeting(self):
        """ Test whether syncTargeting attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.syncTargeting)
        self.assertEqual(self.inst.syncTargeting,exp)

    def testCreationAttribPointStagger(self):
        """ Test whether pointStagger attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.pointStagger)
        self.assertEqual(self.inst.pointStagger,exp)

    def testCreationAttribTravelSpeed(self):
        """ Test whether travelSpeed attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.travelSpeed)
        self.assertEqual(self.inst.travelSpeed,exp)

    def testCreationAttribDuration(self):
        """ Test whether duration attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.duration)
        self.assertEqual(self.inst.duration,exp)

    def testCreationAttribFireConstraint(self):
        """ Test whether fireConstraint attrib is created correctly
        """
        exp = armed.FireConstraint(**self.fireConstraint)
        self.assertEqual(self.inst.fireConstraint,exp)

    def testCreationAttribWeaponEff(self):
        """ Test whether weaponEff attrib is created correctly
        """
        exp = armed.ProjectileWeaponEff(**self.weaponEff)
        self.assertEqual(self.inst.weaponEff,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckWeaponEff(self):
        """ Test whether check returns problems from weaponEff attrib
        """
        self.mockWeapEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests FiringAlignmentDefault ######################"""

class FiringAlignmentDefaultTests(unit.TestCase):
    desc = "Tests FiringAlignmentDefault:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "firingAlignmentType \"Default\"\n"
        )
        cls.alignmentType = {"identifier":"firingAlignmentType",
            "val":"Default"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.FiringAlignmentDefault(
                alignmentType=self.alignmentType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_firingAlignment.parseString(self.parseString)[0]
        res = armed.FiringAlignmentDefault.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid alignment type
        """
        invalid = {"identifier":"firingAlignmentType","val":"INV"}
        with self.assertRaises(RuntimeError):
            res = armed.FiringAlignmentDefault.factory(alignmentType=invalid)

    def testCreationAttribAlignmentType(self):
        """ Test whether alignmentType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["Default","TiltForward"],
                **self.alignmentType)
        self.assertEqual(self.inst.alignmentType,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests TiltingFiringAlignment ######################"""

class TiltingFiringAlignmentTests(unit.TestCase):
    desc = "Tests TiltingFiringAlignment:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "firingAlignmentType \"TiltForward\"\n"
            +"firingTiltAngle 0.500000\n"
        )
        cls.alignmentType = {"identifier":"firingAlignmentType",
            "val":"TiltForward"}
        cls.tiltAngle = {"identifier":"firingTiltAngle","val":0.5}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.TiltingFiringAlignment(
                alignmentType=self.alignmentType,
                tiltAngle=self.tiltAngle
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = armed.g_firingAlignment.parseString(self.parseString)[0]
        res = armed.FiringAlignmentDefault.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTiltAngle(self):
        """ Test whether tiltAngle attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.tiltAngle)
        self.assertEqual(self.inst.tiltAngle,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Armed ##############################"""

class ArmedFixtures(HitableFixtures,HasAttackBehaviorFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.armedString = (
            "NumWeapons 1\n"
            "Weapon\n"
            +"\tWeaponType \"Beam\"\n"
            +"\tdamageEnums\n"
            +"\t\tAttackType \"TITAN\"\n"
            +"\t\tDamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\t\tDamageApplyType \"OVERTIME\"\n"
            +"\t\tDamageType \"PHYSICAL\"\n"
            +"\t\tWeaponClassType \"BEAM\"\n"
            +"\tDamagePerBank:FRONT 1.000000\n"
            +"\tDamagePerBank:BACK 1.000000\n"
            +"\tDamagePerBank:LEFT 1.000000\n"
            +"\tDamagePerBank:RIGHT 1.000000\n"
            +"\tRange 10.000000\n"
            +"\tPreBuffCooldownTime 0.500000\n"
            +"\tCanFireAtFighter TRUE\n"
            +"\tSynchronizedTargeting TRUE\n"
            +"\tPointStaggerDelay 4.000000\n"
            +"\tTravelSpeed 0.500000\n"
            +"\tDuration 7.000000\n"
            +"\tfireConstraintType \"CanAlwaysFire\"\n"
            +"\tWeaponEffects\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.000000\n"
            +"\t\tfireDelay 0.000000\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
            +"m_weaponIndexForRange 1\n"
            +"firingAlignmentType \"Default\"\n"
            +"TargetCountPerBank:FRONT 1\n"
            +"TargetCountPerBank:BACK 0\n"
            +"TargetCountPerBank:LEFT 2\n"
            +"TargetCountPerBank:RIGHT 2\n"
            +"canOnlyTargetStructures FALSE\n"
        )
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"minZoomDistanceMult 1.850000\n"
            +"minShadow 0.000000\n"
            +"maxShadow 0.500000\n"
            +"armorType \"Titan\"\n"
            +"BaseArmorPoints 12.000000\n"
            +"ExplosionName \"testexplosion\"\n"
            +"experiencePointsForDestroying 10.000000\n"
            +"HullPointRestoreRate 1.000000\n"
            +"MaxHullPoints 2.000000\n"
            +"numRandomDebrisLarge 1.000000\n"
            +"numRandomDebrisSmall 1.000000\n"
            +"numSpecificDebris 1\n"
            +"\tspecificDebrisMeshName \"debris\"\n"
            +"defaultAutoAttackRange \"None\"\n"
            +"defaultAutoAttackOn TRUE\n"
            +"prefersToFocusFire TRUE\n"
            +"usesFighterAttack FALSE\n"
            +cls.armedString
        )
        cls.weapons = {
            "counter":{"identifier":"NumWeapons","val":1},
            "elements":[
                {
                    "identifier":"Weapon",
                    "weaponType":{"identifier":"WeaponType","val":"Beam"},
                    "dmgEnums":{
                        "identifier":"damageEnums",
                        "attackType":{"identifier":"AttackType","val":"TITAN"},
                        "dmgAffects":{"identifier":"DamageAffectType",
                            "val":"AFFECTS_ONLY_HULL"},
                        "dmgApply":{"identifier":"DamageApplyType",
                            "val":"OVERTIME"},
                        "dmgType":{"identifier":"DamageType","val":"PHYSICAL"},
                        "weaponClass":{"identifier":"WeaponClassType",
                            "val":"BEAM"}
                    },
                    "dmgFront":{"identifier":"DamagePerBank:FRONT","val":1.0},
                    "dmgBack":{"identifier":"DamagePerBank:BACK","val":1.0},
                    "dmgLeft":{"identifier":"DamagePerBank:LEFT","val":1.0},
                    "dmgRight":{"identifier":"DamagePerBank:RIGHT","val":1.0},
                    "range":{"identifier":"Range","val":10.0},
                    "cooldown":{"identifier":"PreBuffCooldownTime","val":0.5},
                    "canAttackFighter":{"identifier":"CanFireAtFighter",
                        "val":True},
                    "syncTargeting":{"identifier":"SynchronizedTargeting",
                        "val":True},
                    "pointStagger":{"identifier":"PointStaggerDelay","val":4.0},
                    "travelSpeed":{"identifier":"TravelSpeed","val":0.5},
                    "duration":{"identifier":"Duration","val":7.0},
                    "fireConstraint":{
                        "conType":{"identifier":"fireConstraintType",
                            "val":"CanAlwaysFire"}
                    },
                    "weaponEff":{
                        "identifier":"WeaponEffects",
                        "effectType":{"identifier":"weaponType",
                            "val":"Projectile"},
                        "burstCount":{"identifier":"burstCount","val":1},
                        "burstDelay":{"identifier":"burstDelay","val":0.0},
                        "fireDelay":{"identifier":"fireDelay","val":0.0},
                        "muzzleEff":{"identifier":"muzzleEffectName",
                            "val":"effmuzzle"},
                        "sndMuzzleRespawn":{
                            "identifier":"muzzleSoundMinRespawnTime",
                            "val":0.1},
                        "sndMuzzle":{
                            "identifier":"muzzleSounds",
                            "counter":{"identifier":"soundCount","val":1},
                            "elements":[
                                {"identifier":"sound","val":"sndmuzzle"}
                            ]
                        },
                        "hitEff":{"identifier":"hitEffectName","val":"effhit"},
                        "sndHitHull":{
                            "identifier":"hitHullEffectSounds",
                            "counter":{"identifier":"soundCount","val":1},
                            "elements":[
                                {"identifier":"sound","val":"sndhullhit"}
                            ]
                        },
                        "sndHitShields":{
                            "identifier":"hitShieldsEffectSounds",
                            "counter":{"identifier":"soundCount","val":1},
                            "elements":[
                                {"identifier":"sound","val":"sndshieldhit"}
                            ]
                        },
                        "travelEff":{"identifier":"projectileTravelEffectName",
                            "val":"traveleff"}
                    }
                }
            ]
        }
        cls.indexForRange = {"identifier":"m_weaponIndexForRange","val":1}
        cls.firingAlignment = {
            "alignmentType":{"identifier":"firingAlignmentType",
                "val":"Default"}
        }
        cls.targetsFront = {"identifier":"TargetCountPerBank:FRONT","val":1}
        cls.targetsBack = {"identifier":"TargetCountPerBank:BACK","val":0}
        cls.targetsLeft = {"identifier":"TargetCountPerBank:LEFT","val":2}
        cls.targetsRight = {"identifier":"TargetCountPerBank:RIGHT","val":2}
        cls.onlyTargetStructures = {"identifier":"canOnlyTargetStructures",
            "val":False}
        cls.mmod = tco.genMockMod("./")

class ArmedTests(ArmedFixtures,unit.TestCase):
    desc = "Tests Armed:"

    def setUp(self):
        self.inst = armed.Armed(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures
        )
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockWeapons = patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExplosion = patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDebris = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_zoomDist
            +coe.g_shadows
            +hit.g_armorType
            +hit.g_baseArmorPoints
            +hit.g_explosion
            +hit.g_exp
            +hit.g_hullRestore
            +hit.g_hullPoints
            +coe.g_debris
            +armed.g_attackBehaviour
            +armed.g_armed
        )
        parseRes = parser.parseString(self.parseString)
        res = armed.Armed(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribWeapons(self):
        """ Test whether weapons attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=armed.Weapon,**self.weapons)
        self.assertEqual(self.inst.weapons,exp)

    def testCreationAttribIndexForRange(self):
        """ Test whether indexForRange attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.indexForRange)
        self.assertEqual(self.inst.indexForRange,exp)

    def testCreationAttribFiringAlignment(self):
        """ Test whether firingAlignment attrib is created correctly
        """
        exp = armed.FiringAlignmentDefault(**self.firingAlignment)
        self.assertEqual(self.inst.firingAlignment,exp)

    def testCreationAttribTargetsFront(self):
        """ Test whether targetsFront attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.targetsFront)
        self.assertEqual(self.inst.targetsFront,exp)

    def testCreationAttribTargetsBack(self):
        """ Test whether targetsBack attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.targetsBack)
        self.assertEqual(self.inst.targetsBack,exp)

    def testCreationAttribTargetsRight(self):
        """ Test whether targetsRight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.targetsRight)
        self.assertEqual(self.inst.targetsRight,exp)

    def testCreationAttribTargetsLeft(self):
        """ Test whether targetsLeft attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.targetsLeft)
        self.assertEqual(self.inst.targetsLeft,exp)

    def testCreationAttribOnlyTargetStructures(self):
        """ Test whether onlyTargetStructures attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.onlyTargetStructures)
        self.assertEqual(self.inst.onlyTargetStructures,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckWeapons(self):
        """ Test whether check returns problems from weapons attrib
        """
        self.mockWeapons.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.armedString)

"""############################ Tests Squadron #############################"""

class SquadronTests(unit.TestCase):
    desc = "Tests Squadron:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "squadTypeEntityDef:3 \"testsquad\"\n"
            +"squadAntiMatterCost:3 5.000000\n"
        )
        cls.squadDef = {"identifier":"squadTypeEntityDef:3",
            "val":"testsquad"}
        cls.antimatter = {"identifier":"squadAntiMatterCost:3","val":5.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = armed.Squadron(
                squadDef=self.squadDef,
                antimatter=self.antimatter
        )
        patcher = mock.patch.object(self.inst.squadDef,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockSquad = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genStringAttrib("squadTypeEntityDef:3")("squadDef")
            +co_parse.genDecimalAttrib("squadAntiMatterCost:3")("antimatter")
        )
        parseRes = parser.parseString(self.parseString)
        res = armed.Squadron(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSquadDef(self):
        """ Test whether squadDef attrib is created correctly
        """
        exp = coe.EntityRef(types=["Squad"],**self.squadDef)
        self.assertEqual(self.inst.squadDef,exp)

    def testCreationAttribAntimatter(self):
        """ Test whether antimatter attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.antimatter)
        self.assertEqual(self.inst.antimatter,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckSquadDef(self):
        """ Test whether check returns returns problems from squadDef attrib
        """
        self.mockSquad.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests CarriesFighters #########################"""

class CarriesFightersFixtures(ArmedFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"armorType \"Titan\"\n"
            +"BaseArmorPoints 12.0\n"
            +"ExplosionName \"testexplosion\"\n"
            +"experiencePointsForDestroying 10.0\n"
            +"HullPointRestoreRate 1.0\n"
            +"MaxHullPoints 2.0\n"
            +"numRandomDebrisLarge 1.0\n"
            +"numRandomDebrisSmall 1.0\n"
            +"numSpecificDebris 1\n"
            +"\tspecificDebrisMeshName \"debris\"\n"
            +"defaultAutoAttackRange \"None\"\n"
            +"defaultAutoAttackOn TRUE\n"
            +"prefersToFocusFire TRUE\n"
            +"usesFighterAttack FALSE\n"
            +"NumWeapons 1\n"
            +"Weapon\n"
            +"\tWeaponType \"Beam\"\n"
            +"\tdamageEnums\n"
            +"\t\tAttackType \"TITAN\"\n"
            +"\t\tDamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\t\tDamageApplyType \"OVERTIME\"\n"
            +"\t\tDamageType \"PHYSICAL\"\n"
            +"\t\tWeaponClassType \"BEAM\"\n"
            +"\tDamagePerBank:FRONT 1.0\n"
            +"\tDamagePerBank:BACK 1.0\n"
            +"\tDamagePerBank:LEFT 1.0\n"
            +"\tDamagePerBank:RIGHT 1.0\n"
            +"\tRange 10.0\n"
            +"\tPreBuffCooldownTime 0.5\n"
            +"\tCanFireAtFighter TRUE\n"
            +"\tSynchronizedTargeting TRUE\n"
            +"\tPointStaggerDelay 4.0\n"
            +"\tTravelSpeed 0.5\n"
            +"\tDuration 7.0\n"
            +"\tfireConstraintType \"CanAlwaysFire\"\n"
            +"\tWeaponEffects\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.0\n"
            +"\t\tfireDelay 0.0\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.1\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
            +"m_weaponIndexForRange 1\n"
            +"firingAlignmentType \"Default\"\n"
            +"TargetCountPerBank:FRONT 1\n"
            +"TargetCountPerBank:BACK 0\n"
            +"TargetCountPerBank:LEFT 2\n"
            +"TargetCountPerBank:RIGHT 2\n"
            +"canOnlyTargetStructures FALSE\n"
            +"squadTypeEntityDef:0 \"testsquad\"\n"
            +"squadAntiMatterCost:0 5.0\n"
            +"squadTypeEntityDef:1 \"testsquad\"\n"
            +"squadAntiMatterCost:1 5.0\n"
            +"squadTypeEntityDef:2 \"testsquad\"\n"
            +"squadAntiMatterCost:2 5.0\n"
            +"squadTypeEntityDef:3 \"testsquad\"\n"
            +"squadAntiMatterCost:3 5.0\n"
            +"baseCommandPoints 3\n"
        )
        cls.squads = [
            {
                "squadDef":{"identifier":"squadTypeEntityDef:0",
                    "val":"testsquad"},
                "antimatter":{"identifier":"squadAntiMatterCost:0","val":5.0}
            },
            {
                "squadDef":{"identifier":"squadTypeEntityDef:1",
                    "val":"testsquad"},
                "antimatter":{"identifier":"squadAntiMatterCost:1","val":5.0}
            },
            {
                "squadDef":{"identifier":"squadTypeEntityDef:2",
                    "val":"testsquad"},
                "antimatter":{"identifier":"squadAntiMatterCost:2","val":5.0}
            },
            {
                "squadDef":{"identifier":"squadTypeEntityDef:3",
                    "val":"testsquad"},
                "antimatter":{"identifier":"squadAntiMatterCost:3","val":5.0}
            },
        ]
        cls.commandPoints = {"identifier":"baseCommandPoints","val":3}
        cls.commandPointsLvl = {
            "identifier":"CommandPoints",
            "startVal":{"identifier":"StartValue","val":0.6},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.2}
        }
        cls.mmod = tco.genMockMod("./")

class CarriesFightersTests(CarriesFightersFixtures,unit.TestCase):
    desc = "Tests Armed:"

    def setUp(self):
        self.inst = armed.CarriesFighters(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                commandPoints=self.commandPoints
        )
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        for ab in self.inst.squads:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_zoomDist
            +coe.g_shadows
            +hit.g_armorType
            +hit.g_baseArmorPoints
            +hit.g_explosion
            +hit.g_exp
            +hit.g_hullRestore
            +hit.g_hullPoints
            +coe.g_debris
            +armed.g_attackBehaviour
            +armed.g_armed
            +armed.g_squadrons
            +co_parse.genIntAttrib("baseCommandPoints")("commandPoints")
        )
        parseRes = parser.parseString(self.parseString)
        res = armed.CarriesFighters(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSquads(self):
        """ Test whether squads attrib is created correctly
        """
        exp = (
            armed.Squadron(**self.squads[0]),
            armed.Squadron(**self.squads[1]),
            armed.Squadron(**self.squads[2]),
            armed.Squadron(**self.squads[3])
        )
        self.assertEqual(self.inst.squads,exp)

    def testCreationAttribCommandPoints(self):
        """ Test whether commandPoints attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.commandPoints)
        self.assertEqual(self.inst.commandPoints,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckSquads(self):
        """ Test whether check returns problems from squads attrib
        """
        self.inst.squads[2].check.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.inst.squads[2].check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[mock.sentinel.prob])
