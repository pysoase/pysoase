""" Unit tests for the pysoase.entities.upgrades module.
"""

import os
import unittest as unit
import unittest.mock as mock

import pysoase.common.attributes as co_attribs
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.upgrades as upgrades
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_ecommon import UIVisibleFixtures

testdata = os.path.join("entities","upgrades")

"""########################### Tests TiFloatProp ###########################"""

class FloatUpgradePropertyTests(unit.TestCase):
    desc = "Tests FloatUpgradeProperty:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "upgradeProperty\n"
            +"\tpropertyType \"Population\"\n"
            +"\tvalue 50.000000\n"
        )
        cls.identifier = "upgradeProperty"
        cls.propType = {"identifier":"propertyType","val":"Population"}
        cls.value = {"identifier":"value","val":50.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = upgrades.FloatUpgradeProperty(
                possTypes=upgrades.SBUPGRADE_PROPERTY_TYPES,
                identifier=self.identifier,
                propType=self.propType,
                value=self.value
        )

    def testCreationAttribPropType(self):
        """ Test whether propType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=upgrades.SBUPGRADE_PROPERTY_TYPES,
                **self.propType)
        self.assertEqual(self.inst.propType,exp)

    def testCreationAttribValue(self):
        """ Test whether value attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.value)
        self.assertEqual(self.inst.value,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class SbFloatPropTests(unit.TestCase):
    desc = "Tests SbFloatProp:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "upgradeProperty\n"
            +"\tpropertyType \"Population\"\n"
            +"\tvalue 50.0\n"
        )
        cls.identifier = "upgradeProperty"
        cls.propType = {"identifier":"propertyType","val":"Population"}
        cls.value = {"identifier":"value","val":50.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = upgrades.SbFloatProp(
                identifier=self.identifier,
                propType=self.propType,
                value=self.value
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = upgrades.g_SBUpgradeProperty.parseString(self.parseString)[0]
        res = upgrades.SbFloatProp(**parseRes)
        self.assertEqual(res,self.inst)

class TiFloatPropTests(unit.TestCase):
    desc = "Tests TiFloatProp:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "upgradeProperty\n"
            +"\tpropertyType \"AbilityELevel\"\n"
            +"\tvalue 50.0\n"
        )
        cls.identifier = "upgradeProperty"
        cls.propType = {"identifier":"propertyType","val":"AbilityELevel"}
        cls.value = {"identifier":"value","val":50.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = upgrades.TiFloatProp(
                identifier=self.identifier,
                propType=self.propType,
                value=self.value
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = upgrades.g_tiUpgradeProperty.parseString(self.parseString)[0]
        res = upgrades.TiFloatProp(**parseRes)
        self.assertEqual(res,self.inst)

"""######################### Tests SbUpgradeStage ##########################"""

class SbUpgradeStageTests(UIVisibleFixtures,unit.TestCase):
    desc = "Tests SbUpgradeStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "stage\n"
            +"\tprice\n"
            +"\t\tcredits 100\n"
            +"\t\tmetal 200\n"
            +"\t\tcrystal 300\n"
             "\tupgradeTime 0.500000\n"
            +"\tnameStringID \"namestring\"\n"
            +"\tdescStringID \"descstring\"\n"
            +"\tresearchPrerequisites\n"
            +"\t\tNumResearchPrerequisites 1\n"
            +"\t\tResearchPrerequisite\n"
            +"\t\t\tSubject \"research1\"\n"
            +"\t\t\tLevel 2\n"
            +"\t\tRequiredFactionNameID \"faction\"\n"
            +"\t\tRequiredCompletedResearchSubjects 0\n"
            +"\thudIcon \"hudicon\"\n"
            +"\tsmallHudIcon \"smallhudicon\"\n"
            +"\tinfoCardIcon \"infocardicon\"\n"
            +"\tupgradePropertyCount 1\n"
            +"\tupgradeProperty\n"
            +"\t\tpropertyType \"Population\"\n"
            +"\t\tvalue 12.000000\n"
            +"\tupgradeBoolPropertyCount 1\n"
            +"\tupgradeBoolProperty \"CanPreventEnemyColonize\"\n"
        )
        cls.identifier = "stage"
        cls.price = {
            "identifier":"price",
            "credits":{"identifier":"credits","val":100},
            "metal":{"identifier":"metal","val":200},
            "crystal":{"identifier":"crystal","val":300}
        }
        cls.prereqs = {
            "identifier":"researchPrerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"research1"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"faction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":0}
        }
        cls.upgTime = {"identifier":"upgradeTime","val":0.5}
        cls.propsBool = {
            "counter":{"identifier":"upgradeBoolPropertyCount","val":1},
            "elements":[
                {
                    "identifier":"upgradeBoolProperty",
                    "val":"CanPreventEnemyColonize"
                }
            ]
        }
        cls.propsFloat = {
            "counter":{"identifier":"upgradePropertyCount","val":1},
            "elements":[
                {
                    "identifier":"upgradeProperty",
                    "propType":{"identifier":"propertyType",
                        "val":"Population"},
                    "value":{"identifier":"value","val":12.0}
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = upgrades.SbUpgradeStage(
                identifier=self.identifier,
                iconHud=self.iconHud,
                iconHudSmall=self.iconHudSmall,
                iconInfocard=self.iconInfocard,
                nameString=self.nameString,
                descString=self.descString,
                prereqs=self.prereqs,
                propsBool=self.propsBool,
                propsFloat=self.propsFloat,
                price=self.price,
                upgTime=self.upgTime
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly form parse result
        """
        parseRes = upgrades.g_sbUpgradeStage.parseString(self.parseString)[0]
        res = upgrades.SbUpgradeStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCreationAttribPropsBool(self):
        """ Test whether propsBool attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":upgrades.BOOL_PROPERTY_TYPES},
                **self.propsBool)
        self.assertEqual(self.inst.propsBool,exp)

    def testCreationAttribPropsFloat(self):
        """ Test whether propsFloat attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=upgrades.SbFloatProp,**self.propsFloat)
        self.assertEqual(self.inst.propsFloat,exp)

    def testCreationAttribPrice(self):
        """ Test whether price attribute is created correctly
        """
        exp = coe.Cost(**self.price)
        self.assertEqual(self.inst.price,exp)

    def testCreationAttribUpgTime(self):
        """ Test whether upgTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.upgTime)
        self.assertEqual(self.inst.upgTime,exp)

    def testCheckNoProbs(self):
        """ Test whether check without problems returns empty list
        """
        with mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,probs)
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests TiUpgradeStage ##########################"""

class TiUpgradeStageTests(UIVisibleFixtures,unit.TestCase):
    desc = "Tests TiUpgradeStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "stage\n"
            +"\tnameStringID \"namestring\"\n"
            +"\tdescStringID \"descstring\"\n"
            +"\tresearchPrerequisites\n"
            +"\t\tNumResearchPrerequisites 1\n"
            +"\t\tResearchPrerequisite\n"
            +"\t\t\tSubject \"research1\"\n"
            +"\t\t\tLevel 2\n"
            +"\t\tRequiredFactionNameID \"faction\"\n"
            +"\t\tRequiredCompletedResearchSubjects 0\n"
            +"\tlevelRequirement 3\n"
            +"\thudIcon \"hudicon\"\n"
            +"\tsmallHudIcon \"smallhudicon\"\n"
            +"\tinfoCardIcon \"infocardicon\"\n"
            +"\tupgradePropertyCount 1\n"
            +"\tupgradeProperty\n"
            +"\t\tpropertyType \"AbilityELevel\"\n"
            +"\t\tvalue 12.000000\n"
            +"\tupgradeBoolPropertyCount 1\n"
            +"\tupgradeBoolProperty \"CanPreventEnemyColonize\"\n"
        )
        cls.identifier = "stage"
        cls.prereqs = {
            "identifier":"researchPrerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"research1"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"faction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":0}
        }
        cls.propsBool = {
            "counter":{"identifier":"upgradeBoolPropertyCount","val":1},
            "elements":[
                {
                    "identifier":"upgradeBoolProperty",
                    "val":"CanPreventEnemyColonize"
                }
            ]
        }
        cls.propsFloat = {
            "counter":{"identifier":"upgradePropertyCount","val":1},
            "elements":[
                {
                    "identifier":"upgradeProperty",
                    "propType":{"identifier":"propertyType",
                        "val":"AbilityELevel"},
                    "value":{"identifier":"value","val":12.0}
                }
            ]
        }
        cls.levelReq = {"identifier":"levelRequirement","val":3}
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = upgrades.TiUpgradeStage(
                identifier=self.identifier,
                iconHud=self.iconHud,
                iconHudSmall=self.iconHudSmall,
                iconInfocard=self.iconInfocard,
                nameString=self.nameString,
                descString=self.descString,
                prereqs=self.prereqs,
                propsBool=self.propsBool,
                propsFloat=self.propsFloat,
                levelReq=self.levelReq
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly form parse result
        """
        parseRes = upgrades.g_tiUpgradeStage.parseString(self.parseString)[0]
        res = upgrades.TiUpgradeStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCreationAttribPropsBool(self):
        """ Test whether propsBool attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":upgrades.BOOL_PROPERTY_TYPES},
                **self.propsBool)
        self.assertEqual(self.inst.propsBool,exp)

    def testCreationAttribPropsFloat(self):
        """ Test whether propsFloat attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=upgrades.TiFloatProp,**self.propsFloat)
        self.assertEqual(self.inst.propsFloat,exp)

    def testCreationAttribLevelReq(self):
        """ Test whether levelReq attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.levelReq)
        self.assertEqual(self.inst.levelReq,exp)

    def testCheckNoProbs(self):
        """ Test whether check without problems returns empty list
        """
        with mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,probs)
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests TitanUpgrade ###########################"""

class TitanUpgradeTests(unit.TestCase):
    desc = "Tests TitanUpgrade:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "TitanUpgrade.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.stages = {
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "iconHud":{"identifier":"hudIcon","val":"hudicon"},
                    "iconInfocard":{"identifier":"infoCardIcon",
                        "val":"infocardicon"},
                    "iconHudSmall":{"identifier":"smallHudIcon",
                        "val":"smallhudicon"},
                    "descString":{"identifier":"descStringID",
                        "val":"descstring"},
                    "nameString":{"identifier":"nameStringID",
                        "val":"namestring"},
                    "prereqs":{
                        "identifier":"researchPrerequisites",
                        "prereqs":{
                            "counter":{
                                "identifier":"NumResearchPrerequisites",
                                "val":1},
                            "elements":[
                                {
                                    "identifier":"ResearchPrerequisite",
                                    "subject":{
                                        "identifier":"Subject",
                                        "val":"research1"},
                                    "level":{"identifier":"Level",
                                        "val":2}
                                }
                            ]
                        },
                        "reqFaction":{
                            "identifier":"RequiredFactionNameID",
                            "val":"faction"},
                        "reqCompSubjects":{
                            "identifier":"RequiredCompletedResearchSubjects",
                            "val":2}
                    },
                    "propsBool":{
                        "counter":{
                            "identifier":"upgradeBoolPropertyCount",
                            "val":1},
                        "elements":[
                            {
                                "identifier":"upgradeBoolProperty",
                                "val":"CanPreventEnemyColonize"
                            }
                        ]
                    },
                    "propsFloat":{
                        "counter":{"identifier":"upgradePropertyCount",
                            "val":1},
                        "elements":[
                            {
                                "identifier":"upgradeProperty",
                                "propType":{"identifier":"propertyType",
                                    "val":"AbilityELevel"},
                                "value":{"identifier":"value",
                                    "val":12.0}
                            }
                        ]
                    },
                    "levelReq":{"identifier":"levelRequirement","val":3}
                }
            ]
        }
        cls.entityType = {"identifier":"entityType","val":"TitanUpgrade"}
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = upgrades.TitanUpgrade(
                filepath=self.filepath,
                entityType=self.entityType,
                stages=self.stages
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = upgrades.TitanUpgrade.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether instance is created correctly from parse results
        """
        res = upgrades.TitanUpgrade.createInstance(
                "TitanUpgradeMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribStages(self):
        """ Test whether stages attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=upgrades.TiUpgradeStage,**self.stages)
        self.assertEqual(self.inst.stages,exp)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        with mock.patch.object(self.inst.stages,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(co_probs.BasicProblem)
        with mock.patch.object(self.inst.stages,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            self.assertEqual(res[0].probFile,self.filepath)
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests StarbaseUpgrade ##########################"""

class StarbaseUpgradeTests(unit.TestCase):
    desc = "Tests StarbaseUpgrade:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "StarbaseUpgrade.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.stages = {
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "iconHud":{"identifier":"hudIcon","val":"hudicon"},
                    "iconInfocard":{"identifier":"infoCardIcon",
                        "val":"infocardicon"},
                    "iconHudSmall":{"identifier":"smallHudIcon",
                        "val":"smallhudicon"},
                    "descString":{"identifier":"descStringID",
                        "val":"descstring"},
                    "nameString":{"identifier":"nameStringID",
                        "val":"namestring"},
                    "prereqs":{
                        "identifier":"researchPrerequisites",
                        "prereqs":{
                            "counter":{
                                "identifier":"NumResearchPrerequisites",
                                "val":1},
                            "elements":[
                                {
                                    "identifier":"ResearchPrerequisite",
                                    "subject":{
                                        "identifier":"Subject",
                                        "val":"research1"},
                                    "level":{"identifier":"Level",
                                        "val":1}
                                }
                            ]
                        },
                        "reqFaction":{
                            "identifier":"RequiredFactionNameID",
                            "val":"faction"},
                        "reqCompSubjects":{
                            "identifier":"RequiredCompletedResearchSubjects",
                            "val":2}
                    },
                    "propsBool":{
                        "counter":{
                            "identifier":"upgradeBoolPropertyCount",
                            "val":1},
                        "elements":[
                            {
                                "identifier":"upgradeBoolProperty",
                                "val":"CanPreventEnemyColonize"
                            }
                        ]
                    },
                    "propsFloat":{
                        "counter":{"identifier":"upgradePropertyCount",
                            "val":1},
                        "elements":[
                            {
                                "identifier":"upgradeProperty",
                                "propType":{"identifier":"propertyType",
                                    "val":"Population"},
                                "value":{"identifier":"value",
                                    "val":12.0}
                            }
                        ]
                    },
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":100},
                        "metal":{"identifier":"metal","val":200},
                        "crystal":{"identifier":"crystal","val":300}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":60.0}
                }
            ]
        }
        cls.entityType = {"identifier":"entityType","val":"StarBaseUpgrade"}
        cls.xpMod = {"identifier":"xpModifier","val":25.0}
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = upgrades.StarbaseUpgrade(
                filepath=self.filepath,
                entityType=self.entityType,
                xpMod=self.xpMod,
                stages=self.stages
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = upgrades.StarbaseUpgrade.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether instance is created correctly from parse results
        """
        res = upgrades.StarbaseUpgrade.createInstance(
                "StarbaseUpgradeMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribStages(self):
        """ Test whether stages attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=upgrades.SbUpgradeStage,**self.stages)
        self.assertEqual(self.inst.stages,exp)

    def testCreationAttribXpMod(self):
        """ Test whether xpMod attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.xpMod)
        self.assertEqual(self.inst.xpMod,exp)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list without problems
        """
        with mock.patch.object(self.inst.stages,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns correct problems
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(co_probs.BasicProblem)
        with mock.patch.object(self.inst.stages,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            self.assertEqual(res[0].probFile,self.filepath)
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)
