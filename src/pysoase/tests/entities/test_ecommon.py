""" Tests for the pysoase.entities.entity module.
"""

import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.meshes as meshes
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = "entities"

"""####################### Tests WrongEntityTypeProb #######################"""

class WrongEntityTypeProbTest(unit.TestCase):
    desc = "Test WrongEntityTypeProb:"

    def testOutput(self):
        """ Test whether string representation is constructed correctly
        """
        exp = ("ERROR: Reference \"Testref\" has the wrong type. Allowed "
               +"types are "+str(["Star","Planet"])+".\n")
        inst = coe.WrongEntityTypeProb("Testref",["Star","Planet"])
        self.assertEqual(inst.toString(0),exp)

"""############################ Tests EntityRef ############################"""

class EntityRefGoodVals(unit.TestCase):
    desc = "Test EntityRef with good input:"

    def setUp(self):
        self.types = ["CapitalShip","Frigate"]
        self.val = "TestEntity"
        self.identifier = "testRef"
        self.ref = coe.EntityRef(identifier=self.identifier,val=self.val,
                types=self.types)
        self.mod = tco.genMockMod("./")
        self.mod.entities.checkEntityType.return_value = True
        self.mod.entities.checkManifest.return_value = []

    def testSetTypes(self):
        """ Test whether all EntityTypes are accepted
        """
        for t in coe.POSSIBLE_ENTITY_TYPES:
            self.ref.types = [t]
            self.assertListEqual(self.ref.types,[t])

    def testCheckEmpty(self):
        """ Test whether check returns no problems
        """
        self.assertListEqual(self.ref.check(self.mod),[])
        self.mod.entities.checkEntityType.assert_called_once_with(
                self.val+".entity",self.types)
        self.mod.entities.checkManifest.assert_called_once_with(
                self.val+".entity")

    def testOutput(self):
        """ Test whether toString output is correct
        """
        expected = "testRef \"TestEntity\"\n"
        self.assertMultiLineEqual(self.ref.toString(0),expected)

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst = coe.EntityRef(
                identifier=self.identifier,
                val=self.val,
                types=self.types
        )
        self.assertEqual(inst,self.ref)

    def testDifferTypes(self):
        """ Test whether instances with different types test unequal
        """
        inst = coe.EntityRef(
                identifier=self.identifier,
                val=self.val,
                types=["Planet","Star"]
        )
        self.assertNotEqual(inst,self.ref)

class EntityRefInvalidTypes(unit.TestCase):
    desc = "Test EntityRef with Invalid Types:"

    def setUp(self):
        self.invalidTypes = ["Fregatte","Schlachtschiff"]
        self.errorMsg = ".*\'Fregatte\'.*"+str(coe.POSSIBLE_ENTITY_TYPES)

    def testInitThrowsError(self):
        """ Test whether creation with invalid types throws error
        """
        with self.assertRaisesRegexp(ValueError,self.errorMsg):
            coe.EntityRef(identifier="TestRef",val="TestEntity",
                    types=self.invalidTypes)

    def testInitEmptyTypesThrowsError(self):
        """ Test whether creation with empty types throws error
        """
        with self.assertRaisesRegexp(ValueError,"\'EntityRef.types\' must "
                                                "not be empty."):
            coe.EntityRef(identifier="TestRef",val="TestEntity",
                    types=[])

    def testAssignThrowsError(self):
        """ Test whether trying to assign invalid types throws an error
        """
        with self.assertRaisesRegexp(ValueError,self.errorMsg):
            # First create working instance
            entref = coe.EntityRef(identifier="TestRef",val="TestEntity",
                    types=["Frigate"])
            entref.types = self.invalidTypes

    def testAssignEmptyThrowsError(self):
        """ Test whether trying to assign empty types throws an error
        """
        with self.assertRaisesRegexp(ValueError,"\'EntityRef.types\' must "
                                                "not be empty."):
            # First create working instance
            entref = coe.EntityRef(identifier="TestRef",val="TestEntity",
                    types=["Frigate"])
            entref.types = []

class EntityRefProblems(unit.TestCase):
    desc = "Test EntityRef with wrong type:"

    def setUp(self):
        self.ref = coe.EntityRef(identifier="TestRef",val="TestEntity",
                types=["Frigate"])
        self.mockProb = mock.MagicMock()
        self.probType = coe.WrongEntityTypeProb("TestEntity.entity",
                ["Frigate"])
        self.mod = tco.genMockMod("./")
        self.mod.entities.checkEntityType.return_value = coe.TypeCheckResult.invalid
        self.mod.entities.checkManifest.return_value = [self.mockProb]
        self.probList = [self.mockProb,self.probType]

    def testCheckProblems(self):
        """ Test whether the correct problems are returned
        """
        res = self.ref.check(self.mod)
        self.assertEqual(len(res),2)
        self.assertIn(self.probList[0],res)
        self.assertIn(self.probList[1],res)
        self.mod.entities.checkEntityType.assert_called_once_with(
                "TestEntity.entity",["Frigate"])
        self.mod.entities.checkManifest.assert_called_once_with(
                "TestEntity.entity")

class EntityRefFileMissing(unit.TestCase):
    desc = "Test EntityRef with entity file missing:"

    def setUp(self):
        self.ref = coe.EntityRef(identifier="TestRef",val="MissingEntity",
                types=["Frigate"])
        self.mockProb = mock.MagicMock()
        self.mod = tco.genMockMod("./")
        self.mod.entities.checkEntityType.return_value = coe.TypeCheckResult.na
        self.mod.entities.checkManifest.return_value = []

    def testCheckProblems(self):
        """ Test whether Missing File Problem is returned
        """
        res = self.ref.check(self.mod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.assertEqual(res[0].ref,"MissingEntity.entity")
        self.assertEqual(self.mod.entities.checkEntityType.call_count,0)
        self.assertEqual(self.mod.entities.checkManifest.call_count,0)

"""############################# Tests Entity ##############################"""

class EntityTest(unit.TestCase):
    desc = "Test Entity class:"

    @classmethod
    def setUpClass(cls):
        cls.patcher = mock.patch.object(coe.Entity,"__abstractmethods__",
                set())
        cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.patcher.stop()

    def setUp(self):
        self.parseString = "TXT\nentityType \"Frigate\"\n"
        self.entityType = {"identifier":"entityType","val":"Frigate"}
        self.inst = coe.Entity(filepath="Debris.entity",
                entityType=self.entityType)
        self.mmod = tco.genMockMod("./")

    def testCreationEntityType(self):
        """ Test whether entityType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.POSSIBLE_ENTITY_TYPES,
                **self.entityType)
        self.assertEqual(self.inst.etype,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testParseType(self):
        """ Test whether parseType returns correct type
        """
        exp = "Debris"
        res = coe.Entity.parseType("Debris.entity")
        self.assertEqual(res,exp)

    def testParseTypeWrongCase(self):
        """ Test whether parseType is case insensitive for filepaths
        """
        exp = "Debris"
        res = coe.Entity.parseType("DebRIS.entity")
        self.assertEqual(res,exp)

    def testParseTypeMalformed(self):
        """ Test whether parseType handles malformed files correctly
        """
        res = coe.Entity.parseType("malformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst = coe.Entity(filepath="Debris.entity",
                entityType=self.entityType)
        self.assertEqual(inst,self.inst)

    def testDiffEType(self):
        """ Test whether instances with differing etype attribute test equal
        """
        diff = {"identifier":"entityType","val":"Planet"}
        inst = coe.Entity(filepath="Debris.entity",
                entityType=diff)
        self.assertNotEqual(inst,self.inst)

    def testCheckEType(self):
        """ Test whether check checks etype attribute
        """
        with mock.patch.object(self.inst.etype,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)

"""######################### Tests MainViewVisible #########################"""

class MainViewVisibleFixtures(object):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
        )
        cls.zoomDist = {"identifier":"minZoomDistanceMult","val":1.85}
        cls.shadowMin = {"identifier":"minShadow","val":0.0}
        cls.shadowMax = {"identifier":"maxShadow","val":0.5}

class MainViewVisibleTest(MainViewVisibleFixtures,unit.TestCase):
    desc = "Tests for MainViewVisible property:"

    def setUp(self):
        self.inst = coe.MainViewVisible(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parser = coe.g_zoomDist+coe.g_shadows
        parseRes = parser.parseString(self.parseString)
        inst = coe.MainViewVisible(**parseRes)
        self.assertEqual(inst,self.inst)

    def testCreationAttribZoomDist(self):
        """ Test whether attribute zoomDist is created correctly
        """
        exp = co_attribs.AttribNum(**self.zoomDist)
        self.assertEqual(self.inst.zoomDist,exp)

    def testCreationAttribShadowMin(self):
        """ Test whether attribute shadowMin is created correctly
        """
        exp = co_attribs.AttribNum(**self.shadowMin)
        self.assertEqual(self.inst.shadowMin,exp)

    def testCreationAttribShadowMax(self):
        """ Test whether attribute shadowMax is created correctly
        """
        exp = co_attribs.AttribNum(**self.shadowMax)
        self.assertEqual(self.inst.shadowMax,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        self.assertEqual(self.inst.check(self.mmod),[])

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst1 = coe.MainViewVisible(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax
        )
        self.assertEqual(inst1,self.inst)

    def testEqualityDifferZoomDist(self):
        """ Test whether instances differing in zoomDist test unequal
        """
        other = {"identifier":"minZoomDistanceMult","val":1.25}
        inst1 = coe.MainViewVisible(
                zoomDist=other,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax
        )
        self.assertNotEqual(inst1,self.inst)

    def testEqualityDifferShadowMin(self):
        """ Test whether instances differing in shadowMin test unequal
        """
        other = {"identifier":"minShadow","val":0.2}
        inst1 = coe.MainViewVisible(
                zoomDist=self.zoomDist,
                shadowMin=other,
                shadowMax=self.shadowMax
        )
        self.assertNotEqual(inst1,self.inst)

    def testEqualityDifferShadowMax(self):
        """ Test whether instances differing in shadowMax test unequal
        """
        other = {"identifier":"maxShadow","val":0.8}
        inst1 = coe.MainViewVisible(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=other
        )
        self.assertNotEqual(inst1,self.inst)

"""############################ Tests UIVisible ############################"""

class UIVisibleFixtures(object):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
        )
        cls.iconHud = {"identifier":"hudIcon","val":"hudicon"}
        cls.iconInfocard = {"identifier":"infoCardIcon","val":"infocardicon"}
        cls.iconHudSmall = {"identifier":"smallHudIcon","val":"smallhudicon"}
        cls.descString = {"identifier":"descStringID","val":"descstring"}
        cls.descStringLong = {"identifier":"descriptionStringID",
            "val":"descstring"}
        cls.nameString = {"identifier":"nameStringID","val":"namestring"}
        cls.descStringCap = {"identifier":"DescriptionStringID",
            "val":"descstring"}
        cls.nameStringCap = {"identifier":"NameStringID","val":"namestring"}

class UIVisibleTest(UIVisibleFixtures,unit.TestCase):
    desc = "Tests for UIVisible property:"

    def setUp(self):
        self.inst = coe.UIVisible(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parser = (coe.g_uiIcons+coe.g_descriptionStringShort+coe.g_nameString)
        parseRes = parser.parseString(self.parseString)
        inst1 = coe.UIVisible(**parseRes)
        self.assertEqual(inst1,self.inst)

    def testCreationDescString(self):
        """ Test whether descString attribute is constructed correctly
        """
        exp = ui.StringReference(**self.descString)
        self.assertEqual(self.inst.descString,exp)

    def testCreationNameString(self):
        """ Test whether descString attribute is constructed correctly
        """
        exp = ui.StringReference(**self.nameString)
        self.assertEqual(self.inst.nameString,exp)

    def testCreationIconHud(self):
        """ Test whether iconHud attribute is constructed correctly
        """
        exp = ui.BrushRef(**self.iconHud)
        self.assertEqual(self.inst.iconHud,exp)

    def testCreationIconInfocard(self):
        """ Test whether iconInfocard attribute is constructed correctly
        """
        exp = ui.BrushRef(**self.iconInfocard)
        self.assertEqual(self.inst.iconInfocard,exp)

    def testCreationIconHudSmall(self):
        """ Test whether iconHudSmall attribute is constructed correctly
        """
        exp = ui.BrushRef(**self.iconHudSmall)
        self.assertEqual(self.inst.iconHudSmall,exp)

    def testCheck(self):
        """ Test whether check correctly returns empty list of problems
        """
        self.mmod.strings.checkString.return_value = []
        self.mmod.brushes.checkBrush.return_value = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckStringProblems(self):
        """ Test whether check correctly returns string problems
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.strings.checkString.return_value = [mockProb]
        self.mmod.brushes.checkBrush.return_value = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mockProb])

    def testCheckBrushProblems(self):
        """ Test whether check correctly returns brush problems
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.strings.checkString.return_value = []
        self.mmod.brushes.checkBrush.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mockProb])

"""######################## Tests MainViewSelectable #######################"""

class MainViewSelectableFixture(UIVisibleFixtures,MainViewVisibleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"mainViewIcon \"mainviewicon\"\n"
            +"picture \"picture\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
        )
        cls.mainViewIcon = {"identifier":"mainViewIcon","val":"mainviewicon"}
        cls.pic = {"identifier":"picture","val":"picture"}

class MainViewSelectableTest(MainViewSelectableFixture,unit.TestCase):
    desc = "Tests for MainViewSelectable property:"

    def setUp(self):
        self.inst = coe.MainViewSelectable(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_mainViewIcon
            +coe.g_picture
            +coe.g_zoomDist
            +coe.g_shadows
        )
        parseRes = parser.parseString(self.parseString)
        inst1 = coe.MainViewSelectable(**parseRes)
        self.assertEqual(inst1,self.inst)

    def testCreationAttribMainViewIcon(self):
        """ Test whether attribute mainViewIcon is constructed correctly
        """
        exp = ui.BrushRef(**self.mainViewIcon)
        self.assertEqual(self.inst.iconMainView,exp)

    def testCreationAttribPic(self):
        """ Test whether attribute pic is constructed correctly
        """
        exp = ui.BrushRef(**self.pic)
        self.assertEqual(self.inst.pic,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        self.mmod.brushes.checkBrush.return_value = []
        self.mmod.strings.checkString.return_value = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

"""############################# Tests PrereqDef ###########################"""

class PrereqDefTests(unit.TestCase):
    desc = "Tests PrereqDef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "ResearchPrerequisite\n"
            +"\tSubject \"Testsubject\"\n"
            +"\tLevel 2\n"
        )
        cls.identifier = "ResearchPrerequisite"
        cls.subject = {"identifier":"Subject","val":"Testsubject"}
        cls.level = {"identifier":"Level","val":2}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = coe.PrereqDef(
                identifier=self.identifier,
                subject=self.subject,
                level=self.level
        )

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = coe.g_researchPrerequesite.parseString(self.parseString)[0]
        res = coe.PrereqDef(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSubject(self):
        """ Test whether subject attribute is created correctly
        """
        exp = coe.EntityRef(types=["ResearchSubject"],**self.subject)
        self.assertEqual(self.inst.subject,exp)

    def testCreationAttribLevel(self):
        """ Test whether level attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.level)
        self.assertEqual(self.inst.level,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        with mock.patch.object(self.inst.subject,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests ResearchPrerequisite #####################"""

class ResearchPrerequisiteTests(unit.TestCase):
    desc = "Tests ResearchPrerequisite:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "Prerequisites\n"
            +"\tNumResearchPrerequisites 1\n"
            +"\tResearchPrerequisite\n"
            +"\t\tSubject \"Testsubject\"\n"
            +"\t\tLevel 2\n"
            +"\tRequiredFactionNameID \"Testfaction\"\n"
            +"\tRequiredCompletedResearchSubjects 2\n"
        )
        cls.identifier = "Prerequisites"
        cls.prereqs = {
            "counter":{"identifier":"NumResearchPrerequisites","val":1},
            "elements":[
                {
                    "identifier":"ResearchPrerequisite",
                    "subject":{"identifier":"Subject","val":"Testsubject"},
                    "level":{"identifier":"Level","val":2}
                }
            ]
        }
        cls.reqFaction = {"identifier":"RequiredFactionNameID",
            "val":"Testfaction"}
        cls.reqCompSubjects = {
            "identifier":"RequiredCompletedResearchSubjects",
            "val":2}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = coe.ResearchPrerequisite(
                identifier=self.identifier,
                prereqs=self.prereqs,
                reqFaction=self.reqFaction,
                reqCompSubjects=self.reqCompSubjects
        )

    def testCreationParse(self):
        """ Test whether instance is constructed correctly from parse result
        """
        parseRes = coe.g_prereqs.parseString(self.parseString)[0]
        res = coe.ResearchPrerequisite(**parseRes)
        self.assertEqual(self.inst,res)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is constructed correctly
        """
        exp = co_attribs.AttribList(elemType=coe.PrereqDef,**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCreationAttribReqFaction(self):
        """ Test whether reqFaction attribute is constructed correctly
        """
        exp = ui.StringReference(canBeEmpty=True,**self.reqFaction)
        self.assertEqual(self.inst.reqFaction,exp)

    def testCreationAttribReqCompSubjects(self):
        """ Test whether reqCompSubjects attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.reqCompSubjects)
        self.assertEqual(self.inst.reqCompSubjects,exp)

    def testCheck(self):
        """ Test whether check returns correct problems
        """
        mock.patch.object(self.inst.prereqs,"check",autospec=True,
                return_value=[]).start()
        mock.patch.object(self.inst.reqFaction,"check",autospec=True,
                return_value=[]).start()
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
        mock.patch.stopall()

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""################################ Tests Cost #############################"""

class CostTests(unit.TestCase):
    desc = "Tests Cost:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "price\n"
            +"\tcredits 100\n"
            +"\tmetal 200\n"
            +"\tcrystal 300\n"
        )
        cls.identifier = "price"
        cls.credits = {"identifier":"credits","val":100}
        cls.metal = {"identifier":"metal","val":200}
        cls.crystal = {"identifier":"crystal","val":300}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = coe.Cost(
                identifier=self.identifier,
                credits=self.credits,
                metal=self.metal,
                crystal=self.crystal
        )

    def testCreationParse(self):
        """ Test whether instance is constructed correctly from parse result
        """
        parseRes = coe.g_price.parseString(self.parseString)[0]
        res = coe.Cost(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationCredits(self):
        """ Test whether credits attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.credits)
        self.assertEqual(self.inst.credits,exp)

    def testCreationMetal(self):
        """ Test whether metal attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.metal)
        self.assertEqual(self.inst.metal,exp)

    def testCreationCrystal(self):
        """ Test whether crystal attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.crystal)
        self.assertEqual(self.inst.crystal,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests HasAbilities ##########################"""

class HasAbilitiesFixtures(MainViewVisibleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "ability:0 \"ab0\"\n"
            +"ability:1 \"ab1\"\n"
            +"ability:2 \"ab2\"\n"
            +"ability:3 \"ab3\"\n"
            +"ability:4 \"ab4\"\n"
            +"AntiMatterRestoreRate 50.0\n"
            +"MaxAntiMatter 100.0\n"
        )
        cls.abilities = [
            {"identifier":"ability:0","val":"ab0"},
            {"identifier":"ability:1","val":"ab1"},
            {"identifier":"ability:2","val":"ab2"},
            {"identifier":"ability:3","val":"ab3"},
            {"identifier":"ability:4","val":"ab4"}
        ]
        cls.antimatterMax = {"identifier":"MaxAntiMatter","val":100.0}
        cls.antimatterMaxLvl = {
            "identifier":"MaxAntiMatter",
            "startVal":{"identifier":"StartValue","val":225.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":20.0}
        }
        cls.antimatterRestore = {"identifier":"AntiMatterRestoreRate",
            "val":50.0}
        cls.antimatterRestoreLvl = {
            "identifier":"AntiMatterRestoreRate",
            "startVal":{"identifier":"StartValue","val":0.75},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.08}
        }

class HasAbilitiesTests(HasAbilitiesFixtures,unit.TestCase):
    desc = "Test HasAbilities:"

    def setUp(self):
        self.inst = coe.HasAbilities(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parser = (
            coe.g_zoomDist
            +coe.g_shadows
            +coe.g_abilities
            +coe.g_antimatterRestore
            +coe.g_maxAntimatter
        )
        parseRes = parser.parseString(self.parseString)
        inst = coe.HasAbilities(**parseRes)
        self.assertEqual(inst,self.inst)

    def testCreationAbilities(self):
        """ Test whether abilities attribute is contructed correctly
        """
        exp = (
            coe.EntityRef(canBeEmpty=True,types=["Ability"],
                    **self.abilities[0]),
            coe.EntityRef(canBeEmpty=True,types=["Ability"],
                    **self.abilities[1]),
            coe.EntityRef(canBeEmpty=True,types=["Ability"],
                    **self.abilities[2]),
            coe.EntityRef(canBeEmpty=True,types=["Ability"],
                    **self.abilities[3]),
            coe.EntityRef(canBeEmpty=True,types=["Ability"],
                    **self.abilities[4]),
        )
        self.assertEqual(self.inst.abilities,exp)

    def testCreationAttribAntimatterMax(self):
        """ Test whether antimatterMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.antimatterMax)
        self.assertEqual(self.inst.antimatterMax,exp)

    def testCreationAttribAntimatterRestore(self):
        """ Test whether antimatterRestore attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.antimatterRestore)
        self.assertEqual(self.inst.antimatterRestore,exp)

    def testCheck(self):
        """ Test whether check returns empty list when there are no problems
        """
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()

        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
        for ab in self.inst.abilities:
            ab.check.assert_called_once_with(self.mmod,False)
        mock.patch.stopall()

    def testCheckProbs(self):
        """ Test whether check returns problems in abilities attributes
        """
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        self.inst.abilities[1].check.return_value = [mock.sentinel.prob1]
        self.inst.abilities[3].check.return_value = [mock.sentinel.prob2]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob1,mock.sentinel.prob2])
        for ab in self.inst.abilities:
            ab.check.assert_called_once_with(self.mmod,False)
        mock.patch.stopall()

"""############################# Tests MeshInfo ############################"""

class MeshInfoTests(unit.TestCase):
    desc = "Tests MeshInfo:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "MeshInfo\n"
            +"\tmeshName \"testmesh\"\n"
        )
        cls.identifier = "MeshInfo"
        cls.mesh = {"identifier":"meshName","val":"testmesh"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = coe.MeshInfo(
                identifier=self.identifier,
                mesh=self.mesh
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genHeading("MeshInfo")
            +co_parse.genStringAttrib("meshName")("mesh")
        )
        parseRes = parser.parseString(self.parseString)
        res = coe.MeshInfo(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMesh(self):
        """ Test whether mesh attribute is created correctly
        """
        exp = meshes.MeshRef(**self.mesh)
        self.assertEqual(self.inst.mesh,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckMesh(self):
        """ Test whether check returns problems in mesh attribute correctly
        """
        with mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)
