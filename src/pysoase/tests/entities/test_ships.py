""" Tests for the pysoase.entities.ships module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.armed as armed
import pysoase.entities.common as coe
import pysoase.entities.hitable as hit
import pysoase.entities.ships as ships
import pysoase.mod.audio as audio
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_armed import ConstructableFixtures, \
    CarriesFightersFixtures
from pysoase.tests.entities.test_ecommon import MainViewVisibleFixtures

testdata = os.path.join("entities","ships")

"""############################# Tests Moving ##############################"""

class MovingFixtures(MainViewVisibleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            cls.parseString
            +"mass 10.0\n"
            +"maxAccelerationLinear 1.0\n"
            +"maxAccelerationStrafe 2.0\n"
            +"maxDecelerationLinear 3.0\n"
            +"maxAccelerationAngular 4.0\n"
            +"maxDecelerationAngular 5.0\n"
            +"maxSpeedLinear 6.0\n"
            +"maxRollRate 7.0\n"
            +"maxRollAngle 8.0\n"
            +"EngineSoundID \"sndengine\"\n"
            +"ExhaustParticleSystemName \"exhaustsys\"\n"
        )
        cls.mass = {"identifier":"mass","val":10.0}
        cls.exhaustSys = {"identifier":"ExhaustParticleSystemName",
            "val":"exhaustsys"}
        cls.exhaustSysS = {"identifier":"exhaustParticleSystemName",
            "val":"exhaustsys"}
        cls.engineSound = {"identifier":"EngineSoundID","val":"sndengine"}
        cls.engineSoundS = {"identifier":"engineSoundID","val":"sndengine"}
        cls.maxAccelLin = {"identifier":"maxAccelerationLinear","val":1.0}
        cls.maxAccelStrafe = {"identifier":"maxAccelerationStrafe","val":2.0}
        cls.maxDecelLin = {"identifier":"maxDecelerationLinear","val":3.0}
        cls.maxAccelAng = {"identifier":"maxAccelerationAngular","val":4.0}
        cls.maxDecelAng = {"identifier":"maxDecelerationAngular","val":5.0}
        cls.maxSpeedLin = {"identifier":"maxSpeedLinear","val":6.0}
        cls.maxRollRate = {"identifier":"maxRollRate","val":7.0}
        cls.maxRollAngle = {"identifier":"maxRollAngle","val":8.0}
        cls.mmod = tco.genMockMod("./")

class MovingTests(MovingFixtures,unit.TestCase):
    desc = "Tests Moving:"

    def setUp(self):
        self.inst = ships.Moving(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle
        )
        patcher = mock.patch.object(self.inst.exhaustSys,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockExhaustSys = patcher.start()
        patcher = mock.patch.object(self.inst.engineSound,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockEngineSound = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_zoomDist
            +coe.g_shadows
            +coe.g_mass
            +coe.g_movement
            +ships.g_engineSound
            +ships.g_exhaustSys
        )
        parseRes = parser.parseString(self.parseString)
        res = ships.Moving(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMass(self):
        """ Test whether mass attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.mass)
        self.assertEqual(self.inst.mass,exp)

    def testCreationAttribExhaustSys(self):
        """ Test whether exhaustSys attrib is created correctly
        """
        exp = par.ParticleRef(**self.exhaustSys)
        self.assertEqual(self.inst.exhaustSys,exp)

    def testCreationAttribEngineSound(self):
        """ Test whether engineSound attrib is created correctly
        """
        exp = audio.SoundRef(**self.engineSound)
        self.assertEqual(self.inst.engineSound,exp)

    def testCreationAttribMaxAccelLin(self):
        """ Test whether maxAccelLin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxAccelLin)
        self.assertEqual(self.inst.maxAccelLin,exp)

    def testCreationAttribMaxAccelStrafe(self):
        """ Test whether maxAccelStrafe attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxAccelStrafe)
        self.assertEqual(self.inst.maxAccelStrafe,exp)

    def testCreationAttribMaxDecelLin(self):
        """ Test whether maxDecelLin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxDecelLin)
        self.assertEqual(self.inst.maxDecelLin,exp)

    def testCreationAttribMaxAccelAng(self):
        """ Test whether maxAccelAng attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxAccelAng)
        self.assertEqual(self.inst.maxAccelAng,exp)

    def testCreationAttribMaxDecelAng(self):
        """ Test whether maxDecelAng attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxDecelAng)
        self.assertEqual(self.inst.maxDecelAng,exp)

    def testCreationAttribMaxSpeedLin(self):
        """ Test whether maxSpeedLin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxSpeedLin)
        self.assertEqual(self.inst.maxSpeedLin,exp)

    def testCreationAttribMaxRollRate(self):
        """ Test whether maxRollRate attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxRollRate)
        self.assertEqual(self.inst.maxRollRate,exp)

    def testCreationAttribMaxRollAngle(self):
        """ Test whether maxRollAngle attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxRollAngle)
        self.assertEqual(self.inst.maxRollAngle,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckExhaustSys(self):
        """ Test whether check returns problems in exhaustSys attrib
        """
        self.mockExhaustSys.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockExhaustSys.assert_called_once_with(self.mmod,False)

    def testCheckEngineSound(self):
        """ Test whether check returns problems in engineSound attrib
        """
        self.mockEngineSound.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockEngineSound.assert_called_once_with(self.mmod,False)

"""########################### Tests Bombardment ###########################"""

class BombardmentTests(unit.TestCase):
    desc = "Tests Bombardment:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "hasBombingLevels TRUE\n"
            +"baseDamage\n"
            +"\tLevel:0 20.000000\n"
            +"\tLevel:1 30.000000\n"
            +"\tLevel:2 40.000000\n"
            +"\tLevel:3 50.000000\n"
            +"basePopulationKilled\n"
            +"\tLevel:0 20.000000\n"
            +"\tLevel:1 30.000000\n"
            +"\tLevel:2 40.000000\n"
            +"\tLevel:3 50.000000\n"
            +"bombingFreqTime 10.000000\n"
            +"baseRange 6000.000000\n"
            +"bombTransitTime 4.000000\n"
            +"bombEffectCount 2\n"
            +"bombEffectAngleVariance 2.500000\n"
            +"bombEffectsDef\n"
            +"\tweaponType \"Projectile\"\n"
            +"\tburstCount 1\n"
            +"\tburstDelay 0.000000\n"
            +"\tfireDelay 0.000000\n"
            +"\tmuzzleEffectName \"effmuzzle\"\n"
            +"\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\tmuzzleSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndmuzzle\"\n"
            +"\thitEffectName \"effhit\"\n"
            +"\thitHullEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndhullhit\"\n"
            +"\thitShieldsEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndshieldhit\"\n"
            +"\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.hasLevels = {"identifier":"hasBombingLevels","val":True}
        cls.baseDamage = {
            "identifier":"baseDamage",
            "levels":[
                {"identifier":"Level:0","val":20.0},
                {"identifier":"Level:1","val":30.0},
                {"identifier":"Level:2","val":40.0},
                {"identifier":"Level:3","val":50.0}
            ]
        }
        cls.basePopKilled = {
            "identifier":"basePopulationKilled",
            "levels":[
                {"identifier":"Level:0","val":20.0},
                {"identifier":"Level:1","val":30.0},
                {"identifier":"Level:2","val":40.0},
                {"identifier":"Level:3","val":50.0}
            ]
        }
        cls.bombingFreq = {"identifier":"bombingFreqTime","val":10.0}
        cls.baseRange = {"identifier":"baseRange","val":6000.0}
        cls.transitTime = {"identifier":"bombTransitTime","val":4.0}
        cls.effectCount = {"identifier":"bombEffectCount","val":2}
        cls.effectAngleVar = {"identifier":"bombEffectAngleVariance",
            "val":2.5}
        cls.effect = {
            "identifier":"bombEffectsDef",
            "effectType":{"identifier":"weaponType","val":"Projectile"},
            "burstCount":{"identifier":"burstCount","val":1},
            "burstDelay":{"identifier":"burstDelay","val":0.0},
            "fireDelay":{"identifier":"fireDelay","val":0.0},
            "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
            "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                "val":0.1},
            "sndMuzzle":{
                "identifier":"muzzleSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndmuzzle"}
                ]
            },
            "hitEff":{"identifier":"hitEffectName","val":"effhit"},
            "sndHitHull":{
                "identifier":"hitHullEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndhullhit"}
                ]
            },
            "sndHitShields":{
                "identifier":"hitShieldsEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndshieldhit"}
                ]
            },
            "travelEff":{"identifier":"projectileTravelEffectName",
                "val":"traveleff"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = ships.Bombardment(
                hasLevels=self.hasLevels,
                baseDamage=self.baseDamage,
                basePopKilled=self.basePopKilled,
                bombingFreq=self.bombingFreq,
                baseRange=self.baseRange,
                transitTime=self.transitTime,
                effectCount=self.effectCount,
                effectAngleVar=self.effectAngleVar,
                effect=self.effect
        )
        patcher = mock.patch.object(self.inst.effect,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockEffect = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = ships.g_bombardment.parseString(self.parseString)[0]
        res = ships.Bombardment(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribHasLevels(self):
        """ Test whether hasLevels attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.hasLevels)
        self.assertEqual(self.inst.hasLevels,exp)

    def testCreationAttribBaseDamage(self):
        """ Test whether baseDamage attribute is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.baseDamage)
        self.assertEqual(self.inst.baseDamage,exp)

    def testCreationAttribBasePopKilled(self):
        """ Test whether basePopKilled attribute is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.basePopKilled)
        self.assertEqual(self.inst.basePopKilled,exp)

    def testCreationAttribBombingFreq(self):
        """ Test whether bombingFreq attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.bombingFreq)
        self.assertEqual(self.inst.bombingFreq,exp)

    def testCreationAttribBaseRange(self):
        """ Test whether baseRange attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.baseRange)
        self.assertEqual(self.inst.baseRange,exp)

    def testCreationAttribTransitTime(self):
        """ Test whether transitTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.transitTime)
        self.assertEqual(self.inst.transitTime,exp)

    def testCreationAttribEffectCount(self):
        """ Test whether effectCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.effectCount)
        self.assertEqual(self.inst.effectCount,exp)

    def testCreationAttribEffectAngleVar(self):
        """ Test whether effectAngleVar attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.effectAngleVar)
        self.assertEqual(self.inst.effectAngleVar,exp)

    def testCreationAttribEffect(self):
        """ Test whether effect attribute is created correctly
        """
        exp = armed.SharedWeaponEffect.factory(**self.effect)
        self.assertEqual(self.inst.effect,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckEffect(self):
        """ Test whether check returns problems from effect attribute
        """
        self.mockEffect.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Ship ###############################"""

class ShipFixtures(ConstructableFixtures,CarriesFightersFixtures,
        MovingFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "hudIcon \"hudicon\"\n"
            +"smallHudIcon \"smallhudicon\"\n"
            +"infoCardIcon \"infocardicon\"\n"
            +"descStringID \"descstring\"\n"
            +"nameStringID \"namestring\"\n"
            +"minZoomDistanceMult 1.85\n"
            +"minShadow 0.0\n"
            +"maxShadow 0.5\n"
            +"armorType \"Titan\"\n"
            +"BaseArmorPoints 12.0\n"
            +"ExplosionName \"testexplosion\"\n"
            +"experiencePointsForDestroying 10.0\n"
            +"HullPointRestoreRate 1.0\n"
            +"MaxHullPoints 2.0\n"
            +"numRandomDebrisLarge 1.0\n"
            +"numRandomDebrisSmall 1.0\n"
            +"numSpecificDebris 1\n"
            +"\tspecificDebrisMeshName \"debris\"\n"
            +"mainViewIcon \"mainviewicon\"\n"
            +"picture \"picture\"\n"
            +"ability:0 \"ab0\"\n"
            +"ability:1 \"ab1\"\n"
            +"ability:2 \"ab2\"\n"
            +"ability:3 \"ab3\"\n"
            +"ability:4 \"ab4\"\n"
            +"AntiMatterRestoreRate 50.0\n"
            +"MaxAntiMatter 100.0\n"
            +"NumSoundsFor:ONATTACKORDERISSUED 1\n"
            +"SoundID \"attackorder\"\n"
            +"NumSoundsFor:ONCREATION 1\n"
            +"SoundID \"creation\"\n"
            +"NumSoundsFor:ONGENERALORDERISSUED 1\n"
            +"SoundID \"generalorder\"\n"
            +"NumSoundsFor:ONSELECTED 1\n"
            +"SoundID \"selected\"\n"
            +"NumSoundsFor:ONSTARTPHASEJUMP 1\n"
            +"SoundID \"phasejump\"\n"
            +"MaxShieldPoints 12.0\n"
            +"ShieldPointRestoreRate 17.0\n"
            +"maxShieldMitigation 20.0\n"
            +"ShieldMeshName \"testshield\"\n"
            +"renderShield TRUE\n"
            +"basePrice\n"
            +"\tcredits 100\n"
            +"\tmetal 200\n"
            +"\tcrystal 300\n"
            +"formationRank 2\n"
            +"meshNameIncreasedEffectName \"incmesh\"\n"
            +"meshNameDecreasedEffectName \"decmesh\"\n"
            +"MeshNameInfoCount 1\n"
            +"MeshNameInfo\n"
            +"\tmeshName \"testmesh\"\n"
            +"\tcriteriaType \"None\"\n"
            +"Prerequisites\n"
            +"\tNumResearchPrerequisites 1\n"
            +"\tResearchPrerequisite\n"
            +"\t\tSubject \"Testsubject\"\n"
            +"\t\tLevel 2\n"
            +"\tRequiredFactionNameID \"Testfaction\"\n"
            +"\tRequiredCompletedResearchSubjects 2\n"
            +"statCountType \"ModulePointDefense\"\n"
            +"baseBuildTime 6.0\n"
            +"defaultAutoAttackRange \"None\"\n"
            +"defaultAutoAttackOn TRUE\n"
            +"prefersToFocusFire TRUE\n"
            +"usesFighterAttack FALSE\n"
            +"NumWeapons 1\n"
            +"Weapon\n"
            +"\tWeaponType \"Beam\"\n"
            +"\tdamageEnums\n"
            +"\t\tAttackType \"TITAN\"\n"
            +"\t\tDamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\t\tDamageApplyType \"OVERTIME\"\n"
            +"\t\tDamageType \"PHYSICAL\"\n"
            +"\t\tWeaponClassType \"BEAM\"\n"
            +"\tDamagePerBank:FRONT 1.0\n"
            +"\tDamagePerBank:BACK 1.0\n"
            +"\tDamagePerBank:LEFT 1.0\n"
            +"\tDamagePerBank:RIGHT 1.0\n"
            +"\tRange 10.0\n"
            +"\tPreBuffCooldownTime 0.5\n"
            +"\tCanFireAtFighter TRUE\n"
            +"\tSynchronizedTargeting TRUE\n"
            +"\tPointStaggerDelay 4.0\n"
            +"\tTravelSpeed 0.5\n"
            +"\tDuration 7.0\n"
            +"\tfireConstraintType \"CanAlwaysFire\"\n"
            +"\tWeaponEffects\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.0\n"
            +"\t\tfireDelay 0.0\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.1\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
            +"m_weaponIndexForRange 1\n"
            +"firingAlignmentType \"Default\"\n"
            +"TargetCountPerBank:FRONT 1\n"
            +"TargetCountPerBank:BACK 0\n"
            +"TargetCountPerBank:LEFT 2\n"
            +"TargetCountPerBank:RIGHT 2\n"
            +"canOnlyTargetStructures FALSE\n"
            +"squadTypeEntityDef:0 \"testsquad\"\n"
            +"squadAntiMatterCost:0 5.0\n"
            +"squadTypeEntityDef:1 \"testsquad\"\n"
            +"squadAntiMatterCost:1 5.0\n"
            +"squadTypeEntityDef:2 \"testsquad\"\n"
            +"squadAntiMatterCost:2 5.0\n"
            +"squadTypeEntityDef:3 \"testsquad\"\n"
            +"squadAntiMatterCost:3 5.0\n"
            +"baseCommandPoints 3\n"
            +"mass 10.0\n"
            +"maxAccelerationLinear 1.0\n"
            +"maxAccelerationStrafe 2.0\n"
            +"maxDecelerationLinear 3.0\n"
            +"maxAccelerationAngular 4.0\n"
            +"maxDecelerationAngular 5.0\n"
            +"maxSpeedLinear 6.0\n"
            +"maxRollRate 7.0\n"
            +"maxRollAngle 8.0\n"
            +"EngineSoundID \"sndengine\"\n"
            +"ExhaustParticleSystemName \"exhaustsys\"\n"
            +"autoJoinFleetDefault TRUE\n"
            +"canBomb TRUE\n"
            +"hasBombingLevels TRUE\n"
            +"baseDamage\n"
            +"\tLevel:0 20.0\n"
            +"\tLevel:1 30.0\n"
            +"\tLevel:2 40.0\n"
            +"\tLevel:3 50.0\n"
            +"basePopulationKilled\n"
            +"\tLevel:0 20.0\n"
            +"\tLevel:1 30.0\n"
            +"\tLevel:2 40.0\n"
            +"\tLevel:3 50.0\n"
            +"bombingFreqTime 10.0\n"
            +"baseRange 6000.0\n"
            +"bombTransitTime 4.0\n"
            +"bombEffectCount 2\n"
            +"bombEffectAngleVariance 2.5\n"
            +"bombEffectsDef\n"
            +"\tweaponType \"Projectile\"\n"
            +"\tburstCount 1\n"
            +"\tburstDelay 0.0\n"
            +"\tfireDelay 0.0\n"
            +"\tmuzzleEffectName \"effmuzzle\"\n"
            +"\tmuzzleSoundMinRespawnTime 0.1\n"
            +"\tmuzzleSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndmuzzle\"\n"
            +"\thitEffectName \"effhit\"\n"
            +"\thitHullEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndhullhit\"\n"
            +"\thitShieldsEffectSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"sndshieldhit\"\n"
            +"\tprojectileTravelEffectName \"traveleff\"\n"
            +"HyperspaceChargingSoundID \"hypercharge\"\n"
            +"HyperspaceTravelSoundID \"hypertravel\"\n"
            +"slotCount 0.0\n"
        )

        cls.autoJoinFleet = {"identifier":"autoJoinFleetDefault","val":True}
        cls.canBomb = {"identifier":"canBomb","val":True}
        cls.bombardment = {
            "hasLevels":{"identifier":"hasBombingLevels","val":True},
            "baseDamage":{
                "identifier":"baseDamage",
                "levels":[
                    {"identifier":"Level:0","val":20.0},
                    {"identifier":"Level:1","val":30.0},
                    {"identifier":"Level:2","val":40.0},
                    {"identifier":"Level:3","val":50.0}
                ]
            },
            "basePopKilled":{
                "identifier":"basePopulationKilled",
                "levels":[
                    {"identifier":"Level:0","val":20.0},
                    {"identifier":"Level:1","val":30.0},
                    {"identifier":"Level:2","val":40.0},
                    {"identifier":"Level:3","val":50.0}
                ]
            },
            "bombingFreq":{"identifier":"bombingFreqTime","val":10.0},
            "baseRange":{"identifier":"baseRange","val":6000.0},
            "transitTime":{"identifier":"bombTransitTime","val":4.0},
            "effectCount":{"identifier":"bombEffectCount","val":2},
            "effectAngleVar":{"identifier":"bombEffectAngleVariance",
                "val":2.5},
            "effect":{
                "identifier":"bombEffectsDef",
                "effectType":{"identifier":"weaponType","val":"Projectile"},
                "burstCount":{"identifier":"burstCount","val":1},
                "burstDelay":{"identifier":"burstDelay","val":0.0},
                "fireDelay":{"identifier":"fireDelay","val":0.0},
                "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
                "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                    "val":0.1},
                "sndMuzzle":{
                    "identifier":"muzzleSounds",
                    "counter":{"identifier":"soundCount","val":1},
                    "elements":[
                        {"identifier":"sound","val":"sndmuzzle"}
                    ]
                },
                "hitEff":{"identifier":"hitEffectName","val":"effhit"},
                "sndHitHull":{
                    "identifier":"hitHullEffectSounds",
                    "counter":{"identifier":"soundCount","val":1},
                    "elements":[
                        {"identifier":"sound","val":"sndhullhit"}
                    ]
                },
                "sndHitShields":{
                    "identifier":"hitShieldsEffectSounds",
                    "counter":{"identifier":"soundCount","val":1},
                    "elements":[
                        {"identifier":"sound","val":"sndshieldhit"}
                    ]
                },
                "travelEff":{"identifier":"projectileTravelEffectName",
                    "val":"traveleff"}
            }
        }
        cls.hyperChargingSnd = {"identifier":"HyperspaceChargingSoundID",
            "val":"hypercharge"}
        cls.hyperTravelSnd = {"identifier":"HyperspaceTravelSoundID",
            "val":"hypertravel"}
        cls.slotCount = {"identifier":"slotCount","val":0.0}

    def genMocks(self):
        self.mmod = tco.genMockMod("./")
        self.mmod.brushes.checkBrush.return_value = []
        self.mmod.strings.checkString.return_value = []
        self.mmod.audio.checkEffect.return_value = []
        patchers = []
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
            patchers.append(patcher)
        patcher = mock.patch.object(self.inst.shieldMesh,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.explosion,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.debrisSpecific,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshDecreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshIncreasedEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.meshes,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.bombardment,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockBomb = patcher.start()
        patcher = mock.patch.object(self.inst.hyperChargingSnd,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockHyperCharge = patcher.start()
        patcher = mock.patch.object(self.inst.hyperTravelSnd,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockHyperTravel = patcher.start()
        patcher = mock.patch.object(self.inst.weapons,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        for ab in self.inst.squads:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        patcher = mock.patch.object(self.inst.exhaustSys,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.engineSound,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        self.addCleanup(mock.patch.stopall)

class ShipTests(ShipFixtures,unit.TestCase):
    desc = "Tests Ship:"

    def setUp(self):
        self.inst = ships.Ship(
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descString,
                nameString=self.nameString,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.exp,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                commandPoints=self.commandPoints,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                autoJoinFleet=self.autoJoinFleet,
                canBomb=self.canBomb,
                bombardment=self.bombardment,
                hyperChargingSnd=self.hyperChargingSnd,
                hyperTravelSnd=self.hyperTravelSnd,
                slotCount=self.slotCount
        )
        self.genMocks()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            coe.g_uiIcons
            +coe.g_descriptionStringShort
            +coe.g_nameString
            +coe.g_zoomDist
            +coe.g_shadows
            +hit.g_armorType
            +hit.g_baseArmorPoints
            +hit.g_explosion
            +hit.g_exp
            +hit.g_hullRestore
            +hit.g_hullPoints
            +coe.g_debris
            +coe.g_mainViewIcon
            +coe.g_picture
            +coe.g_abilities
            +coe.g_antimatterRestore
            +coe.g_maxAntimatter
            +hit.g_voiceSounds
            +hit.g_maxShield
            +hit.g_shieldRestore
            +hit.g_maxShieldMitigation
            +hit.g_shieldVisuals
            +coe.g_basePrice
            +armed.g_formation
            +armed.g_meshEffects
            +armed.g_criterionMeshInfos
            +coe.g_prereqs
            +armed.g_statType
            +armed.g_baseBuildTime
            +armed.g_attackBehaviour
            +armed.g_armed
            +armed.g_squadrons
            +co_parse.genIntAttrib("baseCommandPoints")("commandPoints")
            +coe.g_mass
            +coe.g_movement
            +ships.g_engineSound
            +ships.g_exhaustSys
            +ships.g_autoJoin
            +ships.g_canBombTrue
            +coe.g_hyperChargeSnd
            +coe.g_hyperTravelSnd
            +ships.g_slotCount
        )
        parseRes = parser.parseString(self.parseString)
        res = ships.Ship(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAutoJoinFleet(self):
        """ Test whether autoJoinFleet attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.autoJoinFleet)
        self.assertEqual(self.inst.autoJoinFleet,exp)

    def testCreationAttribCanBomb(self):
        """ Test whether canBomb attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.canBomb)
        self.assertEqual(self.inst.canBomb,exp)

    def testCreationAttribBombardment(self):
        """ Test whether bombardment attrib is created correctly
        """
        exp = ships.Bombardment(**self.bombardment)
        self.assertEqual(self.inst.bombardment,exp)

    def testCreationAttribHyperChargingSound(self):
        """ Test whether hyperChargingSnd attrib is created correctly
        """
        exp = audio.SoundRef(**self.hyperChargingSnd)
        self.assertEqual(self.inst.hyperChargingSnd,exp)

    def testCreationAttribHyperTravelSnd(self):
        """ Test whether hyperTravelSnd attrib is created correctly
        """
        exp = audio.SoundRef(**self.hyperTravelSnd)
        self.assertEqual(self.inst.hyperTravelSnd,exp)

    def testCreationAttribSlotCount(self):
        """ Test whether slotCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.slotCount)
        self.assertEqual(self.inst.slotCount,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckBombardment(self):
        """ Test whether check returns problems from bombardment attrib
        """
        self.mockBomb.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckHyperChargingSnd(self):
        """ Test whether check returns problems from hyperChargingSnd attrib
        """
        self.mockHyperCharge.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckHyperTravelSnd(self):
        """ Test whether check returns problems from hyperTravelSnd attrib
        """
        self.mockHyperTravel.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

"""############################# Tests Frigate #############################"""

class FrigateTests(ShipFixtures,unit.TestCase):
    desc = "Tests Frigate:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Frigate.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Frigate"}
        cls.roleTypes = {
            "counter":{"identifier":"typeCount","val":1},
            "elements":[
                {"identifier":"frigateRoleType","val":"Siege"}
            ]
        }
        cls.counterDesc = {"identifier":"counterDescriptionStringID",
            "val":"counterdesc"}
        cls.hasLevels = {"identifier":"hasLevels","val":False}
        cls.hasWeapLevels = {"identifier":"hasWeaponLevels","val":False}
        cls.cargoType = {"identifier":"cargoType","val":"Invalid"}
        cls.cargoCapacity = {"identifier":"maxCargoCapacity","val":0.0}
        cls.allegianceDecPerRoundtrip = {
            "identifier":"allegianceDecreasePerRoundtrip","val":0.0}
        cls.buildTime["identifier"] = "BuildTime"
        cls.shieldMitigation["identifier"] = "maxMitigation"
        cls.commandPoints["identifier"] = "maxNumCommandPoints"

    def setUp(self):
        self.inst = ships.Frigate(
                entityType=self.entityType,
                filepath=self.filepath,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPoints,
                explosion=self.explosion,
                exp=self.expCap,
                hullPointRestore=self.hullPointRestore,
                hullPointsMax=self.hullPointsMax,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                commandPoints=self.commandPoints,
                abilities=self.abilities,
                antimatterMax=self.antimatterMax,
                antimatterRestore=self.antimatterRestore,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMax,
                shieldPointRestore=self.shieldPointRestore,
                shieldMitigation=self.shieldMitigation,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                autoJoinFleet=self.autoJoinFleet,
                canBomb=self.canBomb,
                bombardment=self.bombardment,
                hyperChargingSnd=self.hyperChargingSnd,
                hyperTravelSnd=self.hyperTravelSnd,
                slotCount=self.slotCount,
                roleTypes=self.roleTypes,
                counterDesc=self.counterDesc,
                hasLevels=self.hasLevels,
                hasWeapLevels=self.hasWeapLevels,
                cargoType=self.cargoType,
                cargoCapacity=self.cargoCapacity,
                allegianceDecPerRoundtrip=self.allegianceDecPerRoundtrip
        )
        super().genMocks()
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = ships.Frigate.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationAttribRoleTypes(self):
        """ Test whether roleType attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":ships.FRIGATE_ROLE_TYPES},
                **self.roleTypes)
        self.assertEqual(self.inst.roleTypes,exp)

    def testCreationAttribCounterDesc(self):
        """ Test whether counterDesc attrib is created correctly
        """
        exp = ui.StringReference(**self.counterDesc)
        self.assertEqual(self.inst.counterDesc,exp)

    def testCreationAttribHasLevels(self):
        """ Test whether hasLevels attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.hasLevels)
        self.assertEqual(self.inst.hasLevels,exp)

    def testCreationAttribHasWeapLevels(self):
        """ Test whether hasWeapLevels attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.hasWeapLevels)
        self.assertEqual(self.inst.hasWeapLevels,exp)

    def testCreationAttribCargoType(self):
        """ Test whether cargoType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=ships.CARGO_TYPES,
                **self.cargoType)
        self.assertEqual(self.inst.cargoType,exp)

    def testCreationAttribCargoCapacity(self):
        """ Test whether cargoCapacity attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.cargoCapacity)
        self.assertEqual(self.inst.cargoCapacity,exp)

    def testCreationAttribAllegianceDecPerRoundtrip(self):
        """ Test whether allegianceDecPerRoundtrip attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.allegianceDecPerRoundtrip)
        self.assertEqual(self.inst.allegianceDecPerRoundtrip,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckCounterDesc(self):
        """ Test whether check returns problems from counter desc string
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.counterDesc,"check",autospec=True,
                spec_set=True,return_value=[mockProb]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests CapitalShip ###########################"""

class CapitalShipTests(ShipFixtures,unit.TestCase):
    desc = "Tests CapitalShip"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "CapitalShip.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"CapitalShip"}
        cls.cultureProtectRate = {
            "identifier":"CultureProtectRate",
            "startVal":{"identifier":"StartValue","val":0.3},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.05}
        }
        cls.roleType = {"identifier":"roleType","val":"Invalid"}
        cls.weapCooldownDec = {
            "identifier":"weaponCooldownDecreasePerc",
            "startVal":{"identifier":"StartValue","val":0.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.0315}
        }
        cls.weapDmgInc = {
            "identifier":"weaponDamageIncreasePerc",
            "startVal":{"identifier":"StartValue","val":0.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.05}
        }
        cls.buildTime["identifier"] = "BuildTime"
        cls.shieldMitigation["identifier"] = "maxMitigation"

    def setUp(self):
        self.inst = ships.CapitalShip(
                entityType=self.entityType,
                filepath=self.filepath,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPointsLvl,
                explosion=self.explosion,
                hullPointRestore=self.hullPointRestoreLvl,
                hullPointsMax=self.hullPointsMaxLvl,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                commandPoints=self.commandPointsLvl,
                abilities=self.abilities,
                antimatterMax=self.antimatterMaxLvl,
                antimatterRestore=self.antimatterRestoreLvl,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMaxLvl,
                shieldPointRestore=self.shieldPointRestoreLvl,
                shieldMitigation=self.maxMitigationLvl,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                autoJoinFleet=self.autoJoinFleet,
                canBomb=self.canBomb,
                bombardment=self.bombardment,
                hyperChargingSnd=self.hyperChargingSnd,
                hyperTravelSnd=self.hyperTravelSnd,
                slotCount=self.slotCount,
                cultureProtectRate=self.cultureProtectRate,
                roleType=self.roleType,
                weapCooldownDec=self.weapCooldownDec,
                weapDmgInc=self.weapDmgInc
        )
        self.genMocks()
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = ships.CapitalShip.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Titan ##############################"""

class TitanTests(ShipFixtures,unit.TestCase):
    desc = "Tests Titan"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Titan.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Titan"}
        cls.cultureProtectRate = {
            "identifier":"CultureProtectRate",
            "startVal":{"identifier":"StartValue","val":0.3},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.05}
        }
        cls.upgrades = {
            "counter":{"identifier":"UpgradeTypeCount","val":1},
            "elements":[
                {"identifier":"UpgradeType","val":"testupgrade"}
            ]
        }
        cls.weapCooldownDec = {
            "identifier":"weaponCooldownDecreasePerc",
            "startVal":{"identifier":"StartValue","val":0.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.0315}
        }
        cls.weapDmgInc = {
            "identifier":"weaponDamageIncreasePerc",
            "startVal":{"identifier":"StartValue","val":0.0},
            "perLvlInc":{"identifier":"ValueIncreasePerLevel","val":0.05}
        }
        cls.buildTime["identifier"] = "BuildTime"
        cls.shieldMitigation["identifier"] = "maxMitigation"

    def setUp(self):
        self.inst = ships.Titan(
                entityType=self.entityType,
                filepath=self.filepath,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                descString=self.descStringCap,
                nameString=self.nameStringCap,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                armorType=self.armorType,
                armorPoints=self.armorPointsLvl,
                explosion=self.explosion,
                hullPointRestore=self.hullPointRestoreLvl,
                hullPointsMax=self.hullPointsMaxLvl,
                debrisLarge=self.debrisLarge,
                debrisSmall=self.debrisSmall,
                debrisSpecific=self.debrisSpecific,
                autoattackRange=self.autoattackRange,
                autoattackOn=self.autoattackOn,
                focusFire=self.focusFire,
                usesFighterAttack=self.usesFighterAttack,
                weapons=self.weapons,
                indexForRange=self.indexForRange,
                firingAlignment=self.firingAlignment,
                targetsFront=self.targetsFront,
                targetsBack=self.targetsBack,
                targetsLeft=self.targetsLeft,
                targetsRight=self.targetsRight,
                onlyTargetStructures=self.onlyTargetStructures,
                squads=self.squads,
                commandPoints=self.commandPointsLvl,
                abilities=self.abilities,
                antimatterMax=self.antimatterMaxLvl,
                antimatterRestore=self.antimatterRestoreLvl,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                sndAttackOrder=self.sndAttackOrder,
                sndCreation=self.sndCreation,
                sndGeneralOrder=self.sndGeneralOrder,
                sndSelected=self.sndSelected,
                sndPhaseJump=self.sndPhaseJump,
                shieldPointsMax=self.shieldPointsMaxLvl,
                shieldPointRestore=self.shieldPointRestoreLvl,
                shieldMitigation=self.maxMitigationLvl,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                price=self.price,
                formationRank=self.formationRank,
                meshDecreasedEffect=self.meshDecreasedEffect,
                meshIncreasedEffect=self.meshIncreasedEffect,
                meshes=self.meshes,
                prereqs=self.prereqs,
                statcount=self.statcount,
                buildTime=self.buildTime,
                mass=self.mass,
                exhaustSys=self.exhaustSys,
                engineSound=self.engineSound,
                maxAccelLin=self.maxAccelLin,
                maxAccelStrafe=self.maxAccelStrafe,
                maxDecelLin=self.maxDecelLin,
                maxAccelAng=self.maxAccelAng,
                maxDecelAng=self.maxDecelAng,
                maxSpeedLin=self.maxSpeedLin,
                maxRollRate=self.maxRollRate,
                maxRollAngle=self.maxRollAngle,
                autoJoinFleet=self.autoJoinFleet,
                canBomb=self.canBomb,
                bombardment=self.bombardment,
                hyperChargingSnd=self.hyperChargingSnd,
                hyperTravelSnd=self.hyperTravelSnd,
                slotCount=self.slotCount,
                cultureProtectRate=self.cultureProtectRate,
                upgrades=self.upgrades,
                weapCooldownDec=self.weapCooldownDec,
                weapDmgInc=self.weapDmgInc
        )
        self.genMocks()
        self.mockUpgrades = mock.patch.object(self.inst.upgrades,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockUpgrades = self.mockUpgrades.start()
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = ships.Titan.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationAttribUpgrades(self):
        """ Test whether upgrades attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=coe.EntityRef,
                elemArgs={"types":["TitanUpgrade"]},
                **self.upgrades)
        self.assertEqual(self.inst.upgrades,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckUpgrades(self):
        """ Test whether check returns problems from upgrades attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mockUpgrades.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mockProb])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)
