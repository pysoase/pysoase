""" Unit tests for the pysoase.entities.misc module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.misc as misc
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
import pysoase.tests.entities.test_ecommon as tcoe

testdata = os.path.join("entities","misc")

"""############################## Tests Debris #############################"""

class DebrisTests(unit.TestCase):
    desc = "Test for Debris entity:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "Debris.entity"
        cls.entityType = {"identifier":"entityType","val":"Debris"}
        cls.lifetime = {"identifier":"averageLifetime","val":300.0}
        cls.lifetimeVar = {"identifier":"lifetimeVariancePerc","val":0.2}
        cls.speedLinMin = {"identifier":"minLinearSpeed","val":40.0}
        cls.speedLinMax = {"identifier":"maxLinearSpeed","val":60.0}
        cls.speedAngMin = {"identifier":"minAngularSpeed","val":0.1}
        cls.speedAngMax = {"identifier":"maxAngularSpeed","val":0.6}
        cls.dragRate = {"identifier":"dragRate","val":0.05}
        cls.shadowMin = {"identifier":"minShadow","val":0.0}
        cls.shadowMax = {"identifier":"maxShadow","val":0.0}

    def setUp(self):
        self.inst = misc.Debris(
                filepath=self.filepath,
                entityType=self.entityType,
                lifetime=self.lifetime,
                lifetimeVariance=self.lifetimeVar,
                speedLinMin=self.speedLinMin,
                speedLinMax=self.speedLinMax,
                speedAngMin=self.speedAngMin,
                speedAngMax=self.speedAngMax,
                dragRate=self.dragRate,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether createInstance creates instances correctly
        """
        res = misc.Debris.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether createInstance creates instances correctly
        """
        res = misc.Debris.createInstance("DebrisMalformed.entity")
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribLifetime(self):
        """ Test whether lifetime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.lifetime)
        self.assertEqual(self.inst.lifetime,exp)

    def testCreationAttribLifetimeVar(self):
        """ Test whether lifetimeVariance attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.lifetimeVar)
        self.assertEqual(self.inst.lifetimeVariance,exp)

    def testCreationAttribSpeedLinMin(self):
        """ Test whether speedLinMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedLinMin)
        self.assertEqual(self.inst.speedLinMin,exp)

    def testCreationAttribSpeedLinMax(self):
        """ Test whether speedLinMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedLinMax)
        self.assertEqual(self.inst.speedLinMax,exp)

    def testCreationAttribSpeedAngMin(self):
        """ Test whether speedAngMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedAngMin)
        self.assertEqual(self.inst.speedAngMin,exp)

    def testCreationAttribSpeedAngMax(self):
        """ Test whether speedAngMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.speedAngMax)
        self.assertEqual(self.inst.speedAngMax,exp)

    def testCreationAttribDragRate(self):
        """ Test whether dragRate attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.dragRate)
        self.assertEqual(self.inst.dragRate,exp)

    def testCreationAttribShadowMin(self):
        """ Test whether shadowMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.shadowMin)
        self.assertEqual(self.inst.shadowMin,exp)

    def testCreationAttribShadowMax(self):
        """ Test whether shadowMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.shadowMax)
        self.assertEqual(self.inst.shadowMax,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        with open(self.filepath,'r',encoding="utf-8-sig") as f:
            stringRep = f.read()
        self.assertEqual(self.inst.toString(0),stringRep)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        self.assertEqual(self.inst.check(self.mmod),[])

"""############################# Tests PipCloud ############################"""

class PipCloudTests(unit.TestCase):
    desc = "Tests PipCloud:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "PipCloud.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"PipCloud"}
        cls.pipType = {"identifier":"pipCloudType","val":"Planets"}
        cls.empireWindowIcon = {"identifier":"empireWindowPipIcon",
            "val":"PipIconCombatShip"}
        cls.mainIconOffset = {"identifier":"mainViewIconAreaOffsetFromCenter",
            "coord":[0,0,0,0]}
        cls.mainOverlay = {"identifier":"mainViewOverlay","val":"mainoverlay"}
        cls.overlayUnderCursor = {"identifier":"mainViewOverlayUnderCursor",
            "val":"undercursor"}
        cls.maxEntityCount = {"identifier":"mainViewIconMaxEntityCount",
            "val":100}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = misc.PipCloud(
                filepath=self.filepath,
                entityType=self.entityType,
                pipType=self.pipType,
                empireWindowIcon=self.empireWindowIcon,
                mainIconOffset=self.mainIconOffset,
                mainOverlay=self.mainOverlay,
                overlayUnderCursor=self.overlayUnderCursor,
                maxEntityCount=self.maxEntityCount
        )

    def testCreationCreateInstance(self):
        """ Test whether instance is created correctly by createInstance
        """
        res = misc.PipCloud.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationCreateInstanceMalformed(self):
        """ Test whether createInstance returns problem with malformed file
        """
        res = misc.PipCloud.createInstance("PipCloudMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribPipType(self):
        """ Test whether attribute pipType is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=misc.PIP_TYPES,**self.pipType)
        self.assertEqual(self.inst.pipType,exp)

    def testCreationAttribEmpireWindowIcon(self):
        """ Test whether attribute empireWindowIcon is created correctly
        """
        exp = ui.BrushRef(**self.empireWindowIcon)
        self.assertEqual(self.inst.empireWindowIcon,exp)

    def testCreationAttribMainIconOffset(self):
        """ Test whether attribute mainIconOffset is created correctly
        """
        exp = co_attribs.AttribPos4d(**self.mainIconOffset)
        self.assertEqual(self.inst.mainIconOffset,exp)

    def testCreationAttribMainOverlay(self):
        """ Test whether attribute mainOverlay is created correctly
        """
        exp = ui.BrushRef(**self.mainOverlay)
        self.assertEqual(self.inst.mainOverlay,exp)

    def testCreationAttribOverlayUnderCursor(self):
        """ Test whether attribute overlayUnderCursor is created correctly
        """
        exp = ui.BrushRef(**self.overlayUnderCursor)
        self.assertEqual(self.inst.overlayUnderCursor,exp)

    def testCreationAttribMaxEntityCount(self):
        """ Test whether attribute maxEntityCount is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxEntityCount)
        self.assertEqual(self.inst.maxEntityCount,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        self.mmod.brushes.checkBrush.return_value = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

"""############################ Tests CannonShell ##########################"""

class CannonShellTests(tcoe.MainViewSelectableFixture,unit.TestCase):
    desc = "Tests CannonShell"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "CannonShell.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"CannonShell"}
        cls.accel = {"identifier":"acceleration","val":300.0}
        cls.startSpeed = {"identifier":"startingSpeed","val":500.0}
        cls.maxSpeed = {"identifier":"maxSpeed","val":10000.0}
        cls.buff = {"identifier":"buffToApplyOnImpact","val":"impactbuff"}
        cls.impactEff = {"identifier":"planetImpactEffectName",
            "val":"impacteffect"}
        cls.impactSounds = {
            "identifier":"planetImpactSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"impactsound"}
            ]
        }
        cls.cameraShakeMax = {"identifier":"planetImpactCameraShakeMaxSize",
            "val":980000.0}
        cls.cameraShakeStrength = {
            "identifier":"planetImpactCameraShakeStrength",
            "val":1.5}
        cls.cameraShakeDuration = {
            "identifier":"planetImpactCameraShakeDuration",
            "val":0.0}
        cls.mesh = {"identifier":"meshName","val":"mesh"}
        cls.renderMesh = {"identifier":"renderMesh","val":False}

    def setUp(self):
        self.inst = misc.CannonShell(
                filepath=self.filepath,
                entityType=self.entityType,
                accel=self.accel,
                startSpeed=self.startSpeed,
                maxSpeed=self.maxSpeed,
                buff=self.buff,
                impactEff=self.impactEff,
                impactSounds=self.impactSounds,
                cameraShakeMax=self.cameraShakeMax,
                cameraShakeStrength=self.cameraShakeStrength,
                cameraShakeDuration=self.cameraShakeDuration,
                mesh=self.mesh,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                nameString=self.nameString,
                descString=self.descString,
                renderMesh=self.renderMesh,
                zoomDist=self.zoomDist,
                iconHud=self.iconHud,
                iconHudSmall=self.iconHudSmall,
                iconInfocard=self.iconInfocard,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.brushes.checkBrush.return_value = []
        self.mmod.strings.checkString.return_value = []
        patcher = mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mBuff = patcher.start()
        patcher = mock.patch.object(self.inst.impactEff,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mImpactEffect = patcher.start()
        patcher = mock.patch.object(self.inst.impactSounds,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mImpactSounds = patcher.start()
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mMesh = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.mockProb = mock.MagicMock(probFile=None)
        self.mockProb.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = misc.CannonShell.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether createInstance returns problems from malformed file
        """
        res = misc.CannonShell.createInstance("CannonShellMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribAccel(self):
        """ Test whether accel attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.accel)
        self.assertEqual(self.inst.accel,exp)

    def testCreationAttribStartSpeed(self):
        """ Test whether startSpeed attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startSpeed)
        self.assertEqual(self.inst.startSpeed,exp)

    def testCreationAttribmaxSpeed(self):
        """ Test whether maxSpeed attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxSpeed)
        self.assertEqual(self.inst.maxSpeed,exp)

    def testCreationAttribBuff(self):
        """ Test whether buff attribute is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        self.assertEqual(self.inst.buff,exp)

    def testCreationAttribImpactEff(self):
        """ Test whether impactEff attribute is created correctly
        """
        exp = par.ParticleRef(**self.impactEff)
        self.assertEqual(self.inst.impactEff,exp)

    def testCreationAttribImpactSounds(self):
        """ Test whether impactSounds attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.impactSounds)
        self.assertEqual(self.inst.impactSounds,exp)

    def testCreationAttribCameraShakeMax(self):
        """ Test whether cameraShakeMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.cameraShakeMax)
        self.assertEqual(self.inst.cameraShakeMax,exp)

    def testCreationAttribCameraShakeStrength(self):
        """ Test whether cameraShakeStrength attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.cameraShakeStrength)
        self.assertEqual(self.inst.cameraShakeStrength,exp)

    def testCreationAttribCameraShakeDuration(self):
        """ Test whether cameraShakeDuration attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.cameraShakeDuration)
        self.assertEqual(self.inst.cameraShakeDuration,exp)

    def testCreationAttribMesh(self):
        """ Test whether mesh attribute is created correctly
        """
        exp = meshes.MeshRef(**self.mesh)
        self.assertEqual(self.inst.mesh,exp)

    def testCreationAttribRenderMesh(self):
        """ Test whether renderMesh attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.renderMesh)
        self.assertEqual(self.inst.renderMesh,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckBuff(self):
        """ Test whether check returns problems from buff attrib
        """
        self.mBuff.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mBuff.assert_called_once_with(self.mmod,False)

    def testCheckImpactEff(self):
        """ Test whether check returns problems from impactEff attrib
        """
        self.mImpactEffect.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mImpactEffect.assert_called_once_with(self.mmod,False)

    def testCheckImpactSounds(self):
        """ Test whether check returns problems from impactSounds attrib
        """
        self.mImpactSounds.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mImpactSounds.assert_called_once_with(self.mmod,False)

    def testCheckMesh(self):
        """ Test whether check returns problems from mesh attrib
        """
        self.mMesh.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mMesh.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests QuestConstraint ########################"""

class QuestConstraintTests(unit.TestCase):
    desc = "Tests QuestConstraint:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "ReceiverConstraint\n"
            +"\ttype \"TargetHappiness\"\n"
            +"\tmin 0.000000\n"
            +"\tmax 5.000000\n"
        )
        cls.identifier = "ReceiverConstraint"
        cls.constType = {"identifier":"type","val":"TargetHappiness"}
        cls.constMin = {"identifier":"min","val":0.0}
        cls.constMax = {"identifier":"max","val":5.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = misc.QuestConstraint(
                identifier=self.identifier,
                constType=self.constType,
                constMin=self.constMin,
                constMax=self.constMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = misc.g_qReceiverConstraint.parseString(self.parseString)[0]
        res = misc.QuestConstraint(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribConstType(self):
        """ Test whether the constType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=misc.QUEST_TARGET_TYPES,
                **self.constType)
        self.assertEqual(self.inst.constType,exp)

    def testCreationAttribConstMin(self):
        """ Test whether the constMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.constMin)
        self.assertEqual(self.inst.constMin,exp)

    def testCreationAttribConstMax(self):
        """ Test whether the constMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.constMax)
        self.assertEqual(self.inst.constMax,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################### Tests Quest #############################"""

class QuestTests(unit.TestCase):
    desc = "Tests Quest:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "Quest.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Quest"}
        cls.titleString = {"identifier":"titleStringId","val":"title"}
        cls.descString = {"identifier":"infoCardDescStringId","val":"desc"}
        cls.questType = {"identifier":"questType","val":"GiveResources"}
        cls.duration = {"identifier":"duration","val":900.0}
        cls.relDecayRate = {"identifier":"relationshipDecayRate","val":0.0}
        cls.happinessSuccess = {"identifier":"happinessChangeForSuccess",
            "val":0.06}
        cls.happinessFailure = {"identifier":"happinessChangeForFailure",
            "val":-0.03}
        cls.reward = {
            "identifier":"reward",
            "credits":{"identifier":"credits","val":0.0},
            "metal":{"identifier":"metal","val":0.0},
            "crystal":{"identifier":"crystal","val":0.0}
        }
        cls.demand = {
            "identifier":"priceDemanded",
            "credits":{"identifier":"credits","val":0.0},
            "metal":{"identifier":"metal","val":0.0},
            "crystal":{"identifier":"crystal","val":200.0}
        }
        cls.valueKill = {"identifier":"valueToKill","val":0.0}
        cls.constraintsReceiver = {
            "counter":{"identifier":"numReceiverConstraints","val":1},
            "elements":[
                {
                    "identifier":"ReceiverConstraint",
                    "constType":{"identifier":"type","val":"TargetHappiness"},
                    "constMin":{"identifier":"min","val":0.0},
                    "constMax":{"identifier":"max","val":5.0}
                }
            ]
        }
        cls.constraintsTarget = {
            "counter":{"identifier":"numTargetConstraints","val":0},
            "elements":[]
        }
        cls.weight = {"identifier":"weight","val":1.0}
        cls.mmod = tco.genMockMod("./")
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = misc.Quest(
                filepath=self.filepath,
                entityType=self.entityType,
                titleString=self.titleString,
                descString=self.descString,
                questType=self.questType,
                duration=self.duration,
                relDecayRate=self.relDecayRate,
                happinessSuccess=self.happinessSuccess,
                happinessFailure=self.happinessFailure,
                reward=self.reward,
                demand=self.demand,
                valueKill=self.valueKill,
                constraintsReceiver=self.constraintsReceiver,
                constraintsTarget=self.constraintsTarget,
                weight=self.weight
        )
        self.mockProb = mock.MagicMock(probFile=None)
        self.mockProb.mock_add_spec(co_probs.BasicProblem,True)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = misc.Quest.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problem is returned for malformed file
        """
        res = misc.Quest.createInstance("QuestMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribTitleString(self):
        """ Test whether titleString attribute is created correctly
        """
        exp = ui.StringReference(**self.titleString)
        self.assertEqual(self.inst.titleString,exp)

    def testCreationAttribDescString(self):
        """ Test whether descString attribute is created correctly
        """
        exp = ui.StringReference(**self.descString)
        self.assertEqual(self.inst.descString,exp)

    def testCreationAttribQuestType(self):
        """ Test whether questType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=misc.QUEST_TYPES,
                **self.questType)
        self.assertEqual(self.inst.questType,exp)

    def testCreationAttribDuration(self):
        """ Test whether duration attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.duration)
        self.assertEqual(self.inst.duration,exp)

    def testCreationAttribRelDecayRate(self):
        """ Test whether relDecayRate attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.relDecayRate)
        self.assertEqual(self.inst.relDecayRate,exp)

    def testCreationAttribHappinessSuccess(self):
        """ Test whether happinessSuccess attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.happinessSuccess)
        self.assertEqual(self.inst.happinessSuccess,exp)

    def testCreationAttribHappinessFailure(self):
        """ Test whether happinessFailure attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.happinessFailure)
        self.assertEqual(self.inst.happinessFailure,exp)

    def testCreationAttribReward(self):
        """ Test whether reward attribute is created correctly
        """
        exp = coe.Cost(**self.reward)
        self.assertEqual(self.inst.reward,exp)

    def testCreationAttribDemand(self):
        """ Test whether demand attribute is created correctly
        """
        exp = coe.Cost(**self.demand)
        self.assertEqual(self.inst.demand,exp)

    def testCreationAttribValueKill(self):
        """ Test whether valueKill attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.valueKill)
        self.assertEqual(self.inst.valueKill,exp)

    def testCreationAttribConstraintsReceiver(self):
        """ Test whether constraintsReceiver attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=misc.QuestConstraint,
                **self.constraintsReceiver)
        self.assertEqual(self.inst.constraintsReceiver,exp)

    def testCreationAttribConstraintsTarget(self):
        """ Test whether constraintsTarget attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=misc.QuestConstraint,
                **self.constraintsTarget)
        self.assertEqual(self.inst.constraintsTarget,exp)

    def testCreationAttribWeight(self):
        """ Test whether weight attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.weight)
        self.assertEqual(self.inst.weight,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckTitleString(self):
        """ Test whether check returns problems for titleString attrib
        """
        with mock.patch.object(self.inst.titleString,"check",autospec=True,
                spec_set=True,return_value=[self.mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[self.mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckDescString(self):
        """ Test whether check returns problems for descString attrib
        """
        with mock.patch.object(self.inst.descString,"check",autospec=True,
                spec_set=True,return_value=[self.mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[self.mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)
