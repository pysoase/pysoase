""" Tests for the pysoase.entities.research module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.research as research
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_ecommon import UIVisibleFixtures

testdata = os.path.join("entities","research")

"""######################### Tests ResearchWinLoc ##########################"""

class ResearchWinLocTests(unit.TestCase):
    desc = "Tests ResearchWinLoc:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchWindowLocation\n"
            +"\tblock 0\n"
            +"\tpos [0,0]\n"
        )
        cls.identifier = "researchWindowLocation"
        cls.block = {"identifier":"block","val":0}
        cls.pos = {"identifier":"pos","coord":[0,0]}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.ResearchWinLoc(
                identifier=self.identifier,
                block=self.block,
                pos=self.pos
        )

    def testCreationParse(self):
        """ Test whether creation from parse results works correctly
        """
        parseRes = research.g_researchWinLoc.parseString(self.parseString)[0]
        res = research.ResearchWinLoc(**parseRes)
        self.assertEqual(self.inst,res)

    def testCreationAttribBlock(self):
        """ Test whether the block attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.block)
        self.assertEqual(self.inst.block,exp)

    def testCreationAttribPos(self):
        """ Test whether the pos attribute is constructed correctly
        """
        exp = co_attribs.AttribPos2d(**self.pos)
        self.assertEqual(self.inst.pos,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests ResearchMod ############################"""

class ResearchModTests(unit.TestCase):
    desc = "Tests ResearchMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"AllowGalaxyTravel\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType","val":"AllowGalaxyTravel"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.ResearchMod(
                identifier=self.identifier,
                modType=self.modType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = research.g_researchBoolModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationModType(self):
        """ Test whether modType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=research.ResearchMod._modTypes,
                **self.modType)
        self.assertEqual(self.inst.modType,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCreationInvalidType(self):
        """ Test whether invalid mod type raises error in createInstance
        """
        t = {"identifier":"modifierType","val":"foo"}
        with self.assertRaises(RuntimeError):
            research.ResearchMod.factory(modType=t)

class ColonizeMod(unit.TestCase):
    desc = "Tests ColonizeMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"PlanetIsColonizable\"\n"
            +"\tlinkedPlanetType \"Ice\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType","val":"PlanetIsColonizable"}
        cls.linkedPlanet = {"identifier":"linkedPlanetType","val":"Ice"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.ColonizeMod(
                identifier=self.identifier,
                modType=self.modType,
                linkedPlanet=self.linkedPlanet
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = research.g_researchBoolModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribLinkedPlanet(self):
        """ Test whether the linkedPlanet attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.PLANETTEYPE_FOR_RESEARCH,
                **self.linkedPlanet)
        self.assertEqual(self.inst.linkedPlanet,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PactModTests(unit.TestCase):
    desc = "Tests PactMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"UnlockPact\"\n"
            +"\tallianceType \"ArmorPact\"\n"
            +"\tpactUnlockEntityDefName \"Testpact\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType","val":"UnlockPact"}
        cls.allianceType = {"identifier":"allianceType","val":"ArmorPact"}
        cls.pactUnlockEntity = {"identifier":"pactUnlockEntityDefName",
            "val":"Testpact"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.PactMod(
                identifier=self.identifier,
                modType=self.modType,
                allianceType=self.allianceType,
                pactUnlockEntity=self.pactUnlockEntity
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = research.g_researchBoolModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAllianceType(self):
        """ Test whether allianceType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=research.ALLIANCE_TYPES,
                **self.allianceType)
        self.assertEqual(self.inst.allianceType,exp)

    def testCreationAttribPactUnlockEntity(self):
        """ Test whether pactUnlockEntity attribute is constructed correctly
        """
        exp = coe.EntityRef(types=["ResearchSubject"],**self.pactUnlockEntity)
        self.assertEqual(self.inst.pactUnlockEntity,exp)

    def testCheck(self):
        """ Test whether check returns problems correctly
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.pactUnlockEntity,"check",
                autospec=True,spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,probs)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ApplyBuffModTests(unit.TestCase):
    desc = "Tests ApplyBuffMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"ApplyBuffToPlanetAfterColonization\"\n"
            +"\tbuffToApply \"Testbuff\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType",
            "val":"ApplyBuffToPlanetAfterColonization"}
        cls.buff = {"identifier":"buffToApply","val":"Testbuff"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.ApplyBuffMod(
                identifier=self.identifier,
                modType=self.modType,
                buff=self.buff
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = research.g_researchBoolModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBuff(self):
        """ Test whether buff attribute is constructed correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        self.assertEqual(self.inst.buff,exp)

    def testCheck(self):
        """ Test whether check returns problems correctly
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.buff,"check",
                autospec=True,spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,probs)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class FactionModTests(unit.TestCase):
    desc = "Tests FactionMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"SetFaction\"\n"
            +"\tfactionNameID \"testfaction\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType","val":"SetFaction"}
        cls.faction = {"identifier":"factionNameID","val":"testfaction"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.FactionMod(
                identifier=self.identifier,
                modType=self.modType,
                faction=self.faction
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = research.g_researchBoolModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribFaction(self):
        """ Test whether faction attribute is constructed correctly
        """
        exp = ui.StringReference(**self.faction)
        self.assertEqual(self.inst.faction,exp)

    def testCheck(self):
        """ Test whether check returns problems correctly
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.faction,"check",
                autospec=True,spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,probs)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class FloatModTests(unit.TestCase):
    desc = "Tests FloatMod:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"ExperienceConstantGainRate\"\n"
            +"\tbaseValue 12\n"
            +"\tperLevelValue 9\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType",
            "val":"ExperienceConstantGainRate"}
        cls.baseValue = {"identifier":"baseValue","val":12}
        cls.levelValue = {"identifier":"perLevelValue","val":9}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.FloatMod(
                identifier=self.identifier,
                modType=self.modType,
                baseValue=self.baseValue,
                levelValue=self.levelValue
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = research.g_researchFloatModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBaseValue(self):
        """ Test whether baseValue attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.baseValue)
        self.assertEqual(self.inst.baseValue,exp)

    def testCreationAttribLevelValue(self):
        """ Test whether levelValue attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.levelValue)
        self.assertEqual(self.inst.levelValue,exp)

    def testCheck(self):
        """ Test whether check returns empty problem list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LinkedFloatModTestsWeapon(unit.TestCase):
    desc = "Tests LinkedFloatMod with linked weapons:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"WeaponDamageAdjustment\"\n"
            +"\tbaseValue 12\n"
            +"\tperLevelValue 9\n"
            +"\tlinkedWeaponClass \"BEAM\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType",
            "val":"WeaponDamageAdjustment"}
        cls.baseValue = {"identifier":"baseValue","val":12}
        cls.levelValue = {"identifier":"perLevelValue","val":9}
        cls.linked = {"identifier":"linkedWeaponClass","val":"BEAM"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.LinkedFloatMod(
                identifier=self.identifier,
                modType=self.modType,
                baseValue=self.baseValue,
                levelValue=self.levelValue,
                linked=self.linked
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = research.g_researchFloatModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationInvalidModType(self):
        """ Test whether creation with invalid mod type raises error
        """
        invalidModType = {"identifier":"modifierType",
            "val":"foo"}
        with self.assertRaises(RuntimeError):
            self.inst = research.LinkedFloatMod(
                    identifier=self.identifier,
                    modType=invalidModType,
                    baseValue=self.baseValue,
                    levelValue=self.levelValue,
                    linked=self.linked
            )

    def testCreationAttribLinked(self):
        """ Test whether linked attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.WEAPON_CLASSES,**self.linked)
        self.assertEqual(self.inst.linked,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LinkedFloatModTestsPlanet(unit.TestCase):
    desc = "Tests LinkedFloatMod with linked planets:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"PlanetPopulationCapAdjustment\"\n"
            +"\tbaseValue 12\n"
            +"\tperLevelValue 9\n"
            +"\tlinkedPlanetType \"Ice\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType",
            "val":"PlanetPopulationCapAdjustment"}
        cls.baseValue = {"identifier":"baseValue","val":12}
        cls.levelValue = {"identifier":"perLevelValue","val":9}
        cls.linked = {"identifier":"linkedPlanetType","val":"Ice"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.LinkedFloatMod(
                identifier=self.identifier,
                modType=self.modType,
                baseValue=self.baseValue,
                levelValue=self.levelValue,
                linked=self.linked
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = research.g_researchFloatModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribLinked(self):
        """ Test whether linked attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.PLANETTEYPE_FOR_RESEARCH,
                **self.linked)
        self.assertEqual(self.inst.linked,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LinkedFloatModTestsUpgrade(unit.TestCase):
    desc = "Tests LinkedFloatMod with linked upgrades:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "researchModifier\n"
            +"\tmodifierType \"PlanetUpgradeBuildRateAdjustment\"\n"
            +"\tbaseValue 12\n"
            +"\tperLevelValue 9\n"
            +"\tlinkedPlanetUpgradeType \"Infrastructure\"\n"
        )
        cls.identifier = "researchModifier"
        cls.modType = {"identifier":"modifierType",
            "val":"PlanetUpgradeBuildRateAdjustment"}
        cls.baseValue = {"identifier":"baseValue","val":12}
        cls.levelValue = {"identifier":"perLevelValue","val":9}
        cls.linked = {"identifier":"linkedPlanetUpgradeType",
            "val":"Infrastructure"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = research.LinkedFloatMod(
                identifier=self.identifier,
                modType=self.modType,
                baseValue=self.baseValue,
                levelValue=self.levelValue,
                linked=self.linked
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = research.g_researchFloatModifier.parseString(
                self.parseString)[0]
        res = research.ResearchMod.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribLinked(self):
        """ Test whether linked attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=research.PLANETUPGRADE_TYPES,
                **self.linked)
        self.assertEqual(self.inst.linked,exp)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests ResearchSubject ########################"""

class ResearchSubjectFixture(UIVisibleFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "ResearchSubject.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()

        cls.entityType = {"identifier":"entityType",
            "val":"ResearchSubject"}
        cls.winloc = {
            "identifier":"researchWindowLocation",
            "block":{"identifier":"block","val":1},
            "pos":{"identifier":"pos","coord":[1,1]}
        }
        cls.researchField = {"identifier":"ResearchField","val":"NonCombat"}
        cls.prereqs = {
            "identifier":"Prerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"Testsubject"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"Testfaction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":2}
        }
        cls.minArtifact = {"identifier":"MinimumArtifactLevel","val":-1}
        cls.baseUpgTime = {"identifier":"BaseUpgradeTime","val":90.0}
        cls.lvlUpgTime = {"identifier":"PerLevelUpgradeTime","val":10.0}
        cls.baseCost = {
            "identifier":"BaseCost",
            "credits":{"identifier":"credits","val":2500.0},
            "metal":{"identifier":"metal","val":0.0},
            "crystal":{"identifier":"crystal","val":0.0}
        }
        cls.costIncrease = {
            "identifier":"PerLevelCostIncrease",
            "credits":{"identifier":"credits","val":0.0},
            "metal":{"identifier":"metal","val":0.0},
            "crystal":{"identifier":"crystal","val":0.0}
        }
        cls.tier = {"identifier":"Tier","val":1}
        cls.worksWithLabs = {"identifier":"onlyWorksIfTierLabsExist",
            "val":False}
        cls.maxLevels = {"identifier":"MaxNumResearchLevels","val":2}
        cls.priority = {"identifier":"priority","val":1.0}
        cls.boolModifiers = {
            "counter":{"identifier":"researchBoolModifiers","val":1},
            "elements":[
                {
                    "identifier":"researchModifier",
                    "modType":{"identifier":"modifierType",
                        "val":"AllowGalaxyTravel"}
                }
            ]
        }
        cls.floatModifiers = {
            "counter":{"identifier":"researchFloatModifiers","val":1},
            "elements":[
                {
                    "identifier":"researchModifier",
                    "modType":{"identifier":"modifierType",
                        "val":"HyperspaceChargeUpRateAdjustment"},
                    "baseValue":{"identifier":"baseValue","val":0.0},
                    "levelValue":{"identifier":"perLevelValue","val":0.15}
                }
            ]
        }
        cls.picArtifact = {"identifier":"artifactPicture","val":"artpic"}
        cls.overlayBrush = {"identifier":"uniqueOverlayBrush","val":"overlay"}
        cls.dlcID = {"identifier":"dlcId","val":204880}

class ResearchSubjectTests(ResearchSubjectFixture,unit.TestCase):
    desc = "Tests ResearchSubject:"

    def setUp(self):
        self.inst = research.ResearchSubject(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                nameString=self.nameStringCap,
                descString=self.descStringCap,
                winloc=self.winloc,
                researchField=self.researchField,
                prereqs=self.prereqs,
                minArtifact=self.minArtifact,
                baseUpgTime=self.baseUpgTime,
                lvlUpgTime=self.lvlUpgTime,
                baseCost=self.baseCost,
                costIncrease=self.costIncrease,
                tier=self.tier,
                worksWithLabs=self.worksWithLabs,
                maxLevels=self.maxLevels,
                priority=self.priority,
                boolModifiers=self.boolModifiers,
                floatModifiers=self.floatModifiers,
                picArtifact=self.picArtifact,
                overlayBrush=self.overlayBrush,
                dlcID=self.dlcID
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        res = research.ResearchSubject.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether correct problem is returned for malformed entity
        """
        res = research.ResearchSubject.createInstance(
                "ResearchSubjectMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribWinloc(self):
        """ Test whether winloc attribute is created correctly
        """
        exp = research.ResearchWinLoc(**self.winloc)
        self.assertEqual(self.inst.winloc,exp)

    def testCreationAttribResearchField(self):
        """ Test whether researchField attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=research.RESEARCH_FIELDS,
                **self.researchField)
        self.assertEqual(self.inst.researchField,exp)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attribute is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCreationAttribMinArtifact(self):
        """ Test whether minArtifact attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.minArtifact)
        self.assertEqual(self.inst.minArtifact,exp)

    def testCreationAttribBaseUpgTime(self):
        """ Test whether baseUpgTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.baseUpgTime)
        self.assertEqual(self.inst.baseUpgTime,exp)

    def testCreationAttribLvlUpgTime(self):
        """ Test whether lvlUpgTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.lvlUpgTime)
        self.assertEqual(self.inst.lvlUpgTime,exp)

    def testCreationAttribBaseCost(self):
        """ Test whether baseCost attribute is created correctly
        """
        exp = coe.Cost(**self.baseCost)
        self.assertEqual(self.inst.baseCost,exp)

    def testCreationAttribCostIncrease(self):
        """ Test whether costIncrease attribute is created correctly
        """
        exp = coe.Cost(**self.costIncrease)
        self.assertEqual(self.inst.costIncrease,exp)

    def testCreationAttribTier(self):
        """ Test whether Tier attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.tier)
        self.assertEqual(self.inst.tier,exp)

    def testCreationAttribWorksWithLabs(self):
        """ Test whether worksWithLabs attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.worksWithLabs)
        self.assertEqual(self.inst.worksWithLabs,exp)

    def testCreationAttribMaxLevels(self):
        """ Test whether maxLevels attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxLevels)
        self.assertEqual(self.inst.maxLevels,exp)

    def testCreationAttribPriority(self):
        """ Test whether priority attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.priority)
        self.assertEqual(self.inst.priority,exp)

    def testCreationAttribBoolModifiers(self):
        """ Test whether boolModifiers attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=research.ResearchMod,factory=True,
                **self.boolModifiers)
        self.assertEqual(self.inst.boolModifiers,exp)

    def testCreationAttribFloatModifiers(self):
        """ Test whether floatModifiers attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=research.ResearchMod,factory=True,
                **self.floatModifiers)
        self.assertEqual(self.inst.floatModifiers,exp)

    def testCreationAttribPicArtifact(self):
        """ Test whether picArtifact attribute is created correctly
        """
        exp = ui.BrushRef(canBeEmpty=True,**self.picArtifact)
        self.assertEqual(self.inst.picArtifact,exp)

    def testCreationAttribOverlayBrush(self):
        """ Test whether overlayBrush attribute is created correctly
        """
        exp = ui.BrushRef(canBeEmpty=True,**self.overlayBrush)
        self.assertEqual(self.inst.overlayBrush,exp)

    def testCreationAttribDlcID(self):
        """ Test whether dlcID attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.dlcID)
        self.assertEqual(self.inst.dlcID,exp)

    def testToString(self):
        """ Test whether the string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ResearchSubjectCheck(ResearchSubjectFixture,unit.TestCase):
    desc = "Tests ResearchSubject checking:"

    def setUp(self):
        self.mmod = tco.genMockMod("./")
        self.inst = research.ResearchSubject(
                filepath=self.filepath,
                entityType=self.entityType,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                nameString=self.nameString,
                descString=self.descString,
                winloc=self.winloc,
                researchField=self.researchField,
                prereqs=self.prereqs,
                minArtifact=self.minArtifact,
                baseUpgTime=self.baseUpgTime,
                lvlUpgTime=self.lvlUpgTime,
                baseCost=self.baseCost,
                costIncrease=self.costIncrease,
                tier=self.tier,
                worksWithLabs=self.worksWithLabs,
                maxLevels=self.maxLevels,
                priority=self.priority,
                boolModifiers=self.boolModifiers,
                floatModifiers=self.floatModifiers,
                picArtifact=self.picArtifact,
                overlayBrush=self.overlayBrush,
                dlcID=self.dlcID
        )
        self.maxDiff = None
        self.addCleanup(mock.patch.stopall)
        self.mmod = tco.genMockMod("./")
        self.mmod.brushes.checkBrush.return_value = []
        self.mmod.strings.checkString.return_value = []
        patcher = mock.patch.object(self.inst.prereqs,"check",
                autospec=True,return_value=[])
        self.mockPrereqs = patcher.start()
        self.mockProb = mock.MagicMock()
        self.mockProb.probFile = None
        self.mockProb.mock_add_spec(co_probs.BasicProblem,spec_set=True)

    def testCheckNoProbs(self):
        """ Test whether check returns empty list of problems with no probs
        """
        self.assertEqual(self.inst.check(self.mmod),[])

    def testCheckProbPrereqs(self):
        """ Test whether check correctly returns problem in prereq attrib
        """
        self.mockPrereqs.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])

    def testCheckProbPicArtifact(self):
        """ Test whether check correctly returns problem in picArtifact attrib
        """
        with mock.patch.object(self.inst.picArtifact,"check",autospec=True,
                return_value=[self.mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[self.mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbOverlayBrush(self):
        """ Test whether check correctly returns problem in overlayBrush attrib
        """
        with mock.patch.object(self.inst.overlayBrush,"check",autospec=True,
                return_value=[self.mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[self.mockProb])
            mocked.assert_called_once_with(self.mmod,False)
