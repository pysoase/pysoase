""" Tests for the pysoase.entities.buffs_abilities module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.problems as co_probs
import pysoase.entities.armed as armed
import pysoase.entities.buffs_abilities as abilities
import pysoase.entities.common as coe
import pysoase.mod.audio as audio
import pysoase.mod.constants as consts
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_ecommon import UIVisibleFixtures

testdata = os.path.join("entities","abilities")

"""#################### Tests Instant Action Conditions ####################"""

class InstantActionConditionTests(unit.TestCase):
    desc = "Tests InstantActionCondition:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionConditionType \"IfOwnerIsAboutToDie\"\n"
        )
        cls.condType = {"identifier":"instantActionConditionType",
            "val":"IfOwnerIsAboutToDie"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.InstantActionCondition(
                condType=self.condType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionConditionSimple.parseString(
                self.parseString)[0]
        res = abilities.InstantActionCondition.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCondType(self):
        """ Test whether condType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.INSTANT_ACTION_CONDITIONS,
                **self.condType)
        self.assertEqual(self.inst.condType,exp)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid condition type
        """
        cond = {"identifier":"instantActionConditionType",
            "val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.InstantActionCondition.factory(condType=cond)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class InstantActionCondBuff(unit.TestCase):
    desc = "Tests InstantActionCondBuff:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionConditionType \"IfFirstSpawnerHasBuff\"\n"
            +"conditionBuffType \"testbuff\"\n"
            +"conditionBuffShortName \"testname\"\n"
        )
        cls.condType = {"identifier":"instantActionConditionType",
            "val":"IfFirstSpawnerHasBuff"}
        cls.buffType = {"identifier":"conditionBuffType","val":"testbuff"}
        cls.buffShortName = {"identifier":"conditionBuffShortName",
            "val":"testname"}
        cls.mmod = tco.genMockMod("./")
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = abilities.InstantActionCondBuff(
                condType=self.condType,
                buffType=self.buffType,
                buffShortName=self.buffShortName
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionConditionBuff.parseString(
                self.parseString)[0]
        res = abilities.InstantActionCondition.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBuffType(self):
        """ Test whether buffType attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buffType)
        self.assertEqual(self.inst.buffType,exp)

    def testCreationAttribBuffShortName(self):
        """ Test whether buffShortName attrib is created correctly
        """
        exp = ui.StringReference(**self.buffShortName)
        self.assertEqual(self.inst.buffShortName,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        with mock.patch.object(self.inst.buffType,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckBuffType(self):
        """ Test whether check returns problems from buffType
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.buffType,"check",autospec=True,
                spec_set=True,return_value=[mockProb]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])

    def testCheckBuffShortName(self):
        """ Test whether check returns problems from buffShortName
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.strings.checkString.return_value = [mockProb]
        with mock.patch.object(self.inst.buffType,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
        self.mmod.strings.checkString.return_value = []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class InstantActionCondLvl(unit.TestCase):
    desc = "Tests InstantActionCondLvl:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionConditionType \"IfOwnerHasHullLessThanPerc\"\n"
            +"hullPerc\n"
            +"\tLevel:0 1.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 1.000000\n"
            +"\tLevel:3 1.000000\n"
        )
        cls.condType = {"identifier":"instantActionConditionType",
            "val":"IfOwnerHasHullLessThanPerc"}
        cls.hullPerc = {
            "identifier":"hullPerc",
            "levels":[
                {"identifier":"Level:0","val":1.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":1.0},
                {"identifier":"Level:3","val":1.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.InstantActionCondLvl(
                condType=self.condType,
                hullPerc=self.hullPerc
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionConditionLvl.parseString(self.parseString
        )[0]
        res = abilities.InstantActionCondition.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribHullPerc(self):
        """ Test whether hullPerc attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.hullPerc)
        self.assertEqual(self.inst.hullPerc,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests TargetFilter ###########################"""

class TargetFilterTests(unit.TestCase):
    desc = "Tests TargetFilter:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "targetFilter\n"
            +"\tnumOwnerships 1\n"
            +"\townership \"Enemy\"\n"
            +"\tnumObjects 1\n"
            +"\tobject \"Frigate\"\n"
            +"\tnumSpaces 1\n"
            +"\tspace \"Normal\"\n"
            +"\tnumConstraints 0\n"
        )
        cls.identifier = "targetFilter"
        cls.ownerships = {
            "counter":{"identifier":"numOwnerships","val":1},
            "elements":[
                {"identifier":"ownership","val":"Enemy"}
            ]
        }
        cls.objects = {
            "counter":{"identifier":"numObjects","val":1},
            "elements":[
                {"identifier":"object","val":"Frigate"}
            ]
        }
        cls.spaces = {
            "counter":{"identifier":"numSpaces","val":1},
            "elements":[
                {"identifier":"space","val":"Normal"}
            ]
        }
        cls.constraints = {
            "counter":{"identifier":"numConstraints","val":0},
            "elements":[]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.TargetFilter(
                identifier=self.identifier,
                ownerships=self.ownerships,
                objects=self.objects,
                spaces=self.spaces,
                constraints=self.constraints
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_targetFilter.parseString(self.parseString)[0]
        res = abilities.TargetFilter(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribOwnerships(self):
        """ Test whether ownerships attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":abilities.OWNERSHIPS},
                **self.ownerships)
        self.assertEqual(self.inst.ownerships,exp)

    def testCreationAttribObjects(self):
        """ Test whether objects attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":abilities.OBJECTS},
                **self.objects)
        self.assertEqual(self.inst.objects,exp)

    def testCreationAttribSpaces(self):
        """ Test whether spaces attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":abilities.SPACE_TYPES},
                **self.spaces)
        self.assertEqual(self.inst.spaces,exp)

    def testCreationAttribConstraints(self):
        """ Test whether constraints attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":abilities.TARGET_CONSTRAINTS},
                **self.constraints)
        self.assertEqual(self.inst.constraints,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""##################### Tests Instant Action Triggers #####################"""

class InstantActionTriggerTests(unit.TestCase):
    desc = "Tests InstantActionTrigger:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionTriggerType \"OnBuffFinish\"\n"
        )
        cls.triggerType = {"identifier":"instantActionTriggerType",
            "val":"OnBuffFinish"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.InstantActionTrigger(
                triggerType=self.triggerType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionTriggerSimple.parseString(
                self.parseString)[0]
        res = abilities.InstantActionTrigger.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalidType(self):
        """ Test whether factory raises error for invalid trigger type
        """
        invalid = {"identifier":"instantActionTriggerType",
            "val":"foo"}
        with self.assertRaises(RuntimeError):
            res = abilities.InstantActionTrigger.factory(triggerType=invalid)

    def testCreationAttribTriggerType(self):
        """ Test whether triggerType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.INSTANT_ACTION_TRIGGERS,
                **self.triggerType)
        self.assertEqual(self.inst.triggerType,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class TriggerChanceTests(unit.TestCase):
    desc = "Tests TriggerChance:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionTriggerType \"OnChance\"\n"
            +"buffApplyChance\n"
            +"\tLevel:0 0.100000\n"
            +"\tLevel:1 0.000000\n"
            +"\tLevel:2 0.000000\n"
            +"\tLevel:3 0.000000\n"
        )
        cls.triggerType = {"identifier":"instantActionTriggerType",
            "val":"OnChance"}
        cls.applyChance = {
            "identifier":"buffApplyChance",
            "levels":[
                {"identifier":"Level:0","val":0.1},
                {"identifier":"Level:1","val":0.0},
                {"identifier":"Level:2","val":0.0},
                {"identifier":"Level:3","val":0.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.TriggerChance(
                triggerType=self.triggerType,
                applyChance=self.applyChance
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionTriggerChance.parseString(
                self.parseString)[0]
        res = abilities.InstantActionTrigger.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribApplyChance(self):
        """ Test whether applyChance attribute is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.applyChance)
        self.assertEqual(self.inst.applyChance,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class TriggerWeaponFiredTests(unit.TestCase):
    desc = "Tests TriggerWeaponFired:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionTriggerType \"OnWeaponFired\"\n"
            +"numWeaponClasses 1\n"
            +"weaponClassForWeaponPassive \"LASERTECH\"\n"
            +"passiveWeaponEffectChance\n"
            +"\tLevel:0 0.100000\n"
            +"\tLevel:1 0.000000\n"
            +"\tLevel:2 0.000000\n"
            +"\tLevel:3 0.000000\n"
        )
        cls.triggerType = {"identifier":"instantActionTriggerType",
            "val":"OnWeaponFired"}
        cls.weaponClasses = {
            "counter":{"identifier":"numWeaponClasses","val":1},
            "elements":[
                {"identifier":"weaponClassForWeaponPassive",
                    "val":"LASERTECH"}
            ]
        }
        cls.passiveEffectChance = {
            "identifier":"passiveWeaponEffectChance",
            "levels":[
                {"identifier":"Level:0","val":0.1},
                {"identifier":"Level:1","val":0.0},
                {"identifier":"Level:2","val":0.0},
                {"identifier":"Level:3","val":0.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.TriggerWeaponFired(
                triggerType=self.triggerType,
                weaponClasses=self.weaponClasses,
                passiveEffectChance=self.passiveEffectChance
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionTriggerWeapFired.parseString(
                self.parseString)[0]
        res = abilities.InstantActionTrigger.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribWeaponClasses(self):
        """ Test whether weaponClasses attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":coe.WEAPON_CLASSES},
                **self.weaponClasses
        )
        self.assertEqual(self.inst.weaponClasses,exp)

    def testCreationAttribPassiveEffectChance(self):
        """ Test whether passiveEffectChance attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.passiveEffectChance)
        self.assertEqual(self.inst.passiveEffectChance,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class TriggerCondTests(unit.TestCase):
    desc = "Tests TriggerCond:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionTriggerType \"OnCondition\"\n"
            +"instantActionConditionType \"IfOwnerIsAboutToDie\"\n"
        )
        cls.triggerType = {"identifier":"instantActionTriggerType",
            "val":"OnCondition"}
        cls.cond = {
            "condType":{"identifier":"instantActionConditionType",
                "val":"IfOwnerIsAboutToDie"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.TriggerCond(
                triggerType=self.triggerType,
                cond=self.cond
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionTriggerCondition.parseString(
                self.parseString)[0]
        res = abilities.InstantActionTrigger.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCond(self):
        """ Test whether cond attribute is created correctly
        """
        exp = abilities.InstantActionCondition.factory(**self.cond)
        self.assertEqual(self.inst.cond,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class TriggerDelayTests(unit.TestCase):
    desc = "Tests TriggerDelay:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "instantActionTriggerType \"OnDelay\"\n"
            +"delayTime 100.000000\n"
        )
        cls.triggerType = {"identifier":"instantActionTriggerType",
            "val":"OnDelay"}
        cls.delay = {"identifier":"delayTime","val":100.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.TriggerDelay(
                triggerType=self.triggerType,
                delay=self.delay
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionTriggerDelay.parseString(self.parseString
        )[0]
        res = abilities.InstantActionTrigger.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDelay(self):
        """ Test whether delay attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.delay)
        self.assertEqual(self.inst.delay,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests Finish Conditions #########################"""

class BuffFinishCondTests(unit.TestCase):
    desc = "Tests BuffFinishCond:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "finishCondition\n"
            +"\tfinishConditionType \"OwnerChanged\"\n"
        )
        cls.identifier = "finishCondition"
        cls.finishType = {"identifier":"finishConditionType",
            "val":"OwnerChanged"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.BuffFinishCond(
                identifier=self.identifier,
                finishType=self.finishType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_finishCondition.parseString(self.parseString)[0]
        res = abilities.BuffFinishCond.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalidType(self):
        """ Test whether factory raises error when called with an invalid type
        """
        invalid = {"identifier":"finishConditionType",
            "val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.BuffFinishCond.factory(finishType=invalid)

    def testCreationAttribFinishType(self):
        """ Test whether finishType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.FINISH_CONDITIONS,
                **self.finishType)
        self.assertEqual(self.inst.finishType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BuffFinishCondBuffTests(unit.TestCase):
    desc = "Tests BuffFinishCondBuff:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "finishCondition\n"
            +"\tfinishConditionType \"OwnerNoLongerHasBuff\"\n"
            +"\tbuffTypeToQuery \"testbuff\"\n"
        )
        cls.identifier = "finishCondition"
        cls.finishType = {"identifier":"finishConditionType",
            "val":"OwnerNoLongerHasBuff"}
        cls.buff = {"identifier":"buffTypeToQuery","val":"testbuff"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.BuffFinishCondBuff(
                identifier=self.identifier,
                finishType=self.finishType,
                buff=self.buff
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_finishCondition.parseString(self.parseString)[0]
        res = abilities.BuffFinishCond.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBuff(self):
        """ Test whether buff attribute is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        self.assertEqual(self.inst.buff,exp)

    def testCheckBuff(self):
        """ Test whether check returns problems in buff attrib
        """
        with mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BuffFinishCondSimpleTests(unit.TestCase):
    desc = "Tests BuffFinishCondSimple:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "finishCondition\n"
            +"\tfinishConditionType \"OwnerHullAbovePerc\"\n"
            +"\thullPercent\n"
            +"\t\tLevel:0 0\n"
            +"\t\tLevel:1 10\n"
            +"\t\tLevel:2 20\n"
            +"\t\tLevel:3 30\n"
        )
        cls.identifier = "finishCondition"
        cls.finishType = {"identifier":"finishConditionType",
            "val":"OwnerHullAbovePerc"}
        cls.vals = [
            {
                "identifier":"hullPercent",
                "levels":[
                    {"identifier":"Level:0","val":0},
                    {"identifier":"Level:1","val":10},
                    {"identifier":"Level:2","val":20},
                    {"identifier":"Level:3","val":30}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.BuffFinishCondSimple(
                identifier=self.identifier,
                finishType=self.finishType,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_finishCondition.parseString(self.parseString)[0]
        res = abilities.BuffFinishCond.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribHullPercent(self):
        """ Test whether hullPercent attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.hullPercent,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BuffFinishCondDmgTests(unit.TestCase):
    desc = "Tests BuffFinishCondDmg:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "finishCondition\n"
            +"\tfinishConditionType \"DamageTaken\"\n"
            +"\tdamageNeeded\n"
            +"\t\tLevel:0 0\n"
            +"\t\tLevel:1 10\n"
            +"\t\tLevel:2 20\n"
            +"\t\tLevel:3 30\n"
            +"\ttypeOfDamageNeeded \"AFFECTS_ONLY_HULL\"\n"
        )
        cls.identifier = "finishCondition"
        cls.finishType = {"identifier":"finishConditionType",
            "val":"DamageTaken"}
        cls.vals = [
            {
                "identifier":"damageNeeded",
                "levels":[
                    {"identifier":"Level:0","val":0},
                    {"identifier":"Level:1","val":10},
                    {"identifier":"Level:2","val":20},
                    {"identifier":"Level:3","val":30}
                ]
            }
        ]
        cls.dmgType = {"identifier":"typeOfDamageNeeded",
            "val":"AFFECTS_ONLY_HULL"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.BuffFinishCondDmg(
                identifier=self.identifier,
                finishType=self.finishType,
                vals=self.vals,
                dmgType=self.dmgType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_finishCondition.parseString(self.parseString)[0]
        res = abilities.BuffFinishCond.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDamageNeeded(self):
        """ Test whether damageNeeded attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.damageNeeded,exp)

    def testCreationAttribDmgType(self):
        """ Test whether dmgType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,**self.dmgType)
        self.assertEqual(self.inst.dmgType,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class BuffFinishCondResearchTests(unit.TestCase):
    desc = "Tests BuffFinishCondResearch:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "finishCondition\n"
            +"\tfinishConditionType \"ResearchNotMet\"\n"
            +"\tresearchPrerequisites\n"
            +"\t\tNumResearchPrerequisites 1\n"
            +"\t\tResearchPrerequisite\n"
            +"\t\t\tSubject \"research1\"\n"
            +"\t\t\tLevel 2\n"
            +"\t\tRequiredFactionNameID \"faction\"\n"
            +"\t\tRequiredCompletedResearchSubjects 0\n"
        )
        cls.identifier = "finishCondition"
        cls.finishType = {"identifier":"finishConditionType",
            "val":"ResearchNotMet"}
        cls.prereqs = {
            "identifier":"researchPrerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":1},
                "elements":[
                    {
                        "identifier":"ResearchPrerequisite",
                        "subject":{"identifier":"Subject",
                            "val":"research1"},
                        "level":{"identifier":"Level","val":2}
                    }
                ]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":"faction"},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":0}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.BuffFinishCondResearch(
                identifier=self.identifier,
                finishType=self.finishType,
                prereqs=self.prereqs
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_finishCondition.parseString(self.parseString)[0]
        res = abilities.BuffFinishCond.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrereqs(self):
        """ Test whether prereqs attrib is created correctly
        """
        exp = coe.ResearchPrerequisite(**self.prereqs)
        self.assertEqual(self.inst.prereqs,exp)

    def testCheckPrereq(self):
        """ Test whether check returns problems in prereqs attrib
        """
        with mock.patch.object(self.inst.prereqs,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests Effect Info ############################"""

class AttachInfoTests(unit.TestCase):
    desc = "Tests AttachInfo"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "effectAttachInfo\n"
            +"\tattachType \"Center\"\n"
        )
        cls.identifier = "effectAttachInfo"
        cls.attachType = {"identifier":"attachType","val":"Center"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AttachInfo(
                identifier=self.identifier,
                attachType=self.attachType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_effectAttach.parseString(self.parseString)[0]
        res = abilities.AttachInfo.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error for invalid attachType
        """
        invalid = {"identifier":"attachType","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.AttachInfo.factory(attachType=invalid)

    def testCreationAttribAttachType(self):
        """ Test whether attachType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.EFFECT_ATTACH_TYPES,
                **self.attachType)
        self.assertEqual(self.inst.attachType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AttachInfoAbilityTests(unit.TestCase):
    desc = "Tests AttachInfoAbility:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "effectAttachInfo\n"
            +"\tattachType \"Ability\"\n"
            +"\tabilityIndex 2\n"
        )
        cls.identifier = "effectAttachInfo"
        cls.attachType = {"identifier":"attachType","val":"Ability"}
        cls.index = {"identifier":"abilityIndex","val":2}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AttachInfoAbility(
                identifier=self.identifier,
                attachType=self.attachType,
                index=self.index
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_effectAttach.parseString(self.parseString)[0]
        res = abilities.AttachInfo.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribIndex(self):
        """ Test whether index attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.index)
        self.assertEqual(self.inst.index,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AttachEffectTests(unit.TestCase):
    desc = "Tests AttachEffect:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "effectInfo\n"
            +"\teffectAttachInfo\n"
            +"\t\tattachType \"Ability\"\n"
            +"\t\tabilityIndex 2\n"
            +"\tsmallEffectName \"small\"\n"
            +"\tmediumEffectName \"medium\"\n"
            +"\tlargeEffectName \"large\"\n"
            +"\tsoundID \"snd\"\n"
        )
        cls.identifier = "effectInfo"
        cls.attachInfo = {
            "identifier":"effectAttachInfo",
            "attachType":{"identifier":"attachType","val":"Ability"},
            "index":{"identifier":"abilityIndex","val":2}
        }
        cls.effectSmall = {"identifier":"smallEffectName","val":"small"}
        cls.effectMedium = {"identifier":"mediumEffectName","val":"medium"}
        cls.effectLarge = {"identifier":"largeEffectName","val":"large"}
        cls.sound = {"identifier":"soundID","val":"snd"}

    def setUp(self):
        self.inst = abilities.AttachEffect(
                identifier=self.identifier,
                attachInfo=self.attachInfo,
                effectSmall=self.effectSmall,
                effectMedium=self.effectMedium,
                effectLarge=self.effectLarge,
                sound=self.sound
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.audio.checkEffect.return_value = []
        patcher = mock.patch.object(self.inst.effectSmall,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockSmall = patcher.start()
        patcher = mock.patch.object(self.inst.effectMedium,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockMedium = patcher.start()
        patcher = mock.patch.object(self.inst.effectLarge,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockLarge = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_effectInfo.parseString(self.parseString)[0]
        res = abilities.AttachEffect(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAttachInfo(self):
        """ Test whether attachInfo attribute is created correctly
        """
        exp = abilities.AttachInfoAbility(**self.attachInfo)
        self.assertEqual(self.inst.attachInfo,exp)

    def testCreationAttribEffectSmall(self):
        """ Test whether effectSmall attribute is created correctly
        """
        exp = par.ParticleRef(**self.effectSmall)
        self.assertEqual(self.inst.effectSmall,exp)

    def testCreationAttribEffectMedium(self):
        """ Test whether effectMedium attribute is created correctly
        """
        exp = par.ParticleRef(**self.effectMedium)
        self.assertEqual(self.inst.effectMedium,exp)

    def testCreationAttribEffectLarge(self):
        """ Test whether effectLarge attribute is created correctly
        """
        exp = par.ParticleRef(**self.effectLarge)
        self.assertEqual(self.inst.effectLarge,exp)

    def testCreationAttribSound(self):
        """ Test whether sound attribute is created correctly
        """
        exp = audio.SoundRef(canBeEmpty=True,**self.sound)
        self.assertEqual(self.inst.sound,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckEffectSmall(self):
        """ Test whether check returns problems in effectSmall attrib
        """
        self.mockSmall.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockSmall.assert_called_once_with(self.mmod,False)

    def testCheckEffectMedium(self):
        """ Test whether check returns problems in effectMedium attrib
        """
        self.mockMedium.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockMedium.assert_called_once_with(self.mmod,False)

    def testCheckEffectLarge(self):
        """ Test whether check returns problems in effectLarge attrib
        """
        self.mockLarge.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockLarge.assert_called_once_with(self.mmod,False)

    def testCheckSound(self):
        """ Test whether check returns problems in sound attrib
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.audio.checkEffect.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mockProb])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests Weapon Effects ##########################"""

class WeaponEffectSpawnRandTests(unit.TestCase):
    desc = "Tests WeaponEffectSpawnRand:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "targetCentricEffectPointSpawnInfo\n"
            +"\tspawnType \"CompletelyRandom\"\n"
            +"\tminRadius 7500.000000\n"
            +"\tmaxRadius 10000.000000\n"
        )
        cls.identifier = "targetCentricEffectPointSpawnInfo"
        cls.spawnType = {"identifier":"spawnType","val":"CompletelyRandom"}
        cls.radiusMin = {"identifier":"minRadius","val":7500.0}
        cls.radiusMax = {"identifier":"maxRadius","val":10000.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.WeaponEffectSpawnRand(
                identifier=self.identifier,
                spawnType=self.spawnType,
                radiusMin=self.radiusMin,
                radiusMax=self.radiusMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_weapEffectSpawnInfoRand.parseString(
                self.parseString)[0]
        res = abilities.WeaponEffectSpawnRand.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSpawnType(self):
        """ Test whether spawnType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["CompletelyRandom","RandomFromUp"],
                **self.spawnType)
        self.assertEqual(self.inst.spawnType,exp)

    def testCreationAttribRadiusMin(self):
        """ Test whether radiusMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusMin)
        self.assertEqual(self.inst.radiusMin,exp)

    def testCreationAttribRadiusMax(self):
        """ Test whether radiusMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusMax)
        self.assertEqual(self.inst.radiusMax,exp)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid spawnType
        """
        invalid = {"identifier":"spawnType","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.WeaponEffectSpawnRand.factory(spawnType=invalid)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class WeaponEffectSpawnRandUpTests(unit.TestCase):
    desc = "Tests WeaponEffectSpawnRandUp:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "targetCentricEffectPointSpawnInfo\n"
            +"\tspawnType \"RandomFromUp\"\n"
            +"\tminRadius 7500.000000\n"
            +"\tmaxRadius 10000.000000\n"
            +"\tangleVariance 10.000000\n"
        )
        cls.identifier = "targetCentricEffectPointSpawnInfo"
        cls.spawnType = {"identifier":"spawnType","val":"RandomFromUp"}
        cls.radiusMin = {"identifier":"minRadius","val":7500.0}
        cls.radiusMax = {"identifier":"maxRadius","val":10000.0}
        cls.angleVar = {"identifier":"angleVariance","val":10.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.WeaponEffectSpawnRandUp(
                identifier=self.identifier,
                spawnType=self.spawnType,
                radiusMin=self.radiusMin,
                radiusMax=self.radiusMax,
                angleVar=self.angleVar
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_weapEffectSpawnInfoRandUp.parseString(
                self.parseString)[0]
        res = abilities.WeaponEffectSpawnRand.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAngleVar(self):
        """ Test whether angleVar attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.angleVar)
        self.assertEqual(self.inst.angleVar,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class WeaponEffectOriginTests(unit.TestCase):
    desc = "Tests WeaponEffectOrigin:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "weaponEffectOriginType \"Targetter\"\n"
        )
        cls.originType = {"identifier":"weaponEffectOriginType",
            "val":"Targetter"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.WeaponEffectOrigin(
                originType=self.originType
        )

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid originType
        """
        invalid = {"identifier":"weaponEffectOriginType",
            "val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.WeaponEffectOrigin.factory(originType=invalid)

    def testCreationAttribOriginType(self):
        """ Test whether originType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["Target","Targetter"],
                **self.originType)
        self.assertEqual(self.inst.originType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class WeaponEffectOriginTargetTests(unit.TestCase):
    desc = "Tests WeaponEffectOriginTarget:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "weaponEffectOriginType \"Target\"\n"
            "targetCentricEffectPointSpawnInfo\n"
            +"\tspawnType \"CompletelyRandom\"\n"
            +"\tminRadius 7500.000000\n"
            +"\tmaxRadius 10000.000000\n"
        )
        cls.originType = {"identifier":"weaponEffectOriginType",
            "val":"Target"}
        cls.targetCentricEffect = {
            "identifier":"targetCentricEffectPointSpawnInfo",
            "spawnType":{"identifier":"spawnType","val":"CompletelyRandom"},
            "radiusMin":{"identifier":"minRadius","val":7500.0},
            "radiusMax":{"identifier":"maxRadius","val":10000.0}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.WeaponEffectOriginTarget(
                originType=self.originType,
                targetCentricEffect=self.targetCentricEffect
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_weapEffectOriginTarget.parseString(
                self.parseString)[0]
        res = abilities.WeaponEffectOrigin.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTargetCentricEffect(self):
        """ Test whether targetCentricEffect attrib is constructed correctly
        """
        exp = abilities.WeaponEffectSpawnRand(**self.targetCentricEffect)
        self.assertEqual(self.inst.targetCentricEffect,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class WeaponEffectOriginTargetterTests(unit.TestCase):
    desc = "Tests WeaponEffectOriginTargetter:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "weaponEffectOriginType \"Targetter\"\n"
            +"weaponEffectAttachInfo\n"
            +"\tattachType \"Center\"\n"
        )
        cls.originType = {"identifier":"weaponEffectOriginType",
            "val":"Targetter"}
        cls.effectAttach = {
            "identifier":"weaponEffectAttachInfo",
            "attachType":{"identifier":"attachType","val":"Center"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.WeaponEffectOriginTargetter(
                originType=self.originType,
                effectAttach=self.effectAttach
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_weapEffectOriginTargetter.parseString(
                self.parseString)[0]
        res = abilities.WeaponEffectOrigin.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEffectAttach(self):
        """ Test whether effectAttach attrib is created correctly
        """
        exp = abilities.AttachInfo(**self.effectAttach)
        self.assertEqual(self.inst.effectAttach,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests Action Over Time #########################"""

class ActionOverTimeTests(unit.TestCase):
    desc = "Tests ActionOverTime:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "overTimeAction\n"
            +"\tbuffOverTimeActionType \"Magnetize\"\n"
        )
        cls.actionType = {"identifier":"buffOverTimeActionType",
            "val":"Magnetize"}
        cls.identifier = "overTimeAction"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionOverTime(
                identifier=self.identifier,
                actionType=self.actionType
        )

    def testCreationAttribActionType(self):
        """ Test whether actionType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.ACTION_OVER_TIME,
                **self.actionType)
        self.assertEqual(self.inst.actionType,exp)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid actionType
        """
        invalid = {"identifier":"buffOverTimeActionType",
            "val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.ActionOverTime.factory(actionType=invalid)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionOverTimeSimpleTests(unit.TestCase):
    desc = "Tests ActionOverTimeSimple:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "overTimeAction\n"
            +"\tbuffOverTimeActionType \"EarnResources\"\n"
            +"\tearnRate\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType = {"identifier":"buffOverTimeActionType",
            "val":"EarnResources"}
        cls.vals = [
            {
                "identifier":"earnRate",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.identifier = "overTimeAction"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionOverTimeSimple(
                identifier=self.identifier,
                actionType=self.actionType,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffActionOverTime.parseString(
                self.parseString)[0]
        res = abilities.ActionOverTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribActionType(self):
        """ Test whether earnRate attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.earnRate,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionOverTimeDoDmgTests(unit.TestCase):
    desc = "Tests ActionOverTimeDoDmg:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "overTimeAction\n"
            +"\tbuffOverTimeActionType \"DoDamage\"\n"
            +"\tdamageRate\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tdamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\tdamageType \"PHYSICAL\"\n"
            +"\tisDamageShared TRUE\n"
        )
        cls.actionType = {"identifier":"buffOverTimeActionType",
            "val":"DoDamage"}
        cls.vals = [
            {
                "identifier":"damageRate",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.dmgAffect = {"identifier":"damageAffectType",
            "val":"AFFECTS_ONLY_HULL"}
        cls.dmgType = {"identifier":"damageType","val":"PHYSICAL"}
        cls.dmgShared = {"identifier":"isDamageShared","val":True}
        cls.identifier = "overTimeAction"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionOverTimeDoDmg(
                identifier=self.identifier,
                actionType=self.actionType,
                vals=self.vals,
                dmgAffect=self.dmgAffect,
                dmgType=self.dmgType,
                dmgShared=self.dmgShared
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffActionOverTime.parseString(
                self.parseString)[0]
        res = abilities.ActionOverTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDamageRate(self):
        """ Test whether damageRate attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.damageRate,exp)

    def testCreationAttribDmgAffect(self):
        """ Test whether dmgAffect attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,**self.dmgAffect)
        self.assertEqual(self.inst.dmgAffect,exp)

    def testCreationAttribDmgType(self):
        """ Test whether dmgType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,**self.dmgType)
        self.assertEqual(self.inst.dmgType,exp)

    def testCreationAttribDmgShared(self):
        """ Test whether dmgShared attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.dmgShared)
        self.assertEqual(self.inst.dmgShared,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionOverTimeDoDmgDrainAMTests(unit.TestCase):
    desc = "Tests ActionOverTimeDoDmgDrainAM:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "overTimeAction\n"
            +"\tbuffOverTimeActionType \"DrainAntiMatterAndDoDamage\"\n"
            +"\tdrainAntiMatterRate\n"
            +"\t\tLevel:0 1.000000\n"
            +"\t\tLevel:1 2.000000\n"
            +"\t\tLevel:2 3.000000\n"
            +"\t\tLevel:3 4.000000\n"
            +"\tdamageRate\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tisDamageShared TRUE\n"
        )
        cls.actionType = {"identifier":"buffOverTimeActionType",
            "val":"DrainAntiMatterAndDoDamage"}
        cls.vals = [
            {
                "identifier":"drainAntiMatterRate",
                "levels":[
                    {"identifier":"Level:0","val":1.0},
                    {"identifier":"Level:1","val":2.0},
                    {"identifier":"Level:2","val":3.0},
                    {"identifier":"Level:3","val":4.0}
                ]
            },
            {
                "identifier":"damageRate",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.dmgShared = {"identifier":"isDamageShared","val":True}
        cls.identifier = "overTimeAction"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionOverTimeDoDmgDrainAM(
                identifier=self.identifier,
                actionType=self.actionType,
                vals=self.vals,
                dmgShared=self.dmgShared
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffActionOverTime.parseString(
                self.parseString)[0]
        res = abilities.ActionOverTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDamageRate(self):
        """ Test whether damageRate attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.damageRate,exp)

    def testCreationAttribDamageRateDrainAMRate(self):
        """ Test whether drainAntinatterRate attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.drainAntiMatterRate,exp)

    def testCreationAttribDmgShared(self):
        """ Test whether dmgShared attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.dmgShared)
        self.assertEqual(self.inst.dmgShared,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionOverTimeMagnetize(unit.TestCase):
    desc = "Tests ActionOverTimeMagnetize:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "overTimeAction\n"
            +"\tbuffOverTimeActionType \"Magnetize\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tmaxNumTargets\n"
            +"\t\tLevel:0 1.000000\n"
            +"\t\tLevel:1 2.000000\n"
            +"\t\tLevel:2 3.000000\n"
            +"\t\tLevel:3 4.000000\n"
            +"\tdamagePerImpact\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tpullForceMagnitude 100\n"
        )
        cls.actionType = {"identifier":"buffOverTimeActionType",
            "val":"Magnetize"}
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"maxNumTargets",
                "levels":[
                    {"identifier":"Level:0","val":1.0},
                    {"identifier":"Level:1","val":2.0},
                    {"identifier":"Level:2","val":3.0},
                    {"identifier":"Level:3","val":4.0}
                ]
            },
            {
                "identifier":"damagePerImpact",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {"identifier":"pullForceMagnitude","val":100}
        ]
        cls.identifier = "overTimeAction"
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionOverTimeMagnetize(
                identifier=self.identifier,
                actionType=self.actionType,
                vals=self.vals,
                targetFilter=self.targetFilter
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffActionOverTime.parseString(
                self.parseString)[0]
        res = abilities.ActionOverTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTargetFilter(self):
        """ Test whether targetFilter attrib is created correctly
        """
        exp = abilities.TargetFilter(**self.targetFilter)
        self.assertEqual(self.inst.targetFilter,exp)

    def testCreationAttribRange(self):
        """ Test whether range attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribMaxNumTargets(self):
        """ Test whether maxNumTargets attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.maxNumTargets,exp)

    def testCreationAttribDamagePerImpact(self):
        """ Test whether damagePerImpact attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[2])
        self.assertEqual(self.inst.damagePerImpact,exp)

    def testCreationAttribPullForceMagnitude(self):
        """ Test whether pullForceMagnitude attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[3])
        self.assertEqual(self.inst.pullForceMagnitude,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""####################### Tests BuffEntityModifier ########################"""

class BuffEntityModifierTests(unit.TestCase):
    desc = "Tests BuffEntityModifier:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "entityModifier\n"
            +"\tbuffEntityModifierType \"AngularThrust\"\n"
            +"\tvalue\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.identifier = "entityModifier"
        cls.modType = {"identifier":"buffEntityModifierType",
            "val":"AngularThrust"}
        cls.value = {
            "identifier":"value",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.BuffEntityModifier(
                identifier=self.identifier,
                modType=self.modType,
                value=self.value
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffEntityModifier.parseString(self.parseString
        )[0]
        res = abilities.BuffEntityModifier(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribModType(self):
        """ Test whether modType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.BUFF_ENTITY_MODIFIERS,
                **self.modType)
        self.assertEqual(self.inst.modType,exp)

    def testCreationAttribValue(self):
        """ Test whether value attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.value)
        self.assertEqual(self.inst.value,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests Instant Actions ##########################"""

class InstantActionFixture(object):
    @classmethod
    def setUpClass(cls):
        cls.identifier = "instantAction"
        cls.actionType = {"identifier":"buffInstantActionType","val":"Explore"}
        cls.trigger = {
            "triggerType":{"identifier":"instantActionTriggerType",
                "val":"OnDelay"},
            "delay":{"identifier":"delayTime","val":100.0}
        }

class InstantActionTests(InstantActionFixture,unit.TestCase):
    desc = "Tests InstantAction:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"Explore\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
        )
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.InstantAction(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error for invalid actionType
        """
        invalid = {"identifier":"buffInstantActionType","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.InstantAction.factory(actionType=invalid)

    def testCreationAttribActionType(self):
        """ Test whether actionType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.INSTANT_ACTION_TYPES,
                **self.actionType)
        self.assertEqual(self.inst.actionType,exp)

    def testCreationAttribTrigger(self):
        """ Test whether trigger attrib is created correctly
        """
        exp = abilities.TriggerDelay(**self.trigger)
        self.assertEqual(self.inst.trigger,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionWeaponEffectTestsHas(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionWeaponEffect with Effects:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"PlayPersistantBeamEffect\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\thasWeaponEffects TRUE\n"
            +"\tweaponEffectOriginType \"Targetter\"\n"
            +"\tweaponEffectAttachInfo\n"
            +"\t\tattachType \"Center\"\n"
            +"\tweaponEffectImpactOffsetType \"RandomMesh\"\n"
            +"\tcanWeaponEffectHitHull TRUE\n"
            +"\tcanWeaponEffectHitShields TRUE\n"
            +"\tweaponEffectsDef\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.000000\n"
            +"\t\tfireDelay 0.000000\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.actionType["val"] = "PlayPersistantBeamEffect"
        cls.hasWeapEffect = {"identifier":"hasWeaponEffects","val":True}
        cls.origin = {
            "originType":{"identifier":"weaponEffectOriginType",
                "val":"Targetter"},
            "effectAttach":{
                "identifier":"weaponEffectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Center"}
            }
        }
        cls.impactOffsetType = {"identifier":"weaponEffectImpactOffsetType",
            "val":"RandomMesh"}
        cls.effectHitsHull = {"identifier":"canWeaponEffectHitHull","val":True}
        cls.effectHitsShield = {"identifier":"canWeaponEffectHitShields",
            "val":True}
        cls.weaponEffect = {
            "identifier":"weaponEffectsDef",
            "effectType":{"identifier":"weaponType","val":"Projectile"},
            "burstCount":{"identifier":"burstCount","val":1},
            "burstDelay":{"identifier":"burstDelay","val":0.0},
            "fireDelay":{"identifier":"fireDelay","val":0.0},
            "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
            "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                "val":0.1},
            "sndMuzzle":{
                "identifier":"muzzleSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndmuzzle"}
                ]
            },
            "hitEff":{"identifier":"hitEffectName","val":"effhit"},
            "sndHitHull":{
                "identifier":"hitHullEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndhullhit"}
                ]
            },
            "sndHitShields":{
                "identifier":"hitShieldsEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndshieldhit"}
                ]
            },
            "travelEff":{"identifier":"projectileTravelEffectName",
                "val":"traveleff"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionWeaponEffect(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                hasWeapEffect=self.hasWeapEffect,
                origin=self.origin,
                impactOffsetType=self.impactOffsetType,
                effectHitsHull=self.effectHitsHull,
                effectHitsShield=self.effectHitsShield,
                weaponEffect=self.weaponEffect
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribHasWeapEffect(self):
        """ Test whether hasWeapEffect attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.hasWeapEffect)
        self.assertEqual(self.inst.hasWeapEffect,exp)

    def testCreationAttribOrigin(self):
        """ Test whether origin attrib is created correctly
        """
        exp = abilities.WeaponEffectOriginTargetter(**self.origin)
        self.assertEqual(self.inst.origin,exp)

    def testCreationAttribImpactOffsetType(self):
        """ Test whether impactOffsetType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.WEAP_EFFECT_IMPACT_OFFSET,
                **self.impactOffsetType)
        self.assertEqual(self.inst.impactOffsetType,exp)

    def testCreationAttribEffectHitsHull(self):
        """ Test whether effectHitsHull attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.effectHitsHull)
        self.assertEqual(self.inst.effectHitsHull,exp)

    def testCreationAttribEffectHitsShields(self):
        """ Test whether effectHitsShield attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.effectHitsShield)
        self.assertEqual(self.inst.effectHitsShield,exp)

    def testCreationAttribWeaponEffect(self):
        """ Test whether weaponEffect attrib is created correctly
        """
        exp = armed.ProjectileWeaponEff(**self.weaponEffect)
        self.assertEqual(self.inst.weaponEffect,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionWeaponEffectTestsHasNot(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionWeaponEffect without Effects:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"PlayPersistantBeamEffect\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\thasWeaponEffects FALSE\n"
        )
        cls.actionType["val"] = "PlayPersistantBeamEffect"
        cls.hasWeapEffect = {"identifier":"hasWeaponEffects","val":False}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionWeaponEffect(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                hasWeapEffect=self.hasWeapEffect
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribOrigin(self):
        """ Test whether origin attrib is created correctly
        """
        self.assertFalse(hasattr(self.inst,"origin"))

    def testCreationAttribImpactOffsetType(self):
        """ Test whether impactOffsetType attrib is created correctly
        """
        self.assertFalse(hasattr(self.inst,"impactOffsetType"))

    def testCreationAttribEffectHitsHull(self):
        """ Test whether effectHitsHull attrib is created correctly
        """
        self.assertFalse(hasattr(self.inst,"effectHitsHull"))

    def testCreationAttribEffectHitsShields(self):
        """ Test whether effectHitsShield attrib is created correctly
        """
        self.assertFalse(hasattr(self.inst,"effectHitsShield"))

    def testCreationAttribWeaponEffect(self):
        """ Test whether weaponEffect attrib is created correctly
        """
        self.assertFalse(hasattr(self.inst,"weaponEffect"))

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyBuffTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyBuff:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"RemoveBuffOfType\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffTypeToRemove \"testbuff\"\n"
        )
        cls.actionType["val"] = "RemoveBuffOfType"
        cls.buff = {"identifier":"buffTypeToRemove","val":"testbuff"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyBuff(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBuff(self):
        """ Test whether buff attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        self.assertEqual(self.inst.buff,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionWithEffectTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionWithEffect:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"PlayAttachedEffect\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "PlayAttachedEffect"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionWithEffect(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                effectInfo=self.effectInfo
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEffectInfo(self):
        """ Test whether effectInfo attrib is created correctly
        """
        exp = abilities.AttachEffect(**self.effectInfo)
        self.assertEqual(self.inst.effectInfo,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyEffect(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyEffect:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToSelf\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ApplyBuffToSelf"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyEffect(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                effectInfo=self.effectInfo
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyOrRemove(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyOrRemove:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyOrRemoveBuffToSelf\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
            +"\ttoggleStateOnNameStringID \"name\"\n"
            +"\ttoggleStateOnDescStringID \"desc\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ApplyOrRemoveBuffToSelf"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.toggleOnName = {"identifier":"toggleStateOnNameStringID",
            "val":"name"}
        cls.toggleOnDesc = {"identifier":"toggleStateOnDescStringID",
            "val":"desc"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyOrRemove(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                effectInfo=self.effectInfo,
                toggleOnName=self.toggleOnName,
                toggleOnDesc=self.toggleOnDesc
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribToggleOnName(self):
        """ Test whether toggleOnName attrib is created correctly
        """
        exp = ui.StringReference(**self.toggleOnName)
        self.assertEqual(self.inst.toggleOnName,exp)

    def testCreationAttribToggleOnDesc(self):
        """ Test whether toggleOnDesc attrib is created correctly
        """
        exp = ui.StringReference(**self.toggleOnDesc)
        self.assertEqual(self.inst.toggleOnDesc,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateShell(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateShell:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateCannonShell\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tcannonShellType \"testshell\"\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "CreateCannonShell"
        cls.entity = {"identifier":"cannonShellType","val":"testshell"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateShell(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                effectInfo=self.effectInfo
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attrib is created correctly
        """
        exp = coe.EntityRef(types=["CannonShell"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyFilterTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyFilter:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToSelfWithFilter\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
        )
        cls.actionType["val"] = "ApplyBuffToSelfWithFilter"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyFilter(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                targetFilter=self.targetFilter
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTargetFilter(self):
        """ Test whether targetFilter attrib is created correctly
        """
        exp = abilities.TargetFilter(**self.targetFilter)
        self.assertEqual(self.inst.targetFilter,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyEffFilter(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyEffFilter:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToTargetsLinked\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.actionType["val"] = "ApplyBuffToTargetsLinked"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyEffFilter(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                targetFilter=self.targetFilter,
                effectInfo=self.effectInfo
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionSimpleTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionSimple:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyForceFromSpawner\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tforce 2.000000\n"
        )
        cls.actionType["val"] = "ApplyForceFromSpawner"
        cls.vals = [
            {"identifier":"force","val":2.0}
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionSimple(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribForce(self):
        """ Test whether force attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[0])
        self.assertEqual(self.inst.force,exp)

    def testCheck(self):
        """ Test whether force attrib is checked correctly
        """
        with mock.patch.object(self.inst.force,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyEffNum(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyEffNum:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyArbitraryTargettedBuffToSelf\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ApplyArbitraryTargettedBuffToSelf"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyEffNum(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                effectInfo=self.effectInfo,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyTargetEffTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyTargetEff:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToTarget\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ApplyBuffToTarget"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyTargetEff(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                effectInfo=self.effectInfo,
                vals=self.vals,
                targetFilter=self.targetFilter
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyEntryVehicleTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyEntryVehicleTests:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToTargetWithEntryVehicles\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tentryVehicleType \"vehicle\"\n"
            +"\tnumEntryVehicles\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\ttravelTime 10.500000\n"
            +"\tentryVehicleLaunchInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"entryVehicleLaunchInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ApplyBuffToTargetWithEntryVehicles"
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.range = {
            "identifier":"range",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.entryVehicle = {"identifier":"entryVehicleType","val":"vehicle"}
        cls.vals = [
            {
                "identifier":"numEntryVehicles",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {"identifier":"travelTime","val":10.5}
        ]
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyEntryVehicle(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                effectInfo=self.effectInfo,
                vals=self.vals,
                targetFilter=self.targetFilter,
                range=self.range,
                entryVehicle=self.entryVehicle
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.range)
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribEntryVehicle(self):
        """ Test whether entryVehicle attribute is constructed correctly
        """
        exp = coe.EntityRef(types=["EntryVehicle"],**self.entryVehicle)
        self.assertEqual(self.inst.entryVehicle,exp)

    def testCreationAttribNumEntryVehicles(self):
        """ Test whether numEntryVehicles attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numEntryVehicles,exp)

    def testCreationAttribTravelTime(self):
        """ Test whether travelTime attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.vals[1])
        self.assertEqual(self.inst.travelTime,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyConeTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyCone:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToTargetsInDirectionalCone\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"testbuff\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\tweaponBank \"FRONT\"\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tconeAngle\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tmaxTargetCount\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ApplyBuffToTargetsInDirectionalCone"
        cls.weapBank = {"identifier":"weaponBank","val":"FRONT"}
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"coneAngle",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"maxTargetCount",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
        ]
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyCone(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                effectInfo=self.effectInfo,
                vals=self.vals,
                targetFilter=self.targetFilter,
                weapBank=self.weapBank
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribConeAngle(self):
        """ Test whether coneAngle attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.coneAngle,exp)

    def testCreationAttribMaxTargetCount(self):
        """ Test whether maxTargetCount attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[2])
        self.assertEqual(self.inst.maxTargetCount,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionGiveCredits(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionGiveCredits:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"GiveCreditsToPlayer\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tcredits\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "GiveCreditsToPlayer"
        cls.vals = [
            {
                "identifier":"credits",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionGiveCredits(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                effectInfo=self.effectInfo,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCredits(self):
        """ Test whether credits attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.credits,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionPlayEffectRadius(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionPlayEffectRadius:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"PlayDetachedEffectsInRadius\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tnumEffects 3\n"
            +"\tdelayBetweenEffects 4.000000\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "PlayDetachedEffectsInRadius"
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {"identifier":"numEffects","val":3},
            {"identifier":"delayBetweenEffects","val":4.0}
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionPlayEffectRadius(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                effectInfo=self.effectInfo,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribNumEffects(self):
        """ Test whether numEffects attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.vals[1])
        self.assertEqual(self.inst.numEffects,exp)

    def testCreationAttribDelyBetweenEffects(self):
        """ Test whether delayBetweenEffects attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.vals[2])
        self.assertEqual(self.inst.delayBetweenEffects,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionColonizePlanetTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionColonizePlanet:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ColonizePlanet\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tdelayUntilColonization 6.300000\n"
            +"\tentryVehicleType \"vehicle\"\n"
            +"\tnumEntryVehicles\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\ttravelTime 6.200000\n"
            +"\tentryVehicleLaunchInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
            +"\tafterColonizeBuffType \"colbuff\"\n"
        )
        cls.effectInfo = {
            "identifier":"entryVehicleLaunchInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "ColonizePlanet"
        cls.range = {
            "identifier":"range",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.delay = {"identifier":"delayUntilColonization","val":6.3}
        cls.entryVehicles = {"identifier":"entryVehicleType","val":"vehicle"}
        cls.numEntryVehicles = {
            "identifier":"numEntryVehicles",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.travelTime = {"identifier":"travelTime","val":6.2}
        cls.afterColonizeBuff = {"identifier":"afterColonizeBuffType",
            "val":"colbuff"}
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionColonizePlanet(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                effectInfo=self.effectInfo,
                targetFilter=self.targetFilter,
                afterColonizeBuff=self.afterColonizeBuff,
                travelTime=self.travelTime,
                numEntryVehicles=self.numEntryVehicles,
                entryVehicles=self.entryVehicles,
                delay=self.delay,
                range=self.range
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.range)
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribTargetFilter(self):
        """ Test whether targetFilter attrib is created correctly
        """
        exp = abilities.TargetFilter(**self.targetFilter)
        self.assertEqual(self.inst.targetFilter,exp)

    def testCreationAttribDelay(self):
        """ Test whether delay attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.delay)
        self.assertEqual(self.inst.delay,exp)

    def testCreationAttribEntryVehicles(self):
        """ Test whether entryVehicles attrib is created correctly
        """
        exp = coe.EntityRef(types=["EntryVehicle"],**self.entryVehicles)
        self.assertEqual(self.inst.entryVehicles,exp)

    def testCreationAttribNumEntryVehicles(self):
        """ Test whether numEntryVehicles attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.numEntryVehicles)
        self.assertEqual(self.inst.numEntryVehicles,exp)

    def testCreationAttribTravelTime(self):
        """ Test whether travelTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.travelTime)
        self.assertEqual(self.inst.travelTime,exp)

    def testCreationAttribAfterColonizeBuff(self):
        """ Test whether afterColonizeBuff attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.afterColonizeBuff)
        self.assertEqual(self.inst.afterColonizeBuff,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionIllusionFightersTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionIllusionFighters:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateIllusionFighters\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tnumFighters\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
        )
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.actionType["val"] = "CreateIllusionFighters"
        cls.vals = [
            {
                "identifier":"numFighters",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionIllusionFighters(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                effectInfo=self.effectInfo,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribNumFighters(self):
        """ Test whether numFighters attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numFighters,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attribute is constructed correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.expiryTime,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateStarbaseTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateStarbase:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateStarBase\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tstarBaseType \"testbase\"\n"
            +"\tplacementRadius\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "CreateStarBase"
        cls.entity = {"identifier":"starBaseType","val":"testbase"}
        cls.vals = [
            {
                "identifier":"placementRadius",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateStarbase(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                entity=self.entity
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attrib is created correctly
        """
        exp = coe.EntityRef(types=["StarBase"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribPlacementRadius(self):
        """ Test whether placementRadius attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.placementRadius,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionChangePlayerTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionChangePlayer:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ChangePlayerIndexToFirstSpawner\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tisPermanent TRUE\n"
            +"\tfailIfNotEnoughShipSlots FALSE\n"
            +"\texperiencePercentageToAward 0.500000\n"
        )
        cls.actionType["val"] = "ChangePlayerIndexToFirstSpawner"
        cls.permanent = {"identifier":"isPermanent","val":True}
        cls.failIfLackingSupply = {"identifier":"failIfNotEnoughShipSlots",
            "val":False}
        cls.vals = [
            {"identifier":"experiencePercentageToAward","val":0.5}
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionChangePlayer(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                permanent=self.permanent,
                failIfLackingSupply=self.failIfLackingSupply
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribExperiencePercentageToAward(self):
        """ Test whether experiencePercentageToAward attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[0])
        self.assertEqual(self.inst.experiencePercentageToAward,exp)

    def testCreationAttribPermanent(self):
        """ Test whether permanent attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.permanent)
        self.assertEqual(self.inst.permanent,exp)

    def testCreationAttribFailIfLackingSupply(self):
        """ Test whether failIfLackingSupply attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.failIfLackingSupply)
        self.assertEqual(self.inst.failIfLackingSupply,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionSquadToMines(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionSquadToMines:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ConvertSquadMembersToMines\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tspaceMineType \"testbase\"\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "ConvertSquadMembersToMines"
        cls.entity = {"identifier":"spaceMineType","val":"testbase"}
        cls.vals = [
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionSquadToMines(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                entity=self.entity
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attrib is created correctly
        """
        exp = coe.EntityRef(types=["SpaceMine"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.expiryTime,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionDoDamage(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionDoDamage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"DoDamage\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tdamage\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tdamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\tdamageType \"PHYSICAL\"\n"
            +"\tisDamageShared TRUE\n"
        )
        cls.actionType["val"] = "DoDamage"
        cls.vals = [
            {
                "identifier":"damage",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.dmgAffect = {"identifier":"damageAffectType",
            "val":"AFFECTS_ONLY_HULL"}
        cls.dmgType = {"identifier":"damageType","val":"PHYSICAL"}
        cls.isShared = {"identifier":"isDamageShared","val":True}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionDoDamage(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                dmgAffect=self.dmgAffect,
                isShared=self.isShared,
                dmgType=self.dmgType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDamage(self):
        """ Test whether damage attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.damage,exp)

    def testCreationAttribDmgAffect(self):
        """ Test whether dmgAffect attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_AFFECTS,**self.dmgAffect)
        self.assertEqual(self.inst.dmgAffect,exp)

    def testCreationAttribDmgType(self):
        """ Test whether dmgType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,**self.dmgType)
        self.assertEqual(self.inst.dmgType,exp)

    def testCreationAttribIsShared(self):
        """ Test whether isShared attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.isShared)
        self.assertEqual(self.inst.isShared,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionDoDmgPerEntityTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionDoDmgPerEntity:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"DoDamagePerEntityInRadius\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tdamagePerEntity\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tdamageAffectType \"AFFECTS_ONLY_HULL\"\n"
            +"\tdamageType \"PHYSICAL\"\n"
            +"\tisDamageShared TRUE\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tmaxTargetCount\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "DoDamagePerEntityInRadius"
        cls.vals = [
            {
                "identifier":"damagePerEntity",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.dmgAffect = {"identifier":"damageAffectType",
            "val":"AFFECTS_ONLY_HULL"}
        cls.dmgType = {"identifier":"damageType","val":"PHYSICAL"}
        cls.isShared = {"identifier":"isDamageShared","val":True}
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.range = {
            "identifier":"range",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.maxTargets = {
            "identifier":"maxTargetCount",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionDoDmgPerEntity(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                dmgAffect=self.dmgAffect,
                isShared=self.isShared,
                dmgType=self.dmgType,
                targetFilter=self.targetFilter,
                range=self.range,
                maxTargets=self.maxTargets
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDamagePerEntity(self):
        """ Test whether damagePerEntity attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.damagePerEntity,exp)

    def testCreationAttribTargetFilter(self):
        """ Test whether targetFilter attrib is created correctly
        """
        exp = abilities.TargetFilter(**self.targetFilter)
        self.assertEqual(self.inst.targetFilter,exp)

    def testCreationAttribRange(self):
        """ Test whether range attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.range)
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribMaxTargets(self):
        """ Test whether maxTargets attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.maxTargets)
        self.assertEqual(self.inst.maxTargets,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionDebrisToHullTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionDebrisToHull:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ConvertNearbyDebrisToHull\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\thullPerDebris\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tdebrisCleanupSounds\n"
            +"\t\tsoundCount 1\n"
            +"\t\tsound \"testsound\"\n"
        )
        cls.actionType["val"] = "ConvertNearbyDebrisToHull"
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"hullPerDebris",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.cleanupSounds = {
            "identifier":"debrisCleanupSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"testsound"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionDebrisToHull(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                cleanupSounds=self.cleanupSounds
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRange(self):
        """ Test whether range attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribHullPerDebris(self):
        """ Test whether hullPerDebris attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.hullPerDebris,exp)

    def testCreationAttribCleanupSounds(self):
        """ Test whether cleanupSounds attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.cleanupSounds)
        self.assertEqual(self.inst.cleanupSounds,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionPropagateDamageTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionPropagateDamage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType "
             "\"PropagateWeaponDamageReceivedToTargetsInRadius\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tmaxTargetCount\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "PropagateWeaponDamageReceivedToTargetsInRadius"
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"maxTargetCount",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionPropagateDamage(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                targetFilter=self.targetFilter
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTargetFilter(self):
        """ Test whether targetFilter attrib is created correctly
        """
        exp = abilities.TargetFilter(**self.targetFilter)
        self.assertEqual(self.inst.targetFilter,exp)

    def testCreationAttribRange(self):
        """ Test whether range attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribMaxTargetCount(self):
        """ Test whether maxTargetCount attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.maxTargetCount,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionStealResources(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionStealResources:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"StealResources\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tresourceToSteal \"Crystal\"\n"
            +"\tresourceAmount\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "StealResources"
        cls.vals = [
            {
                "identifier":"resourceAmount",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.resource = {"identifier":"resourceToSteal","val":"Crystal"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionStealResources(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                resource=self.resource
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribResourceAmount(self):
        """ Test whether resourceAmount attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.resourceAmount,exp)

    def testCreationAttribResource(self):
        """ Test whether resource attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.RESOURCE_TYPES,
                **self.resource)
        self.assertEqual(self.inst.resource,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionConvertDmgAM(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionConvertDmgAM:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ConvertDamageToAntiMatter\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tdamageType \"PHYSICAL\"\n"
            +"\tantiMatterFromDamagePerc\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "ConvertDamageToAntiMatter"
        cls.dmgType = {"identifier":"damageType","val":"PHYSICAL"}
        cls.vals = [
            {
                "identifier":"antiMatterFromDamagePerc",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionConvertDmgAM(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                dmgType=self.dmgType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAntiMatterFromDamagePerc(self):
        """ Test whether antiMatterFromDamagePerc attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.antiMatterFromDamagePerc,exp)

    def testCreationAttribDmgType(self):
        """ Test whether dmgType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.DMG_TYPE,
                **self.dmgType)
        self.assertEqual(self.inst.dmgType,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateClonedFrgTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateClonedFrg:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateClonedFrigate\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tnumFrigates\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\timpulse 10.500000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tspawnFrigateSoundID \"testsound\"\n"
        )
        cls.actionType["val"] = "CreateClonedFrigate"
        cls.spawnSound = {"identifier":"spawnFrigateSoundID","val":"testsound"}
        cls.vals = [
            {
                "identifier":"numFrigates",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {"identifier":"impulse","val":10.5},
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateClonedFrg(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                spawnSound=self.spawnSound
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribNumFrigates(self):
        """ Test whether numFrigates attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numFrigates,exp)

    def testCreationAttribImpulse(self):
        """ Test whether impulse attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[1])
        self.assertEqual(self.inst.impulse,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[2])
        self.assertEqual(self.inst.expiryTime,exp)

    def testCreationAttribSpawnSound(self):
        """ Test whether spawnSound attrib is created correctly
        """
        exp = audio.SoundRef(**self.spawnSound)
        self.assertEqual(self.inst.spawnSound,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateUnitPlanetModulesTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateUnit with PlanetModule actionType:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreatePlanetModule\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tplanetModuleType \"testtype\"\n"
            +"\tspawnPlanetModuleSoundID \"testsound\"\n"
        )
        cls.actionType["val"] = "CreatePlanetModule"
        cls.spawnSound = {"identifier":"spawnPlanetModuleSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"planetModuleType","val":"testtype"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateUnit(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                spawnSound=self.spawnSound
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attribute is created correctly
        """
        exp = coe.EntityRef(types=["PlanetModuleHangarDefense",
            "PlanetModuleRefinery","PlanetModuleShipFactory",
            "PlanetModuleStandard","PlanetModuleTradePort",
            "PlanetModuleWeaponDefense"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateUnitSpaceMineTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateUnit with SpaceMine actionType:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateSpaceMine\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tspaceMineType \"testtype\"\n"
            +"\tnumMines\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\timpulse 10.500000\n"
            +"\tangleVariance 9.700000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tspawnMineSoundID \"testsound\"\n"
        )
        cls.actionType["val"] = "CreateSpaceMine"
        cls.spawnSound = {"identifier":"spawnMineSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"spaceMineType","val":"testtype"}
        cls.vals = [
            {
                "identifier":"numMines",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {"identifier":"impulse","val":10.5},
            {"identifier":"angleVariance","val":9.7},
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateUnit(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                vals=self.vals,
                spawnSound=self.spawnSound
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attribute is created correctly
        """
        exp = coe.EntityRef(types=["SpaceMine"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribNumMines(self):
        """ Test whether numMines attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numMines,exp)

    def testCreationAttribImpulse(self):
        """ Test whether impulse attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[1])
        self.assertEqual(self.inst.impulse,exp)

    def testCreationAttribAngleVariance(self):
        """ Test whether angleVariance attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[2])
        self.assertEqual(self.inst.angleVariance,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[3])
        self.assertEqual(self.inst.expiryTime,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateUnitSuqadTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateUnit with Squad actionType:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateSquad\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tsquadType \"testtype\"\n"
            +"\tnumSquads\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tspawnSquadSoundID \"testsound\"\n"
        )
        cls.actionType["val"] = "CreateSquad"
        cls.spawnSound = {"identifier":"spawnSquadSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"squadType","val":"testtype"}
        cls.vals = [
            {
                "identifier":"numSquads",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateUnit(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                vals=self.vals,
                spawnSound=self.spawnSound
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attribute is created correctly
        """
        exp = coe.EntityRef(types=["Squad"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribNumSquads(self):
        """ Test whether numSquads attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numSquads,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.expiryTime,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateUnitInvalidActionTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateUnit with invalid actionType:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.actionType["val"] = "DoDamageToPlanet"
        cls.spawnSound = {"identifier":"spawnSquadSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"squadType","val":"testtype"}
        cls.vals = [
            {
                "identifier":"numSquads",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.mmod = tco.genMockMod("./")

    def testCreationInvalidActionType(self):
        """ Test whether creation with invalid actionType raises error
        """
        with self.assertRaises(RuntimeError):
            inst = abilities.ActionCreateUnit(
                    identifier=self.identifier,
                    actionType=self.actionType,
                    trigger=self.trigger,
                    entity=self.entity,
                    vals=self.vals,
                    spawnSound=self.spawnSound
            )

class ActionCreateFrigateTargetArbitraryTests(InstantActionFixture,
        unit.TestCase):
    desc = "Tests ActionCreateFrigateTargetArbitrary:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateFrigateAtArbitraryTarget\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tfrigateType \"testtype\"\n"
            +"\tnumFrigates\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tspawnFrigateSoundID \"testsound\"\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "CreateFrigateAtArbitraryTarget"
        cls.spawnSound = {"identifier":"spawnFrigateSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"frigateType","val":"testtype"}
        cls.vals = [
            {
                "identifier":"numFrigates",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.range = {
            "identifier":"range",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateFrigateTargetArbitrary(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                vals=self.vals,
                spawnSound=self.spawnSound,
                range=self.range
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attribute is created correctly
        """
        exp = coe.EntityRef(types=["Frigate"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribNumFrigates(self):
        """ Test whether numFrigates attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numFrigates,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.expiryTime,exp)

    def testCreationAttribRange(self):
        """ Test whether range attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.range)
        self.assertEqual(self.inst.range,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateFrgTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateFrg:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateFrigate\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tfrigateType \"testtype\"\n"
            +"\tnumFrigates\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tmatchOwnerDamageState TRUE\n"
            +"\timpulse 5.600000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tspawnFrigateSoundID \"testsound\"\n"
            +"\tpostSpawnBuff \"spawnbuff\"\n"
        )
        cls.actionType["val"] = "CreateFrigate"
        cls.spawnSound = {"identifier":"spawnFrigateSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"frigateType","val":"testtype"}
        cls.matchOwnerDmg = {"identifier":"matchOwnerDamageState","val":True}
        cls.postSpawnBuff = {"identifier":"postSpawnBuff","val":"spawnbuff"}
        cls.vals = [
            {"identifier":"impulse","val":5.6},
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.numFrigates = {
            "identifier":"numFrigates",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateFrg(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                vals=self.vals,
                spawnSound=self.spawnSound,
                numFrigates=self.numFrigates,
                postSpawnBuff=self.postSpawnBuff,
                matchOwnerDmg=self.matchOwnerDmg
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attribute is created correctly
        """
        exp = coe.EntityRef(types=["Frigate"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribPostSpawnBuff(self):
        """ Test whether postSpawnBuff attribute is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.postSpawnBuff)
        self.assertEqual(self.inst.postSpawnBuff,exp)

    def testCreationAttribNumFrigates(self):
        """ Test whether numFrigates attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.numFrigates)
        self.assertEqual(self.inst.numFrigates,exp)

    def testCreationAttribImpulse(self):
        """ Test whether impulse attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[0])
        self.assertEqual(self.inst.impulse,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[1])
        self.assertEqual(self.inst.expiryTime,exp)

    def testCreationAttribMatchOwnerDmg(self):
        """ Test whether matchOwnerDmg attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.matchOwnerDmg)
        self.assertEqual(self.inst.matchOwnerDmg,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCreateFrgTargetTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionCreateFrgTarget:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"CreateFrigateAtTarget\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tfrigateType \"testtype\"\n"
            +"\tnumFrigates\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\timpulse 5.600000\n"
            +"\texpiryTime\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tspawnFrigateSoundID \"testsound\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
        )
        cls.actionType["val"] = "CreateFrigateAtTarget"
        cls.spawnSound = {"identifier":"spawnFrigateSoundID",
            "val":"testsound"}
        cls.entity = {"identifier":"frigateType","val":"testtype"}
        cls.vals = [
            {
                "identifier":"numFrigates",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            },
            {"identifier":"impulse","val":5.6},
            {
                "identifier":"expiryTime",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.range = {
            "identifier":"range",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCreateFrgTarget(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                entity=self.entity,
                vals=self.vals,
                spawnSound=self.spawnSound,
                targetFilter=self.targetFilter,
                range=self.range
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribEntity(self):
        """ Test whether entity attribute is created correctly
        """
        exp = coe.EntityRef(types=["Frigate"],**self.entity)
        self.assertEqual(self.inst.entity,exp)

    def testCreationAttribTargetFilter(self):
        """ Test whether targetFilter attrib is created correctly
        """
        exp = abilities.TargetFilter(**self.targetFilter)
        self.assertEqual(self.inst.targetFilter,exp)

    def testCreationAttribRange(self):
        """ Test whether numFrigates attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.range)
        self.assertEqual(self.inst.range,exp)

    def testCreationAttribImpulse(self):
        """ Test whether impulse attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[1])
        self.assertEqual(self.inst.impulse,exp)

    def testCreationAttribExpiryTime(self):
        """ Test whether expiryTime attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[2])
        self.assertEqual(self.inst.expiryTime,exp)

    def testCreationAttribNumFrigates(self):
        """ Test whether numFrigates attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.vals[0])
        self.assertEqual(self.inst.numFrigates,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyWeapTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyWeap:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType "
             "\"ApplyBuffToLastSpawnerWithTravelNoFilterNoRange\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"bufftest\"\n"
            +"\ttravelSpeed 100.000000\n"
            +"\thasWeaponEffects TRUE\n"
            +"\tweaponEffectOriginType \"Targetter\"\n"
            +"\tweaponEffectAttachInfo\n"
            +"\t\tattachType \"Center\"\n"
            +"\tweaponEffectImpactOffsetType \"RandomMesh\"\n"
            +"\tcanWeaponEffectHitHull TRUE\n"
            +"\tcanWeaponEffectHitShields TRUE\n"
            +"\tweaponEffectsDef\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.000000\n"
            +"\t\tfireDelay 0.000000\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.actionType[
            "val"] = "ApplyBuffToLastSpawnerWithTravelNoFilterNoRange"
        cls.buff = {"identifier":"buffType","val":"bufftest"}
        cls.vals = [
            {"identifier":"travelSpeed","val":100.0}
        ]
        cls.hasWeapEffect = {"identifier":"hasWeaponEffects","val":True}
        cls.origin = {
            "originType":{"identifier":"weaponEffectOriginType",
                "val":"Targetter"},
            "effectAttach":{
                "identifier":"weaponEffectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Center"}
            }
        }
        cls.impactOffsetType = {"identifier":"weaponEffectImpactOffsetType",
            "val":"RandomMesh"}
        cls.effectHitsHull = {"identifier":"canWeaponEffectHitHull","val":True}
        cls.effectHitsShield = {"identifier":"canWeaponEffectHitShields",
            "val":True}
        cls.weaponEffect = {
            "identifier":"weaponEffectsDef",
            "effectType":{"identifier":"weaponType","val":"Projectile"},
            "burstCount":{"identifier":"burstCount","val":1},
            "burstDelay":{"identifier":"burstDelay","val":0.0},
            "fireDelay":{"identifier":"fireDelay","val":0.0},
            "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
            "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                "val":0.1},
            "sndMuzzle":{
                "identifier":"muzzleSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndmuzzle"}
                ]
            },
            "hitEff":{"identifier":"hitEffectName","val":"effhit"},
            "sndHitHull":{
                "identifier":"hitHullEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndhullhit"}
                ]
            },
            "sndHitShields":{
                "identifier":"hitShieldsEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndshieldhit"}
                ]
            },
            "travelEff":{"identifier":"projectileTravelEffectName",
                "val":"traveleff"}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyWeap(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                hasWeapEffect=self.hasWeapEffect,
                origin=self.origin,
                impactOffsetType=self.impactOffsetType,
                effectHitsHull=self.effectHitsHull,
                effectHitsShield=self.effectHitsShield,
                weaponEffect=self.weaponEffect,
                buff=self.buff,
                vals=self.vals
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyTargetWeapTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyTargetWeap:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType "
             "\"ApplyBuffToTargetsInRadiusWithTravel\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"bufftest\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tmaxTargetCount\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\teffectInfo\n"
            +"\t\teffectAttachInfo\n"
            +"\t\t\tattachType \"Ability\"\n"
            +"\t\t\tabilityIndex 2\n"
            +"\t\tsmallEffectName \"small\"\n"
            +"\t\tmediumEffectName \"medium\"\n"
            +"\t\tlargeEffectName \"large\"\n"
            +"\t\tsoundID \"snd\"\n"
            +"\ttravelSpeed 100.000000\n"
            +"\teffectStaggerDelay 200.000000\n"
            +"\thasWeaponEffects TRUE\n"
            +"\tweaponEffectOriginType \"Targetter\"\n"
            +"\tweaponEffectAttachInfo\n"
            +"\t\tattachType \"Center\"\n"
            +"\tweaponEffectImpactOffsetType \"RandomMesh\"\n"
            +"\tcanWeaponEffectHitHull TRUE\n"
            +"\tcanWeaponEffectHitShields TRUE\n"
            +"\tweaponEffectsDef\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.000000\n"
            +"\t\tfireDelay 0.000000\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.actionType["val"] = "ApplyBuffToTargetsInRadiusWithTravel"
        cls.buff = {"identifier":"buffType","val":"bufftest"}
        cls.travelSpeed = {"identifier":"travelSpeed","val":100.0}
        cls.staggerDelay = {"identifier":"effectStaggerDelay","val":200.0}
        cls.hasWeapEffect = {"identifier":"hasWeaponEffects","val":True}
        cls.origin = {
            "originType":{"identifier":"weaponEffectOriginType",
                "val":"Targetter"},
            "effectAttach":{
                "identifier":"weaponEffectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Center"}
            }
        }
        cls.impactOffsetType = {"identifier":"weaponEffectImpactOffsetType",
            "val":"RandomMesh"}
        cls.effectHitsHull = {"identifier":"canWeaponEffectHitHull","val":True}
        cls.effectHitsShield = {"identifier":"canWeaponEffectHitShields",
            "val":True}
        cls.weaponEffect = {
            "identifier":"weaponEffectsDef",
            "effectType":{"identifier":"weaponType","val":"Projectile"},
            "burstCount":{"identifier":"burstCount","val":1},
            "burstDelay":{"identifier":"burstDelay","val":0.0},
            "fireDelay":{"identifier":"fireDelay","val":0.0},
            "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
            "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                "val":0.1},
            "sndMuzzle":{
                "identifier":"muzzleSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndmuzzle"}
                ]
            },
            "hitEff":{"identifier":"hitEffectName","val":"effhit"},
            "sndHitHull":{
                "identifier":"hitHullEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndhullhit"}
                ]
            },
            "sndHitShields":{
                "identifier":"hitShieldsEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndshieldhit"}
                ]
            },
            "travelEff":{"identifier":"projectileTravelEffectName",
                "val":"traveleff"}
        }
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.effectInfo = {
            "identifier":"effectInfo",
            "attachInfo":{
                "identifier":"effectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Ability"},
                "index":{"identifier":"abilityIndex","val":2}
            },
            "effectSmall":{"identifier":"smallEffectName","val":"small"},
            "effectMedium":{"identifier":"mediumEffectName","val":"medium"},
            "effectLarge":{"identifier":"largeEffectName","val":"large"},
            "sound":{"identifier":"soundID","val":"snd"}
        }
        cls.range = {
            "identifier":"range",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.maxTargets = {
            "identifier":"maxTargetCount",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyTargetWeap(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                hasWeapEffect=self.hasWeapEffect,
                origin=self.origin,
                impactOffsetType=self.impactOffsetType,
                effectHitsHull=self.effectHitsHull,
                effectHitsShield=self.effectHitsShield,
                weaponEffect=self.weaponEffect,
                buff=self.buff,
                targetFilter=self.targetFilter,
                range=self.range,
                maxTargets=self.maxTargets,
                effectInfo=self.effectInfo,
                travelSpeed=self.travelSpeed,
                staggerDelay=self.staggerDelay
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionApplyTargetWeapNumTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionApplyTargetWeapNum:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"ApplyBuffToLocalOrbitBodyWithTravel\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tbuffType \"bufftest\"\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Frigate\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\ttravelSpeed 100.000000\n"
            +"\thasWeaponEffects TRUE\n"
            +"\tweaponEffectOriginType \"Targetter\"\n"
            +"\tweaponEffectAttachInfo\n"
            +"\t\tattachType \"Center\"\n"
            +"\tweaponEffectImpactOffsetType \"RandomMesh\"\n"
            +"\tcanWeaponEffectHitHull TRUE\n"
            +"\tcanWeaponEffectHitShields TRUE\n"
            +"\tweaponEffectsDef\n"
            +"\t\tweaponType \"Projectile\"\n"
            +"\t\tburstCount 1\n"
            +"\t\tburstDelay 0.000000\n"
            +"\t\tfireDelay 0.000000\n"
            +"\t\tmuzzleEffectName \"effmuzzle\"\n"
            +"\t\tmuzzleSoundMinRespawnTime 0.100000\n"
            +"\t\tmuzzleSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndmuzzle\"\n"
            +"\t\thitEffectName \"effhit\"\n"
            +"\t\thitHullEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndhullhit\"\n"
            +"\t\thitShieldsEffectSounds\n"
            +"\t\t\tsoundCount 1\n"
            +"\t\t\tsound \"sndshieldhit\"\n"
            +"\t\tprojectileTravelEffectName \"traveleff\"\n"
        )
        cls.actionType["val"] = "ApplyBuffToLocalOrbitBodyWithTravel"
        cls.buff = {"identifier":"buffType","val":"bufftest"}
        cls.vals = [
            {"identifier":"travelSpeed","val":100.0}
        ]
        cls.hasWeapEffect = {"identifier":"hasWeaponEffects","val":True}
        cls.origin = {
            "originType":{"identifier":"weaponEffectOriginType",
                "val":"Targetter"},
            "effectAttach":{
                "identifier":"weaponEffectAttachInfo",
                "attachType":{"identifier":"attachType","val":"Center"}
            }
        }
        cls.impactOffsetType = {"identifier":"weaponEffectImpactOffsetType",
            "val":"RandomMesh"}
        cls.effectHitsHull = {"identifier":"canWeaponEffectHitHull","val":True}
        cls.effectHitsShield = {"identifier":"canWeaponEffectHitShields",
            "val":True}
        cls.weaponEffect = {
            "identifier":"weaponEffectsDef",
            "effectType":{"identifier":"weaponType","val":"Projectile"},
            "burstCount":{"identifier":"burstCount","val":1},
            "burstDelay":{"identifier":"burstDelay","val":0.0},
            "fireDelay":{"identifier":"fireDelay","val":0.0},
            "muzzleEff":{"identifier":"muzzleEffectName","val":"effmuzzle"},
            "sndMuzzleRespawn":{"identifier":"muzzleSoundMinRespawnTime",
                "val":0.1},
            "sndMuzzle":{
                "identifier":"muzzleSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndmuzzle"}
                ]
            },
            "hitEff":{"identifier":"hitEffectName","val":"effhit"},
            "sndHitHull":{
                "identifier":"hitHullEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndhullhit"}
                ]
            },
            "sndHitShields":{
                "identifier":"hitShieldsEffectSounds",
                "counter":{"identifier":"soundCount","val":1},
                "elements":[
                    {"identifier":"sound","val":"sndshieldhit"}
                ]
            },
            "travelEff":{"identifier":"projectileTravelEffectName",
                "val":"traveleff"}
        }
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Frigate"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionApplyTargetWeapNum(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                hasWeapEffect=self.hasWeapEffect,
                origin=self.origin,
                impactOffsetType=self.impactOffsetType,
                effectHitsHull=self.effectHitsHull,
                effectHitsShield=self.effectHitsShield,
                weaponEffect=self.weaponEffect,
                buff=self.buff,
                targetFilter=self.targetFilter,
                vals=self.vals
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionRuinPlanetTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionRuinPlanet:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"RuinPlanet\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\ttargetFilter\n"
            +"\t\tnumOwnerships 1\n"
            +"\t\townership \"Enemy\"\n"
            +"\t\tnumObjects 1\n"
            +"\t\tobject \"Planet\"\n"
            +"\t\tnumSpaces 1\n"
            +"\t\tspace \"Normal\"\n"
            +"\t\tnumConstraints 0\n"
            +"\trange\n"
            +"\t\tLevel:0 0.000000\n"
            +"\t\tLevel:1 1.000000\n"
            +"\t\tLevel:2 2.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tafterRuinBuffType \"bufftest\"\n"
        )
        cls.actionType["val"] = "RuinPlanet"
        cls.buff = {"identifier":"afterRuinBuffType","val":"bufftest"}
        cls.vals = [
            {
                "identifier":"range",
                "levels":[
                    {"identifier":"Level:0","val":0.0},
                    {"identifier":"Level:1","val":1.0},
                    {"identifier":"Level:2","val":2.0},
                    {"identifier":"Level:3","val":3.0}
                ]
            }
        ]
        cls.targetFilter = {
            "identifier":"targetFilter",
            "ownerships":{
                "counter":{"identifier":"numOwnerships","val":1},
                "elements":[
                    {"identifier":"ownership","val":"Enemy"}
                ]
            },
            "objects":{
                "counter":{"identifier":"numObjects","val":1},
                "elements":[
                    {"identifier":"object","val":"Planet"}
                ]
            },
            "spaces":{
                "counter":{"identifier":"numSpaces","val":1},
                "elements":[
                    {"identifier":"space","val":"Normal"}
                ]
            },
            "constraints":{
                "counter":{"identifier":"numConstraints","val":0},
                "elements":[]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionRuinPlanet(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                buff=self.buff,
                targetFilter=self.targetFilter,
                vals=self.vals
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionSpawnShipsTests(InstantActionFixture,unit.TestCase):
    desc = "Tests ActionSpawnShips:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "instantAction\n"
            +"\tbuffInstantActionType \"SpawnShipsAtPlanet\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
            +"\tspawnShipsLevelCount 1\n"
            +"\tspawnShips\n"
            +"\t\tminFleetPoints 4\n"
            +"\t\tmaxFleetPoints 8\n"
            +"\t\trequiredShipCount 1\n"
            +"\t\trequiredShip\n"
            +"\t\t\ttype \"testtype\"\n"
            +"\t\t\tminCount 2\n"
            +"\t\t\tmaxCount 3\n"
            +"\t\trandomShipCount 1\n"
            +"\t\trandomShip\n"
            +"\t\t\ttype \"testtype\"\n"
            +"\t\t\tweight 2.300000\n"
            +"\tspawnShipsArrivalDelayTime 2.000000\n"
            +"\tspawnShipsHyperspaceSpawnType \"FleetBeacon\"\n"
        )
        cls.actionType["val"] = "SpawnShipsAtPlanet"
        cls.vals = [
            {"identifier":"spawnShipsArrivalDelayTime","val":2.0}
        ]
        cls.ships = {
            "counter":{"identifier":"spawnShipsLevelCount","val":1},
            "elements":[
                {
                    "identifier":"spawnShips",
                    "ships":{
                        "requiredShips":{
                            "counter":{"identifier":"requiredShipCount",
                                "val":1},
                            "elements":[
                                {
                                    "identifier":"requiredShip",
                                    "shipType":{"identifier":"type",
                                        "val":"testtype"},
                                    "shipsMin":{"identifier":"minCount",
                                        "val":2},
                                    "shipsMax":{"identifier":"maxCount","val":3}
                                }
                            ]
                        },
                        "randomShips":{
                            "counter":{"identifier":"randomShipCount","val":1},
                            "elements":[
                                {
                                    "identifier":"randomShip",
                                    "shipType":{"identifier":"type",
                                        "val":"testtype"},
                                    "weight":{"identifier":"weight","val":2.3}
                                }
                            ]
                        }

                    },
                    "fleetPointsMin":{"identifier":"minFleetPoints","val":4},
                    "fleetPointsMax":{"identifier":"maxFleetPoints","val":8}
                }
            ]
        }
        cls.hyperSpawnType = {"identifier":"spawnShipsHyperspaceSpawnType",
            "val":"FleetBeacon"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionSpawnShips(
                identifier=self.identifier,
                actionType=self.actionType,
                trigger=self.trigger,
                vals=self.vals,
                ships=self.ships,
                hyperSpawnType=self.hyperSpawnType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffInstantAction.parseString(self.parseString
        )[0]
        res = abilities.InstantAction.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribspawnShipsArrivalDelayTime(self):
        """ Test whether spawnShipsArrivalDelayTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.vals[0])
        self.assertEqual(self.inst.spawnShipsArrivalDelayTime,exp)

    def testCreationAttribShips(self):
        """ Test whether ships attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=consts.ShipSpawn,**self.ships)
        self.assertEqual(self.inst.ships,exp)

    def testCreationAttribHyperSpawnType(self):
        """ Test whether hyperSpawnType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.SPAWN_SHIPS_TYPE,
                **self.hyperSpawnType)
        self.assertEqual(self.inst.hyperSpawnType,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################## Tests Periodic Actions #########################"""

class ActionCountTests(unit.TestCase):
    desc = "Tests ActionCount:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "actionCountType \"Infinite\"\n"
        )
        cls.countType = {"identifier":"actionCountType","val":"Infinite"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCount(
                countType=self.countType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionCountInfinite.parseString(
                self.parseString)[0]
        res = abilities.ActionCount.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid count type
        """
        invalid = {"identifier":"actionCountType","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.ActionCount.factory(countType=invalid)

    def testCreationAttribCountType(self):
        """ Test whether countType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["Infinite","Finite"],**self.countType)
        self.assertEqual(self.inst.countType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class ActionCountFiniteTests(unit.TestCase):
    desc = "Tests ActionCountFinite:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "actionCountType \"Finite\"\n"
            +"actionCount\n"
            +"\tLevel:0 3.000000\n"
            +"\tLevel:1 3.000000\n"
            +"\tLevel:2 3.000000\n"
            +"\tLevel:3 3.000000\n"
        )
        cls.countType = {"identifier":"actionCountType","val":"Finite"}
        cls.count = {
            "identifier":"actionCount",
            "levels":[
                {"identifier":"Level:0","val":3.0},
                {"identifier":"Level:1","val":3.0},
                {"identifier":"Level:2","val":3.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.ActionCountFinite(
                countType=self.countType,
                count=self.count
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_actionCountFinite.parseString(
                self.parseString)[0]
        res = abilities.ActionCount.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCount(self):
        """ Test whether count attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.count)
        self.assertEqual(self.inst.count,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PeriodicActionTests(unit.TestCase):
    desc = "Tests PeriodicAction"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "periodicAction\n"
            +"\tactionCountType \"Infinite\"\n"
            +"\tactionIntervalTime\n"
            +"\t\tLevel:0 3.000000\n"
            +"\t\tLevel:1 3.000000\n"
            +"\t\tLevel:2 3.000000\n"
            +"\t\tLevel:3 3.000000\n"
            +"\tbuffInstantActionType \"Explore\"\n"
            +"\tinstantActionTriggerType \"OnDelay\"\n"
            +"\tdelayTime 100.000000\n"
        )
        cls.identifier = "periodicAction"
        cls.count = {
            "countType":{"identifier":"actionCountType","val":"Infinite"}
        }
        cls.interval = {
            "identifier":"actionIntervalTime",
            "levels":[
                {"identifier":"Level:0","val":3.0},
                {"identifier":"Level:1","val":3.0},
                {"identifier":"Level:2","val":3.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.instantAction = {
            "actionType":{"identifier":"buffInstantActionType","val":"Explore"},
            "trigger":{
                "triggerType":{"identifier":"instantActionTriggerType",
                    "val":"OnDelay"},
                "delay":{"identifier":"delayTime","val":100.0}
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.PeriodicAction(
                identifier=self.identifier,
                interval=self.interval,
                count=self.count,
                instantAction=self.instantAction
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_buffPeriodicAction.parseString(self.parseString
        )[0]
        res = abilities.PeriodicAction(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCount(self):
        """ Test whether count attribute is created correctly
        """
        exp = abilities.ActionCount(**self.count)
        self.assertEqual(self.inst.count,exp)

    def testCreationAttribInterval(self):
        """ Test whether interval attribute is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.interval)
        self.assertEqual(self.inst.interval,exp)

    def testCreationAttribInstantAction(self):
        """ Test whether instantAction attribute is created correctly
        """
        exp = abilities.InstantAction(identifier="%remove%",
                **self.instantAction)
        self.assertEqual(self.inst.instantAction,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckAction(self):
        """ Test whether problems in instantAction attrib are returned
        """
        with mock.patch.object(self.inst.instantAction,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Buffs ##############################"""

class BuffTests(unit.TestCase):
    desc = "Tests Buff:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "Buff.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Buff"}
        cls.onReapply = {"identifier":"onReapplyDuplicateType",
            "val":"PrioritizeOldBuffs"}
        cls.stackingLimitType = {"identifier":"buffStackingLimitType",
            "val":"ForAllPlayers"}
        cls.stackingLimit = {"identifier":"stackingLimit","val":1}
        cls.allowFirstSpawnerStack = {"identifier":"allowFirstSpawnerToStack",
            "val":False}
        cls.exclusivityForAI = {"identifier":"buffExclusivityForAIType",
            "val":"ExclusiveForAllPlayers"}
        cls.interruptable = {"identifier":"isInterruptable","val":False}
        cls.channeling = {"identifier":"isChannelling","val":False}
        cls.instantActions = {
            "counter":{"identifier":"numInstantActions","val":1},
            "elements":[
                {
                    "identifier":"instantAction",
                    "actionType":{"identifier":"buffInstantActionType",
                        "val":"RecordDamage"},
                    "trigger":{
                        "triggerType":{"identifier":"instantActionTriggerType",
                            "val":"OnDamageTaken"}
                    }
                }
            ]
        }
        cls.periodicActions = {
            "counter":{"identifier":"numPeriodicActions","val":1},
            "elements":[
                {
                    "identifier":"periodicAction",
                    "count":{
                        "countType":{"identifier":"actionCountType",
                            "val":"Infinite"}
                    },
                    "interval":{
                        "identifier":"actionIntervalTime",
                        "levels":[
                            {"identifier":"Level:0","val":0.1},
                            {"identifier":"Level:1","val":0.1},
                            {"identifier":"Level:2","val":0.1},
                            {"identifier":"Level:3","val":0.1}
                        ]
                    },
                    "instantAction":{
                        "actionType":{"identifier":"buffInstantActionType",
                            "val":"RecordDamage"},
                        "trigger":{
                            "triggerType":{
                                "identifier":"instantActionTriggerType",
                                "val":"OnDamageTaken"}
                        }
                    }
                }
            ]
        }
        cls.actionsOverTime = {
            "counter":{"identifier":"numOverTimeActions","val":0},
            "elements":[]
        }
        cls.entityMods = {
            "counter":{"identifier":"numEntityModifiers","val":1},
            "elements":[
                {
                    "identifier":"entityModifier",
                    "modType":{"identifier":"buffEntityModifierType",
                        "val":"PropagateWeaponDamagePerc"},
                    "value":{
                        "identifier":"value",
                        "levels":[
                            {"identifier":"Level:0","val":0.5},
                            {"identifier":"Level:1","val":0.5},
                            {"identifier":"Level:2","val":0.5},
                            {"identifier":"Level:3","val":0.5}
                        ]
                    }
                }
            ]
        }
        cls.entityModsBool = {
            "counter":{"identifier":"numEntityBoolModifiers","val":0},
            "elements":[]
        }
        cls.finishConditions = {
            "counter":{"identifier":"numFinishConditions","val":1},
            "elements":[
                {
                    "identifier":"finishCondition",
                    "finishType":{"identifier":"finishConditionType",
                        "val":"EnterHyperspace"}
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.Buff(
                filepath=self.filepath,
                entityType=self.entityType,
                onReapply=self.onReapply,
                stackingLimitType=self.stackingLimitType,
                stackingLimit=self.stackingLimit,
                allowFirstSpawnerStack=self.allowFirstSpawnerStack,
                exclusivityForAI=self.exclusivityForAI,
                interruptable=self.interruptable,
                channeling=self.channeling,
                instantActions=self.instantActions,
                periodicActions=self.periodicActions,
                actionsOverTime=self.actionsOverTime,
                entityMods=self.entityMods,
                entityModsBool=self.entityModsBool,
                finishConditions=self.finishConditions
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = abilities.Buff.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problems are returned correctly
        """
        res = abilities.Buff.createInstance("BuffMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribOnReapply(self):
        """ Test whether onReapply attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.ON_REAPPLY_DUPLICATE,
                **self.onReapply)
        self.assertEqual(self.inst.onReapply,exp)

    def testCreationAttribStackingLimitType(self):
        """ Test whether stackingLimitType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.STACKING_LIMITS,
                **self.stackingLimitType)
        self.assertEqual(self.inst.stackingLimitType,exp)

    def testCreationAttribStackingLimit(self):
        """ Test whether stackingLimit attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.stackingLimit)
        self.assertEqual(self.inst.stackingLimit,exp)

    def testCreationAttribAllowFirstSpawnerStack(self):
        """ Test whether allowFirstSpawnerStack attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.allowFirstSpawnerStack)
        self.assertEqual(self.inst.allowFirstSpawnerStack,exp)

    def testCreationAttribExclusivityForAI(self):
        """ Test whether exclusivityForAI attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.BUFF_EXCLUSIVITY,
                **self.exclusivityForAI)
        self.assertEqual(self.inst.exclusivityForAI,exp)

    def testCreationAttribInterruptable(self):
        """ Test whether interruptable attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.interruptable)
        self.assertEqual(self.inst.interruptable,exp)

    def testCreationAttribChanneling(self):
        """ Test whether channeling attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.channeling)
        self.assertEqual(self.inst.channeling,exp)

    def testCreationAttribInstantActions(self):
        """ Test whether instantActions attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=abilities.InstantAction,
                factory=True,
                **self.instantActions)
        self.assertEqual(self.inst.instantActions,exp)

    def testCreationAttribPeriodicActions(self):
        """ Test whether periodicActions attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=abilities.PeriodicAction,
                **self.periodicActions)
        self.assertEqual(self.inst.periodicActions,exp)

    def testCreationAttribActionsOverTime(self):
        """ Test whether actionsOverTime attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=abilities.ActionOverTime,
                factory=True,
                **self.actionsOverTime)
        self.assertEqual(self.inst.actionsOverTime,exp)

    def testCreationAttribEntityMods(self):
        """ Test whether entityMods attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=abilities.BuffEntityModifier,
                **self.entityMods)
        self.assertEqual(self.inst.entityMods,exp)

    def testCreationAttribEntityModsBool(self):
        """ Test whether entityModsBool attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":abilities.BUFF_ENTITY_MODIFIERS_BOOL},
                **self.entityModsBool)
        self.assertEqual(self.inst.entityModsBool,exp)

    def testCreationAttribFinishConditions(self):
        """ Test whether finishConditions attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=abilities.BuffFinishCond,
                factory=True,
                **self.finishConditions)
        self.assertEqual(self.inst.finishConditions,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckInstantActions(self):
        """ Test whether check returns problems from instantActions attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.instantActions,"check",
                autospec=True,spec_set=True,return_value=[mockProb]
        ) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckPeriodicActions(self):
        """ Test whether check returns problems from periodicActions attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.periodicActions,"check",
                autospec=True,spec_set=True,return_value=[mockProb]
        ) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckActionsOverTime(self):
        """ Test whether check returns problems from actionsOverTime attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.actionsOverTime,"check",
                autospec=True,spec_set=True,return_value=[mockProb]
        ) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckFinishConditions(self):
        """ Test whether check returns problems from finishConditions attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.finishConditions,"check",
                autospec=True,spec_set=True,return_value=[mockProb]
        ) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testOutputBuffchain(self):
        """ Test whether outputBuffchain returns only the filename
        """
        exp = self.filepath+":\n\tNo Buffs Referenced\n"
        res = self.inst.outputBuffchain(self.mmod,0)
        self.assertEqual(res,exp)

"""############################ Tests Buffchain ############################"""

class BuffchainTestsBuff(unit.TestCase):
    desc = "Tests Buff for outputBuffchain:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "BuffBuffchain.entity"
        cls.inst = abilities.Buff.createInstance(cls.filepath)

    def setUp(self):
        self.mmod = tco.genMockMod("./")
        self.mockApplyBuff = mock.create_autospec(abilities.Buff,spec_set=True)
        self.mockApplyBuff.outputBuffchain.return_value = (
                "\tApplyBuff.entity:\n"
                "\t\tNo Buffs Referenced\n")
        self.mockColPlanet = mock.create_autospec(abilities.Buff,spec_set=True)
        self.mockColPlanet.outputBuffchain.return_value = (
                "\tColPlanet.entity:\n"
                "\t\tNo Buffs Referenced\n")
        self.mockCreateFrg = mock.create_autospec(abilities.Buff,spec_set=True)
        self.mockCreateFrg.outputBuffchain.return_value = (
                "\tCreateFrg.entity:\n"
                "\t\tNo Buffs Referenced\n")
        self.mmod.entities.getEntity.side_effect = [self.mockApplyBuff,
            self.mockColPlanet,self.mockCreateFrg]
        self.maxDiff = None

    def testOutput(self):
        """ Test whether outputBuffchain returns correct output
        """
        exp = (self.filepath+":\n"
               +"\tApplyBuff.entity:\n"
               +"\t\tNo Buffs Referenced\n"
               +"\tColPlanet.entity:\n"
               +"\t\tNo Buffs Referenced\n"
               +"\tCreateFrg.entity:\n"
               +"\t\tNo Buffs Referenced\n"
        )
        res = self.inst.outputBuffchain(self.mmod,0)
        self.assertEqual(res,exp)

class BuffchainTestsAbility(unit.TestCase):
    desc = "Tests Ability for outputBuffchain:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "AbilityBuffchain.entity"
        cls.inst = abilities.Ability.createInstance(cls.filepath)

    def setUp(self):
        self.mmod = tco.genMockMod("./")
        self.mockApplyBuff = mock.create_autospec(abilities.Buff,spec_set=True)
        self.mockApplyBuff.outputBuffchain.return_value = (
                "\tApplyBuff.entity:\n"
                "\t\tNo Buffs Referenced\n")
        self.mmod.entities.getEntity.return_value = self.mockApplyBuff
        self.maxDiff = None

    def testOutput(self):
        """ Test whether outputBuffchain returns correct output
        """
        exp = (self.filepath+":\n"
               +"\tApplyBuff.entity:\n"
               +"\t\tNo Buffs Referenced\n"
        )
        res = self.inst.outputBuffchain(self.mmod,0)
        self.assertEqual(res,exp)

"""########################### Tests LevelSource ###########################"""

class LevelSourceTests(unit.TestCase):
    desc = "Tests LevelSource:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "levelSourceType \"FixedLevel0\"\n"
        )
        cls.sourceType = {"identifier":"levelSourceType","val":"FixedLevel0"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.LevelSource(
                sourceType=self.sourceType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_lvlSourceSimple.parseString(self.parseString)[0]
        res = abilities.LevelSource.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid sourceType
        """
        invalid = {"identifier":"levelSourceType","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.LevelSource.factory(sourceType=invalid)

    def testCreationAttribSourceType(self):
        """ Test whether sourceType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.LVL_SOURCE,**self.sourceType)
        self.assertEqual(self.inst.sourceType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LevelSourceIntrinsicTests(unit.TestCase):
    desc = "Tests LevelSourceIntrinsic:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "levelSourceType \"Intrinsic\"\n"
            +"minExperienceLevelRequired\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
        )
        cls.sourceType = {"identifier":"levelSourceType","val":"Intrinsic"}
        cls.minExpForLvl = {
            "identifier":"minExperienceLevelRequired",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.LevelSourceIntrinsic(
                sourceType=self.sourceType,
                minExpForLvl=self.minExpForLvl
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_lvlSource.parseString(self.parseString)[0]
        res = abilities.LevelSource.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMinExpForLvl(self):
        """ Test whether minExpForLvl attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.minExpForLvl)
        self.assertEqual(self.inst.minExpForLvl,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LevelSourceResearchTests(unit.TestCase):
    desc = "Tests LevelSourceResearch:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "levelSourceType \"ResearchStartsAt0\"\n"
            +"improveSourceResearchSubject \"testsubject\"\n"
        )
        cls.sourceType = {"identifier":"levelSourceType",
            "val":"ResearchStartsAt0"}
        cls.subject = {"identifier":"improveSourceResearchSubject",
            "val":"testsubject"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.LevelSourceResearch(
                sourceType=self.sourceType,
                subject=self.subject
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_lvlSource.parseString(self.parseString)[0]
        res = abilities.LevelSource.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSubject(self):
        """ Test whether subject attrib is created correctly
        """
        exp = coe.EntityRef(types=["ResearchSubject"],**self.subject)
        self.assertEqual(self.inst.subject,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LevelSourceResearchBaseTests(unit.TestCase):
    desc = "Tests LevelSourceResearchBase:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "levelSourceType \"ResearchWithBase\"\n"
            +"baseSourceResearchSubject \"basesubject\"\n"
            +"improveSourceResearchSubject \"testsubject\"\n"
        )
        cls.sourceType = {"identifier":"levelSourceType",
            "val":"ResearchWithBase"}
        cls.base = {"identifier":"baseSourceResearchSubject",
            "val":"basesubject"}
        cls.subject = {"identifier":"improveSourceResearchSubject",
            "val":"testsubject"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.LevelSourceResearchBase(
                sourceType=self.sourceType,
                subject=self.subject,
                base=self.base
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_lvlSource.parseString(self.parseString)[0]
        res = abilities.LevelSource.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBase(self):
        """ Test whether base attrib is created correctly
        """
        exp = coe.EntityRef(types=["ResearchSubject"],**self.base)
        self.assertEqual(self.inst.base,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class LevelSourceUpgradeTests(unit.TestCase):
    desc = "Tests LevelSourceUpgrade:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "levelSourceType \"StarBaseUpgradeLevel\"\n"
            +"sourcePropertyType \"AbilityALevel\"\n"
        )
        cls.sourceType = {"identifier":"levelSourceType",
            "val":"StarBaseUpgradeLevel"}
        cls.sourceProperty = {"identifier":"sourcePropertyType",
            "val":"AbilityALevel"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.LevelSourceUpgrade(
                sourceType=self.sourceType,
                sourceProperty=self.sourceProperty
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_lvlSource.parseString(self.parseString)[0]
        res = abilities.LevelSource.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribSourceProperty(self):
        """ Test whether sourceProperty attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.SOURCE_PROPERTIES,
                **self.sourceProperty)
        self.assertEqual(self.inst.sourceProperty,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################ Tests AIUseTime ############################"""

class AIUseTimeTests(unit.TestCase):
    desc = "Tests AIUseTime:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTime \"Anytime\"\n"
        )
        cls.aiUseType = {"identifier":"aiUseTime","val":"Anytime"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTime(
                aiUseType=self.aiUseType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTime.parseString(self.parseString)[0]
        res = abilities.AIUseTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid sourceType
        """
        invalid = {"identifier":"aiUseTime","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.AIUseTime.factory(aiUseType=invalid)

    def testCreationAttribAIUseType(self):
        """ Test whether aiUseType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.AI_USE_TIME,**self.aiUseType)
        self.assertEqual(self.inst.aiUseType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AIUseTimeValTests(unit.TestCase):
    desc = "Tests AIUseTimeVal:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTime \"OnlyWhenDebrisWithinRange\"\n"
            +"onlyAutoCastWhenDebrisWithinRange 10.000000\n"
        )
        cls.aiUseType = {"identifier":"aiUseTime",
            "val":"OnlyWhenDebrisWithinRange"}
        cls.val = {"identifier":"onlyAutoCastWhenDebrisWithinRange",
            "val":10.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTimeVal(
                aiUseType=self.aiUseType,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTime.parseString(self.parseString)[0]
        res = abilities.AIUseTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribVal(self):
        """ Test whether aiUseType attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.val)
        self.assertEqual(self.inst.val,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AIUseTimeTargetsNoBuff(unit.TestCase):
    desc = "Tests AIUseTimeTargetsNoBuff:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTime \"OnlyWhenManyTargetsInCombatDoNotHaveBuff\"\n"
            +"onlyAutoCastWhenTargetCountExceedsAmount\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"buffType \"testbuff\"\n"
        )
        cls.aiUseType = {"identifier":"aiUseTime",
            "val":"OnlyWhenManyTargetsInCombatDoNotHaveBuff"}
        cls.val = {
            "identifier":"onlyAutoCastWhenTargetCountExceedsAmount",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTimeTargetsNotBuffed(
                aiUseType=self.aiUseType,
                val=self.val,
                buff=self.buff
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTime.parseString(self.parseString)[0]
        res = abilities.AIUseTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribVal(self):
        """ Test whether val attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.val)
        self.assertEqual(self.inst.val,exp)

    def testCreationAttribBuff(self):
        """ Test whether buff attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        self.assertEqual(self.inst.buff,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AIUseTimeTargetsRangeBank(unit.TestCase):
    desc = "Tests AIUseTimeTargetsRangeBank:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTime \"OnlyWhenManyTargetsInRangeOfBank\"\n"
            +"onlyAutoCastWhenTargetCountExceedsAmount\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"onlyAutoCastWhenTargetsInBank \"FRONT\"\n"
        )
        cls.aiUseType = {"identifier":"aiUseTime",
            "val":"OnlyWhenManyTargetsInRangeOfBank"}
        cls.val = {
            "identifier":"onlyAutoCastWhenTargetCountExceedsAmount",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.bank = {"identifier":"onlyAutoCastWhenTargetsInBank",
            "val":"FRONT"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTimeTargetsRangeBank(
                aiUseType=self.aiUseType,
                val=self.val,
                bank=self.bank
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTime.parseString(self.parseString)[0]
        res = abilities.AIUseTime.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribVal(self):
        """ Test whether val attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.val)
        self.assertEqual(self.inst.val,exp)

    def testCreationAttribBank(self):
        """ Test whether bank attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=armed.WEAPON_BANK,**self.bank)
        self.assertEqual(self.inst.bank,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests AIUseTarget ###########################"""

class AIUseTarget(unit.TestCase):
    desc = "Test AIUseTarget:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTargetCondition \"HasNoWeapons\"\n"
        )
        cls.aiUseTargetType = {"identifier":"aiUseTargetCondition",
            "val":"HasNoWeapons"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTarget(
                aiUseTargetType=self.aiUseTargetType
        )

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid aiUseTargetType
        """
        invalid = {"identifier":"aiUseTargetCondition",
            "val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.AIUseTarget.factory(aiUseTargetType=invalid)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTarget.parseString(self.parseString)[0]
        res = abilities.AIUseTarget.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAIUseTargetType(self):
        """ Test whether aiUseTargetType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.AI_USE_TARGET,
                **self.aiUseTargetType)
        self.assertEqual(self.inst.aiUseTargetType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AIUseTargetVal(unit.TestCase):
    desc = "Test AIUseTargetVal:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTargetCondition \"AntimatterExceedsAmount\"\n"
            +"onlyAutoCastWhenTargetAntimatterExceedsAmount\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
        )
        cls.aiUseTargetType = {"identifier":"aiUseTargetCondition",
            "val":"AntimatterExceedsAmount"}
        cls.val = {
            "identifier":"onlyAutoCastWhenTargetAntimatterExceedsAmount",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTargetVal(
                aiUseTargetType=self.aiUseTargetType,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTarget.parseString(self.parseString)[0]
        res = abilities.AIUseTarget.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribVal(self):
        """ Test whether val attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.val)
        self.assertEqual(self.inst.val,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AIUseTargetHasBuffTests(unit.TestCase):
    desc = "Test AIUseTargetHasBuff:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "aiUseTargetCondition \"HasBuff\"\n"
            +"buffType \"testbuff\"\n"
        )
        cls.aiUseTargetType = {"identifier":"aiUseTargetCondition",
            "val":"HasBuff"}
        cls.buff = {"identifier":"buffType","val":"testbuff"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AIUseTargetHasBuff(
                aiUseTargetType=self.aiUseTargetType,
                buff=self.buff
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_aiUseTarget.parseString(self.parseString)[0]
        res = abilities.AIUseTarget.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribBuff(self):
        """ Test whether buff attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        self.assertEqual(self.inst.buff,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests AbilityUseCost #########################"""

class AbilityUseCostTests(unit.TestCase):
    desc = "Test AbilityUseCost:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"Passive\"\n"
        )
        cls.costType = {"identifier":"useCostType","val":"Passive"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCost(
                costType=self.costType
        )

    def testFactoryInvalid(self):
        """ Test whether factory raises error with invalid aiUseTargetType
        """
        invalid = {"identifier":"useCostType","val":"Foo"}
        with self.assertRaises(RuntimeError):
            abilities.AbilityUseCost.factory(costType=invalid)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCostType(self):
        """ Test whether costType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.COST_TYPES,
                **self.costType)
        self.assertEqual(self.inst.costType,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AbilityUseCostActiveTests(unit.TestCase):
    desc = "Test AbilityUseCostActive:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"None\"\n"
            +"cooldownTime\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"orderAcknowledgementType \"ONCREATION\"\n"
        )
        cls.costType = {"identifier":"useCostType","val":"None"}
        cls.cooldown = {
            "identifier":"cooldownTime",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.orderAcknowledge = {"identifier":"orderAcknowledgementType",
            "val":"ONCREATION"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCostActive(
                costType=self.costType,
                cooldown=self.cooldown,
                orderAcknowledge=self.orderAcknowledge
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribOrderAcknowledge(self):
        """ Test whether orderAcknowledge attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=abilities.ORDER_ACKNOWLEDGE,
                **self.orderAcknowledge)
        self.assertEqual(self.inst.orderAcknowledge,exp)

    def testCreationAttribCooldown(self):
        """ Test whether cooldown attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.cooldown)
        self.assertEqual(self.inst.cooldown,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AbilityUseCostAntimatterTests(unit.TestCase):
    desc = "Test AbilityUseCostAntimatter:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"AntiMatter\"\n"
            +"antiMatterCost\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"cooldownTime\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"orderAcknowledgementType \"ONCREATION\"\n"
        )
        cls.costType = {"identifier":"useCostType","val":"AntiMatter"}
        cls.cooldown = {
            "identifier":"cooldownTime",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.antimatterCost = {
            "identifier":"antiMatterCost",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.orderAcknowledge = {"identifier":"orderAcknowledgementType",
            "val":"ONCREATION"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCostAntimatter(
                costType=self.costType,
                cooldown=self.cooldown,
                orderAcknowledge=self.orderAcknowledge,
                antimatterCost=self.antimatterCost
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAntimatterCost(self):
        """ Test whether antimatterCost attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.antimatterCost)
        self.assertEqual(self.inst.antimatterCost,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AbilityUseCostAMHullTests(unit.TestCase):
    desc = "Test AbilityUseCostAMHull:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"HullAndAntimatter\"\n"
            +"hullCost\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"antiMatterCost\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"cooldownTime\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"orderAcknowledgementType \"ONCREATION\"\n"
        )
        cls.costType = {"identifier":"useCostType","val":"HullAndAntimatter"}
        cls.hullCost = {
            "identifier":"hullCost",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.cooldown = {
            "identifier":"cooldownTime",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.antimatterCost = {
            "identifier":"antiMatterCost",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.orderAcknowledge = {"identifier":"orderAcknowledgementType",
            "val":"ONCREATION"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCostAMHull(
                costType=self.costType,
                cooldown=self.cooldown,
                orderAcknowledge=self.orderAcknowledge,
                antimatterCost=self.antimatterCost,
                hullCost=self.hullCost
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribHullCost(self):
        """ Test whether hullCost attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.hullCost)
        self.assertEqual(self.inst.hullCost,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AbilityUseCostResourcesTests(unit.TestCase):
    desc = "Test AbilityUseCostResources:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"Resources\"\n"
            +"resourceCost\n"
            +"\tcredits 100\n"
            +"\tmetal 200\n"
            +"\tcrystal 300\n"
            +"cooldownTime\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"orderAcknowledgementType \"ONCREATION\"\n"
        )
        cls.costType = {"identifier":"useCostType","val":"Resources"}
        cls.resources = {
            "identifier":"resourceCost",
            "credits":{"identifier":"credits","val":100},
            "metal":{"identifier":"metal","val":200},
            "crystal":{"identifier":"crystal","val":300}
        }
        cls.cooldown = {
            "identifier":"cooldownTime",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.orderAcknowledge = {"identifier":"orderAcknowledgementType",
            "val":"ONCREATION"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCostResources(
                costType=self.costType,
                resources=self.resources,
                cooldown=self.cooldown,
                orderAcknowledge=self.orderAcknowledge
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribResources(self):
        """ Test whether resources attrib is created correctly
        """
        exp = coe.Cost(**self.resources)
        self.assertEqual(self.inst.resources,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AbilityUseCostResourcesSupplyTests(unit.TestCase):
    desc = "Test AbilityUseCostResourcesSupply:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"ResourcesAndMustHaveShipSlots\"\n"
            +"resourceCost\n"
            +"\tcredits 100\n"
            +"\tmetal 200\n"
            +"\tcrystal 300\n"
            +"requiredShipSlots\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"cooldownTime\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"orderAcknowledgementType \"ONCREATION\"\n"
        )
        cls.costType = {"identifier":"useCostType",
            "val":"ResourcesAndMustHaveShipSlots"}
        cls.resources = {
            "identifier":"resourceCost",
            "credits":{"identifier":"credits","val":100},
            "metal":{"identifier":"metal","val":200},
            "crystal":{"identifier":"crystal","val":300}
        }
        cls.cooldown = {
            "identifier":"cooldownTime",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.reqSupply = {
            "identifier":"requiredShipSlots",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.orderAcknowledge = {"identifier":"orderAcknowledgementType",
            "val":"ONCREATION"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCostResourcesSupply(
                costType=self.costType,
                reqSupply=self.reqSupply,
                resources=self.resources,
                cooldown=self.cooldown,
                orderAcknowledge=self.orderAcknowledge
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribReqSupply(self):
        """ Test whether reqSupply attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.reqSupply)
        self.assertEqual(self.inst.reqSupply,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class AbilityUseCostResourcesCapitalTests(unit.TestCase):
    desc = "Test AbilityUseCostResourcesCapital:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useCostType \"ResourcesAndMustHaveCapitalCrew\"\n"
            +"resourceCost\n"
            +"\tcredits 100\n"
            +"\tmetal 200\n"
            +"\tcrystal 300\n"
            +"requiredShipSlots\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"requiredCapitalShipSlots\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"cooldownTime\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
            +"orderAcknowledgementType \"ONCREATION\"\n"
        )
        cls.costType = {"identifier":"useCostType",
            "val":"ResourcesAndMustHaveCapitalCrew"}
        cls.resources = {
            "identifier":"resourceCost",
            "credits":{"identifier":"credits","val":100},
            "metal":{"identifier":"metal","val":200},
            "crystal":{"identifier":"crystal","val":300}
        }
        cls.cooldown = {
            "identifier":"cooldownTime",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.reqSupply = {
            "identifier":"requiredShipSlots",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.reqCapCrews = {
            "identifier":"requiredCapitalShipSlots",
            "levels":[
                {"identifier":"Level:0","val":0.0},
                {"identifier":"Level:1","val":1.0},
                {"identifier":"Level:2","val":2.0},
                {"identifier":"Level:3","val":3.0}
            ]
        }
        cls.orderAcknowledge = {"identifier":"orderAcknowledgementType",
            "val":"ONCREATION"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = abilities.AbilityUseCostResourcesCapital(
                costType=self.costType,
                reqSupply=self.reqSupply,
                resources=self.resources,
                reqCapCrews=self.reqCapCrews,
                cooldown=self.cooldown,
                orderAcknowledge=self.orderAcknowledge
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = abilities.g_useCost.parseString(self.parseString)[0]
        res = abilities.AbilityUseCost.factory(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribReqSupply(self):
        """ Test whether reqCapCrews attrib is created correctly
        """
        exp = co_attribs.AttribFixedLevelable(**self.reqCapCrews)
        self.assertEqual(self.inst.reqCapCrews,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests Ability #############################"""

class AbilityTests(UIVisibleFixtures,unit.TestCase):
    desc = "Tests Ability:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Ability.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Ability"}
        cls.instantAction = {
            "actionType":{"identifier":"buffInstantActionType",
                "val":"RecordDamage"},
            "trigger":{
                "triggerType":{"identifier":"instantActionTriggerType",
                    "val":"OnDamageTaken"}}
        }
        cls.faceTarget = {"identifier":"needsToFaceTarget","val":False}
        cls.canCollideWithTarget = {"identifier":"canCollideWithTarget",
            "val":True}
        cls.moveThruTarget = {"identifier":"moveThruTarget","val":False}
        cls.ultimate = {"identifier":"isUltimateAbility","val":False}
        cls.maxLevels = {"identifier":"maxNumLevels","val":1}
        cls.lvlSource = {
            "sourceType":{"identifier":"levelSourceType",
                "val":"FixedLevel0"}}
        cls.aiUseTime = {
            "aiUseType":{"identifier":"aiUseTime","val":"Invalid"}
        }
        cls.aiUseTarget = {
            "aiUseTargetType":{"identifier":"aiUseTargetCondition",
                "val":"Invalid"}
        }
        cls.canAutocast = {"identifier":"isAutoCastAvailable","val":False}
        cls.isAutocastDefault = {"identifier":"isAutoCastOnByDefault",
            "val":False}
        cls.pickRandPlanetExplore = {
            "identifier":"pickRandomPlanetToExploreForAutoCastTarget",  #
            "val":False}
        cls.ignoreCivShips = {
            "identifier":"ignoreNonCombatShipsForAutoCastTarget",
            "val":True}
        cls.onlyAutocastWhenDmgPerc = {
            "identifier":"onlyAutoCastWhenDamageTakenExceedsPerc",
            "val":0.0}
        cls.cost = {
            "costType":{"identifier":"useCostType","val":"Passive"}
        }
        cls.prereqs = {
            "identifier":"researchPrerequisites",
            "prereqs":{
                "counter":{"identifier":"NumResearchPrerequisites",
                    "val":0},
                "elements":[]
            },
            "reqFaction":{"identifier":"RequiredFactionNameID",
                "val":""},
            "reqCompSubjects":{
                "identifier":"RequiredCompletedResearchSubjects",
                "val":0}
        }

    def setUp(self):
        self.inst = abilities.Ability(
                filepath=self.filepath,
                entityType=self.entityType,
                instantAction=self.instantAction,
                faceTarget=self.faceTarget,
                canCollideWithTarget=self.canCollideWithTarget,
                moveThruTarget=self.moveThruTarget,
                ultimate=self.ultimate,
                maxLevels=self.maxLevels,
                lvlSource=self.lvlSource,
                aiUseTime=self.aiUseTime,
                aiUseTarget=self.aiUseTarget,
                canAutocast=self.canAutocast,
                isAutocastDefault=self.isAutocastDefault,
                pickRandPlanetExplore=self.pickRandPlanetExplore,
                ignoreCivShips=self.ignoreCivShips,
                onlyAutocastWhenDmgPerc=self.onlyAutocastWhenDmgPerc,
                cost=self.cost,
                prereqs=self.prereqs,
                nameString=self.nameString,
                descString=self.descString,
                iconHud=self.iconHud,
                iconHudSmall=self.iconHudSmall,
                iconInfocard=self.iconInfocard
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.brushes.checkBrush.return_value = []
        self.mmod.strings.checkString.return_value = []

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = abilities.Ability.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse errors are returned correctly
        """
        res = abilities.Ability.createInstance("AbilityMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribInstantAction(self):
        """ Test whether instantAction attrib is created correctly
        """
        exp = abilities.InstantAction(identifier="",**self.instantAction)
        self.assertEqual(self.inst.instantAction,exp)

    def testCreationAttribFaceTarget(self):
        """ Test whether faceTarget attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.faceTarget)
        self.assertEqual(self.inst.faceTarget,exp)

    def testCreationAttribCanCollideWithTarget(self):
        """ Test whether canCollideWithTarget attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.canCollideWithTarget)
        self.assertEqual(self.inst.canCollideWithTarget,exp)

    def testCreationAttribMoveThruTarget(self):
        """ Test whether moveThruTarget attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.moveThruTarget)
        self.assertEqual(self.inst.moveThruTarget,exp)

    def testCreationAttribUltimate(self):
        """ Test whether ultimate attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.ultimate)
        self.assertEqual(self.inst.ultimate,exp)

    def testCreationAttribMaxLevels(self):
        """ Test whether maxLevels attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxLevels)
        self.assertEqual(self.inst.maxLevels,exp)

    def testCreationAttribLvlSource(self):
        """ Test whether lvlSource attrib is created correctly
        """
        exp = abilities.LevelSource(**self.lvlSource)
        self.assertEqual(self.inst.lvlSource,exp)

    def testCreationAttribAIUseTime(self):
        """ Test whether aiUseTime attrib is created correctly
        """
        exp = abilities.AIUseTime(**self.aiUseTime)
        self.assertEqual(self.inst.aiUseTime,exp)

    def testCreationAttribAIUseTarget(self):
        """ Test whether aiUseTarget attrib is created correctly
        """
        exp = abilities.AIUseTarget(**self.aiUseTarget)
        self.assertEqual(self.inst.aiUseTarget,exp)

    def testCreationAttribCanAutocast(self):
        """ Test whether canAutocast attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.canAutocast)
        self.assertEqual(self.inst.canAutocast,exp)

    def testCreationAttribisAutocastDefault(self):
        """ Test whether isAutocastDefault attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.isAutocastDefault)
        self.assertEqual(self.inst.isAutocastDefault,exp)

    def testCreationAttribPickRandPlanetToExplore(self):
        """ Test whether pickRandPlanetExplore attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.pickRandPlanetExplore)
        self.assertEqual(self.inst.pickRandPlanetExplore,exp)

    def testCreationAttribIgnoreCivShips(self):
        """ Test whether ignoreCivShips attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.ignoreCivShips)
        self.assertEqual(self.inst.ignoreCivShips,exp)

    def testCreationAttribOnlyAutocastWhenDmgPerc(self):
        """ Test whether onlyAutocastWhenDmgPerc attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.onlyAutocastWhenDmgPerc)
        self.assertEqual(self.inst.onlyAutocastWhenDmgPerc,exp)

    def testCreationAttribCost(self):
        """ Test whether cost attrib is created correctly
        """
        exp = abilities.AbilityUseCost(**self.cost)
        self.assertEqual(self.inst.cost,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])
