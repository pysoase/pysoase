""" Tests for the pysoase.entities.orbitbodies module.
"""

import os
import unittest as unit
from unittest import mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.orbitbodies as orbitbo
import pysoase.entities.uncolonizables as uncol
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
from pysoase.tests.entities.test_ecommon import (UIVisibleFixtures,
    HasAbilitiesFixtures,MainViewSelectableFixture)
from pysoase.tests.entities.test_hitable import HasShieldVisualsFixtures

testdata = os.path.join("entities","orbitbodies")

"""########################### Tests PlanetBonus ###########################"""

class PlanetBonusTest(UIVisibleFixtures,unit.TestCase):
    desc = "Tests PlanetBonus:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        super().setUpClass()
        cls.filepath = "bonus.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"PlanetBonus"}
        cls.minUpgLvl = {"identifier":"minUpgradeLevelNeeded","val":0}
        cls.maxUpgLvl = {"identifier":"maxUpgradeLevelNeeded","val":1}
        cls.nameString = {"identifier":"nameStringID","val":"name"}
        cls.descString = {"identifier":"descStringID","val":"desc"}
        cls.floatBoni = [
            {"identifier":"floatBonus:AdditiveCultureSpreadPerc",
                "val":0.0},
            {"identifier":"floatBonus:AdditivePopulation",
                "val":0.0},
            {"identifier":"floatBonus:AdditivePlanetHealth",
                "val":0.0},
            {"identifier":"floatBonus:AdditiveSlotsCivilian",
                "val":0.0},
            {"identifier":"floatBonus:AdditiveSlotsTactical",
                "val":5.0},
            {"identifier":"floatBonus:AdditiveTaxIncome",
                "val":0.0},
            {"identifier":"floatBonus:AdditiveTradeIncomePerc",
                "val":-0.2},
            {"identifier":"floatBonus:BombingDamageAsDamageTargetAdjustment",
                "val":0.0},
            {"identifier":"floatBonus:MaxAllegiancePercBonus",
                "val":0.0},
            {"identifier":"floatBonus:MetalIncomePerc",
                "val":0.0},
            {"identifier":"floatBonus:CrystalIncomePerc",
                "val":0.0},
            {"identifier":"floatBonus:GravityWellRadiusPerc",
                "val":0.0},
            {"identifier":"floatBonus:ModuleBuildCostPerc",
                "val":0.0},
            {"identifier":"floatBonus:ModuleBuildRatePerc",
                "val":0.0},
            {"identifier":"floatBonus:PlanetUpgradeBuildCostPerc",
                "val":0.0},
            {"identifier":"floatBonus:PlanetUpgradeBuildRatePerc",
                "val":0.0},
            {"identifier":"floatBonus:PopulationGrowthPerc",
                "val":0.0},
            {"identifier":"floatBonus:ShipBuildCostAdjustment",
                "val":0.0},
            {"identifier":"floatBonus:WeaponDamageAtGravityWellMultiplier",
                "val":0.0}
        ]
        cls.intBoni = [
            {"identifier":"intBonus:MaxSpaceMines","val":0},
            {"identifier":"intBonus:MaxStarbases","val":0},
            {"identifier":"intBonus:ModuleConstructors","val":0},
            {"identifier":"intBonus:SpacePonies","val":0}
        ]
        cls.dlcID = {"identifier":"dlcId","val":204880}
        cls.iconHud = {"identifier":"hudIcon","val":"hud"}
        cls.iconHudSmall = {"identifier":"smallHudIcon","val":"smallhud"}
        cls.iconInfocard = {"identifier":"infoCardIcon","val":"infocard"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.PlanetBonus(
                filepath=self.filepath,
                entityType=self.entityType,
                minUpgLvl=self.minUpgLvl,
                maxUpgLvl=self.maxUpgLvl,
                nameString=self.nameString,
                descString=self.descString,
                floatBoni=self.floatBoni,
                intBoni=self.intBoni,
                dlcID=self.dlcID,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall
        )

    def testCreationCreateInstance(self):
        """ Test whether instance is created correctly from createInstance
        """
        res = orbitbo.PlanetBonus.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationCreateInstanceMalformed(self):
        """ Test whether createInstance returns parseProblem when malformed
        """
        res = orbitbo.PlanetBonus.createInstance("bonus_malformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribMinUpgLvl(self):
        """ Test whether attribute minUpgLvl is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.minUpgLvl)
        self.assertEqual(self.inst.minUpgLvl,exp)

    def testCreationAttribMaxUpgLvl(self):
        """ Test whether attribute maxUpgLvl is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.maxUpgLvl)
        self.assertEqual(self.inst.maxUpgLvl,exp)

    def testCreationAttribDlcID(self):
        """ Test whether attribute dlcID is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.dlcID)
        self.assertEqual(self.inst.dlcID,exp)

    def testCreationAttribFloatBoni(self):
        """ Test whether attribute floatBoni is constructed correctly
        """
        exp = [co_attribs.AttribNum(**bonus) for bonus in self.floatBoni]
        self.assertEqual(self.inst.floatBoni,exp)

    def testCreationAttribIntBoni(self):
        """ Test whether attribute intBoni is constructed correctly
        """
        exp = [
            co_attribs.AttribNum(**bonus) for bonus in self.intBoni]
        self.assertEqual(self.inst.intBoni,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        self.mmod.strings.checkString.return_value = []
        self.mmod.brushes.checkBrush.return_value = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMingtMax(self):
        """ Test whether check returns problem with minUpgLvl > maxUpgLvl
        """
        self.mmod.strings.checkString.return_value = []
        self.mmod.brushes.checkBrush.return_value = []
        self.inst.minUpgLvl.value = 3.0
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)

"""########################## Tests CelestialBody ##########################"""

class CelestialBodyFixtures(HasAbilitiesFixtures):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "moveAreaRadius 60000.0\n"
            +"hyperspaceExitRadius 50000.0\n"
            +"isWormhole FALSE\n"
            +"maxStarBaseCountPerPlayer 4\n"
            +"maxSpaceMineCountPerPlayer 500\n"
            +cls.parseString
        )
        cls.moveArea = {"identifier":"moveAreaRadius","val":60000.0}
        cls.hyperspaceExit = {"identifier":"hyperspaceExitRadius",
            "val":50000.0}
        cls.wormhole = {"identifier":"isWormhole","val":False}
        cls.maxStarbases = {"identifier":"maxStarBaseCountPerPlayer","val":4}
        cls.maxMines = {"identifier":"maxSpaceMineCountPerPlayer","val":500}

class CelestialBodyTests(CelestialBodyFixtures,unit.TestCase):
    desc = "Tests CelestialBody:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.CelestialBody(
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities,
                moveArea=self.moveArea,
                hyperspaceExit=self.hyperspaceExit,
                wormhole=self.wormhole,
                maxStarbases=self.maxStarbases,
                maxMines=self.maxMines
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parser = (
            orbitbo.g_celestialBody
            +coe.g_zoomDist
            +coe.g_shadows
            +coe.g_abilities
        )
        parseRes = parser.parseString(self.parseString)
        res = orbitbo.CelestialBody(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMoveArea(self):
        """ Test whether moveArea attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.moveArea)
        self.assertEqual(self.inst.moveArea,exp)

    def testCreationAttribHyperspaceExit(self):
        """ Test whether hyperspaceExit attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.hyperspaceExit)
        self.assertEqual(self.inst.hyperspaceExit,exp)

    def testCreationAttribWormhole(self):
        """ Test whether wormhole attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.wormhole)
        self.assertEqual(self.inst.wormhole,exp)

    def testCreationAttribMaxStarbases(self):
        """ Test whether maxStarbases attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxStarbases)
        self.assertEqual(self.inst.maxStarbases,exp)

    def testCreationAttribMaxMines(self):
        """ Test whether maxMines attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxMines)
        self.assertEqual(self.inst.maxMines,exp)

"""########################## Tests MeshInfoStar ###########################"""

class MeshInfoStarTest(unit.TestCase):
    desc = "Tests MeshInfoStar:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "meshInfo\n"
            +"\tmeshName \"testmesh\"\n"
            +"\tshouldPrefer TRUE\n"
        )
        cls.identifier = "meshInfo"
        cls.mesh = {"identifier":"meshName","val":"testmesh"}
        cls.prefer = {"identifier":"shouldPrefer","val":True}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.MeshInfoStar(
                identifier=self.identifier,
                mesh=self.mesh,
                prefer=self.prefer
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = orbitbo.g_meshInfo.parseString(self.parseString)[0]
        res = orbitbo.MeshInfoStar(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrefer(self):
        """ Test whether prefer attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.prefer)
        self.assertEqual(self.inst.prefer,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Star ###############################"""

class StarFixtures(CelestialBodyFixtures,MainViewSelectableFixture):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Star.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.coronaStartColor = {"identifier":"coronaStartColor",
            "val":"ff94c012"}
        cls.coronaEndColor = {"identifier":"coronaEndColor",
            "val":"ff217001"}
        cls.coronaStartColorDist = {"identifier":"coronaStartColorDistance",
            "val":0.0}
        cls.coronaEndColorDist = {"identifier":"coronaEndColorDistance",
            "val":1.2}
        cls.iconFar = {"identifier":"farIcon","val":"faricon"}
        cls.coloredSkyboxes = {
            "counter":{"identifier":"coloredSkyboxMeshInfoCount","val":1},
            "elements":[
                {
                    "identifier":"meshInfo",
                    "mesh":{"identifier":"meshName","val":"Testmesh1"},
                    "prefer":{"identifier":"shouldPrefer","val":True}
                }
            ]
        }
        cls.deepSpaceSkyboxes = {
            "counter":{"identifier":"deepSpaceSkyboxMeshInfoCount","val":1},
            "elements":[
                {
                    "identifier":"meshInfo",
                    "mesh":{"identifier":"meshName","val":"Testmesh2"},
                    "prefer":{"identifier":"shouldPrefer","val":False}
                }
            ]
        }
        cls.textureIcon = {"identifier":"iconTextureName","val":"texicon"}
        cls.dlcID = {"identifier":"dlcID","val":204880}
        cls.mesh = {"identifier":"MeshName","val":"Testmesh3"}
        cls.maxVisibilityDist = {
            "identifier":"MaxMeshVisibilityDistanceToCamera",
            "val":-1.0}
        cls.entityType = {"identifier":"entityType","val":"Star"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.Star(
                filepath=self.filepath,
                entityType=self.entityType,
                zoomDist=self.zoomDist,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                abilities=self.abilities,
                moveArea=self.moveArea,
                hyperspaceExit=self.hyperspaceExit,
                wormhole=self.wormhole,
                maxStarbases=self.maxStarbases,
                maxMines=self.maxMines,
                coronaStartColor=self.coronaStartColor,
                coronaEndColor=self.coronaEndColor,
                coronaStartColorDist=self.coronaStartColorDist,
                coronaEndColorDist=self.coronaEndColorDist,
                iconFar=self.iconFar,
                coloredSkyboxes=self.coloredSkyboxes,
                deepSpaceSkyboxes=self.deepSpaceSkyboxes,
                textureIcon=self.textureIcon,
                dlcID=self.dlcID,
                mesh=self.mesh,
                maxVisibilityDist=self.maxVisibilityDist
        )
        self.maxDiff = None

class StarTests(StarFixtures,unit.TestCase):
    desc = "Tests Star:"

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orbitbo.Star.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problems are returned correctly
        """
        res = orbitbo.Star.createInstance("StarMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribCoronaStartColor(self):
        """ Test whether coronaStartColor attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.coronaStartColor)
        self.assertEqual(self.inst.coronaStartColor,exp)

    def testCreationAttribCoronaEndColor(self):
        """ Test whether coronaEndColor attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.coronaEndColor)
        self.assertEqual(self.inst.coronaEndColor,exp)

    def testCreationAttribCoronaStartColorDist(self):
        """ Test whether coronaStartColorDist attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.coronaStartColorDist)
        self.assertEqual(self.inst.coronaStartColorDist,exp)

    def testCreationAttribCoronaEndColorDist(self):
        """ Test whether coronaEndColorDist attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.coronaEndColorDist)
        self.assertEqual(self.inst.coronaEndColorDist,exp)

    def testCreationAttribIconFar(self):
        """ Test whether iconFar attribute is created correctly
        """
        exp = ui.BrushRef(**self.iconFar)
        self.assertEqual(self.inst.iconFar,exp)

    def testCreationAttribColoredSkyboxes(self):
        """ Test whether coloredSkyboxes attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.MeshInfoStar,
                **self.coloredSkyboxes)
        self.assertEqual(self.inst.coloredSkyboxes,exp)

    def testCreationAttribDeepSpaceSkyboxes(self):
        """ Test whether deepSpaceSkyboxes attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.MeshInfoStar,
                **self.deepSpaceSkyboxes)
        self.assertEqual(self.inst.deepSpaceSkyboxes,exp)

    def testCreationAttribTextureIcon(self):
        """ Test whether textureIcon attribute is created correctly
        """
        exp = ui.UITextureRef(**self.textureIcon)
        self.assertEqual(self.inst.textureIcon,exp)

    def testCreationAttribDlcID(self):
        """ Test whether dlcID attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.dlcID)
        self.assertEqual(self.inst.dlcID,exp)

    def testCreationAttribMesh(self):
        """ Test whether mesh attribute is created correctly
        """
        exp = meshes.MeshRef(**self.mesh)
        self.assertEqual(self.inst.mesh,exp)

    def testCreationAttribMaxVisibilityDist(self):
        """ Test whether maxVisibilityDist attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxVisibilityDist)
        self.assertEqual(self.inst.maxVisibilityDist,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class StarCheck(StarFixtures,unit.TestCase):
    desc = "Test Star Checking:"

    def setUp(self):
        super().setUp()
        self.mmod.strings.checkString.return_value = True
        self.mmod.brushes.checkBrush.return_value = []
        self.mockProb = mock.MagicMock(probFile=None)
        self.mockProb.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        patcher = mock.patch.object(self.inst.coloredSkyboxes,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockColoredSky = patcher.start()
        patcher = mock.patch.object(self.inst.deepSpaceSkyboxes,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockDeepSky = patcher.start()
        patcher = mock.patch.object(self.inst.mesh,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        patcher = mock.patch.object(self.inst.textureIcon,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTextureIcon = patcher.start()
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckIconFar(self):
        """ Test whether problems for iconFar attribute are returned
        """
        with mock.patch.object(self.inst.iconFar,"check",autospec=True,
                spec_set=True,return_value=[self.mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[self.mockProb])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckColoredSkyboxes(self):
        """ Test whether problems for coloredSkyboxes attribute are returned
        """
        self.mockColoredSky.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockColoredSky.assert_called_once_with(self.mmod,False)

    def testCheckDeepSpaceSkyboxes(self):
        """ Test whether problems for coloredSkyboxes attribute are returned
        """
        self.mockDeepSky.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockDeepSky.assert_called_once_with(self.mmod,False)

    def testCheckTextureIcon(self):
        """ Test whether problems for textureIcon attribute are returned
        """
        self.mockTextureIcon.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockTextureIcon.assert_called_once_with(self.mmod,False)

    def testCheckMesh(self):
        """ Test whether problems for mesh attribute are returned
        """
        self.mockMesh.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockMesh.assert_called_once_with(self.mmod,False)

"""######################### Tests MeshInfoPlanet ##########################"""

class MeshInfoPlanetTests(MainViewSelectableFixture,unit.TestCase):
    desc = "Tests MeshInfoPlanet:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "meshInfo\n"
            +"\ttypeNameStringID \"namestring\"\n"
            +"\tasteroidTemplate \"AstTemp\"\n"
            +"\tdustCloudTemplate \"CloudTemp\"\n"
            +"\tmeshName \"testmesh\"\n"
            +"\tcloudColor 0\n"
            +"\tnullMeshRadius 3000.000000\n"
            +"\tnullMeshParticleEffect \"testparticle\"\n"
            +"\thudIcon \"hudicon\"\n"
            +"\tsmallHudIcon \"smallhudicon\"\n"
            +"\tinfoCardIcon \"infocardicon\"\n"
            +"\tmainViewIcon \"mainviewicon\"\n"
            +"\tundetectedMainViewIcon \"undetected\"\n"
            +"\tpicture \"picture\"\n"
            +"\tshouldPrefer TRUE\n"
        )
        cls.identifier = "meshInfo"
        cls.templateAsteroid = {"identifier":"asteroidTemplate","val":"AstTemp"}
        cls.templateDustCloud = {"identifier":"dustCloudTemplate",
            "val":"CloudTemp"}
        cls.mesh = {"identifier":"meshName","val":"testmesh"}
        cls.colorCloud = {"identifier":"cloudColor","val":"0"}
        cls.nullMeshRadius = {"identifier":"nullMeshRadius","val":3000.0}
        cls.nullMeshEff = {"identifier":"nullMeshParticleEffect",
            "val":"testparticle"}
        cls.iconMainUndetected = {"identifier":"undetectedMainViewIcon",
            "val":"undetected"}
        cls.prefer = {"identifier":"shouldPrefer","val":True}
        cls.nameString["identifier"] = "typeNameStringID"
        cls.mmod = tco.genMockMod("./")
        cls.mmod.brushes.checkBrush.return_value = []
        cls.mmod.strings.checkString.return_value = []

    def setUp(self):
        self.inst = orbitbo.MeshInfoPlanet(
                identifier=self.identifier,
                mesh=self.mesh,
                prefer=self.prefer,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                templateAsteroid=self.templateAsteroid,
                templateDustCloud=self.templateDustCloud,
                colorCloud=self.colorCloud,
                nullMeshRadius=self.nullMeshRadius,
                nullMeshEff=self.nullMeshEff,
                iconMainUndetected=self.iconMainUndetected
        )
        patcher = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        patcher = mock.patch.object(self.inst.templateAsteroid,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockAst = patcher.start()
        patcher = mock.patch.object(self.inst.templateDustCloud,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockCloud = patcher.start()
        patcher = mock.patch.object(self.inst.nullMeshEff,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockEff = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results 
        """
        parseRes = orbitbo.g_plMeshInfo.parseString(self.parseString)[0]
        res = orbitbo.MeshInfoPlanet(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTemplateAsteroid(self):
        """ Test whether templateAsteroid atrrib is created correctly
        """
        exp = uncol.AsteroidRef(**self.templateAsteroid)
        self.assertEqual(self.inst.templateAsteroid,exp)

    def testCreationAttribTemplateDustCloud(self):
        """ Test whether templateDustCloud atrrib is created correctly
        """
        exp = uncol.CloudRef(**self.templateDustCloud)
        self.assertEqual(self.inst.templateDustCloud,exp)

    def testCreationAttribColorCloud(self):
        """ Test whether colorCloud atrrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.colorCloud)
        self.assertEqual(self.inst.colorCloud,exp)

    def testCreationAttribNullMeshRadius(self):
        """ Test whether nullMeshRadius atrrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nullMeshRadius)
        self.assertEqual(self.inst.nullMeshRadius,exp)

    def testCreationAttribNullMeshEffect(self):
        """ Test whether nullMeshEff atrrib is created correctly
        """
        exp = par.ParticleRef(canBeEmpty=True,**self.nullMeshEff)
        self.assertEqual(self.inst.nullMeshEff,exp)

    def testCreationAttribNullMeshEffectNotEmpty(self):
        """ Test whether nullMeshEff atrrib is created correctly not empty
        """
        newMeshRef = {"identifier":"meshName","val":""}
        localInst = orbitbo.MeshInfoPlanet(
                identifier=self.identifier,
                mesh=newMeshRef,
                prefer=self.prefer,
                iconHud=self.iconHud,
                iconInfocard=self.iconInfocard,
                iconHudSmall=self.iconHudSmall,
                nameString=self.nameString,
                iconMainView=self.mainViewIcon,
                pic=self.pic,
                templateAsteroid=self.templateAsteroid,
                templateDustCloud=self.templateDustCloud,
                colorCloud=self.colorCloud,
                nullMeshRadius=self.nullMeshRadius,
                nullMeshEff=self.nullMeshEff,
                iconMainUndetected=self.iconMainUndetected
        )
        exp = par.ParticleRef(**self.nullMeshEff)
        self.assertEqual(localInst.nullMeshEff,exp)

    def testCreationAttribUndetectedMainViewIcon(self):
        """ Test whether iconMainUndetected atrrib is created correctly
        """
        exp = ui.BrushRef(**self.iconMainUndetected)
        self.assertEqual(self.inst.iconMainUndetected,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckTemplateAsteroid(self):
        """ Test whether check returns problems in templateAsteroid attrib
        """
        self.mockAst.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockAst.assert_called_once_with(self.mmod,False)

    def testCheckTemplateDustCloud(self):
        """ Test whether check returns problems in templateDustCloud attrib
        """
        self.mockCloud.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockCloud.assert_called_once_with(self.mmod,False)

    def testCheckNullMeshEff(self):
        """ Test whether check returns problems in nullMeshEff attrib
        """
        self.mockEff.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockEff.assert_called_once_with(self.mmod,False)

    def testCheckIconMainUndetected(self):
        """ Test whether check returns problems in iconMainUndetected attrib
        """
        with mock.patch.object(self.inst.iconMainUndetected,"check",
                autospec=True,spec_set=True,return_value=[mock.sentinel.prob]
        ) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests AsteroidSetup ###########################"""

class AsteroidSetupTests(unit.TestCase):
    desc = "Tests AsteroidSetup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "ident\n"
            +"\tminCount 2\n"
            +"\tmaxCount 4\n"
            +"\textractionRate 20.500000\n"
            +"\trefineryRate 40.500000\n"
            +"\tmaxRefineryCount 3\n"
        )
        cls.identifier = "ident"
        cls.countMin = {"identifier":"minCount","val":2}
        cls.countMax = {"identifier":"maxCount","val":4}
        cls.rateExtract = {"identifier":"extractionRate","val":20.5}
        cls.rateRefinery = {"identifier":"refineryRate","val":40.5}
        cls.refineriesMax = {"identifier":"maxRefineryCount","val":3}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.AsteroidSetup(
                identifier=self.identifier,
                countMin=self.countMin,
                countMax=self.countMax,
                rateExtract=self.rateExtract,
                rateRefinery=self.rateRefinery,
                refineriesMax=self.refineriesMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = co_parse.genHeading("ident")+orbitbo.g_asteroidSetup
        parseRes = parser.parseString(self.parseString)
        res = orbitbo.AsteroidSetup(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCountMin(self):
        """ Test whether countMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.countMin)
        self.assertEqual(self.inst.countMin,exp)

    def testCreationAttribCountMax(self):
        """ Test whether countMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.countMax)
        self.assertEqual(self.inst.countMax,exp)

    def testCreationAttribRateExtract(self):
        """ Test whether rateExtract attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.rateExtract)
        self.assertEqual(self.inst.rateExtract,exp)

    def testCreationAttribRefineryRate(self):
        """ Test whether rateRefinery attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.rateRefinery)
        self.assertEqual(self.inst.rateRefinery,exp)

    def testCreationAttribRefineriesMax(self):
        """ Test whether refineriesMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.refineriesMax)
        self.assertEqual(self.inst.refineriesMax,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test wehther string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests RessourceSetup ##########################"""

class ResourceSetupTests(unit.TestCase):
    desc = "Tests ResourceSetup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetResourceSetupInfo\n"
            +"\tasteroidSpawnAngleVariance 3.140000\n"
            +"\ttotalMaxResourceAsteroids 2\n"
            +"\tmetalResourceAsteroidSetup\n"
            +"\t\tminCount 2\n"
            +"\t\tmaxCount 4\n"
            +"\t\textractionRate 20.500000\n"
            +"\t\trefineryRate 40.500000\n"
            +"\t\tmaxRefineryCount 3\n"
            +"\tcrystalResourceAsteroidSetup\n"
            +"\t\tminCount 0\n"
            +"\t\tmaxCount 2\n"
            +"\t\textractionRate 10.500000\n"
            +"\t\trefineryRate 30.500000\n"
            +"\t\tmaxRefineryCount 3\n"
            +"\tneutralMetalResourceAsteroidSetup\n"
            +"\t\tminCount 0\n"
            +"\t\tmaxCount 0\n"
            +"\t\textractionRate 0.000000\n"
            +"\t\trefineryRate 0.000000\n"
            +"\t\tmaxRefineryCount 0\n"
            +"\tneutralCrystalResourceAsteroidSetup\n"
            +"\t\tminCount 0\n"
            +"\t\tmaxCount 0\n"
            +"\t\textractionRate 0.000000\n"
            +"\t\trefineryRate 0.000000\n"
            +"\t\tmaxRefineryCount 2\n"
        )
        cls.identifier = "planetResourceSetupInfo"
        cls.asteroidSpawnAngle = {"identifier":"asteroidSpawnAngleVariance",
            "val":3.14}
        cls.maxAsteroids = {"identifier":"totalMaxResourceAsteroids",
            "val":2}
        cls.asteroidsMetal = {
            "identifier":"metalResourceAsteroidSetup",
            "countMin":{"identifier":"minCount","val":2},
            "countMax":{"identifier":"maxCount","val":4},
            "rateExtract":{"identifier":"extractionRate","val":20.5},
            "rateRefinery":{"identifier":"refineryRate","val":40.5},
            "refineriesMax":{"identifier":"maxRefineryCount","val":3}
        }
        cls.asteroidsCrystal = {
            "identifier":"crystalResourceAsteroidSetup",
            "countMin":{"identifier":"minCount","val":0},
            "countMax":{"identifier":"maxCount","val":2},
            "rateExtract":{"identifier":"extractionRate","val":10.5},
            "rateRefinery":{"identifier":"refineryRate","val":30.5},
            "refineriesMax":{"identifier":"maxRefineryCount","val":3}
        }
        cls.asteroidsMetalNeutral = {
            "identifier":"neutralMetalResourceAsteroidSetup",
            "countMin":{"identifier":"minCount","val":0},
            "countMax":{"identifier":"maxCount","val":0},
            "rateExtract":{"identifier":"extractionRate","val":0.0},
            "rateRefinery":{"identifier":"refineryRate","val":0.0},
            "refineriesMax":{"identifier":"maxRefineryCount","val":0}
        }
        cls.asteroidsCrystalNeutral = {
            "identifier":"neutralCrystalResourceAsteroidSetup",
            "countMin":{"identifier":"minCount","val":0},
            "countMax":{"identifier":"maxCount","val":0},
            "rateExtract":{"identifier":"extractionRate","val":0.0},
            "rateRefinery":{"identifier":"refineryRate","val":0.0},
            "refineriesMax":{"identifier":"maxRefineryCount","val":2}
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.ResourceSetup(
                identifier=self.identifier,
                asteroidSpawnAngle=self.asteroidSpawnAngle,
                maxAsteroids=self.maxAsteroids,
                asteroidsMetal=self.asteroidsMetal,
                asteroidsCrystal=self.asteroidsCrystal,
                asteroidsMetalNeutral=self.asteroidsMetalNeutral,
                asteroidsCrystalNeutral=self.asteroidsCrystalNeutral
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_resourceSetup.parseString(self.parseString)[0]
        res = orbitbo.ResourceSetup(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribAsteroidSpawnAngle(self):
        """ Test whether asteroidSpawnAngle is created correctly
        """
        exp = co_attribs.AttribNum(**self.asteroidSpawnAngle)
        self.assertEqual(self.inst.asteroidSpawnAngle,exp)

    def testCreationAttribMaxAsteroids(self):
        """ Test whether maxAsteroids is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxAsteroids)
        self.assertEqual(self.inst.maxAsteroids,exp)

    def testCreationAttribAsteroidsMetal(self):
        """ Test whether asteroidsMetal is created correctly
        """
        exp = orbitbo.AsteroidSetup(**self.asteroidsMetal)
        self.assertEqual(self.inst.asteroidsMetal,exp)

    def testCreationAttribAsteroidsCrystal(self):
        """ Test whether asteroidsCrystal is created correctly
        """
        exp = orbitbo.AsteroidSetup(**self.asteroidsCrystal)
        self.assertEqual(self.inst.asteroidsCrystal,exp)

    def testCreationAttribAsteroidsMetalNeutral(self):
        """ Test whether asteroidsMetalNeutral is created correctly
        """
        exp = orbitbo.AsteroidSetup(**self.asteroidsMetalNeutral)
        self.assertEqual(self.inst.asteroidsMetalNeutral,exp)

    def testCreationAttribAsteroidsCrystalNeutral(self):
        """ Test whether asteroidsCrystalNeutral is created correctly
        """
        exp = orbitbo.AsteroidSetup(**self.asteroidsCrystalNeutral)
        self.assertEqual(self.inst.asteroidsCrystalNeutral,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""###################### Tests Planet Upgrade Stages ######################"""

class PlanetStageFixture(object):
    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "stage\n"
            +"\tprice\n"
            +"\t\tcredits 0.000000\n"
            +"\t\tmetal 0.000000\n"
            +"\t\tcrystal 0.000000\n"
            +"\tupgradeTime 0.000000\n"
        )
        cls.identifier = "stage"
        cls.price = {
            "identifier":"price",
            "credits":{"identifier":"credits","val":0.0},
            "metal":{"identifier":"metal","val":0.0},
            "crystal":{"identifier":"crystal","val":0.0}
        }
        cls.upgTime = {"identifier":"upgradeTime","val":0.0}
        cls.mmod = tco.genMockMod("./")

class PlanetUpgradeStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetUpgradeStage:"

    def setUp(self):
        self.inst = orbitbo.PlanetUpgradeStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_plStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetUpgradeStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPrice(self):
        """ Test whether price attrib is created correctly
        """
        exp = coe.Cost(**self.price)
        self.assertEqual(self.inst.price,exp)

    def testCreationAttribUpgTime(self):
        """ Test whether upgTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.upgTime)
        self.assertEqual(self.inst.upgTime,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetPopStage(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetPopStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tmaxPopulation 10.000000\n"
            +"\tpopulationGrowthRate 0.020000\n"
            +"\tdevelopmentTaxPenalty -2.000000\n"
        )
        cls.popMax = {"identifier":"maxPopulation","val":10.0}
        cls.popGrowth = {"identifier":"populationGrowthRate","val":0.02}
        cls.taxPenalty = {"identifier":"developmentTaxPenalty","val":-2.0}

    def setUp(self):
        self.inst = orbitbo.PlanetPopStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                popMax=self.popMax,
                popGrowth=self.popGrowth,
                taxPenalty=self.taxPenalty
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_popStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetPopStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPopMax(self):
        """ Test whether popMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.popMax)
        self.assertEqual(self.inst.popMax,exp)

    def testCreationAttribPopGrowth(self):
        """ Test whether popGrowth attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.popGrowth)
        self.assertEqual(self.inst.popGrowth,exp)

    def testCreationAttribTaxPenalty(self):
        """ Test whether taxPenalty attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.taxPenalty)
        self.assertEqual(self.inst.taxPenalty,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetInfrastructureStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetInfrastructureStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tmaxHealth 1000.000000\n"
        )
        cls.maxHealth = {"identifier":"maxHealth","val":1000.0}

    def setUp(self):
        self.inst = orbitbo.PlanetInfrastructureStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                maxHealth=self.maxHealth
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_infraStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetInfrastructureStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMaxHealth(self):
        """ Test whether maxHealth attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.maxHealth)
        self.assertEqual(self.inst.maxHealth,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetIndustrialStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetIndustrialStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tmaxPopulation 0.000000\n"
            +"\ttradeIncomeModifier 0.000000\n"
            +"\tshipBuildRateModifier 0.000000\n"
            +"\tcultureSpreadRateModifier 0.000000\n"
        )
        cls.popMax = {"identifier":"maxPopulation","val":0.0}
        cls.tradeMod = {"identifier":"tradeIncomeModifier","val":0.0}
        cls.shipBuildMod = {"identifier":"shipBuildRateModifier","val":0.0}
        cls.cultureSpreadMod = {"identifier":"cultureSpreadRateModifier",
            "val":0.0}

    def setUp(self):
        self.inst = orbitbo.PlanetIndustrialStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                popMax=self.popMax,
                tradeMod=self.tradeMod,
                shipBuildMod=self.shipBuildMod,
                cultureSpreadMod=self.cultureSpreadMod
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_industryStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetIndustrialStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPopMax(self):
        """ Test whether popMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.popMax)
        self.assertEqual(self.inst.popMax,exp)

    def testCreationAttribTradeMod(self):
        """ Test whether tradeMod attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.tradeMod)
        self.assertEqual(self.inst.tradeMod,exp)

    def testCreationAttribShipBuildMod(self):
        """ Test whether shipBuildMod attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.shipBuildMod)
        self.assertEqual(self.inst.shipBuildMod,exp)

    def testCreationAttribCultureSpreadMod(self):
        """ Test whether cultureSpreadMod attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.cultureSpreadMod)
        self.assertEqual(self.inst.cultureSpreadMod,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetSocialStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetSocialStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tmaxPopulation 0.000000\n"
            +"\tpopulationGrowthRate 0.000000\n"
            +"\ttradeIncomeModifier 0.000000\n"
            +"\tshipBuildRateModifier 0.000000\n"
            +"\tcultureSpreadRateModifier 0.000000\n"
        )
        cls.popMax = {"identifier":"maxPopulation","val":0.0}
        cls.popGrowth = {"identifier":"populationGrowthRate","val":0.0}
        cls.tradeMod = {"identifier":"tradeIncomeModifier","val":0.0}
        cls.shipBuildMod = {"identifier":"shipBuildRateModifier","val":0.0}
        cls.cultureSpreadMod = {"identifier":"cultureSpreadRateModifier",
            "val":0.0}

    def setUp(self):
        self.inst = orbitbo.PlanetSocialStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                popMax=self.popMax,
                popGrowth=self.popGrowth,
                tradeMod=self.tradeMod,
                shipBuildMod=self.shipBuildMod,
                cultureSpreadMod=self.cultureSpreadMod
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_socStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetSocialStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPopGrowth(self):
        """ Test whether popGrowth attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.popGrowth)
        self.assertEqual(self.inst.popGrowth,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetModuleStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetModuleStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tmaxModuleSlotCount:Tactical 15.000000\n"
            "\tmaxModuleConstructorCount 2\n"
        )
        cls.maxSlots = {"identifier":"maxModuleSlotCount:Tactical","val":15.0}
        cls.maxConstructors = {"identifier":"maxModuleConstructorCount",
            "val":2}

    def setUp(self):
        self.inst = orbitbo.PlanetModuleStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                maxSlots=self.maxSlots,
                maxConstructors=self.maxConstructors
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_tacStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetModuleStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMaxSlots(self):
        """ Test whether maxSlots attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxSlots)
        self.assertEqual(self.inst.maxSlots,exp)

    def testCreationAttribMaxConstructors(self):
        """ Test whether maxConstructors attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxConstructors)
        self.assertEqual(self.inst.maxConstructors,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetHomeStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetHomeStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tisHomePlanet FALSE\n"
            +"\thomePlanetTaxRateBonus 0.000000\n"
            +"\thomePlanetMetalIncomeBonus 0.000000\n"
            +"\thomePlanetCrystalIncomeBonus 0.000000\n"
        )
        cls.isHome = {"identifier":"isHomePlanet","val":False}
        cls.taxBonus = {"identifier":"homePlanetTaxRateBonus","val":0.0}
        cls.metalBonus = {"identifier":"homePlanetMetalIncomeBonus","val":0.0}
        cls.crystalBonus = {"identifier":"homePlanetCrystalIncomeBonus",
            "val":0.0}

    def setUp(self):
        self.inst = orbitbo.PlanetHomeStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                isHome=self.isHome,
                taxBonus=self.taxBonus,
                metalBonus=self.metalBonus,
                crystalBonus=self.crystalBonus
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_homeStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetHomeStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribIsHome(self):
        """ Test whether isHome attrib is constructed correctly
        """
        exp = co_attribs.AttribBool(**self.isHome)
        self.assertEqual(self.inst.isHome,exp)

    def testCreationAttribTaxBonus(self):
        """ Test whether taxBonus attrib is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.taxBonus)
        self.assertEqual(self.inst.taxBonus,exp)

    def testCreationAttribMetalBonus(self):
        """ Test whether metalBonus attrib is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.metalBonus)
        self.assertEqual(self.inst.metalBonus,exp)

    def testCreationAttribCrystalBonus(self):
        """ Test whether crystalBonus attrib is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.crystalBonus)
        self.assertEqual(self.inst.crystalBonus,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetSmugglerStageTests(PlanetStageFixture,unit.TestCase):
    desc = "Tests PlanetSmugglerStage:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString += (
            "\tcorruptionPercent 0.250000\n"
            +"\tsmugglingPercent 0.500000\n"
        )
        cls.corruption = {"identifier":"corruptionPercent","val":0.25}
        cls.smuggling = {"identifier":"smugglingPercent","val":0.5}

    def setUp(self):
        self.inst = orbitbo.PlanetSmugglerStage(
                identifier=self.identifier,
                price=self.price,
                upgTime=self.upgTime,
                corruption=self.corruption,
                smuggling=self.smuggling
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_smugglerStage.parseString(self.parseString)[0]
        res = orbitbo.PlanetSmugglerStage(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCorruption(self):
        """ Test whether corruption attrib is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.corruption)
        self.assertEqual(self.inst.corruption,exp)

    def testCreationAttribMetalBonus(self):
        """ Test whether smuggling attrib is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.smuggling)
        self.assertEqual(self.inst.smuggling,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetUpgradesTests(unit.TestCase):
    desc = "Tests PlanetUpgrades:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetUpgradeDef\n"
            +"\tpath:Population\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tmaxPopulation 10.000000\n"
            +"\t\t\tpopulationGrowthRate 0.020000\n"
            +"\t\t\tdevelopmentTaxPenalty -2.000000\n"
            +"\tpath:CivilianModules\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tmaxModuleSlotCount:Civilian 15.000000\n"
            +"\t\t\tmaxModuleConstructorCount 2\n"
            +"\tpath:TacticalModules\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tmaxModuleSlotCount:Tactical 15.000000\n"
            +"\t\t\tmaxModuleConstructorCount 2\n"
            +"\tpath:Home\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tisHomePlanet FALSE\n"
            +"\t\t\thomePlanetTaxRateBonus 0.000000\n"
            +"\t\t\thomePlanetMetalIncomeBonus 0.000000\n"
            +"\t\t\thomePlanetCrystalIncomeBonus 0.000000\n"
            +"\tpath:ArtifactLevel\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\tpath:Infrastructure\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tmaxHealth 1000.000000\n"
            +"\tpath:Social\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tmaxPopulation 0.000000\n"
            +"\t\t\tpopulationGrowthRate 0.000000\n"
            +"\t\t\ttradeIncomeModifier 0.000000\n"
            +"\t\t\tshipBuildRateModifier 0.000000\n"
            +"\t\t\tcultureSpreadRateModifier 0.000000\n"
            +"\tpath:Industry\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tmaxPopulation 0.000000\n"
            +"\t\t\ttradeIncomeModifier 0.000000\n"
            +"\t\t\tshipBuildRateModifier 0.000000\n"
            +"\t\t\tcultureSpreadRateModifier 0.000000\n"
            +"\tpath:Smuggler\n"
            +"\t\tstageCount 1\n"
            +"\t\tstage\n"
            +"\t\t\tprice\n"
            +"\t\t\t\tcredits 0.000000\n"
            +"\t\t\t\tmetal 0.000000\n"
            +"\t\t\t\tcrystal 0.000000\n"
            +"\t\t\tupgradeTime 0.000000\n"
            +"\t\t\tcorruptionPercent 0.250000\n"
            +"\t\t\tsmugglingPercent 0.500000\n"
        )
        cls.identifier = "planetUpgradeDef"
        cls.population = {
            "identifier":"path:Population",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "popMax":{"identifier":"maxPopulation","val":10.0},
                    "popGrowth":{"identifier":"populationGrowthRate",
                        "val":0.02},
                    "taxPenalty":{"identifier":"developmentTaxPenalty",
                        "val":-2.0}
                }
            ]
        }
        cls.civilian = {
            "identifier":"path:CivilianModules",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "maxSlots":{"identifier":"maxModuleSlotCount:Civilian",
                        "val":15.0},
                    "maxConstructors":{
                        "identifier":"maxModuleConstructorCount",
                        "val":2}
                }
            ]
        }
        cls.tactical = {
            "identifier":"path:TacticalModules",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "maxSlots":{"identifier":"maxModuleSlotCount:Tactical",
                        "val":15.0},
                    "maxConstructors":{
                        "identifier":"maxModuleConstructorCount",
                        "val":2}
                }
            ]
        }
        cls.home = {
            "identifier":"path:Home",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "isHome":{"identifier":"isHomePlanet","val":False},
                    "taxBonus":{"identifier":"homePlanetTaxRateBonus",
                        "val":0.0},
                    "metalBonus":{
                        "identifier":"homePlanetMetalIncomeBonus",
                        "val":0.0},
                    "crystalBonus":{
                        "identifier":"homePlanetCrystalIncomeBonus",
                        "val":0.0}
                }
            ]
        }
        cls.artifact = {
            "identifier":"path:ArtifactLevel",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0}
                }
            ]
        }
        cls.infrastructure = {
            "identifier":"path:Infrastructure",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "maxHealth":{"identifier":"maxHealth","val":1000.0}
                }
            ]
        }
        cls.social = {
            "identifier":"path:Social",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "popMax":{"identifier":"maxPopulation","val":0.0},
                    "popGrowth":{"identifier":"populationGrowthRate",
                        "val":0.0},
                    "tradeMod":{"identifier":"tradeIncomeModifier",
                        "val":0.0},
                    "shipBuildMod":{"identifier":"shipBuildRateModifier",
                        "val":0.0},
                    "cultureSpreadMod":{
                        "identifier":"cultureSpreadRateModifier",
                        "val":0.0}
                }
            ]
        }
        cls.industry = {
            "identifier":"path:Industry",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "popMax":{"identifier":"maxPopulation","val":0.0},
                    "tradeMod":{"identifier":"tradeIncomeModifier",
                        "val":0.0},
                    "shipBuildMod":{"identifier":"shipBuildRateModifier",
                        "val":0.0},
                    "cultureSpreadMod":{
                        "identifier":"cultureSpreadRateModifier",
                        "val":0.0}
                }
            ]
        }
        cls.smuggler = {
            "identifier":"path:Smuggler",
            "counter":{"identifier":"stageCount","val":1},
            "elements":[
                {
                    "identifier":"stage",
                    "price":{
                        "identifier":"price",
                        "credits":{"identifier":"credits","val":0.0},
                        "metal":{"identifier":"metal","val":0.0},
                        "crystal":{"identifier":"crystal","val":0.0}
                    },
                    "upgTime":{"identifier":"upgradeTime","val":0.0},
                    "corruption":{"identifier":"corruptionPercent","val":0.25},
                    "smuggling":{"identifier":"smugglingPercent","val":0.5}
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.PlanetUpgrades(
                identifier=self.identifier,
                population=self.population,
                civilian=self.civilian,
                tactical=self.tactical,
                home=self.home,
                artifact=self.artifact,
                infrastructure=self.infrastructure,
                social=self.social,
                industry=self.industry,
                smuggler=self.smuggler
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_plUpgrades.parseString(self.parseString)[0]
        res = orbitbo.PlanetUpgrades(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPopulation(self):
        """ Test whether population attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetPopStage,**self.population)
        self.assertEqual(self.inst.population,exp)

    def testCreationAttribCivilian(self):
        """ Test whether civilian attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetModuleStage,**self.civilian)
        self.assertEqual(self.inst.civilian,exp)

    def testCreationAttribTactical(self):
        """ Test whether tactical attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetModuleStage,**self.tactical)
        self.assertEqual(self.inst.tactical,exp)

    def testCreationAttribHome(self):
        """ Test whether home attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetHomeStage,**self.home)
        self.assertEqual(self.inst.home,exp)

    def testCreationAttribArtifact(self):
        """ Test whether artifact attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetUpgradeStage,**self.artifact)
        self.assertEqual(self.inst.artifact,exp)

    def testCreationAttribInfrastructure(self):
        """ Test whether infrastructure attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetInfrastructureStage,
                **self.infrastructure)
        self.assertEqual(self.inst.infrastructure,exp)

    def testCreationAttribSocial(self):
        """ Test whether social attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetSocialStage,
                **self.social)
        self.assertEqual(self.inst.social,exp)

    def testCreationAttribIndustry(self):
        """ Test whether industry attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetIndustrialStage,
                **self.industry)
        self.assertEqual(self.inst.industry,exp)

    def testCreationAttribSmuggler(self):
        """ Test whether smuggler attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.PlanetSmugglerStage,
                **self.smuggler)
        self.assertEqual(self.inst.smuggler,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests SkyboxScalar ##########################"""

class SkyboxScalarTests(unit.TestCase):
    desc = "Tests SkyboxScalar:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "skyboxScalars\n"
            +"\tdiffuseScalar 1.000000\n"
            +"\tambientScalar 1.000000\n"
        )
        cls.identifier = "skyboxScalars"
        cls.diffuse = {"identifier":"diffuseScalar","val":1.0}
        cls.ambient = {"identifier":"ambientScalar","val":1.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = orbitbo.SkyboxScalar(
                identifier=self.identifier,
                diffuse=self.diffuse,
                ambient=self.ambient
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_skyboxScalar.parseString(self.parseString)[0]
        res = orbitbo.SkyboxScalar(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribDiffuse(self):
        """ Test whether diffuse attribute was created correctly
        """
        exp = co_attribs.AttribNum(**self.diffuse)
        self.assertEqual(self.inst.diffuse,exp)

    def testCreationAttribAmbient(self):
        """ Test whether ambient attribute was created correctly
        """
        exp = co_attribs.AttribNum(**self.ambient)
        self.assertEqual(self.inst.ambient,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################ Tests PlanetRuin ###########################"""

class PlanetRuinTests(unit.TestCase):
    desc = "Tests PlanetRuin:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetRuinDef\n"
            +"\truinPlanetType \"testruin\"\n"
            +"\tattachType \"Center\"\n"
            +"\teffectName \"testeffect\"\n"
            +"\tstartTime 9.000000\n"
            +"\tsoundID \"testsound\"\n"
            +"\tcreditsOnStrippedToTheCore 2000.000000\n"
            +"\tmetalOnStrippedToTheCore 750.000000\n"
            +"\tcrystalOnStrippedToTheCore 250.000000\n"
            +"\textraScuttleTimeOnStrip 0.000000\n"
        )
        cls.identifier = "planetRuinDef"
        cls.ruinType = {"identifier":"ruinPlanetType","val":"testruin"}
        cls.attachType = {"identifier":"attachType","val":"Center"}
        cls.effect = {"identifier":"effectName","val":"testeffect"}
        cls.timeStart = {"identifier":"startTime","val":9.0}
        cls.sound = {"identifier":"soundID","val":"testsound"}
        cls.gainedCredits = {"identifier":"creditsOnStrippedToTheCore",
            "val":2000.0}
        cls.gainedMetal = {"identifier":"metalOnStrippedToTheCore",
            "val":750.0}
        cls.gainedCrystal = {"identifier":"crystalOnStrippedToTheCore",
            "val":250.0}
        cls.timeScuttle = {"identifier":"extraScuttleTimeOnStrip","val":0.0}

    def setUp(self):
        self.inst = orbitbo.PlanetRuin(
                identifier=self.identifier,
                ruinType=self.ruinType,
                attachType=self.attachType,
                effect=self.effect,
                timeStart=self.timeStart,
                sound=self.sound,
                gainedCredits=self.gainedCredits,
                gainedMetal=self.gainedMetal,
                gainedCrystal=self.gainedCrystal,
                timeScuttle=self.timeScuttle
        )
        self.mmod = tco.genMockMod("./")
        self.mmod.audio.checkEffect.return_value = []
        patcher = mock.patch.object(self.inst.ruinType,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockRuin = patcher.start()
        patcher = mock.patch.object(self.inst.effect,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockEffect = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = orbitbo.g_plRuin.parseString(self.parseString)[0]
        res = orbitbo.PlanetRuin(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribRuinType(self):
        """ Test whether ruinType attrib is created correctly
        """
        exp = coe.EntityRef(types=["Planet"],**self.ruinType)
        self.assertEqual(self.inst.ruinType,exp)

    def testCreationAttribAttachType(self):
        """ Test whether attachType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.ATTACH_TYPES,**self.attachType)
        self.assertEqual(self.inst.attachType,exp)

    def testCreationAttribEffect(self):
        """ Test whether effect attrib is created correctly
        """
        exp = par.ParticleRef(**self.effect)
        self.assertEqual(self.inst.effect,exp)

    def testCreationAttribTimeStart(self):
        """ Test whether timeStart attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.timeStart)
        self.assertEqual(self.inst.timeStart,exp)

    def testCreationAttribSound(self):
        """ Test whether sound attrib is created correctly
        """
        exp = audio.SoundRef(**self.sound)
        self.assertEqual(self.inst.sound,exp)

    def testCreationAttribGainedCredits(self):
        """ Test whether gainedCredits attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.gainedCredits)
        self.assertEqual(self.inst.gainedCredits,exp)

    def testCreationAttribGainedGainedMetal(self):
        """ Test whether gainedMetal attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.gainedMetal)
        self.assertEqual(self.inst.gainedMetal,exp)

    def testCreationAttribGainedGainedCrystal(self):
        """ Test whether gainedCrystal attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.gainedCrystal)
        self.assertEqual(self.inst.gainedCrystal,exp)

    def testCreationAttribGainedTimeScuttle(self):
        """ Test whether timeScuttle attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.timeScuttle)
        self.assertEqual(self.inst.timeScuttle,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckRuinType(self):
        """ Test whether check returns problems in ruinType correctly
        """
        self.mockRuin.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckEffect(self):
        """ Test whether check returns problems in effect correctly
        """
        self.mockEffect.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckSound(self):
        """ Test whether check returns problems in sound correctly
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.audio.checkEffect.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mockProb])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################## Tests Planet #############################"""

class PlanetFixtures(CelestialBodyFixtures,HasShieldVisualsFixtures,
        unit.TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.filepath = "Planet.entity"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entityType = {"identifier":"entityType","val":"Planet"}
        cls.meshes = {
            "counter":{"identifier":"meshInfoCount","val":1},
            "elements":[
                {
                    "identifier":"meshInfo",
                    "templateAsteroid":{"identifier":"asteroidTemplate",
                        "val":"AstTemp"},
                    "templateDustCloud":{"identifier":"dustCloudTemplate",
                        "val":"CloudTemp"},
                    "mesh":{"identifier":"meshName","val":"testmesh"},
                    "colorCloud":{"identifier":"cloudColor","val":"0"},
                    "nullMeshRadius":{"identifier":"nullMeshRadius",
                        "val":3000.0},
                    "nullMeshEff":{"identifier":"nullMeshParticleEffect",
                        "val":"testparticle"},
                    "iconMainUndetected":{
                        "identifier":"undetectedMainViewIcon",
                        "val":"undetected"},
                    "prefer":{"identifier":"shouldPrefer","val":True},
                    "nameString":{"identifier":"typeNameStringID",
                        "val":"namestring"},
                    "iconMainView":{"identifier":"mainViewIcon",
                        "val":"mainviewicon"},
                    "iconHud":{"identifier":"hudIcon","val":"hudicon"},
                    "iconInfocard":{"identifier":"infoCardIcon",
                        "val":"infocardicon"},
                    "iconHudSmall":{"identifier":"smallHudIcon",
                        "val":"smallhudicon"},
                    "pic":{"identifier":"picture","val":"picture"}
                }
            ]
        }
        cls.colorGlow = {"identifier":"glowColor","val":"0"}
        cls.colorRing = {"identifier":"ringColor","val":"0"}
        cls.textureCloud = {"identifier":"cloudLayerTextureName","val":"cloud"}
        cls.resourceSetup = {
            "identifier":"planetResourceSetupInfo",
            "asteroidSpawnAngle":{"identifier":"asteroidSpawnAngleVariance",
                "val":3.14},
            "maxAsteroids":{"identifier":"totalMaxResourceAsteroids",
                "val":2},
            "asteroidsMetal":{
                "identifier":"metalResourceAsteroidSetup",
                "countMin":{"identifier":"minCount","val":2},
                "countMax":{"identifier":"maxCount","val":4},
                "rateExtract":{"identifier":"extractionRate","val":20.5},
                "rateRefinery":{"identifier":"refineryRate","val":40.5},
                "refineriesMax":{"identifier":"maxRefineryCount","val":3}
            },
            "asteroidsCrystal":{
                "identifier":"crystalResourceAsteroidSetup",
                "countMin":{"identifier":"minCount","val":0},
                "countMax":{"identifier":"maxCount","val":2},
                "rateExtract":{"identifier":"extractionRate","val":10.5},
                "rateRefinery":{"identifier":"refineryRate","val":30.5},
                "refineriesMax":{"identifier":"maxRefineryCount","val":3}
            },
            "asteroidsMetalNeutral":{
                "identifier":"neutralMetalResourceAsteroidSetup",
                "countMin":{"identifier":"minCount","val":0},
                "countMax":{"identifier":"maxCount","val":0},
                "rateExtract":{"identifier":"extractionRate","val":0.0},
                "rateRefinery":{"identifier":"refineryRate","val":0.0},
                "refineriesMax":{"identifier":"maxRefineryCount","val":0}
            },
            "asteroidsCrystalNeutral":{
                "identifier":"neutralCrystalResourceAsteroidSetup",
                "countMin":{"identifier":"minCount","val":0},
                "countMax":{"identifier":"maxCount","val":0},
                "rateExtract":{"identifier":"extractionRate","val":0.0},
                "rateRefinery":{"identifier":"refineryRate","val":0.0},
                "refineriesMax":{"identifier":"maxRefineryCount","val":2}
            }
        }
        cls.isAsteroid = {"identifier":"isAsteroid","val":True}
        cls.healthRegen = {"identifier":"healthRegenRate","val":6.0}
        cls.upgrades = {
            "identifier":"planetUpgradeDef",
            "population":{
                "identifier":"path:Population",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "popMax":{"identifier":"maxPopulation","val":10.0},
                        "popGrowth":{"identifier":"populationGrowthRate",
                            "val":0.02},
                        "taxPenalty":{"identifier":"developmentTaxPenalty",
                            "val":-2.0}
                    }
                ]
            },
            "civilian":{
                "identifier":"path:CivilianModules",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "maxSlots":{"identifier":"maxModuleSlotCount:Civilian",
                            "val":15.0},
                        "maxConstructors":{
                            "identifier":"maxModuleConstructorCount",
                            "val":2}
                    }
                ]
            },
            "tactical":{
                "identifier":"path:TacticalModules",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "maxSlots":{"identifier":"maxModuleSlotCount:Tactical",
                            "val":15.0},
                        "maxConstructors":{
                            "identifier":"maxModuleConstructorCount",
                            "val":2}
                    }
                ]
            },
            "home":{
                "identifier":"path:Home",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "isHome":{"identifier":"isHomePlanet","val":False},
                        "taxBonus":{"identifier":"homePlanetTaxRateBonus",
                            "val":0.0},
                        "metalBonus":{
                            "identifier":"homePlanetMetalIncomeBonus",
                            "val":0.0},
                        "crystalBonus":{
                            "identifier":"homePlanetCrystalIncomeBonus",
                            "val":0.0}
                    }
                ]
            },
            "artifact":{
                "identifier":"path:ArtifactLevel",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0}
                    }
                ]
            },
            "infrastructure":{
                "identifier":"path:Infrastructure",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "maxHealth":{"identifier":"maxHealth","val":1000.0}
                    }
                ]
            },
            "social":{
                "identifier":"path:Social",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "popMax":{"identifier":"maxPopulation","val":0.0},
                        "popGrowth":{"identifier":"populationGrowthRate",
                            "val":0.0},
                        "tradeMod":{"identifier":"tradeIncomeModifier",
                            "val":0.0},
                        "shipBuildMod":{"identifier":"shipBuildRateModifier",
                            "val":0.0},
                        "cultureSpreadMod":{
                            "identifier":"cultureSpreadRateModifier",
                            "val":0.0}
                    }
                ]
            },
            "industry":{
                "identifier":"path:Industry",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "popMax":{"identifier":"maxPopulation","val":0.0},
                        "tradeMod":{"identifier":"tradeIncomeModifier",
                            "val":0.0},
                        "shipBuildMod":{"identifier":"shipBuildRateModifier",
                            "val":0.0},
                        "cultureSpreadMod":{
                            "identifier":"cultureSpreadRateModifier",
                            "val":0.0}
                    }
                ]
            },
            "smuggler":{
                "identifier":"path:Smuggler",
                "counter":{"identifier":"stageCount","val":1},
                "elements":[
                    {
                        "identifier":"stage",
                        "price":{
                            "identifier":"price",
                            "credits":{"identifier":"credits","val":0.0},
                            "metal":{"identifier":"metal","val":0.0},
                            "crystal":{"identifier":"crystal","val":0.0}
                        },
                        "upgTime":{"identifier":"upgradeTime","val":0.0},
                        "corruption":{"identifier":"corruptionPercent",
                            "val":0.25},
                        "smuggling":{"identifier":"smugglingPercent","val":0.5}
                    }
                ]
            }
        }
        cls.chanceRings = {"identifier":"ringsChance","val":0.0}
        cls.isColonizable = {"identifier":"isColonizable","val":True}
        cls.researchType = {"identifier":"planetTypeForResearch",
            "val":"Asteroid"}
        cls.skyboxScalars = {
            "counter":{"identifier":"skyboxScalarsCount","val":1},
            "elements":[
                {
                    "identifier":"skyboxScalars",
                    "diffuse":{"identifier":"diffuseScalar","val":1.0},
                    "ambient":{"identifier":"ambientScalar","val":1.0}
                }
            ]
        }
        cls.planetBonusesReq = {
            "counter":{"identifier":"requiredPlanetBonusesCount","val":1},
            "elements":[
                {"identifier":"bonus","val":"reqbonus"}
            ]
        }
        cls.planetBonusesRand = {
            "counter":{"identifier":"possibleRandomPlanetBonusesCount",
                "val":1},
            "elements":[
                {"identifier":"bonus","val":"randbonus"}
            ]
        }
        cls.sndAmbience = {"identifier":"ambienceSoundID","val":"ambsnd"}
        cls.renderVolcanic = {"identifier":"renderAsVolcanic","val":False}
        cls.ruin = {
            "identifier":"planetRuinDef",
            "ruinType":{"identifier":"ruinPlanetType","val":"testruin"},
            "attachType":{"identifier":"attachType","val":"Center"},
            "effect":{"identifier":"effectName","val":"testeffect"},
            "timeStart":{"identifier":"startTime","val":9.0},
            "sound":{"identifier":"soundID","val":"testsound"},
            "gainedCredits":{"identifier":"creditsOnStrippedToTheCore",
                "val":2000.0},
            "gainedMetal":{"identifier":"metalOnStrippedToTheCore",
                "val":750.0},
            "gainedCrystal":{"identifier":"crystalOnStrippedToTheCore",
                "val":250.0},
            "timeScuttle":{"identifier":"extraScuttleTimeOnStrip","val":0.0}
        }
        cls.dlcID = {"identifier":"dlcId","val":204880}

class PlanetTestsGeneral(PlanetFixtures):
    desc = "Tests Planet General:"

    def setUp(self):
        self.inst = orbitbo.Planet(
                filepath=self.filepath,
                entityType=self.entityType,
                meshes=self.meshes,
                zoomDist=self.zoomDist,
                colorGlow=self.colorGlow,
                colorRing=self.colorRing,
                textureCloud=self.textureCloud,
                resourceSetup=self.resourceSetup,
                isAsteroid=self.isAsteroid,
                healthRegen=self.healthRegen,
                upgrades=self.upgrades,
                chanceRings=self.chanceRings,
                isColonizable=self.isColonizable,
                researchType=self.researchType,
                skyboxScalars=self.skyboxScalars,
                planetBonusesReq=self.planetBonusesReq,
                planetBonusesRand=self.planetBonusesRand,
                sndAmbience=self.sndAmbience,
                renderVolcanic=self.renderVolcanic,
                ruin=self.ruin,
                dlcID=self.dlcID,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                moveArea=self.moveArea,
                hyperspaceExit=self.hyperspaceExit,
                wormhole=self.wormhole,
                maxStarbases=self.maxStarbases,
                maxMines=self.maxMines,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = orbitbo.Planet.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problems are returned correctly
        """
        res = orbitbo.Planet.createInstance("PlanetMalformed.entity")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribMeshes(self):
        """ Test whether meshes attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.MeshInfoPlanet,**self.meshes)
        self.assertEqual(self.inst.meshes,exp)

    def testCreationAttribColorGlow(self):
        """ Test whether colorGlow attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.colorGlow)
        self.assertEqual(self.inst.colorGlow,exp)

    def testCreationAttribColorRing(self):
        """ Test whether colorRing attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.colorRing)
        self.assertEqual(self.inst.colorRing,exp)

    def testCreationAttribTextureCloud(self):
        """ Test whether textureCloud attribute is created correctly
        """
        exp = ui.UITextureRef(canBeEmpty=True,**self.textureCloud)
        self.assertEqual(self.inst.textureCloud,exp)

    def testCreationAttribResourceSetup(self):
        """ Test whether resourceSetup attribute is created correctly
        """
        exp = orbitbo.ResourceSetup(**self.resourceSetup)
        self.assertEqual(self.inst.resourceSetup,exp)

    def testCreationAttribIsAsteroid(self):
        """ Test whether isAsteroid attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isAsteroid)
        self.assertEqual(self.inst.isAsteroid,exp)

    def testCreationAttribHealthRegen(self):
        """ Test whether healthRegen attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.healthRegen)
        self.assertEqual(self.inst.healthRegen,exp)

    def testCreationAttribUpgrades(self):
        """ Test whether upgrades attribute is created correctly
        """
        exp = orbitbo.PlanetUpgrades(**self.upgrades)
        self.assertEqual(self.inst.upgrades,exp)

    def testCreationAttribChanceRings(self):
        """ Test whether chanceRings attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.chanceRings)
        self.assertEqual(self.inst.chanceRings,exp)

    def testCreationAttribIsColonizable(self):
        """ Test whether isColonizable attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isColonizable)
        self.assertEqual(self.inst.isColonizable,exp)

    def testCreationAttribResearchType(self):
        """ Test whether researchType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=coe.PLANETTEYPE_FOR_RESEARCH,
                **self.researchType)
        self.assertEqual(self.inst.researchType,exp)

    def testCreationAttribSkyboxScalars(self):
        """ Test whether skyboxScalars attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=orbitbo.SkyboxScalar,**self.skyboxScalars)
        self.assertEqual(self.inst.skyboxScalars,exp)

    def testCreationAttribPlanetBonusesReq(self):
        """ Test whether planetBonusesReq attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["PlanetBonus"]},
                **self.planetBonusesReq)
        self.assertEqual(self.inst.planetBonusesReq,exp)

    def testCreationAttribPlanetBonusesRand(self):
        """ Test whether planetBonusesRand attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["PlanetBonus"]},
                **self.planetBonusesRand)
        self.assertEqual(self.inst.planetBonusesRand,exp)

    def testCreationAttribSndAmbience(self):
        """ Test whether sndAmbience attribute is created correctly
        """
        exp = audio.SoundRef(canBeEmpty=True,**self.sndAmbience)
        self.assertEqual(self.inst.sndAmbience,exp)

    def testCreationAttribRenderVolcanic(self):
        """ Test whether renderVolcanic attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.renderVolcanic)
        self.assertEqual(self.inst.renderVolcanic,exp)

    def testCreationAttribRuin(self):
        """ Test whether ruin attribute is created correctly
        """
        exp = orbitbo.PlanetRuin(**self.ruin)
        self.assertEqual(self.inst.ruin,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class PlanetTestsChecking(PlanetFixtures):
    desc = "Tests Planet Checking:"

    def setUp(self):
        self.inst = orbitbo.Planet(
                filepath=self.filepath,
                entityType=self.entityType,
                meshes=self.meshes,
                zoomDist=self.zoomDist,
                colorGlow=self.colorGlow,
                colorRing=self.colorRing,
                textureCloud=self.textureCloud,
                resourceSetup=self.resourceSetup,
                isAsteroid=self.isAsteroid,
                healthRegen=self.healthRegen,
                upgrades=self.upgrades,
                chanceRings=self.chanceRings,
                isColonizable=self.isColonizable,
                researchType=self.researchType,
                skyboxScalars=self.skyboxScalars,
                planetBonusesReq=self.planetBonusesReq,
                planetBonusesRand=self.planetBonusesRand,
                sndAmbience=self.sndAmbience,
                renderVolcanic=self.renderVolcanic,
                ruin=self.ruin,
                dlcID=self.dlcID,
                shieldMesh=self.shieldMesh,
                shieldRender=self.shieldRender,
                moveArea=self.moveArea,
                hyperspaceExit=self.hyperspaceExit,
                wormhole=self.wormhole,
                maxStarbases=self.maxStarbases,
                maxMines=self.maxMines,
                shadowMin=self.shadowMin,
                shadowMax=self.shadowMax,
                abilities=self.abilities
        )
        self.mockProb = mock.MagicMock(probFile=None)
        self.mockProb.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        self.mmod = tco.genMockMod("./")
        self.mmod.brushes.checkBrush.return_value = []
        self.mmod.strings.checkString.return_value = []
        patcher = mock.patch.object(self.inst.meshes,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcher.start()
        patcher = mock.patch.object(self.inst.textureCloud,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockCloud = patcher.start()
        patcher = mock.patch.object(self.inst.planetBonusesReq,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockBonusReq = patcher.start()
        patcher = mock.patch.object(self.inst.planetBonusesRand,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockBonusRand = patcher.start()
        patcher = mock.patch.object(self.inst.sndAmbience,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockAmbience = patcher.start()
        patcher = mock.patch.object(self.inst.ruin,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockRuin = patcher.start()
        patcher = mock.patch.object(self.inst.shieldMesh,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        for ab in self.inst.abilities:
            patcher = mock.patch.object(ab,"check",autospec=True,spec_set=True,
                    return_value=[])
            patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMeshes(self):
        """ Test whether prolems in meshes attribute are returned
        """
        self.mockMesh.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockMesh.assert_called_once_with(self.mmod,False)

    def testCheckCloud(self):
        """ Test whether prolems in textureCloud attribute are returned
        """
        self.mockCloud.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockCloud.assert_called_once_with(self.mmod,False)

    def testCheckBonusReq(self):
        """ Test whether prolems in planetBonusesReq attribute are returned
        """
        self.mockBonusReq.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockBonusReq.assert_called_once_with(self.mmod,False)

    def testCheckBonusRand(self):
        """ Test whether prolems in planetBonusesRand attribute are returned
        """
        self.mockBonusRand.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockBonusRand.assert_called_once_with(self.mmod,False)

    def testCheckAmbience(self):
        """ Test whether prolems in sndAmbience attribute are returned
        """
        self.mockAmbience.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockAmbience.assert_called_once_with(self.mmod,False)

    def testCheckRuin(self):
        """ Test whether prolems in ruin attribute are returned
        """
        self.mockRuin.return_value = [self.mockProb]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockProb])
        self.mockRuin.assert_called_once_with(self.mmod,False)
