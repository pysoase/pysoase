""" Tests for the pysoase.common.problems module
"""
import pytest

import pysoase.common.problems as co_probs

testdata = "common"

"""########################## Tests ProblemSeverity ########################"""

class TestProblemSeverity:
    desc = "Tests ProblemSeverity:"

    def testWarn(self):
        """ Test whether "warn" returns the warn severity level
        """
        res = co_probs.ProblemSeverity.toSeverity("warn")
        assert res == co_probs.ProblemSeverity.warn

    def testInfo(self):
        """ Test whether "info" returns the info severity level
        """
        res = co_probs.ProblemSeverity.toSeverity("info")
        assert res == co_probs.ProblemSeverity.info

    def testError(self):
        """ Test whether "error" returns the error severity level
        """
        res = co_probs.ProblemSeverity.toSeverity("error")
        assert res == co_probs.ProblemSeverity.error

"""############################# Tests Problem #############################"""

class TestProblem:
    desc = "Tests Problem:"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_probs.Problem(severity=co_probs.ProblemSeverity.warn)

    def testInvalidSeverity(self):
        """ Test whether invalid severity value produces error
        """
        with pytest.raises(ValueError):
            co_probs.Problem(severity="bar")

    def testEq(self):
        """ Test whether equality operator works
        """
        inst1 = co_probs.Problem(severity=co_probs.ProblemSeverity.warn)
        assert self.inst == inst1

    def testEqUnequal(self):
        """ Test whether equality operator recognizes unequal classes
        """
        inst1 = co_probs.BasicProblem(desc="Hello",
                severity=co_probs.ProblemSeverity.warn)
        assert self.inst != inst1

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        exp = "WARN: "
        assert self.inst.toString(0) == exp

    def testToStringLineNum(self):
        """ Test whether string with line number is constructed correctly
        """
        exp = "WARN:l 12: "
        inst1 = co_probs.Problem(severity=co_probs.ProblemSeverity.warn,
                probLine=12)
        assert inst1.toString(0) == exp

"""########################### Tests BasicProblem ##########################"""

class TestBasicProblem:
    desc = "Tests BasicProblem:"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_probs.BasicProblem("Foo Bar",
                severity=co_probs.ProblemSeverity.warn)

    def testEq(self):
        """ Test whether two equal instances test equal
        """
        inst1 = co_probs.BasicProblem("Foo Bar",
                severity=co_probs.ProblemSeverity.warn)
        assert self.inst == inst1

    def testEqUnequal(self):
        """ Test whether two instances differing in desc test unequal
        """
        inst1 = co_probs.BasicProblem("Hello, World",
                severity=co_probs.ProblemSeverity.warn)
        assert self.inst != inst1

"""######################## Tests UsingRebFileProblem ######################"""

class TestUsingRebFileProblem:
    desc = "Tests UsingRebFileProblem:"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.filepath = "foo.bar"
        self.inst = co_probs.UsingRebFileProblem(filepath=self.filepath)

    def testSeverity(self):
        """ Test whether severity is set to info
        """
        assert self.inst.severity == co_probs.ProblemSeverity.info

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        exp = ("INFO: Could not find \'foo.bar\' in Mod. Using vanilla file "
               "instead.\n")
        assert self.inst.toString(0) == exp

    def testEq(self):
        """ Test whether similar instances test equal
        """
        inst1 = co_probs.UsingRebFileProblem(filepath=self.filepath)
        assert inst1 == self.inst

    def testEqUnequal(self):
        """ Test whether two unequal instances test unequal
        """
        inst1 = co_probs.UsingRebFileProblem(filepath="bar.baz")
        assert inst1 != self.inst

"""######################## Tests DuplicateEntryProb #######################"""

class TestDuplicateEntryProb:
    desc = "Tests for DuplicateEntryProb:"
    _msg = "WARN: {} duplicate entries were found for {} in {}.\n"

    def testCountTooSmall(self):
        """ Test whether correct error is raised for count<1
        """
        with pytest.raises(ValueError) as cm:
            co_probs.DuplicateEntryProb(0,"test","testList")
        assert str(cm.value) == "Count must be larger than 0."

    def testOutputCountFloat(self):
        """ Test whether float is correctly converted to int
        """
        prob = co_probs.DuplicateEntryProb(1.5,"test","testList")
        assert prob.toString(0) == self._msg.format(1,"test","testList")

    def testOutput(self):
        """ Test whether toString produces correct string
        """
        prob = co_probs.DuplicateEntryProb(2,"test","testList")
        assert prob.toString(0) == self._msg.format(2,"test","testList")

    def testEquality(self):
        """ Test whether two similar instances test as equal
        """
        prob1 = co_probs.DuplicateEntryProb(2,"foo","barlst")
        prob2 = co_probs.DuplicateEntryProb(2,"foo","barlst")
        assert prob1 == prob2

    def testInequalityCount(self):
        """ Test whether two instances differing in count test as unequal
        """
        prob1 = co_probs.DuplicateEntryProb(2,"foo","barlst")
        prob2 = co_probs.DuplicateEntryProb(3,"foo","barlst")
        assert prob1 != prob2

    def testInequalityKey(self):
        """ Test whether two instances differing in key test as unequal
        """
        prob1 = co_probs.DuplicateEntryProb(3,"foo","barlst")
        prob2 = co_probs.DuplicateEntryProb(3,"baz","barlst")
        assert prob1 != prob2

    def testInequalityLst(self):
        """ Test whether two instances differing in list name test as unequal
        """
        prob1 = co_probs.DuplicateEntryProb(3,"foo","barlst")
        prob2 = co_probs.DuplicateEntryProb(3,"foo","bazlst")
        assert prob1 != prob2

"""######################## Tests MissingRefProblem ########################"""

class TestMissingRefProblem:
    desc = "Tests for MissingRefProblem:"
    _msg = "ERROR: Referenced {0} \'{1}\' could not be resolved.\n"

    def testOutput(self):
        """ Test whether toString produces correct string
        """
        prob = co_probs.MissingRefProblem("File","Test.str")
        assert prob.toString(0) == self._msg.format("File","Test.str")

    def testEquality(self):
        """ Test whether two similar instances test as equal
        """
        prob1 = co_probs.MissingRefProblem("File","Test.str")
        prob2 = co_probs.MissingRefProblem("File","Test.str")
        assert prob1 == prob2

    def testInequalityRef(self):
        """ Test whether two instances differing in reference test as unequal
        """
        prob1 = co_probs.MissingRefProblem("File","Test2.str")
        prob2 = co_probs.MissingRefProblem("File","Test.str")
        assert prob1 != prob2

    def testInequalityKey(self):
        """ Test whether two instances differing in refdesc test as unequal
        """
        prob1 = co_probs.MissingRefProblem("String","Test.str")
        prob2 = co_probs.MissingRefProblem("File","Test.str")
        assert prob1 != prob2
