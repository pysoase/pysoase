""" Tests for the pysoase.common.filehandling module
"""
import os
from unittest import mock

import pytest

import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
import pysoase.common.problems as co_probs
import pysoase.tests.conftest as tco

testdata = "common"

class TestFileHandling:
    desc = "Tests file handling:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.rebpath = os.path.abspath("./comp_reb")
        cls.subdir = "GameInfo"
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebpath)

    @pytest.fixture()
    def fclassFixture(self):
        self.mockInst = mock.create_autospec(co_basics.IsFile)
        self.fclass = mock.create_autospec(co_basics.IsFile,spec_set=True)
        self.fclass.createInstance.return_value = self.mockInst

    def testCheckFileExExists(self):
        """ Test whether checkFileExistence returns empty when file exists
        """
        fsource,probs = co_files.checkFileExistence("GameInfo",
                "testfile",self.mmod)
        assert fsource == co_files.FileSource.mod
        assert probs == []

    def testCheckFileExRebFile(self):
        """ Test whether checkFileExistence returns rebProb with vanilla file
        """
        fsource,probs = co_files.checkFileExistence("Particle",
                "Settlers.particle",self.mmod)
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.UsingRebFileProblem)
        assert fsource == co_files.FileSource.vanilla

    def testCheckFileExRebRefFile(self):
        """ Test whether checkFileExistence returns reb ref over vanilla
        """
        fsource,probs = co_files.checkFileExistence("GameInfo",
                "Debris.entity",self.mmod)
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.UsingRebFileProblem)
        assert fsource == co_files.FileSource.rebref

    def testCheckFileExBundledFile(self):
        """ Test whether checkFileExistence returns bundled over vanilla
        """
        lmod = tco.genMockMod(self.modpath)
        fsource,probs = co_files.checkFileExistence("GameInfo",
                "Explosions.explosiondata",lmod)
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.UsingRebFileProblem)
        assert fsource == co_files.FileSource.bundled

    def testCheckFileExMissing(self):
        """ Test whether checkFileExistence returns Prob with missing file
        """
        fsource,probs = co_files.checkFileExistence("GameInfo","missing",
                self.mmod)
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.MissingRefProblem)
        assert fsource == co_files.FileSource.missing

    def testCheckFileExNoSub(self):
        """ Test whether checkFileExistence works without subdir
        """
        fsource,probs = co_files.checkFileExistence("","testfile3",self.mmod)
        assert fsource == co_files.FileSource.mod
        assert probs == []

    def testCheckFileExNoReb(self):
        """ Test whether checkFileExistence works without rebdir
        """
        noRebMod = tco.genMockMod("./comp")
        fsource,probs = co_files.checkFileExistence("GameInfo","testfile2",
                noRebMod)
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.MissingRefProblem)
        assert fsource == co_files.FileSource.missing

    def testFromMod(self,fclassFixture):
        """ Test whether loading file from mod dir works
        """
        res,probs = co_files.loadFile(self.subdir,"testfile",self.mmod,
                self.fclass)
        assert res == self.mockInst
        assert probs == []
        self.fclass.createInstance.assert_called_once_with(
                os.path.join(self.modpath,self.subdir,"testfile"))

    def testLoadingProb(self,fclassFixture):
        """ Test whether loading problems are returned correctly
        """
        myProb = mock.MagicMock(spec=co_probs.BasicProblem,probFile=None)
        self.fclass.createInstance.return_value = [myProb]
        res,probs = co_files.loadFile(self.subdir,"testfile",self.mmod,
                self.fclass)
        assert res is None
        assert probs == [myProb]
        self.fclass.createInstance.assert_called_once_with(
                os.path.join(self.modpath,self.subdir,"testfile"))

    def testFromRebref(self,fclassFixture):
        """ Test whether loading file from rebref dir works
        """
        res,probs = co_files.loadFile("testsub","testfile2",self.mmod,
                self.fclass)
        assert res == self.mockInst
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.UsingRebFileProblem)
        self.fclass.createInstance.assert_called_once_with(
                os.path.join(self.rebpath,"testsub","testfile2"))

    def testFromBundled(self,fclassFixture):
        """ Test whether loading file from bundled dir works
        """
        lmod = tco.genMockMod(self.modpath)
        res,probs = co_files.loadFile(self.subdir,"Explosions.explosiondata",
                lmod,self.fclass)
        assert res == self.mockInst
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.UsingRebFileProblem)
        self.fclass.createInstance.assert_called_once_with(
                os.path.join(co_files.BUNDLE_PATH,
                        self.subdir,"Explosions.explosiondata"))

    def testMissingFile(self,fclassFixture):
        """ Test whether loading missing file works
        """
        res,probs = co_files.loadFile(self.subdir,"missing",self.mmod,
                self.fclass)
        assert res is None
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.MissingRefProblem)
