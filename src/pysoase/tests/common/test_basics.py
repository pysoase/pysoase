import os
from unittest import mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.filehandling as co_files
import pysoase.common.basics as co_basics
import pysoase.common.problems as co_probs
import pysoase.tests.conftest as tco
import pysoase.tools.modify as modify

testdata = "common"

"""########################### Tests ModComponent ##########################"""

@mock.patch.object(co_basics.ModComponent,"__abstractmethods__",set())
@mock.patch.object(co_files,"VANILLA_MANIFEST",{"testsub":["foo.ctest",
    "bar.ctest","baz.ctest"]})
class TestModComponent:
    desc = "Tests ModComponent:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.rebpath = os.path.abspath("./comp_reb")
        cls.ext = "ctest"
        cls.subdir = "testsub"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.mmod = tco.genMockMod(self.modpath)

    def testModFiles(self):
        """ Test whether component files in mod dir are gathered
        """
        comp = co_basics.ModComponent(self.mmod,self.subdir,self.ext)
        exp = {"foo.ctest":None,"bar.ctest":None,"baz.ctest":None}
        assert comp.compFilesMod == exp

    def testNoCompFileExt(self):
        """ No comp file extension leads to empty file lists
        """
        comp = co_basics.ModComponent(self.mmod,self.subdir,"")
        assert comp.compFilesMod == {}
        assert comp.compFilesVanilla == {}

    def testModFilesMissing(self):
        """ Test whether component files are empty with missing directory
        """
        comp = co_basics.ModComponent(self.mmod,"missing",self.ext)
        exp = {}
        assert comp.compFilesMod == exp

    def testVanillaFiles(self):
        """ Test whether component files in vanilla dir are set to problem
        """
        comp = co_basics.ModComponent(self.mmod,self.subdir,self.ext)
        exp = {"foo.ctest":[co_probs.UsingRebFileProblem("foo.ctest")],
            "bar.ctest":[co_probs.UsingRebFileProblem("bar.ctest")],
            "baz.ctest":[co_probs.UsingRebFileProblem("baz.ctest")]}
        assert comp.compFilesVanilla == exp

    def testVanillaFilesRef(self):
        """ Test whether component files in rebref dir are set to None
        """
        self.mmod = tco.genMockMod(self.modpath,rebrefpath=self.rebpath)
        comp = co_basics.ModComponent(self.mmod,self.subdir,self.ext)
        exp = {"foo.ctest":None,"bar.ctest":None,"baz.ctest":None}
        assert comp.compFilesMod == exp

    def testCheckFileExistence(self):
        """ Test whether delegation of checkFileExistence works
        """
        with mock.patch("pysoase.common.filehandling.checkFileExistence",
                autospec=True,spec_set=True,
                return_value=(co_files.FileSource.mod,[])
                ) as mocked:
            inst = co_basics.ModComponent(self.mmod,"testsub","ctest")
            fsource,probs = inst._checkFileExistence("testsub","testfile")
            assert fsource == co_files.FileSource.mod
            assert probs == []
            mocked.assert_called_once_with("testsub","testfile",self.mmod)

"""############################ Tests Checkable ############################"""

@mock.patch.object(co_basics.Checkable,"__abstractmethods__",set())
class TestCheckable:
    desc = "Tests Checkable:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_basics.Checkable()
        self.attrib1 = mock.create_autospec(co_basics.Checkable)
        self.attrib1.check.return_value = []
        self.inst.attrib1 = self.attrib1
        self.attrib2 = mock.create_autospec(co_basics.Checkable)
        self.attrib2.check.return_value = []
        self.inst.attrib2 = self.attrib2

    def testAttribCheckAll(self):
        """ Test whether attribCheck checks all attributes
        """
        self.inst.check(self.mmod)
        self.attrib1.check.assert_called_once_with(self.mmod,False)
        self.attrib2.check.assert_called_once_with(self.mmod,False)

    def testAttribCheckProblems(self):
        """ Test whether attribCheck returns correct problems
        """
        self.attrib1.check.return_value = [mock.sentinel.prob1]
        self.attrib2.check.return_value = [mock.sentinel.prob2]
        res = self.inst.check(self.mmod)
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res

    def testAttribCheckDuplicateProblems(self):
        """ Test whether attribCheck removes duplicate problems
        """
        self.attrib1.check.return_value = [mock.sentinel.prob1,
            mock.sentinel.prob2]
        self.attrib2.check.return_value = [mock.sentinel.prob1]
        res = self.inst.check(self.mmod)
        assert len(res) == 2
        assert mock.sentinel.prob1 in res

    def testCaching(self):
        """ Test whether checks are only conducted once and use cache
        """
        self.attrib1.check.return_value = [mock.sentinel.prob1,
            mock.sentinel.prob2]
        self.attrib2.check.return_value = [mock.sentinel.prob1]
        self.inst.check(self.mmod)
        self.inst.check(self.mmod)
        self.attrib1.check.assert_called_once_with(self.mmod,False)
        self.attrib2.check.assert_called_once_with(self.mmod,False)

    def testIgnoring(self):
        """ Test whether checks are not conducted when ignored
        """
        self.attrib1.check.return_value = [mock.sentinel.prob1,
            mock.sentinel.prob2]
        self.attrib2.check.return_value = [mock.sentinel.prob1]
        self.inst.ignore = True
        res = self.inst.check(self.mmod)
        assert not self.attrib1.check.called
        assert not self.attrib2.check.called
        assert res == []

    def testCachingAttribCheck(self):
        """ Test whether attribCheck is only called once and uses cache
        """
        self.attrib1.check.return_value = [mock.sentinel.prob1,
            mock.sentinel.prob2]
        self.attrib2.check.return_value = [mock.sentinel.prob1]
        self.inst.attribCheck(self.mmod)
        self.inst.attribCheck(self.mmod)
        self.attrib1.check.assert_called_once_with(self.mmod,False)
        self.attrib2.check.assert_called_once_with(self.mmod,False)

"""########################### Tests HasReference ##########################"""

class TestHasReference:
    desc = "Tests HasReference:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_basics.HasReference()
        self.aref1 = mock.create_autospec(co_attribs.AttribRef,spec_set=True)
        self.aref1.check.return_value = []
        self.inst.aref1 = self.aref1
        self.aref2 = mock.create_autospec(co_attribs.AttribRef,spec_set=True)
        self.aref2.check.return_value = []
        self.inst.aref2 = self.aref2
        self.hasRef1 = mock.create_autospec(co_basics.HasReference,spec_set=True)
        self.hasRef1.checkRefs.return_value = []
        self.hasRef1.getRefs.return_value = [mock.sentinel.ref1]
        self.inst.hasRef1 = self.hasRef1
        self.hasRef2 = mock.create_autospec(co_basics.HasReference,spec_set=True)
        self.hasRef2.checkRefs.return_value = []
        self.hasRef2.getRefs.return_value = [mock.sentinel.ref2]
        self.inst.hasRef2 = self.hasRef2

    def testDirectRefs(self):
        """ Test whether directRefs returns the correct references
        """
        res = self.inst.directRefs()
        assert len(res) == 2
        assert self.aref1 in res
        assert self.aref2 in res

    def testAttribRefs(self):
        """ Test whether attribRefs returns the correct references
        """
        res = self.inst.attribRefs()
        assert len(res) == 2
        assert mock.sentinel.ref1 in res
        assert mock.sentinel.ref2 in res

    def testGetRefs(self):
        """ Test whether getRefs returns the correct references
        """
        res = self.inst.getRefs()
        assert len(res) == 4
        assert mock.sentinel.ref1 in res
        assert mock.sentinel.ref2 in res
        assert self.aref1 in res
        assert self.aref2 in res

    def testCheckDirectRefs(self):
        """ Test whether checkDirectRefs returns correct problems
        """
        self.aref1.check.return_value = [mock.sentinel.prob1]
        self.aref2.check.return_value = [mock.sentinel.prob2]
        res = self.inst.checkDirectRefs(self.mmod)
        res = [val for val in res]
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res
        assert not self.hasRef1.checkRefs.called
        assert not self.hasRef2.checkRefs.called

    def testCheckAttribRefs(self):
        """ Test whether checkAttribRefs returns correct problems
        """
        self.hasRef1.checkRefs.return_value = [mock.sentinel.prob1]
        self.hasRef2.checkRefs.return_value = [mock.sentinel.prob2]
        res = self.inst.checkAttribRefs(self.mmod)
        res = [val for val in res]
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res
        assert not self.aref1.check.called
        assert not self.aref2.check.called

    def testCheckRefs(self):
        """ Test whether checkRefs returns correct problems
        """
        self.hasRef1.checkRefs.return_value = [mock.sentinel.prob1]
        self.hasRef2.checkRefs.return_value = [mock.sentinel.prob2]
        self.aref1.check.return_value = [mock.sentinel.prob3]
        self.aref2.check.return_value = [mock.sentinel.prob4]
        res = self.inst.checkRefs(self.mmod)
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res
        assert mock.sentinel.prob3 in res
        assert mock.sentinel.prob4 in res

    def testCheckRefsCache(self):
        """ Test whether checkRefs caches correctly
        """
        self.hasRef1.checkRefs.return_value = [mock.sentinel.prob1]
        self.hasRef2.checkRefs.return_value = [mock.sentinel.prob2]
        self.aref1.check.return_value = [mock.sentinel.prob3]
        self.aref2.check.return_value = [mock.sentinel.prob4]
        self.inst.checkRefs(self.mmod)
        res = self.inst.checkRefs(self.mmod)
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res
        assert mock.sentinel.prob3 in res
        assert mock.sentinel.prob4 in res
        self.aref1.check.assert_called_once_with(self.mmod)
        self.aref2.check.assert_called_once_with(self.mmod)
        self.hasRef1.checkRefs.assert_called_once_with(self.mmod)
        self.hasRef2.checkRefs.assert_called_once_with(self.mmod)

"""############################## Tests IsFile #############################"""

@mock.patch.object(co_basics.IsFile,"__abstractmethods__",set())
class TestIsFile:
    desc = "Tests IsFile:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.mmod = tco.genMockMod("./")
        cls.filepath = "isfile/IsFile.f"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_basics.IsFile(self.filepath)

    def testInitMissingFile(self):
        """ Test whether creation with invalid file path raises error
        """
        with pytest.raises(FileNotFoundError):
            co_basics.IsFile("missing.f")

    def testCreateInstanceMissingFile(self):
        """ Test whether createInstance throws error with missing file
        """
        with pytest.raises(FileNotFoundError):
            co_basics.IsFile.createInstance("missing.f")

    def testParseMissingFile(self):
        """ Test whether parse throws error with missing file
        """
        with pytest.raises(FileNotFoundError):
            co_basics.IsFile.parse("missing.f")

    def testParse(self):
        """ Test whether parse returns correct result
        """
        succ,res = co_basics.IsFile.parse(self.filepath)
        assert succ

    def testParseMalformed(self):
        """ Test whether parse returns correct problem with malformed file
        """
        succ,res = co_basics.IsFile.parse("isfile/malformed.f")
        assert not succ
        assert isinstance(res[0],co_probs.ParseProblem)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == "TXT\n"

    def testWriteOutUnchanged(self):
        """ Test whether file is not written when the file is unchanged
        """
        self.inst.writeOut(filepath="isfile/foo.bar")
        assert not os.path.exists("isfile/foo.bar")

    def testWriteOutChanged(self):
        """ Test whether file is written when the file is changed
        """
        self.inst.changed = True
        self.inst.writeOut(filepath="isfile/foo.bar")
        assert os.path.exists("isfile/foo.bar")
        os.remove("isfile/foo.bar")

    def testCheck(self):
        """ Test whether check returns problems correctly
        """
        prob = co_probs.BasicProblem("Hello",severity=co_probs.ProblemSeverity.warn)
        with mock.patch("pysoase.common.basics.Checkable.check",autospec=True,
                spec_set=True,return_value=[prob]):
            res = self.inst.check(self.mmod)
            assert res == [prob]
            assert res[0].probFile == "IsFile.f"

@mock.patch.object(co_basics.IsFile,"__abstractmethods__",set())
class TestIsFileModify:
    desc = "Tests IsFile modify:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.filepath = "isfile/IsFile.f"
        cls.mods = [mock.sentinel.mod1,mock.sentinel.mod2]

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.testattrib1 = mock.create_autospec(
                co_attribs.Attribute,spec_set=True)
        self.testattrib1.modify.return_value = []
        self.testattrib2 = mock.create_autospec(
                co_attribs.Attribute,spec_set=True)
        self.testattrib2.modify.return_value = []
        self.inst = co_basics.IsFile(self.filepath)
        self.inst.attrib1 = self.testattrib1
        self.inst.attrib2 = self.testattrib2

    def testModAttribs(self):
        """ Modify method called for all sub attributes
        """
        self.inst.modify(self.mods)
        self.testattrib1.modify.assert_called_once_with(self.mods)
        self.testattrib2.modify.assert_called_once_with(self.mods)

    def testModReturnChanges(self):
        """ Modify method returns list of changes when a change was made
        """
        change1 = modify.ModificationChange("ident",1,2)
        change2 = modify.ModificationChange("ident",3,4)
        changes = [change1,change2]
        self.testattrib1.modify.return_value = [change1]
        self.testattrib2.modify.return_value = [change2]
        res = self.inst.modify(self.mods)
        assert len(res) == 2
        for c in res:
            assert c in changes

    def testModReturnFileSet(self):
        """ Modify method sets file on returned ModificationChanges
        """
        mod1 = modify.ModificationChange("ident",1,2)
        mod2 = modify.ModificationChange("ident",3,4)
        self.testattrib1.modify.return_value = [mod1]
        self.testattrib2.modify.return_value = [mod2]
        res = self.inst.modify(self.mods)
        for c in res:
            assert c.modFile == os.path.basename(self.filepath)

    def testModReturnNoChanges(self):
        """ Modify method returns empty list when no change has been made
        """
        res = self.inst.modify(self.mods)
        assert res == []

    def testChangedSet(self):
        """ Changed is True after modification
        """
        mod1 = modify.ModificationChange("ident",1,2)
        mod2 = modify.ModificationChange("ident",3,4)
        self.testattrib1.modify.return_value = [mod1]
        self.testattrib2.modify.return_value = [mod2]
        self.inst.modify(self.mods)
        assert self.inst.changed

    def testChangedNotSet(self):
        """ Changed is False when no modification occured
        """
        self.inst.modify(self.mods)
        assert not self.inst.changed