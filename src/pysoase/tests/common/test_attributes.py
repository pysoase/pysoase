""" Unit tests for the pysoase.common module.
"""

from unittest import mock

import pyparsing as pp
import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.tests.conftest as tco
import pysoase.tools.modify as modify
from pysoase.entities.ships import genVarLvlAttrib

testdata = "common"

"""############################ Tests AttribEnum ###########################"""

class TestAttribEnumBadValues:
    desc = "Test AttribEnum with bad inputs:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.possibles = ["foo","bar","baz"]

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.attrib = co_attribs.AttribEnum(identifier="Enum",val="hello",
                possibleVals=self.possibles)

    def testEmptyPossibles(self):
        """ Test whether empty 'possibles' parameter is handled correctly
        """
        with pytest.raises(ValueError) as cm:
            self.attrib = co_attribs.AttribEnum(identifier="Enum",val="hello",
                    possibleVals=[])
        assert str(cm.value) == "The list of possible values must not be empty"

    def testSetBadValue(self):
        """ Test whether the attempt to set invalid value raises an error
        """
        msg = ("The value 'world' is invalid. Possible values are: "
              "[\'foo\', \'bar\', \'baz\']")
        with pytest.raises(ValueError) as cm:
            self.attrib.value = "world"
        assert str(cm.value) == msg

    def testChecking(self):
        """ Test whether the list of problems returned from check is empty
        """
        exp = ("ERROR: The value \'hello\' is invalid. Possible values "
               "are: "+str(self.possibles)+"\n")
        res = self.attrib.check(None)
        assert len(res) == 1
        assert res[0].toString(0) == exp

class TestAttribEnumGoodValues:
    desc = "Test AttribEnum with good inputs:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.possibles = ["foo","bar","baz"]

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.attrib = co_attribs.AttribEnum(identifier="Enum",val="bar",
                possibleVals=self.possibles)

    def testSetValue(self):
        """ Test whether the attempt to set valid value works
        """
        self.attrib.value = "baz"
        assert self.attrib.value == "baz"

    def testChecking(self):
        """ Test whether the list of problems is empty
        """
        res = self.attrib.check(None)
        assert res == []

    def testOutput(self):
        """ Test whether the output is correctly formatted
        """
        exp = "Enum \"bar\"\n"
        assert self.attrib.toString(0) == exp

"""############################ Tests AttribList ###########################"""

class TestAttribListEmpty:
    desc = "Test AttribList empty:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":0},
            "identifier":"header"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.lst = co_attribs.AttribList(elemType=co_attribs.AttribNum,
                elemArgs=None,**self.vals)
        self.outHeading = "header\n\tcounter 0\n"

    def testCreation(self):
        """ Test whether the list is correctly created
        """
        newLst = co_attribs.AttribList(elemType=co_attribs.AttribNum,
                elemArgs=None,**self.vals)
        assert newLst.counter == 0
        assert len(newLst.elements) == 0

    def testCreationParse(self):
        """ Test whether list is created correctly from parse results
        """
        parseRes = co_parse.genListAttrib("counter",
                co_parse.genStringAttrib("foo"),
                "header"
        ).parseString(self.outHeading)[0]
        res = co_attribs.AttribList(elemType=co_attribs.AttribString,**parseRes)
        assert res == self.lst

    def testCheck(self):
        """ Test whether check returns the correct problems
        """
        assert self.lst.check("currMod") == []

    def testCheckRefs(self):
        """ Test whether reference checking returns the correct problems
        """
        assert self.lst.checkRefs(None) == []

    def testGetRefs(self):
        """ The whether references are correctly assembled and returned
        """
        assert self.lst.getRefs() == []

    def testLen(self):
        """ Test whether calling len() on an instance returns 0
        """
        assert len(self.lst) == 0

    def testOutputNoHeading(self):
        """ Test whether string output without a heading is correct
        """
        ident = self.vals["identifier"]
        del self.vals["identifier"]
        empty = co_attribs.AttribList(elemType=co_attribs.AttribNum,
                elemArgs=None,**self.vals)
        out = "counter 0\n"
        assert empty.toString(0) == out
        self.vals["identifier"] = ident

    def testOutputHeading(self):
        """ Test whether string output with headings is correct
        """
        assert self.lst.toString(0) == self.outHeading

class TestAttribListFull:
    desc = "Test AttribList full:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":3},
            "elements":[
                {"identifier":"numval","val":1},
                {"identifier":"numval","val":2},
                {"identifier":"numval","val":3}],
            "identifier":"heading"}
        cls.parseString = ("heading\n"
                    "\tcounter 3\n"
                    "\tnumval 1\n"
                    "\tnumval 2\n"
                    "\tnumval 3\n")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.lst = co_attribs.AttribList(elemType=co_attribs.AttribNum,
                elemArgs=None,**self.vals)

    def testCreationParse(self):
        """ Test whether list is created correctly from parse results
        """
        parseRes = co_parse.genListAttrib("counter",
                co_parse.genIntAttrib("numval"),
                "heading").parseString(self.parseString)[0]
        res = co_attribs.AttribList(elemType=co_attribs.AttribNum,**parseRes)
        assert res == self.lst

    def testElemType(self):
        """ Test whether the elements have the correct type
        """
        for elem in self.lst.elements:
            assert isinstance(elem,co_attribs.AttribNum)

    def testElemCount(self):
        """ Test whether the correct number of elements was created
        """
        assert len(self.lst.elements) == 3

    def testCounter(self):
        """ Test whether the counter attribute was created correctly
        """
        assert self.lst.counter == 3

    def testElements(self):
        """ Test whether all elements were created correctly
        """
        elems = [co_attribs.AttribNum(**self.vals["elements"][0]),
            co_attribs.AttribNum(**self.vals["elements"][1]),
            co_attribs.AttribNum(**self.vals["elements"][2])]
        for elem,exp in zip(self.lst.elements,elems):
            assert elem.toString(0) == exp.toString(0)

    def testOutput(self):
        """ Test whether the string output is correct
        """
        assert self.lst.toString(0) == self.parseString

    def testCheckGood(self):
        """ Test whether checking returns the correct problems
        """
        assert self.lst.check(None) == []

    def testCheckTooFewElements(self):
        """ Test whether too few elements are reported correctly
        """
        expected = ("ERROR: There are fewer (2) elements"
                    +" in the list than the given count \'counter\' (3)\n")
        self.lst.elements.pop()
        checkRes = self.lst.check(None)
        assert len(checkRes) == 1
        assert expected == checkRes[0].toString(0)

    def testCheckTooManyElements(self):
        """ Test whether too many elements are reported correctly
        """
        expected = ("ERROR: There are more (4) elements"
                    +" in the list than the given count \'counter\' (3)\n")
        self.lst.elements.append(
                co_attribs.AttribNum(identifier="numval",val=4))
        checkRes = self.lst.check(None)
        assert len(checkRes) == 1
        assert expected == checkRes[0].toString(0)

    def testCheckElemsChecked(self):
        """ Test whether recursive checking of elements works
        """
        for elem in self.lst.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.lst.check(None)
        for elem in self.lst.elements:
            elem.check.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether reference checking works
        """
        assert self.lst.checkRefs(None) == []

    def testGetRefs(self):
        """ Test whether references are assembled and returned correctly
        """
        assert self.lst.getRefs() == []

    def testLen(self):
        """ Test whether calling len() on an instance returns the correct value
        """
        assert len(self.lst) == 3

class TestAttribListRefs:
    """ Test what happens if the elements of a list are references
    """
    desc = "Test AttribList with references:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":3},
            "elements":[
                {"identifier":"file","val":"foo"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"baz"}],
            "identifier":"heading"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.lst = co_attribs.AttribList(elemType=co_attribs.FileRef,
                elemArgs={"subdir":"."},**self.vals)
        self.elems = [co_attribs.FileRef(subdir=".",
                **(self.vals["elements"][0]))]
        self.elems.append(co_attribs.FileRef(subdir=".",
                **self.vals["elements"][1]))
        self.elems.append(co_attribs.FileRef(subdir=".",
                **self.vals["elements"][2]))

    def testElemType(self):
        """ Test whether the elements have the correct type
        """
        for elem in self.lst.elements:
            assert isinstance(elem,co_attribs.FileRef)

    def testElements(self):
        """ Test whether elements were created correctly
        """
        for elem,exp in zip(self.lst.elements,self.elems):
            assert elem.toString(0) == exp.toString(0)

    def testCheckRefs(self):
        """ Test whether references are checked correctly
        """
        probs = [mock.create_autospec(co_probs.MissingRefProblem),
            mock.create_autospec(co_probs.MissingRefProblem),
            mock.create_autospec(co_probs.MissingRefProblem)]
        for elem,prob in zip(self.lst.elements,probs):
            elem.check = mock.MagicMock(return_value=[prob])
        res = self.lst.checkRefs(None)
        assert res == probs
        for elem in self.lst.elements:
            elem.check.assert_called_once_with(None)

    def testGetRefs(self):
        """ Test whether references are assembled and returned correctly
        """
        for elem,exp in zip(self.lst.getRefs(),self.elems):
            assert elem.toString(0) == exp.toString(0)

class TestAttribListFactory:
    desc = "Test AttribList with factory attribute:"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.vals = {
            "counter":{"identifier":"counter","val":3},
            "elements":[
                {"identifier":"file","val":"foo"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"baz"}],
            "identifier":"heading"}
        self.mockAttrib = mock.create_autospec(
                co_attribs.AttribRef)
        self.mockAttrib.factory = mock.MagicMock()
        self.lst = co_attribs.AttribList(elemType=self.mockAttrib,factory=True,
                **self.vals)

    def testElementCreation(self):
        """ Test whether createInstance was used for element creation
        """
        calls = [mock.call(**elem) for elem in self.vals["elements"]]
        self.mockAttrib.factory.assert_has_calls(calls,any_order=True)

    def testElementCreationNotInit(self):
        """ Test whether init was used for element creation
        """
        assert not self.mockAttrib.called

"""######################### Tests AttribList Keyed ########################"""

class TestAttribListKeyedEmpty:
    desc = "Test AttribListKeyed empty:"
    keyfunc = lambda val:val.val

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":0},
            "identifier":"header"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.lst = co_attribs.AttribListKeyed(elemType=co_attribs.AttribNum,
                keyfunc=self.keyfunc,**self.vals)

    def testCreation(self):
        """ Test whether the list is correctly created
        """
        newLst = co_attribs.AttribListKeyed(elemType=co_attribs.AttribNum,
                keyfunc=self.keyfunc,**self.vals)
        assert isinstance(newLst._counter,co_attribs.AttribNum)
        assert newLst.counter == 0
        assert len(newLst.elements) == 0

    def testCheck(self):
        """ Test whether check returns the correct problems
        """
        assert self.lst.check("currMod") == []

    def testCheckRefs(self):
        """ Test whether reference checking returns the correct problems
        """
        assert self.lst.checkRefs(None) == []

    def testGetRefs(self):
        """ The whether references are correctly assembled and returned
        """
        assert self.lst.getRefs() == []

    def testOutputNoHeading(self):
        """ Test whether string output without a heading is correct
        """
        out = "counter 0\n"
        tmp = self.vals["identifier"]
        del self.vals["identifier"]
        empty = co_attribs.AttribListKeyed(elemType=co_attribs.AttribNum,
                keyfunc=self.keyfunc,**self.vals)
        assert empty.toString(0) == out
        self.vals["identifier"] = tmp

    def testOutputHeading(self):
        """ Test whether string output with headings is correct
        """
        outHeading = "header\n\tcounter 0\n"
        assert self.lst.toString(0) == outHeading

    def testRemoveDuplicates(self):
        """ The whether removeDuplicates removes anything
        """
        assert self.lst.removeDuplicates() == []

    def testLen(self):
        """ Test whether calling len() on an instance returns the correct value
        """
        assert len(self.lst) == 0

class TestAttribListKeyedFull:
    desc = "Test AttribListKeyed full:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":3},
            "elements":[
                {"identifier":"numval","val":1},
                {"identifier":"numval","val":2},
                {"identifier":"numval","val":3}],
            "identifier":"heading"}
        cls.out = ("heading\n\tcounter 3\n\tnumval 1\n\tnumval 2\n"
                "\tnumval 3\n")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        keyfunc = lambda val:val.value
        self.lst = co_attribs.AttribListKeyed(elemType=co_attribs.AttribNum,
                keyfunc=keyfunc,**self.vals)
        self.elems = [co_attribs.AttribNum(**self.vals["elements"][0]),
            co_attribs.AttribNum(**self.vals["elements"][1]),
            co_attribs.AttribNum(**self.vals["elements"][2])]

    def testElemType(self):
        """ Test whether the elements have the correct type
        """
        for elem in self.lst.elements:
            assert isinstance(elem,co_attribs.AttribNum)

    def testElemCount(self):
        """ Test whether the correct number of elements was created
        """
        assert len(self.lst.elements) == 3

    def testCounter(self):
        """ Test whether the counter attribute was created correctly
        """
        assert self.lst.counter == 3

    def testElements(self):
        """ Test whether all elements were created correctly
        """
        for elem,exp in zip(self.lst.elements,self.elems):
            assert elem.toString(0) == exp.toString(0)

    def testOutput(self):
        """ Test whether the string output is correct
        """
        assert self.lst.toString(0) == self.out

    def testCheckGood(self):
        """ Test whether checking returns the correct problems
        """
        assert self.lst.check(None) == []

    def testCheckElemsChecked(self):
        """ Test whether recursive checking of elements works
        """
        for elem in self.lst.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.lst.check(None)
        for elem in self.lst.elements:
            elem.check.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether reference checking works
        """
        assert self.lst.checkRefs(None) == []

    def testGetRefs(self):
        """ Test whether references are assembled and returned correctly
        """
        assert self.lst.getRefs() == []

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates removes anything
        """
        assert self.lst.removeDuplicates() == []
        assert len(self.lst.elements) == 3
        assert len(self.lst.elemsSorted) == 3

    def testContains(self):
        """ Test whether \"x in y\" syntax works for existing items 
        """
        for elem in self.elems:
            assert elem.value in self.lst

    def testContainsBadVals(self):
        """ Test whether \"x in y\" syntax works for non-existing items 
        """
        for elem in [10,100,1000]:
            assert elem not in self.lst

    def testLen(self):
        """ Test whether calling len() on an instance returns the correct value
        """
        assert len(self.lst) == 3

class TestAttribListKeyedRefs:
    desc = "Test AttribListKeyed with references:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":3},
            "elements":[
                {"identifier":"file","val":"foo"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"baz"}],
            "identifier":"heading"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        keyfunc = lambda val:val.ref
        self.lst = co_attribs.AttribListKeyed(elemType=co_attribs.FileRef,
                elemArgs={"subdir":"."},keyfunc=keyfunc,**self.vals)
        self.elems = [co_attribs.FileRef(subdir=".",
                **(self.vals["elements"][0])),
            co_attribs.FileRef(subdir=".",**self.vals["elements"][1]),
            co_attribs.FileRef(subdir=".",**self.vals["elements"][2])]

    def testElemType(self):
        """ Test whether the elements have the correct type
        """
        for elem in self.lst.elements:
            assert isinstance(elem,co_attribs.FileRef)

    def testElements(self):
        """ Test whether elements were created correctly
        """
        for elem,exp in zip(self.lst.elements,self.elems):
            assert elem.toString(0) == exp.toString(0)

    def testCheckRefs(self):
        """ Test whether references are checked correctly
        """
        probs = [mock.create_autospec(co_probs.MissingRefProblem),
            mock.create_autospec(co_probs.MissingRefProblem),
            mock.create_autospec(co_probs.MissingRefProblem)]
        for elem,prob in zip(self.lst.elements,probs):
            elem.check = mock.MagicMock(return_value=[prob])
        res = self.lst.checkRefs(None)
        assert res == probs
        for elem in self.lst.elements:
            elem.check.assert_called_once_with(None)

    def testGetRefs(self):
        """ Test whether references are assembled and returned correctly
        """
        for elem,exp in zip(self.lst.getRefs(),self.elems):
            assert elem.toString(0) == exp.toString(0)

    def testGetElement(self):
        """ Test whether getElement returns the correct element
        """
        res = self.lst.getElement("foo")
        assert res == self.elems[0]

class TestAttribListKeyedFactory:
    desc = "Test AttribListKeyed with factory attribute:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":3},
            "elements":[
                {"identifier":"file","val":"foo"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"baz"}],
            "identifier":"heading"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.mockAttrib = mock.create_autospec(
                co_attribs.AttribRef)
        self.mockAttrib.factory = mock.MagicMock(res="foo")
        keyfunc = lambda val:val.ref
        self.lst = co_attribs.AttribListKeyed(elemType=self.mockAttrib,
                keyfunc=keyfunc,factory=True,**self.vals)

    def testElementCreation(self):
        """ Test whether createInstance was used for element creation
        """
        calls = [mock.call(**elem) for elem in self.vals["elements"]]
        self.mockAttrib.factory.assert_has_calls(calls,any_order=True)

    def testElementCreationNotInit(self):
        """ Test whether init was used for element creation
        """
        assert not self.mockAttrib.called

class TestAttribListKeyedDuplicates:
    desc = "Test AttribListKeyed with duplicates:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.vals = {
            "counter":{"identifier":"counter","val":6},
            "elements":[
                {"identifier":"file","val":"foo"},
                {"identifier":"file","val":"foo"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"bar"},
                {"identifier":"file","val":"baz"}],
            "identifier":"heading"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        keyfunc = lambda val:val.ref
        self.lst = co_attribs.AttribListKeyed(elemType=co_attribs.FileRef,
                elemArgs={"subdir":"."},keyfunc=keyfunc,**self.vals)
        self.elems = [co_attribs.FileRef(subdir=".",
                **(self.vals["elements"][0])),
            co_attribs.FileRef(subdir=".",**self.vals["elements"][2]),
            co_attribs.FileRef(subdir=".",**self.vals["elements"][5])]
        for elem in self.lst.elements:
            elem.check = mock.MagicMock(return_value=[])

    def testCheckProbs(self):
        """ Test whether check returns the correct duplicate problems
        """
        fooProb = co_probs.DuplicateEntryProb(2,"foo","heading")
        barProb = co_probs.DuplicateEntryProb(3,"bar","heading")
        res = self.lst.check(None)
        assert len(res) == 2
        assert fooProb in res
        assert barProb in res

    def testRemoveDuplicatesLen(self):
        """ Test whether list has the correct length after removeDuplicates
        """
        self.lst.removeDuplicates()
        assert len(self.lst) == 3

    def testRemoveDuplicatesReturns(self):
        """ Test whether removeDuplicates returns the correct duplicates
        """
        res = self.lst.removeDuplicates()
        assert len(res) == 3
        assert self.elems[0] in res
        assert self.elems[1] in res

    def testRemoveDuplicatesRemoval(self):
        """ Test whether removeDuplicates actually removes duplicates
        """
        self.lst.removeDuplicates()
        assert self.lst.elements == self.elems

    def testRemoveDuplicatesCounts(self):
        """ Test whether counts are updated correctly after duplicate removal
        """
        self.lst.removeDuplicates()
        assert self.lst.counter == 3

    def testRemoveDuplicatesNoProbs(self):
        """ Test whether duplicate probs are returned after removeDuplicates
        """
        self.lst.removeDuplicates()
        assert self.lst.check(None) == []

    def testLen(self):
        """ Test whether calling len() on an instance returns the correct value
        """
        assert len(self.lst) == 6

"""########################### Tests AttribString ##########################"""

class TestAttribString:
    desc = "Test AttribString with non-empty value:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.testString = "hello \"foo\"\n"
        cls.parseRes = co_parse.genStringAttrib("hello")("mystring"
        ).parseString(cls.testString).mystring

    def testCreation(self):
        """ Test whether instance is created correctly from parse results
        """
        res = co_attribs.AttribString(**self.parseRes)
        assert res.identifier == "hello"
        assert res.value == "foo"

    def testOutput(self):
        """ Test whether toString returns a correct string representation
        """
        res = co_attribs.AttribString(**self.parseRes)
        assert res.toString(0) == self.testString

    def testCheck(self):
        """ Test whether check returns an empty list of problems
        """
        res = co_attribs.AttribString(**self.parseRes).check(None)
        assert res == []

class TestAttribStringEmpty:
    desc = "Test AttribString with empty value:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.testString = "hello \"\"\n"
        cls.parseRes = co_parse.genStringAttrib("hello")("mystring"
        ).parseString(cls.testString).mystring

    def testCreation(self):
        """ Test whether instance is created correctly from parse results
        """
        res = co_attribs.AttribString(**self.parseRes)
        assert res.identifier == "hello"
        assert res.value == ""

    def testOutput(self):
        """ Test whether toString returns a correct string 
        """
        res = co_attribs.AttribString(**self.parseRes)
        assert res.toString(0) == self.testString

    def testOutputIndented(self):
        """ Test whether toString returns a correct indent string 
        """
        res = co_attribs.AttribString(**self.parseRes)
        assert res.toString(2) == "\t\t"+self.testString

"""########################### Tests AttribBool ############################"""

class TestAttribBool:
    desc = "Test AttribBool:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident TRUE\n"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribBool(identifier="ident",val=True)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genBoolAttrib("ident").parseString(
                self.parseString)[0]
        res = co_attribs.AttribBool(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check returns empty list
        """
        mmod = tco.genMockMod("./")
        assert self.inst.check(mmod) == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst1 = co_attribs.AttribBool(identifier="ident",val=True)
        assert self.inst == inst1

    def testEqualityDiffVal(self):
        """ Test whether instances with different value test equal
        """
        inst1 = co_attribs.AttribBool(identifier="ident",val=False)
        assert self.inst != inst1

"""########################### Tests AttribPos2d ###########################"""

class TestAttribPos2d:
    desc = "Test AttribPos2d:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident [100,200]\n"
        cls.pos = (100,200)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribPos2d("ident",list(self.pos))

    def testCreationParse(self):
        """ Test whether creation from parse results works correctly
        """
        parser = pp.Group(
                co_parse.g_linestart
                +pp.Keyword("ident")("identifier")
                +co_parse.g_space
                +co_parse.g_pos2
                +co_parse.g_eol
        )
        parseRes = parser.parseString(self.parseString)[0]
        res = co_attribs.AttribPos2d(**parseRes)
        assert self.inst == res

    def testCreationAttribPos(self):
        """ Test whether coord attribute is created correctly
        """
        assert self.inst.pos == self.pos

    def testValue(self):
        """ Value property gets pos value
        """
        assert self.inst.value == self.pos

    def testSetValue(self):
        """ Value property setting sets position
        """
        self.inst.value = (200,300)
        assert self.inst.value == (200,300)
        assert self.inst.pos == (200,300)

    def testSetValueShort(self):
        """ Set Value property with too small tuple raises error
        """
        with pytest.raises(ValueError):
            self.inst.value = (200,)

    def testSetValueLong(self):
        """ Set Value property with too long tuple raises error
        """
        with pytest.raises(ValueError):
            self.inst.value = (200,300,400)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        assert self.inst.check(self.mmod) == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = co_attribs.AttribPos2d("ident",[100,200])
        assert self.inst == inst1

    def testDiffPos(self):
        """ Test whether instances with different pos test unequal
        """
        inst1 = co_attribs.AttribPos2d("ident",[200,100])
        assert self.inst != inst1

"""########################### Tests AttribPos4d ###########################"""

class TestAttribPos4d:
    desc = "Test AttribPos4d:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident [ 100 , 200 , 300 , 400 ]\n"
        cls.mmod = tco.genMockMod("./")
        cls.pos = (100,200,300,400)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribPos4d("ident",list(self.pos))

    def testCreationParse(self):
        """ Test whether creation from parse results works correctly
        """
        parser = pp.Group(
                co_parse.g_linestart
                +pp.Keyword("ident")("identifier")
                +co_parse.g_space
                +co_parse.g_pos4
                +co_parse.g_eol
        )
        parseRes = parser.parseString(self.parseString)[0]
        res = co_attribs.AttribPos4d(**parseRes)
        assert self.inst == res

    def testCreationAttribPos(self):
        """ Test whether coord attribute is created correctly
        """
        assert self.inst.pos == self.pos

    def testValue(self):
        """ Value property gets pos value
        """
        assert self.inst.value == self.pos

    def testSetValue(self):
        """ Value property setting sets position
        """
        self.inst.value = (200,300,400,500)
        assert self.inst.value == (200,300,400,500)
        assert self.inst.pos == (200,300,400,500)

    def testSetValueShort(self):
        """ Set Value property with too small tuple raises error
        """
        with pytest.raises(ValueError):
            self.inst.value = (200,300,400)

    def testSetValueLong(self):
        """ Set Value property with too long tuple raises error
        """
        with pytest.raises(ValueError):
            self.inst.value = (200,300,400,100,500)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        assert self.inst.check(self.mmod) == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = co_attribs.AttribPos4d("ident",[100,200,300,400])
        assert self.inst == inst1

    def testDiffPos(self):
        """ Test whether instances with different pos test unequal
        """
        inst1 = co_attribs.AttribPos4d("ident",[200,100,600,900])
        assert self.inst != inst1

"""############################ Tests AttribNum ############################"""

class TestAttribNum:
    desc = "Tests AttribNum:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident 12\n"
        cls.identifier = "ident"
        cls.val = 12
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribNum(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParseInit(self):
        """ Test whether creation from parse result via init works
        """
        parseRes = co_parse.genIntAttrib("ident").parseString(
                self.parseString)[0]
        res = co_attribs.AttribNum(**parseRes)
        assert res == self.inst

    def testCreationParseFactory(self):
        """ Test whether creation from parse result via factory works
        """
        parseRes = co_parse.genIntAttrib("ident").parseString(
                self.parseString)[0]
        res = co_attribs.AttribNum.factory(parseRes)
        assert res == self.inst

    def testCreationFactoryInvalid(self):
        """ Test whether creation from factory with invalid input raises error
        """
        value = {"foo":"bar","identifier":"val"}
        with pytest.raises(RuntimeError):
            co_attribs.AttribNum.factory(value)

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testToStringLargeFloat(self):
        """ Test whether large numbers are displayed correctly, e.g.
            non-scientific
        """
        lValStr = "ident 30000001227554362788892595812564992.000000\n"
        largeVal = co_attribs.AttribNum("ident",
                30000001227554362788892595812564992.000000)
        assert largeVal.toString(0) == lValStr

    def testToStringLargeInt(self):
        """ Test whether large numbers are displayed correctly, e.g.
            non-scientific
        """
        lValStr = "ident 30000001227554363000000000000000000\n"
        largeVal = co_attribs.AttribNum("ident",
                30000001227554363000000000000000000)
        assert largeVal.toString(0) == lValStr

"""####################### Tests AttribVarLevelable ########################"""

class TestAttribVarLevelable:
    desc = "Tests AttribVarLevelable:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = (
            "ident\n"
            +"\tStartValue 1\n"
            +"\tValueIncreasePerLevel 2\n"
        )
        cls.identifier = "ident"
        cls.startVal = {"identifier":"StartValue","val":1}
        cls.perLvlInc = {"identifier":"ValueIncreasePerLevel","val":2}
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribVarLevelable(
                identifier=self.identifier,
                startVal=self.startVal,
                perLvlInc=self.perLvlInc
        )

    def testCreationParseInit(self):
        """ Test whether creation from parse result via init works
        """
        parseRes = genVarLvlAttrib("ident").parseString(self.parseString)[0]
        res = co_attribs.AttribVarLevelable(**parseRes)
        assert res == self.inst

    def testCreationParseFactory(self):
        """ Test whether creation from parse result via AttribNum factory works
        """
        parseRes = genVarLvlAttrib("ident").parseString(self.parseString)[0]
        res = co_attribs.AttribNum.factory(parseRes)
        assert res == self.inst

    def testCreationStartVal(self):
        """ Test whether startVal attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startVal)
        assert self.inst.startVal == exp

    def testCreationPerLvlInc(self):
        """ Test whether perLvlInc attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.perLvlInc)
        assert self.inst.perLvlInc == exp

    def testCreationValue(self):
        """ Test whether value attribute is created correctly
        """
        assert self.inst.value == self.startVal["val"]

    def testSetLvl(self):
        """ Test whether setLvl sets the value correctly
        """
        self.inst.setLvl(3)
        assert self.inst.value == 5

    def testSetLvlZeroOrOne(self):
        """ Test whether setLvl sets the value correctly for levels 0,1
        """
        self.inst.setLvl(0)
        assert self.inst.value == self.startVal["val"]
        self.inst.setLvl(1)
        assert self.inst.value == self.startVal["val"]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""###################### Tests AttribFixedLevelable #######################"""

class TestAttribFixedLevelable:
    desc = "Tests AttribFixedLevelable:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = (
            "ident\n"
            +"\tLevel:0 0.000000\n"
            +"\tLevel:1 1.000000\n"
            +"\tLevel:2 2.000000\n"
            +"\tLevel:3 3.000000\n"
        )
        cls.identifier = "ident"
        cls.levels = [
            {"identifier":"Level:0","val":0.0},
            {"identifier":"Level:1","val":1.0},
            {"identifier":"Level:2","val":2.0},
            {"identifier":"Level:3","val":3.0}
        ]

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribFixedLevelable(
                identifier=self.identifier,
                levels=self.levels
        )

    def testCreationParseInit(self):
        """ Test whether creation from parse result via init works correctly
        """
        parseRes = coe.genFixedLvlAttrib("ident").parseString(
                self.parseString)[0]
        res = co_attribs.AttribFixedLevelable(**parseRes)
        assert res == self.inst

    def testCreationParseFactory(self):
        """ Test whether creation from parse result via factory works correctly
        """
        parseRes = coe.genFixedLvlAttrib("ident").parseString(
                self.parseString)[0]
        res = co_attribs.AttribNum.factory(parseRes)
        assert res == self.inst

    def testCreationAttribLevels(self):
        """ Test whether the levels attribute is created correctly
        """
        l0 = co_attribs.AttribNum(**self.levels[0])
        l1 = co_attribs.AttribNum(**self.levels[1])
        l2 = co_attribs.AttribNum(**self.levels[2])
        l3 = co_attribs.AttribNum(**self.levels[3])
        exp = (l0,l1,l2,l3)
        assert self.inst.levels == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testSetLvl(self):
        """ Test whether setLvl sets value attribute correctly
        """
        self.inst.setLvl(2)
        assert self.inst.value == 2.0

    def testSetLvlInvalid(self):
        """ Test whether setLvl raises error for values outside [0,3]
        """
        with pytest.raises(RuntimeError):
            self.inst.setLvl(5)

"""########################### Tests AttribColor ###########################"""

class TestAttribColor:
    desc = "Tests AttribColor:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident 1\n"
        cls.identifier = "ident"
        cls.val = "1"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = co_attribs.AttribColor(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genColorAttrib("ident").parseString(
                self.parseString)[0]
        res = co_attribs.AttribColor(**parseRes)
        assert res == self.inst

    def testValue(self):
        """ Value property has correct value
        """
        assert self.inst.value == 1

    def testSetValue(self):
        """ Value property setting works correctly
        """
        self.inst.value = "2"
        assert self.inst.col == 2
        assert self.inst.value == 2

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################# Tests FileRef #############################"""

class TestFileRef:
    desc = "Tests FileRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.moddir = "./fileref/mod"
        cls.rebrefdir = "./fileref/rebref"
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebrefdir)

    def testRefFull(self):
        """ Test whether ref with full filename is handled correctly
        """
        ref = "test.testfile"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.ref == ref

    def testRefMissingExt(self):
        """ Test whether ref with missing file extension is handled correctly
        """
        ref = "test"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.ref == ref+".testfile"

    def testRefCapExt(self):
        """ Test whether extensions with all CAPS letters are handled correctly
        """
        ref = "test.TESTFILE"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.ref == ref.lower()

    def testRefWithDotWithoutExt(self):
        """ Test whether filenames with dots but no ext are handled correctly
        """
        ref = "test.testing"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.ref == "test.testing.testfile"

    def testRefWithDotWithExt(self):
        """ Test whether filenames with dots are handled correctly
        """
        ref = "test.testing.testfile"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.ref == "test.testing.testfile"

    def testToStringFull(self):
        """ Test whether toString with a full filename is constructed correctly
        """
        ref = "test.testfile"
        exp = "ident \"test.testfile\"\n"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.toString(0) == exp

    def testToStringMissingExt(self):
        """ Test whether string without file extension is constructed correctly
        """
        ref = "test"
        exp = "ident \"test\"\n"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.toString(0) == exp

    def testToStringCapExt(self):
        """ Test whether string with capital extension is constructed correctly
        """
        ref = "test.TESTFILE"
        exp = "ident \"test.TESTFILE\"\n"
        inst = co_attribs.FileRef("ident",ref,"",extension="testfile")
        assert inst.toString(0) == exp

    def testCheckExistenceMod(self):
        """ Test whether a file from a mod dir is found correctly
        """
        exp = []
        inst = co_attribs.FileRef("ident","test","",extension="testfile")
        res = inst.check(self.mmod)
        assert res == exp

    def testCheckExistenceCaseMod(self):
        """ Test whether a wrong case file from a mod dir is found correctly
        """
        exp = []
        inst = co_attribs.FileRef("ident","tESt","",extension="testfile")
        res = inst.check(self.mmod)
        assert res == exp

    def testCheckExistenceVanilla(self):
        """ Test whether a file from the vanilla dir is found correctly
        """
        inst = co_attribs.FileRef("ident","entity","",extension="manifest")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckExistenceCaseVanilla(self):
        """ Test whether a wrong case file from the vanilla dir is found correctly
        """
        inst = co_attribs.FileRef("ident","entiTy","",extension="mANIfest")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckExistenceRebRef(self):
        """ Test whether a file from the rebellion ref dir is found correctly
        """
        inst = co_attribs.FileRef("ident","rebref","",extension="testfile")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckExistenceCaseRebRef(self):
        """ Test whether a wrong case file from the reb ref dir is found correctly
        """
        inst = co_attribs.FileRef("ident","rEbref","",extension="testfile")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckExistenceMissing(self):
        """ Test whether a missing file returns correct problem
        """
        inst = co_attribs.FileRef("ident","missing","",extension="testfile")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

"""############################ Tests Attribute ############################"""

class TestAttributeModify:
    desc = "Tests Attribute modify:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.patcher = mock.patch.object(co_attribs.Attribute,
                "__abstractmethods__",set())
        cls.patcher.start()
        yield
        cls.patcher.stop()

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.testattrib1 = mock.create_autospec(
                co_attribs.Attribute,spec_set=True)
        self.testattrib1.modify.return_value = []
        self.testattrib2 = mock.create_autospec(
                co_attribs.Attribute,spec_set=True)
        self.testattrib2.modify.return_value = []
        self.inst = co_attribs.Attribute("ident")
        self.inst.attrib1 = self.testattrib1
        self.inst.attrib2 = self.testattrib2
        self.inst.nonattrib = mock.sentinel.attr
        self.mod1 = mock.create_autospec(modify.ModAttrib,spec_set=True)
        self.mod1.fits.return_value = False
        self.mod2 = mock.create_autospec(modify.ModAttrib,spec_set=True)
        self.mod2.fits.return_value = False
        self.mods = [self.mod1,self.mod2]

    @pytest.fixture()
    def changeFixture(self):
        self.change1 = mock.create_autospec(modify.ModificationChange,
                spec_set=True)
        self.change2 = mock.create_autospec(modify.ModificationChange,
                spec_set=True)

    def testModAttribs(self):
        """ Modify method called for all sub attributes
        """
        self.inst.modify(self.mods)
        self.testattrib1.modify.assert_called_once_with(self.mods)
        self.testattrib2.modify.assert_called_once_with(self.mods)

    def testModNonAttribs(self):
        """ Modify method not called for none attributes
        """
        self.inst.modify(self.mods)

    def testModReturnChanges(self,changeFixture):
        """ Modify method returns ModificationChanges when a change was made
        """
        self.testattrib1.modify.return_value = [self.change1]
        self.testattrib2.modify.return_value = [self.change2]
        res = self.inst.modify(self.mods)
        assert len(res) == 2
        assert set(res) == {self.change1,self.change2}

    def testModReturnNoChanges(self,changeFixture):
        """ Modify method returns empty list when no change has been made
        """
        res = self.inst.modify(self.mods)
        assert res == []

    def testModsApply(self,changeFixture):
        """ Modify method applies fitting modification
        """
        self.mod1.fits.return_value = True
        self.mod1.apply.return_value = self.change1
        res = self.inst.modify(self.mods)
        self.mod1.apply.assert_called_once_with(self.inst)
        assert not self.mod2.apply.called
        assert res == [self.change1]
