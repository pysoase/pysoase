""" Unit tests for the pysoase.tools.tools module.
"""

import os
from unittest import mock

import pytest

import pysoase.common.problems as co_probs
import pysoase.entities.smallcraft as scraft
import pysoase.mod.manifest as manifest
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
import pysoase.tools.tools as tools

testdata = os.path.join("tools","tools")
testmodule = "pysoase.tools.tools"

"""########################### Tests getFileClass ##########################"""

class TestGetFileClassBad:
    desc = "Test getFileClass with a bad file extension:"

    def testUnknownThrowsError(self):
        """ Test whether an error is raised with unknown file extension
        """
        with pytest.raises(RuntimeError):
            tools.getFileClass("foo.bar")

    def testEmptyString(self):
        """ Test whether the empty string is handled correctly
        """
        with pytest.raises(RuntimeError):
            tools.getFileClass("")

    def testEntityFile(self):
        """ Test whether classes for entity files are returned correctly
        """
        res = tools.getFileClass("Vehicle.entity")
        assert res == scraft.EntryVehicle

    def testNoExtension(self):
        """ Test whether file without extension is handled correctly
        """
        with pytest.raises(RuntimeError):
            tools.getFileClass("foo")

    def testIsDirectory(self):
        """ Test whether directory path produces error
        """
        with pytest.raises(RuntimeError):
            tools.getFileClass("testmod/foo/")

class TestGetFileClassKownType:
    desc = "Test getFileClass with known file extension:"

    def testStrFiles(self):
        """ Test whether *.str files are recognized correctly
        """
        res = tools.getFileClass("foo.str")
        assert ui.StringFile == res

    def testManifestFiles(self):
        """ Test whether *.manifest files are recognized correctly
        """
        res = tools.getFileClass("foo.manifest")
        assert manifest.ManifestBase == res

"""########################### Tests getModClass ##########################"""

class TestGetModClassBad:
    desc = "Test getModClass with a bad subdir:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.mmod = tco.genMockMod("./")

    def testUnknownThrowsError(self):
        """ Test whether an error is raised with unknown subdir
        """
        with pytest.raises(RuntimeError):
            tools.getModClass("/mod/subdir",self.mmod)

    def testEmptyString(self):
        """ Test whether the empty string is handled correctly
        """
        with pytest.raises(RuntimeError):
            tools.getModClass("",self.mmod)

    def testIsDirectory(self):
        """ Test whether file path produces error
        """
        with pytest.raises(RuntimeError):
            tools.getModClass("testmod/foo/test.file",self.mmod)

class TestGetModClassKnownType:
    desc = "Test getModClass with known file extension:"

    def testStringDir(self):
        """ Test whether String/ subdir is recognized correctly
        """
        mmod = tco.genMockMod("./")
        res = tools.getModClass("testmod/String",mmod)
        assert res == [mmod.strings]

"""############################# Tests Tools #############################"""

class TestToolsBadInput:
    desc = "Test Tools base class instantiation with invalid inputs:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        patcher = mock.patch.object(tools.Tool,"__abstractmethods__",set())
        patcher.start()
        cls.testdir = os.path.abspath("./Tool")
        cls.currdir = os.getcwd()
        os.chdir(cls.testdir)
        yield
        patcher.stop()
        os.chdir(cls.currdir)

    def testNoArguments(self):
        """ Test whether instantiation with no file or moddir raises error
        """
        with pytest.raises(RuntimeError):
            tools.Tool([])

    def testUknownSubdir(self):
        """ Test whether unknown directory raises error
        """
        with pytest.raises(RuntimeError):
            tools.Tool(["bar/"])

    def testUknownSubdirFile(self):
        """ Test whether a file in unknown directory raises error
        """
        with pytest.raises(RuntimeError):
            tools.Tool(["bar/Test.entity"])

    def testEmptyPathInFiles(self):
        """ Test whether moddir guess works with empty path in files
        """
        with pytest.raises(RuntimeError):
            tools.Tool([""])

    def testInvalidPath(self):
        """ Test whether missing is recognized and throws error
        """
        with pytest.raises(FileNotFoundError):
            tools.Tool(["./baz"])

class TestTool:
    desc = "Test Tool base class instantiation with valid inputs:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def setUpClass(cls):
        patcher = mock.patch.object(tools.Tool,"__abstractmethods__",set())
        patcher.start()
        cls.testdir = os.path.abspath("./Tool")
        cls.currdir = os.getcwd()
        os.chdir(cls.testdir)
        yield
        patcher.stop()
        os.chdir(cls.currdir)

    def testFilesEmpty(self):
        """ Test whether creation with moddir but empty files works
        """
        res = tools.Tool([],moddir=self.testdir)
        assert res.moddir == self.testdir
        assert res.mod.moddir == self.testdir

    def testAbsPath(self):
        """ Test whether all paths are made absolute
        """
        res = tools.Tool([],moddir=".")
        assert res.moddir == self.testdir
        assert res.mod.moddir == self.testdir

    def testDotSlash(self):
        """ Test whether moddir guess works with \'./\'
        """
        os.chdir(os.path.join(self.testdir,"GameInfo"))
        res = tools.Tool(["."])
        assert res.moddir == self.testdir
        os.chdir(self.testdir)

    def testGuessGalaxy(self):
        """ Test whether moddir is correctly guessed from Galaxy subdir
        """
        res = tools.Tool(["./Galaxy"])
        assert res.moddir == self.testdir

    def testGuessGameInfo(self):
        """ Test whether moddir is correctly guessed from GameInfo subdir
        """
        res = tools.Tool(["./GameInfo"])
        assert res.moddir == self.testdir

    def testGuessMesh(self):
        """ Test whether moddir is correctly guessed from Mesh subdir
        """
        res = tools.Tool(["./Mesh"])
        assert res.moddir == self.testdir

    def testGuessMovie(self):
        """ Test whether moddir is correctly guessed from Movie subdir
        """
        res = tools.Tool(["./Movie"])
        assert res.moddir == self.testdir

    def testGuessParticle(self):
        """ Test whether moddir is correctly guessed from Particle subdir
        """
        res = tools.Tool(["./Particle"])
        assert res.moddir == self.testdir

    def testGuessPipelineEffect(self):
        """ Test whether moddir is correctly guessed from PipelineEffect subdir
        """
        res = tools.Tool(["./PipelineEffect"])
        assert res.moddir == self.testdir

    def testGuessSound(self):
        """ Test whether moddir is correctly guessed from Sound subdir
        """
        res = tools.Tool(["./Sound"])
        assert res.moddir == self.testdir

    def testGuessString(self):
        """ Test whether moddir is correctly guessed from String subdir
        """
        res = tools.Tool(["./String"])
        assert res.moddir == self.testdir

    def testGuessTextures(self):
        """ Test whether moddir is correctly guessed from Textures subdir
        """
        res = tools.Tool(["./Textures"])
        assert res.moddir == self.testdir

    def testGuessWindow(self):
        """ Test whether moddir is correctly guessed from Window subdir
        """
        res = tools.Tool(["./Window"])
        assert res.moddir == self.testdir

    def testFileInSubdir(self):
        """ Test whether moddir is guessed correctly from file in subdir
        """
        res = tools.Tool(["./GameInfo/Test.entity"])
        assert res.moddir == self.testdir

    def testGuessManifestFile(self):
        """ Test whether moddir is guessed correctly from manifest file
        """
        res = tools.Tool(["./entity.manifest"])
        assert res.moddir == self.testdir

    def testGuessAny(self):
        """ Test whether moddir is guessed correctly from any entry in files
        """
        files = ["bar","GameInfo/Test.entity"]
        res = tools.Tool(files)
        assert res.moddir == self.testdir

"""########################## Tests filterProblems #########################"""

class TestFilterProblems:
    desc = "Test problem filtering:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,mockTqdm):
        cls.dup1 = co_probs.BasicProblem("Duplicate",
                severity=co_probs.ProblemSeverity.warn)
        cls.dup2 = co_probs.BasicProblem("Duplicate",
                severity=co_probs.ProblemSeverity.warn)
        cls.prob1 = co_probs.BasicProblem("Problem 1",
                severity=co_probs.ProblemSeverity.warn)
        cls.prob2 = co_probs.BasicProblem("Problem 2",
                severity=co_probs.ProblemSeverity.warn)
        cls.probError = co_probs.BasicProblem("Problem 2",
                severity=co_probs.ProblemSeverity.error)
        cls.probs = [cls.dup1,cls.dup2,cls.prob1,cls.prob2,cls.probError]

    def testEmptyProblems(self):
        """ Empty list of problems returns empty list of filtered problems
        """
        res = tools.filterProblems([])
        assert res == []

    def testRemoveDuplicates(self):
        """ Duplicate problems are removed
        """
        res = tools.filterProblems(self.probs)
        assert len(res) == 4

    def testFilterSeverity(self):
        """ Only problems with the given severity or higher are returned
        """
        res = tools.filterProblems(self.probs,
                co_probs.ProblemSeverity.error)
        assert len(res) == 1
        assert res == [self.probError]

"""########################## Tests printProblems ##########################"""

class TestPrintProblemsEmpty:
    desc = "Test printProblems with empty problem list:"

    def testOutput(self):
        """ Test whether output string is correct
        """
        with mock.patch("pysoase.tools.tools.tqdm.tqdm",autospec=True,
                spec_set=True):
            exp = "No problems were found!\nCongrats!"
            assert tools.printProblems([]) == exp

@pytest.mark.usefixtures("mockTqdm")
class TestPrintProblems:
    desc = "Test printProblems with various problems:"

    def testGeneralSingleProb(self):
        """ Test whether single, general problem output is correct
        """
        prob = co_probs.BasicProblem(severity=co_probs.ProblemSeverity.warn,
                desc="Hello, World!")
        exp = "General Problems:\n"
        exp += prob.toString(1)
        exp += "\n\nFound 1 general problem."
        assert tools.printProblems([prob]) == exp

    def testGeneralMultipleProbs(self):
        """ Test whether multiple, general problem output is correct
        """
        sev = co_probs.ProblemSeverity.warn
        probs = [
            co_probs.BasicProblem("Problem 1",severity=sev),
            co_probs.BasicProblem("Problem 2",severity=co_probs.ProblemSeverity.error),
            co_probs.BasicProblem("Problem 3",severity=sev)]
        exp = "General Problems:\n"
        exp += probs[1].toString(1)
        exp += probs[0].toString(1)
        exp += probs[2].toString(1)
        exp += "\n\nFound 3 general problems."
        assert tools.printProblems(probs) == exp

    def testSingleFileSingleProb(self):
        """ Test whether single file, single prob output is correct
        """
        prob = co_probs.BasicProblem("Problem 1",severity=co_probs.ProblemSeverity.warn)
        prob.probFile = "testdir/Testfile"
        exp = "Testfile:\n"
        exp += prob.toString(1)
        exp += "\n\nFound 1 problem in 1 file."
        assert tools.printProblems([prob]) == exp

    def testSingleFileMultipleProb(self):
        """ Test whether single file, multiple prob output is correct
        """
        sev = co_probs.ProblemSeverity.warn
        probs = [
            co_probs.BasicProblem("Problem 1",severity=sev),
            co_probs.BasicProblem("Problem 2",severity=co_probs.ProblemSeverity.error),
            co_probs.BasicProblem("Problem 3",severity=sev)]
        exp = "Testfile:\n"
        for prob in probs:
            prob.probFile = "testdir/Testfile"
        exp += probs[1].toString(1)
        exp += probs[0].toString(1)
        exp += probs[2].toString(1)
        exp += "\n\nFound 3 problems in 1 file."
        assert tools.printProblems(probs) == exp

    def testMultiFileMultiProb(self):
        """ Test whether mutliple file, multiple prob output is correct
        """
        sev = co_probs.ProblemSeverity.warn
        probs = [
            co_probs.BasicProblem("Problem 1",severity=sev),
            co_probs.BasicProblem("Problem 2",severity=sev),
            co_probs.BasicProblem("Problem 3",severity=sev),
            co_probs.BasicProblem("Problem 4",severity=sev),
            co_probs.BasicProblem("Problem 5",severity=sev),
            co_probs.BasicProblem("Problem 6",severity=sev)]
        probs[0].probFile = "adir/AFile.z"
        probs[5].probFile = "adir/AFile.z"
        probs[1].probFile = "testdir/BFile.a"
        probs[4].probFile = "testdir/BFile.a"
        probs[2].probFile = "testdir2/AFile.a"
        probs[3].probFile = "testdir2/AFile.a"
        exp = "AFile.a:\n"
        exp += probs[2].toString(1)
        exp += probs[3].toString(1)
        exp += "BFile.a:\n"
        exp += probs[1].toString(1)
        exp += probs[4].toString(1)
        exp += "AFile.z:\n"
        exp += probs[0].toString(1)
        exp += probs[5].toString(1)
        exp += "\n\nFound 6 problems in 3 files."
        res = tools.printProblems(probs)
        assert res == exp

    def testMultiFileMultiProbNoFooter(self):
        """ Test whether mutliple file, multiple prob no footer output is correct
        """
        sev = co_probs.ProblemSeverity.warn
        probs = [
            co_probs.BasicProblem("Problem 1",severity=sev),
            co_probs.BasicProblem("Problem 2",severity=sev),
            co_probs.BasicProblem("Problem 3",severity=sev),
            co_probs.BasicProblem("Problem 4",severity=sev),
            co_probs.BasicProblem("Problem 5",severity=sev),
            co_probs.BasicProblem("Problem 6",severity=sev)]
        probs[0].probFile = "adir/AFile.z"
        probs[5].probFile = "adir/AFile.z"
        probs[1].probFile = "testdir/BFile.a"
        probs[4].probFile = "testdir/BFile.a"
        probs[2].probFile = "testdir2/AFile.a"
        probs[3].probFile = "testdir2/AFile.a"
        exp = "AFile.a:\n"
        exp += probs[2].toString(1)
        exp += probs[3].toString(1)
        exp += "BFile.a:\n"
        exp += probs[1].toString(1)
        exp += probs[4].toString(1)
        exp += "AFile.z:\n"
        exp += probs[0].toString(1)
        exp += probs[5].toString(1)
        exp += "\n\n"
        res = tools.printProblems(probs,False)
        assert res == exp
