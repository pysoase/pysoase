""" Tests for the pysoase.tools.showing module
"""

import os
import re
from unittest import mock

import pytest

import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
import pysoase.common.problems as co_probs
import pysoase.entities.buffs_abilities as abilities
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
import pysoase.tools.showing as showing

testdata = os.path.join("tools","showing")
testmodule = "pysoase.tools.showing"

"""######################### Tests getRefsFromFile #########################"""

class TestGetRefsFromFile:
    desc = "Tests getRefsFromFile:"

    @pytest.fixture(autouse=True)
    def testFixture(self,mockTqdm):
        self.mockClass = mock.create_autospec(
                co_basics.HasReference,spec_set=True)
        self.mockCreate = mock.MagicMock(createInstance=mock.MagicMock())
        self.mockCreate.createInstance.return_value = self.mockClass
        patcher = mock.patch("pysoase.tools.showing.tools.getFileClass",
                autospec=True,spec_set=True,return_value=self.mockCreate)
        self.mockGetFile = patcher.start()
        p = mock.patch("sys.stdout",autospec=True)
        p.start()
        yield
        p.stop()
        patcher.stop()

    def testGoodFile(self):
        """ Test whether references are returned correctly
        """
        f = "Test.entity"
        ref1 = mock.MagicMock(_REFTYPE="type1")
        ref2 = mock.MagicMock(_REFTYPE="type2")
        self.mockClass.getRefs.return_value = [ref1,ref2]
        expmap = {"type1":[ref1],"type2":[ref2]}
        refmap = {"type1":[],"type2":[]}
        res = showing.getRefsFromFile(f,refmap)
        self.mockGetFile.assert_called_once_with(f)
        self.mockCreate.createInstance.assert_called_once_with(f)
        assert res == []
        assert refmap == expmap

    def testMalformedFile(self):
        """ Test whether problems for malformed files are returned correctly
        """
        f = "Test.entity"
        refmap = {}
        self.mockCreate.createInstance.return_value = [mock.sentinel.prob]
        res = showing.getRefsFromFile(f,refmap)
        self.mockGetFile.assert_called_once_with(f)
        self.mockCreate.createInstance.assert_called_once_with(f)
        assert res == [mock.sentinel.prob]

"""############################# Tests ShowRefs ############################"""

brushref = ui.BrushRef(identifier="brush",val="brushref")
stringref = ui.StringReference(identifier="string",val="stringref")

def getRefsFromFileMock(f,refmap):
    if os.path.basename(f)=="test1.entity":
        refmap[brushref._REFTYPE] = [brushref]
        return mock.DEFAULT
    elif os.path.basename(f)=="test2.entity":
        refmap[stringref._REFTYPE] = [stringref]
        return mock.DEFAULT
    elif os.path.basename(f)=="test3.entity":
        return [co_probs.BasicProblem("Testprob",probFile="test3.entity",
                severity=co_probs.ProblemSeverity.error)]

class TestShowRefs:
    desc = "Tests ShowRefs:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,mockTqdm):
        cls.files = ["test1.entity","test2.entity"]

    @pytest.fixture(autouse=True)
    def testFixture(self):
        pGetBrush = mock.patch.object(brushref,"getBrushfile",autospec=True,
                spec_set=True,return_value="test.brush")
        pGetBrush.start()
        pGetRefs = mock.patch("pysoase.tools.showing.getRefsFromFile",
                autospec=True,spec_set=True,side_effect=getRefsFromFileMock,
                return_value=[])
        self.mockGetFile = pGetRefs.start()
        p = mock.patch("sys.stdout",autospec=True)
        p.start()
        yield
        p.stop()
        pGetBrush.stop()
        pGetRefs.stop()

    def testPrintByFile(self):
        """ Test whether printByFile produces the correct output
        """
        expString = (
            "test1.entity\n"
            +"\tBrush\n"
            +"\t\tbrush \"brushref\" (test.brush)\n"
            +"test2.entity\n"
            +"\tString\n"
            +"\t\tstring \"stringref\"\n"
        )
        inst = showing.ShowRefs(moddir="./",files=self.files,combined=False)
        res = inst._computeResults()
        assert res == (0,expString)

    def testPrintCombined(self):
        """ Test whether printCombined produces the correct output
        """
        expString = (
            "Brush\n"
            +"\tbrush \"brushref\" (test.brush)\n"
            +"String\n"
            +"\tstring \"stringref\"\n"
        )
        inst = showing.ShowRefs(moddir="./",files=self.files,combined=True)
        res = inst._computeResults()
        assert res == (0,expString)

    def testPrintByFileProblems(self):
        """ Test whether printByFile outputs problems correctly
        """
        expString = (
            "test1.entity\n"
            +"\tBrush\n"
            +"\t\tbrush \"brushref\" (test.brush)\n"
            +"test2.entity\n"
            +"\tString\n"
            +"\t\tstring \"stringref\"\n"
            +"test3.entity\n"
            +"\tERROR: Testprob\n"
        )
        probFile = ["test3.entity"]
        probFile.extend(self.files)
        inst = showing.ShowRefs(moddir="./",files=probFile,combined=False)
        res = inst._computeResults()
        assert res == (1,expString)

    def testPrintCombinedProblems(self):
        """ Test whether printCombined outputs problems correctly
        """
        expString = (
            "Brush\n"
            +"\tbrush \"brushref\" (test.brush)\n"
            +"String\n"
            +"\tstring \"stringref\"\n"
            +"test3.entity\n"
            +"\tERROR: Testprob\n"
        )
        self.files.append("test3.entity")
        inst = showing.ShowRefs(moddir="./",files=self.files,combined=True)
        res = inst._computeResults()
        assert res == (1,expString)

"""########################## Tests ShowBuffchains #########################"""

@pytest.mark.usefixtures("mockTqdm")
class TestShowBuffchains:
    desc = "Tests ShowBuffchain:"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.files = ["test1.entity","test2.entity"]
        self.mockClass = mock.create_autospec(abilities.Buff,spec_set=True)
        self.chain = (
            "test1.entity:\n"
            +"\tNo Buffs Referenced\n"
        )
        self.mockClass.outputBuffchain.return_value = self.chain
        self.mockInvalid = mock.create_autospec(ui.BrushFile,spec_set=True)
        self.mockCreate = mock.MagicMock(createInstance=mock.MagicMock())
        self.mockCreate.createInstance.side_effect = [self.mockClass,
            self.mockInvalid]
        pGetFile = mock.patch("pysoase.tools.showing.tools.getFileClass",
                autospec=True,spec_set=True,return_value=self.mockCreate)
        self.mockGetFile = pGetFile.start()
        p = mock.patch("sys.stdout",autospec=True)
        p.start()
        yield
        p.stop()
        pGetFile.stop()

    def testBuffchainOutput(self):
        """ Test whether the correct Buffchains are returned
        """
        exp = (1,
            self.chain
            +"The following files are not of a type which has Buffs:\n"
            +"\ttest2.entity\n"
        )
        inst = showing.ShowBuffchain(moddir="./",files=self.files)
        res = inst._computeResults()
        assert res == exp
        self.mockClass.outputBuffchain.assert_called_once_with(inst.mod,0)

"""############################ Tests String Diff ###########################"""

class TestStringFileDiffFunc:
    desc = "Test string_file_diff function"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,mockTqdm):
        cls.baseFile = ui.StringFile(
            filepath="Test.str",
            entries={
                "counter":{"identifier":"NumStrings","val":3},
                "elements":[
                    {"stringid":"id1","val":"val1"},
                    {"stringid":"id2","val":"val2"},
                    {"stringid":"id3","val":"val3"}
                ]
            }
        )

    def testNoChanges(self):
        """ Test whether file with no changes returns empty lists
        """
        modFile = ui.StringFile(
            filepath="Test.str",
            entries={
                "counter":{"identifier":"NumStrings","val":3},
                "elements":[
                    {"stringid":"id1","val":"val1"},
                    {"stringid":"id2","val":"val2"},
                    {"stringid":"id3","val":"val3"}
                ]
            }
        )
        resModified,resAdded = showing.string_file_diff(self.baseFile,modFile)
        assert resModified == []
        assert resAdded == []

    def testIdentical(self):
        """ Test whether identical file returns empty lists
        """
        modFile = ui.StringFile(
            filepath="Test.str",
            entries={
                "counter":{"identifier":"NumStrings","val":1},
                "elements":[
                    {"stringid":"id3","val":"val3"}
                ]
            }
        )
        resModified,resAdded = showing.string_file_diff(self.baseFile,modFile)
        assert resModified == []
        assert resAdded == []

    def testAdditionsOnly(self):
        """ Test whether only additions are returned correctly
        """
        add1 = {"stringid":"id4","val":"val4"}
        add2 = {"stringid":"id5","val":"val5"}
        modFile = ui.StringFile(
            filepath="Test.str",
            entries={
                "counter":{"identifier":"NumStrings","val":5},
                "elements":[
                    {"stringid":"id1","val":"val1"},
                    {"stringid":"id2","val":"val2"},
                    {"stringid":"id3","val":"val3"},
                    add1,add2
                ]
            }
        )
        resModified,resAdded = showing.string_file_diff(self.baseFile,modFile)
        assert resModified == []
        assert ui.StringInfo(**add1) in resAdded
        assert ui.StringInfo(**add2) in resAdded

    def testModificationsOnly(self):
        """ Test whether only modifications are returned correctly
        """
        mod1 = {"stringid":"id2","val":"mod2"}
        mod2 = {"stringid":"id3","val":"mod3"}
        modFile = ui.StringFile(
            filepath="Test.str",
            entries={
                "counter":{"identifier":"NumStrings","val":3},
                "elements":[
                    {"stringid":"id1","val":"val1"},
                    mod1,mod2
                ]
            }
        )
        resModified,resAdded = showing.string_file_diff(self.baseFile,modFile)
        assert resAdded == []
        assert ui.StringInfo(**mod1) in resModified
        assert ui.StringInfo(**mod2) in resModified

    def testModificationsAndAdditions(self):
        """ Test whether modifications and additions together are returned
        """
        mod1 = {"stringid":"id2","val":"mod2"}
        mod2 = {"stringid":"id3","val":"mod3"}
        add1 = {"stringid":"id4","val":"val4"}
        add2 = {"stringid":"id5","val":"val5"}
        modFile = ui.StringFile(
                filepath="Test.str",
                entries={
                    "counter":{"identifier":"NumStrings","val":5},
                    "elements":[
                        {"stringid":"id1","val":"val1"},
                        add1,add2,mod1,mod2
                    ]
                }
        )
        resModified,resAdded = showing.string_file_diff(self.baseFile,modFile)
        assert ui.StringInfo(**add1) in resAdded
        assert ui.StringInfo(**add2) in resAdded
        assert ui.StringInfo(**mod1) in resModified
        assert ui.StringInfo(**mod2) in resModified

class TestStringDiffIntegration:
    desc = "Test Integration of String Diff tool"

    @classmethod
    @pytest.fixture(scope="class")
    def baseStringfile(cls):
        cls.base = ui.StringFile.createInstance("./stringdiff/base.str")

    @pytest.fixture()
    def mockLoadBaseVanilla(self,baseStringfile):
        patcher = mock.patch.object(showing.ShowStringdiff,
                    "_loadVanillaStringfile",return_value=(0,self.base))
        patcher.start()
        yield
        patcher.stop()

    def testBundledEnglishStr(self):
        """ Test whether English.str file can be loaded from bundle
        """
        lmod = tco.genMockMod("./")
        fsource,probs = co_files.checkFileExistence("String",
                "English.str",lmod)
        assert len(probs) == 1
        assert isinstance(probs[0],co_probs.UsingRebFileProblem)
        assert fsource == co_files.FileSource.bundled

    @pytest.fixture()
    def mockSuccessfullVanilla(self):
        patcher = mock.patch.object(showing.ShowStringdiff,
                "_loadVanillaStringfile",return_value=(0,None))
        patcher.start()
        yield
        patcher.stop()

    def testMalformedStringFile(self,mockSuccessfullVanilla):
        """ Test whether malformed string file leads to correct message
        """
        regexp = re.compile(
            "The following syntax error occured when parsing "
            +"the String file:\n"
            +"\tEnglish.str:\n"
            +"\t\tERROR: The following syntax error was found in file .*\n"
            +"\t\t\tline: .*\n"
            +"\t\t\terror: .*\n\n\n"
            +"Aborting."
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/malformed_string")
        retVal,res = inst._computeResults()
        assert regexp.match(res) is not None
        assert retVal==1

    def testMissingStringDir(self,mockSuccessfullVanilla):
        """ Test whether mod with no string directory gives correct message
        """
        exp = (
            "Could not find \"stringdiff/empty_mod/String\".\n"
            +"Perhaps you missspelled your mod directory?\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/empty_mod")
        retVal,res = inst._computeResults()
        assert res == exp
        assert retVal==1

    def testLanguageNotFound(self,mockSuccessfullVanilla):
        """ Test whether missing string file for given language gives correct
            msg
        """
        exp = (
            "Language File English.str not found. The Mod does not "
            +"contain any changes to English strings.\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/german_mod")
        retVal,res = inst._computeResults()
        assert res == exp
        assert retVal == 1

    def testNoRebRefNotBundled(self):
        """ Test whether nonbundled vanilla language gives error message
        """
        exp = (
            "German.str is not bundled with PySoaSE.\nPlease provide your "
            "Sins Rebellion directory via the --rebref option.\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/german_mod",
                language="German")
        retVal,res = inst._computeResults()
        assert res == exp
        assert retVal == 1

    def testLanguageNotFoundVanilla(self):
        """ Test whether nonexistant vanilla language gives error message
        """
        exp = (
            "Exotic.str String file is not part of the vanilla release, thus\n"
            "there is nothing to compare it to.\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/exotic_mod",
                language="Exotic",rebrefdir="./stringdiff/german_mod")
        retVal,res = inst._computeResults()
        assert res == exp
        assert retVal == 1

    @pytest.fixture()
    def removeResultFiles(self):
        removePaths = []
        yield removePaths
        for p in removePaths:
            if(os.path.exists(p)):
                os.remove(p)

    def testDefaultLanguage(self,mockLoadBaseVanilla,removeResultFiles):
        """ Test whether English is correctly set as default language
        """
        pchanges = "./stringdiff/additions_english/" \
                   "stringdiff_changed_English.txt"
        padditions = "./stringdiff/additions_english/" \
                     "stringdiff_added_English.txt"
        removeResultFiles.extend([
            pchanges,
            padditions
        ])
        expOut = (
            "Found 0 changed and 1 added Strings in "
            "stringdiff/additions_english/String/English.str.\n"
            "Output written to "
            "stringdiff/additions_english/stringdiff_added_English.txt.\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/additions_english")
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(padditions)
        assert not os.path.exists(pchanges)

    def testDefaultAdditions(self,mockLoadBaseVanilla,removeResultFiles):
        """ Test whether English additions are returned
        """
        pchanges = "./stringdiff/additions_english/" \
                   "stringdiff_changed_English.txt"
        padditions = "./stringdiff/additions_english/" \
                     "stringdiff_added_English.txt"
        removeResultFiles.extend([
            pchanges,
            padditions
        ])
        expOut = (
            "Found 0 changed and 1 added Strings in "
            "stringdiff/additions_english/String/English.str.\n"
            "Output written to "
            "stringdiff/additions_english/stringdiff_added_English.txt.\n"
        )
        expAddFile = (
            "StringInfo\n"
            "\tID \"AdditionString\"\n"
            "\tValue \"New String Value\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/additions_english")
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(padditions)
        with open(padditions) as f:
            assert f.read() == expAddFile
        assert not os.path.exists(pchanges)

    def testDefaultChanges(self,mockLoadBaseVanilla,removeResultFiles):
        """ Test whether English changes are returned
        """
        pchanges = "./stringdiff/changes_english/" \
                   "stringdiff_changed_English.txt"
        padditions = "./stringdiff/changes_english/" \
                     "stringdiff_added_English.txt"
        removeResultFiles.extend([
            pchanges,
            padditions
        ])
        expOut = (
            "Found 1 changed and 0 added Strings in "
            "stringdiff/changes_english/String/English.str.\n"
            "Output written to "
            "stringdiff/changes_english/stringdiff_changed_English.txt.\n"
        )
        expChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_LABEL_ABILITYDURATION\"\n"
            "\tValue \"NewDuration\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/changes_english")
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(pchanges)
        with open(pchanges) as f:
            assert f.read() == expChangeFile
        assert not os.path.exists(padditions)

    def testDefaultChangesAndAdditions(self,mockLoadBaseVanilla,removeResultFiles):
        """ Test whether English changes and additions are returned
        """
        pchanges = "./stringdiff/both_english/" \
                   "stringdiff_changed_English.txt"
        padditions = "./stringdiff/both_english/" \
                     "stringdiff_added_English.txt"
        removeResultFiles.extend([
            pchanges,
            padditions
        ])
        expOut = (
            "Found 2 changed and 2 added Strings in "
            "stringdiff/both_english/String/English.str.\n"
            "Output written to "
            "stringdiff/both_english/stringdiff_added_English.txt\n"
            "and stringdiff/both_english/stringdiff_changed_English.txt.\n"
        )
        expChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_LABEL_ABILITYDURATION\"\n"
            "\tValue \"NewDuration\"\n"
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_TITLE_CREDITS\"\n"
            "\tValue \"Credits New\"\n"
        )
        expAddFile = (
            "StringInfo\n"
            "\tID \"NewString1\"\n"
            "\tValue \"String New 1\"\n"
            "StringInfo\n"
            "\tID \"NewString2\"\n"
            "\tValue \"String New 2\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/both_english")
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(padditions)
        assert os.path.exists(pchanges)
        with open(padditions) as f:
            assert f.read() == expAddFile
        with open(pchanges) as f:
            assert f.read() == expChangeFile

    def testGermanChanges(self,mockLoadBaseVanilla,removeResultFiles):
        """ Test whether German changes are returned
        """
        pchanges = "./stringdiff/german_mod/" \
                   "stringdiff_changed_German.txt"
        padditions = "./stringdiff/german_mod/" \
                     "stringdiff_added_German.txt"
        removeResultFiles.extend([
            pchanges,
            padditions
        ])
        expOut = (
            "Found 1 changed and 0 added Strings in "
            "stringdiff/german_mod/String/German.str.\n"
            "Output written to "
            "stringdiff/german_mod/stringdiff_changed_German.txt.\n"
        )
        expChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_TITLE_CREDITS\"\n"
            "\tValue \"Moneten\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/german_mod",
                language="German",rebrefdir="./stringdiff/rebref")
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(pchanges)
        with open(pchanges) as f:
            assert f.read() == expChangeFile
        assert not os.path.exists(padditions)

    def testOutputDir(self,mockLoadBaseVanilla,removeResultFiles):
        """ Test whether result files land in correct output dir
        """
        pchanges = "./stringdiff/compares/" \
                   "stringdiff_changed_English.txt"
        padditions = "./stringdiff/compares/" \
                     "stringdiff_added_English.txt"
        removeResultFiles.extend([
            pchanges,
            padditions
        ])
        expOut = (
            "Found 1 changed and 0 added Strings in "
            "stringdiff/changes_english/String/English.str.\n"
            "Output written to "
            "stringdiff/compares/stringdiff_changed_English.txt.\n"
        )
        expChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_LABEL_ABILITYDURATION\"\n"
            "\tValue \"NewDuration\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/changes_english",
                outdir="./stringdiff/compares")
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(pchanges)
        with open(pchanges) as f:
            assert f.read() == expChangeFile
        assert not os.path.exists(
                padditions)

    @pytest.fixture()
    def replaceStringdiffFiles(self):
        yield
        oldAddFile = (
            "StringInfo\n"
            "\tID \"NewString1\"\n"
            "\tValue \"String New 1\"\n"
            "StringInfo\n"
            "\tID \"NewString2\"\n"
            "\tValue \"String New 2\"\n"
        )
        oldChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_LABEL_ABILITYDURATION\"\n"
            "\tValue \"NewDuration\"\n"
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_TITLE_CREDITS\"\n"
            "\tValue \"Credits New\"\n"
        )
        with open(
                "./stringdiff/update/stringdiff_added_English.txt",
                'w'
                ) as f:
            f.write(oldAddFile)
        with open(
                "./stringdiff/update/stringdiff_changed_English.txt",
                'w'
                ) as f:
            f.write(oldChangeFile)

    def testUpdate(self,mockLoadBaseVanilla,replaceStringdiffFiles):
        """ Test whether update gives correct output
        """
        expOut = (
            "Found 3 changed and 3 added Strings in "
            "stringdiff/update/String/English.str.\n"
            "Output written to "
            "stringdiff/update/stringdiff_added_English.txt\n"
            "and stringdiff/update/stringdiff_changed_English.txt.\n\n"
            "The following newly added Strings were found:\n"
            "\tStringInfo\n"
            "\t\tID \"NewString3\"\n"
            "\t\tValue \"String New 3\"\n\n"
            "The following newly modified Strings were found:\n"
            "\tStringInfo\n"
            "\t\tID \"IDS_RANDOMEVENT_RESOURCEBOOM_NAME\"\n"
            "\t\tValue \"Changed\"\n"
        )
        expChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_LABEL_ABILITYDURATION\"\n"
            "\tValue \"NewDuration\"\n"
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_TITLE_CREDITS\"\n"
            "\tValue \"Credits New\"\n"
            "StringInfo\n"
            "\tID \"IDS_RANDOMEVENT_RESOURCEBOOM_NAME\"\n"
            "\tValue \"Changed\"\n"
        )
        expAddFile = (
            "StringInfo\n"
            "\tID \"NewString1\"\n"
            "\tValue \"String New 1\"\n"
            "StringInfo\n"
            "\tID \"NewString2\"\n"
            "\tValue \"String New 2\"\n"
            "StringInfo\n"
            "\tID \"NewString3\"\n"
            "\tValue \"String New 3\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/update",update=True)
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        assert os.path.exists(
                "./stringdiff/update/stringdiff_added_English.txt")
        assert os.path.exists(
                "./stringdiff/update/stringdiff_changed_English.txt")
        with open(
                "./stringdiff/update/stringdiff_added_English.txt",
                'r'
                ) as f:
            assert f.read() == expAddFile
        with open(
                "./stringdiff/update/stringdiff_changed_English.txt",
                'r'
                ) as f:
            assert f.read() == expChangeFile

    def testUpdateNoChanges(self,mockLoadBaseVanilla):
        """ Test whether no updates with update option is reported
        """
        expOut = (
            "Found 2 changed and 2 added Strings in "
            "stringdiff/update_no_changes/String/English.str.\n"
            "Output written to "
            "stringdiff/update_no_changes/stringdiff_added_English.txt\n"
            "and stringdiff/update_no_changes/stringdiff_changed_English.txt.\n\n"
            "There were no added Strings compared to the already present "
            "Diff file.\n"
            "There were no changed Strings compared to the already present "
            "Diff file.\n"
        )
        expChangeFile = (
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_LABEL_ABILITYDURATION\"\n"
            "\tValue \"NewDuration\"\n"
            "StringInfo\n"
            "\tID \"IDS_INFOCARD_TITLE_CREDITS\"\n"
            "\tValue \"Credits New\"\n"
        )
        expAddFile = (
            "StringInfo\n"
            "\tID \"NewString1\"\n"
            "\tValue \"String New 1\"\n"
            "StringInfo\n"
            "\tID \"NewString2\"\n"
            "\tValue \"String New 2\"\n"
        )
        inst = showing.ShowStringdiff(moddir="./stringdiff/update_no_changes",
                update=True)
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 0
        with open(
                "./stringdiff/update_no_changes/stringdiff_added_English.txt",
                ) as f:
            assert f.read() == expAddFile
        with open(
                "./stringdiff/update_no_changes/stringdiff_changed_English.txt",
                ) as f:
            assert f.read() == expChangeFile

    def testMalformedDiffFile(self,mockLoadBaseVanilla):
        """ Test whether malformed Diff files throw error
        """
        expOut = (
            "Sorry, the diff files in 'stringdiff/update_malformed_diff'\n"
            "are malformed. Please remove the '--update' option to "
            "regenerate them.\n"
        )
        inst = showing.ShowStringdiff(
                moddir="./stringdiff/update_malformed_diff",
                update=True)
        retVal,res = inst._computeResults()
        assert res == expOut
        assert retVal == 1

    def testUpdateNoDiffFiles(self,mockLoadBaseVanilla):
        """ Test whether missing Diff files with update option are reported
        """
        expOut = (
            "No Diff files found in 'stringdiff/both_english'.\n"
            "Run PySoaSE without the '--update' option to generate them.\n"
        )
        inst = showing.ShowStringdiff(
                moddir="./stringdiff/both_english",
                update=True)
        retVal,res = inst._computeResults()
        assert retVal == 1
        assert res == expOut