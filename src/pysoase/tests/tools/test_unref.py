""" Unit tests for the pysoase.tools.unref module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.problems as co_probs
import pysoase.tests.conftest as tco
import pysoase.tools.unref as unref

testdata = os.path.join("tools","unref")

"""########################## Tests printUnrefed ###########################"""

class PrintUnrefedTests(unit.TestCase):
    desc = "Tests printUnrefed:"

    def setUp(self):
        self.patcher = mock.patch("pysoase.tools.unref.tqdm.tqdm",autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        self.patcher.stop()

    def testNoUnrefed(self):
        """ Test whether correct message is returned when no files are unrefed
        """
        refs = {"FT1":{"f1.file","f2.file"},"FT2":{"f4.file"}}
        files = {"FT1":{"f1.file"},"FT2":{"f4.file"}}
        exp = (0,"The following files are currently unreferenced:\n"
                 +"\tThere were no unreferenced files.\n")
        res = unref.printUnrefed(files,refs,[])
        self.assertEqual(res,exp)

    def testUnrefed(self):
        """ Test whether all unrefed files are returned
        """
        refs = {"FT1":{"f1.file","f2.file"},"FT2":{"f4.file"}}
        files = {"FT1":{"f1.file","f4.file"},"FT2":{"f4.file","f5.file"}}
        exp = (1,"The following files are currently unreferenced:\n"
                 +"\tFT1\n"
                 +"\t\tf4.file\n"
                 +"\tFT2\n"
                 +"\t\tf5.file\n"
        )
        res = unref.printUnrefed(files,refs,[])
        self.assertEqual(res,exp)

    def testUnrefedProbs(self):
        """ Test whether all unrefed files are returned
        """
        refs = {"FT1":{"f1.file","f2.file"},"FT2":{"f4.file"}}
        files = {"FT1":{"f1.file","f4.file"},"FT2":{"f4.file","f5.file"}}
        probs = [co_probs.BasicProblem("PROBLEMS!!!",
                severity=co_probs.ProblemSeverity.warn)]
        exp = (1,"The following files are currently unreferenced:\n"
                 +"\tFT1\n"
                 +"\t\tf4.file\n"
                 +"\tFT2\n"
                 +"\t\tf5.file\n"
                 +"\n\nATTENTION: The following problems occured during "
                  "reference gathering. The above list of files may contain "
                  "errors! Please correct problems and rerun unref check.\n"
                 +probs[0].toString(1)
        )
        res = unref.printUnrefed(files,refs,probs)
        self.assertEqual(res,exp)

"""########################### Tests UnrefFiles ############################"""

@mock.patch.dict("pysoase.tools.unref.co_consts.EXT_TO_FILEREF",
        {".t1":"Type1",".t2":"Type2"})
class UnrefFilesTests(unit.TestCase):
    desc = "Tests UnrefFiles:"

    def setUp(self):
        self.mmod = tco.genMockMod("./")
        self.patcher = mock.patch("pysoase.tools.unref.tqdm.tqdm",autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        self.patcher.stop()

    def testInvalidFiletype(self):
        """ Test whether invalid file type throws error
        """
        files = ["./GameInfo/invalid.inv"]
        inst = unref.UnrefFiles(files=files)
        with self.assertRaises(RuntimeError):
            inst._computeResults()

    def testSingleFileCorrect(self):
        """ Test whether single referenced file works
        """
        files = ["./GameInfo/refed1.t1"]
        exp = (0,"The following files are currently unreferenced:\n"
                 +"\tThere were no unreferenced files.\n")
        inst = unref.UnrefFiles(files=files)
        with mock.patch.object(inst.mod,"gatherRefs",autospec=True,
                spec_set=True,return_value=({"Type1":
                    {"refed1.t1","refed2.t4"}},[])) as mocked:
            res = inst._computeResults()
            self.assertEqual(res,exp)
            mocked.assert_called_once_with({"Type1"})

    def testMultiFileCorrect(self):
        """ Test whether multiple referenced files works
        """
        files = ["./GameInfo/refed1.t1","./GameInfo/refed2.t2"]
        exp = (0,"The following files are currently unreferenced:\n"
                 +"\tThere were no unreferenced files.\n")
        inst = unref.UnrefFiles(files=files)
        with mock.patch.object(inst.mod,"gatherRefs",autospec=True,
                spec_set=True,return_value=({"Type1":
                    {"refed1.t1","refed2.t4"},
                    "Type2":{"refed2.t2"}},[])) as mocked:
            res = inst._computeResults()
            self.assertEqual(res,exp)
            mocked.assert_called_once_with({"Type1","Type2"})

    def testMultiFileMixed(self):
        """ Test whether multiple referenced and unreferenced files works
        """
        files = ["./GameInfo/refed1.t1","./GameInfo/refed2.t2",
            "./GameInfo/unrefed1.t1"]
        exp = (1,"The following files are currently unreferenced:\n"
                 +"\tType1\n"
                 +"\t\tunrefed1.t1\n")
        inst = unref.UnrefFiles(files=files)
        with mock.patch.object(inst.mod,"gatherRefs",autospec=True,
                spec_set=True,return_value=({"Type1":
                    {"refed1.t1","refed2.t4"},
                    "Type2":{"refed2.t2"}},[])) as mocked:
            res = inst._computeResults()
            self.assertEqual(res,exp)
            mocked.assert_called_once_with({"Type1","Type2"})

"""########################### Tests UnrefTypes ############################"""

class UnrefTypesTests(unit.TestCase):
    desc = "Tests UnrefTypes:"

    def setUp(self):
        patcher = mock.patch.dict("pysoase.tools.unref.co_consts.FILEREF_TO_EXT",
                {"Type1":".t1","Type2":".t2"})
        patcher.start()
        self.inst = unref.UnrefTypes("./",{"Type1","Type2"})
        self.mmod = tco.genMockMod("./")
        patcher = mock.patch.object(self.inst,"mod",new=self.mmod)
        patcher.start()
        patcher = mock.patch("pysoase.tools.unref.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testInvalidType(self):
        """ Test whether invalid type throws error
        """
        with self.assertRaises(RuntimeError):
            self.inst = unref.UnrefTypes("./",types={"Foo","Bar"})

    def testNoUnrefed(self):
        """ Test whether txpes with no unrefed works
        """
        self.mmod.gatherFiles.return_value = {"Type1":{"refed1.t1"},
            "Type2":{"refed2.t2"}}
        self.mmod.gatherRefs.return_value = ({"Type1":{"refed1.t1"},
            "Type2":{"refed2.t2"}},[])
        exp = (0,"The following files are currently unreferenced:\n"
                 +"\tThere were no unreferenced files.\n")
        res = self.inst._computeResults()
        self.assertEqual(res,exp)
        self.mmod.gatherFiles.assert_called_once_with({".t1",".t2"})
        self.mmod.gatherRefs.assert_called_once_with({"Type1","Type2"})
