""" Unit tests for the pysoase.tools.modify module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
import pysoase.common.problems as co_probs
import pysoase.tools.modify as modify
from pysoase.mod.particles import AttribPos3d
from pysoase.mod.ui import StringReference

testdata = os.path.join("tools","modify")

"""####################### Tests Modify String Parser ######################"""

class ModifyParserTests(unit.TestCase):
    desc = "Tests Modify String Parser:"

    def testScalarAdd(self):
        """ Scalar addition integer parsed correctly
        """
        s = "ident:+20"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"+")
        self.assertEqual(res.change.value,"20")
        self.assertEqual(res.change.type,"scalar")

    def testScalarMult(self):
        """ Scalar multiplication integer parsed correctly
        """
        s = "ident:x20"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"x")
        self.assertEqual(res.change.value,"20")
        self.assertEqual(res.change.type,"scalar")

    def testScalarSub(self):
        """ Scalar substraction integer parsed correctly
        """
        s = "ident:-20"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"-")
        self.assertEqual(res.change.value,"20")
        self.assertEqual(res.change.type,"scalar")

    def testScalarSet(self):
        """ Scalar setting integer parsed correctly
        """
        s = "ident:=20"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"=")
        self.assertEqual(res.change.value,"20")
        self.assertEqual(res.change.type,"scalar")

    def testScalarAddFloat(self):
        """ Scalar addition float parsed correctly
        """
        s = "ident:+0.2"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"+")
        self.assertEqual(res.change.value,"0.2")
        self.assertEqual(res.change.type,"scalar")

    def testScalarMultFloat(self):
        """ Scalar multiplication float parsed correctly
        """
        s = "ident:x0.2"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"x")
        self.assertEqual(res.change.value,"0.2")
        self.assertEqual(res.change.type,"scalar")

    def testScalarSubFloat(self):
        """ Scalar substraction float parsed correctly
        """
        s = "ident:-0.2"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"-")
        self.assertEqual(res.change.value,"0.2")
        self.assertEqual(res.change.type,"scalar")

    def testScalarSetFloat(self):
        """ Scalar setting float parsed correctly
        """
        s = "ident:=0.2"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"=")
        self.assertEqual(res.change.value,"0.2")
        self.assertEqual(res.change.type,"scalar")

    def testStringSet(self):
        """ String setting parsed correctly
        """
        s = "ident:=\'foo\'"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"=")
        self.assertEqual(res.change.value,"foo")
        self.assertEqual(res.change.type,"string")

    def testBoolSet(self):
        """ Bool setting parsed correctly
        """
        s = "ident:=TRUE"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.operator,"=")
        self.assertEqual(res.change.value,"TRUE")
        self.assertEqual(res.change.type,"bool")

    def testVector3Set(self):
        """ Size 3 vector setting parsed
        """
        s = "ident:[=17,=19,=22]"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.change["value"][0:3],["=17","=19","=22"])
        self.assertEqual(res.change.type,"vector")

    def testVector3Mixed(self):
        """ Size 3 vector mixed operations parsed
        """
        s = "ident:[=17,+19,-22]"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.change.value[0:3],["=17","+19","-22"])
        self.assertEqual(res.change.type,"vector")

    def testVector4Mixed(self):
        """ Size 4 vector mixed operations parsed
        """
        s = "ident:[=17,+19,-22,x0.5]"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.change.value[0:4],["=17","+19","-22","x0.5"])
        self.assertEqual(res.change.type,"vector")

    def testVector4Missing(self):
        """ Size 4 vector missing elements
        """
        s = "ident:[,+19,-22,]"
        res = modify.g_mod.parseString(s)[0]
        self.assertEqual(res.identifier,"ident")
        self.assertEqual(res.change.value[0:4],["","+19","-22",""])
        self.assertEqual(res.change.type,"vector")

"""############################ Tests ModScalarOp ##########################"""

class ModScalarOpTests(unit.TestCase):
    desc = "Tests ModScalarOp:"

    @classmethod
    def setUpClass(cls):
        cls.strAddInt = "ident:+5"
        cls.strAddFloat = "ident:+0.5"
        cls.strSubInt = "ident:-5"
        cls.strMultInt = "ident:x5"
        cls.strSetInt = "ident:=5"
        cls.pAddInt = {
            "identifier":"ident",
            "operator":"+",
            "change":{
                "value":"5",
                "type":"scalar"
            }
        }
        cls.pAddFloat = {
            "identifier":"ident",
            "operator":"+",
            "change":{
                "value":"0.5",
                "type":"scalar"
            }
        }
        cls.pSubInt = {
            "identifier":"ident",
            "operator":"-",
            "change":{
                "value":"5",
                "type":"scalar"
            }
        }
        cls.pMultInt = {
            "identifier":"ident",
            "operator":"x",
            "change":{
                "value":"5",
                "type":"scalar"
            }
        }
        cls.pSetInt = {
            "identifier":"ident",
            "operator":"=",
            "change":{
                "value":"5",
                "type":"scalar"
            }
        }

    def setUp(self):
        self.numInt = co_attribs.AttribNum("ident",2)
        self.numFloat = co_attribs.AttribNum("ident",2.5)
        self.addInt = modify.ModScalarOp(**self.pAddInt)
        self.addFloat = modify.ModScalarOp(**self.pAddFloat)
        self.subInt = modify.ModScalarOp(**self.pSubInt)
        self.multInt = modify.ModScalarOp(**self.pMultInt)
        self.setInt = modify.ModScalarOp(**self.pSetInt)

    def testParse(self):
        """ Instances are created correctly from mod string
        """
        inst = modify.ModAttrib.factory(self.strAddInt)
        self.assertEqual(inst,self.addInt)
        inst = modify.ModAttrib.factory(self.strAddFloat)
        self.assertEqual(inst,self.addFloat)
        inst = modify.ModAttrib.factory(self.strSubInt)
        self.assertEqual(inst,self.subInt)
        inst = modify.ModAttrib.factory(self.strMultInt)
        self.assertEqual(inst,self.multInt)
        inst = modify.ModAttrib.factory(self.strSetInt)
        self.assertEqual(inst,self.setInt)

    def testCreationIdentifier(self):
        """ Identifier attribute set correctly
        """
        self.assertEqual(self.addInt.identifier,self.pAddInt["identifier"])
        self.assertEqual(self.addFloat.identifier,self.pAddFloat["identifier"])
        self.assertEqual(self.subInt.identifier,self.pSubInt["identifier"])
        self.assertEqual(self.multInt.identifier,self.pMultInt["identifier"])
        self.assertEqual(self.setInt.identifier,self.pSetInt["identifier"])

    def testCreationOp(self):
        """ Operator attribute set correctly
        """
        self.assertEqual(self.addInt.op,self.pAddInt["operator"])
        self.assertEqual(self.addFloat.op,self.pAddFloat["operator"])
        self.assertEqual(self.subInt.op,self.pSubInt["operator"])
        self.assertEqual(self.multInt.op,self.pMultInt["operator"])
        self.assertEqual(self.setInt.op,self.pSetInt["operator"])

    def testCreationVal(self):
        """ Val attribute set correctly
        """
        self.assertEqual(str(self.addInt.val),self.pAddInt["change"]["value"])
        self.assertEqual(str(self.addFloat.val),
                self.pAddFloat["change"]["value"])
        self.assertEqual(str(self.subInt.val),self.pSubInt["change"]["value"])
        self.assertEqual(str(self.multInt.val),self.pMultInt["change"]["value"])
        self.assertEqual(str(self.setInt.val),self.pSetInt["change"]["value"])

    def testFitsNumName(self):
        """ Fits AttribNum with correct name
        """
        self.assertTrue(self.addInt.fits(self.numInt))

    def testFitsNotWrongName(self):
        """ Does not fit AttribNum with wrong name
        """
        wrong = co_attribs.AttribNum("ident1",2)
        self.assertFalse(self.addInt.fits(wrong))

    def testFitsNotWrongType(self):
        """ Does not fit Non-AttribNum with correct name
        """
        wrong = co_attribs.AttribBool("ident",True)
        self.assertFalse(self.addInt.fits(wrong))

    def testApplyAddIntToInt(self):
        """ Addition Int correctly applied to Int AttribNum
        """
        self.addInt.apply(self.numInt)
        self.assertEqual(self.numInt.value,7)

    def testChange(self):
        """ Apply returns ModificationChange
        """
        res = self.addInt.apply(self.numInt)
        exp = modify.ModificationChange("ident",2,7)
        self.assertEqual(res,exp)

    def testApplyAddFloatToInt(self):
        """ Addition Float correctly applied to Int AttribNum
        """
        self.addFloat.apply(self.numInt)
        self.assertEqual(self.numInt.value,2.5)

    def testApplyAddFloatToFloat(self):
        """ Addition Float correctly applied to Float AttribNum
        """
        self.addFloat.apply(self.numFloat)
        self.assertEqual(self.numFloat.value,3.0)

    def testApplyAddIntToFloat(self):
        """ Addition Int correctly applied to Float AttribNum
        """
        self.addInt.apply(self.numFloat)
        self.assertEqual(self.numFloat.value,7.5)

    def testApplySubIntFromInt(self):
        """ Substraction Int correctly applied to Int AttribNum
        """
        self.subInt.apply(self.numInt)
        self.assertEqual(self.numInt.value,-3)

    def testApplySubIntFromFloat(self):
        """ Substraction Int correctly applied to Float AttribNum
        """
        self.subInt.apply(self.numFloat)
        self.assertEqual(self.numFloat.value,-2.5)

    def testApplyMultIntWithFloat(self):
        """ Multiplication Int correctly applied to Float AttribNum
        """
        self.multInt.apply(self.numFloat)
        self.assertEqual(self.numFloat.value,12.5)

    def testApplyMultIntWithInt(self):
        """ Multiplication Int correctly applied to Int AttribNum
        """
        self.multInt.apply(self.numInt)
        self.assertEqual(self.numInt.value,10)

    def testApplySetIntWithInt(self):
        """ Set Int correctly applied to Int AttribNum
        """
        self.setInt.apply(self.numInt)
        self.assertEqual(self.numInt.value,5)

"""############################ Tests ModStringOp ##########################"""

class ModStringOpTests(unit.TestCase):
    desc = "Tests ModStringOp:"

    @classmethod
    def setUpClass(cls):
        cls.strSet = "ident:=\'foo\'"
        cls.pSet = {
            "identifier":"ident",
            "operator":"=",
            "change":{
                "value":"foo",
                "type":"string"
            }
        }

    def setUp(self):
        self.aref = StringReference("ident","bar")
        self.astring = co_attribs.AttribString("ident","bar")
        self.set = modify.ModStringOp(**self.pSet)

    def testParse(self):
        """ Instances are created correctly from mod string
        """
        inst = modify.ModAttrib.factory(self.strSet)
        self.assertEqual(inst,self.set)

    def testCreationIdentifier(self):
        """ Identifier attribute set correctly
        """
        self.assertEqual(self.set.identifier,self.pSet["identifier"])

    def testCreationOp(self):
        """ Operator attribute set correctly
        """
        self.assertEqual(self.set.op,self.pSet["operator"])

    def testCreationVal(self):
        """ Val attribute set correctly
        """
        self.assertEqual(self.set.val,self.pSet["change"]["value"])

    def testFitsNameAref(self):
        """ Fits StringReference with correct name
        """
        self.assertTrue(self.set.fits(self.aref))

    def testFitsNameAString(self):
        """ Fits AttribString with correct name
        """
        self.assertTrue(self.set.fits(self.astring))

    def testFitsNotWrongName(self):
        """ Does not fit AttribString with wrong name
        """
        wrong = co_attribs.AttribString("ident1","foo")
        self.assertFalse(self.set.fits(wrong))

    def testApplySetAttribString(self):
        """ Attribute set correctly for AttribString
        """
        self.set.apply(self.astring)
        self.assertEqual(self.astring.value,"foo")

    def testApplySetAttribRef(self):
        """ Attribute set correctly for StringReference
        """
        self.set.apply(self.aref)
        self.assertEqual(self.aref.value,"foo")

    def testChange(self):
        """ Apply returns ModificationChange
        """
        res = self.set.apply(self.aref)
        exp = modify.ModificationChange("ident","bar","foo")
        self.assertEqual(res,exp)

"""############################# Tests ModBoolOp ###########################"""

class ModBoolOpTests(unit.TestCase):
    desc = "Tests ModBoolOp:"

    @classmethod
    def setUpClass(cls):
        cls.strSetTrue = "ident:=TRUE"
        cls.strSetFalse = "ident:=FALSE"
        cls.pSetTrue = {
            "identifier":"ident",
            "operator":"=",
            "change":{
                "value":"TRUE",
                "type":"bool"
            }
        }
        cls.pSetFalse = {
            "identifier":"ident",
            "operator":"=",
            "change":{
                "value":"FALSE",
                "type":"bool"
            }
        }

    def setUp(self):
        self.abool = co_attribs.AttribBool("ident",True)
        self.astring = co_attribs.AttribString("ident","bar")
        self.setTrue = modify.ModBoolOp(**self.pSetTrue)
        self.setFalse = modify.ModBoolOp(**self.pSetFalse)

    def testParse(self):
        """ Instances are created correctly from mod string
        """
        inst = modify.ModAttrib.factory(self.strSetTrue)
        self.assertEqual(inst,self.setTrue)
        inst = modify.ModAttrib.factory(self.strSetFalse)
        self.assertEqual(inst,self.setFalse)

    def testCreationIdentifier(self):
        """ Identifier attribute set correctly
        """
        self.assertEqual(self.setTrue.identifier,self.pSetTrue["identifier"])
        self.assertEqual(self.setFalse.identifier,self.pSetFalse["identifier"])

    def testCreationOp(self):
        """ Operator attribute set correctly
        """
        self.assertEqual(self.setTrue.op,self.pSetTrue["operator"])
        self.assertEqual(self.setFalse.op,self.pSetFalse["operator"])

    def testCreationVal(self):
        """ Val attribute set correctly
        """
        self.assertEqual(self.setTrue.val,True)
        self.assertEqual(self.setFalse.val,False)

    def testFitsName(self):
        """ Fits AttribBool with correct name
        """
        self.assertTrue(self.setTrue.fits(self.abool))

    def testFitsNotNameAString(self):
        """ Does not fit AttribString with correct name
        """
        self.assertFalse(self.setTrue.fits(self.astring))

    def testFitsNotWrongName(self):
        """ Does not fit AttribBool with wrong name
        """
        wrong = co_attribs.AttribBool("ident1",False)
        self.assertFalse(self.setTrue.fits(wrong))

    def testApplySetTrue(self):
        """ Attribute set correctly for AttribBool
        """
        self.setFalse.apply(self.abool)
        self.assertEqual(self.abool.value,False)

    def testChange(self):
        """ Apply returns ModificationChange
        """
        res = self.setFalse.apply(self.abool)
        exp = modify.ModificationChange("ident",True,False)
        self.assertEqual(res,exp)

"""############################# Tests ModVector ###########################"""

class ModVectorTests(unit.TestCase):
    desc = "Tests ModVector:"

    @classmethod
    def setUpClass(cls):
        cls.strVec2d = "ident:[+17,=12]"
        cls.strVec3d = "ident:[+17,=12,=2]"
        cls.strVec4d = "ident:[=2,-8,x3,+1.5]"
        cls.strVec4dMissing = "ident:[,-8,x3,+1.5]"
        cls.pVec2d = {
            "identifier":"ident",
            "change":{
                "value":["+17","=12"],
                "type":"vector"
            }
        }
        cls.pVec3d = {
            "identifier":"ident",
            "change":{
                "value":["+17","=12","=2"],
                "type":"vector"
            }
        }
        cls.pVec4d = {
            "identifier":"ident",
            "change":{
                "value":["=2","-8","x3","+1.5"],
                "type":"vector"
            }
        }
        cls.pVec4dMissing = {
            "identifier":"ident",
            "change":{
                "value":["","-8","x3","+1.5"],
                "type":"vector"
            }
        }

    def setUp(self):
        self.vec2d = modify.ModVectorOp(**self.pVec2d)
        self.vec3d = modify.ModVectorOp(**self.pVec3d)
        self.vec4d = modify.ModVectorOp(**self.pVec4d)
        self.vec4dMissing = modify.ModVectorOp(**self.pVec4dMissing)
        self.pos2d = co_attribs.AttribPos2d("ident",(1.5,6))
        self.pos3d = AttribPos3d("ident",(1.5,6,4))
        self.pos4d = co_attribs.AttribPos4d("ident",(1.5,6,7,8.5))

    def testParse(self):
        """ Instances are created correctly from mod string
        """
        inst = modify.ModAttrib.factory(self.strVec2d)
        self.assertEqual(inst,self.vec2d)
        inst = modify.ModAttrib.factory(self.strVec3d)
        self.assertEqual(inst,self.vec3d)
        inst = modify.ModAttrib.factory(self.strVec4d)
        self.assertEqual(inst,self.vec4d)
        inst = modify.ModAttrib.factory(self.strVec4dMissing)
        self.assertEqual(inst,self.vec4dMissing)

    def testCreationIdentifier(self):
        """ Identifier attribute set correctly
        """
        self.assertEqual(self.vec2d.identifier,self.pVec2d["identifier"])
        self.assertEqual(self.vec3d.identifier,self.pVec3d["identifier"])
        self.assertEqual(self.vec4d.identifier,self.pVec4d["identifier"])
        self.assertEqual(self.vec4dMissing.identifier,
                self.pVec4dMissing["identifier"])

    def testCreationVal(self):
        """ Val attribute set correctly
        """
        self.assertEqual(self.vec2d.val,[("+",17),("=",12)])
        self.assertEqual(self.vec3d.val,[("+",17),("=",12),("=",2)])
        self.assertEqual(self.vec4d.val,[("=",2),("-",8),("x",3),("+",1.5)])
        self.assertEqual(self.vec4dMissing.val,[None,("-",8),("x",3),("+",1.5)])

    def testFitsName(self):
        """ Fits AttribPos with correct name and length
        """
        self.assertTrue(self.vec2d.fits(self.pos2d))
        self.assertTrue(self.vec3d.fits(self.pos3d))
        self.assertTrue(self.vec4d.fits(self.pos4d))
        self.assertTrue(self.vec4dMissing.fits(self.pos4d))

    def testFitsNotWrongName(self):
        """ Does not fit AttribPos with wrong name
        """
        wrong = co_attribs.AttribPos2d("ident1",(1.5,6))
        self.assertFalse(self.vec2d.fits(wrong))
        wrong = AttribPos3d("ident1",(1.5,6,7))
        self.assertFalse(self.vec3d.fits(wrong))
        wrong = co_attribs.AttribPos2d("ident1",(1.5,6,7,8.5))
        self.assertFalse(self.vec4d.fits(wrong))
        self.assertFalse(self.vec4dMissing.fits(wrong))

    def testFitsSizeTooSmall(self):
        """ Too small AttribPos does not fit
        """
        self.assertFalse(self.vec4d.fits(self.pos2d))

    def testFitsSizeTooLarge(self):
        """ Too large AttribPos does not fit
        """
        self.assertFalse(self.vec2d.fits(self.pos4d))

    def testApply2d(self):
        """ Modifications applied correctly for 2d vector
        """
        self.vec2d.apply(self.pos2d)
        self.assertEqual(self.pos2d.value,(18.5,12))

    def testApply3d(self):
        """ Modifications applied correctly for 3d vector
        """
        self.vec3d.apply(self.pos3d)
        self.assertEqual(self.pos3d.value,(18.5,12,2))

    def testApply4d(self):
        """ Modifications applied correctly for 4d vector
        """
        self.vec4d.apply(self.pos4d)
        self.assertEqual(self.pos4d.value,(2,-2,21,10.0))

    def testApply4dMissing(self):
        """ Modifications applied correctly for 4d vector with missing elems
        """
        self.vec4dMissing.apply(self.pos4d)
        self.assertEqual(self.pos4d.value,(1.5,-2,21,10.0))

    def testChange(self):
        """ Apply returns ModificationChange
        """
        res = self.vec4d.apply(self.pos4d)
        exp = modify.ModificationChange("ident",(1.5,6,7,8.5),(2,-2,21,10.0))
        self.assertEqual(res,exp)

"""######################### Tests ModificationChange ######################"""

class ModificationChangeTests(unit.TestCase):
    desc = "Tests ModificationChange:"

    def setUp(self):
        self.cScalar = modify.ModificationChange(
                modFile="test.file",
                identifier="ident",
                oldVal=3,
                newVal=5
        )
        self.cString = modify.ModificationChange(
                modFile="test.file",
                identifier="ident",
                oldVal="foo",
                newVal="bar"
        )
        self.cVec = modify.ModificationChange(
                modFile="test.file",
                identifier="ident",
                oldVal=(1,2),
                newVal=(3,4)
        )

    def testCreationNoFile(self):
        """ Change is created correctly without modFile
        """
        inst = modify.ModificationChange(
                identifier="ident",
                oldVal=3,
                newVal=2
        )
        self.assertEqual(inst.modFile,None)

    def testCreationModFile(self):
        """ Attrib modFile set correctly during creation
        """
        self.assertEqual(self.cScalar.modFile,"test.file")

    def testCreationOldVal(self):
        """ Attrib oldVal set correctly during creation
        """
        self.assertEqual(self.cScalar.oldVal,3)

    def testCreationNewVal(self):
        """ Attrib newVal set correctly during creation
        """
        self.assertEqual(self.cScalar.newVal,5)

    def testCreationIdentifier(self):
        """ Attrib identifier set correctly during creation
        """
        self.assertEqual(self.cScalar.identifier,"ident")

    def testToStringScalar(self):
        """ Scalar change is printed correctly
        """
        exp = ("test.file:\n"
               "\tChanged \'ident\' from \'3\' to \'5\'\n")
        self.assertEqual(self.cScalar.toString(0),exp)

    def testToStringString(self):
        """ String change is printed correctly
        """
        exp = ("test.file:\n"
               "\tChanged \'ident\' from \'foo\' to \'bar\'\n")
        self.assertEqual(self.cString.toString(0),exp)

    def testToStringVec(self):
        """ Vector change is printed correctly
        """
        exp = ("test.file:\n"
               "\tChanged \'ident\' from \'(1, 2)\' to \'(3, 4)\'\n")
        self.assertEqual(self.cVec.toString(0),exp)

"""############################### Tests Modify ############################"""

class ModifyTests(unit.TestCase):
    desc = "Tests Modify command:"

    @classmethod
    def setUpClass(cls):
        cls.path = "./cmd"

    def setUp(self):
        self.files = [os.path.join(self.path,"f1.entity"),
            os.path.join(self.path,"f2.entity")]
        self.patcher = mock.patch("pysoase.tools.modify.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcher.start()
        self.mockF1 = mock.create_autospec(
                co_basics.IsFile,spec_set=True)
        self.mockF1.createInstance.return_value = self.mockF1
        self.mockF2 = mock.create_autospec(
                co_basics.IsFile,spec_set=True)
        self.mockF2.createInstance.return_value = self.mockF2
        self.mockGetClass = mock.patch(
                "pysoase.tools.modify.tools.getFileClass",
                spec_set=True,side_effect=[self.mockF1,self.mockF2])
        self.mockGetClass.start()

    def tearDown(self):
        self.patcher.stop()
        self.mockGetClass.stop()

    def testModsSingle(self):
        """ Single modification string parsed corrrectly
        """
        pStr = "ident:+3.5"
        exp = modify.ModAttrib.factory(pStr)
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        self.assertEqual(inst.modifications,[exp])

    def testModsMultiple(self):
        """ Single modification string parsed corrrectly
        """
        pStr = "ident:+3.5;ident2:=2.3"
        exp = [modify.ModAttrib.factory("ident:+3.5"),
            modify.ModAttrib.factory("ident2:=2.3")]
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        self.assertEqual(inst.modifications,exp)

    def testDirInput(self):
        """ Directories in file list trigger exception
        """
        pStr = "ident:+3.5;ident2:=2.3"
        f = [os.path.join(self.path,"dir1"),os.path.join(self.path,"dir2")]
        exp = ("The modify command does not support directories as input:\n"
               "\t"+f[0]+"\n\t"+f[1])
        with self.assertRaisesRegexp(ValueError,exp):
            modify.Modify(files=f,moddir=self.path,modStr=pStr)

    def testModifyCalls(self):
        """ Modify calls modify methods with correct modifications
        """
        pStr = "ident:+3.5;ident2:=2.3"
        self.mockF1.modify.return_value = []
        self.mockF2.modify.return_value = []
        mods = [modify.ModAttrib.factory("ident:+3.5"),
            modify.ModAttrib.factory("ident2:=2.3")]
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        inst._computeResults()
        self.mockF1.modify.assert_called_once_with(mods)
        self.mockF2.modify.assert_called_once_with(mods)

    def testOutputNoChange(self):
        """ Special message is shown when no changes are made
        """
        pStr = "ident:+3.5;ident2:=2.3"
        self.mockF1.modify.return_value = []
        self.mockF2.modify.return_value = []
        exp = ("All files were scanned, but no changes were made.\n"
               +"Please check your modification string for typos in the "
               +"identifier names.\n")
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        retVal,res = inst._computeResults()
        self.assertEqual(retVal,1)
        self.assertEqual(res,exp)

    def testOutputChangeOneFile(self):
        """ Correct changes output for changes in only one file
        """
        pStr = "ident:+3.5;ident2:=2.3"
        change = modify.ModificationChange("ident",1.5,5.0,"f1.entity")
        self.mockF1.modify.return_value = [change]
        self.mockF2.modify.return_value = []
        exp = ("The following changes were made:\n"
               +change.toString(1)
               +"\tf2.entity\n"
               +"\t\tNo changes made\n")
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        retVal,res = inst._computeResults()
        self.assertEqual(retVal,0)
        self.assertEqual(res,exp)

    def testWriteChangeOneFile(self):
        """ Write out only for files which actually were changed
        """
        pStr = "ident:+3.5;ident2:=2.3"
        change = modify.ModificationChange("ident",1.5,5.0,"f1.entity")
        self.mockF1.modify.return_value = [change]
        self.mockF2.modify.return_value = []
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        inst._computeResults()
        self.mockF1.writeOut.assert_called_once_with()
        self.assertFalse(self.mockF2.writeOut.called)

    def testOutputChangeBothFiles(self):
        """ Correct changes output for changes in two files
        """
        pStr = "ident:+3.5;ident2:=2.3"
        change0 = modify.ModificationChange("ident",1.5,5.0,"f1.entity")
        change1 = modify.ModificationChange("ident2",1.5,2.3,"f1.entity")
        change2 = modify.ModificationChange("ident",1.5,4.0,"f2.entity")
        change3 = modify.ModificationChange("ident2",1.5,2.3,"f2.entity")
        self.mockF1.modify.return_value = [change0,change1]
        self.mockF2.modify.return_value = [change2,change3]
        exp = ("The following changes were made:\n"
               +change0.toString(1)
               +change1.toString(1)
               +change2.toString(1)
               +change3.toString(1))
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        retVal,res = inst._computeResults()
        self.assertEqual(retVal,0)
        self.assertEqual(res,exp)

    def testWriteChangeBothFiles(self):
        """ WriteOut called for both files
        """
        pStr = "ident:+3.5;ident2:=2.3"
        change0 = modify.ModificationChange("ident",1.5,5.0,"f1.entity")
        change1 = modify.ModificationChange("ident2",1.5,2.3,"f1.entity")
        change2 = modify.ModificationChange("ident",1.5,4.0,"f2.entity")
        change3 = modify.ModificationChange("ident2",1.5,2.3,"f2.entity")
        self.mockF1.modify.return_value = [change0,change1]
        self.mockF2.modify.return_value = [change2,change3]
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        inst._computeResults()
        self.mockF1.writeOut.assert_called_once_with()
        self.mockF2.writeOut.assert_called_once_with()

    def testOutputChangeWithProblem(self):
        """ Correct output with change and parsing error
        """
        pStr = "ident:+3.5;ident2:=2.3"
        change0 = modify.ModificationChange("ident",1.5,5.0,"f1.entity")
        prob = co_probs.BasicProblem("Parsing Problem",probFile="f1.entity",
                severity=co_probs.ProblemSeverity.error)
        self.mockF1.modify.return_value = [change0]
        self.mockF2.createInstance.return_value = [prob]
        exp = ("The following changes were made:\n"
               +change0.toString(1))
        exp += ("The following problems occured, thus no change was made "
                "to these files:\n"
                +prob.toString(1))
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        retVal,res = inst._computeResults()
        self.assertEqual(retVal,1)
        self.assertEqual(res,exp)

    def testWriteChangeWithProblem(self):
        """ Only files with changes and no problems are written out
        """
        pStr = "ident:+3.5;ident2:=2.3"
        change0 = modify.ModificationChange("ident",1.5,5.0,"f1.entity")
        prob = co_probs.BasicProblem("Parsing Problem",probFile="f1.entity",
                severity=co_probs.ProblemSeverity.error)
        self.mockF1.modify.return_value = [change0]
        self.mockF2.createInstance.return_value = [prob]
        inst = modify.Modify(files=self.files,moddir=self.path,modStr=pStr)
        inst._computeResults()
        self.mockF1.writeOut.assert_called_once_with()
        self.assertFalse(self.mockF2.writeOut.called)
