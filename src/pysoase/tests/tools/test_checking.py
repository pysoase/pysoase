""" Unit tests for the pysoase.tools.checking module.
"""

import datetime
import glob
import os
import pickle
import tempfile
import unittest as unit
from unittest import mock

import pysoase.common.problems as co_probs
import pysoase.mod.manifest as manifest
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco
import pysoase.tools.checking as checking
import pysoase.tools.tools as tools

testdata = os.path.join("tools","checking")

"""########################## Tests Syntax Checker #########################"""

class CheckerSyntaxNoProbs(unit.TestCase):
    desc = "Test syntax checker without probs occuring:"

    def setUp(self):
        self.mockMan = mock.create_autospec(manifest.ManifestBase,
                spec_set=True)
        self.mockMan.parse.return_value = (True,[])
        self.mockString = mock.create_autospec(ui.StringFile,spec_set=True)
        self.mockString.parse.return_value = (True,[])
        self.patcherFiles = mock.patch.dict(tools.EXT_TO_CLASS,values={
            ".manifest":self.mockMan,
            ".str":self.mockString})
        self.patcherFiles.start()
        self.patcher = mock.patch("pysoase.tools.checking.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        self.patcherFiles.stop()
        self.patcher.stop()

    def testSingleDirectory(self):
        """ Test whether a single directory is checked correctly
        """
        files = ["./Checker/String"]
        res = checking.CheckerSyntax(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(res.mod.strings,"checkSyntax",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = res.check()
            mocked.assert_called_once_with()
            self.assertEqual(res,[])

    def testSingleFile(self):
        """ Test whether a single file is checked correctly
        """
        files = ["./Checker/entity.manifest"]
        res = checking.CheckerSyntax(files=files,
                severity=co_probs.ProblemSeverity.warn)
        res = res.check()
        self.mockMan.parse.assert_called_once_with(os.path.abspath(files[0]))
        self.assertEqual(res,[])

    def testMultipleFiles(self):
        """ Test whether multiple files are checked correctly
        """
        files = ["./Checker/brush.manifest","./Checker/String/foo.str"]
        res = checking.CheckerSyntax(files=files,
                severity=co_probs.ProblemSeverity.warn)
        res = res.check()
        self.assertEqual(self.mockMan.parse.call_count,1)
        self.assertEqual(self.mockString.parse.call_count,1)
        self.mockMan.parse.assert_called_once_with(
                os.path.abspath("./Checker/brush.manifest"))
        self.mockString.parse.assert_called_once_with(
                os.path.abspath("./Checker/String/foo.str"))
        self.assertEqual(res,[])

class CheckerSyntaxProbs(unit.TestCase):
    desc = "Test syntax checker with probs occuring:"

    def setUp(self):
        self.mockMan = mock.create_autospec(manifest.ManifestBase,
                spec_set=True)
        self.mockString = mock.create_autospec(ui.StringFile,spec_set=True)
        self.mockString.parse.return_value = (True,[])
        self.patcherFiles = mock.patch.dict(tools.EXT_TO_CLASS,values={
            ".manifest":self.mockMan,
            ".str":self.mockString})

        self.manProb = co_probs.BasicProblem(severity=co_probs.ProblemSeverity.warn,
                desc="Prob1")
        self.mockMan.parse.return_value = (False,[self.manProb])
        self.stringProb = co_probs.BasicProblem(severity=co_probs.ProblemSeverity.warn,
                desc="Prob2")
        self.mockString.parse.return_value = (False,[self.stringProb])
        self.patcherFiles.start()
        self.patcher = mock.patch("pysoase.tools.checking.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        self.patcherFiles.stop()
        self.patcher.stop()

    def testSingleDirectory(self):
        """ Test whether a single directory is checked correctly
        """
        files = ["./Checker/String"]
        res = checking.CheckerSyntax(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(res.mod.strings,"checkSyntax",autospec=True,
                spec_set=True,return_value=[self.stringProb]) as mocked:
            res = res.check()
            mocked.assert_called_once_with()
            self.assertEqual(res,[self.stringProb])

    def testSingleFile(self):
        """ Test whether a single file is checked correctly
        """
        files = ["./Checker/entity.manifest"]
        res = checking.CheckerSyntax(files=files,
                severity=co_probs.ProblemSeverity.warn)
        res = res.check()
        self.mockMan.parse.assert_called_once_with(os.path.abspath(files[0]))
        self.assertEqual(res,[self.manProb])

    def testMultipleFiles(self):
        """ Test whether multiple files are checked correctly
        """
        files = ["./Checker/brush.manifest","./Checker/String/foo.str"]
        res = checking.CheckerSyntax(files=files,
                severity=co_probs.ProblemSeverity.warn)
        res = res.check()
        self.assertEqual(self.mockMan.parse.call_count,1)
        self.assertEqual(self.mockString.parse.call_count,1)
        self.mockMan.parse.assert_called_once_with(
                os.path.abspath("./Checker/brush.manifest"))
        self.mockString.parse.assert_called_once_with(
                os.path.abspath("./Checker/String/foo.str"))
        self.assertEqual(res,[self.manProb,self.stringProb])

"""######################### Tests General Checker #########################"""

class CheckerGeneralNoProbs(unit.TestCase):
    desc = "Test general checker without probs occuring:"

    def setUp(self):
        self.patcher = mock.patch("pysoase.tools.checking.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        self.patcher.stop()

    def testSingleDirectory(self):
        """ Test whether a single directory is checked correctly
        """
        files = ["./Checker/String"]
        checker = checking.CheckerGeneral(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(checker.mod.strings,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = checker.check()
            mocked.assert_called_once_with()
            self.assertEqual(res,[])

    def testSingleFile(self):
        """ Test whether a single file is checked correctly
        """
        files = ["./Checker/entity.manifest"]
        checker = checking.CheckerGeneral(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(checker.mod.entities,"checkFile",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = checker.check()
            mocked.assert_called_once_with(os.path.abspath(files[0]))
            self.assertEqual(res,[])

    def testMultipleFiles(self):
        """ Test whether multiple files are checked correctly
        """
        files = ["./Checker/entity.manifest","./Checker/String/foo.str"]
        checker = checking.CheckerGeneral(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(checker.mod.entities,"checkFile",autospec=True,
                spec_set=True,return_value=[]) as mockEnt, \
                mock.patch.object(checker.mod.strings,"checkFile",
                        autospec=True,spec_set=True,return_value=[]) as mockStr:
            res = checker.check()
            self.assertEqual(mockEnt.call_count,1)
            self.assertEqual(mockStr.call_count,1)
            mockEnt.assert_called_once_with(os.path.abspath(files[0]))
            mockStr.assert_called_once_with(os.path.abspath(files[1]))
            self.assertEqual(res,[])

class CheckerGeneralProbs(unit.TestCase):
    desc = "Test General checker with probs occuring:"

    def setUp(self):
        self.manProb = co_probs.BasicProblem(severity=co_probs.ProblemSeverity.warn,
                desc="Prob1")
        self.stringProb = co_probs.BasicProblem(severity=co_probs.ProblemSeverity.warn,
                desc="Prob2")
        self.patcher = mock.patch("pysoase.tools.checking.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        self.patcher.stop()

    def testSingleFile(self):
        """ Test whether a single file is checked correctly
        """
        files = ["./Checker/entity.manifest"]
        checker = checking.CheckerGeneral(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(checker.mod.entities,"checkFile",autospec=True,
                spec_set=True,return_value=[self.manProb]) as mocked:
            res = checker.check()
            mocked.assert_called_once_with(os.path.abspath(files[0]))
            self.assertEqual(res,[self.manProb])

    def testSingleDirectory(self):
        """ Test whether a single directory is checked correctly
        """
        files = ["./Checker/String"]
        checker = checking.CheckerGeneral(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(checker.mod.strings,"check",autospec=True,
                spec_set=True,return_value=[self.stringProb]) as mocked:
            res = checker.check()
            mocked.assert_called_once_with()
            self.assertEqual(res,[self.stringProb])

    def testMultipleFiles(self):
        """ Test whether multiple files are checked correctly
        """
        files = ["./Checker/entity.manifest","./Checker/String/foo.str"]
        checker = checking.CheckerGeneral(files=files,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(checker.mod.entities,"checkFile",autospec=True,
                spec_set=True,return_value=[self.manProb]) as mockEnt, \
                mock.patch.object(checker.mod.strings,"checkFile",
                        autospec=True,spec_set=True,
                        return_value=[self.stringProb]) as mockStr:
            res = checker.check()
            self.assertEqual(mockEnt.call_count,1)
            self.assertEqual(mockStr.call_count,1)
            mockEnt.assert_called_once_with(os.path.abspath(files[0]))
            mockStr.assert_called_once_with(os.path.abspath(files[1]))
            self.assertEqual(len(res),2)
            self.assertIn(self.manProb,res)
            self.assertIn(self.stringProb,res)

"""########################## Tests Entity Checker #########################"""

class CheckerEntitiesTests(unit.TestCase):
    desc = "Tests CheckerEntities"

    def setUp(self):
        self.mmod = tco.genMockMod("./entities")
        self.mmod.entities.ENT_TO_CLASS = {"Debris":None,"Frigate":None}
        patcher = mock.patch("pysoase.tools.tools.mod.Mod",autospec=True,
                spec_set=True,return_value=self.mmod)
        patcher.start()
        self.patcher = mock.patch("pysoase.tools.checking.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCheck(self):
        """ Check whether check checks all given types
        """
        self.mmod.entities.checkType.return_value = []
        types = ["Debris","Frigates"]
        inst = checking.CheckerEntities("./entities/GameInfo",types=types,
                severity=co_probs.ProblemSeverity.warn)
        res = inst.check()
        self.assertEqual(res,[])
        self.mmod.entities.checkType.assert_called_once_with(types)

    def testCheckNoTypes(self):
        """ Check whether check checks all types if none are provided
        """
        self.mmod.entities.checkType.return_value = []
        self.mmod.entities.ENT_TO_CLASS = {"Debris":None,"Frigate":None}
        types = ["Debris","Frigate"]
        inst = checking.CheckerEntities("./entities/GameInfo",types="",
                severity=co_probs.ProblemSeverity.warn)
        res = inst.check()
        self.assertEqual(res,[])
        self.mmod.entities.checkType.assert_called_once_with(types)

"""########################### Tests Mod Checker ###########################"""

class CheckerModTests(unit.TestCase):
    desc = "Tests CheckerMod:"

    @classmethod
    def setUpClass(cls):
        cls.moddir = os.path.abspath("./Checker")

    def testModDir(self):
        """ Test whether mod directory is set correctly
        """
        inst = checking.CheckerMod(directory=self.moddir,strict=False,
                severity=co_probs.ProblemSeverity.warn)
        self.assertEqual(inst.mod.moddir,self.moddir)

    def testModDirParameter(self):
        """ Test whether mod directory is set correctly with --mod parameter
        """
        inst = checking.CheckerMod(directory=self.moddir,moddir="Wrong",
                strict=False,
                severity=co_probs.ProblemSeverity.warn)
        self.assertEqual(inst.mod.moddir,self.moddir)

    def testChecking(self):
        """ Test whether problems are returned correctly
        """
        inst = checking.CheckerMod(directory=self.moddir,strict=False,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(inst.mod,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = inst.check()
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(False)

    def testCheckingStrict(self):
        """ Test whether strict checking works
        """
        inst = checking.CheckerMod(directory=self.moddir,strict=True,
                severity=co_probs.ProblemSeverity.warn)
        with mock.patch.object(inst.mod,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = inst.check()
            self.assertEqual(res,[])
            mocked.assert_called_once_with(True)

"""############################# Tests Checker #############################"""

class CheckerTests(unit.TestCase):
    desc = "Tests Checker:"

    @classmethod
    def setUpClass(cls):
        cls.patcherAbstract = mock.patch.object(checking.Checker,
                "__abstractmethods__",set())
        cls.patcherAbstract.start()

    def setUp(self):
        self.probs = [co_probs.BasicProblem("Foo",severity=co_probs.ProblemSeverity.warn),
            co_probs.BasicProblem("Bar",severity=co_probs.ProblemSeverity.error)]
        self.tmpdir = tempfile.TemporaryDirectory()
        self.inst = checking.Checker(moddir="./",files=[],
                severity=co_probs.ProblemSeverity.warn,store=self.tmpdir.name)
        self.patcher = mock.patch.object(self.inst,"check",spec_set=True,
                return_value=self.probs)
        self.patcherTqdm = mock.patch("pysoase.tools.checking.tqdm.tqdm",
                autospec=True,
                spec_set=True)
        self.patcherTqdm.start()
        self.mockCheck = self.patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.patcherAbstract.stop()

    def tearDown(self):
        self.tmpdir.cleanup()
        self.patcher.stop()
        self.patcherTqdm.stop()

    def testFilename(self):
        """ Filename for problems file created correctly
        """
        currTime = datetime.datetime.utcnow()
        regexp = currTime.strftime("%Y%m%d%H")+"([0-5][0-9]){2}\.probs"
        self.inst._computeResults()
        path = glob.glob(os.path.join(self.tmpdir.name,"*.probs"))[0]
        self.assertRegex(os.path.basename(path),regexp)

    def testPickleEmpty(self):
        """ Empty list pickled correctly
        """
        self.mockCheck.return_value = []
        self.inst._computeResults()
        path = glob.glob(os.path.join(self.tmpdir.name,"*.probs"))[0]
        with open(path,'rb') as f:
            probs = pickle.load(f)
            self.assertEqual(probs,[])

    def testPickleFull(self):
        """ List of problems pickled correctly
        """
        self.inst._computeResults()
        path = glob.glob(os.path.join(self.tmpdir.name,"*.probs"))[0]
        with open(path,'rb') as f:
            probs = pickle.load(f)
            self.assertEqual(probs,self.probs)
