""" Some common functions for pysoase unit tests.
"""

import os
import unittest.mock as mock

import pytest

from pysoase.common.problems import BasicProblem
import pysoase.entities.component as comp
import pysoase.mod.audio as audio
import pysoase.mod.mod as mod
import pysoase.mod.texanim as anim
import pysoase.mod.ui as ui

def genMockMod(modpath,rebrefpath=None):
    mockMod = mock.MagicMock()
    mockMod.moddir = modpath
    mockMod.rebrefdir = rebrefpath
    mockMod.dirlist_mod = {}
    mockMod.dirlist_rebref = {}
    mockMod.dirlist_bundled = {}
    mockMod.quiet = False
    mockMod.mock_add_spec(mod.Mod,spec_set=True)
    mockMod.entities = mock.create_autospec(comp.ModEntities,
            spec_set=True)
    mockMod.brushes = mock.create_autospec(ui.ModBrushes,
            spec_set=True)
    mockMod.strings = mock.create_autospec(ui.ModStrings,
            spec_set=True)
    mockMod.texanims = mock.create_autospec(anim.ModTextureAnim,
            spec_set=True)
    mockMod.audio = mock.create_autospec(audio.ModAudio,spec_set=True)
    return mockMod

def expandParseDict(cls,parseDict):
    for key,val in parseDict.items():
        setattr(cls,key,val)

@pytest.fixture(scope="module",autouse=True)
def testdir(request):
    olddir = os.getcwd()
    os.chdir(os.path.join(olddir,"testdata",request.module.testdata))
    yield
    os.chdir(olddir)

@pytest.fixture(scope="class")
def mockTqdm(request):
    patcher = mock.patch(request.module.testmodule+".tqdm.tqdm",
            autospec=True,spec_set=True)
    patcher.start()
    yield
    patcher.stop()

@pytest.fixture(scope="class")
def mockProb():
    prob = mock.MagicMock(probFile=None,probLine=1)
    prob.mock_add_spec(BasicProblem,spec_set=True)
    return prob
