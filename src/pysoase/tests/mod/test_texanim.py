""" Unit tests for the pysoase.mod.texanim module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.filehandling as co_files
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.texanim as anim
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","texanim")

"""######################### Tests TextureAnimRef ##########################"""

class TextureAnimRefTests(unit.TestCase):
    desc = "Test TextureAnimRef:"

    def setUp(self):
        self.parseString = "ident \"ta2\""
        self.inst = anim.TextureAnimRef(
                identifier="ident",
                val="ta2"
        )
        self.mmod = tco.genMockMod("./comp")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = anim.TextureAnimRef(**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckExisting(self):
        """ Test whether check retuns empty list of problems for existing anim
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMissing(self):
        """ Test whether check retuns empty list of problems for existing anim
        """
        inst = anim.TextureAnimRef(identifier="ident",val="missing")
        res = inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

"""######################## Tests TextureAnimation #########################"""

class TextureAnimationTests(unit.TestCase):
    desc = "Test TextureAnimation:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "TextureAnimation.texanim"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.texture = {"identifier":"textureFileName","val":"testtex"}
        cls.frames = {"identifier":"numFrames","val":8}
        cls.framesPerRow = {"identifier":"numFramesPerRow","val":2}
        cls.topLeft = {"identifier":"startTopLeft","coord":[0,512]}
        cls.frameSize = {"identifier":"frameSize","coord":[59,126]}
        cls.frameStride = {"identifier":"frameStride","coord":[61,128]}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = anim.TextureAnimation(
                filepath=self.filepath,
                texture=self.texture,
                frames=self.frames,
                framesPerRow=self.framesPerRow,
                topLeft=self.topLeft,
                frameSize=self.frameSize,
                frameStride=self.frameStride
        )

    def testCreationParse(self):
        """ Test whether instances are created correctly from parse results
        """
        res = anim.TextureAnimation.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether parse problems are returned for malformed files
        """
        res = anim.TextureAnimation.createInstance(
                "TextureAnimationMalformed.texanim")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribTexture(self):
        """ Test whether texture attribute is created correctly
        """
        exp = ui.UITextureRef(**self.texture)
        self.assertEqual(self.inst.texture,exp)

    def testCreationAttribFrames(self):
        """ Test whether frames attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.frames)
        self.assertEqual(self.inst.frames,exp)

    def testCreationAttribFramesPerRow(self):
        """ Test whether framesPerRow attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.framesPerRow)
        self.assertEqual(self.inst.framesPerRow,exp)

    def testCreationAttribTopLeft(self):
        """ Test whether topLeft attribute is created correctly
        """
        exp = co_attribs.AttribPos2d(**self.topLeft)
        self.assertEqual(self.inst.topLeft,exp)

    def testCreationAttribFrameSize(self):
        """ Test whether frameSize attribute is created correctly
        """
        exp = co_attribs.AttribPos2d(**self.frameSize)
        self.assertEqual(self.inst.frameSize,exp)

    def testCreationAttribFrameStride(self):
        """ Test whether frameStride attribute is created correctly
        """
        exp = co_attribs.AttribPos2d(**self.frameStride)
        self.assertEqual(self.inst.frameStride,exp)

    def testCheck(self):
        """ Test whether check returns empty list with no problems
        """
        with mock.patch.object(self.inst.texture,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckProbs(self):
        """ Test whether check returns problems correctly
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.texture,"check",autospec=True,
                spec_set=True,return_value=[mockProb]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mockProb])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests ModTextureAnim ##########################"""

class ModTextureAnimCreation(unit.TestCase):
    desc = "Tests of ModTextureAnim creation:"

    @classmethod
    def setUpClass(cls):
        cls.moddir = os.path.abspath("./comp")
        cls.rebdir = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebdir)
        cls.maxDiff = None

    def setUp(self):
        self.inst = anim.ModTextureAnim(self.mmod)

    def testVanillaAnims(self):
        """ Test whether vanilla texAnims are gathered
        """
        self.assertCountEqual(list(self.inst.compFilesVanilla.keys()),
                co_files.VANILLA_MANIFEST["TextureAnimations"])

    def testNoModAnimsDir(self):
        """ Test whether missing TextureAnimations dir is handled
        """
        lmod = tco.genMockMod("./empty")
        inst1 = anim.ModTextureAnim(lmod)
        self.assertFalse(inst1.compFilesMod)

    def testDiscovery(self):
        """ Test whether initial discovery finds correct texanim files
        """
        self.assertEqual(len(self.inst.compFilesMod),2)
        self.assertIn("ta1.texanim",self.inst.compFilesMod)
        self.assertIn("ta2.texanim",self.inst.compFilesMod)

class ModTextureAnimLoading(unit.TestCase):
    desc = "Tests of ModTextureAnim loading:"

    @classmethod
    def setUpClass(cls):
        cls.moddir = os.path.abspath("./comp")
        cls.rebdir = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebdir)

    def setUp(self):
        self.inst = anim.ModTextureAnim(self.mmod)
        self.mockCreate = mock.create_autospec(
                anim.TextureAnimation.createInstance,spec_set=True)
        self.mock1 = mock.create_autospec(anim.TextureAnimation,spec_set=True)
        self.mock2 = mock.create_autospec(anim.TextureAnimation,spec_set=True)
        self.mockCreate.side_effect = [self.mock1,self.mock2]
        patcher = mock.patch(
                "pysoase.mod.texanim.TextureAnimation.createInstance",
                new=self.mockCreate)
        patcher.start()
        patcher = mock.patch("pysoase.mod.texanim.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testLoadTexAnim(self):
        """ Test whether tex anim is loaded correctly
        """
        res = self.inst.loadTexAnim("ta1.texanim")
        self.assertEqual(res,[])
        self.assertIs(self.inst.compFilesMod["ta1.texanim"],self.mock1)

    def testLoadTexAnimMissing(self):
        """ Test whether MissingRef is returned for missing tex anim
        """
        res = self.inst.loadTexAnim("missing.texanim")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.assertEqual(len(self.inst.compFilesMod["missing.texanim"]),1)
        self.assertIsInstance(self.inst.compFilesMod["missing.texanim"][0],
                co_probs.MissingRefProblem)

    def testLoadTexAnimMalformed(self):
        """ Test whether problem is returned for malformed tex anim
        """
        self.mockCreate.side_effect = [[mock.sentinel.prob]]
        res = self.inst.loadTexAnim("ta1.texanim")
        self.assertEqual(len(res),1)
        self.assertEqual(res[0],mock.sentinel.prob)
        self.assertEqual(len(self.inst.compFilesMod["ta1.texanim"]),1)
        self.assertEqual(self.inst.compFilesMod["ta1.texanim"][0],
                mock.sentinel.prob)

    def testLoadAll(self):
        """ Test whether loadAll loads all anims
        """
        res = self.inst.loadAll()
        self.assertEqual(res,[])
        self.assertEqual(len(self.inst.compFilesMod),2)
        self.assertIsInstance(self.inst.compFilesMod["ta1.texanim"],
                anim.TextureAnimation)
        self.assertIsInstance(self.inst.compFilesMod["ta2.texanim"],
                anim.TextureAnimation)

    def testLoadAllProbs(self):
        """ Test whether loadAll returns problems occuring during loading
        """
        self.mockCreate.side_effect = [self.mock1,[mock.sentinel.prob]]
        res = self.inst.loadAll()
        self.assertEqual(res,[mock.sentinel.prob])
        self.assertEqual(len(self.inst.compFilesMod),2)

class ModTextureAnimChecking(unit.TestCase):
    desc = "Tests of ModTextureAnim checking:"

    @classmethod
    def setUpClass(cls):
        cls.moddir = os.path.abspath("./comp")
        cls.rebdir = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebdir)

    def setUp(self):
        self.inst = anim.ModTextureAnim(self.mmod)
        self.mockCreate = mock.create_autospec(
                anim.TextureAnimation.createInstance,spec_set=True)
        self.mock1 = mock.create_autospec(anim.TextureAnimation,spec_set=True)
        self.mock2 = mock.create_autospec(anim.TextureAnimation,spec_set=True)
        self.mockCreate.side_effect = [self.mock1,self.mock2]
        patcher = mock.patch(
                "pysoase.mod.texanim.TextureAnimation.createInstance",
                new=self.mockCreate)
        patcher.start()
        patcher = mock.patch("pysoase.mod.texanim.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCheckTexAnim(self):
        """ Test whether an empty problem list returned for a correct anim
        """
        self.mock1.check.return_value = []
        res = self.inst.checkTexAnim("ta1.texanim")
        self.assertEqual(res,[])
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckTexAnimProb(self):
        """ Test whether problems are returned by checkTexAnim
        """
        self.mock1.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkTexAnim("ta1.texanim")
        self.assertEqual(res,[mock.sentinel.prob])
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckTexAnimLoadProb(self):
        """ Test whether loading problems are returned by checkTexAnim
        """
        self.mockCreate.side_effect = None
        self.mockCreate.return_value = [mock.sentinel.prob]
        res = self.inst.checkTexAnim("ta1.texanim")
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckTexAnimPreloadLoadProb(self):
        """ Test whether loading problems are returned by checkTexAnim preload
        """
        self.mockCreate.side_effect = None
        self.mockCreate.return_value = [mock.sentinel.prob]
        self.inst.loadTexAnim("ta1.texanim")
        res = self.inst.checkTexAnim("ta1.texanim")
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheck(self):
        """ Test whether check returns empty list for good anims
        """
        self.mock1.check.return_value = []
        self.mock2.check.return_value = []
        res = self.inst.check()
        self.assertEqual(res,[])
        self.mock1.check.assert_called_once_with(self.mmod)
        self.mock2.check.assert_called_once_with(self.mmod)

    def testCheckProbs(self):
        """ Test whether check returns problems correctly
        """
        self.mock1.check.return_value = [mock.sentinel.prob1]
        self.mock2.check.return_value = [mock.sentinel.prob2]
        res = self.inst.check()
        self.assertEqual(len(res),2)
        self.assertIn(mock.sentinel.prob1,res)
        self.assertIn(mock.sentinel.prob2,res)
        self.mock1.check.assert_called_once_with(self.mmod)
        self.mock2.check.assert_called_once_with(self.mmod)

    def testCheckFile(self):
        """ Test whether checkFile returns empty list for good anims
        """
        self.mock1.check.return_value = []
        res = self.inst.checkFile("ta1.texanim")
        self.assertEqual(res,[])
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckFileProbs(self):
        """ Test whether checkFile returns problems correctly
        """
        self.mock1.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkFile("ta1.texanim")
        self.assertEqual(res,[mock.sentinel.prob])
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckFileInvalid(self):
        """ Test whether checkFile raises error for invalid file type
        """
        with self.assertRaises(RuntimeError):
            self.inst.checkFile("ta1.foo")
