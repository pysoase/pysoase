""" Tests for the pysoase.mod.audio module.
"""

import collections
import os
from unittest import mock

import pytest

import pysoase.common.attributes as co_attribs
from pysoase.common.filehandling import BUNDLE_PATH
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.audio as audio
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","audio")
testmodule = "pysoase.mod.audio"

"""########################## Tests AudioFileRef ###########################"""

class TestAudioFileRef:
    desc = "Tests AudioFileRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"sound.ogg\"\n"
        cls.identifier = "ident"
        cls.val = "sound.ogg"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.AudioFileRef(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = audio.AudioFileRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check finds existing file and returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckMissing(self):
        """ Test whether check returns missing ref problem for missing file
        """
        localInst = audio.AudioFileRef(
                identifier=self.identifier,
                val="missing.ogg"
        )
        res = localInst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

class TestAudioFileRefWav:
    desc = "Tests AudioFileRef with Wav:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"sound_w.wav\"\n"
        cls.identifier = "ident"
        cls.val = "sound_w.wav"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.AudioFileRef(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(
                self.parseString)[0]
        res = audio.AudioFileRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check finds existing file and returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckMissing(self):
        """ Test whether check returns missing ref problem for missing file
        """
        localInst = audio.AudioFileRef(
                identifier=self.identifier,
                val="missing.wav"
        )
        res = localInst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

"""########################## Tests SoundEffect ############################"""

@pytest.fixture(scope="module")
def soundEffectFixture():
    parseString = (
        "effect\n"
        +"\tname \"test\"\n"
        +"\tfileName \"testsound\"\n"
    )
    parseDict = {
        "identifier":"effect",
        "name":{"identifier":"name","val":"test"},
        "soundfile":{"identifier":"fileName","val":"testsound"}
    }
    return parseString,parseDict

class TestSoundEffect:
    desc = "Tests SoundEffect:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,soundEffectFixture):
        cls.parseString,parseDict = soundEffectFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.SoundEffect(
                identifier=self.identifier,
                name=self.name,
                soundfile=self.soundfile
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = audio.g_soundDialogue.parseString(self.parseString)[0]
        res = audio.SoundEffect(**parseRes)
        assert res == self.inst

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        assert self.inst.name == exp

    def testCreationAttribSoundfile(self):
        """ Test whether soundfile attribute is created correctly
        """
        exp = audio.AudioFileRef(**self.soundfile)
        assert self.inst.soundfile == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.soundfile,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckSoundfile(self):
        """ Test whether check returns problems in soundfile attrib
        """
        with mock.patch.object(self.inst.soundfile,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests DialogueData ###########################"""

class TestDialogueData:
    desc = "Tests DialogueData:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,soundEffectFixture):
        _,soundEffectDict = soundEffectFixture
        cls.filepath = "dialogue.sounddata"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.dialogue = {
            "counter":{"identifier":"numEffects","val":1},
            "elements":[soundEffectDict]
        }
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.DialogueData(
                filepath=self.filepath,
                dialogue=self.dialogue
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        res = audio.DialogueData.createInstance(self.filepath)
        assert res == self.inst

    def testCreationParseMalformed(self):
        """ Test whether parse problems during creation are returned
        """
        res = audio.DialogueData.createInstance("DialogueMalformed.sounddata")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCreationAttribDialogue(self):
        """ Test whether the dialogue attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(elemType=audio.SoundEffect,
                keyfunc=lambda val:val.name.value,**self.dialogue)
        assert self.inst.dialogue == exp

    def testCheckEntry(self):
        """ Test whether checkEntry returns empty list for existing entry
        """
        res = self.inst.checkEntry("test")
        assert res

    def testCheckEntryMissing(self):
        """ Test whether checkEntry returns MissingRef for missing entry
        """
        res = self.inst.checkEntry("missing")
        assert not res

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        with mock.patch.object(self.inst.dialogue,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckDialogue(self,mockProb):
        """ Test whether check returns problems in dialogue
        """
        with mock.patch.object(self.inst.dialogue,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mockProb]
            mocked.assert_called_once_with(self.mmod,False)

    def testGatherRefsAudiofiles(self):
        """ Test whether gatherRefs returns correct audiofile references
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,{"audiofile"})
        assert len(res) == 1
        assert "audiofile" in res
        assert len(res["audiofile"]) == 1
        assert "testsound.ogg" in res["audiofile"]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################# Tests Music ###############################"""

@pytest.fixture(scope="module")
def musicFixture():
    parseString = (
        "music\n"
        +"\tname \"test\"\n"
        +"\tfileName \"testsound\"\n"
        +"\tisLooping FALSE\n"
        +"\tactionLevel 0.500000\n"
        +"\tactionRange 0.500000\n"
        +"\temotionLevel 0.000000\n"
        +"\temotionRange 1.000000\n"
    )
    parseDict = {
        "identifier":"music",
        "name":{"identifier":"name","val":"test"},
        "soundfile":{"identifier":"fileName","val":"testsound"},
        "looping":{"identifier":"isLooping","val":False},
        "actionLvl":{"identifier":"actionLevel","val":0.5},
        "actionRange":{"identifier":"actionRange","val":0.5},
        "emotionLvl":{"identifier":"emotionLevel","val":0.0},
        "emotionRange":{"identifier":"emotionRange","val":1.0}
    }
    return parseString,parseDict

class TestMusic:
    desc = "Tests Music:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,musicFixture):
        cls.parseString,parseDict = musicFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.Music(
                identifier=self.identifier,
                name=self.name,
                soundfile=self.soundfile,
                looping=self.looping,
                actionLvl=self.actionLvl,
                actionRange=self.actionRange,
                emotionLvl=self.emotionLvl,
                emotionRange=self.emotionRange
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = audio.g_soundMusic.parseString(self.parseString)[0]
        res = audio.Music(**parseRes)
        assert res == self.inst

    def testCreationAttribLooping(self):
        """ Test whether looping attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.looping)
        assert self.inst.looping == exp

    def testCreationAttribActionLvl(self):
        """ Test whether actionLvl attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.actionLvl)
        assert self.inst.actionLvl == exp

    def testCreationAttribActionRange(self):
        """ Test whether actionRange attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.actionRange)
        assert self.inst.actionRange == exp

    def testCreationAttribEmotionLvl(self):
        """ Test whether emotionLvl attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.emotionLvl)
        assert self.inst.emotionLvl == exp

    def testCreationAttribEmotionRange(self):
        """ Test whether emotionRange attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.emotionRange)
        assert self.inst.emotionRange == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests MusicData ############################"""

class TestMusicData:
    desc = "Tests MusicData:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,musicFixture):
        _,musicDict = musicFixture
        cls.filepath = "music.sounddata"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.music = {
            "counter":{"identifier":"numMusic","val":1},
            "elements":[musicDict]
        }
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.MusicData(
                filepath=self.filepath,
                music=self.music
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        res = audio.MusicData.createInstance(self.filepath)
        assert res == self.inst

    def testCreationParseMalformed(self):
        """ Test whether parse problems during creation are returned
        """
        res = audio.MusicData.createInstance("MusicMalformed.sounddata")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCreationAttribMusic(self):
        """ Test whether the music attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(elemType=audio.Music,
                keyfunc=lambda val:val.name.value,**self.music)
        assert self.inst.music == exp

    def testCheckEntry(self):
        """ Test whether checkEntry returns empty list for existing entry
        """
        res = self.inst.checkEntry("test")
        assert res

    def testCheckEntryMissing(self):
        """ Test whether checkEntry returns MissingRef for missing entry
        """
        res = self.inst.checkEntry("missing")
        assert not res

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        with mock.patch.object(self.inst.music,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckDialogue(self,mockProb):
        """ Test whether check returns problems in music
        """
        with mock.patch.object(self.inst.music,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mockProb]
            mocked.assert_called_once_with(self.mmod,False)

    def testGatherRefsAudiofiles(self):
        """ Test whether gatherRefs returns correct audiofile references
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,{"audiofile"})
        assert len(res) == 1
        assert "audiofile" in res
        assert len(res["audiofile"]) == 1
        assert "testsound.ogg" in res["audiofile"]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests SoundRef #############################"""

class TestSoundRef:
    desc = "Tests SoundRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"testref\""
        cls.identifier = "ident"
        cls.val = "testref"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.SoundRef(
                identifier=self.identifier,
                val=self.val
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(
                self.parseString)[0]
        res = audio.SoundRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        self.mmod.audio.checkEffect.return_value = []
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckProb(self,mockProb):
        """ Test whether check returns problems correctly
        """
        self.mmod.audio.checkEffect.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]

    def testCheckEmpty(self):
        """ Test whether check skips check with empty reference
        """
        localInst = audio.SoundRef(
                identifier=self.identifier,
                val=""
        )
        self.mmod.audio.checkEffect.return_value = []
        res = localInst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.BasicProblem)
        assert not self.mmod.audio.checkEffect.called

"""############################# Tests Effect ##############################"""

@pytest.fixture(scope="module")
def effectFixture():
    parseString = (
        "effect\n"
        +"\tname \"test\"\n"
        +"\tfileName \"testsound\"\n"
        +"\ttype \"Effect\"\n"
        +"\tis3D FALSE\n"
        +"\tpriority 1\n"
        +"\tthreshold 20000.000000\n"
        +"\tminAttenuationDist 400.000000\n"
        +"\tmaxNumPlayingSimultaneously 4\n"
        +"\tisLooping FALSE\n"
        +"\tisResident FALSE\n"
        +"\tminRespawnTime 0.100000\n"
        +"\tfadeInTime 1.000000\n"
        +"\tfadeOutTime 0.700000\n"
        +"\talternateGroup \"testgrp\"\n"
    )
    parseDict = {
        "identifier":"effect",
        "name":{"identifier":"name","val":"test"},
        "soundfile":{"identifier":"fileName","val":"testsound"},
        "effType":{"identifier":"type","val":"Effect"},
        "is3d":{"identifier":"is3D","val":False},
        "priority":{"identifier":"priority","val":1},
        "threshold":{"identifier":"threshold","val":20000.0},
        "minAttenuationDist":{"identifier":"minAttenuationDist","val":400.0},
        "maxParallelPlaying":{"identifier":"maxNumPlayingSimultaneously","val":4},
        "looping":{"identifier":"isLooping","val":False},
        "resident":{"identifier":"isResident","val":False},
        "respawnTimeMin":{"identifier":"minRespawnTime","val":0.1},
        "fadeInTime":{"identifier":"fadeInTime","val":1.0},
        "fadeOutTime":{"identifier":"fadeOutTime","val":0.7},
        "alternateGroup":{"identifier":"alternateGroup","val":"testgrp"}
    }
    return parseString,parseDict

class TestEffect:
    desc = "Tests Effect:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,effectFixture):
        cls.parseString,parseDict = effectFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.Effect(
                identifier=self.identifier,
                name=self.name,
                soundfile=self.soundfile,
                effType=self.effType,
                is3d=self.is3d,
                priority=self.priority,
                threshold=self.threshold,
                minAttenuationDist=self.minAttenuationDist,
                maxParallelPlaying=self.maxParallelPlaying,
                looping=self.looping,
                resident=self.resident,
                respawnTimeMin=self.respawnTimeMin,
                fadeInTime=self.fadeInTime,
                fadeOutTime=self.fadeOutTime,
                alternateGroup=self.alternateGroup
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = audio.g_soundEffect.parseString(self.parseString)[0]
        res = audio.Effect(**parseRes)
        assert res == self.inst

    def testCreationAttribEffType(self):
        """ Test whether effType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=audio.EFFECT_TYPES,**self.effType)
        assert self.inst.effType == exp

    def testCreationAttribIs3D(self):
        """ Test whether is3d attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.is3d)
        assert self.inst.is3d == exp

    def testCreationAttribPriority(self):
        """ Test whether priority attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.priority)
        assert self.inst.priority == exp

    def testCreationAttribThreshold(self):
        """ Test whether threshold attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.threshold)
        assert self.inst.threshold == exp

    def testCreationAttribMinAttenuationDist(self):
        """ Test whether minAttenuationDist attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.minAttenuationDist)
        assert self.inst.minAttenuationDist == exp

    def testCreationAttribMaxParallelPlaying(self):
        """ Test whether maxParallelPlaying attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxParallelPlaying)
        assert self.inst.maxParallelPlaying == exp

    def testCreationAttribLooping(self):
        """ Test whether looping attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.looping)
        assert self.inst.looping == exp

    def testCreationAttribResident(self):
        """ Test whether resident attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.resident)
        assert self.inst.resident == exp

    def testCreationAttribRespawnTimeMin(self):
        """ Test whether respawnTimeMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.respawnTimeMin)
        assert self.inst.respawnTimeMin == exp

    def testCreationAttribFadeInTime(self):
        """ Test whether fadeInTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.fadeInTime)
        assert self.inst.fadeInTime == exp

    def testCreationAttribFadeOutTime(self):
        """ Test whether fadeOutTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.fadeOutTime)
        assert self.inst.fadeOutTime == exp

    def testCreationAttribAlternateGroup(self):
        """ Test whether alternateGroup attribute is created correctly
        """
        exp = audio.SoundRef(**self.alternateGroup)
        assert self.inst.alternateGroup == exp

    @pytest.fixture()
    def checkFixture(self):
        patcherFile = mock.patch.object(self.inst.soundfile,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockFile = patcherFile.start()
        patcherAlternate = mock.patch.object(self.inst.alternateGroup,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockAlternate = patcherAlternate.start()
        yield
        patcherFile.stop()
        patcherAlternate.stop()

    def testCheck(self,checkFixture):
        """ Test whether check returns empty list when no problems occur
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckAlternateGroup(self,checkFixture):
        """ Test whether problems in alternateGroup attrib are reported
        """
        self.mockAlternate.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockAlternate.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################### Tests EffectData ############################"""

class TestEffectData:
    desc = "Tests EffectData:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,effectFixture):
        cls.filepath = "effects.sounddata"
        _,parseDict = effectFixture
        tco.expandParseDict(cls,parseDict)
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()

        cls.effects = {
            "counter":{"identifier":"numEffects","val":1},
            "elements":[parseDict]
        }
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.EffectData(
                filepath=self.filepath,
                effects=self.effects
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        res = audio.EffectData.createInstance(self.filepath)
        assert res == self.inst

    def testCreationParseMalformed(self):
        """ Test whether parse problems are returned correctly
        """
        res = audio.EffectData.createInstance("EffectsMalformed.sounddata")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCreationAttribEffects(self):
        """ Test whether effects attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=audio.Effect,
                keyfunc=lambda val:val.name.value,
                **self.effects
        )
        assert self.inst.effects == exp

    def testCheckEntry(self):
        """ Test whether checkEntry returns empty list with existing effect
        """
        res = self.inst.checkEntry("test")
        assert res

    def testCheckEntryMissing(self):
        """ Test whether checkEntry returns MissingRef for missing entry
        """
        res = self.inst.checkEntry("missing")
        assert not res

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        with mock.patch.object(self.inst.effects,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckEffects(self):
        """ Test whether problems in effects are returned
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.effects,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mockProb]
            mocked.assert_called_once_with(self.mmod,False)

    def testGatherRefsAudiofiles(self):
        """ Test whether gatherRefs returns correct audiofile references
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,{"audiofile"})
        assert len(res) == 1
        assert "audiofile" in res
        assert len(res["audiofile"]) == 1
        assert "testsound.ogg" in res["audiofile"]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests ModAudio #############################"""

class TestModAudioLoading:
    desc = "Test ModAudio file loading:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.rebpath = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebpath)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.ModAudio(self.mmod)

    def testLoadDialogue(self):
        """ Test whether SoundDialogue file is loaded correctly
        """
        diaMock = mock.create_autospec(audio.DialogueData,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(diaMock,[])) as mocked:
            res = self.inst.dialogue
            assert res == diaMock
            mocked.assert_called_once_with("GameInfo",
                    "SoundDialogue.sounddata",audio.DialogueData)
            assert self.inst._dialogue == diaMock

    @pytest.fixture()
    def mockCreateDialogue(self):
        mockCreateDialogue = mock.create_autospec(
                audio.DialogueData.createInstance,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.audio.DialogueData.createInstance",
                new=mockCreateDialogue)
        patcher.start()
        yield mockCreateDialogue
        patcher.stop()

    def testLoadDialogueMissing(self,mockCreateDialogue):
        """ Test whether missing SoundDialogue is loaded from bundled file
        """
        diaMock = mock.create_autospec(audio.DialogueData,
                spec_set=True)
        mockCreateDialogue.return_value = diaMock
        lmod = tco.genMockMod(os.path.abspath("./comp_empty"))
        linst = audio.ModAudio(lmod)
        res = linst.dialogue
        assert isinstance(res,audio.DialogueData)
        mockCreateDialogue.assert_called_once_with(
                os.path.join(BUNDLE_PATH,"GameInfo","SoundDialogue.sounddata"))

    def testLoadEffect(self):
        """ Test whether SoundEffect file is loaded correctly
        """
        effMock = mock.create_autospec(audio.EffectData,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(effMock,[])) as mocked:
            res = self.inst.effects
            assert res == effMock
            mocked.assert_called_once_with("GameInfo",
                    "SoundEffects.sounddata",audio.EffectData)
            assert self.inst._effects == effMock

    @pytest.fixture()
    def mockCreateEffect(self):
        mockCreateEffect = mock.create_autospec(
                audio.EffectData.createInstance,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.audio.EffectData.createInstance",
                new=mockCreateEffect)
        patcher.start()
        yield mockCreateEffect
        patcher.stop()

    def testLoadEffectMissing(self,mockCreateEffect):
        """ Test whether missing SoundEffect is loaded from bundled file
        """
        effMock = mock.create_autospec(audio.EffectData,spec_set=True)
        mockCreateEffect.return_value = effMock
        lmod = tco.genMockMod(os.path.abspath("./comp_empty"))
        linst = audio.ModAudio(lmod)
        res = linst.effects
        assert isinstance(res,audio.EffectData)
        mockCreateEffect.assert_called_once_with(
                os.path.join(BUNDLE_PATH,"GameInfo","SoundEffects.sounddata"))

    def testLoadMusic(self):
        """ Test whether SoundMusic file is loaded correctly
        """
        musicMock = mock.create_autospec(audio.MusicData,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(musicMock,[])) as mocked:
            res = self.inst.music
            assert res == musicMock
            mocked.assert_called_once_with("GameInfo",
                    "SoundMusic.sounddata",audio.MusicData)
            assert self.inst._music == musicMock

    @pytest.fixture()
    def mockCreateMusic(self):
        mockCreateMusic = mock.create_autospec(
                audio.MusicData.createInstance,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.audio.MusicData.createInstance",
                new=mockCreateMusic)
        patcher.start()
        yield mockCreateMusic
        patcher.stop()

    def testLoadMusicMissing(self,mockCreateMusic):
        """ Test whether missing SoundMusic is loaded from bundled file
        """
        musicMock = mock.create_autospec(audio.MusicData,spec_set=True)
        mockCreateMusic.return_value = musicMock
        lmod = tco.genMockMod(os.path.abspath("./comp_empty"))
        linst = audio.ModAudio(lmod)
        res = linst.music
        assert isinstance(res,audio.MusicData)
        mockCreateMusic.assert_called_once_with(
                os.path.join(BUNDLE_PATH,"GameInfo","SoundMusic.sounddata"))

    def testLoadAll(self,mockTqdm,mockCreateMusic,mockCreateEffect,
            mockCreateDialogue):
        """ Test whether loadAll returns empty list when no problems occur
        """
        mockCreateMusic.return_value = mock.create_autospec(
                audio.MusicData)
        mockCreateEffect.return_value = mock.create_autospec(
                audio.EffectData)
        mockCreateDialogue.return_value = mock.create_autospec(
                audio.DialogueData)
        res = self.inst.loadAll()
        assert res == []

    def testLoadAllProbs(self,mockTqdm,mockCreateMusic,mockCreateEffect,
            mockCreateDialogue):
        """ Test whether loadAll returns problems correctly
        """
        mockCreateMusic.return_value = [mock.sentinel.prob1]
        mockCreateEffect.return_value = [mock.sentinel.prob2]
        mockCreateDialogue.return_value = [mock.sentinel.prob3]
        res = self.inst.loadAll()
        assert len(res) == 3
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res
        assert mock.sentinel.prob3 in res

class TestModAudiochecking:
    desc = "Test ModAudio checking:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.rebpath = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebpath)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.ModAudio(self.mmod)

    def testGatherFiles(self):
        """ Test whether gatherFiles returns correct ogg files
        """
        res = collections.defaultdict(set)
        self.inst.gatherFiles(res,{".ogg"})
        assert res["audiofile"] == {"test1.ogg","test2.ogg"}

    @pytest.fixture()
    def mockDialogue(self):
        mockDialogue = mock.create_autospec(audio.DialogueData,
                spec_set=True)
        self.inst._dialogue = mockDialogue
        yield mockDialogue

    @pytest.fixture()
    def mockEffects(self):
        mockEffects = mock.create_autospec(audio.EffectData,
                spec_set=True)
        self.inst._effects = mockEffects
        yield mockEffects

    @pytest.fixture()
    def mockMusic(self):
        mockMusic = mock.create_autospec(audio.MusicData,
                spec_set=True)
        self.inst._music = mockMusic
        yield mockMusic

    def testGatherRefsWorking(self,mockDialogue,mockEffects,mockMusic,mockTqdm):
        """ Test whether gatherRefs returns correct references
        """
        mockMusic.gatherRefs.side_effect = lambda res,types:res["audiofile"
        ].update({"music.ogg"})
        mockEffects.gatherRefs.side_effect = lambda res,types:res[
            "audiofile"
        ].update({"effects.ogg"})
        mockDialogue.gatherRefs.side_effect = lambda res,types:res[
            "audiofile"
        ].update({"dialogue.ogg"})
        res = collections.defaultdict(set)
        probs = self.inst.gatherRefs(res,{"audiofile"})
        assert probs == []
        assert len(res) == 1
        assert "audiofile" in res
        assert res["audiofile"] == {"music.ogg","effects.ogg","dialogue.ogg"}

    def testGatherRefsProbs(self,mockDialogue,mockEffects,mockTqdm):
        """ Test whether gatherRefs returns loading problems correctly
        """
        self.inst._music = [mock.sentinel.prob]
        mockEffects.gatherRefs.side_effect = lambda res,types:res[
            "audiofile"
        ].update({"effects.ogg"})
        mockDialogue.gatherRefs.side_effect = lambda res,types:res[
            "audiofile"
        ].update({"dialogue.ogg"})
        res = collections.defaultdict(set)
        probs = self.inst.gatherRefs(res,{"audiofile"})
        assert probs == [mock.sentinel.prob]
        assert len(res) == 1
        assert "audiofile" in res
        assert res["audiofile"] == {"effects.ogg","dialogue.ogg"}

    def testCheck(self,mockDialogue,mockEffects,mockMusic,mockTqdm):
        """ Test whether check correctly checks all audio files
        """
        res = self.inst.check()
        assert res == []
        mockDialogue.check.assert_called_once_with(self.mmod)
        mockEffects.check.assert_called_once_with(self.mmod)
        mockMusic.check.assert_called_once_with(self.mmod)

    def testCheckAttribDialogue(self,mockDialogue,mockEffects,mockMusic,
            mockTqdm):
        """ Test whether check returns problems from dialogue file
        """
        mockDialogue.check.return_value = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]
        mockDialogue.check.assert_called_once_with(self.mmod)

    def testCheckAttribEffects(self,mockDialogue,mockEffects,mockMusic,
            mockTqdm):
        """ Test whether check returns problems from effects file
        """
        mockEffects.check.return_value = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]
        mockEffects.check.assert_called_once_with(self.mmod)

    def testCheckAttribMusic(self,mockDialogue,mockEffects,mockMusic,mockTqdm):
        """ Test whether check returns problems from music file
        """
        mockMusic.check.return_value = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]
        mockMusic.check.assert_called_once_with(self.mmod)

    def testCheckAttribProbLoadingMusic(self,mockDialogue,mockEffects,mockMusic,
            mockTqdm):
        """ Test whether check returns loading probs for music file
        """
        self.inst._music = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]

    def testCheckAttribProbLoadingEffects(self,mockDialogue,mockEffects,
            mockMusic,mockTqdm):
        """ Test whether check returns loading probs for effects file
        """
        self.inst._effects = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]

    def testCheckAttribProbLoadingDialogue(self,mockDialogue,mockEffects,
            mockMusic,mockTqdm):
        """ Test whether check returns loading probs for dialogue file
        """
        self.inst._dialogue = [mock.sentinel.prob]
        res = self.inst.check()
        assert res == [mock.sentinel.prob]

    def testCheckDialogue(self,mockEffects,mockDialogue):
        """ Test whether checkDialogue returns empty list when no problem occurs
        """
        mockDialogue.checkEntry.return_value = True
        mockEffects.checkEntry.return_value = False
        res = self.inst.checkEffect("test")
        assert res == []
        mockDialogue.checkEntry.assert_called_once_with("test",False)

    def testCheckDialogueLoadingProb(self,mockEffects):
        """ Test whether checkDialogue returns loading problems
        """
        mockEffects.checkEntry.return_value = False
        self.inst._dialogue = [mock.sentinel.prob]
        res = self.inst.checkEffect("test")
        assert len(res) == 2
        assert res[0] == mock.sentinel.prob
        assert isinstance(res[1],co_probs.MissingRefProblem)

    def testCheckEffects(self,mockEffects,mockDialogue):
        """ Test whether checkEffect returns empty list when no problem occurs
        """
        mockEffects.checkEntry.return_value = True
        mockDialogue.checkEntry.return_value = False
        res = self.inst.checkEffect("test")
        assert res == []
        mockEffects.checkEntry.assert_called_once_with("test",False)

    def testCheckEffectLoadingProb(self,mockEffects,mockDialogue,mockMusic):
        """ Test whether checkEffect returns loading problems
        """
        self.inst._effects = [mock.sentinel.prob]
        res = self.inst.checkEffect("test")
        assert res == [mock.sentinel.prob]

    def testCheckMusic(self,mockMusic):
        """ Test whether checkMusic returns empty list when no problem occurs
        """
        mockMusic.checkEntry.return_value = True
        res = self.inst.checkMusic("test")
        assert res == []
        mockMusic.checkEntry.assert_called_once_with("test",False)

    def testCheckMusicLoadingProb(self):
        """ Test whether checkMusic returns loading problems
        """
        self.inst._music = [mock.sentinel.prob]
        res = self.inst.checkMusic("test")
        assert res == [mock.sentinel.prob]

    def testCheckFileDialogue(self,mockDialogue):
        """ Test whether checkFile checks dialogue file correctly
        """
        mockDialogue.check.return_value = []
        res = self.inst.checkFile("SoundDialogue.sounddata")
        assert res == []
        mockDialogue.check.assert_called_once_with(self.mmod)

    def testCheckFileEffects(self,mockEffects):
        """ Test whether checkFile checks effects file correctly
        """
        mockEffects.check.return_value = []
        res = self.inst.checkFile("SoundEffects.sounddata")
        assert res == []
        mockEffects.check.assert_called_once_with(self.mmod)

    def testCheckFileMusic(self,mockMusic):
        """ Test whether checkFile checks music file correctly
        """
        mockMusic.check.return_value = []
        res = self.inst.checkFile("SoundMusic.sounddata")
        assert res == []
        mockMusic.check.assert_called_once_with(self.mmod)

    def testCheckFileInvalid(self):
        """ Test whether checkFile raises error for invalid file type
        """
        with pytest.raises(RuntimeError):
            self.inst.checkFile("foo.bar")

    def testCheckFileMalformed(self):
        """ Test whether checkFile returns loading problems
        """
        self.inst._music = [mock.sentinel.prob]
        res = self.inst.checkFile("SoundMusic.sounddata")
        assert res == [mock.sentinel.prob]

"""########################### Tests DialogueRef ###########################"""

class TestDialogueRef:
    desc = "Tests DialogueRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.val = "testref"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.DialogueRef(
                identifier=self.identifier,
                val=self.val
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = audio.DialogueRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        self.mmod.audio.checkEffect.return_value = []
        res = self.inst.check(self.mmod)
        assert res == []
        self.mmod.audio.checkEffect.assert_called_once_with("testref",False)

    def testCheckProb(self):
        """ Test whether check returns problems correctly
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.audio.checkEffect.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]

    def testCheckEmpty(self):
        """ Test whether check returns problem when ref is empty
        """
        linst = audio.DialogueRef(
                identifier=self.identifier,
                val=""
        )
        res = linst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.BasicProblem)
        assert not self.mmod.audio.checkEffect.called

"""############################ Tests MusicRef #############################"""

class TestMusicRef:
    desc = "Tests MusicRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.val = "testref"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = audio.MusicRef(
                identifier=self.identifier,
                val=self.val
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = audio.MusicRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        self.mmod.audio.checkMusic.return_value = []
        res = self.inst.check(self.mmod)
        assert res == []
        self.mmod.audio.checkMusic.assert_called_once_with("testref",False)

    def testCheckProb(self):
        """ Test whether check returns problems correctly
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.audio.checkMusic.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]

    def testCheckEmpty(self):
        """ Test whether check returns problem when ref is empty
        """
        linst = audio.MusicRef(
                identifier=self.identifier,
                val=""
        )
        res = linst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.BasicProblem)
        assert not self.mmod.audio.checkMusic.called
