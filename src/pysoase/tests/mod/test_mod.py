""" Tests for the pysoase.mod.mod module.
"""

import os
import unittest as unit
import unittest.mock as mock

import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
import pysoase.common.problems as co_probs
import pysoase.entities.component as ecomp
import pysoase.mod.audio as audio
import pysoase.mod.constants as const
import pysoase.mod.events as events
import pysoase.mod.mod as mod
import pysoase.mod.particles as particles
import pysoase.mod.scenarios as scen
import pysoase.mod.texanim as anim
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui
import pysoase.mod.visuals as vis

testdata = os.path.join("mod","mod")

"""################################ Tests Mod ##############################"""

class TestsMod(object):
    @classmethod
    def setUpClass(cls):
        cls.moddir = "./complete"
        cls.rebrefdir = "./rebref"
        cls.origEntities = ecomp.ModEntities
        patcher = mock.patch("pysoase.mod.mod.ecomp.ModEntities",autospec=True,
                spec_set=True)
        patcher.start()
        cls.origScenarios = scen.ModScenarios
        patcher = mock.patch("pysoase.mod.mod.scen.ModScenarios",autospec=True,
                spec_set=True)
        patcher.start()
        cls.origExplosions = vis.Explosions
        patcher = mock.patch("pysoase.mod.mod.vis.Explosions",autospec=True,
                spec_set=True)
        patcher.start().createInstance.return_value = mock.create_autospec(
                cls.origExplosions,spec_set=True)
        cls.origStarscape = vis.Starscape
        patcher = mock.patch("pysoase.mod.mod.vis.Starscape",autospec=True,
                spec_set=True)
        patcher.start().createInstance.return_value = mock.create_autospec(
                cls.origStarscape,spec_set=True)
        cls.origSkyboxBackdrops = vis.SkyboxBackdrops
        patcher = mock.patch("pysoase.mod.mod.vis.SkyboxBackdrops",
                autospec=True,
                spec_set=True)
        patcher.start().createInstance.return_value = mock.create_autospec(
                cls.origSkyboxBackdrops,spec_set=True)
        cls.origRandEvent = events.RandomEventDef
        patcher = mock.patch("pysoase.mod.mod.events.RandomEventDef",
                autospec=True,spec_set=True)
        patcher.start().createInstance.return_value = mock.create_autospec(
                cls.origRandEvent,spec_set=True)
        cls.origGameEvent = events.GameEventDef
        patcher = mock.patch("pysoase.mod.mod.events.GameEventDef",
                autospec=True,spec_set=True)
        patcher.start().createInstance.return_value = mock.create_autospec(
                cls.origGameEvent,spec_set=True)
        cls.origConst = const.Constants
        patcher = mock.patch("pysoase.mod.mod.const.Constants",autospec=True,
                spec_set=True)
        patcher.start().createInstance.return_value = mock.create_autospec(
                cls.origConst,spec_set=True)
        cls.origAudio = audio.ModAudio
        patcher = mock.patch("pysoase.mod.mod.audio.ModAudio",autospec=True,
                spec_set=True)
        patcher.start()
        cls.origParticles = particles.ModParticles
        patcher = mock.patch("pysoase.mod.mod.particles.ModParticles",
                autospec=True,spec_set=True)
        patcher.start()
        cls.origBrushes = ui.ModBrushes
        patcher = mock.patch("pysoase.mod.mod.ui.ModBrushes",autospec=True,
                spec_set=True)
        patcher.start()
        cls.origStrings = ui.ModStrings
        patcher = mock.patch("pysoase.mod.mod.ui.ModStrings",autospec=True,
                spec_set=True)
        patcher.start()
        cls.origThemes = themes.ModThemes
        patcher = mock.patch("pysoase.mod.mod.themes.ModThemes",autospec=True,
                spec_set=True)
        patcher.start()
        cls.origTexanims = anim.ModTextureAnim
        patcher = mock.patch("pysoase.mod.mod.anim.ModTextureAnim",
                autospec=True,spec_set=True)
        patcher.start()

    @classmethod
    def tearDownClass(cls):
        mock.patch.stopall()

    def tearDown(self):
        ecomp.ModEntities.reset_mock()
        scen.ModScenarios.reset_mock()
        vis.Explosions.reset_mock()
        vis.Starscape.reset_mock()
        vis.SkyboxBackdrops.reset_mock()
        events.RandomEventDef.reset_mock()
        events.GameEventDef.reset_mock()
        const.Constants.reset_mock()
        audio.ModAudio.reset_mock()
        particles.ModParticles.reset_mock()
        ui.ModBrushes.reset_mock()
        ui.ModStrings.reset_mock()
        themes.ModThemes.reset_mock()
        anim.ModTextureAnim.reset_mock()

class ModInvalidPath(unit.TestCase):
    desc = "Test Mod with invalid paths:"

    def testInvalidModPath(self):
        """ Test whether an invalid mod path throws file not found error
        """
        with self.assertRaises(FileNotFoundError):
            mod.Mod("nonexisting")

    def testInvalidRebellionRefPath(self):
        """ Test whether an invalid rebref path throws file not found error
        """
        with self.assertRaises(FileNotFoundError):
            mod.Mod("./",rebrefdir="nonexisting")

class ModCreation(TestsMod,unit.TestCase):
    desc = "Test Mod class instantiation:"

    def testModDir(self):
        """ Test whether the mod directory is set correctly
        """
        res = mod.Mod(self.moddir)
        self.assertEqual(res.moddir,self.moddir)

    def testRebellionRefDir(self):
        """ Test whether the rebellion ref directory is set correctly
        """
        res = mod.Mod(self.moddir,self.rebrefdir)
        self.assertEqual(res.rebrefdir,self.rebrefdir)

    def testModAudio(self):
        """ Test whether ModAudio is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.audio,self.origAudio)
        audio.ModAudio.assert_called_once_with(res)

    def testModBrush(self):
        """ Test whether ModBrush is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.brushes,self.origBrushes)
        ui.ModBrushes.assert_called_once_with(res)

    def testModEntities(self):
        """ Test whether ModEntities is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.entities,self.origEntities)
        ecomp.ModEntities.assert_called_once_with(res)

    def testModParticles(self):
        """ Test whether ModParticles is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.particles,self.origParticles)
        particles.ModParticles.assert_called_once_with(res)

    def testModScenarios(self):
        """ Test whether ModScenarios is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.scenarios,self.origScenarios)
        scen.ModScenarios.assert_called_once_with(res)

    def testModStrings(self):
        """ Test whether ModStrings is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.strings,self.origStrings)
        ui.ModStrings.assert_called_once_with(res)

    def testModThemes(self):
        """ Test whether ModThemes is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.themes,self.origThemes)
        themes.ModThemes.assert_called_once_with(res)

    def testModTexanims(self):
        """ Test whether ModTextureAnim is created correctly
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.texanims,self.origTexanims)
        anim.ModTextureAnim.assert_called_once_with(res)

    def testLazyLoadCreation(self):
        """ Test whether file class attributes are instantiated in constructor
        """
        mod.Mod(self.moddir)
        self.assertFalse(audio.ModAudio.called)
        self.assertFalse(ui.ModBrushes.called)
        self.assertFalse(ui.ModStrings.called)
        self.assertFalse(themes.ModThemes.called)
        self.assertFalse(ecomp.ModEntities.called)
        self.assertFalse(particles.ModParticles.called)
        self.assertFalse(scen.ModScenarios.called)
        self.assertFalse(vis.Explosions.called)
        self.assertFalse(vis.Starscape.called)
        self.assertFalse(vis.SkyboxBackdrops.called)
        self.assertFalse(events.GameEventDef.called)
        self.assertFalse(events.RandomEventDef.called)
        self.assertFalse(const.Constants.called)
        self.assertFalse(anim.ModTextureAnim.called)

    def testLazyExplosions(self):
        """ Test whether lazy loading works for Explosions
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.explosions,self.origExplosions)
        vis.Explosions.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/Explosions.explosiondata")

    def testLazyStarscape(self):
        """ Test whether lazy loading works for Starscape
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.starscape,self.origStarscape)
        vis.Starscape.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/Starscape.starscapedata")

    def testLazySkyboxBackdrops(self):
        """ Test whether lazy loading works for SkyboxBackdrops
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.skyboxBackdrops,self.origSkyboxBackdrops)
        vis.SkyboxBackdrops.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/SkyBoxProperties.skyboxbackdropdata")

    def testLazyRandEvent(self):
        """ Test whether lazy loading works for RandomEventDef
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.randEvents,self.origRandEvent)
        events.RandomEventDef.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/RandomEventDefs.randomeventdefs")

    def testLazyGameEvent(self):
        """ Test whether lazy loading works for GameEventDef
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.gameEvents,self.origGameEvent)
        events.GameEventDef.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/GameEventData.gameeventdata")

    def testLazyConstants(self):
        """ Test whether lazy loading works for Constants
        """
        res = mod.Mod(self.moddir)
        self.assertIsInstance(res.constants,self.origConst)
        const.Constants.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/Gameplay.constants")

    def testCheckExistence(self):
        """ Test whether checkFileExistence is delegated correctly
        """
        inst = mod.Mod(self.moddir)
        with mock.patch("pysoase.common.filehandling.checkFileExistence",
                autospec=True,spec_set=True,
                return_value=(co_files.FileSource.mod,[])
                ) as mocked:
            fsource,probs = inst.checkFileExistence("","testfile")
            self.assertEqual(fsource,co_files.FileSource.mod)
            self.assertEqual(probs,[])
            mocked.assert_called_once_with("","testfile",inst)

    def testLoadFile(self):
        """ Test whether loadFile is delegated correctly
        """
        inst = mod.Mod(self.moddir)
        with mock.patch("pysoase.common.filehandling.loadFile",autospec=True,
                spec_set=True,return_value=(mock.sentinel.res,[])) as mocked:
            res,probs = inst.loadFile("","testfile",
                    co_basics.IsFile)
            self.assertEqual(res,mock.sentinel.res)
            self.assertEqual(probs,[])
            mocked.assert_called_once_with("","testfile",inst,
                    co_basics.IsFile)

class ModLoad(TestsMod,unit.TestCase):
    desc = "Test Mod loadAll method:"

    def setUp(self):
        self.mod = mod.Mod(self.moddir)
        self.patcher = mock.patch("pysoase.mod.mod.tqdm.tqdm",autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        super().tearDown()
        self.patcher.stop()

    def testModEntities(self):
        """ Test whether loadAll is called for ModEntities
        """
        self.mod.loadAll()
        self.mod.entities.loadAll.assert_called_once_with()

    def testModScenarios(self):
        """ Test whether loadAll is called for ModScenarios
        """
        self.mod.loadAll()
        self.mod.scenarios.loadAll.assert_called_once_with()

    def testModAudio(self):
        """ Test whether loadAll is called for ModAudio
        """
        self.mod.loadAll()
        self.mod.audio.loadAll.assert_called_once_with()

    def testModParticles(self):
        """ Test whether loadAll is called for ModParticles
        """
        self.mod.loadAll()
        self.mod.particles.loadAll.assert_called_once_with()

    def testModBrushes(self):
        """ Test whether loadAll is called for ModBrushes
        """
        self.mod.loadAll()
        self.mod.brushes.loadAll.assert_called_once_with()

    def testModStrings(self):
        """ Test whether loadAll is called for ModStrings
        """
        self.mod.loadAll()
        self.mod.strings.loadAll.assert_called_once_with()

    def testModThemes(self):
        """ Test whether loadAll is called for ModThemes
        """
        self.mod.loadAll()
        self.mod.themes.loadAll.assert_called_once_with()

    def testModTexanims(self):
        """ Test whether loadAll is called for ModTextureAnim
        """
        self.mod.loadAll()
        self.mod.texanims.loadAll.assert_called_once_with()

    def testExplosions(self):
        """ Test whether Explosions is created
        """
        self.mod.loadAll()
        vis.Explosions.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/Explosions.explosiondata")

    def testStarscape(self):
        """ Test whether Starscape is created
        """
        self.mod.loadAll()
        vis.Starscape.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/Starscape.starscapedata")

    def testSkyboxBackdrops(self):
        """ Test whether SkyboxBackdrops is created
        """
        self.mod.loadAll()
        vis.SkyboxBackdrops.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/SkyBoxProperties.skyboxbackdropdata")

    def testRandEvent(self):
        """ Test whether RandomEventDef is created
        """
        self.mod.loadAll()
        events.RandomEventDef.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/RandomEventDefs.randomeventdefs")

    def testGameEvent(self):
        """ Test whether GameEventDef is created
        """
        self.mod.loadAll()
        events.GameEventDef.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/GameEventData.gameeventdata")

    @unit.expectedFailure
    def testConstants(self):
        """ Test whether Constants is created
        """
        self.mod.loadAll()
        const.Constants.createInstance.assert_called_once_with(
                self.moddir+"/GameInfo/Gameplay.constants")

class ModCheck(TestsMod,unit.TestCase):
    desc = "Test Mod check method:"

    def setUp(self):
        self.mod = mod.Mod(self.moddir)
        self.testProb = co_probs.BasicProblem("Testprob",
                severity=co_probs.ProblemSeverity.warn)
        self.patcher = mock.patch("pysoase.mod.mod.tqdm.tqdm",autospec=True,
                spec_set=True)
        self.patcher.start()

    def tearDown(self):
        super().tearDown()
        self.patcher.stop()

    def testModEntities(self):
        """ Test whether check is called for ModEntities
        """
        with mock.patch.object(self.mod.entities,"check",
                return_value=[self.testProb]) as mocked:
            res = self.mod.check()
            mocked.assert_called_once_with(False)
            self.assertListEqual(res,[self.testProb])

    def testModScenarios(self):
        """ Test whether check is called for ModScenarios
        """
        self.mod.scenarios.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.scenarios.check.assert_called_once_with(False)
        self.assertListEqual(res,[self.testProb])
        self.mod.scenarios.check.return_value = []

    def testModAudio(self):
        """ Test whether check is called for ModAudio
        """
        self.mod.audio.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.audio.check.assert_called_once_with()
        self.assertListEqual(res,[self.testProb])
        self.mod.audio.check.return_value = []

    def testModParticles(self):
        """ Test whether check is called for ModParticles
        """
        self.mod.particles.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.particles.check.assert_called_once_with()
        self.assertListEqual(res,[self.testProb])
        self.mod.particles.check.return_value = []

    def testModBrushes(self):
        """ Test whether check is called for ModBrushes
        """
        self.mod.brushes.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.brushes.check.assert_called_once_with(False)
        self.assertListEqual(res,[self.testProb])
        self.mod.brushes.check.return_value = []

    def testModThemes(self):
        """ Test whether check is called for ModThemes
        """
        with mock.patch.object(self.mod.themes,"check",
                return_value=[self.testProb]) as mocked:
            res = self.mod.check()
            mocked.assert_called_once_with(False)
            self.assertListEqual(res,[self.testProb])

    def testModTextureAnim(self):
        """ Test whether check is called for ModTextureAnim
        """
        with mock.patch.object(self.mod.texanims,"check",
                return_value=[self.testProb]) as mocked:
            res = self.mod.check()
            mocked.assert_called_once_with()
            self.assertListEqual(res,[self.testProb])

    def testModStrings(self):
        """ Test whether check is called for ModStrings
        """
        self.mod.strings.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.strings.check.assert_called_once_with()
        self.assertListEqual(res,[self.testProb])
        self.mod.strings.check.return_value = []

    def testExplosions(self):
        """ Test whether Explosions is checked
        """
        self.mod.explosions.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.explosions.check.assert_called_once_with(self.mod)
        self.assertListEqual(res,[self.testProb])
        self.mod.explosions.check.return_value = []

    def testStarscape(self):
        """ Test whether Starscape is checked
        """
        self.mod.starscape.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.starscape.check.assert_called_once_with(self.mod)
        self.assertListEqual(res,[self.testProb])
        self.mod.starscape.check.return_value = []

    def testSkyboxBackdrops(self):
        """ Test whether SkyboxBackdrops is checked
        """
        self.mod.skyboxBackdrops.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.skyboxBackdrops.check.assert_called_once_with(self.mod)
        self.assertListEqual(res,[self.testProb])
        self.mod.skyboxBackdrops.check.return_value = []

    def testRandEvent(self):
        """ Test whether RandomEventDef is checked
        """
        self.mod.randEvents.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.randEvents.check.assert_called_once_with(self.mod)
        self.assertListEqual(res,[self.testProb])
        self.mod.randEvents.check.return_value = []

    def testGameEvent(self):
        """ Test whether GameEventDef is checked
        """
        self.mod.gameEvents.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.gameEvents.check.assert_called_once_with(self.mod)
        self.assertListEqual(res,[self.testProb])
        self.mod.gameEvents.check.return_value = []

    @unit.expectedFailure
    def testConstants(self):
        """ Test whether Constants is checked
        """
        self.mod.constants.check.return_value = [self.testProb]
        res = self.mod.check()
        self.mod.constants.check.assert_called_once_with(self.mod)
        self.assertListEqual(res,[self.testProb])
        self.mod.constants.check.return_value = []

class ModCheckFile(TestsMod,unit.TestCase):
    desc = "Test Mod checkFile method:"

    def setUp(self):
        self.mod = mod.Mod(self.moddir)
        self.probs = [mock.sentinel.prob]

    def testManifestBrush(self):
        """ Test whether checkFile works with brush.manifest
        """
        with mock.patch.object(self.mod.brushes,"checkFile",
                return_value=self.probs) as mocked:
            res = self.mod.checkFile("brush.manifest")
            mocked.assert_called_once_with("brush.manifest")
            self.assertEqual(res,self.probs)

    def testManifestEntity(self):
        """ Test whether checkFile works with entity.manifest
        """
        with mock.patch.object(self.mod.entities,"checkFile",
                return_value=self.probs) as mocked:
            res = self.mod.checkFile("entity.manifest")
            mocked.assert_called_once_with("entity.manifest")
            self.assertEqual(res,self.probs)

    def testManifestGalaxy(self):
        """ Test whether checkFile works with galaxy.manifest
        """
        with mock.patch.object(self.mod.scenarios,"checkFile",
                return_value=self.probs) as mocked:
            res = self.mod.checkFile("galaxy.manifest")
            mocked.assert_called_once_with("galaxy.manifest")
            self.assertEqual(res,self.probs)

    def testManifestThemes(self):
        """ Test whether checkFile works with playerThemes.manifest
        """
        with mock.patch.object(self.mod.themes,"checkFile",
                return_value=self.probs) as mocked:
            res = self.mod.checkFile("playerThemes.manifest")
            mocked.assert_called_once_with("playerThemes.manifest")
            self.assertEqual(res,self.probs)

    def testManifestPictures(self):
        """ Test whether checkFile works with playerPictures.manifest
        """
        with mock.patch.object(self.mod.themes,"checkFile",
                return_value=self.probs) as mocked:
            res = self.mod.checkFile("playerPictures.manifest")
            mocked.assert_called_once_with("playerPictures.manifest")
            self.assertEqual(res,self.probs)

    def testExplosions(self):
        """ Test whether checkFile works with Explosions.explosiondata
        """
        self.mod.explosions.check.return_value = self.probs
        res = self.mod.checkFile("Explosions.explosiondata")
        self.mod.explosions.check.assert_called_once_with(self.mod)
        self.assertEqual(res,self.probs)

    def testGameEvents(self):
        """ Test whether checkFile works with GameEventData.gameeventdata
        """
        self.mod.gameEvents.check.return_value = self.probs
        res = self.mod.checkFile("GameEventData.gameeventdata")
        self.mod.gameEvents.check.assert_called_once_with(self.mod)
        self.assertEqual(res,self.probs)

    def testRandomEvents(self):
        """ Test whether checkFile works with RandomEventDefs.randomeventdefs
        """
        self.mod.randEvents.check.return_value = self.probs
        res = self.mod.checkFile("RandomEventDefs.randomeventdefs")
        self.mod.randEvents.check.assert_called_once_with(self.mod)
        self.assertEqual(res,self.probs)

    def testStarscape(self):
        """ Test whether checkFile works with Starscape.starscapedata
        """
        self.mod.starscape.check.return_value = self.probs
        res = self.mod.checkFile("Starscape.starscapedata")
        self.mod.starscape.check.assert_called_once_with(self.mod)
        self.assertEqual(res,self.probs)

    def testSkyboxBackdrops(self):
        """ Test whether checkFile works with SkyBoxProperties.skyboxbackdropdata
        """
        self.mod.skyboxBackdrops.check.return_value = self.probs
        res = self.mod.checkFile("SkyBoxProperties.skyboxbackdropdata")
        self.mod.skyboxBackdrops.check.assert_called_once_with(self.mod)
        self.assertEqual(res,self.probs)

    def testSubdirPresent(self):
        """ Test whether hasSubdir returns true with existing dir
        """
        self.assertTrue(self.mod.hasSubdir("GameInfo"))

    def testSubdirMissing(self):
        """ Test whether hasSubdir returns false with missing dir
        """
        self.assertFalse(self.mod.hasSubdir("String"))

    def testInvalid(self):
        """ Test whether checkFile with invalid filetype raises error
        """
        with self.assertRaises(RuntimeError):
            self.mod.checkFile("foo.bar")

def mockAudioRefs(res,types):
    res["audiofile"].update({"t1.ogg","t2.ogg"})
    return [mock.sentinel.prob]

class ModGathering(TestsMod,unit.TestCase):
    desc = "Test Mod gathering of files and references:"

    def setUp(self):
        self.mod = mod.Mod(self.moddir)

    def testGatherOggFiles(self):
        """ Test whether gathering .ogg files works
        """
        self.mod.audio.gatherFiles.side_effect = lambda res,endings:res[
            "audiofile"].update(
                {"t1.ogg","t2.ogg"}
        )
        res = self.mod.gatherFiles({".ogg"})
        self.assertSetEqual(res["audiofile"],{"t1.ogg","t2.ogg"})

    def testGatherAudiofiles(self):
        """ Test whether gathering audiofile type references works
        """
        self.mod.audio.gatherRefs.side_effect = mockAudioRefs
        self.mod.audio.gatherRefs.return_value = [mock.sentinel.prob]
        res,probs = self.mod.gatherRefs({"audiofile"})
        self.assertSetEqual(res["audiofile"],{"t1.ogg","t2.ogg"})
        self.assertEqual(probs,[mock.sentinel.prob])
