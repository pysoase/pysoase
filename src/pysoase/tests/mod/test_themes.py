""" Unit tests for the pysoase.mod.themes module.
"""

import os
import unittest as unit
import unittest.mock as mock

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.manifest as manifest
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","themes")

"""########################## Tests PictureEntry ###########################"""

class PictureEntryTest(unit.TestCase):
    desc = "Test PictureEntry:"

    def setUp(self):
        self.parseString = (
            "ident\n"
            +"\ttextureName \"test.tga\"\n"
            +"\tstartTopLeft [1,79]\n"
            +"\twidth 10\n"
            +"\theight 12\n"
            +"\trowItemCount 6\n"
            +"\thorizontalStride 40\n"
            +"\tverticalStride 30\n"
        )
        self.identifier = "ident"
        self.texture = {"identifier":"textureName","val":"test.tga"}
        self.topLeft = {"identifier":"startTopLeft","coord":[1,79]}
        self.width = {"identifier":"width","val":10}
        self.height = {"identifier":"height","val":12}
        self.rowItemCount = {"identifier":"rowItemCount","val":6}
        self.horizStride = {"identifier":"horizontalStride","val":40}
        self.vertStride = {"identifier":"verticalStride","val":30}
        self.inst = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=self.width,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parser = pp.Group(
                co_parse.genHeading("ident")
                +themes.g_pictureEntryContent
        )
        parseRes = parser.parseString(self.parseString)[0]
        res = themes.PictureEntry(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribTexture(self):
        """ Test whether attribute texture is created correctly
        """
        exp = ui.UITextureRef(**self.texture)
        self.assertEqual(self.inst.texture,exp)

    def testCreationAttribTopLeft(self):
        """ Test whether attribute topLeft is created correctly
        """
        exp = co_attribs.AttribPos2d(**self.topLeft)
        self.assertEqual(self.inst.topLeft,exp)

    def testCreationAttribWidth(self):
        """ Test whether attribute width is created correctly
        """
        exp = co_attribs.AttribNum(**self.width)
        self.assertEqual(self.inst.width,exp)

    def testCreationAttribHeight(self):
        """ Test whether attribute height is created correctly
        """
        exp = co_attribs.AttribNum(**self.height)
        self.assertEqual(self.inst.height,exp)

    def testCreationAttribRowItems(self):
        """ Test whether attribute rowItemCount is created correctly
        """
        exp = co_attribs.AttribNum(**self.rowItemCount)
        self.assertEqual(self.inst.rowItemCount,exp)

    def testCreationAttribHorizStride(self):
        """ Test whether attribute horizStride is created correctly
        """
        exp = co_attribs.AttribNum(**self.horizStride)
        self.assertEqual(self.inst.horizStride,exp)

    def testCreationAttribVertStride(self):
        """ Test whether attribute vertStride is created correctly
        """
        exp = co_attribs.AttribNum(**self.vertStride)
        self.assertEqual(self.inst.vertStride,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckProb(self):
        """ Test whether check for texture ref is called
        """
        with mock.patch.object(self.inst.texture,"check",autospec=True,
                return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,[mock.sentinel.prob])

    def testEquality(self):
        """ Test whether similar instances evaluate as equal
        """
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=self.width,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.assertEqual(self.inst,inst1)

    def testDiffTexture(self):
        """ Test whether instances differing in texture attribute test equal
        """
        other = {"identifier":"textureName","val":"test2.tga"}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=other,
                topLeft=self.topLeft,
                width=self.width,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffTopLeft(self):
        """ Test whether instances differing in topLeft attribute test equal
        """
        other = {"identifier":"startTopLeft","coord":[5,6]}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=other,
                width=self.width,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffWidth(self):
        """ Test whether instances differing in width attribute test equal
        """
        other = {"identifier":"width","val":20}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=other,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffHeight(self):
        """ Test whether instances differing in height attribute test equal
        """
        other = {"identifier":"height","val":13}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=self.width,
                height=other,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffRowItems(self):
        """ Test whether instances differing in rowItemCount test equal
        """
        other = {"identifier":"rowItemCount","val":200}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=self.width,
                height=self.height,
                rowItemCount=other,
                horizStride=self.horizStride,
                vertStride=self.vertStride
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffHorizStride(self):
        """ Test whether instances differing in horizStride test equal
        """
        other = {"identifier":"horizontalStride","val":403}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=self.width,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=other,
                vertStride=self.vertStride
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffVertStride(self):
        """ Test whether instances differing in vertStride test equal
        """
        other = {"identifier":"verticalStride","val":80}
        inst1 = themes.PictureEntry(
                identifier=self.identifier,
                texture=self.texture,
                topLeft=self.topLeft,
                width=self.width,
                height=self.height,
                rowItemCount=self.rowItemCount,
                horizStride=self.horizStride,
                vertStride=other
        )
        self.assertNotEqual(self.inst,inst1)

"""######################### Tests PlayerPictures ##########################"""

class FixturesPlayerPictures(object):
    @classmethod
    def setUpClass(cls):
        with open("PlayerTech.playerPictures",'r') as f:
            cls.parseString = f.read()
        cls.filepath = "PlayerTech.playerPictures"
        cls.picCount = {"identifier":"pictureCount","val":8}
        cls.picLarge = {
            "identifier":"LargeScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_L.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":195},
            "height":{"identifier":"height","val":219},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":198},
            "vertStride":{"identifier":"verticalStride","val":222}
        }
        cls.picSmall = {
            "identifier":"SmallScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_S.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[5,5]},
            "width":{"identifier":"width","val":56},
            "height":{"identifier":"height","val":67},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":61},
            "vertStride":{"identifier":"verticalStride","val":72}
        }
        cls.picBottomBar = {
            "identifier":"BottomBarPicture",
            "texture":{"identifier":"textureName","val":"Portrait_W.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[1,1]},
            "width":{"identifier":"width","val":169},
            "height":{"identifier":"height","val":77},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":170},
            "vertStride":{"identifier":"verticalStride","val":78}
        }
        cls.button = {
            "identifier":"ButtonIcon",
            "texture":{"identifier":"textureName","val":"Portrait_B.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[3,1]},
            "width":{"identifier":"width","val":24},
            "height":{"identifier":"height","val":29},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":27},
            "vertStride":{"identifier":"verticalStride","val":32}
        }
        cls.picMedium = {
            "identifier":"MediumScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":99},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        cls.mmod = tco.genMockMod("./")

class PlayerPicturesTests(FixturesPlayerPictures,unit.TestCase):
    desc = "Test PlayerPictures:"

    def setUp(self):
        self.inst = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=self.picMedium
        )
        self.maxDiff = None

    def testCreateInstance(self):
        """ Test whether instance is constructed correctly from file
        """
        res = themes.PlayerPictures.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreateMalformed(self):
        """ Test whether correct problem is returned for malformed file
        """
        res = themes.PlayerPictures.createInstance("malformed.playerPictures")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreateAttribPicCount(self):
        """ Test whether the picCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.picCount)
        self.assertEqual(self.inst.picCount,exp)

    def testCreateAttribPicLarge(self):
        """ Test whether the picLarge attribute is created correctly
        """
        exp = themes.PictureEntry(**self.picLarge)
        self.assertEqual(self.inst.picLarge,exp)

    def testCreateAttribPicSmall(self):
        """ Test whether the picSmall attribute is created correctly
        """
        exp = themes.PictureEntry(**self.picSmall)
        self.assertEqual(self.inst.picSmall,exp)

    def testCreateAttribPicBottomBar(self):
        """ Test whether the picBottomBar attribute is created correctly
        """
        exp = themes.PictureEntry(**self.picBottomBar)
        self.assertEqual(self.inst.picBottomBar,exp)

    def testCreateAttribButton(self):
        """ Test whether the button attribute is created correctly
        """
        exp = themes.PictureEntry(**self.button)
        self.assertEqual(self.inst.button,exp)

    def testCreateAttribPicMedium(self):
        """ Test whether the picMedium attribute is created correctly
        """
        exp = themes.PictureEntry(**self.picMedium)
        self.assertEqual(self.inst.picMedium,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=self.picMedium
        )
        self.assertEqual(self.inst,inst1)

    def testDiffPicCount(self):
        """ Test whether instances differing in picCount test as unequal
        """
        other = {"identifier":"pictureCount","val":1}
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=other,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=self.picMedium
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffPicLarge(self):
        """ Test whether instances differing in picLarge test as unequal
        """
        other = {
            "identifier":"MediumScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=other,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=self.picMedium
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffPicSmall(self):
        """ Test whether instances differing in picSmall test as unequal
        """
        other = {
            "identifier":"MediumScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=other,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=self.picMedium
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffPicBottomBar(self):
        """ Test whether instances differing in picBottomBar test as unequal
        """
        other = {
            "identifier":"MediumScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=other,
                button=self.button,
                picMedium=self.picMedium
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffButton(self):
        """ Test whether instances differing in button test as unequal
        """
        other = {
            "identifier":"MediumScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=other,
                picMedium=self.picMedium
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffPicMedium(self):
        """ Test whether instances differing in picMedium test as unequal
        """
        other = {
            "identifier":"MediumScreenPicture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=other
        )
        self.assertNotEqual(self.inst,inst1)

class PlayerPicturesCheck(FixturesPlayerPictures,unit.TestCase):
    desc = "Test PlayerPictures check method:"

    def setUp(self):
        self.inst = themes.PlayerPictures(
                filepath=self.filepath,
                picCount=self.picCount,
                picLarge=self.picLarge,
                picSmall=self.picSmall,
                picBottomBar=self.picBottomBar,
                button=self.button,
                picMedium=self.picMedium
        )
        mock.patch.object(self.inst.picCount,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.picLarge,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.picSmall,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.picBottomBar,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.button,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.picMedium,"check",autospec=True,
                spec_set=True).start()
        self.addCleanup(mock.patch.stopall)
        self.prob = mock.MagicMock(probFile=None)
        self.prob.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)

    def testCheckPicCount(self):
        """ Test whether picCount attribute is checked
        """
        self.inst.picCount.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.picCount.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckPicLarge(self):
        """ Test whether picLarge attribute is checked
        """
        self.inst.picLarge.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.picLarge.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckPicSmall(self):
        """ Test whether picSmall attribute is checked
        """
        self.inst.picSmall.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.picSmall.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckPicBottomBar(self):
        """ Test whether picBottomBar attribute is checked
        """
        self.inst.picBottomBar.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.picBottomBar.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckButton(self):
        """ Test whether button attribute is checked
        """
        self.inst.button.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.button.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckpicMedium(self):
        """ Test whether picMedium attribute is checked
        """
        self.inst.picMedium.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.picMedium.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

"""######################### Tests PlayerThemes ##########################"""

class FixturesPlayerThemes(object):
    @classmethod
    def setUpClass(cls):
        with open("PlayerTech.playerThemes",'r') as f:
            cls.parseString = f.read()
        cls.filepath = "PlayerTech.playerThemes"
        cls.themeCount = {"identifier":"themeCount","val":42}
        cls.icon = {
            "identifier":"Icon",
            "texture":{"identifier":"textureName","val":"Portrait_L.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[1,791]},
            "width":{"identifier":"width","val":32},
            "height":{"identifier":"height","val":32},
            "rowItemCount":{"identifier":"rowItemCount","val":14},
            "horizStride":{"identifier":"horizontalStride","val":40},
            "vertStride":{"identifier":"verticalStride","val":40}
        }
        cls.iconOverlay = {
            "identifier":"IconOverlay",
            "texture":{"identifier":"textureName","val":"Portrait_S.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[1440,791]},
            "width":{"identifier":"width","val":14},
            "height":{"identifier":"height","val":14},
            "rowItemCount":{"identifier":"rowItemCount","val":14},
            "horizStride":{"identifier":"horizontalStride","val":20},
            "vertStride":{"identifier":"verticalStride","val":20}
        }
        cls.picture = {
            "identifier":"Picture",
            "texture":{"identifier":"textureName","val":"Portrait_W.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[1,0]},
            "width":{"identifier":"width","val":105},
            "height":{"identifier":"height","val":105},
            "rowItemCount":{"identifier":"rowItemCount","val":14},
            "horizStride":{"identifier":"horizontalStride","val":110},
            "vertStride":{"identifier":"verticalStride","val":110}
        }
        cls.iconPlanet = {
            "identifier":"PlanetIcon",
            "texture":{"identifier":"textureName","val":"Portrait_B.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[731,791]},
            "width":{"identifier":"width","val":23},
            "height":{"identifier":"height","val":23},
            "rowItemCount":{"identifier":"rowItemCount","val":14},
            "horizStride":{"identifier":"horizontalStride","val":40},
            "vertStride":{"identifier":"verticalStride","val":40}
        }
        cls.mmod = tco.genMockMod("./")

class PlayerThemesTests(FixturesPlayerThemes,unit.TestCase):
    desc = "Test PlayerThemes:"

    def setUp(self):
        self.inst = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=self.icon,
                iconOverlay=self.iconOverlay,
                picture=self.picture,
                iconPlanet=self.iconPlanet
        )
        self.maxDiff = None

    def testCreateInstance(self):
        """ Test whether instance is constructed correctly from file
        """
        res = themes.PlayerThemes.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreateMalformed(self):
        """ Test whether correct problem is returned for malformed file
        """
        res = themes.PlayerThemes.createInstance("malformed.playerThemes")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreateAttribThemeCount(self):
        """ Test whether the themeCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.themeCount)
        self.assertEqual(self.inst.themeCount,exp)

    def testCreateAttribIcon(self):
        """ Test whether the icon attribute is created correctly
        """
        exp = themes.PictureEntry(**self.icon)
        self.assertEqual(self.inst.icon,exp)

    def testCreateAttribIconOverlay(self):
        """ Test whether the iconOverlay attribute is created correctly
        """
        exp = themes.PictureEntry(**self.iconOverlay)
        self.assertEqual(self.inst.iconOverlay,exp)

    def testCreateAttribPicture(self):
        """ Test whether the picture attribute is created correctly
        """
        exp = themes.PictureEntry(**self.picture)
        self.assertEqual(self.inst.picture,exp)

    def testCreateAttribIconPlanet(self):
        """ Test whether the iconPlanet attribute is created correctly
        """
        exp = themes.PictureEntry(**self.iconPlanet)
        self.assertEqual(self.inst.iconPlanet,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=self.icon,
                iconOverlay=self.iconOverlay,
                picture=self.picture,
                iconPlanet=self.iconPlanet
        )
        self.assertEqual(self.inst,inst1)

    def testDiffThemeCount(self):
        """ Test whether instances differing in themeCount test as unequal
        """
        other = {"identifier":"themeCount","val":1}
        inst1 = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=other,
                icon=self.icon,
                iconOverlay=self.iconOverlay,
                picture=self.picture,
                iconPlanet=self.iconPlanet
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffIcon(self):
        """ Test whether instances differing in icon test as unequal
        """
        other = {
            "identifier":"Icon",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=other,
                iconOverlay=self.iconOverlay,
                picture=self.picture,
                iconPlanet=self.iconPlanet
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffIconOverlay(self):
        """ Test whether instances differing in iconOverlay test as unequal
        """
        other = {
            "identifier":"IconOverlay",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=self.icon,
                iconOverlay=other,
                picture=self.picture,
                iconPlanet=self.iconPlanet
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffPicture(self):
        """ Test whether instances differing in picture test as unequal
        """
        other = {
            "identifier":"Picture",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=self.icon,
                iconOverlay=self.iconOverlay,
                picture=other,
                iconPlanet=self.iconPlanet
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffIconPlanet(self):
        """ Test whether instances differing in iconPlanet test as unequal
        """
        other = {
            "identifier":"IconPlanet",
            "texture":{"identifier":"textureName","val":"Portrait_M.tga"},
            "topLeft":{"identifier":"startTopLeft","coord":[0,0]},
            "width":{"identifier":"width","val":97},
            "height":{"identifier":"height","val":109},
            "rowItemCount":{"identifier":"rowItemCount","val":8},
            "horizStride":{"identifier":"horizontalStride","val":800},
            "vertStride":{"identifier":"verticalStride","val":109}
        }
        inst1 = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=self.icon,
                iconOverlay=self.iconOverlay,
                picture=self.picture,
                iconPlanet=other
        )
        self.assertNotEqual(self.inst,inst1)

class PlayerThemesCheck(FixturesPlayerThemes,unit.TestCase):
    desc = "Test PlayerThemes check method:"

    def setUp(self):
        self.inst = themes.PlayerThemes(
                filepath=self.filepath,
                themeCount=self.themeCount,
                icon=self.icon,
                iconOverlay=self.iconOverlay,
                picture=self.picture,
                iconPlanet=self.iconPlanet
        )
        mock.patch.object(self.inst.themeCount,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.icon,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.iconOverlay,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.picture,"check",autospec=True,
                spec_set=True).start()
        mock.patch.object(self.inst.iconPlanet,"check",autospec=True,
                spec_set=True).start()
        self.addCleanup(mock.patch.stopall)
        self.prob = mock.MagicMock(probFile=None)
        self.prob.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)

    def testCheckThemeCount(self):
        """ Test whether themeCount attribute is checked
        """
        self.inst.themeCount.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.themeCount.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckIcon(self):
        """ Test whether icon attribute is checked
        """
        self.inst.icon.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.icon.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckIconOverlay(self):
        """ Test whether iconOverlay attribute is checked
        """
        self.inst.iconOverlay.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.iconOverlay.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckPicture(self):
        """ Test whether picture attribute is checked
        """
        self.inst.picture.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.picture.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

    def testCheckIconPlanet(self):
        """ Test whether iconPlanet attribute is checked
        """
        self.inst.iconPlanet.check.return_value = [self.prob]
        res = self.inst.check(self.mmod)
        self.inst.iconPlanet.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[self.prob])

"""######################### Tests PlayerThemeRef ##########################"""

class PlayerThemeRefTest(unit.TestCase):
    desc = "Test PlayerThemeRef:"

    def setUp(self):
        self.parseString = "ident \"testref\"\n"
        self.inst = themes.PlayerThemeRef(
                identifier="ident",val="testref")
        self.mmod = tco.genMockMod("./")

    def testCreateParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = themes.PlayerThemeRef(**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckNoProbs(self):
        """ Test whether checking works
        """
        with mock.patch.object(self.mmod.themes,"checkManifestThemes",
                return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with("testref.playerThemes")
            self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether checking works when problems occur
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.mmod.themes,"checkManifestThemes",
                return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with("testref.playerThemes")
            self.assertEqual(res,[mockProb])

"""######################## Tests PlayerPictureRef #########################"""

class PlayerPictureRefTest(unit.TestCase):
    desc = "Test PlayerPictureRef:"

    def setUp(self):
        self.parseString = "ident \"testref\"\n"
        self.inst = themes.PlayerPictureRef(
                identifier="ident",val="testref")
        self.mmod = tco.genMockMod("./")

    def testCreateParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = themes.PlayerPictureRef(**parseRes)
        self.assertEqual(res,self.inst)

    def testCheckNoProbs(self):
        """ Test whether checking works
        """
        with mock.patch.object(self.mmod.themes,"checkManifestPictures",
                return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with("testref.playerPictures")
            self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether checking works when problems occur
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.mmod.themes,"checkManifestPictures",
                return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with("testref.playerPictures")
            self.assertEqual(res,[mockProb])

"""####################### Tests PlayerPicturesDef #########################"""

class PlayerPicturesDefTests(unit.TestCase):
    desc = "Test PlayerPicturesDef:"

    def setUp(self):
        self.filepath = "./Window/PlayerPicturesDef.playerPicturesDef"
        with open(self.filepath,'r') as f:
            self.parseString = f.read()
        self.playerPicDefault = {"identifier":"defaultPlayerPictureGroupName",
            "val":"PlayerTech"}
        self.inst = themes.PlayerPicturesDef(
                filepath=self.filepath,
                playerPicDefault=self.playerPicDefault
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        res = themes.PlayerPicturesDef.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationMalformed(self):
        """ Test whether creation from malformed file works correctly
        """
        res = themes.PlayerPicturesDef.createInstance(
                "./Window/malformed.playerPicturesDef")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribPlayerPicDefault(self):
        """ Test whether attribute playerPicDefault is created correctly
        """
        exp = themes.PlayerPictureRef(**self.playerPicDefault)
        self.assertEqual(self.inst.playerPicDefault,exp)

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheck(self):
        """ Test whether checking works correctly
        """
        with mock.patch.object(self.inst.playerPicDefault,"check",
                autospec=True,spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,[])

    def testCheckProb(self):
        """ Test whether checking works correctly with problem
        """
        prob = mock.MagicMock(probFile=None)
        prob.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.playerPicDefault,"check",
                autospec=True,spec_set=True,return_value=[prob]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,[prob])

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = themes.PlayerPicturesDef(
                filepath=self.filepath,
                playerPicDefault=self.playerPicDefault
        )
        self.assertEqual(self.inst,inst1)

    def testDifferPlayerPicDefault(self):
        """ Test whether instances differing in playerPicDefault test unequal
        """
        other = {"identifier":"defaultPlayerPictureGroupName",
            "val":"MissingRef"}
        inst1 = themes.PlayerPicturesDef(
                filepath=self.filepath,
                playerPicDefault=other
        )
        self.assertNotEqual(self.inst,inst1)

"""######################## Tests PlayerThemesDef ##########################"""

class PlayerThemesDef(unit.TestCase):
    desc = "Test PlayerThemesDef:"

    def setUp(self):
        self.filepath = "./Window/PlayerThemesDef.playerThemesDef"
        with open(self.filepath,'r') as f:
            self.parseString = f.read()
        self.playerThemesDef = {"identifier":"defaultPlayerThemeGroupName",
            "val":"PlayerTech"}
        self.playerThemesSpecial = {"identifier":"specialPlayerThemeGroupName",
            "val":"Special"}
        self.unownedIndex = {"identifier":"unownedThemeIndex","val":1}
        self.unknownIndex = {"identifier":"unknownThemeIndex","val":0}
        self.inst = themes.PlayerThemesDef(
                filepath=self.filepath,
                playerThemeDef=self.playerThemesDef,
                playerThemeSpecial=self.playerThemesSpecial,
                unownedIndex=self.unownedIndex,
                unknownIndex=self.unknownIndex
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether creation from file works correctly
        """
        res = themes.PlayerThemesDef.createInstance(self.filepath)
        self.assertEqual(res,self.inst)

    def testCreationMalformed(self):
        """ Test whether creation from malformed file works correctly
        """
        res = themes.PlayerThemesDef.createInstance(
                "./Window/malformed.playerThemesDef")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribPlayerThemeDef(self):
        """ Test whether playerThemeDef is created correctly
        """
        exp = themes.PlayerThemeRef(**self.playerThemesDef)
        self.assertEqual(self.inst.playerThemeDef,exp)

    def testCreationAttribPlayerThemeSpecial(self):
        """ Test whether playerThemeSpecial is created correctly
        """
        exp = themes.PlayerThemeRef(**self.playerThemesSpecial)
        self.assertEqual(self.inst.playerThemeSpecial,exp)

    def testCreationAttribUnownedIndex(self):
        """ Test whether unownedIndex is created correctly
        """
        exp = co_attribs.AttribNum(**self.unownedIndex)
        self.assertEqual(self.inst.unownedIndex,exp)

    def testCreationAttribUnknownIndex(self):
        """ Test whether unknownIndex is created correctly
        """
        exp = co_attribs.AttribNum(**self.unknownIndex)
        self.assertEqual(self.inst.unknownIndex,exp)

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheck(self):
        """ Test whether attributes are checked correctly
        """
        with mock.patch.object(self.inst.playerThemeDef,"check",autospec=True,
                spec_set=True,return_value=[]) as mockDef, \
                mock.patch.object(self.inst.playerThemeSpecial,"check",
                        autospec=True,spec_set=True,
                        return_value=[]) as mockSpecial:
            res = self.inst.check(self.mmod)
            mockDef.assert_called_once_with(self.mmod,False)
            mockSpecial.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,[])

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = themes.PlayerThemesDef(
                filepath=self.filepath,
                playerThemeDef=self.playerThemesDef,
                playerThemeSpecial=self.playerThemesSpecial,
                unownedIndex=self.unownedIndex,
                unknownIndex=self.unknownIndex
        )
        self.assertEqual(self.inst,inst1)

    def testDiffPlayerThemeDef(self):
        """ Test whether instances with differing playerThemeDef test unequal
        """
        other = {"identifier":"defaultPlayerThemeGroupName",
            "val":"PlayerTech1"}
        inst1 = themes.PlayerThemesDef(
                filepath=self.filepath,
                playerThemeDef=other,
                playerThemeSpecial=self.playerThemesSpecial,
                unownedIndex=self.unownedIndex,
                unknownIndex=self.unknownIndex
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffPlayerThemeSpecial(self):
        """ Test whether instances with differing playerThemeSpec test unequal
        """
        other = {"identifier":"specialPlayerThemeGroupName",
            "val":"Special2"}
        inst1 = themes.PlayerThemesDef(
                filepath=self.filepath,
                playerThemeDef=self.playerThemesDef,
                playerThemeSpecial=other,
                unownedIndex=self.unownedIndex,
                unknownIndex=self.unknownIndex
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffUnownedIndex(self):
        """ Test whether instances with differing unownedIndex test unequal
        """
        other = {"identifier":"unownedThemeIndex","val":4}
        inst1 = themes.PlayerThemesDef(
                filepath=self.filepath,
                playerThemeDef=self.playerThemesDef,
                playerThemeSpecial=self.playerThemesSpecial,
                unownedIndex=other,
                unknownIndex=self.unknownIndex
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffUnknownIndex(self):
        """ Test whether instances with differing unknownIndex test unequal
        """
        other = {"identifier":"unknownThemeIndex","val":5}
        inst1 = themes.PlayerThemesDef(
                filepath=self.filepath,
                playerThemeDef=self.playerThemesDef,
                playerThemeSpecial=self.playerThemesSpecial,
                unownedIndex=self.unownedIndex,
                unknownIndex=other
        )
        self.assertNotEqual(self.inst,inst1)

"""########################### Tests ModThemes #############################"""

class ModThemesCreation(unit.TestCase):
    desc = "Test ModThemes creation:"

    def setUp(self):
        self.modpath = os.path.abspath("./modthemes")
        self.rebpath = os.path.abspath("./rebthemes")
        self.mmod = tco.genMockMod(self.modpath,self.rebpath)
        self.inst = themes.ModThemes(self.mmod)

    def testMissingWindowDir(self):
        """ Missing Window dir leads to empty file list
        """
        lmod = tco.genMockMod("./modthemes_empty")
        linst = themes.ModThemes(lmod)
        self.assertEqual(linst.compFilesMod,{})

    def testCreationThemes(self):
        """ Test whether initial discovery of themes works
        """
        initialThemes = ["theme1.playerThemes","theme2.playerThemes"]
        for theme in initialThemes:
            self.assertIn(theme,self.inst.compFilesMod)

    def testCreationPictures(self):
        """ Test whether initial discovery of pictures works
        """
        initialPictures = ["picture1.playerPictures","picture2.playerPictures"]
        for picture in initialPictures:
            self.assertIn(picture,self.inst.compFilesMod)

    def testCreationManifestThemes(self):
        """ Test whether themes manifest is created correctly
        """
        manMock = mock.create_autospec(manifest.ManifestPlayerTheme,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = self.inst.manifestThemes
            mocked.assert_called_once_with("","playerThemes.manifest",
                    manifest.ManifestPlayerTheme)
            self.assertEqual(res,manMock)

    def testCreationManifestThemesLazy(self):
        """ Test whether themes manifest lazy loadind works
        """
        manMock = mock.create_autospec(manifest.ManifestPlayerTheme,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = self.inst.manifestThemes
            mocked.assert_called_once_with("","playerThemes.manifest",
                    manifest.ManifestPlayerTheme)
            self.assertEqual(res,manMock)
            res1 = self.inst.manifestThemes
            self.assertEquals(mocked.call_count,1)
            self.assertEquals(res,res1)

    def testCreationManifestThemesMissing(self):
        """ Test whether missing themes manifest is handled correctly
        """
        currMod = tco.genMockMod("./modthemes_empty")
        currInst = themes.ModThemes(currMod)
        manMock = mock.create_autospec(manifest.ManifestPlayerTheme,
                spec_set=True)
        with mock.patch.object(currInst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = currInst.manifestThemes
            mocked.assert_called_once_with("","playerThemes.manifest",
                    manifest.ManifestPlayerTheme)
            self.assertEqual(res,manMock)

    def testCreationManifestPictures(self):
        """ Test whether pictures manifest is created correctly
        """
        manMock = mock.create_autospec(manifest.ManifestPlayerPicture,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = self.inst.manifestPictures
            mocked.assert_called_once_with("","playerPictures.manifest",
                    manifest.ManifestPlayerPicture)
            self.assertEqual(res,manMock)

    def testCreationManifestPicturesLazy(self):
        """ Test whether pictures manifest lazy loadind works
        """
        manMock = mock.create_autospec(manifest.ManifestPlayerPicture,
                spec_set=True)
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = self.inst.manifestPictures
            mocked.assert_called_once_with("","playerPictures.manifest",
                    manifest.ManifestPlayerPicture)
            self.assertEqual(res,manMock)
            res1 = self.inst.manifestPictures
            self.assertEquals(mocked.call_count,1)
            self.assertEquals(res,res1)

    def testCreationManifestPicturesMissing(self):
        """ Test whether missing picture manifest is handled correctly
        """
        currMod = tco.genMockMod("./modthemes_empty")
        currInst = themes.ModThemes(currMod)
        manMock = mock.create_autospec(manifest.ManifestPlayerPicture,
                spec_set=True)
        with mock.patch.object(currInst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = currInst.manifestPictures
            mocked.assert_called_once_with("","playerPictures.manifest",
                    manifest.ManifestPlayerPicture)
            self.assertEqual(res,manMock)

    def testCreationThemesDef(self):
        """ Test whether themesDef attribute is created correctly
        """
        mocked = mock.create_autospec(themes.PlayerThemesDef,spec_set=True)
        with mock.patch.object(self.inst,"loadFile",return_value=(mocked,[]),
                autospec=True,spec_set=True) as p:
            res = self.inst.themesDef
            p.assert_called_once_with(self.inst.subName,
                    "PlayerThemesDef.playerThemesDef",themes.PlayerThemesDef)
            self.assertIs(res,mocked)

    def testCreationThemesDefLazy(self):
        """ Test whether themesDef lazy loading works
        """
        mocked = mock.create_autospec(themes.PlayerThemesDef,spec_set=True)
        with mock.patch.object(self.inst,"loadFile",return_value=(mocked,[]),
                autospec=True,spec_set=True) as p:
            res = self.inst.themesDef
            p.assert_called_once_with(self.inst.subName,
                    "PlayerThemesDef.playerThemesDef",themes.PlayerThemesDef)
            self.assertIs(res,mocked)
            res1 = self.inst.themesDef
            self.assertIs(res1,mocked)
            self.assertEqual(p.call_count,1)

    def testCreationThemesDefMissing(self):
        """ Test whether missing themes def is handled correctly
        """
        currMod = tco.genMockMod("./modthemes_empty")
        currInst = themes.ModThemes(currMod)
        res = currInst.themesDef
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],
                co_probs.UsingRebFileProblem)

    def testCreationPicturesDef(self):
        """ Test whether picturesDef attribute is created correctly
        """
        mocked = mock.create_autospec(themes.PlayerPicturesDef,spec_set=True)
        with mock.patch.object(self.inst,"loadFile",return_value=(mocked,[]),
                autospec=True,spec_set=True) as p:
            res = self.inst.picturesDef
            p.assert_called_once_with(self.inst.subName,
                    "PlayerPicturesDef.playerPicturesDef",
                    themes.PlayerPicturesDef)
            self.assertIs(res,mocked)

    def testCreationPicturesDefLazy(self):
        """ Test whether picturesDef lazy loading works
        """
        mocked = mock.create_autospec(themes.PlayerPicturesDef,spec_set=True)
        with mock.patch.object(self.inst,"loadFile",return_value=(mocked,[]),
                autospec=True,spec_set=True) as p:
            res = self.inst.picturesDef
            p.assert_called_once_with(self.inst.subName,
                    "PlayerPicturesDef.playerPicturesDef",
                    themes.PlayerPicturesDef)
            self.assertIs(res,mocked)
            res1 = self.inst.picturesDef
            self.assertIs(res1,mocked)
            self.assertEqual(p.call_count,1)

    def testCreationPicturesDefMissing(self):
        """ Test whether missing pictures def is handled correctly
        """
        currMod = tco.genMockMod("./modthemes_empty")
        currInst = themes.ModThemes(currMod)
        res = currInst.picturesDef
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],
                co_probs.UsingRebFileProblem)

class ModThemesLoading(unit.TestCase):
    desc = "Test ModThemes loading functions"

    def setUp(self):
        self.modpath = os.path.abspath("./modthemes")
        self.rebpath = os.path.abspath("./rebthemes")
        self.mmod = tco.genMockMod(self.modpath,self.rebpath)
        self.inst = themes.ModThemes(self.mmod)

        patcher = mock.patch("pysoase.mod.themes.PlayerThemes.createInstance",
                side_effect=self.genMockTheme)
        patcher.start()

        patcher = mock.patch("pysoase.mod.themes.PlayerPictures.createInstance",
                side_effect=self.genMockPicture)
        patcher.start()
        patcher = mock.patch("pysoase.mod.themes.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    @staticmethod
    def genMockTheme(*args):
        return mock.create_autospec(themes.PlayerThemes,spec_set=True)

    @staticmethod
    def genMockPicture(*args):
        return mock.create_autospec(themes.PlayerPictures,spec_set=True)

    def testLoadTheme(self):
        """ Test whether loadEntry works with existing theme
        """
        self.inst.loadEntry("theme1.playerThemes")
        self.assertIsInstance(self.inst.compFilesMod["theme1.playerThemes"],
                themes.PlayerThemes)
        themes.PlayerThemes.createInstance.assert_called_once_with(
                os.path.join(self.modpath,"Window","theme1.playerThemes"))
        self.inst.loadEntry("theme1.playerThemes")
        self.assertEqual(themes.PlayerThemes.createInstance.call_count,1)

    def testLoadThemeMissing(self):
        """ Test whether loadEntry works with missing theme
        """
        res = self.inst.loadEntry("missing.playerThemes")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

    def testLoadThemeParseProb(self):
        """ Test whether loadEntry works with parsing problem for theme
        """
        themes.PlayerThemes.createInstance.side_effect = None
        themes.PlayerThemes.createInstance.return_value = [mock.sentinel.prob]
        res = self.inst.loadEntry("theme1.playerThemes")
        themes.PlayerThemes.createInstance.assert_called_once_with(
                os.path.join(self.modpath,"Window","theme1.playerThemes"))
        self.assertEqual(res,[mock.sentinel.prob])

    def testLoadPicture(self):
        """ Test whether loadEntry works with existing picture
        """
        self.inst.loadEntry("picture1.playerPictures")
        self.assertIsInstance(self.inst.compFilesMod["picture1.playerPictures"],
                themes.PlayerPictures)
        themes.PlayerPictures.createInstance.assert_called_once_with(
                os.path.join(self.modpath,"Window","picture1.playerPictures"))
        self.inst.loadEntry("picture1.playerPictures")
        self.assertEqual(themes.PlayerPictures.createInstance.call_count,1)

    def testLoadPictureMissing(self):
        """ Test whether loadEntry works with missing picture
        """
        res = self.inst.loadEntry("missing.playerPictures")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

    def testLoadPictureParseProb(self):
        """ Test whether loadEntry works with parsing problem
        """
        themes.PlayerPictures.createInstance.side_effect = None
        themes.PlayerPictures.createInstance.return_value = [mock.sentinel.prob]
        res = self.inst.loadEntry("picture1.playerPictures")
        themes.PlayerPictures.createInstance.assert_called_once_with(
                os.path.join(self.modpath,"Window","picture1.playerPictures"))
        self.assertEqual(res,[mock.sentinel.prob])

    def testLoadAll(self):
        """ Test whether loadAll actually loads all themes and pictures
        """
        res = self.inst.loadAll()
        self.assertEqual(res,[])
        calls_themes = [
            mock.call(os.path.join(self.modpath,"Window",
                    "theme1.playerThemes")),
            mock.call(os.path.join(self.modpath,"Window",
                    "theme2.playerThemes"))
        ]
        calls_pictures = [
            mock.call(os.path.join(self.modpath,"Window",
                    "picture1.playerPictures")),
            mock.call(os.path.join(self.modpath,"Window",
                    "picture2.playerPictures"))
        ]
        themes.PlayerThemes.createInstance.assert_has_calls(calls_themes,
                any_order=True)
        self.assertEqual(themes.PlayerThemes.createInstance.call_count,2)
        themes.PlayerPictures.createInstance.assert_has_calls(calls_pictures,
                any_order=True)
        self.assertEqual(themes.PlayerPictures.createInstance.call_count,2)

class ModThemesChecking(unit.TestCase):
    desc = "Test ModThemes checking methods:"

    def setUp(self):
        self.modpath = os.path.abspath("./modthemes")
        self.rebpath = os.path.abspath("./rebthemes")
        self.mmod = tco.genMockMod(self.modpath,self.rebpath)

        self.inst = themes.ModThemes(self.mmod)
        self.mockManThemes = mock.create_autospec(manifest.ManifestPlayerTheme,
                spec_set=True)
        self.inst._manifestThemes = self.mockManThemes
        self.mockManPictures = mock.create_autospec(
                manifest.ManifestPlayerPicture,
                spec_set=True)
        self.inst._manifestPictures = self.mockManPictures

        self.mockTheme = mock.create_autospec(themes.PlayerThemes,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.themes.PlayerThemes.createInstance",
                return_value=self.mockTheme)
        patcher.start()

        self.mockPicture = mock.create_autospec(themes.PlayerPictures,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.themes.PlayerPictures.createInstance",
                return_value=self.mockPicture)
        patcher.start()

        self.mockThemesDef = mock.create_autospec(themes.PlayerThemesDef,
                spec_set=True)
        self.inst._themesDef = self.mockThemesDef

        self.mockPicturesDef = mock.create_autospec(themes.PlayerPicturesDef,
                spec_set=True)
        self.inst._picturesDef = self.mockPicturesDef

        patcher = mock.patch("pysoase.mod.themes.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()

        self.addCleanup(mock.patch.stopall)

    def testCheckThemes(self):
        """ Test whether checkEntry checks correct theme
        """
        res = self.inst.checkEntry("theme1.playerThemes")
        self.assertEqual(res,[])
        self.mockTheme.check.assert_called_once_with(self.mmod)
        self.mockManThemes.checkEntry.assert_called_once_with(
                "theme1.playerThemes")

    def testCheckThemesProb(self):
        """ Test whether checkEntry return correct probs for themes
        """
        themes.PlayerThemes.createInstance.return_value = [mock.sentinel.prob]
        res = self.inst.checkEntry("theme1.playerThemes")
        self.assertEqual(res,[mock.sentinel.prob])
        self.assertFalse(self.mockTheme.check.called)

    def testCheckThemesPreloadProb(self):
        """ Test whether checkEntry return correct preload probs for themes
        """
        themes.PlayerThemes.createInstance.return_value = [mock.sentinel.prob]
        self.inst.loadEntry("theme1.playerThemes")
        res = self.inst.checkEntry("theme1.playerThemes")
        self.assertEqual(res,[mock.sentinel.prob])
        self.assertFalse(self.mockTheme.check.called)

    def testCheckPictures(self):
        """ Test whether checkEntry checks correct picture
        """
        res = self.inst.checkEntry("picture1.playerPictures")
        self.assertEqual(res,[])
        self.mockPicture.check.assert_called_once_with(self.mmod)
        self.mockManPictures.checkEntry.assert_called_once_with(
                "picture1.playerPictures")

    def testCheckPicturesProb(self):
        """ Test whether checkEntry return correct probs for pictures
        """
        themes.PlayerPictures.createInstance.return_value = [mock.sentinel.prob]
        res = self.inst.checkEntry("picture1.playerPictures")
        self.assertEqual(res,[mock.sentinel.prob])
        self.assertFalse(self.mockPicture.check.called)

    def testCheckPicturesPreloadProb(self):
        """ Test whether checkEntry returns correct preload probs for pictures
        """
        themes.PlayerPictures.createInstance.return_value = [mock.sentinel.prob]
        self.inst.loadEntry("picture1.playerPictures")
        res = self.inst.checkEntry("picture1.playerPictures")
        self.assertEqual(res,[mock.sentinel.prob])
        self.assertFalse(self.mockPicture.check.called)

    def testCheckManifestThemes(self):
        """ Test whether themes manifest checking works correctly 
        """
        self.mockManThemes.checkEntry.return_value = []
        res = self.inst.checkManifestThemes("theme1.playerThemes")
        self.mockManThemes.checkEntry.assert_called_once_with(
                "theme1.playerThemes")
        self.assertEqual(res,[])

    def testCheckManifestThemesParseProb(self):
        """ Test whether themes man checking works correctly without manifest
        """
        self.inst._manifestThemes = [mock.sentinel.prob]
        res = self.inst.checkManifestThemes("theme1.playerThemes")
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckManifestPictures(self):
        """ Test whether pictures manifest checking works correctly 
        """
        self.mockManPictures.checkEntry.return_value = []
        res = self.inst.checkManifestPictures("picture1.playerPictures")
        self.mockManPictures.checkEntry.assert_called_once_with(
                "picture1.playerPictures")
        self.assertEqual(res,[])

    def testCheckManifestPicturesParseProb(self):
        """ Test whether pictures man checking works correctly without manifest
        """
        self.inst._manifestPictures = [mock.sentinel.prob]
        res = self.inst.checkManifestPictures("picture1.playerPictures")
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckAllManThemes(self):
        """ Test whether check checks themes manifest
        """
        self.inst.check()
        self.mockManThemes.check.assert_called_once_with(self.mmod)

    def testCheckAllManPictures(self):
        """ Test whether check checks pictures manifest
        """
        self.inst.check()
        self.mockManPictures.check.assert_called_once_with(self.mmod)

    def testCheckAllDefThemes(self):
        """ Test whether check checks themes def
        """
        self.inst.check()
        self.mockThemesDef.check.assert_called_once_with(self.mmod)

    def testCheckAllDefPictures(self):
        """ Test whether check checks pictures def
        """
        self.inst.check()
        self.mockPicturesDef.check.assert_called_once_with(self.mmod)

    def testCheckAllManThemesProb(self):
        """ Test whether check checks themes manifest with prob
        """
        self.inst._manifestThemes = [mock.sentinel.prob]
        res = self.inst.check()
        self.assertEqual(res,[mock.sentinel.prob,
            mock.sentinel.prob,mock.sentinel.prob])

    def testCheckAllManPicturesProb(self):
        """ Test whether check checks pictures manifest with prob
        """
        self.inst._manifestPictures = [mock.sentinel.prob]
        res = self.inst.check()
        self.assertEqual(res,[mock.sentinel.prob,
            mock.sentinel.prob,mock.sentinel.prob])

    def testCheckAllDefThemesProb(self):
        """ Test whether check checks themes def with prob
        """
        self.inst._themesDef = [mock.sentinel.prob]
        res = self.inst.check()
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckAllDefPicturesProb(self):
        """ Test whether check checks pictures def with prob
        """
        self.inst._picturesDef = [mock.sentinel.prob]
        res = self.inst.check()
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckAllThemes(self):
        """ Test whether check checks all themes
        """
        self.inst.check()
        self.assertEqual(self.mockTheme.check.call_count,2)

    def testCheckAllPictures(self):
        """ Test whether check checks all pictures
        """
        self.inst.check()
        self.assertEqual(self.mockPicture.check.call_count,2)

    def testCheckFileManPics(self):
        """ Test whether checkFile for pictures manifest works
        """
        filepath = "playerPictures.manifest"
        self.inst.checkFile(filepath)
        self.mockManPictures.check.assert_called_once_with(self.mmod)

    def testCheckFileManThemes(self):
        """ Test whether checkFile for themes manifest works
        """
        filepath = "playerThemes.manifest"
        self.inst.checkFile(filepath)
        self.mockManThemes.check.assert_called_once_with(self.mmod)

    def testCheckFileDefThemes(self):
        """ Test whether checkFile for themes def works
        """
        filepath = "PlayerThemesDef.playerThemesDef"
        self.inst.checkFile(filepath)
        self.mockThemesDef.check.assert_called_once_with(self.mmod)

    def testCheckFileDefPictures(self):
        """ Test whether checkFile for pictures def works
        """
        filepath = "PlayerPicturesDef.playerPicturesDef"
        self.inst.checkFile(filepath)
        self.mockPicturesDef.check.assert_called_once_with(self.mmod)

    def testCheckFilePictures(self):
        """ Test whether checkFile for pictures works
        """
        filepath = "picture1.playerPictures"
        self.inst.checkFile(filepath)
        self.mockPicture.check.assert_called_once_with(self.mmod)

    def testCheckFileThemes(self):
        """ Test whether checkFile for themes works
        """
        filepath = "theme1.playerThemes"
        self.inst.checkFile(filepath)
        self.mockTheme.check.assert_called_once_with(self.mmod)

    def testCheckFileInvalid(self):
        """ Test whether checkFile for invalid types raises error
        """
        filepath = "foo.bar"
        with self.assertRaises(RuntimeError):
            self.inst.checkFile(filepath)
