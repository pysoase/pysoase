""" Tests for the pysoase.mod.manifest module.
"""

import os
import unittest as unit
from unittest import mock

import pysoase.common.attributes as co_attribs
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.meshes as meshes
import pysoase.tests.conftest as tco
from pysoase.mod import manifest

testdata = os.path.join("mod","manifest")

"""########################### Tests Base Manifest #########################"""

class ManifestBaseTypeOmitted(unit.TestCase):
    desc = "Test the ManifestBase.createInstance with no type arg:"

    def testCreateBrushManifest(self):
        """ Test whether createInstance works for brush manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestBrush",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestBrush))
            res = manifest.ManifestBase.createInstance("brush.manifest")
        self.assertIsInstance(res,manifest.ManifestBrush)

    def testCreateEntityManifest(self):
        """ Test whether createInstance works for entity manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestEntity",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestEntity))
            res = manifest.ManifestBase.createInstance("entity.manifest")
        self.assertIsInstance(res,manifest.ManifestEntity)

    def testCreateGalaxyManifest(self):
        """ Test whether createInstance works for Galaxy manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestGalaxy",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestGalaxy))
            res = manifest.ManifestBase.createInstance("galaxy.manifest")
        self.assertIsInstance(res,manifest.ManifestGalaxy)

    def testCreateSkyboxManifest(self):
        """ Test whether createInstance works for Skybox manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestSkybox",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestSkybox))
            res = manifest.ManifestBase.createInstance("skybox.manifest")
        self.assertIsInstance(res,manifest.ManifestSkybox)

    def testCreatePlayerPicturesManifest(self):
        """ Test whether createInstance works for Player Picture manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestPlayerPicture",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(
                            spec=manifest.ManifestPlayerPicture))
            res = manifest.ManifestBase.createInstance(
                    "playerPictures.manifest")
        self.assertIsInstance(res,manifest.ManifestPlayerPicture)

    def testCreatePlayerThemeManifest(self):
        """ Test whether createInstance works for Player Theme manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestPlayerTheme",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(
                            spec=manifest.ManifestPlayerTheme))
            res = manifest.ManifestBase.createInstance("playerThemes.manifest")
        self.assertIsInstance(res,manifest.ManifestPlayerTheme)

    def testCannotGuessType(self):
        """ Test whether unguessable type leads to error
        """
        expMsg = ("Could not guess what type of manifest \'wrong.manifest\' "
                  "is. Please provide manifest type.")
        with self.assertRaises(RuntimeError) as cm:
            manifest.ManifestBase.createInstance("wrong.manifest")
        self.assertMultiLineEqual(str(cm.exception),expMsg)

class ManifestBaseTypeProvided(unit.TestCase):
    desc = "Test the ManifestBase.createInstance with type arg:"

    def testCreateBrushManifest(self):
        """ Test whether createInstance works for brush manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestBrush",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestBrush))
            res = manifest.ManifestBase.createInstance("brush.manifest",
                    "brush")
        self.assertIsInstance(res,manifest.ManifestBrush)

    def testCreateEntityManifest(self):
        """ Test whether createInstance works for entity manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestEntity",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestEntity))
            res = manifest.ManifestBase.createInstance("entity.manifest",
                    "entity")
        self.assertIsInstance(res,manifest.ManifestEntity)

    def testCreateGalaxyManifest(self):
        """ Test whether createInstance works for Galaxy manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestGalaxy",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestGalaxy))
            res = manifest.ManifestBase.createInstance("galaxy.manifest",
                    "galaxy")
        self.assertIsInstance(res,manifest.ManifestGalaxy)

    def testCreateSkyboxManifest(self):
        """ Test whether createInstance works for Skybox manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestSkybox",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(spec=manifest.ManifestSkybox))
            res = manifest.ManifestBase.createInstance("skybox.manifest",
                    "skybox")
        self.assertIsInstance(res,manifest.ManifestSkybox)

    def testCreatePlayerPictureManifest(self):
        """ Test whether createInstance works for Player Picture manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestPlayerPicture",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(
                            spec=manifest.ManifestPlayerPicture))
            res = manifest.ManifestBase.createInstance(
                    "playerPictures.manifest",
                    "playerPictures")
        self.assertIsInstance(res,manifest.ManifestPlayerPicture)

    def testCreatePlayerThemeManifest(self):
        """ Test whether createInstance works for Player Theme manifests 
        """
        with mock.patch("pysoase.mod.manifest.ManifestPlayerTheme",
                autospec=True,
                spec_set=True) as classMock:
            classMock.createInstance = mock.MagicMock(
                    return_value=mock.MagicMock(
                            spec=manifest.ManifestPlayerTheme))
            res = manifest.ManifestBase.createInstance("playerThemes.manifest",
                    "playerThemes")
        self.assertIsInstance(res,manifest.ManifestPlayerTheme)

    def testManifestTypeEmpty(self):
        """ Test whether createInstance with manType=\'\' throws error
        """
        expMsg = "Unknown manifest type \'{}\' given for \'brush.manifest\'."
        with self.assertRaises(RuntimeError) as cm:
            manifest.ManifestBase.createInstance("brush.manifest","")
        self.assertEqual(str(cm.exception),expMsg.format(""))

    def testManifestTypeUnknown(self):
        """ Test whether createInstance with manType=\'rand\' throws error
        """
        expMsg = "Unknown manifest type \'{}\' given for \'brush.manifest\'."
        with self.assertRaises(RuntimeError) as cm:
            manifest.ManifestBase.createInstance("brush.manifest","rand")
        self.assertEqual(str(cm.exception),expMsg.format("rand"))

"""########################## Tests Brush Manifest ########################"""

autospecFileRef = co_attribs.FileRef("brushFile",val="foo",subdir="Window",
        extension="brushes")

def genMockFileRef(val,**kwargs):
    m = mock.MagicMock(spec_set=autospecFileRef)
    m.ref = val
    return m

def cmpParseResults(test,exp,act):
    expCount = exp["entries"]["counter"]
    expElements = exp["entries"]["elements"]
    actCounter = act["entries"]["counter"]
    actElements = act["entries"]["elements"]
    test.assertEqual(expCount["identifier"],actCounter["identifier"])
    test.assertEqual(expCount["val"],actCounter["val"])
    test.assertEqual(len(expElements),len(actElements))
    for expElem,actElem in zip(expElements,actElements):
        test.assertEqual(expElem["identifier"],actElem["identifier"])
        test.assertEqual(expElem["val"],actElem["val"])

class ManifestBrushMissingFile(unit.TestCase):
    desc = "Test ManifestBrush with missing file:"
    filepath = "missingManifest.manifest"
    expectedError = "File \'"+filepath+"\' does not exist."

    def testCreation(self):
        """ Test whether createInstance raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestBrush.createInstance(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

    def testParse(self):
        """ Test whether parse raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestBrush.parse(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

class ManifestBrushMalformed(unit.TestCase):
    desc = "Test ManifestBrush with malformed file:"

    def setUp(self):
        self.origProb = co_probs.ParseProblem
        patcher = mock.patch("pysoase.mod.manifest.co_probs.ParseProblem",
                autospec=True,spec_set=True)
        mockProb = patcher.start()
        self.addCleanup(patcher.stop)
        mockProb.side_effect = mock.create_autospec(spec=self.origProb,
                spec_set=True)

    def testMisspelledHeaderParse(self):
        """ Test whether parse throws correct error with misspellings
        """
        out,res = manifest.ManifestBrush.parse("brush_misspelled.manifest")
        self.assertFalse(out)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingParse(self):
        """ Test whether parse throws correct error with missing count line
        """
        succ,res = manifest.ManifestBrush.parse("brush_missing_count.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsParse(self):
        """ Test whether parse throws correct error with too few elements
        """
        succ,res = manifest.ManifestBrush.parse("brush_too_few.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsParse(self):
        """ Test whether parse throws correct error with too many elements
        """
        succ,res = manifest.ManifestBrush.parse("brush_too_many.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testMisspelledHeaderCreateInstance(self):
        """ Test whether createInstance throws correct error with misspellings
        """
        res = manifest.ManifestBrush.createInstance(
                "brush_misspelled.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingCreateInstance(self):
        """ Test whether createInstance throws correct error with missing count line
        """
        res = manifest.ManifestBrush.createInstance(
                "brush_missing_count.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too few elements
        """
        res = manifest.ManifestBrush.createInstance("brush_too_few.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too many elements
        """
        res = manifest.ManifestBrush.createInstance(
                "brush_too_many.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

class ManifestBrushNoEntries(unit.TestCase):
    desc = "Test ManifestBrush with no entries:"

    def setUp(self):
        self.manifestFile = "brush_no_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"brushFileCount","val":0}}}
        self.manifest = manifest.ManifestBrush(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestBrush.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestBrush)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertFalse(res.entries)
        self.assertListEqual(res.entries.elements,[])
        self.assertEqual(res.filedir,"Window")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns an empty list
        """
        self.assertListEqual(self.manifest.getRefs(),[])

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        outcome,result = manifest.ManifestBrush.parse(self.manifestFile)
        self.assertTrue(outcome)
        expCount = self.manifestEntries["entries"]["counter"]
        actCounter = result["entries"]["counter"]
        self.assertNotIn("elements",result["entries"])
        self.assertEqual(expCount["identifier"],actCounter["identifier"])
        self.assertEqual(expCount["val"],actCounter["val"])

    def testCheckMissingEntry(self):
        """ Test whether checkEntry throws problems on random call
        """
        res = self.manifest.checkEntry("missing_ref")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],manifest.MissingManifestEntry)
        self.assertEqual(res[0].reference,"missing_ref")
        self.assertEqual(res[0].manifestType,"brush")

class ManifestBrushBadEntries(unit.TestCase):
    desc = "Test ManifestBrush with some bad entries:"

    def setUp(self):
        self.manifestFile = "brush_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"brushFileCount","val":8},
            "elements":[
                {"identifier":"brushFile","val":"brush1.brushes"},
                {"identifier":"brushFile","val":"brush2.brushes"},
                {"identifier":"brushFile","val":"brush3.brushes"},
                {"identifier":"brushFile","val":"brush3.brushes"},
                {"identifier":"brushFile","val":"brush800.brushes"},
                {"identifier":"brushFile","val":"brush4.brushes"},
                {"identifier":"brushFile","val":"brush5.brushes"},
                {"identifier":"brushFile","val":"brush100.brushes"}
            ]}}
        self.origRef = co_attribs.FileRef
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestBrush(self.manifestFile,
                self.manifestEntries["entries"])
        for elem in self.manifest.entries.elements:
            elem.checkExistence = mock.MagicMock(return_value=[])
        self.manifest.entries.elements[4].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","brush800.brushes")])
        self.manifest.entries.elements[7].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","brush100.brushes")])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestBrush.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestBrush)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Window")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(None)
        for prob in res:
            self.assertEqual(prob.probFile,"brush_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(None)
        self.assertEqual(len(res),3)

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="brush_bad_entries.manifest")
        res = self.manifest.check(None)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(None)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("brush800.brushes",missRefs)
        self.assertIn("brush100.brushes",missRefs)

class ManifestBrushGoodEntries(unit.TestCase):
    desc = "Test ManifestBrush with only good entries:"

    def setUp(self):
        self.manifestFile = "brush_good_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"brushFileCount","val":8},
            "elements":[
                {"identifier":"brushFile","val":"brush1.brushes"},
                {"identifier":"brushFile","val":"brush2.brushes"},
                {"identifier":"brushFile","val":"brush3.brushes"},
                {"identifier":"brushFile","val":"brush7.brushes"},
                {"identifier":"brushFile","val":"brush8.brushes"},
                {"identifier":"brushFile","val":"brush4.brushes"},
                {"identifier":"brushFile","val":"brush5.brushes"},
                {"identifier":"brushFile","val":"brush6.brushes"}
            ]}}
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        self.origRef = co_attribs.FileRef
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestBrush(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestBrush.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestBrush)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Window")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns a list with all manifest entries
        """
        res = self.manifest.getRefs()
        self.assertEqual(len(res),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertIsInstance(res[0],self.origRef)
        for entry in self.manifest.entries.elements:
            self.assertIn(entry,res)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        self.maxDiff = None
        outcome,result = manifest.ManifestBrush.parse(self.manifestFile)
        self.assertTrue(outcome)
        cmpParseResults(self,self.manifestEntries,result)

    def testCheckMissingEntry(self):
        """ Test whether checkEntry returns no problem for any entry
        """
        for entry in self.manifestEntries["entries"]["elements"]:
            res = self.manifest.checkEntry(entry["val"])
            self.assertListEqual(res,[])

class ManifestBrushIntegration(unit.TestCase):
    desc = "Integration test for ManifestBrush"

    def setUp(self):
        self.manifestFile = "brush_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"brushFileCount","val":8},
            "elements":[
                {"identifier":"brushFile","val":"brush1.brushes"},
                {"identifier":"brushFile","val":"brush2.brushes"},
                {"identifier":"brushFile","val":"brush3.brushes"},
                {"identifier":"brushFile","val":"brush3.brushes"},
                {"identifier":"brushFile","val":"brush800.brushes"},
                {"identifier":"brushFile","val":"brush4.brushes"},
                {"identifier":"brushFile","val":"brush5.brushes"},
                {"identifier":"brushFile","val":"brush100.brushes"}
            ]}}
        self.manifest = manifest.ManifestBrush(self.manifestFile,
                self.manifestEntries["entries"])
        self.mod = tco.genMockMod("./")

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestBrush.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestBrush)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(res.filedir,"Window")

    def testCreateInstanceEntries(self):
        """ Test whether the entries in the created instance are correct
        """
        res = manifest.ManifestBrush.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries.elements),
                len(self.manifestEntries["entries"]["elements"]))
        for act,exp in zip(res.entries.elements,
                self.manifestEntries["entries"]["elements"]):
            self.assertEqual(act.identifier,exp["identifier"])
            self.assertEqual(act.ref,exp["val"])
            self.assertIsInstance(act,co_attribs.FileRef)

    def testCreateInstanceSorted(self):
        """ Test whether the sortedEntries are created correctly
        """
        exp = [val["val"] for val
            in self.manifestEntries["entries"]["elements"]]
        res = manifest.ManifestBrush.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries),
                len(exp))
        self.assertCountEqual([val.ref for val in res.entries.elements],exp)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns correct duplicates
        """
        res = self.manifest.removeDuplicates()
        self.assertEqual(len(res),1)
        self.assertEqual(res[0].ref,"brush3.brushes")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(self.mod)
        for prob in res:
            self.assertEqual(prob.probFile,"brush_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(self.mod)
        self.assertEqual(len(res),3)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="brush_bad_entries.manifest")
        res = self.manifest.check(self.mod)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(self.mod)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("brush800.brushes",missRefs)
        self.assertIn("brush100.brushes",missRefs)

    def testWriteOut(self):
        """ Test whether writeOut produces the expected file
        """
        with open(self.manifestFile) as f:
            orig = f.read()
        self.manifest.writeOut(filepath="out_brush.manifest",force=True)
        with open("out_brush.manifest") as f:
            writeout = f.read()
        self.assertEqual(writeout.replace("\t",""),orig)
        os.remove("out_brush.manifest")

"""########################## Tests Entity Manifest ########################"""

autospecEntityRef = coe.EntityRef("foo","bar",["Frigate"])

def genMockRef(val,**kwargs):
    m = mock.MagicMock(spec_set=autospecEntityRef)
    m.ref = val
    return m

def cmpParseResults(test,exp,act):
    expCount = exp["entries"]["counter"]
    expElements = exp["entries"]["elements"]
    actCounter = act["entries"]["counter"]
    actElements = act["entries"]["elements"]
    test.assertEqual(expCount["identifier"],actCounter["identifier"])
    test.assertEqual(expCount["val"],actCounter["val"])
    test.assertEqual(len(expElements),len(actElements))
    for expElem,actElem in zip(expElements,actElements):
        test.assertEqual(expElem["identifier"],actElem["identifier"])
        test.assertEqual(expElem["val"],actElem["val"])

class ManifestEntityMissingFile(unit.TestCase):
    desc = "Test ManifestEntity with missing file:"
    filepath = "missingManifest.manifest"
    expectedError = "File \'"+filepath+"\' does not exist."

    def testCreation(self):
        """ Test whether createInstance raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestEntity.createInstance(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

    def testParse(self):
        """ Test whether parse raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestEntity.parse(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

class ManifestEntityMalformed(unit.TestCase):
    desc = "Test ManifestEntity with malformed file:"

    def setUp(self):
        self.origProb = co_probs.ParseProblem
        patcher = mock.patch("pysoase.mod.manifest.co_probs.ParseProblem",
                autospec=True,spec_set=True)
        mockProb = patcher.start()
        self.addCleanup(patcher.stop)
        mockProb.side_effect = mock.create_autospec(spec=self.origProb,
                spec_set=True)

    def testMisspelledHeaderParse(self):
        """ Test whether parse throws correct error with misspellings
        """
        out,res = manifest.ManifestEntity.parse("entity_misspelled.manifest")
        self.assertFalse(out)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingParse(self):
        """ Test whether parse throws correct error with missing count line
        """
        succ,res = manifest.ManifestEntity.parse(
                "entity_missing_count.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsParse(self):
        """ Test whether parse throws correct error with too few elements
        """
        succ,res = manifest.ManifestEntity.parse("entity_too_few.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsParse(self):
        """ Test whether parse throws correct error with too many elements
        """
        succ,res = manifest.ManifestEntity.parse("entity_too_many.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testMisspelledHeaderCreateInstance(self):
        """ Test whether createInstance throws correct error with misspellings
        """
        res = manifest.ManifestEntity.createInstance(
                "entity_misspelled.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingCreateInstance(self):
        """ Test whether createInstance throws correct error with missing count line
        """
        res = manifest.ManifestEntity.createInstance(
                "entity_missing_count.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too few elements
        """
        res = manifest.ManifestEntity.createInstance("entity_too_few.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too many elements
        """
        res = manifest.ManifestEntity.createInstance(
                "entity_too_many.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

class ManifestEntityNoEntries(unit.TestCase):
    desc = "Test ManifestEntity with no entries:"

    def setUp(self):
        self.manifestFile = "entity_no_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"entityNameCount","val":0}}}
        self.manifest = manifest.ManifestEntity(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestEntity.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestEntity)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertFalse(res.entries)
        self.assertListEqual(res.entries.elements,[])
        self.assertEqual(res.filedir,"GameInfo")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns an empty list
        """
        self.assertListEqual(self.manifest.getRefs(),[])

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        outcome,result = manifest.ManifestEntity.parse(self.manifestFile)
        self.assertTrue(outcome)
        expCount = self.manifestEntries["entries"]["counter"]
        actCounter = result["entries"]["counter"]
        self.assertNotIn("elements",result["entries"])
        self.assertEqual(expCount["identifier"],actCounter["identifier"])
        self.assertEqual(expCount["val"],actCounter["val"])

    def testCheckMissingEntry(self):
        """ Test whether checkEntry throws problems on random call
        """
        res = self.manifest.checkEntry("missing_ref")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],manifest.MissingManifestEntry)
        self.assertEqual(res[0].reference,"missing_ref")
        self.assertEqual(res[0].manifestType,"entity")

class ManifestEntityBadEntries(unit.TestCase):
    desc = "Test ManifestEntity with some bad entries:"

    def setUp(self):
        self.manifestFile = "entity_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"entityNameCount","val":8},
            "elements":[
                {"identifier":"entityName","val":"ent1.entity"},
                {"identifier":"entityName","val":"ent2.entity"},
                {"identifier":"entityName","val":"ent3.entity"},
                {"identifier":"entityName","val":"ent3.entity"},
                {"identifier":"entityName","val":"ent800.entity"},
                {"identifier":"entityName","val":"ent4.entity"},
                {"identifier":"entityName","val":"ent5.entity"},
                {"identifier":"entityName","val":"ent100.entity"}
            ]}}
        self.origRef = coe.EntityRef
        patcher = mock.patch("pysoase.mod.manifest.coe.EntityRef",
                autospec=True,spec_set=True)
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockRef
        self.manifest = manifest.ManifestEntity(self.manifestFile,
                self.manifestEntries["entries"])
        for elem in self.manifest.entries.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.manifest.entries.elements[4].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","ent800.entity")])
        self.manifest.entries.elements[7].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","ent100.entity")])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestEntity.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestEntity)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"GameInfo")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(None)
        for prob in res:
            self.assertEqual(prob.probFile,"entity_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(None)
        self.assertEqual(len(res),3)

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="entity_bad_entries.manifest")
        res = self.manifest.check(None)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(None)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("ent800.entity",missRefs)
        self.assertIn("ent100.entity",missRefs)

class ManifestEntityGoodEntries(unit.TestCase):
    desc = "Test ManifestEntity with only good entries:"

    def setUp(self):
        self.manifestFile = "entity_good_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"entityNameCount","val":8},
            "elements":[
                {"identifier":"entityName","val":"ent1.entity"},
                {"identifier":"entityName","val":"ent2.entity"},
                {"identifier":"entityName","val":"ent3.entity"},
                {"identifier":"entityName","val":"ent7.entity"},
                {"identifier":"entityName","val":"ent8.entity"},
                {"identifier":"entityName","val":"ent4.entity"},
                {"identifier":"entityName","val":"ent5.entity"},
                {"identifier":"entityName","val":"ent6.entity"}
            ]}}
        patcher = mock.patch("pysoase.mod.manifest.coe.EntityRef",
                autospec=True,spec_set=True)
        self.origRef = coe.EntityRef
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockRef
        self.manifest = manifest.ManifestEntity(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestEntity.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestEntity)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"GameInfo")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns a list with all manifest entries
        """
        res = self.manifest.getRefs()
        self.assertEqual(len(res),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertIsInstance(res[0],self.origRef)
        for entry in self.manifest.entries.elements:
            self.assertIn(entry,res)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        self.maxDiff = None
        outcome,result = manifest.ManifestEntity.parse(self.manifestFile)
        self.assertTrue(outcome)
        cmpParseResults(self,self.manifestEntries,result)

    def testCheckMissingEntry(self):
        """ Test whether checkEntry returns no problem for any entry
        """
        for entry in self.manifestEntries["entries"]["elements"]:
            res = self.manifest.checkEntry(entry["val"])
            self.assertListEqual(res,[])

class ManifestEntityIntegration(unit.TestCase):
    desc = "Integration test for ManifestEntity"

    def setUp(self):
        self.manifestFile = "entity_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"entityNameCount","val":8},
            "elements":[
                {"identifier":"entityName","val":"ent1.entity"},
                {"identifier":"entityName","val":"ent2.entity"},
                {"identifier":"entityName","val":"ent3.entity"},
                {"identifier":"entityName","val":"ent3.entity"},
                {"identifier":"entityName","val":"ent800.entity"},
                {"identifier":"entityName","val":"ent4.entity"},
                {"identifier":"entityName","val":"ent5.entity"},
                {"identifier":"entityName","val":"ent100.entity"}
            ]}}
        self.manifest = manifest.ManifestEntity(self.manifestFile,
                self.manifestEntries["entries"])
        self.mod = tco.genMockMod("./")
        self.mod.entities.checkManifest = mock.MagicMock(return_value=[])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestEntity.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestEntity)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(res.filedir,"GameInfo")

    def testCreateInstanceEntries(self):
        """ Test whether the entries in the created instance are correct
        """
        res = manifest.ManifestEntity.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries.elements),
                len(self.manifestEntries["entries"]["elements"]))
        for act,exp in zip(res.entries.elements,
                self.manifestEntries["entries"]["elements"]):
            self.assertEqual(act.identifier,exp["identifier"])
            self.assertEqual(act.ref,exp["val"])
            self.assertIsInstance(act,coe.EntityRef)

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(self.mod)
        for prob in res:
            self.assertEqual(prob.probFile,"entity_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(self.mod)
        self.assertEqual(len(res),3)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="entity_bad_entries.manifest")
        res = self.manifest.check(self.mod)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(self.mod)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("ent800.entity",missRefs)
        self.assertIn("ent100.entity",missRefs)

    def testWriteOut(self):
        """ Test whether writeOut produces the expected file
        """
        with open(self.manifestFile) as f:
            orig = f.read()
        self.manifest.writeOut(filepath="out_entity.manifest",force=True)
        with open("out_entity.manifest") as f:
            writeout = f.read()
        self.assertEqual(writeout.replace("\t",""),orig)
        os.remove("out_entity.manifest")

"""########################## Tests Galaxy Manifest ########################"""

autospecFileRef = co_attribs.FileRef("brushFile",val="foo",subdir="Galaxy",
        extension="galaxy")

def genMockFileRef(val,**kwargs):
    m = mock.MagicMock(spec_set=autospecFileRef)
    m.ref = val
    return m

def cmpParseResults(test,exp,act):
    expCount = exp["entries"]["counter"]
    expElements = exp["entries"]["elements"]
    actCounter = act["entries"]["counter"]
    actElements = act["entries"]["elements"]
    test.assertEqual(expCount["identifier"],actCounter["identifier"])
    test.assertEqual(expCount["val"],actCounter["val"])
    test.assertEqual(len(expElements),len(actElements))
    for expElem,actElem in zip(expElements,actElements):
        test.assertEqual(expElem["identifier"],actElem["identifier"])
        test.assertEqual(expElem["val"],actElem["val"])

class ManifestGalaxyMissingFile(unit.TestCase):
    desc = "Test ManifestGalaxy with missing file:"
    filepath = "missingManifest.manifest"
    expectedError = "File \'"+filepath+"\' does not exist."

    def testCreation(self):
        """ Test whether createInstance raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestGalaxy.createInstance(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

    def testParse(self):
        """ Test whether parse raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestGalaxy.parse(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

class ManifestGalaxyMalformed(unit.TestCase):
    desc = "Test ManifestGalaxy with malformed file:"

    def setUp(self):
        self.origProb = co_probs.ParseProblem
        patcher = mock.patch("pysoase.mod.manifest.co_probs.ParseProblem",
                autospec=True,spec_set=True)
        mockProb = patcher.start()
        self.addCleanup(patcher.stop)
        mockProb.side_effect = mock.create_autospec(spec=self.origProb,
                spec_set=True)

    def testMisspelledHeaderParse(self):
        """ Test whether parse throws correct error with misspellings
        """
        out,res = manifest.ManifestGalaxy.parse("galaxy_misspelled.manifest")
        self.assertFalse(out)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingParse(self):
        """ Test whether parse throws correct error with missing count line
        """
        succ,res = manifest.ManifestGalaxy.parse(
                "galaxy_missing_count.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsParse(self):
        """ Test whether parse throws correct error with too few elements
        """
        succ,res = manifest.ManifestGalaxy.parse("galaxy_too_few.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsParse(self):
        """ Test whether parse throws correct error with too many elements
        """
        succ,res = manifest.ManifestGalaxy.parse("galaxy_too_many.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testMisspelledHeaderCreateInstance(self):
        """ Test whether createInstance throws correct error with misspellings
        """
        res = manifest.ManifestGalaxy.createInstance(
                "galaxy_misspelled.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingCreateInstance(self):
        """ Test whether createInstance throws correct error with missing count line
        """
        res = manifest.ManifestGalaxy.createInstance(
                "galaxy_missing_count.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too few elements
        """
        res = manifest.ManifestGalaxy.createInstance("galaxy_too_few.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too many elements
        """
        res = manifest.ManifestGalaxy.createInstance(
                "galaxy_too_many.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

class ManifestGalaxyNoEntries(unit.TestCase):
    desc = "Test ManifestGalaxy with no entries:"

    def setUp(self):
        self.manifestFile = "galaxy_no_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":0}}}
        self.manifest = manifest.ManifestGalaxy(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestGalaxy.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestGalaxy)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertFalse(res.entries)
        self.assertListEqual(res.entries.elements,[])
        self.assertEqual(res.filedir,"Galaxy")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns an empty list
        """
        self.assertListEqual(self.manifest.getRefs(),[])

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        outcome,result = manifest.ManifestGalaxy.parse(self.manifestFile)
        self.assertTrue(outcome)
        expCount = self.manifestEntries["entries"]["counter"]
        actCounter = result["entries"]["counter"]
        self.assertNotIn("elements",result["entries"])
        self.assertEqual(expCount["identifier"],actCounter["identifier"])
        self.assertEqual(expCount["val"],actCounter["val"])

    def testCheckMissingEntry(self):
        """ Test whether checkEntry throws problems on random call
        """
        res = self.manifest.checkEntry("missing_ref")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],manifest.MissingManifestEntry)
        self.assertEqual(res[0].reference,"missing_ref")
        self.assertEqual(res[0].manifestType,"galaxy")

class ManifestGalaxyBadEntries(unit.TestCase):
    desc = "Test ManifestGalaxy with some bad entries:"

    def setUp(self):
        self.manifestFile = "galaxy_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"gal1"},
                {"identifier":"fileName","val":"gal2"},
                {"identifier":"fileName","val":"gal3"},
                {"identifier":"fileName","val":"gal3"},
                {"identifier":"fileName","val":"gal800"},
                {"identifier":"fileName","val":"gal4"},
                {"identifier":"fileName","val":"gal5"},
                {"identifier":"fileName","val":"gal100"}
            ]}}
        self.origRef = co_attribs.FileRef
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestGalaxy(self.manifestFile,
                self.manifestEntries["entries"])
        for elem in self.manifest.entries.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.manifest.entries.elements[4].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","gal800.galaxy")])
        self.manifest.entries.elements[7].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","gal100.galaxy")])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestGalaxy.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestGalaxy)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Galaxy")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(None)
        for prob in res:
            self.assertEqual(prob.probFile,"galaxy_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(None)
        self.assertEqual(len(res),3)

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="galaxy_bad_entries.manifest")
        res = self.manifest.check(None)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(None)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("gal800.galaxy",missRefs)
        self.assertIn("gal100.galaxy",missRefs)

class ManifestGalaxyGoodEntries(unit.TestCase):
    desc = "Test ManifestGalaxy with only good entries:"

    def setUp(self):
        self.manifestFile = "galaxy_good_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"gal1"},
                {"identifier":"fileName","val":"gal2"},
                {"identifier":"fileName","val":"gal3"},
                {"identifier":"fileName","val":"gal7"},
                {"identifier":"fileName","val":"gal8"},
                {"identifier":"fileName","val":"gal4"},
                {"identifier":"fileName","val":"gal5"},
                {"identifier":"fileName","val":"gal6"}
            ]}}
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        self.origRef = co_attribs.FileRef
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestGalaxy(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestGalaxy.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestGalaxy)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Galaxy")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns a list with all manifest entries
        """
        res = self.manifest.getRefs()
        self.assertEqual(len(res),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertIsInstance(res[0],self.origRef)
        for entry in self.manifest.entries.elements:
            self.assertIn(entry,res)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        self.maxDiff = None
        outcome,result = manifest.ManifestGalaxy.parse(self.manifestFile)
        self.assertTrue(outcome)
        cmpParseResults(self,self.manifestEntries,result)

    def testCheckMissingEntry(self):
        """ Test whether checkEntry returns no problem for any entry
        """
        for entry in self.manifestEntries["entries"]["elements"]:
            res = self.manifest.checkEntry(entry["val"])
            self.assertListEqual(res,[])

    @unit.expectedFailure
    def testCheckMissingEntryExt(self):
        """ Test whether checkEntry returns no problem for entry with file ext
        """
        res = self.manifest.checkEntry("gal1.galaxy")
        self.assertListEqual(res,[])

class ManifestGalaxyIntegration(unit.TestCase):
    desc = "Integration test for ManifestGalaxy"

    def setUp(self):
        self.manifestFile = "galaxy_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"gal1"},
                {"identifier":"fileName","val":"gal2"},
                {"identifier":"fileName","val":"gal3"},
                {"identifier":"fileName","val":"gal3"},
                {"identifier":"fileName","val":"gal800"},
                {"identifier":"fileName","val":"gal4"},
                {"identifier":"fileName","val":"gal5"},
                {"identifier":"fileName","val":"gal100"}
            ]}}
        self.manifest = manifest.ManifestGalaxy(self.manifestFile,
                self.manifestEntries["entries"])
        self.mod = tco.genMockMod("./")

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestGalaxy.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestGalaxy)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(res.filedir,"Galaxy")

    def testCreateInstanceEntries(self):
        """ Test whether the entries in the created instance are correct
        """
        res = manifest.ManifestGalaxy.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries.elements),
                len(self.manifestEntries["entries"]["elements"]))
        for act,exp in zip(res.entries.elements,
                self.manifestEntries["entries"]["elements"]):
            self.assertEqual(act.identifier,exp["identifier"])
            self.assertEqual(act.ref,exp["val"]+".galaxy")
            self.assertIsInstance(act,co_attribs.FileRef)

    def testCreateInstanceSorted(self):
        """ Test whether the sortedEntries are created correctly
        """
        exp = [val["val"]+".galaxy" for val
            in self.manifestEntries["entries"]["elements"]]
        res = manifest.ManifestGalaxy.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries),
                len(exp))
        self.assertCountEqual([val.ref for val in res.entries.elements],exp)

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(self.mod)
        for prob in res:
            self.assertEqual(prob.probFile,"galaxy_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(self.mod)
        self.assertEqual(len(res),3)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"]+".galaxy",
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="galaxy_bad_entries.manifest")
        res = self.manifest.check(self.mod)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(self.mod)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("gal800.galaxy",missRefs)
        self.assertIn("gal100.galaxy",missRefs)

    def testWriteOut(self):
        """ Test whether writeOut produces the expected file
        """
        with open(self.manifestFile) as f:
            orig = f.read()
        self.manifest.writeOut(filepath="out_galaxy.manifest",force=True)
        with open("out_galaxy.manifest") as f:
            writeout = f.read()
        self.assertEqual(writeout.replace("\t",""),orig)
        os.remove("out_galaxy.manifest")

"""###################### Tests PlayerPictures Manifest ####################"""

autospecFileRef = co_attribs.FileRef("brushFile",val="foo",subdir="Window",
        extension="playerPictures")

def genMockFileRef(val,**kwargs):
    m = mock.MagicMock(spec_set=autospecFileRef)
    m.ref = val
    return m

def cmpParseResults(test,exp,act):
    expCount = exp["entries"]["counter"]
    expElements = exp["entries"]["elements"]
    actCounter = act["entries"]["counter"]
    actElements = act["entries"]["elements"]
    test.assertEqual(expCount["identifier"],actCounter["identifier"])
    test.assertEqual(expCount["val"],actCounter["val"])
    test.assertEqual(len(expElements),len(actElements))
    for expElem,actElem in zip(expElements,actElements):
        test.assertEqual(expElem["identifier"],actElem["identifier"])
        test.assertEqual(expElem["val"],actElem["val"])

class ManifestPlayerPictureMissingFile(unit.TestCase):
    desc = "Test ManifestPlayerPicture with missing file:"
    filepath = "missingManifest.manifest"
    expectedError = "File \'"+filepath+"\' does not exist."

    def testCreation(self):
        """ Test whether createInstance raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestPlayerPicture.createInstance(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

    def testParse(self):
        """ Test whether parse raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestPlayerPicture.parse(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

class ManifestPlayerPictureMalformed(unit.TestCase):
    desc = "Test ManifestPlayerPicture with malformed file:"

    def setUp(self):
        self.origProb = co_probs.ParseProblem
        patcher = mock.patch("pysoase.mod.manifest.co_probs.ParseProblem",
                autospec=True,spec_set=True)
        mockProb = patcher.start()
        self.addCleanup(patcher.stop)
        mockProb.side_effect = mock.create_autospec(spec=self.origProb,
                spec_set=True)

    def testMisspelledHeaderParse(self):
        """ Test whether parse throws correct error with misspellings
        """
        out,res = manifest.ManifestPlayerPicture.parse(
                "playerpic_misspelled.manifest")
        self.assertFalse(out)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingParse(self):
        """ Test whether parse throws correct error with missing count line
        """
        succ,res = manifest.ManifestPlayerPicture.parse(
                "playerpic_missing_count.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsParse(self):
        """ Test whether parse throws correct error with too few elements
        """
        succ,res = manifest.ManifestPlayerPicture.parse(
                "playerpic_too_few.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsParse(self):
        """ Test whether parse throws correct error with too many elements
        """
        succ,res = manifest.ManifestPlayerPicture.parse(
                "playerpic_too_many.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testMisspelledHeaderCreateInstance(self):
        """ Test whether createInstance throws correct error with misspellings
        """
        res = manifest.ManifestPlayerPicture.createInstance(
                "playerpic_misspelled.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingCreateInstance(self):
        """ Test whether createInstance throws correct error with missing count line
        """
        res = manifest.ManifestPlayerPicture.createInstance(
                "playerpic_missing_count.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too few elements
        """
        res = manifest.ManifestPlayerPicture.createInstance(
                "playerpic_too_few.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too many elements
        """
        res = manifest.ManifestPlayerPicture.createInstance(
                "playerpic_too_many.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

class ManifestPlayerPictureNoEntries(unit.TestCase):
    desc = "Test ManifestPlayerPicture with no entries:"

    def setUp(self):
        self.manifestFile = "playerpic_no_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":0}}}
        self.manifest = manifest.ManifestPlayerPicture(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerPicture.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerPicture)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertFalse(res.entries)
        self.assertListEqual(res.entries.elements,[])
        self.assertEqual(res.filedir,"Window")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns an empty list
        """
        self.assertListEqual(self.manifest.getRefs(),[])

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        outcome,result = manifest.ManifestPlayerPicture.parse(self.manifestFile)
        self.assertTrue(outcome)
        expCount = self.manifestEntries["entries"]["counter"]
        actCounter = result["entries"]["counter"]
        self.assertNotIn("elements",result["entries"])
        self.assertEqual(expCount["identifier"],actCounter["identifier"])
        self.assertEqual(expCount["val"],actCounter["val"])

    def testCheckMissingEntry(self):
        """ Test whether checkEntry throws problems on random call
        """
        res = self.manifest.checkEntry("missing_ref")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],manifest.MissingManifestEntry)
        self.assertEqual(res[0].reference,"missing_ref")
        self.assertEqual(res[0].manifestType,"playerPictures")

class ManifestPlayerPictureBadEntries(unit.TestCase):
    desc = "Test ManifestPlayerPicture with some bad entries:"

    def setUp(self):
        self.manifestFile = "playerpic_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"playpic1.playerPictures"},
                {"identifier":"fileName","val":"playpic2.playerPictures"},
                {"identifier":"fileName","val":"playpic3.playerPictures"},
                {"identifier":"fileName","val":"playpic3.playerPictures"},
                {"identifier":"fileName","val":"playpic800.playerPictures"},
                {"identifier":"fileName","val":"playpic4.playerPictures"},
                {"identifier":"fileName","val":"playpic5.playerPictures"},
                {"identifier":"fileName","val":"playpic100.playerPictures"}
            ]}}
        self.origRef = co_attribs.FileRef
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestPlayerPicture(self.manifestFile,
                self.manifestEntries["entries"])
        for elem in self.manifest.entries.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.manifest.entries.elements[4].checkExistence = mock.MagicMock(
                return_value=[
                    co_probs.MissingRefProblem("File","playpic800.playerPictures")])
        self.manifest.entries.elements[7].checkExistence = mock.MagicMock(
                return_value=[
                    co_probs.MissingRefProblem("File","playpic100.playerPictures")])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerPicture.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerPicture)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Window")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(None)
        for prob in res:
            self.assertEqual(prob.probFile,"playerpic_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(None)
        self.assertEqual(len(res),3)

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="playerpic_bad_entries.manifest")
        res = self.manifest.check(None)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(None)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("playpic800.playerPictures",missRefs)
        self.assertIn("playpic100.playerPictures",missRefs)

class ManifestPlayerPictureGoodEntries(unit.TestCase):
    desc = "Test ManifestPlayerPicture with only good entries:"

    def setUp(self):
        self.manifestFile = "playerpic_good_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"playpic1.playerPictures"},
                {"identifier":"fileName","val":"playpic2.playerPictures"},
                {"identifier":"fileName","val":"playpic3.playerPictures"},
                {"identifier":"fileName","val":"playpic7.playerPictures"},
                {"identifier":"fileName","val":"playpic8.playerPictures"},
                {"identifier":"fileName","val":"playpic4.playerPictures"},
                {"identifier":"fileName","val":"playpic5.playerPictures"},
                {"identifier":"fileName","val":"playpic6.playerPictures"}
            ]}}
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        self.origRef = co_attribs.FileRef
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestPlayerPicture(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerPicture.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerPicture)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Window")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns a list with all manifest entries
        """
        res = self.manifest.getRefs()
        self.assertEqual(len(res),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertIsInstance(res[0],self.origRef)
        for entry in self.manifest.entries.elements:
            self.assertIn(entry,res)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        self.maxDiff = None
        outcome,result = manifest.ManifestPlayerPicture.parse(self.manifestFile)
        self.assertTrue(outcome)
        cmpParseResults(self,self.manifestEntries,result)

    def testCheckMissingEntry(self):
        """ Test whether checkEntry returns no problem for any entry
        """
        for entry in self.manifestEntries["entries"]["elements"]:
            res = self.manifest.checkEntry(entry["val"])
            self.assertListEqual(res,[])

    def testCheckMissingEntryExt(self):
        """ Test whether checkEntry returns no problem for entry with file ext
        """
        res = self.manifest.checkEntry("playpic1.playerPictures")
        self.assertListEqual(res,[])

class ManifestPlayerPictureIntegration(unit.TestCase):
    desc = "Integration test for ManifestPlayerPicture"

    def setUp(self):
        self.manifestFile = "playerpic_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"playpic1.playerPictures"},
                {"identifier":"fileName","val":"playpic2.playerPictures"},
                {"identifier":"fileName","val":"playpic3.playerPictures"},
                {"identifier":"fileName","val":"playpic3.playerPictures"},
                {"identifier":"fileName","val":"playpic800.playerPictures"},
                {"identifier":"fileName","val":"playpic4.playerPictures"},
                {"identifier":"fileName","val":"playpic5.playerPictures"},
                {"identifier":"fileName","val":"playpic100.playerPictures"}
            ]}}
        self.manifest = manifest.ManifestPlayerPicture(self.manifestFile,
                self.manifestEntries["entries"])
        self.mod = tco.genMockMod("./")

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerPicture.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerPicture)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(res.filedir,"Window")

    def testCreateInstanceEntries(self):
        """ Test whether the entries in the created instance are correct
        """
        res = manifest.ManifestPlayerPicture.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries.elements),
                len(self.manifestEntries["entries"]["elements"]))
        for act,exp in zip(res.entries.elements,
                self.manifestEntries["entries"]["elements"]):
            self.assertEqual(act.identifier,exp["identifier"])
            self.assertEqual(act.ref,exp["val"])
            self.assertIsInstance(act,co_attribs.FileRef)

    def testCreateInstanceSorted(self):
        """ Test whether the sortedEntries are created correctly
        """
        exp = [val["val"] for val
            in self.manifestEntries["entries"]["elements"]]
        res = manifest.ManifestPlayerPicture.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries),
                len(exp))
        self.assertCountEqual([val.ref for val in res.entries.elements],exp)

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(self.mod)
        for prob in res:
            self.assertEqual(prob.probFile,"playerpic_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(self.mod)
        self.assertEqual(len(res),3)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="playerpic_bad_entries.manifest")
        res = self.manifest.check(self.mod)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(self.mod)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("playpic800.playerPictures",missRefs)
        self.assertIn("playpic100.playerPictures",missRefs)

    def testWriteOut(self):
        """ Test whether writeOut produces the expected file
        """
        with open(self.manifestFile) as f:
            orig = f.read()
        self.manifest.writeOut(filepath="out_playerpic.manifest",force=True)
        with open("out_playerpic.manifest") as f:
            writeout = f.read()
        self.assertEqual(writeout.replace("\t",""),orig)
        os.remove("out_playerpic.manifest")

"""####################### Tests PlayerThemes Manifest #####################"""

autospecFileRef = co_attribs.FileRef("brushFile",val="foo",subdir="Window",
        extension="playerThemes")

def genMockFileRef(val,**kwargs):
    m = mock.MagicMock(spec_set=autospecFileRef)
    m.ref = val
    return m

def cmpParseResults(test,exp,act):
    expCount = exp["entries"]["counter"]
    expElements = exp["entries"]["elements"]
    actCounter = act["entries"]["counter"]
    actElements = act["entries"]["elements"]
    test.assertEqual(expCount["identifier"],actCounter["identifier"])
    test.assertEqual(expCount["val"],actCounter["val"])
    test.assertEqual(len(expElements),len(actElements))
    for expElem,actElem in zip(expElements,actElements):
        test.assertEqual(expElem["identifier"],actElem["identifier"])
        test.assertEqual(expElem["val"],actElem["val"])

class ManifestPlayerThemeMissingFile(unit.TestCase):
    desc = "Test ManifestPlayerTheme with missing file:"
    filepath = "missingManifest.manifest"
    expectedError = "File \'"+filepath+"\' does not exist."

    def testCreation(self):
        """ Test whether createInstance raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestPlayerTheme.createInstance(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

    def testParse(self):
        """ Test whether parse raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestPlayerTheme.parse(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

class ManifestPlayerThemeMalformed(unit.TestCase):
    desc = "Test ManifestPlayerTheme with malformed file:"

    def setUp(self):
        self.origProb = co_probs.ParseProblem
        patcher = mock.patch("pysoase.mod.manifest.co_probs.ParseProblem",
                autospec=True,spec_set=True)
        mockProb = patcher.start()
        self.addCleanup(patcher.stop)
        mockProb.side_effect = mock.create_autospec(spec=self.origProb,
                spec_set=True)

    def testMisspelledHeaderParse(self):
        """ Test whether parse throws correct error with misspellings
        """
        out,res = manifest.ManifestPlayerTheme.parse(
                "playertheme_misspelled.manifest")
        self.assertFalse(out)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingParse(self):
        """ Test whether parse throws correct error with missing count line
        """
        succ,res = manifest.ManifestPlayerTheme.parse(
                "playertheme_missing_count.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsParse(self):
        """ Test whether parse throws correct error with too few elements
        """
        succ,res = manifest.ManifestPlayerTheme.parse(
                "playertheme_too_few.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsParse(self):
        """ Test whether parse throws correct error with too many elements
        """
        succ,res = manifest.ManifestPlayerTheme.parse(
                "playertheme_too_many.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testMisspelledHeaderCreateInstance(self):
        """ Test whether createInstance throws correct error with misspellings
        """
        res = manifest.ManifestPlayerTheme.createInstance(
                "playertheme_misspelled.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingCreateInstance(self):
        """ Test whether createInstance throws correct error with missing count line
        """
        res = manifest.ManifestPlayerTheme.createInstance(
                "playertheme_missing_count.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too few elements
        """
        res = manifest.ManifestPlayerTheme.createInstance(
                "playertheme_too_few.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too many elements
        """
        res = manifest.ManifestPlayerTheme.createInstance(
                "playertheme_too_many.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

class ManifestPlayerThemeNoEntries(unit.TestCase):
    desc = "Test ManifestPlayerTheme with no entries:"

    def setUp(self):
        self.manifestFile = "playertheme_no_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":0}}}
        self.manifest = manifest.ManifestPlayerTheme(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerTheme.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerTheme)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertFalse(res.entries)
        self.assertListEqual(res.entries.elements,[])
        self.assertEqual(res.filedir,"Window")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns an empty list
        """
        self.assertListEqual(self.manifest.getRefs(),[])

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        outcome,result = manifest.ManifestPlayerTheme.parse(self.manifestFile)
        self.assertTrue(outcome)
        expCount = self.manifestEntries["entries"]["counter"]
        actCounter = result["entries"]["counter"]
        self.assertNotIn("elements",result["entries"])
        self.assertEqual(expCount["identifier"],actCounter["identifier"])
        self.assertEqual(expCount["val"],actCounter["val"])

    def testCheckMissingEntry(self):
        """ Test whether checkEntry throws problems on random call
        """
        res = self.manifest.checkEntry("missing_ref")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],manifest.MissingManifestEntry)
        self.assertEqual(res[0].reference,"missing_ref")
        self.assertEqual(res[0].manifestType,"playerThemes")

class ManifestPlayerThemeBadEntries(unit.TestCase):
    desc = "Test ManifestPlayerTheme with some bad entries:"

    def setUp(self):
        self.manifestFile = "playertheme_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"playtheme1.playerThemes"},
                {"identifier":"fileName","val":"playtheme2.playerThemes"},
                {"identifier":"fileName","val":"playtheme3.playerThemes"},
                {"identifier":"fileName","val":"playtheme3.playerThemes"},
                {"identifier":"fileName","val":"playtheme800.playerThemes"},
                {"identifier":"fileName","val":"playtheme4.playerThemes"},
                {"identifier":"fileName","val":"playtheme5.playerThemes"},
                {"identifier":"fileName","val":"playtheme100.playerThemes"}
            ]}}
        self.origRef = co_attribs.FileRef
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestPlayerTheme(self.manifestFile,
                self.manifestEntries["entries"])
        for elem in self.manifest.entries.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.manifest.entries.elements[4].checkExistence = mock.MagicMock(
                return_value=[
                    co_probs.MissingRefProblem("File","playtheme800.playerThemes")])
        self.manifest.entries.elements[7].checkExistence = mock.MagicMock(
                return_value=[
                    co_probs.MissingRefProblem("File","playtheme100.playerThemes")])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerTheme.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerTheme)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Window")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(None)
        for prob in res:
            self.assertEqual(prob.probFile,"playertheme_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(None)
        self.assertEqual(len(res),3)

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="playertheme_bad_entries.manifest")
        res = self.manifest.check(None)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(None)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("playtheme800.playerThemes",missRefs)
        self.assertIn("playtheme100.playerThemes",missRefs)

class ManifestPlayerThemeGoodEntries(unit.TestCase):
    desc = "Test ManifestPlayerTheme with only good entries:"

    def setUp(self):
        self.manifestFile = "playertheme_good_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"playtheme1.playerThemes"},
                {"identifier":"fileName","val":"playtheme2.playerThemes"},
                {"identifier":"fileName","val":"playtheme3.playerThemes"},
                {"identifier":"fileName","val":"playtheme7.playerThemes"},
                {"identifier":"fileName","val":"playtheme8.playerThemes"},
                {"identifier":"fileName","val":"playtheme4.playerThemes"},
                {"identifier":"fileName","val":"playtheme5.playerThemes"},
                {"identifier":"fileName","val":"playtheme6.playerThemes"}
            ]}}
        patcher = mock.patch("pysoase.mod.manifest.co_attribs.FileRef",
                autospec=True,spec_set=True)
        self.origRef = co_attribs.FileRef
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockFileRef
        self.manifest = manifest.ManifestPlayerTheme(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerTheme.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerTheme)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Window")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns a list with all manifest entries
        """
        res = self.manifest.getRefs()
        self.assertEqual(len(res),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertIsInstance(res[0],self.origRef)
        for entry in self.manifest.entries.elements:
            self.assertIn(entry,res)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        self.maxDiff = None
        outcome,result = manifest.ManifestPlayerTheme.parse(self.manifestFile)
        self.assertTrue(outcome)
        cmpParseResults(self,self.manifestEntries,result)

    def testCheckMissingEntry(self):
        """ Test whether checkEntry returns no problem for any entry
        """
        for entry in self.manifestEntries["entries"]["elements"]:
            res = self.manifest.checkEntry(entry["val"])
            self.assertListEqual(res,[])

    def testCheckMissingEntryExt(self):
        """ Test whether checkEntry returns no problem for entry with file ext
        """
        res = self.manifest.checkEntry("playtheme1.playerThemes")
        self.assertListEqual(res,[])

class ManifestPlayerThemeIntegration(unit.TestCase):
    desc = "Integration test for ManifestPlayerTheme"

    def setUp(self):
        self.manifestFile = "playertheme_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"fileCount","val":8},
            "elements":[
                {"identifier":"fileName","val":"playtheme1.playerThemes"},
                {"identifier":"fileName","val":"playtheme2.playerThemes"},
                {"identifier":"fileName","val":"playtheme3.playerThemes"},
                {"identifier":"fileName","val":"playtheme3.playerThemes"},
                {"identifier":"fileName","val":"playtheme800.playerThemes"},
                {"identifier":"fileName","val":"playtheme4.playerThemes"},
                {"identifier":"fileName","val":"playtheme5.playerThemes"},
                {"identifier":"fileName","val":"playtheme100.playerThemes"}
            ]}}
        self.manifest = manifest.ManifestPlayerTheme(self.manifestFile,
                self.manifestEntries["entries"])
        self.mod = tco.genMockMod("./")

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestPlayerTheme.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestPlayerTheme)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(res.filedir,"Window")

    def testCreateInstanceEntries(self):
        """ Test whether the entries in the created instance are correct
        """
        res = manifest.ManifestPlayerTheme.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries.elements),
                len(self.manifestEntries["entries"]["elements"]))
        for act,exp in zip(res.entries.elements,
                self.manifestEntries["entries"]["elements"]):
            self.assertEqual(act.identifier,exp["identifier"])
            self.assertEqual(act.ref,exp["val"])
            self.assertIsInstance(act,co_attribs.FileRef)

    def testCreateInstanceSorted(self):
        """ Test whether the sortedEntries are created correctly
        """
        exp = [val["val"] for val
            in self.manifestEntries["entries"]["elements"]]
        res = manifest.ManifestPlayerTheme.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries),
                len(exp))
        self.assertCountEqual([val.ref for val in res.entries.elements],exp)

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(self.mod)
        for prob in res:
            self.assertEqual(prob.probFile,"playertheme_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(self.mod)
        self.assertEqual(len(res),3)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="playertheme_bad_entries.manifest")
        res = self.manifest.check(self.mod)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(self.mod)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("playtheme800.playerThemes",missRefs)
        self.assertIn("playtheme100.playerThemes",missRefs)

    def testWriteOut(self):
        """ Test whether writeOut produces the expected file
        """
        with open(self.manifestFile) as f:
            orig = f.read()
        self.manifest.writeOut(filepath="out_playertheme.manifest",force=True)
        with open("out_playertheme.manifest") as f:
            writeout = f.read()
        self.assertEqual(writeout.replace("\t",""),orig)
        os.remove("out_playertheme.manifest")

"""########################## Tests Skybox Manifest ########################"""

autospecMeshRef = meshes.MeshRef("brushFile",val="foo")

def genMockMeshRef(val,**kwargs):
    m = mock.MagicMock(spec_set=autospecMeshRef)
    m.ref = val
    return m

def cmpParseResults(test,exp,act):
    expCount = exp["entries"]["counter"]
    expElements = exp["entries"]["elements"]
    actCounter = act["entries"]["counter"]
    actElements = act["entries"]["elements"]
    test.assertEqual(expCount["identifier"],actCounter["identifier"])
    test.assertEqual(expCount["val"],actCounter["val"])
    test.assertEqual(len(expElements),len(actElements))
    for expElem,actElem in zip(expElements,actElements):
        test.assertEqual(expElem["identifier"],actElem["identifier"])
        test.assertEqual(expElem["val"],actElem["val"])

class ManifestSkyboxMissingFile(unit.TestCase):
    desc = "Test ManifestSkybox with missing file:"
    filepath = "missingManifest.manifest"
    expectedError = "File \'"+filepath+"\' does not exist."

    def testCreation(self):
        """ Test whether createInstance raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestSkybox.createInstance(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

    def testParse(self):
        """ Test whether parse raises FileNotFound
        """
        with self.assertRaises(FileNotFoundError) as cm:
            manifest.ManifestSkybox.parse(self.filepath)
        self.assertEqual(self.expectedError,str(cm.exception))

class ManifestSkyboxMalformed(unit.TestCase):
    desc = "Test ManifestSkybox with malformed file:"

    def setUp(self):
        self.origProb = co_probs.ParseProblem
        patcher = mock.patch("pysoase.mod.manifest.co_probs.ParseProblem",
                autospec=True,spec_set=True)
        mockProb = patcher.start()
        self.addCleanup(patcher.stop)
        mockProb.side_effect = mock.create_autospec(spec=self.origProb,
                spec_set=True)

    def testMisspelledHeaderParse(self):
        """ Test whether parse throws correct error with misspellings
        """
        out,res = manifest.ManifestSkybox.parse("skybox_misspelled.manifest")
        self.assertFalse(out)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingParse(self):
        """ Test whether parse throws correct error with missing count line
        """
        succ,res = manifest.ManifestSkybox.parse(
                "skybox_missing_count.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsParse(self):
        """ Test whether parse throws correct error with too few elements
        """
        succ,res = manifest.ManifestSkybox.parse("skybox_too_few.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsParse(self):
        """ Test whether parse throws correct error with too many elements
        """
        succ,res = manifest.ManifestSkybox.parse("skybox_too_many.manifest")
        self.assertFalse(succ)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testMisspelledHeaderCreateInstance(self):
        """ Test whether createInstance throws correct error with misspellings
        """
        res = manifest.ManifestSkybox.createInstance(
                "skybox_misspelled.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountMissingCreateInstance(self):
        """ Test whether createInstance throws correct error with missing count line
        """
        res = manifest.ManifestSkybox.createInstance(
                "skybox_missing_count.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooFewElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too few elements
        """
        res = manifest.ManifestSkybox.createInstance("skybox_too_few.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

    def testCountTooManyElementsCreateInstance(self):
        """ Test whether createInstance throws correct error with too many elements
        """
        res = manifest.ManifestSkybox.createInstance(
                "skybox_too_many.manifest")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],self.origProb)

class ManifestSkyboxNoEntries(unit.TestCase):
    desc = "Test ManifestSkybox with no entries:"

    def setUp(self):
        self.manifestFile = "skybox_no_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"meshFileCount","val":0}}}
        self.manifest = manifest.ManifestSkybox(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestSkybox.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestSkybox)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertFalse(res.entries)
        self.assertListEqual(res.entries.elements,[])
        self.assertEqual(res.filedir,"Mesh")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns an empty list
        """
        self.assertListEqual(self.manifest.getRefs(),[])

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        outcome,result = manifest.ManifestSkybox.parse(self.manifestFile)
        self.assertTrue(outcome)
        expCount = self.manifestEntries["entries"]["counter"]
        actCounter = result["entries"]["counter"]
        self.assertNotIn("elements",result["entries"])
        self.assertEqual(expCount["identifier"],actCounter["identifier"])
        self.assertEqual(expCount["val"],actCounter["val"])

    def testCheckMissingEntry(self):
        """ Test whether checkEntry throws problems on random call
        """
        res = self.manifest.checkEntry("missing_ref")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],manifest.MissingManifestEntry)
        self.assertEqual(res[0].reference,"missing_ref")
        self.assertEqual(res[0].manifestType,"skybox")

class ManifestSkyboxBadEntries(unit.TestCase):
    desc = "Test ManifestSkybox with some bad entries:"

    def setUp(self):
        self.manifestFile = "skybox_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"meshFileCount","val":8},
            "elements":[
                {"identifier":"meshFileName","val":"sky1.mesh"},
                {"identifier":"meshFileName","val":"sky2.mesh"},
                {"identifier":"meshFileName","val":"sky3.mesh"},
                {"identifier":"meshFileName","val":"sky3.mesh"},
                {"identifier":"meshFileName","val":"sky800.mesh"},
                {"identifier":"meshFileName","val":"sky4.mesh"},
                {"identifier":"meshFileName","val":"sky5.mesh"},
                {"identifier":"meshFileName","val":"sky100.mesh"}
            ]}}
        self.origRef = meshes.MeshRef
        patcher = mock.patch("pysoase.mod.manifest.meshes.MeshRef",
                autospec=True,spec_set=True)
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockMeshRef
        self.manifest = manifest.ManifestSkybox(self.manifestFile,
                self.manifestEntries["entries"])
        for elem in self.manifest.entries.elements:
            elem.check = mock.MagicMock(return_value=[])
        self.manifest.entries.elements[4].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","sky800.mesh")])
        self.manifest.entries.elements[7].checkExistence = mock.MagicMock(
                return_value=[co_probs.MissingRefProblem("File","sky100.mesh")])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestSkybox.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestSkybox)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Mesh")

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(None)
        for prob in res:
            self.assertEqual(prob.probFile,"skybox_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(None)
        self.assertEqual(len(res),3)

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="skybox_bad_entries.manifest")
        res = self.manifest.check(None)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(None)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("sky800.mesh",missRefs)
        self.assertIn("sky100.mesh",missRefs)

class ManifestSkyboxGoodEntries(unit.TestCase):
    desc = "Test ManifestSkybox with only good entries:"

    def setUp(self):
        self.manifestFile = "skybox_good_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"meshFileCount","val":8},
            "elements":[
                {"identifier":"meshFileName","val":"sky1.mesh"},
                {"identifier":"meshFileName","val":"sky2.mesh"},
                {"identifier":"meshFileName","val":"sky3.mesh"},
                {"identifier":"meshFileName","val":"sky7.mesh"},
                {"identifier":"meshFileName","val":"sky8.mesh"},
                {"identifier":"meshFileName","val":"sky4.mesh"},
                {"identifier":"meshFileName","val":"sky5.mesh"},
                {"identifier":"meshFileName","val":"sky6.mesh"}
            ]}}
        patcher = mock.patch("pysoase.mod.manifest.meshes.MeshRef",
                autospec=True,spec_set=True)
        self.origRef = meshes.MeshRef
        mockRef = patcher.start()
        self.addCleanup(patcher.stop)
        mockRef.side_effect = genMockMeshRef
        self.manifest = manifest.ManifestSkybox(self.manifestFile,
                self.manifestEntries["entries"])

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestSkybox.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestSkybox)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertEqual(len(res.entries),
                len(self.manifestEntries["entries"]["elements"]))
        for elem in self.manifest.entries.elements:
            self.assertIsInstance(elem,self.origRef)
        self.assertEqual(res.filedir,"Mesh")

    def testCheck(self):
        """ Test whether check returns an empty list
        """
        self.assertListEqual(self.manifest.check(None),[])

    def testCheckCalled(self):
        """ Test whether check calls all reference check methods
        """
        self.manifest.check(None)
        for elem in self.manifest.entries.elements:
            elem.checkExistence.assert_called_once_with(None)

    def testCheckRefs(self):
        """ Test whether checkRefs returns an empty list
        """
        self.assertListEqual(self.manifest.checkRefs(None),[])

    def testGetRefs(self):
        """ Test whether getRefs returns a list with all manifest entries
        """
        res = self.manifest.getRefs()
        self.assertEqual(len(res),
                len(self.manifestEntries["entries"]["elements"]))
        self.assertIsInstance(res[0],self.origRef)
        for entry in self.manifest.entries.elements:
            self.assertIn(entry,res)

    def testRemoveDuplicates(self):
        """ Test whether removeDuplicates returns an empty list
        """
        self.assertListEqual(self.manifest.removeDuplicates(),[])

    def testParse(self):
        """ Test whether parse works and returns the correct result
        """
        self.maxDiff = None
        outcome,result = manifest.ManifestSkybox.parse(self.manifestFile)
        self.assertTrue(outcome)
        cmpParseResults(self,self.manifestEntries,result)

    def testCheckMissingEntry(self):
        """ Test whether checkEntry returns no problem for any entry
        """
        for entry in self.manifestEntries["entries"]["elements"]:
            res = self.manifest.checkEntry(entry["val"])
            self.assertListEqual(res,[])

class ManifestSkyboxIntegration(unit.TestCase):
    desc = "Integration test for ManifestSkybox"

    def setUp(self):
        self.manifestFile = "skybox_bad_entries.manifest"
        self.manifestEntries = {"entries":{
            "counter":{"identifier":"meshFileCount","val":8},
            "elements":[
                {"identifier":"meshFileName","val":"sky1.mesh"},
                {"identifier":"meshFileName","val":"sky2.mesh"},
                {"identifier":"meshFileName","val":"sky3.mesh"},
                {"identifier":"meshFileName","val":"sky3.mesh"},
                {"identifier":"meshFileName","val":"sky800.mesh"},
                {"identifier":"meshFileName","val":"sky4.mesh"},
                {"identifier":"meshFileName","val":"sky5.mesh"},
                {"identifier":"meshFileName","val":"sky100.mesh"}
            ]}}
        self.manifest = manifest.ManifestSkybox(self.manifestFile,
                self.manifestEntries["entries"])
        self.mod = tco.genMockMod("./")

    def testCreateInstance(self):
        """ Test whether createInstance produces a correct instance
        """
        res = manifest.ManifestSkybox.createInstance(self.manifestFile)
        self.assertIsInstance(res,manifest.ManifestSkybox)
        self.assertEqual(res.filepath,self.manifestFile)
        self.assertEqual(res.filedir,"Mesh")

    def testCreateInstanceEntries(self):
        """ Test whether the entries in the created instance are correct
        """
        res = manifest.ManifestSkybox.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries.elements),
                len(self.manifestEntries["entries"]["elements"]))
        for act,exp in zip(res.entries.elements,
                self.manifestEntries["entries"]["elements"]):
            self.assertEqual(act.identifier,exp["identifier"])
            self.assertEqual(act.ref,exp["val"])
            self.assertIsInstance(act,co_attribs.FileRef)

    def testCreateInstanceSorted(self):
        """ Test whether the sortedEntries are created correctly
        """
        exp = [val["val"] for val
            in self.manifestEntries["entries"]["elements"]]
        res = manifest.ManifestSkybox.createInstance(self.manifestFile)
        self.assertEqual(len(res.entries),len(exp))
        self.assertCountEqual([val.ref for val in res.entries.elements],exp)

    def testProblemFileAttrib(self):
        """ Test whether the file attribute is set correctly on the problems
        """
        res = self.manifest.check(self.mod)
        for prob in res:
            self.assertEqual(prob.probFile,"skybox_bad_entries.manifest")

    def testProblemCount(self):
        """ Test whether the correct number of problems is produced
        """
        res = self.manifest.check(self.mod)
        self.assertEqual(len(res),3)

    def testCheckDuplicates(self):
        """ Test whether check returns the correct duplicate problem
        """
        exp = co_probs.DuplicateEntryProb(2,
                self.manifestEntries["entries"]["elements"][2]["val"],
                self.manifestEntries["entries"]["counter"]["identifier"],
                probFile="skybox_bad_entries.manifest")
        res = self.manifest.check(self.mod)
        basicProbs = [prob for prob in res if
            isinstance(prob,co_probs.DuplicateEntryProb)]
        self.assertEqual(len(basicProbs),1)
        self.assertEqual(basicProbs[0],exp)

    def testCheckMissing(self):
        """ Test whether check returns the correct MssingRef problems
        """
        res = self.manifest.check(self.mod)
        missProbs = [prob for prob in res
            if isinstance(prob,co_probs.MissingRefProblem)]
        self.assertEqual(len(missProbs),2)
        missRefs = [prob.ref for prob in missProbs]
        self.assertIn("sky800.mesh",missRefs)
        self.assertIn("sky100.mesh",missRefs)

    def testWriteOut(self):
        """ Test whether writeOut produces the expected file
        """
        with open(self.manifestFile) as f:
            orig = f.read()
        self.manifest.writeOut(filepath="out_skybox.manifest",force=True)
        with open("out_skybox.manifest") as f:
            writeout = f.read()
        self.assertEqual(writeout.replace("\t",""),orig)
        os.remove("out_skybox.manifest")

"""####################### Tests MissingManifestEntry ######################"""

class MissingManifestEntry(unit.TestCase):
    desc = "Test MissingManifestEntry with good values:"

    manifestTypes = ["brush","entity","galaxy","playerPictures",
        "playerThemes","skybox"]

    def setUp(self):
        self.prob = manifest.MissingManifestEntry("myref.entity","entity")

    def testCreation(self):
        """ Test whether creation works correctly for all manifest types
        """
        for manType in self.manifestTypes:
            with self.subTest(manType=manType):
                res = manifest.MissingManifestEntry("myref",manType)
                self.assertEqual(res.reference,"myref")
                self.assertEqual(res.manifestType,manType)
                self.assertEqual(res.severity,
                        co_probs.ProblemSeverity.warn)

    def testSetManType(self):
        """ Test whether all manifest types are accepted for manifest setting
        """
        for manType in self.manifestTypes:
            with self.subTest(manType=manType):
                self.prob.manifestType = manType
                self.assertEqual(self.prob.manifestType,manType)

    def testOutput(self):
        """ Test whether the toString method works correctly
        """
        exp = ("WARN: The reference \'myref.entity\' has no entry in the "
               "entity manifest.\n")
        self.assertMultiLineEqual(self.prob.toString(0),exp)

    def testEquality(self):
        """ Test whether two similar instances test equal
        """
        inst1 = manifest.MissingManifestEntry("myref.entity","entity")
        self.assertEqual(self.prob,inst1)

class MissingManifestEntryInvalidTypes(unit.TestCase):
    desc = "Test MissingManifestEntry with invalid types"

    def setUp(self):
        self.prob = manifest.MissingManifestEntry("myref.entity","entity")
        self.invalidTypes = ["foo","burshes"]
        self.manifestTypes = ["brush","entity","galaxy","playerPictures",
            "playerThemes","skybox"]

    def testCreation(self):
        """ Test whether instantiation throws errors with invalid types
        """
        msg = ("{} is not a valid manifest type. Valid values are "
               +str(self.manifestTypes))
        for manType in self.invalidTypes:
            with self.subTest(manType=manType):
                with self.assertRaises(ValueError) as cm:
                    manifest.MissingManifestEntry("myref",manType)
                self.assertEqual(msg.format(manType),str(cm.exception))

    def testSetType(self):
        """ Test whether trying to set invalid manifest type throws error
        """
        msg = ("{} is not a valid manifest type. Valid values are "
               +str(self.manifestTypes))
        for manType in self.invalidTypes:
            with self.subTest(manType=manType):
                with self.assertRaises(ValueError) as cm:
                    self.prob.manifestType = manType
                self.assertEqual(msg.format(manType),str(cm.exception))
