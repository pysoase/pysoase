""" Unit tests for the pysoase.mod.ui module.
"""

import os
import unittest.mock as mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.manifest as manifest
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","ui")
testmodule = "pysoase.mod.ui"

"""############################ Tests StringRef ############################"""

class TestStringRef:
    desc = "Test StringRef class:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.mmod = tco.genMockMod("./")
        cls.parseString = (
            "Ident \"myRef\"\n"
        )
        cls.identifier = "Ident"
        cls.reference = "myRef"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.StringReference(
            identifier=self.identifier,
            val=self.reference
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("Ident").parseString(
                self.parseString
        )[0]
        res = ui.StringReference(**parseRes)
        assert res == self.inst

    def testOutput(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    @pytest.fixture()
    def checkFixture(self):
        yield
        self.mmod.strings.checkString.reset_mock()

    def testCheck(self,checkFixture):
        """ Test whether check calls correct method
        """
        self.inst.check(self.mmod)
        self.mmod.strings.checkString.assert_called_once_with("myRef")

    def testCheckEmptyRef(self,checkFixture):
        """ Test whether check tests for empty reference
        """
        ref = ui.StringReference(self.identifier,"")
        res = ref.check(self.mmod)
        assert not self.mmod.strings.checkString.called
        assert len(res) == 1
        assert isinstance(res[0],co_probs.BasicProblem)

    def testEqualityEqual(self):
        """ Test whether equal refs test as equal
        """
        ref = ui.StringReference(self.identifier,self.reference)
        assert ref == self.inst

    def testEqualityUnequal(self):
        """ Test whether refs with different identifier and ref test unequal
        """
        ref = ui.StringReference("foo","bar")
        assert ref != self.inst

    def testEqualityUnequalIdent(self):
        """ Test whether refs with different identifier test unequal
        """
        ref = ui.StringReference("ident1","myRef")
        assert ref != self.inst

    def testEqualityUnequalRef(self):
        """ Test whether refs with different references test unequal
        """
        ref = ui.StringReference("Ident","bar")
        assert ref != self.inst

"""############################ Tests StringInfo ###########################"""

class TestStringInfo:
    desc = "Test the StringInfo class:"

    def testCreationParseChinese(self):
        """ Test whether instance created correctly from chinese parse results
        """
        pstring = (
            "StringInfo\n"
            "\tID \"TestID\"\n"
            "\tValue \"侦测到更多洪魔孢子\"\n"
        )
        stringid = {"identifier":"ID","val":"TestID"}
        val = {"identifier":"Value","val":"侦测到更多洪魔孢子"}
        chInst = ui.StringInfo(stringid=stringid,val=val)
        parseRes = ui.g_stringInfo("info").parseString(
                pstring)[0]
        res = ui.StringInfo(**parseRes)
        assert res == chInst
        assert res.toString(0) == chInst.toString(0)

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = (
            "StringInfo\n"
            "\tID \"TestID\"\n"
            "\tValue \"Foo bar\"\n"
        )
        cls.stringid = {"identifier":"ID","val":"TestID"}
        cls.val = {"identifier":"Value","val":"Foo bar"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.StringInfo(
            stringid=self.stringid,
            val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance created correctly from parse results
        """
        parseRes = ui.g_stringInfo("info").parseString(
                self.parseString)[0]
        res = ui.StringInfo(**parseRes)
        assert res == self.inst

    def testOutput(self):
        """ Test whether toString returns a correct string
        """
        assert self.inst.toString(0),self.parseString

    def testEquality(self):
        """ Test whether similar objects compare as equal
        """
        obj = ui.StringInfo(stringid=self.stringid,val=self.val)
        assert obj == self.inst

    def testEqualityValue(self):
        """ Test whether objects with different values compare as equal
        """
        obj = ui.StringInfo(stringid=self.stringid,val="bar")
        assert obj != self.inst

    def testEqualityID(self):
        """ Test whether objects with different IDs compare as equal
        """
        obj = ui.StringInfo(stringid="TestID1",val=self.val)
        assert obj != self.inst

"""############################ Tests StringFile ###########################"""

class TestStringFile:
    desc = "Test for StringFile:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.filepath = "StringFile.str"
        with open(cls.filepath,encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.entries = {"counter":{"identifier":"NumStrings","val":5},
            "elements":[
                {"stringid":"id1","val":"val1"},
                {"stringid":"id1","val":"val1"},
                {"stringid":"id2","val":"val2"},
                {"stringid":"id2","val":"val4"},
                {"stringid":"id3","val":"val3"}
            ]
        }

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.StringFile(self.filepath,self.entries)

    def testIter(self):
        """ Test whether iteration over all string infos works
        """
        elemIds = [v["stringid"] for v in self.entries["elements"]]
        res = [s.stringid for s in self.inst]
        assert len(res) == len(self.entries["elements"])
        for exp in elemIds:
            assert exp in res


    def testCreateInstance(self):
        """ Test whether createInstance creates a correct instance
        """
        res = ui.StringFile.createInstance(self.filepath)
        assert res == self.inst

    def testCreationParseMalformed(self):
        """ Test whether malformed file return correct problem
        """
        res = ui.StringFile.createInstance("StringFileMalformed.str")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCheckEntries(self,mockProb):
        """ Test whether check correctly returns entries's problems
        """
        with mock.patch.object(self.inst.entries,"check",autospec=True,
                spec_set=True,return_value=[mockProb]):
            res = self.inst.check(None)
            assert res == [mockProb]

    def testRemoveDuplicates(self):
        """ Test whether duplicate removal works
        """
        dup = ui.StringInfo("id1","val1")
        res = self.inst.removeDuplicates()
        assert self.inst.entries.elements.count(dup) == 1
        assert res == [dup]

    def testRemoveDuplicatesNotChanged(self):
        """ Test whether changed is unchanged without duplicate removal
        """
        with mock.patch.object(self.inst.entries,"removeDuplicates",
                autospec=True,spec_set=True,return_value=[]):
            self.inst.removeDuplicates()
            assert not self.inst.changed

    def testRemoveDuplicatesChanged(self):
        """ Test whether changed flag is set correctly after duplicate removal
        """
        with mock.patch.object(self.inst.entries,"removeDuplicates",
                autospec=True,spec_set=True,return_value=[mock.sentinel.dup]):
            self.inst.removeDuplicates()
            assert self.inst.changed

    def testToString(self):
        """ Test whether toString produces correct string
        """
        res = self.inst.toString(0)
        assert res == self.parseString

    def testWriteOut(self):
        """ Test whether writeOut works correctly
        """
        outfile = "writeout.str"
        self.inst.writeOut(True,outfile)
        with open(outfile) as f:
            res = f.read()
        assert res == self.parseString
        os.remove("writeout.str")

    def testContains(self):
        """ Test whether item existence works with existing item
        """
        assert "id1" in self.inst

    def testContainsMissing(self):
        """ Test whether item existence works with missing item
        """
        assert "foo" not in self.inst

    def testGetitem(self):
        """ Test whether existing string is returned correctly
        """
        res = self.inst["id1"]
        assert res.stringid == "id1"
        assert res.value == "val1"

    def testGetitemMissing(self):
        """ Test whether missing string produces key error
        """
        with pytest.raises(KeyError):
            self.inst["foo"]

"""############################ Tests ModString ############################"""

origStrFile = ui.StringFile

def genMockStrFile(*args):
    m = mock.create_autospec(origStrFile,spec_set=True)
    m.__contains__.return_value = True
    return m

class TestModString:
    desc = "Test ModString:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.mmod = tco.genMockMod(os.path.abspath("./modstring/normal"),None)
        cls.strdir = os.path.join(os.path.abspath("./modstring/normal"),
                "String")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.ModStrings(self.mmod)

    @pytest.fixture()
    def creationFixture(self):
        self.mockCreate = mock.create_autospec(ui.StringFile.createInstance,
                autospec=True,side_effect=genMockStrFile)
        patcher = mock.patch("pysoase.mod.ui.StringFile.createInstance",
                new=self.mockCreate)
        patcher.start()
        yield
        patcher.stop()

    def testLanguages(self):
        """ Test whether language files are created correctly
        """
        assert len(self.inst.compFilesMod) == 2
        assert "English.str" in self.inst.compFilesMod
        assert "German.str" in self.inst.compFilesMod
        assert self.inst.compFilesMod["English.str"] is None
        assert self.inst.compFilesMod["German.str"] is None

    def testCheckStringfileLazyLoad(self,creationFixture):
        """ Test whether string file is loaded when requested by checkStringfile
        """
        self.inst.checkStringfile("German.str")
        assert isinstance(self.inst.compFilesMod["German.str"],origStrFile)
        self.mockCreate.assert_called_once_with(os.path.join(
                self.strdir,"German.str"))

    def testCheckStringNoLanguage(self,creationFixture):
        """ Test whether checkString without language checks all languages
        """
        self.inst.checkString("Foo")
        self.inst.compFilesMod[
            "German.str"].__contains__.assert_called_once_with(
                "Foo")
        self.inst.compFilesMod[
            "English.str"].__contains__.assert_called_once_with(
                "Foo")

    @pytest.fixture()
    def checkFixture(self):
        self.inst.compFilesMod["German.str"] = genMockStrFile()
        self.inst.compFilesMod["English.str"] = genMockStrFile()
        yield

    def testCheckString(self,checkFixture):
        """ Test whether checkString with language checks correct language
        """
        res = self.inst.checkString("Foo","English")
        assert res == []
        self.inst.compFilesMod[
            "English.str"].__contains__.assert_called_once_with(
                "Foo")
        assert not self.inst.compFilesMod["German.str"].__contains__.called

    def testCheckStringLoadProb(self,creationFixture):
        """ Test whether checkString returns file loading problems
        """
        self.mockCreate.side_effect = None
        self.mockCreate.return_value = [mock.sentinel.prob]
        res = self.inst.checkString("Foo","English")
        assert res == [mock.sentinel.prob]

    def testCheckStringMissingLanguage(self):
        """ Test whether checkString with missing language raises error
        """
        with pytest.raises(RuntimeError):
            self.inst.checkString("Foo","Bar")

    def testCheckStringProb(self,checkFixture):
        """ Test whether checkString returns correct problem
        """
        prob = co_probs.MissingRefProblem("German.str String","Foo")
        self.inst.compFilesMod["German.str"].__contains__.return_value = False
        res = self.inst.checkString("Foo","German")
        assert res == [prob]

    def testCheckStringfile(self,checkFixture):
        """ Test whether checkStringfile checks correct language
        """
        self.inst.checkStringfile("English.str")
        self.inst.compFilesMod["English.str"].check.assert_called_once_with(
                self.mmod)
        assert not self.inst.compFilesMod["German.str"].check.called

    def testCheckStringfileLoadProb(self,creationFixture):
        """ Test whether checkStringfile returns load problems
        """
        self.mockCreate.side_effect = [[mock.sentinel.prob],genMockStrFile()]
        res = self.inst.checkStringfile("English.str")
        assert res == [mock.sentinel.prob]

    def testCheckStringfilePreloadLoadProb(self,creationFixture):
        """ Test whether checkStringfile returns preload load problems
        """
        self.mockCreate.side_effect = [[mock.sentinel.prob],genMockStrFile()]
        self.inst.loadStringfile("English.str")
        res = self.inst.checkStringfile("English.str")
        assert res == [mock.sentinel.prob]

    def testCheckStringfileMissing(self):
        """ Test whether checkStringfile returns load problems
        """
        res = self.inst.checkStringfile("foo.str")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

    def testCheck(self,mockTqdm,checkFixture):
        """ Test whether check checks all languages
        """
        self.inst.check()
        self.inst.compFilesMod["English.str"].check.assert_called_once_with(
                self.mmod)
        self.inst.compFilesMod["German.str"].check.assert_called_once_with(
                self.mmod)

    def testCheckProbs(self,mockTqdm,checkFixture):
        """ Test whether check returns correct problems
        """
        prob = co_probs.BasicProblem(severity=co_probs.ProblemSeverity.warn,
                desc="Prob1")
        self.inst.compFilesMod["German.str"].check.return_value = [prob]
        res = self.inst.check()
        assert res == [prob]

    @pytest.fixture()
    def parseFixture(self):
        self.mockParse = mock.create_autospec(ui.StringFile.parse,
                autospec=True,return_value=(True,[]))
        patcher = mock.patch("pysoase.mod.ui.StringFile.parse",
                new=self.mockParse)
        patcher.start()
        yield
        patcher.stop()

    def testCheckSyntax(self,parseFixture):
        """ Test whether checkSyntax checks all languages
        """
        self.inst.checkSyntax()
        assert self.mockParse.call_count == 2
        self.mockParse.assert_any_call(
                os.path.join(self.strdir,"English.str"))
        self.mockParse.assert_any_call(
                os.path.join(self.strdir,"German.str"))

    def testCheckSyntaxFail(self,parseFixture):
        """ Test whether checkSyntax checks all languages
        """
        self.mockParse.return_value = (False,[mock.sentinel.prob])
        res = self.inst.checkSyntax()
        assert res == [mock.sentinel.prob,mock.sentinel.prob]

    def testLoadAll(self,mockTqdm,creationFixture):
        """ Test whether loadAll loads all languages
        """
        self.inst.loadAll()
        assert self.mockCreate.call_count == 2
        self.mockCreate.assert_any_call(
                os.path.join(self.strdir,"English.str"))
        self.mockCreate.assert_any_call(
                os.path.join(self.strdir,"German.str"))

    def testCheckFile(self,checkFixture):
        """ Test whether checkFile returns correct problems
        """
        self.inst.compFilesMod["English.str"].check.return_value = [
            mock.sentinel.prob]
        res = self.inst.checkFile("English.str")
        assert res == [mock.sentinel.prob]

    def testGetLanguageFile(self,creationFixture):
        """ Test whether language string file is returned correctly
        """
        res = self.inst.getLanguageFile("English")
        assert res == self.inst.compFilesMod["English.str"]

    def testGetLanguageFileMissing(self):
        """ Test whether getLanguageFile returns problem for missing language
        """
        res = self.inst.getLanguageFile("Chinese")
        assert isinstance(res,list)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

    def testGetLanguageFileLoadProb(self,creationFixture):
        """ Test whether getLanguageFile returns loading problems
        """
        self.mockCreate.side_effect = None
        self.mockCreate.return_value = [mock.sentinel.prob]
        res = self.inst.getLanguageFile("English")
        print(res)
        assert res == [mock.sentinel.prob]

"""########################## Tests UITextureRef ###########################"""

class TestUITextureRef:
    desc = "Test UITextureRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def setUpClass(cls):
        cls.mmod = tco.genMockMod("./uiref_mod","./uiref_reb")

    def testCheck(self):
        """ Test whether check finds existing file
        """
        inst = ui.UITextureRef("ident","exists.tga")
        res = inst.check(self.mmod)
        assert res == []

    def testCheckReb(self):
        """ Test whether check finds file in reb dir
        """
        inst = ui.UITextureRef("ident","reb.tga")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckNoExtTga(self):
        """ Test whether check finds existing tga file without extension
        """
        inst = ui.UITextureRef("ident","exists")
        res = inst.check(self.mmod)
        assert res == []

    def testCheckNoExtTgaReb(self):
        """ Test whether check finds tga file without extension in reb
        """
        inst = ui.UITextureRef("ident","reb")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckNoExtDDS(self):
        """ Test whether check finds existing dds file without extension
        """
        inst = ui.UITextureRef("ident","exdds")
        assert inst.check(self.mmod) == []

    def testCheckNoExtDDSReb(self):
        """ Test whether check finds dds file without extension in reb
        """
        inst = ui.UITextureRef("ident","rebdds")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)

    def testCheckMissing(self):
        """ Test whether check returns correct problem for missing file
        """
        inst = ui.UITextureRef("ident","missing")
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

"""############################ Tests Margins ##############################"""

@pytest.fixture(scope="module")
def marginsFixture():
    parseString = (
        "stretchMargins\n"
        +"\tleft 1\n"
        +"\tright 2\n"
        +"\ttop 3\n"
        +"\tbottom 4\n"
    )
    parseDict = {
        "identifier":"stretchMargins",
        "left":{"identifier":"left","val":1},
        "right":{"identifier":"right","val":2},
        "top":{"identifier":"top","val":3},
        "bottom":{"identifier":"bottom","val":4}
    }
    return parseString,parseDict

class TestMargins:
    desc = "Test Margins:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,marginsFixture):
        cls.parseString,parseDict = marginsFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.Margins(
                identifier=self.identifier,
                left=self.left,
                right=self.right,
                top=self.top,
                bottom=self.bottom
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = (
            co_parse.genHeading("stretchMargins")+ui.g_margin).parseString(
                self.parseString)
        inst1 = ui.Margins(**parseRes)
        assert inst1 == self.inst

    def testCreationAttribLeft(self):
        """ Test whether the left attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.left)
        assert self.inst.left == exp

    def testCreationAttribRight(self):
        """ Test whether the right attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.right)
        assert self.inst.right == exp

    def testCreationAttribTop(self):
        """ Test whether the top attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.top)
        assert self.inst.top == exp

    def testCreationAttribBottom(self):
        """ Test whether the bottom attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.bottom)
        assert self.inst.bottom == exp

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        assert self.inst.check(self.mmod) == []

    def testToString(self):
        """ Test whether string representation is constructed correclty
        """
        assert self.inst.toString(0) == self.parseString

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst1 = ui.Margins(
                identifier=self.identifier,
                left=self.left,
                right=self.right,
                top=self.top,
                bottom=self.bottom
        )
        assert self.inst == inst1

    def testEqualityDiffLeft(self):
        """ Test whether instances with differing left test unequal
        """
        other = {"identifier":"left","val":10}
        inst1 = ui.Margins(
                identifier=self.identifier,
                left=other,
                right=self.right,
                top=self.top,
                bottom=self.bottom
        )
        assert self.inst != inst1

    def testEqualityDiffRight(self):
        """ Test whether instances with differing right test unequal
        """
        other = {"identifier":"right","val":20}
        inst1 = ui.Margins(
                identifier=self.identifier,
                left=self.left,
                right=other,
                top=self.top,
                bottom=self.bottom
        )
        assert self.inst != inst1

    def testEqualityDiffTop(self):
        """ Test whether instances with differing top test unequal
        """
        other = {"identifier":"top","val":30}
        inst1 = ui.Margins(
                identifier=self.identifier,
                left=self.left,
                right=self.right,
                top=other,
                bottom=self.bottom
        )
        assert self.inst != inst1

    def testEqualityDiffBottom(self):
        """ Test whether instances with differing bottom test unequal
        """
        other = {"identifier":"bottom","val":40}
        inst1 = ui.Margins(
                identifier=self.identifier,
                left=self.left,
                right=self.right,
                top=self.top,
                bottom=other
        )
        assert self.inst != inst1

"""############################# Tests Brush ###############################"""

class TestBrush:
    desc = "Tests Brush attribute:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = (
            "brush\n"
            +"\tname \"testname\"\n"
            +"\tcontent \"States\"\n"
        )
        cls.identifier = "brush"
        cls.name = {"identifier":"name","val":"testname"}
        cls.brushType = {"identifier":"content","val":"States"}
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.inst = ui.Brush(
                identifier=self.identifier,
                name=self.name,
                brushType=self.brushType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (co_parse.genHeading("brush")+co_parse.genStringAttrib("name")("name")
                  +co_parse.genEnumAttrib("content",["States"])("brushType"))
        parseRes = parser.parseString(self.parseString)
        inst = ui.Brush(**parseRes)
        assert inst == self.inst

    def testCreationAttribName(self):
        """ Test whether name attribute is constructed correctly
        """
        exp = co_attribs.AttribString(**self.name)
        assert self.inst.name == exp

    def testCreationAttribBrushType(self):
        """ Test whether brushType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["Simple","Margins","States",
            "MarginsAndStates"],**self.brushType)
        assert self.inst.brushType == exp

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        assert self.inst.check(self.mmod) == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testCreateInstanceInvalidType(self):
        """ Test createInstance with invalid content type
        """
        diff = {"identifier":"content","val":"foo"}
        with pytest.raises(RuntimeError):
            ui.Brush.factory(identifier=self.identifier,
                    name=self.name,brushType=diff)

"""########################## Tests BrushContent ###########################"""

@pytest.fixture(scope="module")
def brushContentFixture():
    parseString = (
        "fileName \"testfile\"\n"
        +"pixelBox [ 4 , 290 , 85 , 38 ]\n"
    )
    parseDict = {
        "texture":{"identifier":"fileName","val":"testfile"},
        "pixelBox":{"identifier":"pixelBox","coord":[4,290,85,38]}
    }
    return parseString,parseDict

class TestBrushContent:
    desc = "Tests BrushContent:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,brushContentFixture):
        cls.parseString,parseDict = brushContentFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushContent(
                texture=self.texture,
                pixelBox=self.pixelBox
        )

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = ui.g_brushDef.parseString(self.parseString)
        inst1 = ui.BrushContent(**parseRes)
        assert inst1 == self.inst

    def testCreationAttribTexture(self):
        """ Test whether attribute texture is created correctly
        """
        exp = ui.UITextureRef(**self.texture)
        assert self.inst.texture == exp

    def testCreationAttribPixelBox(self):
        """ Test whether attribute pixelBox is created correctly
        """
        exp = co_attribs.AttribPos4d(**self.pixelBox)
        assert self.inst.pixelBox == exp

    def testCheckNoProbs(self):
        """ Test whether check with no probs returns empty list
        """
        with mock.patch.object(self.inst.texture,"check",autospec=True,
                return_value=[]):
            assert self.inst.check(self.mmod) == []

    def testCheckWithProbs(self):
        """ Test whether check returns correct problems
        """
        with mock.patch.object(self.inst.texture,"check",autospec=True,
                return_value=[mock.sentinel.prob]):
            assert self.inst.check(self.mmod) == [mock.sentinel.prob]

    def testToStringWithoutHeader(self):
        """ Test whether string is constructed correctly without identifier
        """
        assert self.inst.toString(0) == self.parseString

    def testToStringWithHeader(self):
        """ Test whether string is constructed correctly with identifier
        """
        exp = (
            "ident\n"
            "\tfileName \"testfile\"\n"
            +"\tpixelBox [ 4 , 290 , 85 , 38 ]\n"
        )
        identifier = "ident"
        inst = ui.BrushContent(
                identifier=identifier,
                texture=self.texture,
                pixelBox=self.pixelBox
        )
        assert inst.toString(0) == exp

"""########################## Tests BrushSimple ###########################"""

@pytest.fixture(scope="module")
def brushSimpleFixture():
    parseString = (
        "brush\n"
        +"\tname \"testsimple\"\n"
        +"\tcontent \"Simple\"\n"
        +"\tfileName \"testfile\"\n"
        +"\tpixelBox [ 4 , 290 , 85 , 38 ]\n"
    )
    parseDict = {
        "identifier":"brush",
        "name":{"identifier":"name","val":"testsimple"},
        "brushType":{"identifier":"content","val":"Simple"},
        "brushContent":{
            "texture":{"identifier":"fileName","val":"testfile"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,290,85,38]}
        }
    }
    return parseString,parseDict

class TestBrushSimple:
    desc = "Tests BrushSimple attribute:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,brushSimpleFixture):
        cls.parseString,parseDict = brushSimpleFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushSimple(
                identifier=self.identifier,
                name=self.name,
                brushType=self.brushType,
                brushContent=self.brushContent
        )

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.BrushSimple(**parseRes)
        assert self.inst == inst1

    def testCreationCreateInstance(self):
        """ Test whether creation from Brush.createInstance works correctly
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.Brush.factory(**parseRes)
        assert self.inst == inst1

    def testCreationAttribBrushContent(self):
        """ Test whether brushContent attribute is created correctly
        """
        exp = ui.BrushContent(**self.brushContent)
        assert self.inst.brushContent == exp

    def testCheck(self):
        """ Test whether check returns correct problems
        """
        with mock.patch.object(self.inst.brushContent,"check",autospec=True,
                return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests BrushMargins ###########################"""

@pytest.fixture(scope="module")
def brushMarginsFixture():
    parseString = (
        "brush\n"
        +"\tname \"testmargins\"\n"
        +"\tcontent \"Margins\"\n"
        +"\tstretchMargins\n"
        +"\t\tleft 18\n"
        +"\t\tright 12\n"
        +"\t\ttop 1\n"
        +"\t\tbottom 2\n"
        +"\tcontentMargins\n"
        +"\t\tleft 90\n"
        +"\t\tright 70\n"
        +"\t\ttop 100\n"
        +"\t\tbottom 24\n"
        +"\tfileName \"testfile\"\n"
        +"\tpixelBox [ 4 , 290 , 85 , 38 ]\n"
    )
    parseDict = {
        "identifier":"brush",
        "name":{"identifier":"name","val":"testmargins"},
        "brushType":{"identifier":"content","val":"Margins"},
        "brushContent":{
            "texture":{"identifier":"fileName","val":"testfile"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,290,85,38]}
        },
        "stretch":{
            "identifier":"stretchMargins",
            "left":{"identifier":"left","val":18},
            "right":{"identifier":"right","val":12},
            "top":{"identifier":"top","val":1},
            "bottom":{"identifier":"bottom","val":2}
        },
        "content":{
            "identifier":"contentMargins",
            "left":{"identifier":"left","val":90},
            "right":{"identifier":"right","val":70},
            "top":{"identifier":"top","val":100},
            "bottom":{"identifier":"bottom","val":24}
        }
    }
    return parseString,parseDict

class TestBrushMargins:
    desc = "Tests BrushMargins attribute:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,brushMarginsFixture):
        cls.parseString,parseDict = brushMarginsFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushMargins(
                identifier=self.identifier,
                name=self.name,
                brushType=self.brushType,
                brushContent=self.brushContent,
                stretch=self.stretch,
                content=self.content
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.BrushMargins(**parseRes)
        assert inst1 == self.inst

    def testCreationCreateInstance(self):
        """ Test whether creation from Brush.createInstance works correctly
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.Brush.factory(**parseRes)
        assert inst1 == self.inst

    def testCreationAttribStretch(self):
        """ Test whether attribute stretch is created correctly
        """
        exp = ui.Margins(**self.stretch)
        assert self.inst.stretch == exp

    def testCreationAttribContent(self):
        """ Test whether attribute content is created correctly
        """
        exp = ui.Margins(**self.content)
        assert self.inst.content == exp

    def testCheck(self):
        """ Test whether check returns the correct problems
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.brushContent,"check",autospec=True,
                return_value=probs):
            res = self.inst.check(self.mmod)
            assert res == probs

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests BrushStates ############################"""

@pytest.fixture(scope="module")
def brushStatesFixture():
    parseString = (
        "brush\n"
        +"\tname \"teststates\"\n"
        +"\tcontent \"States\"\n"
        +"\tDisabled\n"
        +"\t\tfileName \"testfileDis\"\n"
        +"\t\tpixelBox [ 4 , 290 , 85 , 38 ]\n"
        +"\tPressed\n"
        +"\t\tfileName \"testfilePre\"\n"
        +"\t\tpixelBox [ 4 , 390 , 85 , 38 ]\n"
        +"\tCursorOver\n"
        +"\t\tfileName \"testfileCur\"\n"
        +"\t\tpixelBox [ 4 , 390 , 95 , 38 ]\n"
        +"\tFocused\n"
        +"\t\tfileName \"testfileFoc\"\n"
        +"\t\tpixelBox [ 4 , 390 , 95 , 78 ]\n"
        +"\tNormal\n"
        +"\t\tfileName \"testfileNormal\"\n"
        +"\t\tpixelBox [ 40 , 390 , 95 , 78 ]\n"
    )
    parseDict = {
        "identifier":"brush",
        "name":{"identifier":"name","val":"teststates"},
        "brushType":{"identifier":"content","val":"States"},
        "disabled":{
            "identifier":"Disabled",
            "texture":{"identifier":"fileName","val":"testfileDis"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,290,85,38]}
        },
        "pressed":{
            "identifier":"Pressed",
            "texture":{"identifier":"fileName","val":"testfilePre"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,390,85,38]}
        },
        "cursorOver":{
            "identifier":"CursorOver",
            "texture":{"identifier":"fileName","val":"testfileCur"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,390,95,38]}
        },
        "focused":{
            "identifier":"Focused",
            "texture":{"identifier":"fileName","val":"testfileFoc"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,390,95,78]}
        },
        "normal":{
            "identifier":"Normal",
            "texture":{"identifier":"fileName","val":"testfileNormal"},
            "pixelBox":{"identifier":"pixelBox","coord":[40,390,95,78]}
        }
    }
    return parseString,parseDict

class TestBrushStates:
    desc = "Tests BrushStates attribute:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,brushStatesFixture):
        cls.parseString,parseDict = brushStatesFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushStates(
                identifier=self.identifier,
                name=self.name,
                brushType=self.brushType,
                disabled=self.disabled,
                pressed=self.pressed,
                cursorOver=self.cursorOver,
                focused=self.focused,
                normal=self.normal
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.BrushStates(**parseRes)
        assert self.inst == inst1

    def testCreationCreateInstance(self):
        """ Test whether instance is well created from Brush.createInstance
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.Brush.factory(**parseRes)
        assert self.inst == inst1

    def testCreationAttribDisabled(self):
        """ Test whether attribute disabled is created correctly
        """
        exp = ui.BrushContent(**self.disabled)
        assert self.inst.disabled == exp

    def testCreationAttribPressed(self):
        """ Test whether attribute pressed is created correctly
        """
        exp = ui.BrushContent(**self.pressed)
        assert self.inst.pressed == exp

    def testCreationAttribCursorOver(self):
        """ Test whether attribute cursorOver is created correctly
        """
        exp = ui.BrushContent(**self.cursorOver)
        assert self.inst.cursorOver == exp

    def testCreationAttribFocused(self):
        """ Test whether attribute focused is created correctly
        """
        exp = ui.BrushContent(**self.focused)
        assert self.inst.focused == exp

    def testCreationAttribNormal(self):
        """ Test whether attribute normal is created correctly
        """
        exp = ui.BrushContent(**self.normal)
        assert self.inst.normal == exp

    def testCheck(self):
        """ Test whether check returns the correct problems
        """
        with mock.patch.object(self.inst.disabled,"check",return_value=[]), \
                mock.patch.object(self.inst.pressed,"check",return_value=[]), \
                mock.patch.object(self.inst.cursorOver,"check",
                        return_value=[mock.sentinel.prob]), \
                mock.patch.object(self.inst.focused,"check",return_value=[]), \
                mock.patch.object(self.inst.normal,"check",return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether the string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""####################### Tests BrushMarginsStates ########################"""

@pytest.fixture(scope="module")
def brushMarginsStatesFixture():
    parseString = (
        "brush\n"
        +"\tname \"testmarginsstates\"\n"
        +"\tcontent \"MarginsAndStates\"\n"
        +"\tstretchMargins\n"
        +"\t\tleft 18\n"
        +"\t\tright 12\n"
        +"\t\ttop 1\n"
        +"\t\tbottom 2\n"
        +"\tcontentMargins\n"
        +"\t\tleft 90\n"
        +"\t\tright 70\n"
        +"\t\ttop 100\n"
        +"\t\tbottom 24\n"
        +"\tDisabled\n"
        +"\t\tfileName \"testfileDis\"\n"
        +"\t\tpixelBox [ 4 , 290 , 85 , 38 ]\n"
        +"\tPressed\n"
        +"\t\tfileName \"testfilePre\"\n"
        +"\t\tpixelBox [ 4 , 390 , 85 , 38 ]\n"
        +"\tCursorOver\n"
        +"\t\tfileName \"testfileCur\"\n"
        +"\t\tpixelBox [ 4 , 390 , 95 , 38 ]\n"
        +"\tFocused\n"
        +"\t\tfileName \"testfileFoc\"\n"
        +"\t\tpixelBox [ 4 , 390 , 95 , 78 ]\n"
        +"\tNormal\n"
        +"\t\tfileName \"testfileNormal\"\n"
        +"\t\tpixelBox [ 40 , 390 , 95 , 78 ]\n"
    )
    parseDict = {
        "identifier":"brush",
        "name":{"identifier":"name","val":"testmarginsstates"},
        "brushType":{"identifier":"content","val":"MarginsAndStates"},
        "stretch":{
            "identifier":"stretchMargins",
            "left":{"identifier":"left","val":18},
            "right":{"identifier":"right","val":12},
            "top":{"identifier":"top","val":1},
            "bottom":{"identifier":"bottom","val":2}
        },
        "content":{
            "identifier":"contentMargins",
            "left":{"identifier":"left","val":90},
            "right":{"identifier":"right","val":70},
            "top":{"identifier":"top","val":100},
            "bottom":{"identifier":"bottom","val":24}
        },
        "disabled":{
            "identifier":"Disabled",
            "texture":{"identifier":"fileName","val":"testfileDis"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,290,85,38]}
        },
        "pressed":{
            "identifier":"Pressed",
            "texture":{"identifier":"fileName","val":"testfilePre"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,390,85,38]}
        },
        "cursorOver":{
            "identifier":"CursorOver",
            "texture":{"identifier":"fileName","val":"testfileCur"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,390,95,38]}
        },
        "focused":{
            "identifier":"Focused",
            "texture":{"identifier":"fileName","val":"testfileFoc"},
            "pixelBox":{"identifier":"pixelBox","coord":[4,390,95,78]}
        },
        "normal":{
            "identifier":"Normal",
            "texture":{"identifier":"fileName","val":"testfileNormal"},
            "pixelBox":{"identifier":"pixelBox","coord":[40,390,95,78]}
        }
    }
    return parseString,parseDict

class TestBrushMarginsStates:
    desc = "Tests BrushMarginsStates attribute:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,brushMarginsStatesFixture):
        cls.parseString,parseDict = brushMarginsStatesFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushMarginsStates(
                identifier=self.identifier,
                name=self.name,
                brushType=self.brushType,
                stretch=self.stretch,
                disabled=self.disabled,
                pressed=self.pressed,
                cursorOver=self.cursorOver,
                focused=self.focused,
                normal=self.normal,
                content=self.content
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.BrushMarginsStates(**parseRes)
        assert inst1 == self.inst

    def testCreationCreateInstance(self):
        """ Test whether creation from Brush.createInstance works correctly
        """
        parseRes = ui.g_brush.parseString(self.parseString)[0]
        inst1 = ui.Brush.factory(**parseRes)
        assert inst1 == self.inst

    def testCreationAttribStretch(self):
        """ Test whether attribute stretch is created correctly
        """
        exp = ui.Margins(**self.stretch)
        assert self.inst.stretch == exp

    def testCreationAttribContent(self):
        """ Test whether attribute content is created correctly
        """
        exp = ui.Margins(**self.content)
        assert self.inst.content == exp

    def testCheck(self):
        """ Test whether check returns the correct problems
        """
        with mock.patch.object(self.inst.disabled,"check",return_value=[]), \
                mock.patch.object(self.inst.pressed,"check",return_value=[]), \
                mock.patch.object(self.inst.cursorOver,"check",
                        return_value=[mock.sentinel.prob]), \
                mock.patch.object(self.inst.focused,"check",return_value=[]), \
                mock.patch.object(self.inst.normal,"check",return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################### Tests BrushFile #############################"""

class TestBrushFile:
    desc = "Tests BrushFile class:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,brushSimpleFixture,brushMarginsFixture,
            brushStatesFixture,brushMarginsStatesFixture):
        cls.filepath = "test.brushes"
        _,brushSimpleDict = brushSimpleFixture
        _,brushMarginsDict = brushMarginsFixture
        _,brushStatesDict = brushStatesFixture
        _,brushMarginsStatesDict = brushMarginsStatesFixture
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.loadOnDemand = {"identifier":"onlyLoadOnDemand","val":False}
        cls.brushes = {
            "counter":{"identifier":"brushCount","val":4},
            "elements":[brushSimpleDict,brushMarginsStatesDict,
                brushMarginsDict,brushStatesDict]
        }

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushFile(
                filepath=self.filepath,
                loadOnDemand=self.loadOnDemand,
                brushes=self.brushes
        )
        self.mmod = tco.genMockMod("./")

    def testCreationCreateInstance(self):
        """ Test whether creation from createInstance works correctly
        """
        inst1 = ui.BrushFile.createInstance(self.filepath)
        assert inst1 == self.inst

    def testCreationCreateInstanceMalformed(self):
        """ Test whether createInstance returns problems for malformed files
        """
        res = ui.BrushFile.createInstance("malformed.brushes")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCreationAttribOnDemand(self):
        """ Test whether loadOnDemand attribute is constructed correctly
        """
        exp = co_attribs.AttribBool(**self.loadOnDemand)
        assert self.inst.loadOnDemand == exp

    def testCreationAttribBrushes(self):
        """ Test whether brushes attribute is constructed correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=ui.Brush,
                factory=True,
                keyfunc=lambda val:val.name.value,
                **self.brushes
        )
        assert self.inst.brushes == exp

    def testCheckBrushEx(self):
        """ Test whether checkBrushExistence recognizes existing brush entries
        """
        ex = ["testsimple","testmarginsstates","testmargins","teststates"]
        assert all(self.inst.checkBrushExistence(n) for n in ex)

    def testCheckBrushExMissing(self):
        """ Test whether checkBrushExistence recognizes missing brush entries
        """
        ex = ["foo","bar","baz"]
        assert not any(self.inst.checkBrushExistence(n) for n in ex)

    def testCheck(self,mockProb):
        """ Test whether check returns correct problems
        """
        probs = [mockProb]
        with mock.patch.object(self.inst.brushes,"check",autospec=True,
                spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            assert res == probs
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests ModBrushes ###########################"""

class TestModBrushesCreation:
    desc = "Tests ModBrushes creation:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.moddir = os.path.abspath("./modbrushes")
        cls.brushfiles = ["test1.brushes","test2.brushes"]
        cls.mmod = tco.genMockMod(cls.moddir,None)
        cls.windir = os.path.join(cls.moddir,"Window")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.ModBrushes(self.mmod)

    def testCreationBrushes(self):
        """ Test whether initial discovery of brush files works correctly
        """
        assert len(self.inst.compFilesMod) == 2
        assert all(f in self.inst.compFilesMod for f in self.brushfiles)

    def testCreationBrushesWrongFile(self):
        """ Test whether non brushfiles are ignored in brush discovery
        """
        assert all(f not in self.inst.compFilesMod
            for f in ["wrong.foo","wrong.bar"]
        )

    @pytest.fixture()
    def manMock(self):
        return mock.create_autospec(manifest.ManifestBrush,spec_set=True)

    def testCreationManifest(self,manMock):
        """ Test whether manifest is created correctly
        """
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = self.inst.manifest
            assert res == manMock
            mocked.assert_called_once_with("","brush.manifest",
                    manifest.ManifestBrush)

    def testCreationManifestLazy(self,manMock):
        """ Test whether manifest lazy loadind works
        """
        with mock.patch.object(self.inst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = self.inst.manifest
            assert res == manMock
            mocked.assert_called_once_with("","brush.manifest",
                    manifest.ManifestBrush)
            res1 = self.inst.manifest
            assert mocked.call_count == 1
            assert res == res1

    def testCreationManifestMissing(self,manMock):
        """ Test whether missing manifest is loaded from bundled files
        """
        currMod = tco.genMockMod(os.path.join(self.moddir,"empty"))
        currInst = ui.ModBrushes(currMod)
        with mock.patch.object(currInst,"loadFile",spec_set=True,autospec=True,
                return_value=(manMock,[])) as mocked:
            res = currInst.manifest
            assert res == manMock
            mocked.assert_called_once_with("","brush.manifest",
                    manifest.ManifestBrush)

class TestModBrushesLoading:
    desc = "Test ModBrushes loading:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.moddir = os.path.abspath("./modbrushes")
        cls.rebrefdir = os.path.abspath("./modbrushes_rebref")
        cls.brushfiles = ["test1.brushes","test2.brushes"]
        cls.windir = os.path.join(cls.moddir,"Window")
        cls.mmod = tco.genMockMod(cls.moddir,rebrefpath=cls.rebrefdir)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.ModBrushes(self.mmod)
        self.mockCreate = mock.create_autospec(ui.BrushFile.createInstance,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.ui.BrushFile.createInstance",
                new=self.mockCreate)
        patcher.start()
        yield
        patcher.stop()

    @pytest.fixture()
    def mockBrush(self):
        return mock.create_autospec(ui.BrushFile,spec_set=True)

    def testLoadBrushfile(self,mockBrush):
        """ Test whether loadBrushfile loads file correctly
        """
        self.mockCreate.return_value = mockBrush
        res = self.inst.loadBrushfile("test1.brushes")
        assert res == []
        assert self.inst.compFilesMod["test1.brushes"] == mockBrush
        self.mockCreate.assert_called_once_with(os.path.join(self.windir,
                "test1.brushes"))

    def testLoadBrushfileLazy(self,mockBrush):
        """ Test whether loadBrushfile loads file lazily
        """
        self.mockCreate.return_value = mockBrush
        self.inst.loadBrushfile("test1.brushes")
        res1 = self.inst.loadBrushfile("test1.brushes")
        assert res1 == []
        assert self.inst.compFilesMod["test1.brushes"] == mockBrush
        assert self.mockCreate.call_count == 1

    def testLoadBrushfileMalformed(self):
        """ Test whether loadBrushfile returns problems with malformed file
        """
        probs = [mock.sentinel.prob]
        self.mockCreate.return_value = probs
        res = self.inst.loadBrushfile("test1.brushes")
        assert res == probs
        assert self.inst.compFilesMod["test1.brushes"] == probs
        self.mockCreate.assert_called_once_with(os.path.join(self.windir,
                "test1.brushes"))

    def testLoadBrushfileMissing(self):
        """ Test whether loadBrushfile returns problems with missing file
        """
        res = self.inst.loadBrushfile("missing.brushes")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)

    def testLoadBrushfileReb(self,mockBrush):
        """ Test whether loadBrushfile loads rebref file correctly
        """
        self.mockCreate.return_value = mockBrush
        res = self.inst.loadBrushfile("reb.brushes")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.UsingRebFileProblem)
        assert self.inst.compFilesMod["reb.brushes"] == mockBrush
        self.mockCreate.assert_called_once_with(os.path.join(self.rebrefdir,
                "Window","reb.brushes"))

    def testLoadAll(self,mockBrush,mockTqdm):
        """ Test whether loadAll loads all brush files
        """
        calls = [mock.call(os.path.join(self.moddir,"Window","test1.brushes")),
            mock.call(os.path.join(self.moddir,"Window","test2.brushes")),
            mock.call(os.path.join(self.rebrefdir,"Window","reb.brushes"))]
        mockMan = mock.MagicMock()
        mockMan.entries = mock.create_autospec(
                co_attribs.AttribListKeyed,
                spec_set=True)
        mockMan.entries.elements = [mock.MagicMock(
                co_attribs.FileRef,
                ref="reb.brushes")]
        mockMan.mock_add_spec(manifest.ManifestBrush,
                spec_set=True)
        with mock.patch.object(self.inst,"_manifest",mockMan):
            self.mockCreate.return_value = mockBrush
            res = self.inst.loadAll()
            assert len(res) == 1
            assert isinstance(res[0],co_probs.UsingRebFileProblem)
            assert self.mockCreate.call_count == 3
            self.mockCreate.assert_has_calls(calls,any_order=True)

class TestModBrushesChecking:
    desc = "Test ModBrushes checking:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        moddir = os.path.abspath("./modbrushes")
        cls.mmod = tco.genMockMod(moddir,None)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.ModBrushes(self.mmod)

    @pytest.fixture()
    def mockCreate(self):
        self.mock1 = mock.create_autospec(ui.BrushFile,spec_set=True)
        self.mock2 = mock.create_autospec(ui.BrushFile,spec_set=True)
        self.mockCreate = mock.create_autospec(ui.BrushFile.createInstance,
                spec_set=True,side_effect=[self.mock1,self.mock2])
        patcher = mock.patch("pysoase.mod.ui.BrushFile.createInstance",
                new=self.mockCreate)
        patcher.start()
        yield
        patcher.stop()

    @pytest.fixture()
    def mockManifest(self):
        self.mockMan = mock.create_autospec(manifest.ManifestBrush,
                spec_set=True)
        self.mockMan.checkEntry.return_value = []
        patcher = mock.patch.object(self.inst,"_manifest",self.mockMan)
        patcher.start()
        yield
        patcher.stop()

    def testCheckBrushfile(self,mockCreate,mockManifest):
        """ Test whether checkBrushfile correctly checks the given file
        """
        self.mock1.check.return_value = []
        res = self.inst.checkBrushfile("test1.brushes")
        assert res == []
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckBrushfileLoadingProblem(self,mockManifest,mockCreate):
        """ Test whether checkBrushfile correctly returns loading problems
        """
        self.mockCreate.side_effect = [[mock.sentinel.prob]]
        res = self.inst.checkBrushfile("test1.brushes")
        assert res == [mock.sentinel.prob]
        assert not self.mock1.check.called

    def testCheckBrushfilePreloadLoadingProblem(self,mockManifest,mockCreate):
        """ Test whether checkBrushfile correctly returns preload problems
        """
        self.mockCreate.side_effect = [[mock.sentinel.prob]]
        self.inst.loadBrushfile("test1.brushes")
        res = self.inst.checkBrushfile("test1.brushes")
        assert res == [mock.sentinel.prob]
        assert not self.mock1.check.called

    def testCheckBrushfileProblem(self,mockManifest,mockCreate):
        """ Test whether checkBrushfile correctly returns problems
        """
        self.mock1.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkBrushfile("test1.brushes")
        assert res == [mock.sentinel.prob]
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckBrushfileManifestProblem(self,mockManifest,mockCreate):
        """ Test whether checkBrushfile correctly returns manifest problems
        """
        self.mockMan.checkEntry.return_value = [mock.sentinel.prob]
        self.mock1.check.return_value = []
        res = self.inst.checkBrushfile("test1.brushes")
        assert res == [mock.sentinel.prob]
        self.mockMan.checkEntry.assert_called_once_with("test1.brushes")

    def testCheckManifest(self,mockManifest):
        """ Test whether checkManifest returns empty list for existing entry
        """
        res = self.inst.checkManifest("test1.brushes")
        assert res == []
        self.mockMan.checkEntry.assert_called_once_with("test1.brushes")

    def testCheckManifestMissing(self,mockManifest):
        """ Test whether checkManifest returns correct problems missing file
        """
        self.mockMan.checkEntry.return_value = [mock.sentinel.prob]
        res = self.inst.checkManifest("test1.brushes")
        assert res == [mock.sentinel.prob]
        self.mockMan.checkEntry.assert_called_once_with("test1.brushes")

    def testCheckManifestProblem(self):
        """ Test whether checkManifest returns correct problems malformed file
        """
        self.inst._manifest = [mock.sentinel.prob]
        res = self.inst.checkManifest("test1.brushes")
        assert res == [mock.sentinel.prob]

    @pytest.fixture()
    def mockCheckBrush(self,mockCreate):
        self.mockMan = mock.MagicMock()
        self.mockMan.entries = mock.create_autospec(
                co_attribs.AttribListKeyed,
                spec_set=True)
        self.mockMan.entries.elements = []
        self.mockMan.mock_add_spec(manifest.ManifestBrush,
                spec_set=True)
        self.mockMan.checkEntry.return_value = []
        patcher = mock.patch.object(self.inst,"_manifest",self.mockMan)
        patcher.start()
        yield
        patcher.stop()

    def testCheckBrush(self,mockCheckBrush,mockTqdm):
        """ Test whether checkBrush returns empty list of problems
        """
        self.mock1.checkBrushExistence.return_value = True
        self.mock2.checkBrushExistence.return_value = True
        res = self.inst.checkBrush("testbrush")
        assert res == []
        self.mock1.checkBrushExistence.assert_called_once_with("testbrush")

    def testCheckBrushMalformedFile(self,mockCheckBrush,mockProb,mockTqdm):
        """ Test whether checkBrush returns malformed file problems
        """
        self.mock1.checkBrushExistence.return_value = True
        self.mockCreate.side_effect = [self.mock1,[mockProb]]
        res = self.inst.checkBrush("testbrush")
        assert res == [mockProb]
        self.mock1.checkBrushExistence.assert_called_once_with("testbrush")

    def testCheckBrushMissingManifest(self,mockCheckBrush,mockTqdm):
        """ Test whether checkBrush returns missing manifest entry problem
        """
        self.mock1.checkBrushExistence.return_value = True
        self.mock2.checkBrushExistence.return_value = True
        self.mockMan.checkEntry.return_value = [mock.sentinel.prob]
        res = self.inst.checkBrush("testbrush")
        assert res == [mock.sentinel.prob]
        self.mock1.checkBrushExistence.assert_called_once_with("testbrush")

    def testCheckBrushMissing(self,mockCheckBrush,mockTqdm):
        """ Test whether checkBrush returns missing ref problem
        """
        self.mock1.checkBrushExistence.return_value = False
        self.mock2.checkBrushExistence.return_value = False
        res = self.inst.checkBrush("testbrush")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        self.mock1.checkBrushExistence.assert_called_once_with("testbrush")
        self.mock2.checkBrushExistence.assert_called_once_with("testbrush")

    def testCheck(self,mockManifest,mockCreate,mockTqdm):
        """ Test whether check checks all components
        """
        self.mockMan.check.return_value = []
        self.mock1.check.return_value = []
        self.mock2.check.return_value = []
        res = self.inst.check()
        assert res == []
        self.mock1.check.assert_called_once_with(self.mmod)
        self.mock2.check.assert_called_once_with(self.mmod)
        self.mockMan.check.assert_called_once_with(self.mmod)

    def testCheckStrict(self,mockCheckBrush,mockTqdm):
        """ Test whether strict check checks only files in manifest
        """
        self.mockMan.check.return_value = []
        self.mockMan.entries.elements = [mock.MagicMock(ref="test1.brushes")]
        self.mock1.check.return_value = [mock.sentinel.prob]
        self.mock2.check.return_value = [mock.sentinel.prob]
        res = self.inst.check(strict=True)
        assert res == [mock.sentinel.prob]

    def testCheckMalformedManifest(self,mockCreate,mockTqdm):
        """ Test whether check returns problems for malformed manifest
        """
        self.inst._manifest = [mock.sentinel.prob]
        self.mock1.check.return_value = []
        self.mock2.check.return_value = []
        res = self.inst.check()
        assert res == [mock.sentinel.prob,mock.sentinel.prob,mock.sentinel.prob]
        self.mock1.check.assert_called_once_with(self.mmod)
        self.mock2.check.assert_called_once_with(self.mmod)

    def testCheckFileManifest(self,mockManifest):
        """ Test whether checkFile works for manifest
        """
        res = self.inst.checkFile("brush.manifest")
        assert res == []
        self.mockMan.check.assert_called_once_with(self.mmod)

    def testCheckFileBrush(self,mockCreate,mockManifest):
        """ Test whether checkFile works for brush files
        """
        res = self.inst.checkFile("test1.brushes")
        assert res == []
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckFileInvalid(self):
        """ Test whether checkFile raises error for invalid file type
        """
        with pytest.raises(RuntimeError):
            self.inst.checkFile("foo.bar")

    def testGetBrushfile(self,mockCreate):
        """ Test whether getBrushfile returns the correct file
        """
        self.mock1.checkBrushExistence.return_value = False
        self.mock2.checkBrushExistence.return_value = True
        res = self.inst.getBrushfile("testbrush")
        for filename,brushmock in self.inst.compFilesMod.items():
            if brushmock.checkBrushExistence.return_value:
                assert res == filename
        self.mock1.checkBrushExistence.assert_called_once_with("testbrush")
        self.mock2.checkBrushExistence.assert_called_once_with("testbrush")

    def testGetBrushfileMissing(self,mockCreate):
        """ Test whether getBrushfile returns None for missing brush
        """
        self.mock1.checkBrushExistence.return_value = False
        self.mock2.checkBrushExistence.return_value = False
        res = self.inst.getBrushfile("testbrush")
        assert res is None
        self.mock1.checkBrushExistence.assert_called_once_with("testbrush")
        self.mock2.checkBrushExistence.assert_called_once_with("testbrush")

"""############################# Tests BrushRef ############################"""

class TestBrushRef:
    desc = "Tests BrushRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.ref = "testref"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = ui.BrushRef(
                identifier=self.identifier,
                val=self.ref
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        inst1 = ui.BrushRef(**parseRes)
        assert self.inst == inst1

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testCheck(self):
        """ Test whether check checks brush correctly
        """
        self.mmod.brushes.checkBrush.return_value = []
        res = self.inst.check(self.mmod)
        assert res == []
        self.mmod.brushes.checkBrush.assert_called_once_with("testref")

    def testCheckProblem(self,mockProb):
        """ Test whether check returns problems correctly
        """
        self.mmod.brushes.checkBrush.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]
        self.mmod.brushes.checkBrush.assert_called_once_with("testref")

    def testGetBrushfile(self):
        """ Test whether getBrushfile returns correct file
        """
        self.mmod.brushes.getBrushfile.return_value = "file.brushes"
        res = self.inst.getBrushfile(self.mmod)
        assert res == "file.brushes"
        self.mmod.brushes.getBrushfile.assert_called_once_with("testref")
