""" Tests for the pysoase.mod.scenario module.
"""

import os
import unittest as unit
import unittest.mock as mock

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.filehandling as co_files
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.manifest as manifest
import pysoase.mod.scenarios as scen
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","scenario")

"""########################## Tests ScenarioTemplate #######################"""

class ScenarioTemplateTests(unit.TestCase):
    desc = "Test ScenarioTemplate:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetItemsTemplate\n"
            +"\ttemplateName \"Template:Test\"\n"
            +"\tsubTemplates 2\n"
            +"\ttemplate \"Template:Sub1\"\n"
            +"\ttemplate \"Template:Sub2\"\n"
            +"\tgroups 2\n"
            +"\tgroup\n"
            +"\t\tcondition\n"
            +"\t\t\ttype \"Always\"\n"
            +"\t\t\tparam \"Test Param\"\n"
            +"\t\towner \"PlanetOwner\"\n"
            +"\t\tcolonizeChance 1\n"
            +"\t\titems 2\n"
            +"\t\titem \"item 1\"\n"
            +"\t\titem \"item 2\"\n"
            +"\tgroup\n"
            +"\t\tcondition\n"
            +"\t\t\ttype \"Always\"\n"
            +"\t\t\tparam \"Test Param2\"\n"
            +"\t\towner \"PlanetOwner\"\n"
            +"\t\tcolonizeChance 1\n"
            +"\t\titems 2\n"
            +"\t\titem \"item 4\"\n"
            +"\t\titem \"item 5\"\n"
        )
        cls.identifier = "planetItemsTemplate"
        cls.name = {"identifier":"templateName","val":"Template:Test"}
        cls.subTemps = {
            "counter":{"identifier":"subTemplates","val":2},
            "elements":[
                {"identifier":"template","val":"Template:Sub1"},
                {"identifier":"template","val":"Template:Sub2"}
            ]
        }
        cls.groups = {
            "counter":{"identifier":"groups","val":2},
            "elements":[
                {
                    "identifier":"group",
                    "condition":{
                        "identifier":"condition",
                        "condType":{"identifier":"type","val":"Always"},
                        "param":{"identifier":"param","val":"Test Param"}
                    },
                    "owner":{"identifier":"owner","val":"PlanetOwner"},
                    "colonizeChance":{"identifier":"colonizeChance","val":1},
                    "items":{
                        "counter":{"identifier":"items","val":2},
                        "elements":[
                            {"identifier":"item","val":"item 1"},
                            {"identifier":"item","val":"item 2"}
                        ]
                    }
                },
                {
                    "identifier":"group",
                    "condition":{
                        "identifier":"condition",
                        "condType":{"identifier":"type","val":"Always"},
                        "param":{"identifier":"param","val":"Test Param2"}
                    },
                    "owner":{"identifier":"owner","val":"PlanetOwner"},
                    "colonizeChance":{"identifier":"colonizeChance","val":1},
                    "items":{
                        "counter":{"identifier":"items","val":2},
                        "elements":[
                            {"identifier":"item","val":"item 4"},
                            {"identifier":"item","val":"item 5"}
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.ScenarioTemplate(
                name=self.name,
                identifier=self.identifier,
                subTemplates=self.subTemps,
                groups=self.groups
        )
        patcher = mock.patch.object(self.inst.name,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.subTemplates,"check",
                autospec=True,spec_set=True,return_value=[])
        patcher.start()
        patcher = mock.patch.object(self.inst.groups,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = scen.g_planetItemTemplate.parseString(self.parseString)[0]
        inst = scen.ScenarioTemplate(**parseRes)
        self.assertEqual(self.inst,inst)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribSubTemplates(self):
        """ Test whether subTemplates attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.TemplateRef,**self.subTemps)
        self.assertEqual(self.inst.subTemplates,exp)

    def testCreationAttribSubTemplatesParent(self):
        """ Test whether subTemplates attribute with parent created correctly
        """
        parent = mock.MagicMock(spec_set=scen.Galaxy)
        inst1 = scen.ScenarioTemplate(
                name=self.name,
                identifier=self.identifier,
                subTemplates=self.subTemps,
                groups=self.groups,
                parent=parent
        )
        exp = co_attribs.AttribList(elemType=scen.TemplateRef,
                elemArgs={"parent":parent},
                **self.subTemps)
        self.assertEqual(inst1.subTemplates,exp)

    def testCreationAttribGroups(self):
        """ Test whether groups attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.ScenarioGroup,**self.groups)
        self.assertEqual(self.inst.groups,exp)

    def testToString(self):
        """ Test whether the string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckCallName(self):
        """ Test whether name attribute is checked
        """
        self.inst.check(self.mmod)
        self.inst.name.check.assert_called_once_with(self.mmod,False)

    def testCheckCallSubTemplate(self):
        """ Test whether subTemplates attribute is checked
        """
        self.inst.check(self.mmod)
        self.inst.subTemplates.check.assert_called_once_with(self.mmod,False)

    def testCheckCallGroups(self):
        """ Test whether groups attribute is checked
        """
        self.inst.check(self.mmod)
        self.inst.groups.check.assert_called_once_with(self.mmod,False)

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = scen.ScenarioTemplate(
                name=self.name,
                identifier=self.identifier,
                subTemplates=self.subTemps,
                groups=self.groups
        )
        self.assertEqual(self.inst,inst1)

    def testEqualityDiffName(self):
        """ Test whether instances with differing name test equal
        """
        other = {"identifier":"templateName","val":"Template:Foo"}
        inst1 = scen.ScenarioTemplate(
                name=other,
                identifier=self.identifier,
                subTemplates=self.subTemps,
                groups=self.groups
        )
        self.assertNotEqual(self.inst,inst1)

    def testEqualityDiffSubTemplates(self):
        """ Test whether instances with differing subTemplates test equal
        """
        other = {
            "counter":{"identifier":"subTemplates","val":1},
            "elements":[
                {"identifier":"template","val":"Template:Sub2"}
            ]
        }
        inst1 = scen.ScenarioTemplate(
                name=self.name,
                identifier=self.identifier,
                subTemplates=other,
                groups=self.groups
        )
        self.assertNotEqual(self.inst,inst1)

    def testEqualityDiffGroups(self):
        """ Test whether instances with differing groups test equal
        """
        other = {
            "counter":{"identifier":"groups","val":2},
            "elements":[
                {
                    "identifier":"group",
                    "condition":{
                        "identifier":"condition",
                        "condType":{"identifier":"type","val":"Always"},
                        "param":{"identifier":"param","val":"Test3 Param"}
                    },
                    "owner":{"identifier":"owner","val":"PlanetOwner"},
                    "colonizeChance":{"identifier":"colonizeChance","val":1},
                    "items":{
                        "counter":{"identifier":"items","val":2},
                        "elements":[
                            {"identifier":"item","val":"item 1"},
                            {"identifier":"item","val":"item 2"}
                        ]
                    }
                },
                {
                    "identifier":"group",
                    "condition":{
                        "identifier":"condition",
                        "condType":{"identifier":"type","val":"Always"},
                        "param":{"identifier":"param","val":"Test Param2"}
                    },
                    "owner":{"identifier":"owner","val":"PlanetOwner"},
                    "colonizeChance":{"identifier":"colonizeChance","val":1},
                    "items":{
                        "counter":{"identifier":"items","val":2},
                        "elements":[
                            {"identifier":"item","val":"item 4"},
                            {"identifier":"item","val":"item 5"}
                        ]
                    }
                }
            ]
        }
        inst1 = scen.ScenarioTemplate(
                name=self.name,
                identifier=self.identifier,
                subTemplates=self.subTemps,
                groups=other
        )
        self.assertNotEqual(self.inst,inst1)

"""########################### Tests ScenarioGroup #########################"""

class ScenarioGroupTests(unit.TestCase):
    desc = "Test ScenarioGroup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "group\n"
            +"\tcondition\n"
            +"\t\ttype \"Always\"\n"
            +"\t\tparam \"Test Param\"\n"
            +"\towner \"PlanetOwner\"\n"
            +"\tcolonizeChance 1\n"
            +"\titems 2\n"
            +"\titem \"item 1\"\n"
            +"\titem \"item 2\"\n"
        )
        cls.mmod = tco.genMockMod("./")
        cls.identifier = "group"
        cls.condition = {
            "identifier":"condition",
            "condType":{"identifier":"type","val":"Always"},
            "param":{"identifier":"param","val":"Test Param"},
        }
        cls.owner = {"identifier":"owner","val":"PlanetOwner"}
        cls.colonizeChance = {"identifier":"colonizeChance","val":1}
        cls.items = {
            "counter":{"identifier":"items","val":2},
            "elements":[
                {"identifier":"item","val":"item 1"},
                {"identifier":"item","val":"item 2"}
            ]
        }

    def setUp(self):
        self.inst = scen.ScenarioGroup(
                identifier=self.identifier,
                condition=self.condition,
                owner=self.owner,
                colonizeChance=self.colonizeChance,
                items=self.items
        )
        patcher = mock.patch.object(self.inst.items,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = scen.g_itemGroup.parseString(self.parseString)[0]
        inst = scen.ScenarioGroup(**parseRes)
        self.assertEqual(inst,self.inst)

    def testCreationCondition(self):
        """ Test whether creation of condition attribute works correctly
        """
        exp = scen.Condition(**self.condition)
        self.assertEqual(self.inst.condition,exp)

    def testCreationOwner(self):
        """ Test whether creation of owner attribute works correctly
        """
        exp = co_attribs.AttribString(**self.owner)
        self.assertEqual(self.inst.owner,exp)

    def testCreationColChance(self):
        """ Test whether creation of colChance attribute works correctly
        """
        exp = co_attribs.AttribNum(**self.colonizeChance)
        self.assertEqual(self.inst.colChance,exp)

    def testCreationItems(self):
        """ Test whether creation of items attribute works correctly
        """
        exp = co_attribs.AttribList(elemType=scen.ItemRef,**self.items)
        self.assertEqual(self.inst.items,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckCallCond(self):
        """ Test whether condition.check is called during checking
        """
        with mock.patch.object(self.inst.condition,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckCallOwner(self):
        """ Test whether owner.check is called during checking
        """
        with mock.patch.object(self.inst.owner,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckCallColChance(self):
        """ Test whether colChance.check is called during checking
        """
        with mock.patch.object(self.inst.colChance,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckCallItems(self):
        """ Test whether items.check is called during checking
        """
        self.inst.check(self.mmod)
        self.inst.items.check.assert_called_once_with(self.mmod,False)

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst1 = scen.ScenarioGroup(
                identifier=self.identifier,
                condition=self.condition,
                owner=self.owner,
                colonizeChance=self.colonizeChance,
                items=self.items
        )
        self.assertEqual(self.inst,inst1)

    def testEqualityDifferingCond(self):
        """ Test whether instances with differing conditions test unqual
        """
        other = {
            "identifier":"condition",
            "condType":{"identifier":"type","val":"Always"},
            "param":{"identifier":"param","val":"Test2 Param"},
        }
        inst1 = scen.ScenarioGroup(
                identifier=self.identifier,
                condition=other,
                owner=self.owner,
                colonizeChance=self.colonizeChance,
                items=self.items
        )
        self.assertNotEqual(self.inst,inst1)

    def testEqualityDifferingOwner(self):
        """ Test whether instances with differing owner test unqual
        """
        other = {"identifier":"owner","val":"Player1"}
        inst1 = scen.ScenarioGroup(
                identifier=self.identifier,
                condition=self.condition,
                owner=other,
                colonizeChance=self.colonizeChance,
                items=self.items
        )
        self.assertNotEqual(self.inst,inst1)

    def testEqualityDifferingColChance(self):
        """ Test whether instances with differing colChance test unqual
        """
        other = {"identifier":"colonizeChance","val":3}
        self.items = {
            "counter":{"identifier":"items","val":2},
            "elements":[
                {"identifier":"item","val":"item 1"},
                {"identifier":"item","val":"item 2"}
            ]
        }
        inst1 = scen.ScenarioGroup(
                identifier=self.identifier,
                condition=self.condition,
                owner=self.owner,
                colonizeChance=other,
                items=self.items
        )
        self.assertNotEqual(self.inst,inst1)

    def testEqualityDifferingItems(self):
        """ Test whether instances with differing items test unqual
        """
        other = {
            "counter":{"identifier":"items","val":2},
            "elements":[
                {"identifier":"item","val":"item 1"},
                {"identifier":"item","val":"item 4"}
            ]
        }
        inst1 = scen.ScenarioGroup(
                identifier=self.identifier,
                condition=self.condition,
                owner=self.owner,
                colonizeChance=self.colonizeChance,
                items=other
        )
        self.assertNotEqual(self.inst,inst1)

"""############################## Tests ItemRef ############################"""

class ItemRefTest(unit.TestCase):
    desc = "Test ItemRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"testref\"\n"

    def setUp(self):
        self.inst = scen.ItemRef(identifier="ident",val="testref")
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether creation from parseRes works correctly
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        inst = scen.ItemRef(**parseRes)
        self.assertEqual(self.inst,inst)

    def testCheckCallsNoProbs(self):
        """ Test whether correct methods for check are called with no probs
        """
        self.mmod.scenarios.galDef.planetItems.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.mmod.scenarios.galDef.planetItems.__contains__.assert_called_once_with(
                "testref"
        )
        self.assertEqual(res,[])

    def testCheckMalformedGalDef(self):
        """ Test whether check with malformed GalDef works
        """
        self.mmod.scenarios.galDef = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckCallsProbs(self):
        """ Test whether correct methods for check are called with probs
        """
        self.mmod.scenarios.galDef.planetItems.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.mmod.scenarios.galDef.planetItems.__contains__.assert_called_once_with(
                "testref"
        )
        self.assertEqual(res,[
            co_probs.MissingRefProblem("Planet Item","testref")])

"""############################# Tests Condition ###########################"""

class ConditionTest(unit.TestCase):
    desc = "Test Condition:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "condition\n"
            +"\ttype \"Always\"\n"
            +"\tparam \"TestParam\"\n"
        )
        cls.identifier = "condition"
        cls.condType = {"identifier":"type","val":"Always"}
        cls.param = {"identifier":"param","val":"TestParam"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.Condition(
                condType=self.condType,
                param=self.param,
                identifier=self.identifier
        )

    def testParseCreation(self):
        """ Test whether creation from parse results works correctly
        """
        parseRes = scen.g_itemCondition.parseString(self.parseString)[0]
        res = scen.Condition(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribCondType(self):
        """ Test whether condType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=scen.GROUP_COND_TYPES,**self.condType)
        self.assertEqual(self.inst.condType,exp)

    def testCreationAttribParam(self):
        """ Test whether param attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.param)
        self.assertEqual(self.inst.param,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheck(self):
        """ Test whether all attributes are checked correctly
        """
        with mock.patch.object(self.inst.condType,"check",
                autospec=True) as mocked:
            self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = scen.Condition(
                condType=self.condType,
                param=self.param,
                identifier=self.identifier
        )
        self.assertEqual(self.inst,inst1)

    def testEqualDifferentType(self):
        """ Test whether instances with different condType test as equal
        """
        differing = {"identifier":"type","val":"NoPlanetOwner"}
        inst1 = scen.Condition(
                condType=differing,
                param=self.param,
                identifier=self.identifier
        )
        self.assertNotEqual(self.inst,inst1)

    def testEqualDifferentParam(self):
        """ Test whether instances with different condType test as equal
        """
        differing = {"identifier":"param","val":"TestParam2"}
        inst1 = scen.Condition(
                condType=self.condType,
                param=differing,
                identifier=self.identifier
        )
        self.assertNotEqual(self.inst,inst1)

"""########################### Tests OrbitBodyType #########################"""

class OrbitBodyTypeTest(unit.TestCase):
    desc = "Test OrbitBodyType:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "orbitBodyType\n"
            +"\ttypeName \"test type\"\n"
            +"\tentityDefName \"test entity\"\n"
            +"\tdefaultTemplateName \"test template\"\n"
        )
        cls.identifier = "orbitBodyType"
        cls.name = {"identifier":"typeName","val":"test type"}
        cls.entityDef = {"identifier":"entityDefName","val":"test entity"}
        cls.defTemplate = {"identifier":"defaultTemplateName",
            "val":"test template"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.OrbitBodyType(
                name=self.name,
                entityDef=self.entityDef,
                defTemplate=self.defTemplate,
                identifier=self.identifier
        )

    def testParseCreation(self):
        """ Test whether creation from parse result works
        """
        parseRes = scen.g_orbitBodyType.parseString(self.parseString)[0]
        res = scen.OrbitBodyType(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribEntityDef(self):
        """ Test whether entityDef attribute is created correctly
        """
        exp = coe.EntityRef(types=["Star","Planet"],**self.entityDef)
        self.assertEqual(self.inst.entityDef,exp)

    def testCreationAttribDefTemplate(self):
        """ Test whether defTemplate attribute is created correctly
        """
        exp = scen.TemplateRef(**self.defTemplate)
        self.assertEqual(self.inst.defTemplate,exp)

    def testToString(self):
        """ Test whether the string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckCallEntityDef(self):
        """ Test whether check is called for entityDef attribute
        """
        with mock.patch.object(self.inst.entityDef,"check",autospec=True,
                return_value=[mock.sentinel.prob]) as mocked, \
                mock.patch.object(self.inst.defTemplate,"check",
                        autospec=True,
                        return_value=[]):
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,mocked.return_value)

    def testCheckCallDefTemplate(self):
        """ Test whether check is called for defTemplate attribute
        """
        with mock.patch.object(self.inst.defTemplate,"check",autospec=True,
                return_value=[mock.sentinel.prob]) as mocked, \
                mock.patch.object(self.inst.entityDef,"check",autospec=True,
                        return_value=[]):
            res = self.inst.check(self.mmod)
            mocked.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,mocked.return_value)

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst1 = scen.OrbitBodyType(
                name=self.name,
                entityDef=self.entityDef,
                defTemplate=self.defTemplate,
                identifier=self.identifier
        )
        self.assertEqual(self.inst,inst1)

    def testDiffName(self):
        """ Test whether instances with different names test unequal
        """
        other = {"identifier":"typeName","val":"test2 type"}
        inst1 = scen.OrbitBodyType(
                name=other,
                entityDef=self.entityDef,
                defTemplate=self.defTemplate,
                identifier=self.identifier
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffEntityDef(self):
        """ Test whether instances with different entityDefs test unequal
        """
        other = {"identifier":"entityDefName","val":"test2 entity"}
        inst1 = scen.OrbitBodyType(
                name=self.name,
                entityDef=other,
                defTemplate=self.defTemplate,
                identifier=self.identifier
        )
        self.assertNotEqual(self.inst,inst1)

    def testDiffDefTemplate(self):
        """ Test whether instances with different defTemplates test unequal
        """
        other = {"identifier":"defaultTemplateName","val":"test2 template"}
        inst1 = scen.OrbitBodyType(
                name=self.name,
                entityDef=self.entityDef,
                defTemplate=other,
                identifier=self.identifier
        )
        self.assertNotEqual(self.inst,inst1)

"""############################ Tests TemplateRef ##########################"""

class TemplateRefTest(unit.TestCase):
    desc = "Test TemplateRef"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "subTemplate \"Test\"\n"
        cls.identifier = "subTemplate"
        cls.val = "Test"

    def setUp(self):
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Check whether creation from parse result works
        """
        parseRes = co_parse.genStringAttrib("subTemplate").parseString(
                self.parseString)[0]
        inst = scen.TemplateRef(**parseRes)
        self.assertEqual(inst.identifier,self.identifier)
        self.assertEqual(inst.ref,self.val)

    def testCheckNoProb(self):
        """ Test whether existing ref check returns no problems
        """
        inst = scen.TemplateRef(self.identifier,self.val)
        self.mmod.scenarios.galDef.planetTemplates.__contains__.return_value = True
        res = inst.check(self.mmod)
        self.mmod.scenarios.galDef.planetTemplates.__contains__.assert_called_once_with(
                self.val)
        self.assertEqual(res,[])

    def testCheckMalformedGalDef(self):
        """ Test whether malformed GalDef works
        """
        inst = scen.TemplateRef(self.identifier,self.val)
        self.mmod.scenarios.galDef = []
        res = inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckNonexistent(self):
        """ Test whether nonexistent ref check returns correct problem
        """
        self.mmod.scenarios.galDef.planetTemplates.__contains__.return_value = False
        prob = co_probs.MissingRefProblem("Scenario Template",self.val)
        inst = scen.TemplateRef(self.identifier,self.val)
        res = inst.check(self.mmod)
        self.mmod.scenarios.galDef.planetTemplates.__contains__.assert_called_once_with(
                self.val)
        self.assertEqual(res,[prob])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        inst = scen.TemplateRef(self.identifier,self.val)
        self.assertEqual(inst.toString(0),self.parseString)

    def testEqual(self):
        """ Test whether similar refs test equal
        """
        inst = scen.TemplateRef(self.identifier,self.val)
        inst2 = scen.TemplateRef(self.identifier,self.val)
        self.assertEqual(inst,inst2)

    def testUnequal(self):
        """ Test whether differing refs test unequal
        """
        inst = scen.TemplateRef(self.identifier,self.val)
        inst2 = scen.TemplateRef(self.identifier,"foo")
        self.assertNotEqual(inst,inst2)

"""########################## Tests OrbitBodyDesign ########################"""

class OrbitBodyDesignTest(unit.TestCase):
    desc = "Test OrbitBodyDeisgn:"

    @classmethod
    def setUpClass(cls):
        cls.mmod = tco.genMockMod("./")
        cls.stringStar = ("starType\n"
                          +"\tdesignName \"test name\"\n"
                          +"\tdesignStringId \"test string\"\n"
                          +"\torbitBodyTypeCount 2\n"
                          +"\torbitBodyType \"type1\"\n"
                          +"\torbitBodyType \"type2\"\n"
        )
        cls.stringPlanet = ("planetType\n"
                            +"\tdesignName \"test name\"\n"
                            +"\tdesignStringId \"test string\"\n"
                            +"\torbitBodyTypeCount 2\n"
                            +"\torbitBodyType \"type1\"\n"
                            +"\torbitBodyType \"type2\"\n"
        )
        cls.identifier = "planetType"
        cls.designStringId = {
            "identifier":"designStringId",
            "val":"test string"
        }
        cls.name = {
            "identifier":"designName",
            "val":"test name"
        }
        cls.types = {
            "counter":{"identifier":"orbitBodyTypeCount","val":2},
            "elements":[
                {"identifier":"orbitBodyType","val":"type1"},
                {"identifier":"orbitBodyType","val":"type2"}
            ]
        }
        cls.galScenDef = mock.create_autospec(scen.GalaxyScenarioDef,
                spec_set=True)

    def setUp(self):
        self.inst = scen.OrbitBodyDesign(
                parent=self.galScenDef,
                name=self.name,
                designStringId=self.designStringId,
                orbitBodyTypes=self.types,
                identifier=self.identifier)

    def testCreationTPlanet(self):
        """ Test whether creation with planet type from parseres works correctly
        """
        parseRes = scen.g_orbitBodyDesignPlanet.parseString(self.stringPlanet)[
            0]
        inst = scen.OrbitBodyDesign(parent=self.galScenDef,**parseRes)
        self.assertEqual(inst.types.elements[0].bodyType,"Planet")

    def testCreationTStar(self):
        """ Test whether creation with star type from parseres works correctly
        """
        parseRes = scen.g_orbitBodyDesignStar.parseString(self.stringStar)[0]
        inst = scen.OrbitBodyDesign(parent=self.galScenDef,**parseRes)
        self.assertEqual(inst.types.elements[0].bodyType,"Star")

    def testCreationTInvalid(self):
        """ Test whether creation with invalid type raises error
        """
        with self.assertRaises(RuntimeError):
            scen.OrbitBodyDesign(
                    parent=self.galScenDef,
                    name=self.name,
                    designStringId=self.designStringId,
                    orbitBodyTypes=self.types,
                    identifier="foo")

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribDesignString(self):
        """ Test whether designStringId attribute is created correctly
        """
        exp = ui.StringReference(**self.designStringId)
        self.assertEqual(self.inst.designStringId,exp)

    def testCreationAttribTypes(self):
        """ Test whether types attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.OrbitBodyTypeRef,
                elemArgs={"bodyType":"Planet","parent":self.galScenDef},
                **self.types)
        self.assertEqual(self.inst.types,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.stringPlanet)

    def testCheckCall(self):
        """ Test whether check methods for all attributes are called
        """
        with mock.patch.object(self.inst.types,"check",autospec=True), \
                mock.patch.object(self.inst.name,"check",autospec=True), \
                mock.patch.object(self.inst.designStringId,"check",
                        autospec=True):
            self.inst.check(self.mmod)
            self.inst.types.check.assert_called_once_with(self.mmod,False)
            self.inst.name.check.assert_called_once_with(self.mmod,False)
            self.inst.designStringId.check.assert_called_once_with(self.mmod,
                    False)

    def testEqual(self):
        """ Test whether similar instances test as equal
        """
        inst1 = scen.OrbitBodyDesign(
                parent=self.galScenDef,
                name=self.name,
                designStringId=self.designStringId,
                orbitBodyTypes=self.types,
                identifier=self.identifier)
        self.assertEqual(self.inst,inst1)

"""########################## Tests OrbitBodyTypeRef #######################"""

class OrbitBodyTypeRefTest(unit.TestCase):
    desc = "Test OrbitBodyTypeRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "orbitBodyType \"testRef\"\n"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.galScenDef = mock.create_autospec(
                scen.GalaxyScenarioDef,
                spec_set=True)

    def testCreationFromParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = co_parse.genStringAttrib("orbitBodyType").parseString(self.parseString)[
            0]
        inst = scen.OrbitBodyTypeRef(bodyType="Planet",parent=self.galScenDef,
                **res)
        self.assertEqual(inst.identifier,"orbitBodyType")
        self.assertEqual(inst.ref,"testRef")
        self.assertEqual(inst.bodyType,"Planet")

    def testCreationInvalidType(self):
        """ Test whether creation with invalid bodyType raises error
        """
        with self.assertRaises(RuntimeError):
            scen.OrbitBodyTypeRef(
                    identifier="orbitBodyType",
                    val="testRef",
                    parent=self.galScenDef,
                    bodyType="Fighter"
            )

    def testCreationNoneParent(self):
        """ Test whether creation with parent=None raises error
        """
        with self.assertRaises(RuntimeError):
            scen.OrbitBodyTypeRef(
                    identifier="orbitBodyType",
                    val="testRef",
                    parent=None,
                    bodyType="Star"
            )

    def testCreationInvalidParent(self):
        """ Test whether creation with invalid parent raises error
        """
        mockStringFile = mock.create_autospec(ui.StringFile,
                spec_set=True)
        with self.assertRaises(RuntimeError):
            scen.OrbitBodyTypeRef(
                    identifier="orbitBodyType",
                    val="testRef",
                    parent=mockStringFile,
                    bodyType="Star"
            )

    def testCheckCallStar(self):
        """ Test whether correct checking methods are called with star type
        """
        self.galScenDef.orbitBodyTypes.__contains__.return_value = True
        self.galScenDef.checkTypeOrbitBodyType.return_value = True
        inst = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Star"
        )
        inst.check(self.mmod)
        self.galScenDef.orbitBodyTypes.__contains__.assert_called_once_with(
                "testRef")
        self.galScenDef.checkTypeOrbitBodyType.assert_called_once_with(
                self.mmod,"testRef","Star")

    def testCheckCallPlanet(self):
        """ Test whether correct checking methods are called with planet type
        """
        self.galScenDef.orbitBodyTypes.__contains__.return_value = True
        self.galScenDef.checkTypeOrbitBodyType.return_value = True
        inst = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Planet"
        )
        inst.check(self.mmod)
        self.galScenDef.orbitBodyTypes.__contains__.assert_called_once_with(
                "testRef")
        self.galScenDef.checkTypeOrbitBodyType.assert_called_once_with(
                self.mmod,"testRef","Planet")

    def testCheckProbMissing(self):
        """ Test whether missing reference produces correct problem
        """
        self.galScenDef.orbitBodyTypes.__contains__.return_value = False
        inst = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Planet"
        )
        res = inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.assertEqual(res[0].ref,"testRef")

    def testCheckProbWrongType(self):
        """ Test whether wrong entity type produces correct problem
        """
        self.galScenDef.orbitBodyTypes.__contains__.return_value = True
        self.galScenDef.checkTypeOrbitBodyType.return_value = False
        inst = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Planet"
        )
        res = inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)

    def testEqual(self):
        """ Test whether similar instances test as equal
        """
        inst1 = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Planet"
        )
        inst2 = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Planet"
        )
        self.assertEqual(inst1,inst2)

    def testEqualDifferentTypeParent(self):
        """ Test whether instances with different type/parent test equal
        """
        inst1 = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=self.galScenDef,
                bodyType="Planet"
        )
        inst2 = scen.OrbitBodyTypeRef(
                identifier="orbitBodyType",
                val="testRef",
                parent=mock.create_autospec(
                        scen.GalaxyScenarioDef,
                        spec_set=True),
                bodyType="Star"
        )
        self.assertEqual(inst1,inst2)

"""############################ Tests EntityDesign #########################"""

class EntityDesignTest(unit.TestCase):
    desc = "Test EntityDesign:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetItemType\n\tdesignName \"Testdesign\"\n"
            +"\tentityDefName \"TestEntity\"\n"
        )
        cls.entityTypes = ["Frigate","Fighter"]
        cls.identifier = "planetItemType"
        cls.name = {"identifier":"designName","val":"Testdesign"}
        cls.entityDef = {"identifier":"entityDefName","val":"TestEntity"}

    def setUp(self):
        self.inst = scen.EntityDesign(
                identifier=self.identifier,
                name=self.name,
                entityDef=self.entityDef,
                entTypes=self.entityTypes
        )
        self.mmod = tco.genMockMod("./")

    def testCreationFromParseRes(self):
        """ Test whether instance is initialized correctly from parse result
        """
        parsed = scen.g_planetItemDesign.parseString(self.parseString)[0]
        res = scen.EntityDesign(entTypes=self.entityTypes,**parsed)
        self.assertEqual(res,self.inst)

    def testCheck(self):
        """ Test whether check returns an empty list and calls attribute checks
        """
        with mock.patch.object(self.inst.entityDef,"check",spec_set=True,
                autospec=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.inst.entityDef.check.assert_called_once_with(self.mmod,False)
            self.assertEqual(res,[])

    def testCheckProbs(self):
        """ Test whether check returns problems
        """
        probs = [co_probs.BasicProblem("Test1",severity=co_probs.ProblemSeverity.warn)]
        with mock.patch.object(self.inst.entityDef,"check",spec_set=True,
                autospec=True,return_value=probs):
            self.assertEqual(self.inst.check(self.mmod),probs)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests ScenarioPlayer ########################"""

class ScenarioPlayerTest(unit.TestCase):
    desc = "Test ScenarioPlayer:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "player\n"
            +"\tdesignName \"testdesign\"\n"
            +"\tinGameName \"testingame\"\n"
            +"\toverrideRaceName \"overname\"\n"
            +"\tteamIndex 2\n"
            +"\tstartingCredits 1000\n"
            +"\tstartingMetal 250\n"
            +"\tstartingCrystal 490\n"
            +"\tisNormalPlayer FALSE\n"
            +"\tisRaidingPlayer FALSE\n"
            +"\tisInsurgentPlayer FALSE\n"
            +"\tisOccupationPlayer FALSE\n"
            +"\tisMadVasariPlayer FALSE\n"
            +"\tthemeGroup \"testtheme\"\n"
            +"\tthemeIndex 4\n"
            +"\tpictureGroup \"testpicture\"\n"
            +"\tpictureIndex 9\n"
        )
        cls.name = {"identifier":"designName","val":"testdesign"}
        cls.ingameName = {"identifier":"inGameName","val":"testingame"}
        cls.overrideRaceName = {"identifier":"overrideRaceName",
            "val":"overname"}
        cls.teamIndex = {"identifier":"teamIndex","val":2}
        cls.startingCredits = {"identifier":"startingCredits","val":1000}
        cls.startingMetal = {"identifier":"startingMetal","val":250}
        cls.startingCrystal = {"identifier":"startingCrystal","val":490}
        cls.isNormal = {"identifier":"isNormalPlayer","val":False}
        cls.isRaiding = {"identifier":"isRaidingPlayer","val":False}
        cls.isInsurgent = {"identifier":"isInsurgentPlayer","val":False}
        cls.isOccupation = {"identifier":"isOccupationPlayer","val":False}
        cls.isMad = {"identifier":"isMadVasariPlayer","val":False}
        cls.theme = {"identifier":"themeGroup","val":"testtheme"}
        cls.themeIndex = {"identifier":"themeIndex","val":4}
        cls.picture = {"identifier":"pictureGroup","val":"testpicture"}
        cls.pictureIndex = {"identifier":"pictureIndex","val":9}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.ScenarioPlayer(
                identifier="player",
                name=self.name,
                ingameName=self.ingameName,
                overrideRaceName=self.overrideRaceName,
                teamIndex=self.teamIndex,
                startingCredits=self.startingCredits,
                startingMetal=self.startingMetal,
                startingCrystal=self.startingCrystal,
                isNormal=self.isNormal,
                isRaiding=self.isRaiding,
                isInsurgent=self.isInsurgent,
                isOccupation=self.isOccupation,
                isMad=self.isMad,
                theme=self.theme,
                themeIndex=self.themeIndex,
                picture=self.picture,
                pictureIndex=self.pictureIndex
        )
        patcher = mock.patch.object(self.inst.ingameName,"check",autospec=True,
                spec_set=True)
        patcher.start()
        patcher = mock.patch.object(self.inst.theme,"check",autospec=True,
                spec_set=True)
        patcher.start()
        patcher = mock.patch.object(self.inst.picture,"check",autospec=True,
                spec_set=True)
        patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether creation from parse result works correctly
        """
        parseRes = scen.g_scenarioPlayer.parseString(self.parseString)[0]
        res = scen.ScenarioPlayer(**parseRes)
        self.assertEqual(self.inst,res)

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        self.assertEqual(self.inst.name,exp)

    def testCreationAttribIngameName(self):
        """ Test whether ingameName attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.ingameName)
        self.assertEqual(self.inst.ingameName,exp)

    def testCreationAttribOverrideRaceName(self):
        """ Test whether overrideRaceName attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.overrideRaceName)
        self.assertEqual(self.inst.overrideRaceName,exp)

    def testCreationAttribTeamIndex(self):
        """ Test whether teamIndex attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.teamIndex)
        self.assertEqual(self.inst.teamIndex,exp)

    def testCreationAttribStartingCredits(self):
        """ Test whether startingCredits attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startingCredits)
        self.assertEqual(self.inst.startingCredits,exp)

    def testCreationAttribStartingMetal(self):
        """ Test whether startingMetal attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startingMetal)
        self.assertEqual(self.inst.startingMetal,exp)

    def testCreationAttribStartingCrystal(self):
        """ Test whether startingCrystal attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startingCrystal)
        self.assertEqual(self.inst.startingCrystal,exp)

    def testCreationAttribIsNormal(self):
        """ Test whether isNormal attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isNormal)
        self.assertEqual(self.inst.isNormal,exp)

    def testCreationAttribIsRaiding(self):
        """ Test whether isRaiding attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isRaiding)
        self.assertEqual(self.inst.isRaiding,exp)

    def testCreationAttribIsInsurgent(self):
        """ Test whether isInsurgent attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isInsurgent)
        self.assertEqual(self.inst.isInsurgent,exp)

    def testCreationAttribIsOccupation(self):
        """ Test whether isOccupation attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isOccupation)
        self.assertEqual(self.inst.isOccupation,exp)

    def testCreationAttribIsMad(self):
        """ Test whether isMad attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isMad)
        self.assertEqual(self.inst.isMad,exp)

    def testCreationAttribTheme(self):
        """ Test whether theme attribute is created correctly
        """
        exp = themes.PlayerThemeRef(**self.theme)
        self.assertEqual(self.inst.theme,exp)

    def testCreationAttribPicture(self):
        """ Test whether picture attribute is created correctly
        """
        exp = themes.PlayerPictureRef(**self.picture)
        self.assertEqual(self.inst.picture,exp)

    def testCreationAttribThemeIndex(self):
        """ Test whether themeIndex attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.themeIndex)
        self.assertEqual(self.inst.themeIndex,exp)

    def testCreationAttribPictureIndex(self):
        """ Test whether themeIndex attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.pictureIndex)
        self.assertEqual(self.inst.pictureIndex,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

    def testCheckIngameName(self):
        """ Test whether check checks ingameName attribute
        """
        self.inst.ingameName.check.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.inst.ingameName.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckTheme(self):
        """ Test whether check checks theme attribute
        """
        self.inst.theme.check.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.inst.theme.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[mock.sentinel.prob])

    def testCheckPicture(self):
        """ Test whether check checks picture attribute
        """
        self.inst.picture.check.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.inst.picture.check.assert_called_once_with(self.mmod,False)
        self.assertEqual(res,[mock.sentinel.prob])

    def testEquality(self):
        """ Test whether similar instances test equal
        """
        inst1 = scen.ScenarioPlayer(
                identifier="player",
                name=self.name,
                ingameName=self.ingameName,
                overrideRaceName=self.overrideRaceName,
                teamIndex=self.teamIndex,
                startingCredits=self.startingCredits,
                startingMetal=self.startingMetal,
                startingCrystal=self.startingCrystal,
                isNormal=self.isNormal,
                isRaiding=self.isRaiding,
                isInsurgent=self.isInsurgent,
                isOccupation=self.isOccupation,
                isMad=self.isMad,
                theme=self.theme,
                themeIndex=self.themeIndex,
                picture=self.picture,
                pictureIndex=self.pictureIndex
        )
        self.assertEqual(inst1,self.inst)

"""########################## Tests GalaxyScenarioDef ######################"""

class GalaxyScenarioDefTest(object):
    @classmethod
    def setUpClass(cls):
        cls.filepath = "GalaxyScenarioDef.galaxyScenarioDef"
        with open(cls.filepath,'r') as f:
            cls.parseString = f.read()
        cls.starTypes = {
            "counter":{"identifier":"starTypeCount","val":2},
            "elements":[
                {"identifier":"starType",
                    "name":{"identifier":"designName","val":"RandomStar"},
                    "designStringId":{"identifier":"designStringId",
                        "val":"IDSGalaxyScenarioTypeStarRandom"},
                    "orbitBodyTypes":{
                        "counter":{"identifier":"orbitBodyTypeCount",
                            "val":2},
                        "elements":[
                            {"identifier":"orbitBodyType",
                                "val":"BlueStar"},
                            {"identifier":"orbitBodyType",
                                "val":"GreenStar"}
                        ]
                    }
                },
                {"identifier":"starType",
                    "name":{"identifier":"designName",
                        "val":"RandomNonGreenStar"},
                    "designStringId":{"identifier":"designStringId",
                        "val":"IDSGalaxyScenarioTypeStarRandomNonGreen"},
                    "orbitBodyTypes":{
                        "counter":{"identifier":"orbitBodyTypeCount",
                            "val":1},
                        "elements":[
                            {"identifier":"orbitBodyType",
                                "val":"BlueStar"}
                        ]
                    }
                }
            ]
        }
        cls.planetTypes = {
            "counter":{"identifier":"planetTypeCount","val":1},
            "elements":[
                {"identifier":"planetType",
                    "name":{"identifier":"designName",
                        "val":"WeightedNonIce"},
                    "designStringId":{"identifier":"designStringId",
                        "val":"IDSGalaxyScenarioTypePlanetWeightedNonIce"},
                    "orbitBodyTypes":{
                        "counter":{"identifier":"orbitBodyTypeCount",
                            "val":3},
                        "elements":[
                            {"identifier":"orbitBodyType",
                                "val":"Terran"},
                            {"identifier":"orbitBodyType",
                                "val":"Desert"},
                            {"identifier":"orbitBodyType",
                                "val":"Volcanic"}
                        ]
                    }
                }
            ]
        }
        cls.orbitBodyTypes = {
            "counter":{"identifier":"orbitBodyTypeCount","val":5},
            "elements":[
                {
                    "identifier":"orbitBodyType",
                    "name":{"identifier":"typeName","val":"BlueStar"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"STAR_BLUE"},
                    "defTemplate":{"identifier":"defaultTemplateName",
                        "val":""}
                },
                {
                    "identifier":"orbitBodyType",
                    "name":{"identifier":"typeName","val":"GreenStar"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"STAR_GREEN"},
                    "defTemplate":{"identifier":"defaultTemplateName",
                        "val":""}
                },
                {
                    "identifier":"orbitBodyType",
                    "name":{"identifier":"typeName","val":"Terran"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"PlanetTerran"},
                    "defTemplate":{"identifier":"defaultTemplateName",
                        "val":"Template:DefaultStart_Terran"}
                },
                {
                    "identifier":"orbitBodyType",
                    "name":{"identifier":"typeName","val":"Desert"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"PlanetDesert"},
                    "defTemplate":{"identifier":"defaultTemplateName",
                        "val":"Template:DefaultStart_Terran"}
                },
                {
                    "identifier":"orbitBodyType",
                    "name":{"identifier":"typeName","val":"Volcanic"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"PlanetVolcanic"},
                    "defTemplate":{"identifier":"defaultTemplateName",
                        "val":"Template:DefaultStart_Terran"}
                }
            ]
        }
        cls.planetItems = {
            "counter":{"identifier":"planetItemTypeCount","val":4},
            "elements":[
                {
                    "identifier":"planetItemType",
                    "name":{"identifier":"designName",
                        "val":"Tech:Module:CrystalExtractor"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"PLANETMODULE_TECHORBITALCRYSTALEXTRACTOR"}
                },
                {
                    "identifier":"planetItemType",
                    "name":{"identifier":"designName",
                        "val":"Tech:Frigate:AntiFighter"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"FrigateTechAntiFighter"}
                },
                {
                    "identifier":"planetItemType",
                    "name":{"identifier":"designName",
                        "val":"Tech:Frigate:Light"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"FrigateTechLight"}
                },
                {
                    "identifier":"planetItemType",
                    "name":{"identifier":"designName",
                        "val":"Tech:Frigate:LongRange"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"FrigateTechLongRange"}
                }
            ]
        }
        cls.players = {
            "counter":{"identifier":"playerTypeCount","val":2},
            "elements":[
                {
                    "identifier":"playerType",
                    "name":{"identifier":"designName",
                        "val":"TechLoyalist"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"PlayerTechLoyalist"}
                },
                {
                    "identifier":"playerType",
                    "name":{"identifier":"designName",
                        "val":"PsiLoyalist"},
                    "entityDef":{"identifier":"entityDefName",
                        "val":"PlayerPsiLoyalist"}
                }
            ]
        }

        cls.planetTemplates = {
            "counter":{"identifier":"planetItemsTemplateCount",
                "val":3},
            "elements":[
                {
                    "identifier":"planetItemsTemplate",
                    "name":{"identifier":"templateName",
                        "val":"Template:DefaultStart_Terran"},
                    "subTemplates":{
                        "counter":{"identifier":"subTemplates","val":1},
                        "elements":[
                            {"identifier":"template",
                                "val":"Template:LocalMilitia_Terran"}
                        ]
                    },
                    "groups":{
                        "counter":{"identifier":"groups","val":0},
                        "elements":[]
                    }
                },
                {
                    "identifier":"planetItemsTemplate",
                    "name":{"identifier":"templateName",
                        "val":"Template:LocalMilitiaStrong"},
                    "subTemplates":{
                        "counter":{"identifier":"subTemplates","val":0},
                        "elements":[]
                    },
                    "groups":{
                        "counter":{"identifier":"groups","val":1},
                        "elements":[
                            {
                                "identifier":"group",
                                "condition":{
                                    "identifier":"condition",
                                    "condType":{"identifier":"type",
                                        "val":"PlanetOwnerIsMilitia"},
                                    "param":{"identifier":"param",
                                        "val":""}
                                },
                                "owner":{"identifier":"owner",
                                    "val":"PlanetOwner"},
                                "colonizeChance":{
                                    "identifier":"colonizeChance","val":0},
                                "items":{
                                    "counter":{"identifier":"items",
                                        "val":3},
                                    "elements":[
                                        {
                                            "identifier":"item",
                                            "val":"Tech:Frigate:Light"
                                        },
                                        {
                                            "identifier":"item",
                                            "val":"Tech:Frigate:LongRange"
                                        },
                                        {
                                            "identifier":"item",
                                            "val":"Tech:Frigate:AntiFighter"
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                },
                {
                    "identifier":"planetItemsTemplate",
                    "name":{"identifier":"templateName",
                        "val":"Template:LocalMilitia_Terran"},
                    "subTemplates":{
                        "counter":{"identifier":"subTemplates","val":1},
                        "elements":[
                            {"identifier":"template",
                                "val":"Template:LocalMilitiaStrong"}
                        ]
                    },
                    "groups":{
                        "counter":{"identifier":"groups","val":0},
                        "elements":[]
                    }
                },
            ]
        }
        cls.validPictures = {
            "counter":{"identifier":"validPictureGroups","val":1},
            "elements":[
                {"identifier":"pictureGroup","val":"PlayerTech"}
            ]
        }
        cls.validThemes = {
            "counter":{"identifier":"validThemeGroups","val":1},
            "elements":[
                {"identifier":"themeGroup","val":"PlayerPhase"}
            ]
        }
        cls.autoPlayers = {
            "counter":{"identifier":"autoAddPlayers","val":1},
            "elements":[
                {
                    "identifier":"player",
                    "name":{"identifier":"designName","val":"Pirate"},
                    "ingameName":{"identifier":"inGameName",
                        "val":"IDSScenarioPlayerNamePirates"},
                    "overrideRaceName":{"identifier":"overrideRaceName",
                        "val":"Pirate"},
                    "teamIndex":{"identifier":"teamIndex",
                        "val":-1},
                    "startingCredits":{"identifier":"startingCredits",
                        "val":1000},
                    "startingMetal":{"identifier":"startingMetal",
                        "val":300},
                    "startingCrystal":{"identifier":"startingCrystal",
                        "val":100},
                    "isNormal":{"identifier":"isNormalPlayer",
                        "val":False},
                    "isRaiding":{"identifier":"isRaidingPlayer",
                        "val":True},
                    "isInsurgent":{"identifier":"isInsurgentPlayer",
                        "val":False},
                    "isOccupation":{"identifier":"isOccupationPlayer",
                        "val":False},
                    "isMad":{"identifier":"isMadVasariPlayer",
                        "val":False},
                    "theme":{"identifier":"themeGroup",
                        "val":"SpecialNPC"},
                    "themeIndex":{"identifier":"themeIndex",
                        "val":0},
                    "picture":{"identifier":"pictureGroup",
                        "val":"NPC"},
                    "pictureIndex":{"identifier":"pictureIndex",
                        "val":0},
                }
            ]
        }
        cls.autoMilitiaPerGalaxy = {
            "identifier":"autoAddMiltiaPlayerCountPerGalaxy","val":2}
        cls.autoMilitiaPlayers = {
            "counter":{"identifier":"autoAddMilitiaPlayers","val":1},
            "elements":[
                {
                    "identifier":"player",
                    "name":{"identifier":"designName","val":"Militia0"},
                    "ingameName":{"identifier":"inGameName",
                        "val":"IDSScenarioPlayerNameMilitia0"},
                    "overrideRaceName":{"identifier":"overrideRaceName",
                        "val":"TechRebel"},
                    "teamIndex":{"identifier":"teamIndex",
                        "val":-1},
                    "startingCredits":{"identifier":"startingCredits",
                        "val":1000},
                    "startingMetal":{"identifier":"startingMetal",
                        "val":300},
                    "startingCrystal":{"identifier":"startingCrystal",
                        "val":100},
                    "isNormal":{"identifier":"isNormalPlayer",
                        "val":False},
                    "isRaiding":{"identifier":"isRaidingPlayer",
                        "val":False},
                    "isInsurgent":{"identifier":"isInsurgentPlayer",
                        "val":False},
                    "isOccupation":{"identifier":"isOccupationPlayer",
                        "val":False},
                    "isMad":{"identifier":"isMadVasariPlayer",
                        "val":False},
                    "theme":{"identifier":"themeGroup",
                        "val":"NPC"},
                    "themeIndex":{"identifier":"themeIndex",
                        "val":2},
                    "picture":{"identifier":"pictureGroup",
                        "val":"NPC"},
                    "pictureIndex":{"identifier":"pictureIndex",
                        "val":2},
                }
            ]
        }

class GalaxyScenarioDefCreation(GalaxyScenarioDefTest,unit.TestCase):
    desc = "Test GalaxyScenarioDef creation:"

    def setUp(self):
        self.inst = scen.GalaxyScenarioDef(
                filepath=self.filepath,
                starTypes=self.starTypes,
                planetTypes=self.planetTypes,
                orbitBodyTypes=self.orbitBodyTypes,
                planetItems=self.planetItems,
                players=self.players,
                planetTemplates=self.planetTemplates,
                validPictures=self.validPictures,
                validThemes=self.validThemes,
                autoPlayers=self.autoPlayers,
                autoMilitiaPerGalaxy=self.autoMilitiaPerGalaxy,
                autoMilitiaPlayers=self.autoMilitiaPlayers
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is constructed correctly from parse results
        """
        inst = scen.GalaxyScenarioDef.createInstance(self.filepath)
        self.assertEqual(inst,self.inst)

    def testCreationParseMalformed(self):
        """ Test whether malformed file is recognized and handled correclty
        """
        res = scen.GalaxyScenarioDef.createInstance("malformed.gal")
        self.assertIsInstance(res,list)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)

    def testCreationAttribStarTypes(self):
        """ Test whether starTypes attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.OrbitBodyDesign,
                elemArgs={"parent":self.inst},
                keyfunc=lambda val:val.name.value,
                **self.starTypes)
        self.assertEqual(self.inst.starTypes,exp)

    def testCreationAttribPlanetTypes(self):
        """ Test whether planetTypes attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.OrbitBodyDesign,
                elemArgs={"parent":self.inst},
                keyfunc=lambda val:val.name.value,
                **self.planetTypes)
        self.assertEqual(self.inst.planetTypes,exp)

    def testCreationAttribOrbitBodyTypes(self):
        """ Test whether orbitBodyTypes attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.OrbitBodyType,
                keyfunc=lambda val:val.name.value,
                **self.orbitBodyTypes
        )
        self.assertEqual(self.inst.orbitBodyTypes,exp)

    def testCreationAttribPlanetItems(self):
        """ Test whether planetItems attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.EntityDesign,
                elemArgs={"entTypes":self.inst._allowedPlanetItems},
                keyfunc=lambda val:val.name.value,
                **self.planetItems
        )
        self.assertEqual(self.inst.planetItems,exp)

    def testCreationAttribPlayers(self):
        """ Test whether players attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.EntityDesign,
                elemArgs={"entTypes":["Player"]},
                keyfunc=lambda val:val.name.value,
                **self.players
        )
        self.assertEqual(self.inst.players,exp)

    def testCreationAttribPlanetTemplates(self):
        """ Test whether planetTemplates attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.ScenarioTemplate,
                keyfunc=lambda val:val.name.value,
                **self.planetTemplates
        )
        self.assertEqual(self.inst.planetTemplates,exp)

    def testCreationAttribValidPictures(self):
        """ Test whether validPictures attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=themes.PlayerPictureRef,
                **self.validPictures)
        self.assertEqual(self.inst.validPictures,exp)

    def testCreationAttribValidThemes(self):
        """ Test whether validThemes attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=themes.PlayerThemeRef,
                **self.validThemes
        )
        self.assertEqual(self.inst.validThemes,exp)

    def testCreationAttribAutoPlayers(self):
        """ Test whether autoPlayers attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.ScenarioPlayer,
                **self.autoPlayers
        )
        self.assertEqual(self.inst.autoPlayers,exp)

    def testCreationAttribMilitiaPerGalaxy(self):
        """ Test whether autoMilitiaPerGalaxy attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.autoMilitiaPerGalaxy)
        self.assertEqual(self.inst.autoMilitiaPerGalaxy,exp)

    def testCreationAttribMilitiaPlayers(self):
        """ Test whether autoMilitiaPlayers attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.ScenarioPlayer,
                **self.autoMilitiaPlayers
        )
        self.assertEqual(self.inst.autoMilitiaPlayers,exp)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class GalaxyScenarioDefChecks(GalaxyScenarioDefTest,unit.TestCase):
    desc = "Test GalaxyScenarioDef checking:"

    def setUp(self):
        self.inst = scen.GalaxyScenarioDef(
                filepath=self.filepath,
                starTypes=self.starTypes,
                planetTypes=self.planetTypes,
                orbitBodyTypes=self.orbitBodyTypes,
                planetItems=self.planetItems,
                players=self.players,
                planetTemplates=self.planetTemplates,
                validPictures=self.validPictures,
                validThemes=self.validThemes,
                autoPlayers=self.autoPlayers,
                autoMilitiaPerGalaxy=self.autoMilitiaPerGalaxy,
                autoMilitiaPlayers=self.autoMilitiaPlayers
        )
        self.maxDiff = None
        self.mmod = tco.genMockMod("./")
        self.mmod.scenarios.galDef.return_value = self.inst

    def testCheckOrbitBodyTypeNotIn(self):
        """ Test whether checkTypeOrbitBodyType returns false with missing name
        """
        self.assertFalse(self.inst.checkTypeOrbitBodyType(self.mmod,"Foo",
                "bar"))

    def testCheckOrbitBodyTypeCorrectType(self):
        """ Test whether checkTypeOrbitBodyType returns true with correct entry
        """
        elem = self.inst.orbitBodyTypes.getElement("BlueStar").entityDef
        with mock.patch.object(elem,"checkType",return_value=True):
            self.assertTrue(self.inst.checkTypeOrbitBodyType(self.mmod,
                    "BlueStar","Star"))

    def testCheckOrbitBodyTypeIncorrectType(self):
        """ Test whether checkTypeOrbitBodyType returns false wrong type
        """
        elem = self.inst.orbitBodyTypes.getElement("BlueStar").entityDef
        with mock.patch.object(elem,"checkType",return_value=False):
            self.assertFalse(self.inst.checkTypeOrbitBodyType(self.mmod,
                    "BlueStar","Star"))

"""############################ Tests HomeUpgrades #########################"""

class HomeUpgradeTests(unit.TestCase):
    desc = "Tests HomeUpgrades:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "normalStartHomePlanetUpgradeLevel:Population 3\n"
            +"normalStartHomePlanetUpgradeLevel:CivilianModules 1\n"
            +"normalStartHomePlanetUpgradeLevel:TacticalModules 0\n"
            +"normalStartHomePlanetUpgradeLevel:Home 1\n"
            +"normalStartHomePlanetUpgradeLevel:ArtifactLevel 10\n"
            +"normalStartHomePlanetUpgradeLevel:Infrastructure 2\n"
        )
        cls.pop = {"identifier":"normalStartHomePlanetUpgradeLevel:Population",
            "val":3}
        cls.civ = {
            "identifier":"normalStartHomePlanetUpgradeLevel:CivilianModules",
            "val":1}
        cls.tac = {
            "identifier":"normalStartHomePlanetUpgradeLevel:TacticalModules",
            "val":0}
        cls.home = {"identifier":"normalStartHomePlanetUpgradeLevel:Home",
            "val":1}
        cls.artifact = {
            "identifier":"normalStartHomePlanetUpgradeLevel:ArtifactLevel",
            "val":10}
        cls.infrastructure = {
            "identifier":"normalStartHomePlanetUpgradeLevel:Infrastructure",
            "val":2
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.HomeUpgrades(
                pop=self.pop,
                civ=self.civ,
                tac=self.tac,
                home=self.home,
                artifact=self.artifact,
                infrastructure=self.infrastructure
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_homeUpgNormal.parseString(self.parseString)[0]
        res = scen.HomeUpgrades(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribPop(self):
        """ Test whether pop attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.pop)
        self.assertEqual(self.inst.pop,exp)

    def testCreationAttribCiv(self):
        """ Test whether civ attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.civ)
        self.assertEqual(self.inst.civ,exp)

    def testCreationAttribTac(self):
        """ Test whether tac attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.tac)
        self.assertEqual(self.inst.tac,exp)

    def testCreationAttribHome(self):
        """ Test whether home attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.home)
        self.assertEqual(self.inst.home,exp)

    def testCreationAttribArtifact(self):
        """ Test whether artifact attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.artifact)
        self.assertEqual(self.inst.artifact,exp)

    def testCreationAttribInfrastructure(self):
        """ Test whether infrastructure attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.infrastructure)
        self.assertEqual(self.inst.infrastructure,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""######################### Tests OrbitBodyDesignRef ######################"""

class OrbitBodyDesignRefTest(unit.TestCase):
    desc = "Tests OrbitBodyDesignRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.val = "testref"

    def setUp(self):
        self.inst = scen.OrbitBodyDesignRef(
                bodyType="Star",
                identifier=self.identifier,
                val=self.val
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = scen.OrbitBodyDesignRef(bodyType="Star",**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationInvalidBodyType(self):
        """ Test whether error is raised for invalid body type
        """
        with self.assertRaises(RuntimeError):
            scen.OrbitBodyDesignRef(
                    bodyType="Foo",
                    identifier=self.identifier,
                    val=self.val
            )

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        self.mmod.scenarios.galDef.starTypes.__contains__.return_value = True
        self.mmod.scenarios.galDef.planetTypes.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMalformedGalDef(self):
        """ Test whether check with malformed GalDef works
        """
        self.mmod.scenarios.galDef = []
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckStarType(self):
        """ Test whether check returns problem for missing star type design
        """
        self.mmod.scenarios.galDef.starTypes.__contains__.return_value = False
        self.mmod.scenarios.galDef.planetTypes.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.mmod.scenarios.galDef.starTypes.__contains__.assert_called_once_with(
                "testref")

    def testCheckPlanetType(self):
        """ Test whether check returns problem for missing planet type design
        """
        self.mmod.scenarios.galDef.starTypes.__contains__.return_value = False
        self.mmod.scenarios.galDef.planetTypes.__contains__.return_value = False
        self.inst.bodyType = "Planet"
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.mmod.scenarios.galDef.planetTypes.__contains__.assert_called_once_with(
                "testref")

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################ Tests MinMaxRadius #########################"""

class MinMaxRadiusTests(unit.TestCase):
    desc = "Tests MinMaxRadius:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "ident\n"
            +"\tminPercentage 0.100000\n"
            +"\tmaxPercentage 0.400000\n"
        )
        cls.identifier = "ident"
        cls.minPercent = {"identifier":"minPercentage","val":0.1}
        cls.maxPercent = {"identifier":"maxPercentage","val":0.4}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.MinMaxRadius(
                identifier=self.identifier,
                minPercent=self.minPercent,
                maxPercent=self.maxPercent
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genHeading("ident")
            +scen.g_minMaxPerc
        )
        parseRes = parser.parseString(self.parseString)
        res = scen.MinMaxRadius(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMinPercent(self):
        """ Test whether minPercent attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.minPercent)
        self.assertEqual(self.inst.minPercent,exp)

    def testCreationAttribMaxPercent(self):
        """ Test whether maxPercent attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxPercent)
        self.assertEqual(self.inst.maxPercent,exp)

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################ Tests PlanetGroup ##########################"""

class PlanetGroupTests(unit.TestCase):
    desc = "Tests PlanetGroup:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "planetGroup\n"
            +"\tminCount 1\n"
            +"\tmaxCount 2\n"
            +"\tplanetTypeCount 1\n"
            +"\tplanetType \"testtype\"\n"
        )
        cls.identifier = "planetGroup"
        cls.minCount = {"identifier":"minCount","val":1}
        cls.maxCount = {"identifier":"maxCount","val":2}
        cls.planetTypes = {
            "counter":{"identifier":"planetTypeCount","val":1},
            "elements":[
                {"identifier":"planetType","val":"testtype"}
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.PlanetGroup(
                identifier=self.identifier,
                minCount=self.minCount,
                maxCount=self.maxCount,
                planetTypes=self.planetTypes
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_planetGroup.parseString(self.parseString)[0]
        res = scen.PlanetGroup(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMinCount(self):
        """ Test whether minCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.minCount)
        self.assertEqual(self.inst.minCount,exp)

    def testCreationAttribMaxCount(self):
        """ Test whether maxCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxCount)
        self.assertEqual(self.inst.maxCount,exp)

    def testCreationAttribPlanetTypes(self):
        """ Test whether planetTypes attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.OrbitBodyDesignRef,
                elemArgs={"bodyType":"Planet"},
                **self.planetTypes
        )
        self.assertEqual(self.inst.planetTypes,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.planetTypes,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckPlanetTypes(self):
        """ Test whether check returns problems from planetTypes attrib
        """
        with mock.patch.object(self.inst.planetTypes,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""################################ Tests Ring #############################"""

class RingTests(unit.TestCase):
    desc = "Tests Ring:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "ring\n"
            +"\tstarRadiusRange\n"
            +"\t\tminPercentage 0.300000\n"
            +"\t\tmaxPercentage 0.700000\n"
            +"\tmilitiaColonizationPerc 0\n"
            +"\tplanetGroupCount 1\n"
             "\tplanetGroup\n"
            +"\t\tminCount 1\n"
            +"\t\tmaxCount 2\n"
            +"\t\tplanetTypeCount 1\n"
            +"\t\tplanetType \"testtype\"\n"
        )
        cls.identifier = "ring"
        cls.starDistance = {
            "identifier":"starRadiusRange",
            "minPercent":{"identifier":"minPercentage","val":0.3},
            "maxPercent":{"identifier":"maxPercentage","val":0.7}
        }
        cls.militiaColPerc = {"identifier":"militiaColonizationPerc","val":0}
        cls.planets = {
            "counter":{"identifier":"planetGroupCount","val":1},
            "elements":[
                {
                    "identifier":"planetGroup",
                    "minCount":{"identifier":"minCount","val":1},
                    "maxCount":{"identifier":"maxCount","val":2},
                    "planetTypes":{
                        "counter":{"identifier":"planetTypeCount","val":1},
                        "elements":[
                            {"identifier":"planetType","val":"testtype"}
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.Ring(
                identifier=self.identifier,
                starDistance=self.starDistance,
                militiaColPerc=self.militiaColPerc,
                planets=self.planets
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_ring.parseString(self.parseString)[0]
        res = scen.Ring(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribStarDistance(self):
        """ Test whether starDistance attrib is created correctly
        """
        exp = scen.MinMaxRadius(**self.starDistance)
        self.assertEqual(self.inst.starDistance,exp)

    def testCreationAttribMilitiaColPerc(self):
        """ Test whether militiaColPerc attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.militiaColPerc)
        self.assertEqual(self.inst.militiaColPerc,exp)

    def testCreationAttribPlanets(self):
        """ Test whether planets attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.PlanetGroup,**self.planets)
        self.assertEqual(self.inst.planets,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.planets,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckPlanets(self):
        """ Test whether check returns problems from planets attrib
        """
        with mock.patch.object(self.inst.planets,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests FixedOrbitBody ########################"""

class FixedOrbitBodyFixture(object):
    @classmethod
    def setUpClass(cls):
        cls.identifier = "star"
        cls.designName = {"identifier":"designName","val":"desname"}
        cls.ingameName = {"identifier":"inGameName","val":"ingamename"}
        cls.type = {"identifier":"type","val":"testtype"}
        cls.pos = {"identifier":"pos","coord":[1107,101]}
        cls.mmod = tco.genMockMod("./")

    def testCreationAttribDesignName(self):
        """ Test whether designName attrib is created correctly
        """
        exp = co_attribs.AttribString(**self.designName)
        self.assertEqual(self.inst.designName,exp)

    def testCreationAttribIngameName(self):
        """ Test whether ingameName attrib is created correctly
        """
        exp = co_attribs.AttribString(**self.ingameName)
        self.assertEqual(self.inst.ingameName,exp)

    def testCreationAttribType(self):
        """ Test whether type attrib is created correctly
        """
        exp = scen.OrbitBodyDesignRef(bodyType="Star",**self.type)
        self.assertEqual(self.inst.type,exp)

    def testCreationAttribPos(self):
        """ Test whether pos attrib is created correctly
        """
        exp = co_attribs.AttribPos2d(**self.pos)
        self.assertEqual(self.inst.pos,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.type,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

class FixedOrbitBodyTestsV4(FixedOrbitBodyFixture,unit.TestCase):
    desc = "Tests FixedOrbitBody v4:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "star\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [ 1107 , 101 ]\n"
            +"\tmoveAreaRadius 40\n"
            +"\thyperspaceExitRadius 30\n"
        )
        cls.moveRadius = {"identifier":"moveAreaRadius","val":40}
        cls.hyperRadius = {"identifier":"hyperspaceExitRadius","val":30}
        cls.patcher = mock.patch.multiple(scen.FixedOrbitBody,
                __abstractmethods__=set())
        cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.patcher.stop()

    def setUp(self):
        self.inst = scen.FixedOrbitBody(
                identifier=self.identifier,
                bodyType="Star",
                designName=self.designName,
                ingameName=self.ingameName,
                type=self.type,
                pos=self.pos,
                moveRadius=self.moveRadius,
                hyperRadius=self.hyperRadius,
                fileVersion=4
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genHeading("star")
            +scen.g_orbitBodyPreamble
            +co_parse.genDecimalAttrib("moveAreaRadius")("moveRadius")
            +co_parse.genDecimalAttrib("hyperspaceExitRadius")("hyperRadius")
        )
        parseRes = parser.parseString(self.parseString)
        res = scen.FixedOrbitBody(bodyType="Star",fileVersion=4,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMoveRadius(self):
        """ Test whether moveRadius attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.moveRadius)
        self.assertEqual(self.inst.moveRadius,exp)

    def testCreationAttribHyperRadius(self):
        """ Test whether hyperRadius attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.hyperRadius)
        self.assertEqual(self.inst.hyperRadius,exp)

    def testCheckType(self):
        """ Test whether check returns problems from type attrib
        """
        with mock.patch.object(self.inst.type,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

class FixedOrbitBodyTestsV5(FixedOrbitBodyFixture,unit.TestCase):
    desc = "Tests FixedOrbitBody v5:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "star\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [ 1107 , 101 ]\n"
        )
        cls.patcher = mock.patch.multiple(scen.FixedOrbitBody,
                __abstractmethods__=set())
        cls.patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.patcher.stop()

    def setUp(self):
        self.inst = scen.FixedOrbitBody(
                identifier=self.identifier,
                bodyType="Star",
                designName=self.designName,
                ingameName=self.ingameName,
                type=self.type,
                pos=self.pos,
                fileVersion=5
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genHeading("star")
            +scen.g_orbitBodyPreamble
        )
        parseRes = parser.parseString(self.parseString)
        res = scen.FixedOrbitBody(bodyType="Star",fileVersion=5,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribMoveRadius(self):
        """ Test whether moveRadius is absent
        """
        self.assertFalse(hasattr(self.inst,"moveRadius"))

    def testCreationAttribHyperRadius(self):
        """ Test whether hyperRadius is absent
        """
        self.assertFalse(hasattr(self.inst,"hyperRadius"))

    def testCheckType(self):
        """ Test whether check returns problems from type attrib
        """
        with mock.patch.object(self.inst.type,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

"""############################# Tests RandomStar ##########################"""

class RandomStarFixture(object):
    @classmethod
    def setUpClass(cls):
        cls.identifier = "star"
        cls.starType = {"identifier":"type","val":"testtype"}
        cls.radius = {"identifier":"radius","val":100}
        cls.connectionChance = {"identifier":"connectionChance","val":.75}
        cls.connectionRadius = {"identifier":"connectionStarRadiusRange",
            "val":.7}
        cls.maxPlayers = {"identifier":"maxPlayerCount","val":6}
        cls.planets = {
            "counter":{"identifier":"ringCount","val":1},
            "elements":[
                {
                    "identifier":"ring",
                    "starDistance":{
                        "identifier":"starRadiusRange",
                        "minPercent":{"identifier":"minPercentage","val":0.3},
                        "maxPercent":{"identifier":"maxPercentage","val":0.7}
                    },
                    "militiaColPerc":{"identifier":"militiaColonizationPerc",
                        "val":0},
                    "planets":{
                        "counter":{"identifier":"planetGroupCount","val":1},
                        "elements":[
                            {
                                "identifier":"planetGroup",
                                "minCount":{"identifier":"minCount","val":1},
                                "maxCount":{"identifier":"maxCount","val":2},
                                "planetTypes":{
                                    "counter":{"identifier":"planetTypeCount",
                                        "val":1},
                                    "elements":[
                                        {"identifier":"planetType",
                                            "val":"testtype"}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        patcher = mock.patch.object(self.inst.starType,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockType = patcher.start()
        patcher = mock.patch.object(self.inst.planets,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockPlanets = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationAttribStarType(self):
        """ Test whether starType instance is created correctly
        """
        exp = scen.OrbitBodyDesignRef(bodyType="Star",**self.starType)
        self.assertEqual(self.inst.starType,exp)

    def testCreationAttribRadius(self):
        """ Test whether radius instance is created correctly
        """
        exp = co_attribs.AttribNum(**self.radius)
        self.assertEqual(self.inst.radius,exp)

    def testCreationAttribConnectionRadius(self):
        """ Test whether connectionRadius instance is created correctly
        """
        exp = co_attribs.AttribNum(**self.connectionRadius)
        self.assertEqual(self.inst.connectionRadius,exp)

    def testCreationAttribConnectionChance(self):
        """ Test whether connectionChance instance is created correctly
        """
        exp = co_attribs.AttribNum(**self.connectionChance)
        self.assertEqual(self.inst.connectionChance,exp)

    def testCreationAttribMaxPlayers(self):
        """ Test whether maxPlayers instance is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxPlayers)
        self.assertEqual(self.inst.maxPlayers,exp)

    def testCreationAttribPlanets(self):
        """ Test whether planets instance is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.Ring,**self.planets)
        self.assertEqual(self.inst.planets,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckType(self):
        """ Test whether check returns problems from starType attrib
        """
        self.mockType.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockType.assert_called_once_with(self.mmod,False)

    def testCheckPlanets(self):
        """ Test whether check returns problems from planets attrib
        """
        self.mockPlanets.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockPlanets.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class RandomStarTestsV4(RandomStarFixture,unit.TestCase):
    desc = "Tests RandomStar v4:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "star\n"
            +"\ttype \"testtype\"\n"
            +"\tradius 100\n"
            +"\tmoveAreaRadius 60000.000000\n"
            +"\thyperspaceExitRadius 50000.000000\n"
            +"\tconnectionStarRadiusRange 0.700000\n"
            +"\tconnectionChance 0.750000\n"
            +"\tmaxPlayerCount 6\n"
            +"\tringCount 1\n"
            +"\tring\n"
            +"\t\tstarRadiusRange\n"
            +"\t\t\tminPercentage 0.300000\n"
            +"\t\t\tmaxPercentage 0.700000\n"
            +"\t\tmilitiaColonizationPerc 0\n"
            +"\t\tplanetGroupCount 1\n"
            +"\t\tplanetGroup\n"
            +"\t\t\tminCount 1\n"
            +"\t\t\tmaxCount 2\n"
            +"\t\t\tplanetTypeCount 1\n"
            +"\t\t\tplanetType \"testtype\"\n"
        )
        super().setUpClass()
        cls.hyperRadius = {"identifier":"hyperspaceExitRadius","val":50000.0}
        cls.moveRadius = {"identifier":"moveAreaRadius","val":60000.0}

    def setUp(self):
        self.inst = scen.RandomStar(
                identifier=self.identifier,
                starType=self.starType,
                radius=self.radius,
                moveRadius=self.moveRadius,
                hyperRadius=self.hyperRadius,
                connectionRadius=self.connectionRadius,
                connectionChance=self.connectionChance,
                maxPlayers=self.maxPlayers,
                planets=self.planets,
                fileVersion=4
        )
        super().setUp()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<scen.g_radii
        parseRes = scen.g_randomStar.parseString(self.parseString)[0]
        res = scen.RandomStar(fileVersion=4,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

    def testCreationAttribMoveRadius(self):
        """ Test whether moveRadius instance is created correctly
        """
        exp = co_attribs.AttribNum(**self.moveRadius)
        self.assertEqual(self.inst.moveRadius,exp)

    def testCreationAttribHyperRadius(self):
        """ Test whether hyperRadius instance is created correctly
        """
        exp = co_attribs.AttribNum(**self.hyperRadius)
        self.assertEqual(self.inst.hyperRadius,exp)

class RandomStarTestsV5(RandomStarFixture,unit.TestCase):
    desc = "Tests RandomStar v5:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "star\n"
            +"\ttype \"testtype\"\n"
            +"\tradius 100\n"
            +"\tconnectionStarRadiusRange 0.700000\n"
            +"\tconnectionChance 0.750000\n"
            +"\tmaxPlayerCount 6\n"
            +"\tringCount 1\n"
            +"\tring\n"
            +"\t\tstarRadiusRange\n"
            +"\t\t\tminPercentage 0.300000\n"
            +"\t\t\tmaxPercentage 0.700000\n"
            +"\t\tmilitiaColonizationPerc 0\n"
            +"\t\tplanetGroupCount 1\n"
            +"\t\tplanetGroup\n"
            +"\t\t\tminCount 1\n"
            +"\t\t\tmaxCount 2\n"
            +"\t\t\tplanetTypeCount 1\n"
            +"\t\t\tplanetType \"testtype\"\n"
        )
        super().setUpClass()

    def setUp(self):
        self.inst = scen.RandomStar(
                identifier=self.identifier,
                starType=self.starType,
                radius=self.radius,
                connectionRadius=self.connectionRadius,
                connectionChance=self.connectionChance,
                maxPlayers=self.maxPlayers,
                planets=self.planets,
                fileVersion=5
        )
        super().setUp()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<pp.Empty().leaveWhitespace()
        parseRes = scen.g_randomStar.parseString(self.parseString)[0]
        res = scen.RandomStar(fileVersion=5,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

    def testCreationAttribMoveRadius(self):
        """ Test whether moveRadius is absent
        """
        self.assertFalse(hasattr(self.inst,"moveRadius"))

    def testCreationAttribHyperRadius(self):
        """ Test whether hyperRadius instance is created correctly
        """
        self.assertFalse(hasattr(self.inst,"hyperRadius"))

"""############################ Tests PlayerParams #########################"""

class PlayerParamsTests(unit.TestCase):
    desc = "Tests PlayerParams:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "playerParams\n"
            +"\tstartingCredits 3000\n"
            +"\tstartingMetal 800\n"
            +"\tstartingCrystal 250\n"
            +"\thomePlanetType \"homeref\"\n"
            +"\thomePlanetStarRadiusRange\n"
            +"\t\tminPercentage 1\n"
            +"\t\tmaxPercentage 1.100000\n"
            +"\tareExtraPlanetsColonized FALSE\n"
            +"\textraPlanetsMaxRadius 15\n"
            +"\textraPlanetsRadiusRange\n"
            +"\t\tminPercentage 0.900000\n"
            +"\t\tmaxPercentage 1.000000\n"
            +"\textraPlanetGroupCount 1\n"
            +"\textraPlanetGroup\n"
            +"\t\tminCount 1\n"
            +"\t\tmaxCount 2\n"
            +"\t\tplanetTypeCount 1\n"
            +"\t\tplanetType \"testtype\"\n"
        )
        cls.identifier = "playerParams"
        cls.startCredits = {"identifier":"startingCredits","val":3000}
        cls.startMetal = {"identifier":"startingMetal","val":800}
        cls.startCrystal = {"identifier":"startingCrystal","val":250}
        cls.homePlanet = {"identifier":"homePlanetType","val":"homeref"}
        cls.homePlanetRadius = {
            "identifier":"homePlanetStarRadiusRange",
            "minPercent":{"identifier":"minPercentage","val":1},
            "maxPercent":{"identifier":"maxPercentage","val":1.1}
        }
        cls.extrasColonized = {"identifier":"areExtraPlanetsColonized",
            "val":False}
        cls.extrasRadiusMax = {"identifier":"extraPlanetsMaxRadius","val":15}
        cls.extrasRadiusRange = {
            "identifier":"extraPlanetsRadiusRange",
            "minPercent":{"identifier":"minPercentage","val":0.9},
            "maxPercent":{"identifier":"maxPercentage","val":1.0}
        }
        cls.extraPlanets = {
            "counter":{"identifier":"extraPlanetGroupCount","val":1},
            "elements":[
                {
                    "identifier":"extraPlanetGroup",
                    "minCount":{"identifier":"minCount","val":1},
                    "maxCount":{"identifier":"maxCount","val":2},
                    "planetTypes":{
                        "counter":{"identifier":"planetTypeCount","val":1},
                        "elements":[
                            {"identifier":"planetType","val":"testtype"}
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.PlayerParams(
                identifier=self.identifier,
                startCredits=self.startCredits,
                startMetal=self.startMetal,
                startCrystal=self.startCrystal,
                homePlanet=self.homePlanet,
                homePlanetRadius=self.homePlanetRadius,
                extrasColonized=self.extrasColonized,
                extrasRadiusMax=self.extrasRadiusMax,
                extrasRadiusRange=self.extrasRadiusRange,
                extraPlanets=self.extraPlanets
        )
        patcher = mock.patch.object(self.inst.homePlanet,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockHome = patcher.start()
        patcher = mock.patch.object(self.inst.extraPlanets,"check",
                autospec=True,
                spec_set=True,return_value=[])
        self.mockPlanets = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_playerParams.parseString(self.parseString)[0]
        res = scen.PlayerParams(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribStartCredits(self):
        """ Test whether startCredits attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.startCredits)
        self.assertEqual(self.inst.startCredits,exp)

    def testCreationAttribStartMetal(self):
        """ Test whether startMetal attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.startMetal)
        self.assertEqual(self.inst.startMetal,exp)

    def testCreationAttribStartCrystal(self):
        """ Test whether startCrystal attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.startCrystal)
        self.assertEqual(self.inst.startCrystal,exp)

    def testCreationAttribHomePlanet(self):
        """ Test whether homePlanet attrib is created correctly
        """
        exp = scen.OrbitBodyDesignRef(bodyType="Planet",**self.homePlanet)
        self.assertEqual(self.inst.homePlanet,exp)

    def testCreationAttribHomePlanetRadius(self):
        """ Test whether homePlanetRadius attrib is created correctly
        """
        exp = scen.MinMaxRadius(**self.homePlanetRadius)
        self.assertEqual(self.inst.homePlanetRadius,exp)

    def testCreationAttribExtrasColonized(self):
        """ Test whether extrasColonized attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.extrasColonized)
        self.assertEqual(self.inst.extrasColonized,exp)

    def testCreationAttribExtrasRadiusMax(self):
        """ Test whether extrasRadiusMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.extrasRadiusMax)
        self.assertEqual(self.inst.extrasRadiusMax,exp)

    def testCreationAttribExtrasRadiusRange(self):
        """ Test whether extrasRadiusRange attrib is created correctly
        """
        exp = scen.MinMaxRadius(**self.extrasRadiusRange)
        self.assertEqual(self.inst.extrasRadiusRange,exp)

    def testCreationAttribExtraPlanets(self):
        """ Test whether extraPlanets attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.PlanetGroup,**self.extraPlanets)
        self.assertEqual(self.inst.extraPlanets,exp)

    def testCheck(self):
        """ Test whether check retuns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckHomePlanet(self):
        """ Test whether check retuns problems from homePlanet attrib
        """
        self.mockHome.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockHome.assert_called_once_with(self.mmod,False)

    def testCheckExtraPlanets(self):
        """ Test whether check retuns problems from extraPlanets attrib
        """
        self.mockPlanets.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockPlanets.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests RandParams ##########################"""

class RandParamsTests(unit.TestCase):
    desc = "Tests RandParams:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "randomizerParams\n"
            +"\tstarPosOffsetRange\n"
            +"\t\tminPercentage 10.000000\n"
            +"\t\tmaxPercentage 15.000000\n"
            +"\tplayerParams\n"
            +"\t\tstartingCredits 3000\n"
            +"\t\tstartingMetal 800\n"
            +"\t\tstartingCrystal 250\n"
            +"\t\thomePlanetType \"homeref\"\n"
            +"\t\thomePlanetStarRadiusRange\n"
            +"\t\t\tminPercentage 1\n"
            +"\t\t\tmaxPercentage 1.100000\n"
            +"\t\tareExtraPlanetsColonized FALSE\n"
            +"\t\textraPlanetsMaxRadius 15\n"
            +"\t\textraPlanetsRadiusRange\n"
            +"\t\t\tminPercentage 0.900000\n"
            +"\t\t\tmaxPercentage 1.000000\n"
            +"\t\textraPlanetGroupCount 1\n"
            +"\t\textraPlanetGroup\n"
            +"\t\t\tminCount 1\n"
            +"\t\t\tmaxCount 2\n"
            +"\t\t\tplanetTypeCount 1\n"
            +"\t\t\tplanetType \"testtype\"\n"
        )
        cls.identifier = "randomizerParams"
        cls.starPosOffset = {
            "identifier":"starPosOffsetRange",
            "minPercent":{"identifier":"minPercentage","val":10.0},
            "maxPercent":{"identifier":"maxPercentage","val":15.0}
        }
        cls.playerParams = {
            "identifier":"playerParams",
            "startCredits":{"identifier":"startingCredits","val":3000},
            "startMetal":{"identifier":"startingMetal","val":800},
            "startCrystal":{"identifier":"startingCrystal","val":250},
            "homePlanet":{"identifier":"homePlanetType","val":"homeref"},
            "homePlanetRadius":{
                "identifier":"homePlanetStarRadiusRange",
                "minPercent":{"identifier":"minPercentage","val":1},
                "maxPercent":{"identifier":"maxPercentage","val":1.1}
            },
            "extrasColonized":{"identifier":"areExtraPlanetsColonized",
                "val":False},
            "extrasRadiusMax":{"identifier":"extraPlanetsMaxRadius","val":15},
            "extrasRadiusRange":{
                "identifier":"extraPlanetsRadiusRange",
                "minPercent":{"identifier":"minPercentage","val":0.9},
                "maxPercent":{"identifier":"maxPercentage","val":1.0}
            },
            "extraPlanets":{
                "counter":{"identifier":"extraPlanetGroupCount","val":1},
                "elements":[
                    {
                        "identifier":"extraPlanetGroup",
                        "minCount":{"identifier":"minCount","val":1},
                        "maxCount":{"identifier":"maxCount","val":2},
                        "planetTypes":{
                            "counter":{"identifier":"planetTypeCount","val":1},
                            "elements":[
                                {"identifier":"planetType","val":"testtype"}
                            ]
                        }
                    }
                ]
            }
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.RandParams(
                identifier=self.identifier,
                starPosOffset=self.starPosOffset,
                playerParams=self.playerParams
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_randParams.parseString(self.parseString)[0]
        res = scen.RandParams(**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribStarPosOffset(self):
        """ Test whether starPosOffset attrib is created correctly
        """
        exp = scen.MinMaxRadius(**self.starPosOffset)
        self.assertEqual(self.inst.starPosOffset,exp)

    def testCreationAttribPlayerParams(self):
        """ Test whether playerParams attrib is created correctly
        """
        exp = scen.PlayerParams(**self.playerParams)
        self.assertEqual(self.inst.playerParams,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.playerParams,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[])

    def testCheckPlayerParams(self):
        """ Test whether check returns problems from playerParams attrib
        """
        with mock.patch.object(self.inst.playerParams,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            self.assertEqual(res,[mock.sentinel.prob])
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################## Tests RandomCartography ######################"""

class RandomCartographyTests(unit.TestCase):
    desc = "Tests RandomCartography:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useRandomGenerator TRUE\n"
            +"randomizerParams\n"
            +"\tstarPosOffsetRange\n"
            +"\t\tminPercentage 10.000000\n"
            +"\t\tmaxPercentage 15.000000\n"
            +"\tplayerParams\n"
            +"\t\tstartingCredits 3000\n"
            +"\t\tstartingMetal 800\n"
            +"\t\tstartingCrystal 250\n"
            +"\t\thomePlanetType \"homeref\"\n"
            +"\t\thomePlanetStarRadiusRange\n"
            +"\t\t\tminPercentage 1\n"
            +"\t\t\tmaxPercentage 1.100000\n"
            +"\t\tareExtraPlanetsColonized FALSE\n"
            +"\t\textraPlanetsMaxRadius 15\n"
            +"\t\textraPlanetsRadiusRange\n"
            +"\t\t\tminPercentage 0.900000\n"
            +"\t\t\tmaxPercentage 1.000000\n"
            +"\t\textraPlanetGroupCount 1\n"
            +"\t\textraPlanetGroup\n"
            +"\t\t\tminCount 1\n"
            +"\t\t\tmaxCount 2\n"
            +"\t\t\tplanetTypeCount 1\n"
            +"\t\t\tplanetType \"testtype\"\n"
            +"\tstarCount 1\n"
            +"\tstar\n"
            +"\t\ttype \"testtype\"\n"
            +"\t\tradius 100\n"
            +"\t\tmoveAreaRadius 60000.000000\n"
            +"\t\thyperspaceExitRadius 50000.000000\n"
            +"\t\tconnectionStarRadiusRange 0.700000\n"
            +"\t\tconnectionChance 0.750000\n"
            +"\t\tmaxPlayerCount 6\n"
            +"\t\tringCount 1\n"
            +"\t\tring\n"
            +"\t\t\tstarRadiusRange\n"
            +"\t\t\t\tminPercentage 0.300000\n"
            +"\t\t\t\tmaxPercentage 0.700000\n"
            +"\t\t\tmilitiaColonizationPerc 0\n"
            +"\t\t\tplanetGroupCount 1\n"
            +"\t\t\tplanetGroup\n"
            +"\t\t\t\tminCount 1\n"
            +"\t\t\t\tmaxCount 2\n"
            +"\t\t\t\tplanetTypeCount 1\n"
            +"\t\t\t\tplanetType \"testtype\"\n"
        )
        cls.random = {"identifier":"useRandomGenerator","val":True}
        cls.randParams = {
            "identifier":"randomizerParams",
            "starPosOffset":{
                "identifier":"starPosOffsetRange",
                "minPercent":{"identifier":"minPercentage","val":10.0},
                "maxPercent":{"identifier":"maxPercentage","val":15.0}
            },
            "playerParams":{
                "identifier":"playerParams",
                "startCredits":{"identifier":"startingCredits","val":3000},
                "startMetal":{"identifier":"startingMetal","val":800},
                "startCrystal":{"identifier":"startingCrystal","val":250},
                "homePlanet":{"identifier":"homePlanetType","val":"homeref"},
                "homePlanetRadius":{
                    "identifier":"homePlanetStarRadiusRange",
                    "minPercent":{"identifier":"minPercentage","val":1},
                    "maxPercent":{"identifier":"maxPercentage","val":1.1}
                },
                "extrasColonized":{"identifier":"areExtraPlanetsColonized",
                    "val":False},
                "extrasRadiusMax":{"identifier":"extraPlanetsMaxRadius",
                    "val":15},
                "extrasRadiusRange":{
                    "identifier":"extraPlanetsRadiusRange",
                    "minPercent":{"identifier":"minPercentage","val":0.9},
                    "maxPercent":{"identifier":"maxPercentage","val":1.0}
                },
                "extraPlanets":{
                    "counter":{"identifier":"extraPlanetGroupCount","val":1},
                    "elements":[
                        {
                            "identifier":"extraPlanetGroup",
                            "minCount":{"identifier":"minCount","val":1},
                            "maxCount":{"identifier":"maxCount","val":2},
                            "planetTypes":{
                                "counter":{"identifier":"planetTypeCount",
                                    "val":1},
                                "elements":[
                                    {"identifier":"planetType","val":"testtype"}
                                ]
                            }
                        }
                    ]
                }
            }
        }
        cls.stars = {
            "counter":{"identifier":"starCount","val":1},
            "elements":[
                {
                    "identifier":"star",
                    "starType":{"identifier":"type","val":"testtype"},
                    "radius":{"identifier":"radius","val":100},
                    "moveRadius":{"identifier":"moveAreaRadius","val":60000.0},
                    "hyperRadius":{"identifier":"hyperspaceExitRadius",
                        "val":50000.0},
                    "connectionRadius":{
                        "identifier":"connectionStarRadiusRange",
                        "val":.7},
                    "connectionChance":{"identifier":"connectionChance",
                        "val":.75},
                    "maxPlayers":{"identifier":"maxPlayerCount","val":6},
                    "planets":{
                        "counter":{"identifier":"ringCount","val":1},
                        "elements":[
                            {
                                "identifier":"ring",
                                "starDistance":{
                                    "identifier":"starRadiusRange",
                                    "minPercent":{"identifier":"minPercentage",
                                        "val":0.3},
                                    "maxPercent":{"identifier":"maxPercentage",
                                        "val":0.7}
                                },
                                "militiaColPerc":{
                                    "identifier":"militiaColonizationPerc",
                                    "val":0},
                                "planets":{
                                    "counter":{"identifier":"planetGroupCount",
                                        "val":1},
                                    "elements":[
                                        {
                                            "identifier":"planetGroup",
                                            "minCount":{"identifier":"minCount",
                                                "val":1},
                                            "maxCount":{"identifier":"maxCount",
                                                "val":2},
                                            "planetTypes":{
                                                "counter":{
                                                    "identifier":"planetTypeCount",
                                                    "val":1},
                                                "elements":[
                                                    {"identifier":"planetType",
                                                        "val":"testtype"}
                                                ]
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.RandomCartography(
                random=self.random,
                randParams=self.randParams,
                stars=self.stars,
                fileVersion=4
        )
        patcher = mock.patch.object(self.inst.randParams,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockParams = patcher.start()
        patcher = mock.patch.object(self.inst.stars,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockStars = patcher.start()
        self.addCleanup(mock.patch.stopall)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<scen.g_radii
        parent = mock.MagicMock(version=mock.Mock(value=4))
        parent.mock_add_spec(spec=scen.Galaxy)
        parseRes = scen.g_randomCartography.parseString(self.parseString)[0]
        res = scen.Cartography.factory(parent=parent,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

    def testCreationAttribRandParams(self):
        """ Test whether randParams attrib is created correctly
        """
        exp = scen.RandParams(**self.randParams)
        self.assertEqual(self.inst.randParams,exp)

    def testCreationAttribStars(self):
        """ Test whether stars attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.RandomStar,
                elemArgs={"fileVersion":4},**self.stars)
        self.assertEqual(self.inst.stars,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckRandParams(self):
        """ Test whether check returns problems from randParams attribute
        """
        self.mockParams.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockParams.assert_called_once_with(self.mmod,False)

    def testCheckStars(self):
        """ Test whether check returns problems from stars attribute
        """
        self.mockStars.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockStars.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests PlayerDesignRef #######################"""

class PlayerDesignRefTests(unit.TestCase):
    desc = "Tests PlayerDesignRef:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.ref = "testref"
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.parent = mock.MagicMock(cartography=mock.MagicMock())
        self.parent.cartography.players = mock.MagicMock()
        self.parent.cartography.players.__contains__ = mock.MagicMock(
                return_value=True)
        self.parent.mock_add_spec(scen.Galaxy,spec_set=True)
        self.inst = scen.PlayerDesignRef(
                parent=self.parent,
                identifier=self.identifier,
                val=self.ref)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = scen.PlayerDesignRef(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationWrongParent(self):
        """ Test whether error is raised when parent is wrong type
        """
        mocked = mock.MagicMock()
        with self.assertRaises(RuntimeError):
            scen.PlayerDesignRef(
                    parent=mocked,
                    identifier=self.identifier,
                    val=self.ref
            )

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckMissing(self):
        """ Test whether MissingRef problem is returned when player is missing
        """
        self.parent.cartography.players.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)

"""############################ Tests FixedPlanet ##########################"""

class FixedPlanetFixture(FixedOrbitBodyFixture):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.identifier = "planet"
        cls.designName = {"identifier":"designName","val":"desname"}
        cls.ingameName = {"identifier":"inGameName","val":"ingamename"}
        cls.type = {"identifier":"type","val":"testtype"}
        cls.pos = {"identifier":"pos","coord":[1107,101]}
        cls.owner = {"identifier":"owner","val":"player1"}
        cls.isHome = {"identifier":"isHomePlanet","val":True}
        cls.normalUpg = {
            "pop":{"identifier":"normalStartUpgradeLevelForPopulation",
                "val":3},
            "civ":{"identifier":"normalStartUpgradeLevelForCivilianModules",
                "val":1},
            "tac":{"identifier":"normalStartUpgradeLevelForTacticalModules",
                "val":0},
            "artifact":{"identifier":"normalStartUpgradeLevelForArtifacts",
                "val":10},
            "infrastructure":{
                "identifier":"normalStartUpgradeLevelForInfrastructure",
                "val":2}
        }
        cls.quickUpg = {
            "pop":{"identifier":"quickStartUpgradeLevelForPopulation",
                "val":3},
            "civ":{"identifier":"quickStartUpgradeLevelForCivilianModules",
                "val":1},
            "tac":{"identifier":"quickStartUpgradeLevelForTacticalModules",
                "val":0},
            "artifact":{"identifier":"quickStartUpgradeLevelForArtifacts",
                "val":10},
            "infrastructure":{
                "identifier":"quickStartUpgradeLevelForInfrastructure",
                "val":2}
        }
        cls.items = {
            "identifier":"planetItems",
            "name":{"identifier":"templateName","val":"Template:Test"},
            "subTemplates":{
                "counter":{"identifier":"subTemplates","val":2},
                "elements":[
                    {"identifier":"template","val":"Template:Sub1"},
                    {"identifier":"template","val":"Template:Sub2"}
                ]
            },
            "groups":{
                "counter":{"identifier":"groups","val":2},
                "elements":[
                    {
                        "identifier":"group",
                        "condition":{
                            "identifier":"condition",
                            "condType":{"identifier":"type","val":"Always"},
                            "param":{"identifier":"param","val":"Test Param"}
                        },
                        "owner":{"identifier":"owner","val":"PlanetOwner"},
                        "colonizeChance":{"identifier":"colonizeChance",
                            "val":1},
                        "items":{
                            "counter":{"identifier":"items","val":2},
                            "elements":[
                                {"identifier":"item","val":"item 1"},
                                {"identifier":"item","val":"item 2"}
                            ]
                        }
                    },
                    {
                        "identifier":"group",
                        "condition":{
                            "identifier":"condition",
                            "condType":{"identifier":"type","val":"Always"},
                            "param":{"identifier":"param","val":"Test Param2"}
                        },
                        "owner":{"identifier":"owner","val":"PlanetOwner"},
                        "colonizeChance":{"identifier":"colonizeChance",
                            "val":1},
                        "items":{
                            "counter":{"identifier":"items","val":2},
                            "elements":[
                                {"identifier":"item","val":"item 4"},
                                {"identifier":"item","val":"item 5"}
                            ]
                        }
                    }
                ]
            }
        }
        cls.spawnProb = {"identifier":"spawnProbability","val":1.0}
        cls.useDefTemplate = {"identifier":"useDefaultTemplate","val":True}
        cls.entityCount = {"identifier":"entityCount","val":0}
        cls.asteroidCount = {"identifier":"asteroidCount","val":0}

    def setUp(self):
        patcher = mock.patch.object(self.inst.type,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockType = patcher.start()
        patcher = mock.patch.object(self.inst.owner,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockOwner = patcher.start()
        patcher = mock.patch.object(self.inst.items,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockItems = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationAttribOwner(self):
        """ Test whether owner attribute is created correctly
        """
        exp = scen.PlayerDesignRef(parent=self.parent,**self.owner)
        self.assertEqual(self.inst.owner,exp)

    def testCreationAttribIsHome(self):
        """ Test whether isHome attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isHome)
        self.assertEqual(self.inst.isHome,exp)

    def testCreationAttribNormalUpg(self):
        """ Test whether normalUpg attribute is created correctly
        """
        exp = scen.HomeUpgrades(**self.normalUpg)
        self.assertEqual(self.inst.normalUpg,exp)

    def testCreationAttribQuickUpg(self):
        """ Test whether quickUpg attribute is created correctly
        """
        exp = scen.HomeUpgrades(**self.quickUpg)
        self.assertEqual(self.inst.quickUpg,exp)

    def testCreationAttribItems(self):
        """ Test whether items attribute is created correctly
        """
        exp = scen.ScenarioTemplate(parent=self.parent,**self.items)
        self.assertEqual(self.inst.items,exp)

    def testCreationAttribSpawnProb(self):
        """ Test whether spawnProb attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnProb)
        self.assertEqual(self.inst.spawnProb,exp)

    def testCreationAttribUseDefTemplate(self):
        """ Test whether useDefTemplate attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.useDefTemplate)
        self.assertEqual(self.inst.useDefTemplate,exp)

    def testCreationAttribEntityCount(self):
        """ Test whether entityCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.entityCount)
        self.assertEqual(self.inst.entityCount,exp)

    def testCreationAttribAsteroidCount(self):
        """ Test whether asteroidCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.asteroidCount)
        self.assertEqual(self.inst.asteroidCount,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckOwner(self):
        """ Test whether check returns problems from owner attrib
        """
        self.mockOwner.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockOwner.assert_called_once_with(self.mmod,False)

    def testCheckItems(self):
        """ Test whether check returns problems from items attrib
        """
        self.mockItems.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockItems.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class FixedPlanetTestV4(FixedPlanetFixture,unit.TestCase):
    desc = "Tests FixedPlanet v4:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "planet\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [1107,101]\n"
            +"\tmoveAreaRadius 40\n"
            +"\thyperspaceExitRadius 30\n"
            +"\towner \"player1\"\n"
            +"\tisHomePlanet TRUE\n"
            +"\tnormalStartUpgradeLevelForPopulation 3\n"
            +"\tnormalStartUpgradeLevelForCivilianModules 1\n"
            +"\tnormalStartUpgradeLevelForTacticalModules 0\n"
            +"\tnormalStartUpgradeLevelForArtifacts 10\n"
            +"\tnormalStartUpgradeLevelForInfrastructure 2\n"
            +"\tquickStartUpgradeLevelForPopulation 3\n"
            +"\tquickStartUpgradeLevelForCivilianModules 1\n"
            +"\tquickStartUpgradeLevelForTacticalModules 0\n"
            +"\tquickStartUpgradeLevelForArtifacts 10\n"
            +"\tquickStartUpgradeLevelForInfrastructure 2\n"
            +"\tplanetItems\n"
            +"\t\ttemplateName \"Template:Test\"\n"
            +"\t\tsubTemplates 2\n"
            +"\t\ttemplate \"Template:Sub1\"\n"
            +"\t\ttemplate \"Template:Sub2\"\n"
            +"\t\tgroups 2\n"
            +"\t\tgroup\n"
            +"\t\t\tcondition\n"
            +"\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\tparam \"Test Param\"\n"
            +"\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\tcolonizeChance 1\n"
            +"\t\t\titems 2\n"
            +"\t\t\titem \"item 1\"\n"
            +"\t\t\titem \"item 2\"\n"
            +"\t\tgroup\n"
            +"\t\t\tcondition\n"
            +"\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\tparam \"Test Param2\"\n"
            +"\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\tcolonizeChance 1\n"
            +"\t\t\titems 2\n"
            +"\t\t\titem \"item 4\"\n"
            +"\t\t\titem \"item 5\"\n"
            +"\tspawnProbability 1.000000\n"
            +"\tuseDefaultTemplate TRUE\n"
            +"\tentityCount 0\n"
            +"\tasteroidCount 0\n"
        )
        cls.moveRadius = {"identifier":"moveAreaRadius","val":40}
        cls.hyperRadius = {"identifier":"hyperspaceExitRadius","val":30}

    def setUp(self):
        self.parent = mock.MagicMock(version=mock.Mock(value=4))
        self.parent.mock_add_spec(spec=scen.Galaxy)
        self.inst = scen.FixedPlanet(
                identifier=self.identifier,
                parent=self.parent,
                designName=self.designName,
                ingameName=self.ingameName,
                type=self.type,
                pos=self.pos,
                moveRadius=self.moveRadius,
                hyperRadius=self.hyperRadius,
                owner=self.owner,
                isHome=self.isHome,
                normalUpg=self.normalUpg,
                quickUpg=self.quickUpg,
                items=self.items,
                spawnProb=self.spawnProb,
                useDefTemplate=self.useDefTemplate,
                entityCount=self.entityCount,
                asteroidCount=self.asteroidCount
        )
        super().setUp()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<scen.g_radii
        parseRes = scen.g_fixedPlanet.parseString(self.parseString)[0]
        res = scen.FixedPlanet(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

class FixedPlanetTestV5(FixedPlanetFixture,unit.TestCase):
    desc = "Tests FixedPlanet v5:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "planet\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [1107,101]\n"
            +"\towner \"player1\"\n"
            +"\tisHomePlanet TRUE\n"
            +"\tnormalStartUpgradeLevelForPopulation 3\n"
            +"\tnormalStartUpgradeLevelForCivilianModules 1\n"
            +"\tnormalStartUpgradeLevelForTacticalModules 0\n"
            +"\tnormalStartUpgradeLevelForArtifacts 10\n"
            +"\tnormalStartUpgradeLevelForInfrastructure 2\n"
            +"\tquickStartUpgradeLevelForPopulation 3\n"
            +"\tquickStartUpgradeLevelForCivilianModules 1\n"
            +"\tquickStartUpgradeLevelForTacticalModules 0\n"
            +"\tquickStartUpgradeLevelForArtifacts 10\n"
            +"\tquickStartUpgradeLevelForInfrastructure 2\n"
            +"\tplanetItems\n"
            +"\t\ttemplateName \"Template:Test\"\n"
            +"\t\tsubTemplates 2\n"
            +"\t\ttemplate \"Template:Sub1\"\n"
            +"\t\ttemplate \"Template:Sub2\"\n"
            +"\t\tgroups 2\n"
            +"\t\tgroup\n"
            +"\t\t\tcondition\n"
            +"\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\tparam \"Test Param\"\n"
            +"\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\tcolonizeChance 1\n"
            +"\t\t\titems 2\n"
            +"\t\t\titem \"item 1\"\n"
            +"\t\t\titem \"item 2\"\n"
            +"\t\tgroup\n"
            +"\t\t\tcondition\n"
            +"\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\tparam \"Test Param2\"\n"
            +"\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\tcolonizeChance 1\n"
            +"\t\t\titems 2\n"
            +"\t\t\titem \"item 4\"\n"
            +"\t\t\titem \"item 5\"\n"
            +"\tspawnProbability 1.000000\n"
            +"\tuseDefaultTemplate TRUE\n"
            +"\tentityCount 0\n"
            +"\tasteroidCount 0\n"
        )

    def setUp(self):
        self.parent = mock.MagicMock(version=mock.Mock(value=5))
        self.parent.mock_add_spec(spec=scen.Galaxy)
        self.inst = scen.FixedPlanet(
                identifier=self.identifier,
                parent=self.parent,
                designName=self.designName,
                ingameName=self.ingameName,
                type=self.type,
                pos=self.pos,
                owner=self.owner,
                isHome=self.isHome,
                normalUpg=self.normalUpg,
                quickUpg=self.quickUpg,
                items=self.items,
                spawnProb=self.spawnProb,
                useDefTemplate=self.useDefTemplate,
                entityCount=self.entityCount,
                asteroidCount=self.asteroidCount
        )
        super().setUp()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<pp.Empty().leaveWhitespace()
        parseRes = scen.g_fixedPlanet.parseString(self.parseString)[0]
        res = scen.FixedPlanet(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

"""########################## Tests PlanetConnection #######################"""

class PlanetConnectionTests(unit.TestCase):
    desc = "Tests PlanetConnection:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "connection\n"
            +"\tplanetIndexA 2\n"
            +"\tplanetIndexB 0\n"
            +"\tspawnProbability 1.000000\n"
            +"\ttype \"PhaseLane\"\n"
        )
        cls.identifier = "connection"
        cls.indexA = {"identifier":"planetIndexA","val":2}
        cls.indexB = {"identifier":"planetIndexB","val":0}
        cls.spawnProb = {"identifier":"spawnProbability","val":1.0}
        cls.connType = {"identifier":"type","val":"PhaseLane"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.star = mock.MagicMock()
        self.star.designName = mock.MagicMock()
        self.star.designName.value = "mystar"
        self.star.mock_add_spec(scen.FixedStar)
        self.star.planets.counter = 3
        self.inst = scen.PlanetConnection(
                star=self.star,
                identifier=self.identifier,
                indexA=self.indexA,
                indexB=self.indexB,
                spawnProb=self.spawnProb,
                connType=self.connType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_gravWellConnection.parseString(self.parseString)[0]
        res = scen.PlanetConnection(star=self.star,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribIndexA(self):
        """ Test whether indexA attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.indexA)
        self.assertEqual(self.inst.indexA,exp)

    def testCreationAttribIndexB(self):
        """ Test whether indexB attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.indexB)
        self.assertEqual(self.inst.indexB,exp)

    def testCreationAttribSpawnProb(self):
        """ Test whether spawnProb attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnProb)
        self.assertEqual(self.inst.spawnProb,exp)

    def testCreationAttribConnType(self):
        """ Test whether connType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=scen.CONNECTION_TYPES,**self.connType)
        self.assertEqual(self.inst.connType,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckEqualIndices(self):
        """ Test whether check returns problem for similar indices
        """
        expMsg = ("planetIndexA==planetIndexB, planet connected to itself in "
                  "star mystar.")
        inst1 = scen.PlanetConnection(
                star=self.star,
                identifier=self.identifier,
                indexA=self.indexA,
                indexB=self.indexA,
                spawnProb=self.spawnProb,
                connType=self.connType
        )
        res = inst1.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testCheckOutOfBoundsIndexA(self):
        """ Test whether check returns problem indexA out of bounds
        """
        expMsg = ("Index out of bounds: planetIndexA=2 is larger than 1, the "
                  "number of planets-1 at the star mystar.")
        self.star.planets.counter = 2
        inst1 = scen.PlanetConnection(
                star=self.star,
                identifier=self.identifier,
                indexA=self.indexA,
                indexB=self.indexB,
                spawnProb=self.spawnProb,
                connType=self.connType
        )
        res = inst1.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testCheckOutOfBoundsIndexB(self):
        """ Test whether check returns problem indexB out of bounds
        """
        expMsg = ("Index out of bounds: planetIndexB=4 is larger than 2, the "
                  "number of planets-1 at the star mystar.")
        index = {"identifier":"planetIndexB","val":4}
        inst1 = scen.PlanetConnection(
                star=self.star,
                identifier=self.identifier,
                indexA=self.indexA,
                indexB=index,
                spawnProb=self.spawnProb,
                connType=self.connType
        )
        res = inst1.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""########################### Tests StarConnection ########################"""

class StarConnectionTests(unit.TestCase):
    desc = "Tests StarConnection:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "interStarConnection\n"
            +"\tstarIndexA 12\n"
            +"\tplanetIndexA 3\n"
            +"\tstarIndexB 7\n"
            +"\tplanetIndexB 0\n"
            +"\tspawnProbability 1.000000\n"
            +"\ttype \"Wormhole\"\n"
        )
        cls.identifier = "interStarConnection"
        cls.starA = {"identifier":"starIndexA","val":12}
        cls.planetA = {"identifier":"planetIndexA","val":3}
        cls.starB = {"identifier":"starIndexB","val":7}
        cls.planetB = {"identifier":"planetIndexB","val":0}
        cls.spawnProb = {"identifier":"spawnProbability","val":1.0}
        cls.connType = {"identifier":"type","val":"Wormhole"}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.mockStar = mock.MagicMock(name="Foo")
        self.mockStar.planets.counter = 4
        self.mockStar.designName = mock.MagicMock()
        self.mockStar.designName.value = "mystar"
        self.mockStar.mock_add_spec(spec=scen.FixedStar,spec_set=True)
        mockStars = mock.MagicMock(spec_set=co_attribs.AttribList)
        mockStars.counter = 13
        mockStars.elements.__getitem__ = mock.MagicMock()
        mockStars.elements.__getitem__.return_value = self.mockStar
        self.mockCartography = mock.MagicMock(spec_set=scen.FixedCartography)
        self.mockCartography.stars = mockStars
        self.parent = mock.MagicMock()
        self.parent.cartography = self.mockCartography
        self.inst = scen.StarConnection(
                identifier=self.identifier,
                parent=self.parent,
                starA=self.starA,
                planetA=self.planetA,
                starB=self.starB,
                planetB=self.planetB,
                spawnProb=self.spawnProb,
                connType=self.connType
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = scen.g_starConnection.parseString(self.parseString)[0]
        res = scen.StarConnection(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)

    def testCreationAttribStarA(self):
        """ Test whether attrib starA is created correctly
        """
        exp = co_attribs.AttribNum(**self.starA)
        self.assertEqual(self.inst.starA,exp)

    def testCreationAttribStarB(self):
        """ Test whether attrib starB is created correctly
        """
        exp = co_attribs.AttribNum(**self.starB)
        self.assertEqual(self.inst.starB,exp)

    def testCreationAttribPlanetA(self):
        """ Test whether attrib planetA is created correctly
        """
        exp = co_attribs.AttribNum(**self.planetA)
        self.assertEqual(self.inst.planetA,exp)

    def testCreationAttribPlanetB(self):
        """ Test whether attrib planetB is created correctly
        """
        exp = co_attribs.AttribNum(**self.planetB)
        self.assertEqual(self.inst.planetB,exp)

    def testCreationAttribSpawnProb(self):
        """ Test whether attrib spawnProb is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnProb)
        self.assertEqual(self.inst.spawnProb,exp)

    def testCreationAttribConnType(self):
        """ Test whether attrib connType is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=scen.CONNECTION_TYPES,**self.connType)
        self.assertEqual(self.inst.connType,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckSameStars(self):
        """ Test whether check returns problem when the same star is referenced
        """
        expMsg = scen.StarConnection._selfConnMsg.format(12,12)
        self.inst.starB.value = 12
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testCheckInvalidStarA(self):
        """ Test whether check returns problem when starA out of bounds
        """
        expMsg = scen.StarConnection._starOutOfBoundsMsg.format("starIndexA",
                13,12)
        self.inst.starA.value = 13
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testCheckInvalidStarB(self):
        """ Test whether check returns problem when starB out of bounds
        """
        expMsg = scen.StarConnection._starOutOfBoundsMsg.format("starIndexB",
                13,12)
        self.inst.starB.value = 13
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testCheckInvalidPlanetA(self):
        """ Test whether check returns problem when planetA out of bounds
        """
        expMsg = scen.StarConnection._planetOutOfBoundsMsg.format(
                "planetIndexA",
                4,3,"mystar")
        self.inst.planetA.value = 4
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testCheckInvalidPlanetB(self):
        """ Test whether check returns problem when planetB out of bounds
        """
        expMsg = scen.StarConnection._planetOutOfBoundsMsg.format(
                "planetIndexB",
                4,3,"mystar")
        self.inst.planetB.value = 4
        res = self.inst.check(self.mmod)
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.BasicProblem)
        self.assertEqual(res[0].desc,expMsg)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################# Tests FixedStar ###########################"""

class FixedStarFixture(FixedOrbitBodyFixture):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.identifier = "star"
        cls.radius = {"identifier":"radius","val":100.0}
        cls.planets = {
            "counter":{"identifier":"planetCount","val":1},
            "elements":[
                {
                    "identifier":"planet",
                    "designName":{"identifier":"designName","val":"desname"},
                    "ingameName":{"identifier":"inGameName","val":"ingamename"},
                    "type":{"identifier":"type","val":"testtype"},
                    "pos":{"identifier":"pos","coord":[1107,101]},
                    "moveRadius":{"identifier":"moveAreaRadius","val":40},
                    "hyperRadius":{"identifier":"hyperspaceExitRadius",
                        "val":30},
                    "owner":{"identifier":"owner","val":"player1"},
                    "isHome":{"identifier":"isHomePlanet","val":True},
                    "normalUpg":{
                        "pop":{
                            "identifier":"normalStartUpgradeLevelForPopulation",
                            "val":3},
                        "civ":{
                            "identifier":"normalStartUpgradeLevelForCivilianModules",
                            "val":1},
                        "tac":{
                            "identifier":"normalStartUpgradeLevelForTacticalModules",
                            "val":0},
                        "artifact":{
                            "identifier":"normalStartUpgradeLevelForArtifacts",
                            "val":10},
                        "infrastructure":{
                            "identifier":"normalStartUpgradeLevelForInfrastructure",
                            "val":2}
                    },
                    "quickUpg":{
                        "pop":{
                            "identifier":"quickStartUpgradeLevelForPopulation",
                            "val":3},
                        "civ":{
                            "identifier":"quickStartUpgradeLevelForCivilianModules",
                            "val":1},
                        "tac":{
                            "identifier":"quickStartUpgradeLevelForTacticalModules",
                            "val":0},
                        "artifact":{
                            "identifier":"quickStartUpgradeLevelForArtifacts",
                            "val":10},
                        "infrastructure":{
                            "identifier":"quickStartUpgradeLevelForInfrastructure",
                            "val":2}
                    },
                    "items":{
                        "identifier":"planetItems",
                        "name":{"identifier":"templateName",
                            "val":"Template:Test"},
                        "subTemplates":{
                            "counter":{"identifier":"subTemplates","val":2},
                            "elements":[
                                {"identifier":"template","val":"Template:Sub1"},
                                {"identifier":"template","val":"Template:Sub2"}
                            ]
                        },
                        "groups":{
                            "counter":{"identifier":"groups","val":2},
                            "elements":[
                                {
                                    "identifier":"group",
                                    "condition":{
                                        "identifier":"condition",
                                        "condType":{"identifier":"type",
                                            "val":"Always"},
                                        "param":{"identifier":"param",
                                            "val":"Test Param"}
                                    },
                                    "owner":{"identifier":"owner",
                                        "val":"PlanetOwner"},
                                    "colonizeChance":{
                                        "identifier":"colonizeChance","val":1},
                                    "items":{
                                        "counter":{"identifier":"items",
                                            "val":2},
                                        "elements":[
                                            {"identifier":"item",
                                                "val":"item 1"},
                                            {"identifier":"item","val":"item 2"}
                                        ]
                                    }
                                },
                                {
                                    "identifier":"group",
                                    "condition":{
                                        "identifier":"condition",
                                        "condType":{"identifier":"type",
                                            "val":"Always"},
                                        "param":{"identifier":"param",
                                            "val":"Test Param2"}
                                    },
                                    "owner":{"identifier":"owner",
                                        "val":"PlanetOwner"},
                                    "colonizeChance":{
                                        "identifier":"colonizeChance","val":1},
                                    "items":{
                                        "counter":{"identifier":"items",
                                            "val":2},
                                        "elements":[
                                            {"identifier":"item",
                                                "val":"item 4"},
                                            {"identifier":"item","val":"item 5"}
                                        ]
                                    }
                                }
                            ]
                        }
                    },
                    "spawnProb":{"identifier":"spawnProbability","val":1.0},
                    "useDefTemplate":{"identifier":"useDefaultTemplate",
                        "val":True},
                    "entityCount":{"identifier":"entityCount","val":0},
                    "asteroidCount":{"identifier":"asteroidCount","val":0}
                }
            ]
        }
        cls.connections = {
            "counter":{"identifier":"connectionCount","val":1},
            "elements":[
                {
                    "identifier":"connection",
                    "indexA":{"identifier":"planetIndexA","val":2},
                    "indexB":{"identifier":"planetIndexB","val":0},
                    "spawnProb":{"identifier":"spawnProbability","val":1.0},
                    "connType":{"identifier":"type","val":"PhaseLane"}
                }
            ]
        }
        cls.entityCount = {"identifier":"entityCount","val":0}
        cls.spawnProb = {"identifier":"spawnProbability","val":1.0}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        patcher = mock.patch.object(self.inst._planets,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockPlanets = patcher.start()
        patcher = mock.patch.object(self.inst.connections,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockConns = patcher.start()
        patcher = mock.patch.object(self.inst.type,"check",autospec=True,
                spec_set=True,return_value=[])
        patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationAttribRadius(self):
        """ Test whether radius attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.radius)
        self.assertEqual(self.inst.radius,exp)

    def testCreationAttribPlanets(self):
        """ Test whether planets attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.FixedPlanet,
                elemArgs={"parent":self.parent},**self.planets)
        self.assertEqual(self.inst.planets,exp)

    def testCreationAttribConnections(self):
        """ Test whether connections attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=scen.PlanetConnection,
                elemArgs={"star":self.inst},**self.connections)
        self.assertEqual(self.inst.connections,exp)

    def testCreationAttribEntityCount(self):
        """ Test whether entityCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.entityCount)
        self.assertEqual(self.inst.entityCount,exp)

    def testCreationAttribSpawnProb(self):
        """ Test whether spawnProb attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnProb)
        self.assertEqual(self.inst.spawnProb,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckPlanets(self):
        """ Test whether check returns problems from planets attrib
        """
        self.mockPlanets.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockPlanets.assert_called_once_with(self.mmod,False)

    def testCheckConnections(self):
        """ Test whether check returns problems from connections attrib
        """
        self.mockConns.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockConns.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class FixedStarTestsV4(FixedStarFixture,unit.TestCase):
    desc = "Tests FixedStarV4:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "star\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [1107,101]\n"
            +"\tradius 100.000000\n"
            +"\tmoveAreaRadius 40\n"
            +"\thyperspaceExitRadius 30\n"
            +"\tplanetCount 1\n"
            +"\tplanet\n"
            +"\t\tdesignName \"desname\"\n"
            +"\t\tinGameName \"ingamename\"\n"
            +"\t\ttype \"testtype\"\n"
            +"\t\tpos [1107,101]\n"
            +"\t\tmoveAreaRadius 40\n"
            +"\t\thyperspaceExitRadius 30\n"
            +"\t\towner \"player1\"\n"
            +"\t\tisHomePlanet TRUE\n"
            +"\t\tnormalStartUpgradeLevelForPopulation 3\n"
            +"\t\tnormalStartUpgradeLevelForCivilianModules 1\n"
            +"\t\tnormalStartUpgradeLevelForTacticalModules 0\n"
            +"\t\tnormalStartUpgradeLevelForArtifacts 10\n"
            +"\t\tnormalStartUpgradeLevelForInfrastructure 2\n"
            +"\t\tquickStartUpgradeLevelForPopulation 3\n"
            +"\t\tquickStartUpgradeLevelForCivilianModules 1\n"
            +"\t\tquickStartUpgradeLevelForTacticalModules 0\n"
            +"\t\tquickStartUpgradeLevelForArtifacts 10\n"
            +"\t\tquickStartUpgradeLevelForInfrastructure 2\n"
            +"\t\tplanetItems\n"
            +"\t\t\ttemplateName \"Template:Test\"\n"
            +"\t\t\tsubTemplates 2\n"
            +"\t\t\ttemplate \"Template:Sub1\"\n"
            +"\t\t\ttemplate \"Template:Sub2\"\n"
            +"\t\t\tgroups 2\n"
            +"\t\t\tgroup\n"
            +"\t\t\t\tcondition\n"
            +"\t\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\t\tparam \"Test Param\"\n"
            +"\t\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\t\tcolonizeChance 1\n"
            +"\t\t\t\titems 2\n"
            +"\t\t\t\titem \"item 1\"\n"
            +"\t\t\t\titem \"item 2\"\n"
            +"\t\t\tgroup\n"
            +"\t\t\t\tcondition\n"
            +"\t\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\t\tparam \"Test Param2\"\n"
            +"\t\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\t\tcolonizeChance 1\n"
            +"\t\t\t\titems 2\n"
            +"\t\t\t\titem \"item 4\"\n"
            +"\t\t\t\titem \"item 5\"\n"
            +"\t\tspawnProbability 1.000000\n"
            +"\t\tuseDefaultTemplate TRUE\n"
            +"\t\tentityCount 0\n"
            +"\t\tasteroidCount 0\n"
            +"\tconnectionCount 1\n"
            +"\tconnection\n"
            +"\t\tplanetIndexA 2\n"
            +"\t\tplanetIndexB 0\n"
            +"\t\tspawnProbability 1.000000\n"
            +"\t\ttype \"PhaseLane\"\n"
            +"\tentityCount 0\n"
            +"\tspawnProbability 1.000000\n"
        )
        cls.parent = mock.MagicMock(version=mock.Mock(value=4))
        cls.parent.mock_add_spec(spec=scen.Galaxy)
        cls.moveRadius = {"identifier":"moveAreaRadius","val":40}
        cls.hyperRadius = {"identifier":"hyperspaceExitRadius","val":30}

    def setUp(self):
        self.inst = scen.FixedStar(
                identifier=self.identifier,
                parent=self.parent,
                designName=self.designName,
                ingameName=self.ingameName,
                type=self.type,
                pos=self.pos,
                moveRadius=self.moveRadius,
                hyperRadius=self.hyperRadius,
                radius=self.radius,
                planets=self.planets,
                connections=self.connections,
                entityCount=self.entityCount,
                spawnProb=self.spawnProb
        )
        super().setUp()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<scen.g_radii
        parseRes = scen.g_fixexStar.parseString(self.parseString)[0]
        res = scen.FixedStar(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

class FixedStarTestsV5(FixedStarFixture,unit.TestCase):
    desc = "Tests FixedStarV5:"

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.parseString = (
            "star\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [1107,101]\n"
            +"\tradius 100.000000\n"
            +"\tplanetCount 1\n"
            +"\tplanet\n"
            +"\t\tdesignName \"desname\"\n"
            +"\t\tinGameName \"ingamename\"\n"
            +"\t\ttype \"testtype\"\n"
            +"\t\tpos [1107,101]\n"
            +"\t\towner \"player1\"\n"
            +"\t\tisHomePlanet TRUE\n"
            +"\t\tnormalStartUpgradeLevelForPopulation 3\n"
            +"\t\tnormalStartUpgradeLevelForCivilianModules 1\n"
            +"\t\tnormalStartUpgradeLevelForTacticalModules 0\n"
            +"\t\tnormalStartUpgradeLevelForArtifacts 10\n"
            +"\t\tnormalStartUpgradeLevelForInfrastructure 2\n"
            +"\t\tquickStartUpgradeLevelForPopulation 3\n"
            +"\t\tquickStartUpgradeLevelForCivilianModules 1\n"
            +"\t\tquickStartUpgradeLevelForTacticalModules 0\n"
            +"\t\tquickStartUpgradeLevelForArtifacts 10\n"
            +"\t\tquickStartUpgradeLevelForInfrastructure 2\n"
            +"\t\tplanetItems\n"
            +"\t\t\ttemplateName \"Template:Test\"\n"
            +"\t\t\tsubTemplates 2\n"
            +"\t\t\ttemplate \"Template:Sub1\"\n"
            +"\t\t\ttemplate \"Template:Sub2\"\n"
            +"\t\t\tgroups 2\n"
            +"\t\t\tgroup\n"
            +"\t\t\t\tcondition\n"
            +"\t\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\t\tparam \"Test Param\"\n"
            +"\t\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\t\tcolonizeChance 1\n"
            +"\t\t\t\titems 2\n"
            +"\t\t\t\titem \"item 1\"\n"
            +"\t\t\t\titem \"item 2\"\n"
            +"\t\t\tgroup\n"
            +"\t\t\t\tcondition\n"
            +"\t\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\t\tparam \"Test Param2\"\n"
            +"\t\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\t\tcolonizeChance 1\n"
            +"\t\t\t\titems 2\n"
            +"\t\t\t\titem \"item 4\"\n"
            +"\t\t\t\titem \"item 5\"\n"
            +"\t\tspawnProbability 1.000000\n"
            +"\t\tuseDefaultTemplate TRUE\n"
            +"\t\tentityCount 0\n"
            +"\t\tasteroidCount 0\n"
            +"\tconnectionCount 1\n"
            +"\tconnection\n"
            +"\t\tplanetIndexA 2\n"
            +"\t\tplanetIndexB 0\n"
            +"\t\tspawnProbability 1.000000\n"
            +"\t\ttype \"PhaseLane\"\n"
            +"\tentityCount 0\n"
            +"\tspawnProbability 1.000000\n"
        )
        cls.parent = mock.MagicMock(version=mock.Mock(value=5))
        cls.parent.mock_add_spec(spec=scen.Galaxy)
        del cls.planets["elements"][0]["moveRadius"]
        del cls.planets["elements"][0]["hyperRadius"]

    def setUp(self):
        self.inst = scen.FixedStar(
                identifier=self.identifier,
                parent=self.parent,
                designName=self.designName,
                ingameName=self.ingameName,
                type=self.type,
                pos=self.pos,
                radius=self.radius,
                planets=self.planets,
                connections=self.connections,
                entityCount=self.entityCount,
                spawnProb=self.spawnProb
        )
        super().setUp()

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        scen.g_radii_fwd<<pp.Empty().leaveWhitespace()
        parseRes = scen.g_fixexStar.parseString(self.parseString)[0]
        res = scen.FixedStar(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

"""########################## Tests FixedCartography #######################"""

class FixedCartographyTests(unit.TestCase):
    desc = "Tests FixedCartography:"

    @classmethod
    def setUpClass(cls):
        cls.parseString = (
            "useRandomGenerator FALSE\n"
            +"galaxyWidth 1920.000000\n"
            +"galaxyHeight 1126.000000\n"
            +"nextStarNameUniqueId 1\n"
            +"nextPlanetNameUniqueId 1\n"
            +"triggerCount 0\n"
            +"starCount 1\n"
            +"star\n"
            +"\tdesignName \"desname\"\n"
            +"\tinGameName \"ingamename\"\n"
            +"\ttype \"testtype\"\n"
            +"\tpos [1107,101]\n"
            +"\tradius 100.000000\n"
            +"\tmoveAreaRadius 40\n"
            +"\thyperspaceExitRadius 30\n"
            +"\tplanetCount 1\n"
            +"\tplanet\n"
            +"\t\tdesignName \"desname\"\n"
            +"\t\tinGameName \"ingamename\"\n"
            +"\t\ttype \"testtype\"\n"
            +"\t\tpos [1107,101]\n"
            +"\t\tmoveAreaRadius 40\n"
            +"\t\thyperspaceExitRadius 30\n"
            +"\t\towner \"player1\"\n"
            +"\t\tisHomePlanet TRUE\n"
            +"\t\tnormalStartUpgradeLevelForPopulation 3\n"
            +"\t\tnormalStartUpgradeLevelForCivilianModules 1\n"
            +"\t\tnormalStartUpgradeLevelForTacticalModules 0\n"
            +"\t\tnormalStartUpgradeLevelForArtifacts 10\n"
            +"\t\tnormalStartUpgradeLevelForInfrastructure 2\n"
            +"\t\tquickStartUpgradeLevelForPopulation 3\n"
            +"\t\tquickStartUpgradeLevelForCivilianModules 1\n"
            +"\t\tquickStartUpgradeLevelForTacticalModules 0\n"
            +"\t\tquickStartUpgradeLevelForArtifacts 10\n"
            +"\t\tquickStartUpgradeLevelForInfrastructure 2\n"
            +"\t\tplanetItems\n"
            +"\t\t\ttemplateName \"Template:Test\"\n"
            +"\t\t\tsubTemplates 2\n"
            +"\t\t\ttemplate \"Template:Sub1\"\n"
            +"\t\t\ttemplate \"Template:Sub2\"\n"
            +"\t\t\tgroups 2\n"
            +"\t\t\tgroup\n"
            +"\t\t\t\tcondition\n"
            +"\t\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\t\tparam \"Test Param\"\n"
            +"\t\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\t\tcolonizeChance 1\n"
            +"\t\t\t\titems 2\n"
            +"\t\t\t\titem \"item 1\"\n"
            +"\t\t\t\titem \"item 2\"\n"
            +"\t\t\tgroup\n"
            +"\t\t\t\tcondition\n"
            +"\t\t\t\t\ttype \"Always\"\n"
            +"\t\t\t\t\tparam \"Test Param2\"\n"
            +"\t\t\t\towner \"PlanetOwner\"\n"
            +"\t\t\t\tcolonizeChance 1\n"
            +"\t\t\t\titems 2\n"
            +"\t\t\t\titem \"item 4\"\n"
            +"\t\t\t\titem \"item 5\"\n"
            +"\t\tspawnProbability 1.000000\n"
            +"\t\tuseDefaultTemplate TRUE\n"
            +"\t\tentityCount 0\n"
            +"\t\tasteroidCount 0\n"
            +"\tconnectionCount 1\n"
            +"\tconnection\n"
            +"\t\tplanetIndexA 2\n"
            +"\t\tplanetIndexB 0\n"
            +"\t\tspawnProbability 1.000000\n"
            +"\t\ttype \"PhaseLane\"\n"
            +"\tentityCount 0\n"
            +"\tspawnProbability 1.000000\n"
            +"interStarConnectionCount 1\n"
            +"interStarConnection\n"
            +"\tstarIndexA 12\n"
            +"\tplanetIndexA 3\n"
            +"\tstarIndexB 7\n"
            +"\tplanetIndexB 0\n"
            +"\tspawnProbability 1.000000\n"
            +"\ttype \"Wormhole\"\n"
            +"playerCount 1\n"
            +"player\n"
            +"\tdesignName \"testdesign\"\n"
            +"\tinGameName \"testingame\"\n"
            +"\toverrideRaceName \"overname\"\n"
            +"\tteamIndex 2\n"
            +"\tstartingCredits 1000\n"
            +"\tstartingMetal 250\n"
            +"\tstartingCrystal 490\n"
            +"\tisNormalPlayer FALSE\n"
            +"\tisRaidingPlayer FALSE\n"
            +"\tisInsurgentPlayer FALSE\n"
            +"\tisOccupationPlayer FALSE\n"
            +"\tisMadVasariPlayer FALSE\n"
            +"\tthemeGroup \"testtheme\"\n"
            +"\tthemeIndex 4\n"
            +"\tpictureGroup \"testpicture\"\n"
            +"\tpictureIndex 9\n"
            +"templates 1\n"
            +"template\n"
            +"\ttemplateName \"Template:Test\"\n"
            +"\tsubTemplates 2\n"
            +"\ttemplate \"Template:Sub1\"\n"
            +"\ttemplate \"Template:Sub2\"\n"
            +"\tgroups 2\n"
            +"\tgroup\n"
            +"\t\tcondition\n"
            +"\t\t\ttype \"Always\"\n"
            +"\t\t\tparam \"Test Param\"\n"
            +"\t\towner \"PlanetOwner\"\n"
            +"\t\tcolonizeChance 1\n"
            +"\t\titems 2\n"
            +"\t\titem \"item 1\"\n"
            +"\t\titem \"item 2\"\n"
            +"\tgroup\n"
            +"\t\tcondition\n"
            +"\t\t\ttype \"Always\"\n"
            +"\t\t\tparam \"Test Param2\"\n"
            +"\t\towner \"PlanetOwner\"\n"
            +"\t\tcolonizeChance 1\n"
            +"\t\titems 2\n"
            +"\t\titem \"item 4\"\n"
            +"\t\titem \"item 5\"\n"
        )
        cls.parent = mock.MagicMock(version=mock.Mock(value=4))
        cls.parent.mock_add_spec(spec=scen.Galaxy)
        cls.random = {"identifier":"useRandomGenerator","val":False}
        cls.galWidth = {"identifier":"galaxyWidth","val":1920.0}
        cls.galHeight = {"identifier":"galaxyHeight","val":1126.0}
        cls.nextStarNameId = {"identifier":"nextStarNameUniqueId",
            "val":1}
        cls.nextPlanetNameId = {"identifier":"nextPlanetNameUniqueId","val":1}
        cls.triggerCount = {"identifier":"triggerCount","val":0}
        cls.stars = {
            "counter":{"identifier":"starCount","val":1},
            "elements":[
                {
                    "identifier":"star",
                    "designName":{"identifier":"designName","val":"desname"},
                    "ingameName":{"identifier":"inGameName","val":"ingamename"},
                    "type":{"identifier":"type","val":"testtype"},
                    "pos":{"identifier":"pos","coord":[1107,101]},
                    "moveRadius":{"identifier":"moveAreaRadius","val":40},
                    "hyperRadius":{"identifier":"hyperspaceExitRadius",
                        "val":30},
                    "radius":{"identifier":"radius","val":100.0},
                    "planets":{
                        "counter":{"identifier":"planetCount","val":1},
                        "elements":[
                            {
                                "identifier":"planet",
                                "designName":{"identifier":"designName",
                                    "val":"desname"},
                                "ingameName":{"identifier":"inGameName",
                                    "val":"ingamename"},
                                "type":{"identifier":"type","val":"testtype"},
                                "pos":{"identifier":"pos","coord":[1107,101]},
                                "moveRadius":{"identifier":"moveAreaRadius",
                                    "val":40},
                                "hyperRadius":{
                                    "identifier":"hyperspaceExitRadius",
                                    "val":30},
                                "owner":{"identifier":"owner","val":"player1"},
                                "isHome":{"identifier":"isHomePlanet",
                                    "val":True},
                                "normalUpg":{
                                    "pop":{
                                        "identifier":"normalStartUpgradeLevelForPopulation",
                                        "val":3},
                                    "civ":{
                                        "identifier":"normalStartUpgradeLevelForCivilianModules",
                                        "val":1},
                                    "tac":{
                                        "identifier":"normalStartUpgradeLevelForTacticalModules",
                                        "val":0},
                                    "artifact":{
                                        "identifier":"normalStartUpgradeLevelForArtifacts",
                                        "val":10},
                                    "infrastructure":{
                                        "identifier":"normalStartUpgradeLevelForInfrastructure",
                                        "val":2}
                                },
                                "quickUpg":{
                                    "pop":{
                                        "identifier":"quickStartUpgradeLevelForPopulation",
                                        "val":3},
                                    "civ":{
                                        "identifier":"quickStartUpgradeLevelForCivilianModules",
                                        "val":1},
                                    "tac":{
                                        "identifier":"quickStartUpgradeLevelForTacticalModules",
                                        "val":0},
                                    "artifact":{
                                        "identifier":"quickStartUpgradeLevelForArtifacts",
                                        "val":10},
                                    "infrastructure":{
                                        "identifier":"quickStartUpgradeLevelForInfrastructure",
                                        "val":2}
                                },
                                "items":{
                                    "identifier":"planetItems",
                                    "name":{"identifier":"templateName",
                                        "val":"Template:Test"},
                                    "subTemplates":{
                                        "counter":{"identifier":"subTemplates",
                                            "val":2},
                                        "elements":[
                                            {"identifier":"template",
                                                "val":"Template:Sub1"},
                                            {"identifier":"template",
                                                "val":"Template:Sub2"}
                                        ]
                                    },
                                    "groups":{
                                        "counter":{"identifier":"groups",
                                            "val":2},
                                        "elements":[
                                            {
                                                "identifier":"group",
                                                "condition":{
                                                    "identifier":"condition",
                                                    "condType":{
                                                        "identifier":"type",
                                                        "val":"Always"},
                                                    "param":{
                                                        "identifier":"param",
                                                        "val":"Test Param"}
                                                },
                                                "owner":{"identifier":"owner",
                                                    "val":"PlanetOwner"},
                                                "colonizeChance":{
                                                    "identifier":"colonizeChance",
                                                    "val":1},
                                                "items":{
                                                    "counter":{
                                                        "identifier":"items",
                                                        "val":2},
                                                    "elements":[
                                                        {"identifier":"item",
                                                            "val":"item 1"},
                                                        {"identifier":"item",
                                                            "val":"item 2"}
                                                    ]
                                                }
                                            },
                                            {
                                                "identifier":"group",
                                                "condition":{
                                                    "identifier":"condition",
                                                    "condType":{
                                                        "identifier":"type",
                                                        "val":"Always"},
                                                    "param":{
                                                        "identifier":"param",
                                                        "val":"Test Param2"}
                                                },
                                                "owner":{"identifier":"owner",
                                                    "val":"PlanetOwner"},
                                                "colonizeChance":{
                                                    "identifier":"colonizeChance",
                                                    "val":1},
                                                "items":{
                                                    "counter":{
                                                        "identifier":"items",
                                                        "val":2},
                                                    "elements":[
                                                        {"identifier":"item",
                                                            "val":"item 4"},
                                                        {"identifier":"item",
                                                            "val":"item 5"}
                                                    ]
                                                }
                                            }
                                        ]
                                    }
                                },
                                "spawnProb":{"identifier":"spawnProbability",
                                    "val":1.0},
                                "useDefTemplate":{
                                    "identifier":"useDefaultTemplate",
                                    "val":True},
                                "entityCount":{"identifier":"entityCount",
                                    "val":0},
                                "asteroidCount":{"identifier":"asteroidCount",
                                    "val":0}
                            }
                        ]
                    },
                    "connections":{
                        "counter":{"identifier":"connectionCount","val":1},
                        "elements":[
                            {
                                "identifier":"connection",
                                "indexA":{"identifier":"planetIndexA","val":2},
                                "indexB":{"identifier":"planetIndexB","val":0},
                                "spawnProb":{"identifier":"spawnProbability",
                                    "val":1.0},
                                "connType":{"identifier":"type",
                                    "val":"PhaseLane"}
                            }
                        ]
                    },
                    "entityCount":{"identifier":"entityCount","val":0},
                    "spawnProb":{"identifier":"spawnProbability","val":1.0}
                }
            ]
        }
        cls.starConnections = {
            "counter":{"identifier":"interStarConnectionCount","val":1},
            "elements":[
                {
                    "identifier":"interStarConnection",
                    "starA":{"identifier":"starIndexA","val":12},
                    "planetA":{"identifier":"planetIndexA","val":3},
                    "starB":{"identifier":"starIndexB","val":7},
                    "planetB":{"identifier":"planetIndexB","val":0},
                    "spawnProb":{"identifier":"spawnProbability","val":1.0},
                    "connType":{"identifier":"type","val":"Wormhole"}
                }
            ]
        }
        cls.players = {
            "counter":{"identifier":"playerCount","val":1},
            "elements":[
                {
                    "identifier":"player",
                    "name":{"identifier":"designName","val":"testdesign"},
                    "ingameName":{"identifier":"inGameName","val":"testingame"},
                    "overrideRaceName":{"identifier":"overrideRaceName",
                        "val":"overname"},
                    "teamIndex":{"identifier":"teamIndex","val":2},
                    "startingCredits":{"identifier":"startingCredits",
                        "val":1000},
                    "startingMetal":{"identifier":"startingMetal","val":250},
                    "startingCrystal":{"identifier":"startingCrystal",
                        "val":490},
                    "isNormal":{"identifier":"isNormalPlayer","val":False},
                    "isRaiding":{"identifier":"isRaidingPlayer","val":False},
                    "isInsurgent":{"identifier":"isInsurgentPlayer",
                        "val":False},
                    "isOccupation":{"identifier":"isOccupationPlayer",
                        "val":False},
                    "isMad":{"identifier":"isMadVasariPlayer","val":False},
                    "theme":{"identifier":"themeGroup","val":"testtheme"},
                    "themeIndex":{"identifier":"themeIndex","val":4},
                    "picture":{"identifier":"pictureGroup","val":"testpicture"},
                    "pictureIndex":{"identifier":"pictureIndex","val":9},
                }
            ]
        }
        cls.templates = {
            "counter":{"identifier":"templates","val":1},
            "elements":[
                {
                    "identifier":"template",
                    "name":{"identifier":"templateName","val":"Template:Test"},
                    "subTemplates":{
                        "counter":{"identifier":"subTemplates","val":2},
                        "elements":[
                            {"identifier":"template","val":"Template:Sub1"},
                            {"identifier":"template","val":"Template:Sub2"}
                        ]
                    },
                    "groups":{
                        "counter":{"identifier":"groups","val":2},
                        "elements":[
                            {
                                "identifier":"group",
                                "condition":{
                                    "identifier":"condition",
                                    "condType":{"identifier":"type",
                                        "val":"Always"},
                                    "param":{"identifier":"param",
                                        "val":"Test Param"}
                                },
                                "owner":{"identifier":"owner",
                                    "val":"PlanetOwner"},
                                "colonizeChance":{"identifier":"colonizeChance",
                                    "val":1},
                                "items":{
                                    "counter":{"identifier":"items","val":2},
                                    "elements":[
                                        {"identifier":"item","val":"item 1"},
                                        {"identifier":"item","val":"item 2"}
                                    ]
                                }
                            },
                            {
                                "identifier":"group",
                                "condition":{
                                    "identifier":"condition",
                                    "condType":{"identifier":"type",
                                        "val":"Always"},
                                    "param":{"identifier":"param",
                                        "val":"Test Param2"}
                                },
                                "owner":{"identifier":"owner",
                                    "val":"PlanetOwner"},
                                "colonizeChance":{"identifier":"colonizeChance",
                                    "val":1},
                                "items":{
                                    "counter":{"identifier":"items","val":2},
                                    "elements":[
                                        {"identifier":"item","val":"item 4"},
                                        {"identifier":"item","val":"item 5"}
                                    ]
                                }
                            }
                        ]
                    }
                }
            ]
        }
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.FixedCartography(
                parent=self.parent,
                random=self.random,
                galWidth=self.galWidth,
                galHeight=self.galHeight,
                nextStarNameId=self.nextStarNameId,
                nextPlanetNameId=self.nextPlanetNameId,
                triggerCount=self.triggerCount,
                stars=self.stars,
                starConnections=self.starConnections,
                players=self.players,
                templates=self.templates
        )
        patcher = mock.patch.object(self.inst.stars,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockStars = patcher.start()
        patcher = mock.patch.object(self.inst.starConnections,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockConns = patcher.start()
        patcher = mock.patch.object(self.inst.players,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockPlayers = patcher.start()
        patcher = mock.patch.object(self.inst.templates,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTemplates = patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        # g_radii_fwd is a placeholder for the moveArea/hyperspace radius
        # lines. These were removed in v5 of the file format, but here 
        # we are testing v4 file format. As we only test a partial parser here,
        # we need to readd the lines manually, while in the full file parser,
        # that is done automatically in the versionNumber line parser.
        scen.g_radii_fwd<<scen.g_radii
        parseRes = scen.g_fixedCartography.parseString(self.parseString)[0]
        res = scen.Cartography.factory(parent=self.parent,**parseRes)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

    def testCreationAttribGalWidth(self):
        """ Test whether galWidth attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.galWidth)
        self.assertEqual(self.inst.galWidth,exp)

    def testCreationAttribGalHeight(self):
        """ Test whether galHeight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.galHeight)
        self.assertEqual(self.inst.galHeight,exp)

    def testCreationAttribNextStarNameId(self):
        """ Test whether nextStarNameId attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nextStarNameId)
        self.assertEqual(self.inst.nextStarNameId,exp)

    def testCreationAttribNextPlanetNameId(self):
        """ Test whether nextPlanetNameId attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nextPlanetNameId)
        self.assertEqual(self.inst.nextPlanetNameId,exp)

    def testCreationAttribTriggerCount(self):
        """ Test whether triggerCount attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.triggerCount)
        self.assertEqual(self.inst.triggerCount,exp)

    def testCreationAttribStars(self):
        """ Test whether stars attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.FixedStar,
                elemArgs={"parent":self.parent},
                **self.stars)
        self.assertEqual(self.inst.stars,exp)

    def testCreationAttribStarConnections(self):
        """ Test whether starConnections attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.StarConnection,
                elemArgs={"parent":self.parent},
                **self.starConnections)
        self.assertEqual(self.inst.starConnections,exp)

    def testCreationAttribPlayers(self):
        """ Test whether players attrib is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=scen.ScenarioPlayer,
                keyfunc=lambda val:val.name.value,
                **self.players)
        self.assertEqual(self.inst.players,exp)

    def testCreationAttribTemplates(self):
        """ Test whether templates attrib is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=scen.ScenarioTemplate,
                elemArgs={"parent":self.parent},
                **self.templates)
        self.assertEqual(self.inst.templates,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckStars(self):
        """ Test whether check returns problems from stars attrib
        """
        self.mockStars.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockStars.assert_called_once_with(self.mmod,False)

    def testCheckStarConnections(self):
        """ Test whether check returns problems from starConnections attrib
        """
        self.mockConns.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockConns.assert_called_once_with(self.mmod,False)

    def testCheckPlayers(self):
        """ Test whether check returns problems from players attrib
        """
        self.mockPlayers.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockPlayers.assert_called_once_with(self.mmod,False)

    def testCheckTemplates(self):
        """ Test whether check returns problems from templates attrib
        """
        self.mockTemplates.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockTemplates.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

"""############################### Tests Galaxy ############################"""

class GalaxyFixture(object):
    @classmethod
    def setUpClass(cls):
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.version = {"identifier":"versionNumber","val":4}
        cls.browsable = {"identifier":"isBrowsable","val":True}
        cls.picName = {"identifier":"browsePictureName","val":"galpic"}
        cls.browseName = {"identifier":"browseName","val":"browsename"}
        cls.browseDesc = {"identifier":"browseDescription","val":"browsedesc"}
        cls.firstCapitalIsFlagship = {
            "identifier":"isFirstCapitalShipIsFlagship",
            "val":True}
        cls.randStartPos = {"identifier":"randomizeStartingPositions",
            "val":True}
        cls.artifactDensity = {"identifier":"planetArtifactDensity","val":0.15}
        cls.bonusDensity = {"identifier":"planetBonusDensity","val":0.6}
        cls.homeUpgradesNormal = {
            "pop":{
                "identifier":"normalStartHomePlanetUpgradeLevel:Population",
                "val":3},
            "civ":{
                "identifier":"normalStartHomePlanetUpgradeLevel:CivilianModules",
                "val":1},
            "tac":{
                "identifier":"normalStartHomePlanetUpgradeLevel:TacticalModules",
                "val":0},
            "home":{"identifier":"normalStartHomePlanetUpgradeLevel:Home",
                "val":1},
            "artifact":{
                "identifier":"normalStartHomePlanetUpgradeLevel:ArtifactLevel",
                "val":10},
            "infrastructure":{
                "identifier":"normalStartHomePlanetUpgradeLevel:Infrastructure",
                "val":2
            }

        }
        cls.homeUpgradesQuick = {
            "pop":{
                "identifier":"quickStartHomePlanetUpgradeLevel:Population",
                "val":4},
            "civ":{
                "identifier":"quickStartHomePlanetUpgradeLevel:CivilianModules",
                "val":1},
            "tac":{
                "identifier":"quickStartHomePlanetUpgradeLevel:TacticalModules",
                "val":0},
            "home":{"identifier":"quickStartHomePlanetUpgradeLevel:Home",
                "val":1},
            "artifact":{
                "identifier":"quickStartHomePlanetUpgradeLevel:ArtifactLevel",
                "val":10},
            "infrastructure":{
                "identifier":"quickStartHomePlanetUpgradeLevel:Infrastructure",
                "val":2
            }

        }
        cls.recommGameTypes = {
            "counter":{"identifier":"recommendedGameTypeCount","val":1},
            "elements":[
                {"identifier":"recommendedGameType","val":"FFA"}
            ]
        }
        cls.meterPerUnit = {"identifier":"metersPerGalaxyUnit","val":2500.0}
        cls.meterPerPixel = {"identifier":"pixelsPerGalaxyUnit","val":2.959997}
        cls.mmod = tco.genMockMod("./")

    def setUp(self):
        self.inst = scen.Galaxy(
                filepath=self.filepath,
                version=self.version,
                browsable=self.browsable,
                picName=self.picName,
                browseName=self.browseName,
                browseDesc=self.browseDesc,
                firstCapitalIsFlagship=self.firstCapitalIsFlagship,
                randStartPos=self.randStartPos,
                artifactDensity=self.artifactDensity,
                bonusDensity=self.bonusDensity,
                homeUpgradesNormal=self.homeUpgradesNormal,
                homeUpgradesQuick=self.homeUpgradesQuick,
                recommGameTypes=self.recommGameTypes,
                meterPerUnit=self.meterPerUnit,
                meterPerPixel=self.meterPerPixel,
                cartography=self.cartography
        )
        patcher = mock.patch.object(self.inst.picName,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockPic = patcher.start()
        patcher = mock.patch.object(self.inst.cartography,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockCart = patcher.start()
        self.mockprob = mock.MagicMock(probFile=None)
        self.mockprob.mock_add_spec(spec=co_probs.BasicProblem,spec_set=True)
        self.addCleanup(mock.patch.stopall)
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = scen.Galaxy.createInstance(self.filepath)
        self.assertEqual(res,self.inst)
        scen.g_radii_fwd<<pp.Forward()

    def testCreationAttribVersion(self):
        """ Test whether version attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.version)
        self.assertEqual(self.inst.version,exp)

    def testCreationAttribBrowsable(self):
        """ Test whether browsable attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.browsable)
        self.assertEqual(self.inst.browsable,exp)

    def testCreationAttribPicName(self):
        """ Test whether picName attribute is created correctly
        """
        exp = ui.UITextureRef(**self.picName)
        self.assertEqual(self.inst.picName,exp)

    def testCreationAttribBrowseName(self):
        """ Test whether browseName attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.browseName)
        self.assertEqual(self.inst.browseName,exp)

    def testCreationAttribBrowseDesc(self):
        """ Test whether browseDesc attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.browseDesc)
        self.assertEqual(self.inst.browseDesc,exp)

    def testCreationAttribFirstCapitalIsFlagship(self):
        """ Test whether firstCapitalIsFlagship attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.firstCapitalIsFlagship)
        self.assertEqual(self.inst.firstCapitalIsFlagship,exp)

    def testCreationAttribRandStartPos(self):
        """ Test whether randStartPos attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.randStartPos)
        self.assertEqual(self.inst.randStartPos,exp)

    def testCreationAttribArtifactDensity(self):
        """ Test whether artifactDensity attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.artifactDensity)
        self.assertEqual(self.inst.artifactDensity,exp)

    def testCreationAttribBonusDensity(self):
        """ Test whether bonusDensity attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.bonusDensity)
        self.assertEqual(self.inst.bonusDensity,exp)

    def testCreationAttribHomeUpgradesNormal(self):
        """ Test whether homeUpgradesNormal attribute is created correctly
        """
        exp = scen.HomeUpgrades(**self.homeUpgradesNormal)
        self.assertEqual(self.inst.homeUpgradesNormal,exp)

    def testCreationAttribHomeUpgradesQuick(self):
        """ Test whether homeUpgradesQuick attribute is created correctly
        """
        exp = scen.HomeUpgrades(**self.homeUpgradesQuick)
        self.assertEqual(self.inst.homeUpgradesQuick,exp)

    def testCreationAttribRecommGameTypes(self):
        """ Test whether recommGameTypes attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":scen.GAME_TYPES},
                **self.recommGameTypes
        )
        self.assertEqual(self.inst.recommGameTypes,exp)

    def testCreationAttribMeterPerUnit(self):
        """ Test whether meterPerUnit attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.meterPerUnit)
        self.assertEqual(self.inst.meterPerUnit,exp)

    def testCreationAttribMeterPerPixel(self):
        """ Test whether meterPerPixel attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.meterPerPixel)
        self.assertEqual(self.inst.meterPerPixel,exp)

    def testCreationAttribCartography(self):
        """ Test whether cartography attribute is created correctly
        """
        exp = scen.Cartography.factory(parent=self.inst,**self.cartography)
        self.assertEqual(self.inst.cartography,exp)

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[])

    def testCheckPic(self):
        """ Test whether check returns problems from picName attrib
        """
        self.mockPic.return_value = [self.mockprob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockprob])
        self.mockPic.assert_called_once_with(self.mmod,False)

    def testCheckCartography(self):
        """ Test whether check returns problems from cartography attrib
        """
        self.mockCart.return_value = [self.mockprob]
        res = self.inst.check(self.mmod)
        self.assertEqual(res,[self.mockprob])
        self.mockCart.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        self.assertEqual(self.inst.toString(0),self.parseString)

class GalaxyFixedTest(GalaxyFixture,unit.TestCase):
    desc = "Tests Galaxy with fixed cartography:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "GalaxyFixed.galaxy"
        super().setUpClass()
        cls.cartography = {
            "random":{"identifier":"useRandomGenerator","val":False},
            "galWidth":{"identifier":"galaxyWidth","val":1920.0},
            "galHeight":{"identifier":"galaxyHeight","val":1126.0},
            "nextStarNameId":{"identifier":"nextStarNameUniqueId",
                "val":1},
            "nextPlanetNameId":{"identifier":"nextPlanetNameUniqueId","val":1},
            "triggerCount":{"identifier":"triggerCount","val":0},
            "stars":{
                "counter":{"identifier":"starCount","val":1},
                "elements":[
                    {
                        "identifier":"star",
                        "designName":{"identifier":"designName",
                            "val":"desname"},
                        "ingameName":{"identifier":"inGameName",
                            "val":"ingamename"},
                        "type":{"identifier":"type","val":"testtype"},
                        "pos":{"identifier":"pos","coord":[1107,101]},
                        "moveRadius":{"identifier":"moveAreaRadius","val":40},
                        "hyperRadius":{"identifier":"hyperspaceExitRadius",
                            "val":30},
                        "radius":{"identifier":"radius","val":100.0},
                        "planets":{
                            "counter":{"identifier":"planetCount","val":1},
                            "elements":[
                                {
                                    "identifier":"planet",
                                    "designName":{"identifier":"designName",
                                        "val":"desname"},
                                    "ingameName":{"identifier":"inGameName",
                                        "val":"ingamename"},
                                    "type":{"identifier":"type",
                                        "val":"testtype"},
                                    "pos":{"identifier":"pos",
                                        "coord":[1107,101]},
                                    "moveRadius":{"identifier":"moveAreaRadius",
                                        "val":40},
                                    "hyperRadius":{
                                        "identifier":"hyperspaceExitRadius",
                                        "val":30},
                                    "owner":{"identifier":"owner",
                                        "val":"player1"},
                                    "isHome":{"identifier":"isHomePlanet",
                                        "val":True},
                                    "normalUpg":{
                                        "pop":{
                                            "identifier":"normalStartUpgradeLevelForPopulation",
                                            "val":3},
                                        "civ":{
                                            "identifier":"normalStartUpgradeLevelForCivilianModules",
                                            "val":1},
                                        "tac":{
                                            "identifier":"normalStartUpgradeLevelForTacticalModules",
                                            "val":0},
                                        "artifact":{
                                            "identifier":"normalStartUpgradeLevelForArtifacts",
                                            "val":10},
                                        "infrastructure":{
                                            "identifier":"normalStartUpgradeLevelForInfrastructure",
                                            "val":2}
                                    },
                                    "quickUpg":{
                                        "pop":{
                                            "identifier":"quickStartUpgradeLevelForPopulation",
                                            "val":3},
                                        "civ":{
                                            "identifier":"quickStartUpgradeLevelForCivilianModules",
                                            "val":1},
                                        "tac":{
                                            "identifier":"quickStartUpgradeLevelForTacticalModules",
                                            "val":0},
                                        "artifact":{
                                            "identifier":"quickStartUpgradeLevelForArtifacts",
                                            "val":10},
                                        "infrastructure":{
                                            "identifier":"quickStartUpgradeLevelForInfrastructure",
                                            "val":2}
                                    },
                                    "items":{
                                        "identifier":"planetItems",
                                        "name":{"identifier":"templateName",
                                            "val":"Template:Test"},
                                        "subTemplates":{
                                            "counter":{
                                                "identifier":"subTemplates",
                                                "val":2},
                                            "elements":[
                                                {"identifier":"template",
                                                    "val":"Template:Sub1"},
                                                {"identifier":"template",
                                                    "val":"Template:Sub2"}
                                            ]
                                        },
                                        "groups":{
                                            "counter":{"identifier":"groups",
                                                "val":0},
                                            "elements":[]
                                        }
                                    },
                                    "spawnProb":{
                                        "identifier":"spawnProbability",
                                        "val":1.0},
                                    "useDefTemplate":{
                                        "identifier":"useDefaultTemplate",
                                        "val":True},
                                    "entityCount":{"identifier":"entityCount",
                                        "val":0},
                                    "asteroidCount":{
                                        "identifier":"asteroidCount","val":0}
                                }
                            ]
                        },
                        "connections":{
                            "counter":{"identifier":"connectionCount","val":1},
                            "elements":[
                                {
                                    "identifier":"connection",
                                    "indexA":{"identifier":"planetIndexA",
                                        "val":2},
                                    "indexB":{"identifier":"planetIndexB",
                                        "val":0},
                                    "spawnProb":{
                                        "identifier":"spawnProbability",
                                        "val":1.0},
                                    "connType":{"identifier":"type",
                                        "val":"PhaseLane"}
                                }
                            ]
                        },
                        "entityCount":{"identifier":"entityCount","val":0},
                        "spawnProb":{"identifier":"spawnProbability","val":1.0}
                    }
                ]
            },
            "starConnections":{
                "counter":{"identifier":"interStarConnectionCount","val":1},
                "elements":[
                    {
                        "identifier":"interStarConnection",
                        "starA":{"identifier":"starIndexA","val":12},
                        "planetA":{"identifier":"planetIndexA","val":3},
                        "starB":{"identifier":"starIndexB","val":7},
                        "planetB":{"identifier":"planetIndexB","val":0},
                        "spawnProb":{"identifier":"spawnProbability","val":1.0},
                        "connType":{"identifier":"type","val":"Wormhole"}
                    }
                ]
            },
            "players":{
                "counter":{"identifier":"playerCount","val":1},
                "elements":[
                    {
                        "identifier":"player",
                        "name":{"identifier":"designName","val":"testdesign"},
                        "ingameName":{"identifier":"inGameName",
                            "val":"testingame"},
                        "overrideRaceName":{"identifier":"overrideRaceName",
                            "val":"overname"},
                        "teamIndex":{"identifier":"teamIndex","val":2},
                        "startingCredits":{"identifier":"startingCredits",
                            "val":1000},
                        "startingMetal":{"identifier":"startingMetal",
                            "val":250},
                        "startingCrystal":{"identifier":"startingCrystal",
                            "val":490},
                        "isNormal":{"identifier":"isNormalPlayer","val":False},
                        "isRaiding":{"identifier":"isRaidingPlayer",
                            "val":False},
                        "isInsurgent":{"identifier":"isInsurgentPlayer",
                            "val":False},
                        "isOccupation":{"identifier":"isOccupationPlayer",
                            "val":False},
                        "isMad":{"identifier":"isMadVasariPlayer","val":False},
                        "theme":{"identifier":"themeGroup","val":"testtheme"},
                        "themeIndex":{"identifier":"themeIndex","val":4},
                        "picture":{"identifier":"pictureGroup",
                            "val":"testpicture"},
                        "pictureIndex":{"identifier":"pictureIndex","val":9},
                    }
                ]
            },
            "templates":{
                "counter":{"identifier":"templates","val":1},
                "elements":[
                    {
                        "identifier":"template",
                        "name":{"identifier":"templateName",
                            "val":"Template:Test"},
                        "subTemplates":{
                            "counter":{"identifier":"subTemplates","val":0},
                            "elements":[]
                        },
                        "groups":{
                            "counter":{"identifier":"groups","val":1},
                            "elements":[
                                {
                                    "identifier":"group",
                                    "condition":{
                                        "identifier":"condition",
                                        "condType":{"identifier":"type",
                                            "val":"Always"},
                                        "param":{"identifier":"param",
                                            "val":"Test Param2"}
                                    },
                                    "owner":{"identifier":"owner",
                                        "val":"PlanetOwner"},
                                    "colonizeChance":{
                                        "identifier":"colonizeChance","val":1},
                                    "items":{
                                        "counter":{"identifier":"items",
                                            "val":1},
                                        "elements":[
                                            {"identifier":"item","val":"item 5"}
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }

    def testCreationParseMalformed(self):
        """ Test whether parse problem is returned for malformed file
        """
        res = scen.Galaxy.createInstance("GalaxyFixedMalformed.galaxy")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)
        scen.g_radii_fwd<<pp.Forward()

class GalaxyRandomTest(GalaxyFixture,unit.TestCase):
    desc = "Tests Galaxy with random cartography:"

    @classmethod
    def setUpClass(cls):
        cls.filepath = "GalaxyRandom.galaxy"
        super().setUpClass()
        cls.cartography = {
            "random":{"identifier":"useRandomGenerator","val":True},
            "randParams":{
                "identifier":"randomizerParams",
                "starPosOffset":{
                    "identifier":"starPosOffsetRange",
                    "minPercent":{"identifier":"minPercentage","val":10.0},
                    "maxPercent":{"identifier":"maxPercentage","val":15.0}
                },
                "playerParams":{
                    "identifier":"playerParams",
                    "startCredits":{"identifier":"startingCredits","val":3000},
                    "startMetal":{"identifier":"startingMetal","val":800},
                    "startCrystal":{"identifier":"startingCrystal","val":250},
                    "homePlanet":{"identifier":"homePlanetType",
                        "val":"homeref"},
                    "homePlanetRadius":{
                        "identifier":"homePlanetStarRadiusRange",
                        "minPercent":{"identifier":"minPercentage","val":1},
                        "maxPercent":{"identifier":"maxPercentage","val":1.1}
                    },
                    "extrasColonized":{"identifier":"areExtraPlanetsColonized",
                        "val":False},
                    "extrasRadiusMax":{"identifier":"extraPlanetsMaxRadius",
                        "val":15},
                    "extrasRadiusRange":{
                        "identifier":"extraPlanetsRadiusRange",
                        "minPercent":{"identifier":"minPercentage","val":0.9},
                        "maxPercent":{"identifier":"maxPercentage","val":1.0}
                    },
                    "extraPlanets":{
                        "counter":{"identifier":"extraPlanetGroupCount",
                            "val":1},
                        "elements":[
                            {
                                "identifier":"extraPlanetGroup",
                                "minCount":{"identifier":"minCount","val":1},
                                "maxCount":{"identifier":"maxCount","val":2},
                                "planetTypes":{
                                    "counter":{"identifier":"planetTypeCount",
                                        "val":1},
                                    "elements":[
                                        {"identifier":"planetType",
                                            "val":"testtype"}
                                    ]
                                }
                            }
                        ]
                    }
                }
            },
            "stars":{
                "counter":{"identifier":"starCount","val":1},
                "elements":[
                    {
                        "identifier":"star",
                        "starType":{"identifier":"type","val":"testtype"},
                        "radius":{"identifier":"radius","val":100},
                        "moveRadius":{"identifier":"moveAreaRadius",
                            "val":60000.0},
                        "hyperRadius":{"identifier":"hyperspaceExitRadius",
                            "val":50000.0},
                        "connectionRadius":{
                            "identifier":"connectionStarRadiusRange",
                            "val":.7},
                        "connectionChance":{"identifier":"connectionChance",
                            "val":.75},
                        "maxPlayers":{"identifier":"maxPlayerCount","val":6},
                        "planets":{
                            "counter":{"identifier":"ringCount","val":1},
                            "elements":[
                                {
                                    "identifier":"ring",
                                    "starDistance":{
                                        "identifier":"starRadiusRange",
                                        "minPercent":{
                                            "identifier":"minPercentage",
                                            "val":0.3},
                                        "maxPercent":{
                                            "identifier":"maxPercentage",
                                            "val":0.7}
                                    },
                                    "militiaColPerc":{
                                        "identifier":"militiaColonizationPerc",
                                        "val":0},
                                    "planets":{
                                        "counter":{
                                            "identifier":"planetGroupCount",
                                            "val":1},
                                        "elements":[
                                            {
                                                "identifier":"planetGroup",
                                                "minCount":{
                                                    "identifier":"minCount",
                                                    "val":1},
                                                "maxCount":{
                                                    "identifier":"maxCount",
                                                    "val":2},
                                                "planetTypes":{
                                                    "counter":{
                                                        "identifier":"planetTypeCount",
                                                        "val":1},
                                                    "elements":[
                                                        {
                                                            "identifier":"planetType",
                                                            "val":"testtype"}
                                                    ]
                                                }
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    }
                ]
            }
        }

    def testCreationParseMalformed(self):
        """ Test whether parse problem is returned for malformed file
        """
        res = scen.Galaxy.createInstance("GalaxyRandomMalformed.galaxy")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.ParseProblem)
        scen.g_radii_fwd<<pp.Forward()

"""############################# Tests ModScenario #########################"""

class ModScenarioCreationTests(unit.TestCase):
    desc = "Tests ModScenario Creation:"

    @classmethod
    def setUpClass(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.subpath = os.path.join(cls.modpath,"Galaxy")
        cls.rebrefpath = os.path.abspath("./comprebref")

    def setUp(self):
        self.mmod = tco.genMockMod(self.modpath)
        self.inst = scen.ModScenarios(self.mmod)

    def testFindGalaxies(self):
        """ Test whether galaxy files are found correctly
        """
        self.assertIn("gal1.galaxy",self.inst.compFilesMod)
        self.assertIn("gal2.galaxy",self.inst.compFilesMod)
        self.assertNotIn("gal.foo",self.inst.compFilesMod)

    def testLazy(self):
        """ Test whether manifest and GalDef are loaded at initialization
        """
        self.assertIs(self.inst._manifest,None)
        self.assertIs(self.inst._galDef,None)

    def testCreationManifest(self):
        """ Test whether manifest is created correctly
        """
        manMock = mock.create_autospec(manifest.ManifestGalaxy,
                spec_set=True)
        fileMock = mock.create_autospec(manifest.ManifestGalaxy,spec_set=True)
        manMock.createInstance.return_value = fileMock
        moduleMock = mock.MagicMock(ManifestGalaxy=manMock)
        packageMock = mock.MagicMock()
        packageMock.mod.manifest = moduleMock
        with mock.patch.dict("sys.modules",{"pysoase":packageMock,
                "pysoase.mod.manifest":moduleMock}):
            res = self.inst.manifest
            manMock.createInstance.assert_called_once_with(
                    os.path.join(self.modpath,"galaxy.manifest"))
            self.assertEqual(res,fileMock)

    def testCreationManifestLazy(self):
        """ Test whether manifest lazy loadind works
        """
        manMock = mock.create_autospec(manifest.ManifestGalaxy,
                spec_set=True)
        fileMock = mock.create_autospec(manifest.ManifestGalaxy,spec_set=True)
        manMock.createInstance.return_value = fileMock
        moduleMock = mock.MagicMock(ManifestGalaxy=manMock)
        packageMock = mock.MagicMock()
        packageMock.mod.manifest = moduleMock
        with mock.patch.dict("sys.modules",{"pysoase":packageMock,
                "pysoase.mod.manifest":moduleMock}):
            res = self.inst.manifest
            manMock.createInstance.assert_called_once_with(
                    os.path.join(self.modpath,"galaxy.manifest"))
            self.assertEqual(res,fileMock)
        res1 = self.inst.manifest
        self.assertEquals(manMock.createInstance.call_count,1)
        self.assertEquals(res,res1)

    def testCreationManifestMissing(self):
        """ Test whether missing manifest is loaded from bundled files
        """
        currMod = tco.genMockMod("./comp_empty")
        currInst = scen.ModScenarios(currMod)
        manMock = mock.create_autospec(manifest.ManifestGalaxy,
                spec_set=True)
        fileMock = mock.create_autospec(manifest.ManifestGalaxy,spec_set=True)
        manMock.createInstance.return_value = fileMock
        moduleMock = mock.MagicMock(ManifestGalaxy=manMock)
        packageMock = mock.MagicMock()
        packageMock.mod.manifest = moduleMock
        with mock.patch.dict("sys.modules",{"pysoase":packageMock,
                "pysoase.mod.manifest":moduleMock}):
            res = currInst.manifest
            manMock.createInstance.assert_called_once_with(
                    os.path.join(co_files.BUNDLE_PATH,"galaxy.manifest"))
            self.assertEqual(res,fileMock)

    def testCreationGalDef(self):
        """ Test whether GalDef is created correctly
        """
        galDefMock = mock.create_autospec(scen.GalaxyScenarioDef,spec_set=True)
        mockCreate = mock.create_autospec(scen.GalaxyScenarioDef.createInstance,
                spec_set=True,return_value=galDefMock)
        with mock.patch(
                "pysoase.mod.scenarios.GalaxyScenarioDef.createInstance",
                new=mockCreate
        ) as mocked:
            res = self.inst.galDef
            self.assertEqual(res,galDefMock)
            mocked.assert_called_once_with(os.path.join(self.modpath,
                    "GameInfo","GalaxyScenarioDef.galaxyScenarioDef"))

    def testCreationGalDefReb(self):
        """ Test whether GalDef is created correctly from vanilla file
        """
        currMod = tco.genMockMod("./comp_empty",self.rebrefpath)
        inst = scen.ModScenarios(currMod)
        galDefMock = mock.create_autospec(scen.GalaxyScenarioDef,spec_set=True)
        mockCreate = mock.create_autospec(scen.GalaxyScenarioDef.createInstance,
                spec_set=True,return_value=galDefMock)
        with mock.patch(
                "pysoase.mod.scenarios.GalaxyScenarioDef.createInstance",
                new=mockCreate
        ) as mocked:
            res = inst.galDef
            self.assertEqual(res,galDefMock)
            mocked.assert_called_once_with(os.path.join(self.rebrefpath,
                    "GameInfo","GalaxyScenarioDef.galaxyScenarioDef"))

    def testCreationGalDefMissing(self):
        """ Test whether missing GalDef is loaded from bundled files
        """
        currMod = tco.genMockMod("./comp_empty")
        inst = scen.ModScenarios(currMod)
        galDefMock = mock.create_autospec(scen.GalaxyScenarioDef,spec_set=True)
        mockCreate = mock.create_autospec(scen.GalaxyScenarioDef.createInstance,
                spec_set=True,return_value=galDefMock)
        with mock.patch(
                "pysoase.mod.scenarios.GalaxyScenarioDef.createInstance",
                new=mockCreate
        ) as mocked:
            res = inst.galDef
            self.assertEqual(res,galDefMock)
            mocked.assert_called_once_with(os.path.join(
                    co_files.BUNDLE_PATH,
                    "GameInfo","GalaxyScenarioDef.galaxyScenarioDef"))

class ModScenarioLoading(unit.TestCase):
    desc = "Tests ModScenario Loading:"

    @classmethod
    def setUpClass(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.rebpath = os.path.abspath("./compreb")
        cls.subpath = os.path.join(cls.modpath,"Galaxy")
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebpath)

    def setUp(self):
        self.mockGal = mock.create_autospec(scen.Galaxy,spec_set=True)
        self.mockCreate = mock.create_autospec(scen.Galaxy.createInstance,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.scenarios.Galaxy.createInstance",
                new=self.mockCreate)
        patcher.start()
        patcher = mock.patch("pysoase.mod.scenarios.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.inst = scen.ModScenarios(self.mmod)

    def testLoadGalaxy(self):
        """ Test whether galaxy file is loaded correctly
        """
        self.mockCreate.return_value = self.mockGal
        res = self.inst.loadGalaxy("gal1.galaxy")
        self.assertEqual(res,[])
        self.assertEqual(self.inst.compFilesMod["gal1.galaxy"],self.mockGal)
        self.mockCreate.assert_called_once_with(os.path.join(self.subpath,
                "gal1.galaxy"))

    def testLoadGalaxyMissing(self):
        """ Test whether missing galaxy return appropriate problem
        """
        self.mockCreate.return_value = self.mockGal
        res = self.inst.loadGalaxy("missing.galaxy")
        self.assertEqual(len(res),1)
        self.assertIsInstance(res[0],co_probs.MissingRefProblem)
        self.assertEqual(len(self.inst.compFilesMod["missing.galaxy"]),1)
        self.assertIsInstance(self.inst.compFilesMod["missing.galaxy"][0],
                co_probs.MissingRefProblem)
        self.assertEqual(self.mockCreate.call_count,0)

    def testLoadGalaxyMalformed(self):
        """ Test whether malformed galaxy file returns appropriate problem
        """
        self.mockCreate.return_value = [mock.sentinel.prob]
        res = self.inst.loadGalaxy("gal1.galaxy")
        self.assertEqual(res,[mock.sentinel.prob])
        self.assertEqual(self.inst.compFilesMod["gal1.galaxy"],
                [mock.sentinel.prob])
        self.mockCreate.assert_called_once_with(os.path.join(self.subpath,
                "gal1.galaxy"))

    def testLoadAll(self):
        """ Test whether loadAll loads all galaxies
        """
        calls = [mock.call(os.path.join(self.subpath,"gal1.galaxy")),
            mock.call(os.path.join(self.subpath,"gal2.galaxy"))]
        self.mockCreate.return_value = self.mockGal
        res = self.inst.loadAll()
        self.assertEqual(res,[])
        self.assertEqual(self.mockCreate.call_count,2)
        self.mockCreate.assert_has_calls(calls,any_order=True)
        self.assertEqual(len(self.inst.compFilesMod),2)
        self.assertEqual(self.inst.compFilesMod["gal1.galaxy"],self.mockGal)
        self.assertEqual(self.inst.compFilesMod["gal2.galaxy"],self.mockGal)

class ModScenarioCheckTests(unit.TestCase):
    desc = "Tests ModScenario checking:"

    @classmethod
    def setUpClass(cls):
        cls.modpath = os.path.abspath("./comp")
        cls.rebpath = os.path.abspath("./compreb")
        cls.subpath = os.path.join(cls.modpath,"Galaxy")
        cls.mmod = tco.genMockMod(cls.modpath,cls.rebpath)

    def setUp(self):
        self.mockGal = mock.create_autospec(scen.Galaxy,spec_set=True)
        self.mockCreate = mock.create_autospec(scen.Galaxy.createInstance,
                spec_set=True)
        patcher = mock.patch("pysoase.mod.scenarios.Galaxy.createInstance",
                new=self.mockCreate)
        patcher.start()
        self.addCleanup(mock.patch.stopall)
        self.inst = scen.ModScenarios(self.mmod)
        self.mockMan = mock.MagicMock(entries=mock.MagicMock(elements=[
            mock.MagicMock(ref="gal1.galaxy")]))
        self.mockMan.mock_add_spec(manifest.ManifestGalaxy,
                spec_set=True)
        self.mockMan.checkEntry.return_value = []
        self.mock1 = mock.create_autospec(scen.Galaxy,spec_set=True)
        self.inst.compFilesMod["gal1.galaxy"] = self.mock1
        self.mock2 = mock.create_autospec(scen.Galaxy,spec_set=True)
        self.inst.compFilesMod["gal2.galaxy"] = self.mock2
        self.inst._manifest = self.mockMan
        patcher = mock.patch("pysoase.mod.scenarios.tqdm.tqdm",autospec=True,
                spec_set=True)
        patcher.start()

    def testCheckGalaxy(self):
        """ Test whether problems from galaxies are returned
        """
        self.mock1.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkGalaxy("gal1.galaxy")
        self.assertEqual(res,[mock.sentinel.prob])
        self.mock1.check.assert_called_once_with(self.mmod)

    def testCheckGalaxyManifest(self):
        """ Test whether checkGalaxy checks manifest entry
        """
        self.mock1.check.return_value = []
        self.mockMan.checkEntry.return_value = [mock.sentinel.prob]
        res = self.inst.checkGalaxy("gal1.galaxy")
        self.assertEqual(res,[mock.sentinel.prob])
        self.mockMan.checkEntry.assert_called_once_with("gal1.galaxy")

    def testCheckGalaxyLoadingProb(self):
        """ Check whether loading problems are returned correctly from checkGal
        """
        # loadGal = mock.create_autospec(self.inst.loadGalaxy,spec_set=True)
        self.inst.compFilesMod["gal1.galaxy"] = [mock.sentinel.prob]
        with mock.patch.object(self.inst,"loadGalaxy",spec_set=True,
                autospec=True,
                return_value=[mock.sentinel.prob]):
            res = self.inst.checkGalaxy("gal1.galaxy")
            self.assertEqual(res,[mock.sentinel.prob])

    def testCheckGalaxyPreloadLoadingProb(self):
        """ Check whether preload problems are returned correctly from checkGal
        """
        self.inst.compFilesMod["gal1.galaxy"] = None
        self.mockCreate.return_value = [mock.sentinel.prob]
        self.inst.loadGalaxy("gal1.galaxy")
        res = self.inst.checkGalaxy("gal1.galaxy")
        self.assertEqual(res,[mock.sentinel.prob])
