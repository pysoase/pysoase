""" Unit tests for the pysoase.mod.constants module.
"""

import os
from unittest import mock

import pyparsing as pp
import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.misc as co_misc
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe
import pysoase.mod.constants as consts
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","constants")

"""########################## Tests RequiredShips ##########################"""

@pytest.fixture(scope="module")
def reqShipsFixture():
    parseString = (
        "requiredShip\n"
        +"\ttype \"testtype\"\n"
        +"\tminCount 2\n"
        +"\tmaxCount 3\n"
    )
    parseDict = {
        "identifier":"requiredShip",
        "shipType":{"identifier":"type","val":"testtype"},
        "shipsMin":{"identifier":"minCount","val":2},
        "shipsMax":{"identifier":"maxCount","val":3}
    }
    return parseString,parseDict

class TestRequiredShips:
    desc = "Tests RequiredShips:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,reqShipsFixture):
        cls.parseString,parseDict = reqShipsFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = consts.RequiredShips(
                identifier=self.identifier,
                shipType=self.shipType,
                shipsMin=self.shipsMin,
                shipsMax=self.shipsMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = consts.g_spawnRequiredShips.parseString(self.parseString)[0]
        res = consts.RequiredShips(**parseRes)
        assert res == self.inst

    def testCreationAttribShipType(self):
        """ Test whether shipType attrib is created correctly
        """
        exp = coe.EntityRef(types=["Frigate","CapitalShip","Titan"],
                **self.shipType)
        assert self.inst.shipType == exp

    def testCreationAttribShipsMin(self):
        """ Test whether shipsMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.shipsMin)
        assert self.inst.shipsMin == exp

    def testCreationAttribShipsMax(self):
        """ Test whether shipsMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.shipsMax)
        assert self.inst.shipsMax == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.shipType,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckShipType(self):
        """ Test whether check returns from shipType attribute
        """
        with mock.patch.object(self.inst.shipType,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################### Tests RandomShip ############################"""

@pytest.fixture(scope="module")
def randShipFixture():
    parseString = (
        "randomShip\n"
        +"\ttype \"testtype\"\n"
        +"\tweight 2.300000\n"
    )
    parseDict = {
        "identifier":"randomShip",
        "shipType":{"identifier":"type","val":"testtype"},
        "weight":{"identifier":"weight","val":2.3}
    }
    return parseString,parseDict

class TestRandomShip:
    desc = "Tests RandomShip:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,randShipFixture):
        cls.parseString,parseDict = randShipFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = consts.RandomShip(
                identifier=self.identifier,
                shipType=self.shipType,
                weight=self.weight
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = consts.g_spawnRandomShips.parseString(self.parseString)[0]
        res = consts.RandomShip(**parseRes)
        assert res == self.inst

    def testCreationAttribShipType(self):
        """ Test whether shipType attrib is created correctly
        """
        exp = coe.EntityRef(types=["Frigate","CapitalShip","Titan"],
                **self.shipType)
        assert self.inst.shipType == exp

    def testCreationAttribWeight(self):
        """ Test whether weight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.weight)
        assert self.inst.weight == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.shipType,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckShipType(self):
        """ Test whether check returns from shipType attribute
        """
        with mock.patch.object(self.inst.shipType,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################### Tests ShipComposition #########################"""

@pytest.fixture(scope="module")
def shipCompFixture(reqShipsFixture,randShipFixture):
    randStr,randDict = randShipFixture
    reqStr,reqDict = reqShipsFixture
    parseString = (
        "requiredShipCount 1\n"
        +reqStr
        +"randomShipCount 1\n"
        +randStr
    )
    parseDict = {
        "requiredShips":{
            "counter":{"identifier":"requiredShipCount","val":1},
            "elements":[reqDict]
        },
        "randomShips":{
            "counter":{"identifier":"randomShipCount","val":1},
            "elements":[randDict]
        }
    }
    return parseString,parseDict

class TestShipCompositionNoHeading:
    desc = "Tests ShipComposition no heading:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def setUpClass(cls,shipCompFixture):
        cls.parseString,parseDict = shipCompFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = consts.ShipComposition(
                requiredShips=self.requiredShips,
                randomShips=self.randomShips
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = consts.g_shipRequirements.parseString(self.parseString)[0]
        res = consts.ShipComposition(**parseRes)
        assert res == self.inst

    def testCreationAttribRequiredShips(self):
        """ Test whether requiredShips attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=consts.RequiredShips,
                **self.requiredShips)
        assert self.inst.requiredShips == exp

    def testCreationAttribRandomShips(self):
        """ Test whether randomShips attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=consts.RandomShip,
                **self.randomShips)
        assert self.inst.randomShips == exp

    @pytest.fixture()
    def checkFixture(self):
        patcherReq = mock.patch.object(self.inst.requiredShips,"check",
                autospec=True,spec_set=True,return_value=[])
        mReq = patcherReq.start()
        patcherRand = mock.patch.object(self.inst.randomShips,"check",
                autospec=True,spec_set=True,return_value=[])
        mRand = patcherRand.start()
        yield mReq,mRand
        patcherReq.stop()
        patcherRand.stop()

    def testCheck(self,checkFixture):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckRequiredShips(self,checkFixture):
        """ Test whether check returns problems from requiredShips attribute
        """
        mReq,_ = checkFixture
        mReq.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]

    def testCheckRandomShips(self,checkFixture):
        """ Test whether check returns problems from randomShips attribute
        """
        _,mRand = checkFixture
        mRand.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestShipCompositionHeading:
    desc = "Tests ShipComposition with heading:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,shipCompFixture):
        compString,compDict = shipCompFixture
        cls.parseString = (
            "ident\n"
            +co_misc.indentText(compString,1)
        )
        cls.identifier = "ident"
        tco.expandParseDict(cls,compDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = consts.ShipComposition(
                identifier=self.identifier,
                requiredShips=self.requiredShips,
                randomShips=self.randomShips
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = co_parse.genHeading("ident")+pp.ungroup(consts.g_shipRequirements)
        parseRes = parser.parseString(self.parseString)
        res = consts.ShipComposition(**parseRes)
        assert res == self.inst

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests ShipSpawn ############################"""

@pytest.fixture(scope="module")
def shipSpawnFixture(shipCompFixture):
    compStr,compDict = shipCompFixture
    parseString = (
        "spawnShips\n"
        +"\tminFleetPoints 4\n"
        +"\tmaxFleetPoints 8\n"
        +co_misc.indentText(compStr,1)
    )
    parseDict = {
        "identifier":"spawnShips",
        "ships":compDict,
        "fleetPointsMin":{"identifier":"minFleetPoints","val":4},
        "fleetPointsMax":{"identifier":"maxFleetPoints","val":8}
    }
    return parseString,parseDict

class TestShipSpawn:
    desc = "Tests ShipSpawn:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,shipSpawnFixture):
        cls.parseString,parseDict = shipSpawnFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = consts.ShipSpawn(
                identifier=self.identifier,
                fleetPointsMin=self.fleetPointsMin,
                fleetPointsMax=self.fleetPointsMax,
                ships=self.ships
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = consts.g_spawnShips.parseString(self.parseString)[0]
        res = consts.ShipSpawn(**parseRes)
        assert res == self.inst

    def testCreationAttribFleetPointsMin(self):
        """ Test whether fleetPointsMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.fleetPointsMin)
        assert self.inst.fleetPointsMin == exp

    def testCreationAttribFleetPointsMax(self):
        """ Test whether fleetPointsMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.fleetPointsMax)
        assert self.inst.fleetPointsMax == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.ships,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckShips(self):
        """ Test whether check returns problems from ships attribute
        """
        with mock.patch.object(self.inst.ships,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString
