""" Unit tests for the pysoase.mod.visuals module
"""

import collections
import os
from textwrap import indent
from unittest import mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui
import pysoase.mod.visuals as vis
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","visuals")

"""########################### Tests SubExplosion ##########################"""

@pytest.fixture(scope="module")
def baseSubExplosionFixture():
    parseString = (
        "\tplayTime 0\n"
        +"\tcameraShakeMaxSize 20000.000000\n"
        +"\tcameraShakeStrength 0.500000\n"
        +"\tcameraShakeDuration 3.000000\n"
    )
    parseDict = {
        "identifier":"subExplosion",
        "playTime":{"identifier":"playTime","val":0},
        "camShakeSize":{"identifier":"cameraShakeMaxSize","val":20000.0},
        "camShakeStrength":{"identifier":"cameraShakeStrength",
            "val":0.5},
        "camShakeDuration":{"identifier":"cameraShakeDuration",
            "val":3.0}
    }
    return parseString,parseDict

class TestSubExplosion:
    desc = "Tests SubExplosion:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,baseSubExplosionFixture):
        pStr,pDict = baseSubExplosionFixture
        cls.parseString = (
            "subExplosion\n"
            +"\ttype \"Effect\"\n"
            +pStr
        )
        tco.expandParseDict(cls,pDict)
        cls.mmod = tco.genMockMod("./")
        cls.explosionType = {"identifier":"type","val":"Effect"}

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.inst = vis.SubExplosion(
                identifier=self.identifier,
                explosionType=self.explosionType,
                playTime=self.playTime,
                camShakeSize=self.camShakeSize,
                camShakeStrength=self.camShakeStrength,
                camShakeDuration=self.camShakeDuration
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parser = (
            co_parse.genHeading("subExplosion")
            +co_parse.genKeyValPair("type","Effect")("explosionType")
            +vis.g_subExplosionBasic
        )
        parseRes = parser.parseString(self.parseString)
        res = vis.SubExplosion(**parseRes)
        assert res == self.inst

    def testFactoryUnknownType(self):
        """ Test whether factory raises error for unknown explosion type
        """
        unknownType = {"identifier":"type","val":"Foo"}
        with pytest.raises(RuntimeError):
            vis.SubExplosion.factory(explosionType=unknownType)

    def testCreationAttribType(self):
        """ Test whether explosionType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=["Effect","Sound"],
                **self.explosionType)
        assert self.inst.explosionType == exp

    def testCreationAttribPlayTime(self):
        """ Test whether playTime attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.playTime)
        assert self.inst.playTime == exp

    def testCreationAttribCamShakeSize(self):
        """ Test whether camShakeSize attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.camShakeSize)
        assert self.inst.camShakeSize == exp

    def testCreationAttribCamShakeStrength(self):
        """ Test whether camShakeStrength attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.camShakeStrength)
        assert self.inst.camShakeStrength == exp

    def testCreationAttribCamShakeDuration(self):
        """ Test whether camShakeDuration attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.camShakeDuration)
        assert self.inst.camShakeDuration == exp

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correclty
        """
        assert self.inst.toString(0) == self.parseString

@pytest.fixture(scope="module")
def soundSubExplosionFixture(baseSubExplosionFixture):
    parseString,parseDict = baseSubExplosionFixture
    parseString = (
        "subExplosion\n"
        +"\ttype \"Sound\"\n"
        +parseString
        +"\texplosionSounds\n"
        +"\t\tsoundCount 1\n"
        +"\t\tsound \"testsound\"\n"
    )
    sndDict = {
        "explosionType":{"identifier":"type","val":"Sound"},
        "sounds":{
            "identifier":"explosionSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"testsound"}
            ]
        }
    }
    sndDict.update(parseDict)
    return parseString,sndDict

class TestSoundSubExplosion:
    desc = "Tests SoundSubExplosion:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,soundSubExplosionFixture):
        cls.parseString,parseDict = soundSubExplosionFixture
        tco.expandParseDict(cls,parseDict)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.SoundSubExplosion(
                identifier=self.identifier,
                explosionType=self.explosionType,
                playTime=self.playTime,
                camShakeSize=self.camShakeSize,
                camShakeStrength=self.camShakeStrength,
                camShakeDuration=self.camShakeDuration,
                sounds=self.sounds
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = vis.g_subExplosion.parseString(self.parseString)[0]
        res = vis.SubExplosion.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribSounds(self):
        """ Test whether sounds attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=audio.SoundRef,**self.sounds)
        assert self.inst.sounds == exp

    def testCheckSounds(self):
        """ Test whether check returns problems from sound attrib
        """
        mmod = tco.genMockMod("./")
        mmod.audio.checkEffect.return_value = []
        with mock.patch.object(self.inst.sounds,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(mmod)
            assert res == [mock.sentinel.prob]
            mocked.assert_called_once_with(mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

@pytest.fixture(scope="module")
def effectSubExplosionFixture(baseSubExplosionFixture):
    parseString,parseDict = baseSubExplosionFixture
    parseString = (
        "subExplosion\n"
        +"\ttype \"Effect\"\n"
        +parseString
        +"\tlocation \"Surface\"\n"
        +"\tparticleSystemName \"particles\"\n"
        +"\texplosionSounds\n"
        +"\t\tsoundCount 1\n"
        +"\t\tsound \"testsound\"\n"
    )
    effDict = {
        "explosionType":{"identifier":"type","val":"Effect"},
        "location":{"identifier":"location","val":"Surface"},
        "particleSys":{"identifier":"particleSystemName","val":"particles"},
        "sounds":{
            "identifier":"explosionSounds",
            "counter":{"identifier":"soundCount","val":1},
            "elements":[
                {"identifier":"sound","val":"testsound"}
            ]
        }
    }
    effDict.update(parseDict)
    return parseString,effDict

class TestEffectSubExplosion:
    desc = "Tests EffectSubExplosion:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,effectSubExplosionFixture):
        cls.parseString,parseDict = effectSubExplosionFixture
        tco.expandParseDict(cls,parseDict)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.EffectSubExplosion(
                identifier=self.identifier,
                explosionType=self.explosionType,
                playTime=self.playTime,
                camShakeSize=self.camShakeSize,
                camShakeStrength=self.camShakeStrength,
                camShakeDuration=self.camShakeDuration,
                location=self.location,
                particleSys=self.particleSys,
                sounds=self.sounds
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = vis.g_subExplosion.parseString(self.parseString)[0]
        res = vis.SubExplosion.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribLocation(self):
        """ Test whether location attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=vis.EFFECT_LOCATION,
                **self.location)
        assert self.inst.location == exp

    def testCreationAttribParticleSys(self):
        """ Test whether particleSys attrib is created correctly
        """
        exp = par.ParticleRef(**self.particleSys)
        assert self.inst.particleSys == exp

    def testCheckParticleSys(self):
        """ Test whether check returns problems from particleSys attrib
        """
        mmod = tco.genMockMod("./")
        mmod.audio.checkEffect.return_value = []
        with mock.patch.object(self.inst.particleSys,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(mmod)
            assert res == [mock.sentinel.prob]
            mocked.assert_called_once_with(mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests ExplosionEffect ########################"""

@pytest.fixture(scope="module")
def explosionEffectFixture(effectSubExplosionFixture,soundSubExplosionFixture):
    effString,effDict = effectSubExplosionFixture
    sndString,sndDict = soundSubExplosionFixture
    parseString = (
        "explosionEffectDef\n"
        +"\ttotalPlayTime 15\n"
        +"\tmakeMeshInvisibleTime 6\n"
        +"\tsubExplosionCount 2\n"
        +indent(effString,"\t")
        +indent(sndString,"\t")
    )
    parseDict = {
        "identifier":"explosionEffectDef",
        "totalTime":{"identifier":"totalPlayTime","val":15},
        "meshInvisibleTime":{"identifier":"makeMeshInvisibleTime","val":6},
        "subExplosions":{
            "counter":{"identifier":"subExplosionCount","val":2},
            "elements":[effDict,sndDict]
        }
    }
    return parseString,parseDict

class TestExplosionEffect:
    desc = "Tests ExplosionEffect:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,explosionEffectFixture):
        cls.parseString,parseDict = explosionEffectFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.ExplosionEffect(
                identifier=self.identifier,
                totalTime=self.totalTime,
                meshInvisibleTime=self.meshInvisibleTime,
                subExplosions=self.subExplosions
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = vis.g_explosionEffect.parseString(self.parseString)[0]
        res = vis.ExplosionEffect(**parseRes)
        assert res == self.inst

    def testCreationAttribTotalTime(self):
        """ Test whether totalTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.totalTime)
        assert self.inst.totalTime == exp

    def testCreationAttribMeshInvisibleTime(self):
        """ Test whether meshInvisibleTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.meshInvisibleTime)
        assert self.inst.meshInvisibleTime == exp

    def testCreationAttribSubExplosions(self):
        """ Test whether subExplosions attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=vis.SubExplosion,factory=True,
                **self.subExplosions)
        assert self.inst.subExplosions == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occur
        """
        with mock.patch.object(self.inst.subExplosions,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckSubExplosions(self):
        """ Test whether check returns problems from subExplosions attrib
        """
        with mock.patch.object(self.inst.subExplosions,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################## Tests ExplosionEffectGroup #####################"""

@pytest.fixture(scope="module")
def explosionEffectGroupFixture(explosionEffectFixture):
    parseString,effectsDict = explosionEffectFixture
    parseString = (
        "explosionEffectGroup\n"
        +"\tgroupName \"CapitalShip0\"\n"
        +"\texplosionEffectDefCount 1\n"
        +indent(parseString,"\t")
    )
    parseDict = {
        "identifier":"explosionEffectGroup",
        "name":{"identifier":"groupName","val":"CapitalShip0"},
        "effects":{
            "counter":{"identifier":"explosionEffectDefCount","val":1},
            "elements":[effectsDict]
        }
    }
    return parseString,parseDict

class TestExplosionEffectGroup:
    desc = "Tests ExplosionEffectGroup:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,explosionEffectGroupFixture):
        cls.parseString,parseDict = explosionEffectGroupFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.ExplosionEffectGroup(
                identifier=self.identifier,
                name=self.name,
                effects=self.effects
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = vis.g_explosionEffectGroup.parseString(self.parseString)[0]
        res = vis.ExplosionEffectGroup(**parseRes)
        assert res == self.inst

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        assert self.inst.name == exp

    def testCreationAttribEffects(self):
        """ Test whether effects attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=vis.ExplosionEffect,**self.effects)
        assert self.inst.effects == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.effects,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckEffects(self):
        """ Test whether check returns problems from effects attribute
        """
        with mock.patch.object(self.inst.effects,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################# Tests Explosions ##########################"""

class TestExplosions:
    desc = "Tests Explosions:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,explosionEffectGroupFixture):
        _,effDict = explosionEffectGroupFixture
        cls.filepath = "Explosions.explosiondata"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.explosions = {
            "counter":{"identifier":"explosionEffectGroupCount","val":1},
            "elements":[effDict]
        }
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.Explosions(
                filepath=self.filepath,
                explosions=self.explosions
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = vis.Explosions.createInstance(self.filepath)
        assert res == self.inst

    def testCreationParseMalformed(self):
        """ Test whether parse problem is returned for malformed file
        """
        res = vis.Explosions.createInstance("ExplosionsMalformed.explosiondata")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCreationAttribExplosions(self):
        """ Test whether explosions attrib is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=vis.ExplosionEffectGroup,
                keyfunc=lambda val:val.name.value,
                **self.explosions
        )
        assert self.inst.explosions == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.explosions,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckExplosions(self):
        """ Test whether check returns problems from explosions attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.ParseProblem,spec_set=True)
        with mock.patch.object(self.inst.explosions,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mockProb]
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckExistence(self):
        """ Test whether checkExplosionExistence finds existing explosion
        """
        assert self.inst.checkExplosionExistence("CapitalShip0")

    def testCheckExistenceMissing(self):
        """ Test whether checkExplosionExistence returns false for missing exp
        """
        assert not self.inst.checkExplosionExistence("Foo")

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests ExplosionRef #########################"""

class TestExplosionRef:
    desc = "Tests ExplosionRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"Testref\"\n"
        cls.identifier = "ident"
        cls.mmod = tco.genMockMod("./")
        cls.val = "Testref"

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.ExplosionRef(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = vis.ExplosionRef(**parseRes)
        assert res == self.inst

    @pytest.fixture()
    def mockExplosionFixture(self):
        mockExplosion = mock.create_autospec(vis.Explosions,spec_set=True)
        mockExplosion.checkExplosionExistence.return_value = True
        self.mmod.explosions = mockExplosion
        return mockExplosion

    def testCheckExists(self,mockExplosionFixture):
        """ Test whether check returns no problem for existing explosion
        """
        res = self.inst.check(self.mmod)
        assert res == []
        mockExplosionFixture.checkExplosionExistence.assert_called_once_with(
                self.val)

    def testCheckMissing(self,mockExplosionFixture):
        """ Test whether check returns problem for missing explosion
        """
        mockExplosion = mockExplosionFixture
        mockExplosion.checkExplosionExistence.return_value = False
        res = self.inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        mockExplosion.checkExplosionExistence.assert_called_once_with(
                self.val)

"""############################ Tests Starscape ############################"""

class TestStarscape:
    desc = "Tests Starscape:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.filepath = "Starscape.starscapedata"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.starWidth = {"identifier":"starWidth","val":4.0}
        cls.starHeight = {"identifier":"starHeight","val":4.0}
        cls.starDist = {"identifier":"starDistance","val":200.0}
        cls.starBlinksPerSec = {"identifier":"starBlinksPerSecond","val":20.0}
        cls.starFadeDuration = {"identifier":"starFadeDuration","val":0.15}
        cls.starTextures = [
            {"identifier":"starTextureName:0","val":"text0"},
            {"identifier":"starTextureName:1","val":"text1"},
            {"identifier":"starTextureName:2","val":""},
            {"identifier":"starTextureName:3","val":""},
            {"identifier":"starTextureName:4","val":""},
            {"identifier":"starTextureName:5","val":""},
            {"identifier":"starTextureName:6","val":""},
            {"identifier":"starTextureName:7","val":""},
            {"identifier":"starTextureName:8","val":""},
            {"identifier":"starTextureName:9","val":""}
        ]
        cls.expRefs = collections.defaultdict(set)
        cls.expRefs["texture"] = {"text1","text0"}
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.Starscape(
                filepath=self.filepath,
                starWidth=self.starWidth,
                starHeight=self.starHeight,
                starDist=self.starDist,
                starBlinksPerSec=self.starBlinksPerSec,
                starFadeDuration=self.starFadeDuration,
                starTextures=self.starTextures
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = vis.Starscape.createInstance(self.filepath)
        assert res == self.inst

    def testCreationAttribStarWidth(self):
        """ Test whether starWidth attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.starWidth)
        assert self.inst.starWidth == exp

    def testCreationAttribStarHeight(self):
        """ Test whether starHeight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.starHeight)
        assert self.inst.starHeight == exp

    def testCreationAttribStarDist(self):
        """ Test whether starDist attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.starDist)
        assert self.inst.starDist == exp

    def testCreationAttribStarBlinkPerSec(self):
        """ Test whether starBlinksPerSec attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.starBlinksPerSec)
        assert self.inst.starBlinksPerSec == exp

    def testCreationAttribStarFadeDuration(self):
        """ Test whether starFadeDuration attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.starFadeDuration)
        assert self.inst.starFadeDuration == exp

    def testCreationAttribStarTextures(self):
        """ Test whether starTextures attrib is created correctly
        """
        exp = tuple(map(
                lambda vals:ui.UITextureRef(canBeEmpty=True,**vals),
                self.starTextures))
        assert self.inst.starTextures == exp

    def testGatherRefsAll(self):
        """ Test whether all reference types are gathered correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res)
        assert res == self.expRefs

    def testGatherRefsTextures(self):
        """ Test whether texture reference type is gathered correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,types={"texture"})
        assert res == self.expRefs

    @pytest.fixture()
    def mockTextFixture(self):
        textmocks = []
        patchers = []
        for m in self.inst.starTextures:
            patcher = mock.patch.object(m,"check",autospec=True,spec_set=True,
                    return_value=[])
            textmocks.append(patcher.start())
            patchers.append(patcher)
        yield textmocks
        for p in patchers:
            p.stop()

    def testCheck(self,mockTextFixture):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckStarTextures(self,mockProb,mockTextFixture):
        """ Test whether check returns problems from starTextures attrib
        """
        textmocks = mockTextFixture
        textmocks[0].return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]

    def testGetRefs(self):
        """ Test whether getRefs returns all references
        """
        res = self.inst.getRefs()
        assert len(res) == 10
        assert all(t in res for t in self.inst.starTextures)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################### Tests SkyboxProperties ########################"""

@pytest.fixture(scope="module")
def skyboxPropertiesFixture():
    parseString = (
            "properties\n"
            +"\tmeshName \"testmesh\"\n"
            +"\tdiffuseLiteColor ffddffff\n"
            +"\tdiffuseDarkColor ff6398d4\n"
            +"\tambientLiteColor ff22303e\n"
            +"\tambientDarkColor ff151a1d\n"
            +"\tspecularColor ffeaf9fa\n"
            +"\tdustCloudColor ff271a14\n"
            +"\tnumFarStarIconCloudTextureNames 1\n"
            +"\ttextureName \"testtex\"\n"
            +"\tenvironmentMapName \"environmap.dds\"\n"
            +"\tenvironmentIlluminationMapName \"environillummap.dds\"\n"
        )
    parseDict = {
        "identifier":"properties",
        "mesh":{"identifier":"meshName","val":"testmesh"},
        "diffuseLiteCol":{"identifier":"diffuseLiteColor","val":"ffddffff"},
        "diffuseDarkCol":{"identifier":"diffuseDarkColor","val":"ff6398d4"},
        "ambientLiteCol":{"identifier":"ambientLiteColor","val":"ff22303e"},
        "ambientDarkCol":{"identifier":"ambientDarkColor","val":"ff151a1d"},
        "specularColor":{"identifier":"specularColor","val":"ffeaf9fa"},
        "dustColor":{"identifier":"dustCloudColor","val":"ff271a14"},
        "farStarTextures":{
            "counter":{"identifier":"numFarStarIconCloudTextureNames",
                "val":1},
            "elements":[{"identifier":"textureName","val":"testtex"}]
        },
        "environMap":{"identifier":"environmentMapName","val":"environmap.dds"},
        "environIllumMap":{"identifier":"environmentIlluminationMapName",
            "val":"environillummap.dds"}
    }
    return parseString,parseDict


class TestSkyboxProperties:
    desc = "Tests SkyboxProperties:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,skyboxPropertiesFixture):
        cls.parseString,parseDict = skyboxPropertiesFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.SkyboxProperties(
                identifier=self.identifier,
                mesh=self.mesh,
                diffuseLiteCol=self.diffuseLiteCol,
                diffuseDarkCol=self.diffuseDarkCol,
                ambientLiteCol=self.ambientLiteCol,
                ambientDarkCol=self.ambientDarkCol,
                specularColor=self.specularColor,
                dustColor=self.dustColor,
                farStarTextures=self.farStarTextures,
                environMap=self.environMap,
                environIllumMap=self.environIllumMap
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = vis.g_skyboxProperties.parseString(self.parseString)[0]
        res = vis.SkyboxProperties(**parseRes)
        assert res == self.inst

    def testCreationAttribMesh(self):
        """ Test whether mesh attrib is created correctly
        """
        exp = meshes.MeshRef(**self.mesh)
        assert self.inst.mesh == exp

    def testCreationAttribDiffuseLiteCol(self):
        """ Test whether diffuseLiteCol attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.diffuseLiteCol)
        assert self.inst.diffuseLiteCol == exp

    def testCreationAttribDiffuseDarkCol(self):
        """ Test whether diffuseDarkCol attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.diffuseDarkCol)
        assert self.inst.diffuseDarkCol == exp

    def testCreationAttribAmbientLiteCol(self):
        """ Test whether ambientLiteCol attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.ambientLiteCol)
        assert self.inst.ambientLiteCol == exp

    def testCreationAttribAmbientDarkCol(self):
        """ Test whether ambientDarkCol attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.ambientDarkCol)
        assert self.inst.ambientDarkCol == exp

    def testCreationAttribSpecularColor(self):
        """ Test whether specularColor attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.specularColor)
        assert self.inst.specularColor == exp

    def testCreationAttribDustColor(self):
        """ Test whether dustColor attrib is created correctly
        """
        exp = co_attribs.AttribColor(**self.dustColor)
        assert self.inst.dustColor == exp

    def testCreationAttribFarStarTextures(self):
        """ Test whether farStarTextures attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=ui.UITextureRef,**self.farStarTextures)
        assert self.inst.farStarTextures == exp

    def testCreationAttribEnvironMap(self):
        """ Test whether environMap attrib is created correctly
        """
        exp = ui.UITextureRef(**self.environMap)
        assert self.inst.environMap == exp

    def testCreationAttribEnvironIllumMap(self):
        """ Test whether environIllumMap attrib is created correctly
        """
        exp = ui.UITextureRef(**self.environIllumMap)
        assert self.inst.environIllumMap == exp

    @pytest.fixture()
    def checkFixture(self):
        patcherMesh = mock.patch.object(self.inst.mesh,"check",autospec=True,
                spec_set=True,return_value=[])
        self.mockMesh = patcherMesh.start()
        patcherTex = mock.patch.object(self.inst.farStarTextures,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTex = patcherTex.start()
        patcherEnviron = mock.patch.object(self.inst.environMap,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockEnviron = patcherEnviron.start()
        patcherEnvironIllum = mock.patch.object(self.inst.environIllumMap,
                "check",autospec=True,spec_set=True,return_value=[])
        self.mockEnvironIllum = patcherEnvironIllum.start()
        yield
        patcherMesh.stop()
        patcherTex.stop()
        patcherEnviron.stop()
        patcherEnvironIllum.stop()

    def testCheck(self,checkFixture):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckMesh(self,checkFixture):
        """ Test whether check returns problems from mesh attrib
        """
        self.mockMesh.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockMesh.assert_called_once_with(self.mmod,False)

    def testCheckFarStarTextures(self,checkFixture):
        """ Test whether check returns problems from farStarTextures attrib
        """
        self.mockTex.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockTex.assert_called_once_with(self.mmod,False)

    def testCheckEnvironMap(self,checkFixture):
        """ Test whether check returns problems from environMap attrib
        """
        self.mockEnviron.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockEnviron.assert_called_once_with(self.mmod,False)

    def testCheckEnvironIllumMap(self,checkFixture):
        """ Test whether check returns problems from environIllumMap attrib
        """
        self.mockEnvironIllum.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockEnvironIllum.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################### Tests SkyboxBackdrops #########################"""

class TestSkyboxBackdrops:
    desc = "Tests SkyboxBackdrops:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,skyboxPropertiesFixture):
        _,propDict = skyboxPropertiesFixture
        cls.filepath = "SkyBoxProperties.skyboxbackdropdata"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.properties = {
            "counter":{"identifier":"numProperties","val":1},
            "elements":[propDict]
        }
        cls.mmod = tco.genMockMod("./")
        cls.expRefs = collections.defaultdict(set)
        cls.expRefs["texture"] = {"testtex","environmap.dds",
            "environillummap.dds"}
        cls.expRefs["mesh"] = {"testmesh.mesh"}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = vis.SkyboxBackdrops(
                filepath=self.filepath,
                properties=self.properties
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = vis.SkyboxBackdrops.createInstance(self.filepath)
        assert res == self.inst

    def testCreationAttribProperties(self):
        """ Test whether properties attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=vis.SkyboxProperties,**self.properties)
        assert self.inst.properties == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.properties,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckProperties(self):
        """ Test whether check returns problems from properties attrib
        """
        mockProb = mock.MagicMock(probFile=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        with mock.patch.object(self.inst.properties,"check",autospec=True,
                spec_set=True,return_value=[mockProb]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == [mockProb]
            mocked.assert_called_once_with(self.mmod,False)

    def testGatherRefsAll(self):
        """ Test whether gatherRefs returns all ref types correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res)
        assert res == self.expRefs

    def testGatherRefsTextures(self):
        """ Test whether gatherRefs returns texture refs correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,{"texture"})
        assert res["texture"] == self.expRefs["texture"]
        assert len(res) == 1

    def testGatherRefsMeshes(self):
        """ Test whether gatherRefs returns mesh refs correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,{"mesh"})
        assert res["mesh"] == self.expRefs["mesh"]
        assert len(res) == 1

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString
