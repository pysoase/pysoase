""" Tests for the pysoase.mod.particles module.
"""

import os
from unittest import mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.texanim as anim
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","particles")
testmodule = "pysoase.mod.particles"

"""########################### Tests AttribPos3d ###########################"""

class TestAttribPos3d:
    desc = "Tests for AttribPos3d:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "Position [ 0.000000 -1.500000 700.000000 ]\n"
        cls.identifier = "Position"
        cls.val = [0.0,-1.5,700.0]
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.AttribPos3d(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instances are constructed correctly from parse res
        """
        parseRes = par.gen3DAttrib("Position").parseString(
                self.parseString)[0]
        inst = par.AttribPos3d(**parseRes)
        assert inst == self.inst

    def testCreationAttribVal(self):
        """ Test whether val attribute was constructed correctly
        """
        assert self.inst.value == self.val

    def testToString(self):
        """ Test whether string representation was constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testEquality(self):
        """ Test whether similar instances test as equal
        """
        inst1 = par.AttribPos3d(
                identifier=self.identifier,
                val=self.val
        )
        assert inst1 == self.inst

    def testDiffVal(self):
        """ Test whether instances differing in val test unequal
        """
        inst1 = par.AttribPos3d(
                identifier=self.identifier,
                val=[1.0,1.0,1.0]
        )
        assert inst1 != self.inst

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        assert res == []

"""########################### Tests Orientation ###########################"""

@pytest.fixture(scope="module")
def orientationFixt():
    parseString = (
        "Orientation\n"
        +"\t[ 1.000000 0.000000 1.000000 ]\n"
        +"\t[ 2.000000 1.000000 3.000000 ]\n"
        +"\t[ 5.000000 1.000000 3.000000 ]\n"
    )
    parseDict = {
        "identifier":"Orientation",
        "vals":[
            [1.0,0.0,1.0],
            [2.0,1.0,3.0],
            [5.0,1.0,3.0]
        ]
    }
    return parseString,parseDict

class TestOrientation:
    desc = "Tests Orientation:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,orientationFixt):
        cls.parseString,parseDict = orientationFixt
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.Orientation(
                identifier=self.identifier,
                vals=self.vals
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = par.g_orientation.parseString(self.parseString)[0]
        res = par.Orientation(**parseRes)
        assert res == self.inst

    def testCreationVals(self):
        """ Test whether vals attribute is created correctly
        """
        exp = ((1.0,0.0,1.0),(2.0,1.0,3.0),(5.0,1.0,3.0))
        assert self.inst.vals == exp

    def testCheck(self):
        """ Test whether check returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether the string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################### Tests PipelineRef ###########################"""

class TestPipelineRef:
    desc = "Tests PipelineRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"test.fx\"\n"
        cls.identifier = "ident"
        cls.val = "test.fx"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.PipelineRef(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = par.PipelineRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check finds existing file and returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

"""########################### Tests ParticleRef ###########################"""

class TestParticleRef:
    desc = "Tests ParticleRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"test.particle\"\n"
        cls.identifier = "ident"
        cls.val = "test.particle"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.ParticleRef(
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = par.ParticleRef(**parseRes)
        assert res == self.inst

    def testCheck(self):
        """ Test whether check finds existing file and returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

"""########################### Tests EmitterRef ############################"""

class TestEmitterRef:
    desc = "Test EmitterRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"Testref\"\n"
        cls.identifier = "ident"
        cls.val = "Testref"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.parent = mock.MagicMock()
        self.parent.emitters = mock.MagicMock()
        self.inst = par.EmitterRef(
                parent=self.parent,
                identifier=self.identifier,
                val=self.val
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = co_parse.genStringAttrib("ident").parseString(self.parseString)[0]
        res = par.EmitterRef(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCheckExists(self):
        """ Test whether check returns empty list for existing emitter
        """
        self.parent.emitters.__contains__.return_value = True
        res = self.inst.check(self.mmod)
        assert res == []
        self.parent.emitters.__contains__.assert_called_once_with(self.val)

    def testCheckMissing(self):
        """ Test whether check returns MissingRef for missing emitter
        """
        self.parent.emitters.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        self.parent.emitters.__contains__.assert_called_once_with(self.val)

"""########################### Tests Affectors #############################"""

@pytest.fixture(scope="module")
def affectorFixt():
    parseString = (
        "AffectorContents\n"
        +"\tName \"Testaffector\"\n"
        +"\tEnabled TRUE\n"
        +"\tStartTime 0.000000\n"
        +"\tHasInfiniteLifeTime TRUE\n"
        +"\tTotalLifeTime 0.000000\n"
        +"\tUseYoungParticleAffectThreshold FALSE\n"
        +"\tYoungParticleAffectThreshold 0.000000\n"
        +"\tUseOldParticleAffectThreshold TRUE\n"
        +"\tOldParticleAffectThreshold 3.000000\n"
        +"\tAffectAttachedParticles TRUE\n"
        +"\tnumAttachedEmitters 1\n"
        +"\tattachedEmitterName \"testemitter\"\n"
    )
    parseDict = {
        "identifier":"AffectorContents",
        "name":{"identifier":"Name","val":"Testaffector"},
        "enabled":{"identifier":"Enabled","val":True},
        "startTime":{"identifier":"StartTime","val":0.0},
        "lifetimeInfinite":{"identifier":"HasInfiniteLifeTime","val":True},
        "lifetimeTotal":{"identifier":"TotalLifeTime","val":0.0},
        "useYoungPartAffThresh":{
            "identifier":"UseYoungParticleAffectThreshold","val":False},
        "youngPartAffThresh":{"identifier":"YoungParticleAffectThreshold",
            "val":0.0},
        "useOldPartAffThresh":{
            "identifier":"UseOldParticleAffectThreshold",
            "val":True},
        "oldPartAffThresh":{"identifier":"OldParticleAffectThreshold",
            "val":3.0},
        "affectAttachedPart":{"identifier":"AffectAttachedParticles",
            "val":True},
        "attachedEmitters":{
            "counter":{"identifier":"numAttachedEmitters","val":1},
            "elements":[
                {"identifier":"attachedEmitterName","val":"testemitter"}
            ]
        }
    }
    return parseString,parseDict

@pytest.fixture()
def affParentFixt():
    parent = mock.MagicMock()
    parent.emitters = mock.MagicMock()
    parent.emitters.__contains__ = mock.MagicMock(return_value=True)
    return parent

class TestAffShared:
    desc = "Tests Shared Affector Content:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffShared(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = par.g_affShared.parseString(self.parseString)
        res = par.AffShared(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        assert self.inst.name == exp

    def testCreationAttribEnabled(self):
        """ Test whether enabled attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.enabled)
        assert self.inst.enabled == exp

    def testCreationAttribStartTime(self):
        """ Test whether startTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startTime)
        assert self.inst.startTime == exp

    def testCreationAttribLifetimeInfinite(self):
        """ Test whether lifetimeInfinite attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.lifetimeInfinite)
        assert self.inst.lifetimeInfinite == exp

    def testCreationAttribLifetimeTotal(self):
        """ Test whether lifetimeTotal attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.lifetimeTotal)
        assert self.inst.lifetimeTotal == exp

    def testCreationAttribUseYoungPartAffThresh(self):
        """ Test whether useYoungPartAffThresh attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.useYoungPartAffThresh)
        assert self.inst.useYoungPartAffThresh == exp

    def testCreationAttribYoungPartAffThresh(self):
        """ Test whether youngPartAffThresh attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.youngPartAffThresh)
        assert self.inst.youngPartAffThresh == exp

    def testCreationAttribUseOldPartAffThresh(self):
        """ Test whether useOldPartAffThresh attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.useOldPartAffThresh)
        assert self.inst.useOldPartAffThresh == exp

    def testCreationAttriboldPartAffThresh(self):
        """ Test whether oldPartAffThresh attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.oldPartAffThresh)
        assert self.inst.oldPartAffThresh == exp

    def testCreationAttribAffectAttachedPart(self):
        """ Test whether affectAttachedPart attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.affectAttachedPart)
        assert self.inst.affectAttachedPart == exp

    def testCreationAttribAttachedEmitters(self):
        """ Test whether attachedEmitters attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=par.EmitterRef,
                elemArgs={"parent":self.parent},**self.attachedEmitters)
        assert self.inst.attachedEmitters == exp

    def testCheck(self):
        """ Test whether check returns no problems
        """
        res = self.inst.check(self.mmod)
        assert res == []
        self.parent.emitters.__contains__.assert_called_once_with(
                "testemitter")

    def testCheckProbs(self):
        """ Test whether check returns problems correctly
        """
        self.parent.emitters.__contains__.return_value = False
        res = self.inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        self.parent.emitters.__contains__.assert_called_once_with(
                "testemitter")

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffJitter:
    desc = "Tests AffJitter:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tJitterForce 400.000000\n"
            "\tUseCommonForce TRUE\n"
        )
        cls.jitterForce = {"identifier":"JitterForce","val":400.0}
        cls.useCommonForce = {"identifier":"UseCommonForce","val":True}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffJitter(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                jitterForce=self.jitterForce,
                useCommonForce=self.useCommonForce
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentJitter.parseString(self.parseString)[0]
        res = par.AffJitter(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribJitterForce(self):
        """ Test whether jitterForce attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.jitterForce)
        assert self.inst.jitterForce == exp

    def testCreationAttribUseCommonForce(self):
        """ Test whether useCommonForce attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.useCommonForce)
        assert self.inst.useCommonForce == exp

    def testToString(self):
        """ Test whether string representation is created correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffInflate:
    desc = "Tests AffInflate:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tWidthInflateRate 400.000000\n"
            +"\tHeightInflateRate 500.000000\n"
        )
        cls.inflateRateWidth = {"identifier":"WidthInflateRate","val":400.0}
        cls.inflateRateHeight = {"identifier":"HeightInflateRate","val":500.0}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffInflate(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                inflateRateWidth=self.inflateRateWidth,
                inflateRateHeight=self.inflateRateHeight
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentInflate.parseString(self.parseString)[0]
        res = par.AffInflate(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribInflateRateWidth(self):
        """ Test whether inflateRateWidth attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.inflateRateWidth)
        assert self.inst.inflateRateWidth == exp

    def testCreationAttribInflateRateHeight(self):
        """ Test whether inflateRateHeight attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.inflateRateHeight)
        assert self.inst.inflateRateHeight == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffSizeOscillator:
    desc = "Tests AffSizeOscillator:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tTransitionPeriod 1.000000\n"
            +"\tBeginSizeX 0.000000\n"
            +"\tBeginSizeY 0.100000\n"
            +"\tEndSizeX 1.000000\n"
            +"\tEndSizeY 3.000000\n"
        )
        cls.transitionPeriod = {"identifier":"TransitionPeriod","val":1.0}
        cls.beginSizeX = {"identifier":"BeginSizeX","val":0.0}
        cls.beginSizeY = {"identifier":"BeginSizeY","val":0.1}
        cls.endSizeX = {"identifier":"EndSizeX","val":1.0}
        cls.endSizeY = {"identifier":"EndSizeY","val":3.0}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffSizeOscillator(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                transitionPeriod=self.transitionPeriod,
                beginSizeX=self.beginSizeX,
                beginSizeY=self.beginSizeY,
                endSizeX=self.endSizeX,
                endSizeY=self.endSizeY
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = par.g_contentSizeOscillator.parseString(self.parseString)[0]
        res = par.AffSizeOscillator(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribTransitionPeriod(self):
        """ Test whether transitionPeriod attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.transitionPeriod)
        assert self.inst.transitionPeriod == exp

    def testCreationAttribBeginSizeX(self):
        """ Test whether beginSizeX attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.beginSizeX)
        assert self.inst.beginSizeX == exp

    def testCreationAttribBeginSizeY(self):
        """ Test whether beginSizeY attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.beginSizeY)
        assert self.inst.beginSizeY == exp

    def testCreationAttribEndSizeX(self):
        """ Test whether endSizeX attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.endSizeX)
        assert self.inst.endSizeX == exp

    def testCreationAttribEndSizeY(self):
        """ Test whether endSizeY attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.endSizeY)
        assert self.inst.endSizeY == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffRotateAxis:
    desc = "Tests AffRotateAxis:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tAngularVelocity 10.000000\n"
            +"\tRadius 1.500000\n"
            +"\tAxisOfRotation [ 0.000000 0.000000 1.000000 ]\n"
            +"\tAxisOrigin [ 0.000000 0.000000 0.000000 ]\n"
        )
        cls.angSpeed = {"identifier":"AngularVelocity","val":10.0}
        cls.radius = {"identifier":"Radius","val":1.5}
        cls.axisRotation = {"identifier":"AxisOfRotation","val":[0.0,0.0,1.0]}
        cls.axisOrigin = {"identifier":"AxisOrigin","val":[0.0,0.0,0.0]}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffRotateAxis(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                angSpeed=self.angSpeed,
                radius=self.radius,
                axisRotation=self.axisRotation,
                axisOrigin=self.axisOrigin
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentAxisRotate.parseString(self.parseString)[0]
        res = par.AffRotateAxis(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribAngSpeed(self):
        """ Test whether angSpeed attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.angSpeed)
        assert self.inst.angSpeed == exp

    def testCreationAttribRadius(self):
        """ Test whether radius attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.radius)
        assert self.inst.radius == exp

    def testCreationAttribAxisRotation(self):
        """ Test whether axisRotation attribute is created correctly
        """
        exp = par.AttribPos3d(**self.axisRotation)
        assert self.inst.axisRotation == exp

    def testCreationAttribAxisOrigin(self):
        """ Test whether axisOrigin attribute is created correctly
        """
        exp = par.AttribPos3d(**self.axisOrigin)
        assert self.inst.axisOrigin == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffColorOscillator:
    desc = "Tests AffColorOscillator:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tTransitionPeriod 4.000000\n"
            +"\tStartColor ffff8000\n"
            +"\tStartAlpha 1.000000\n"
            +"\tEndColor ffc80000\n"
            +"\tEndAlpha 2.000000\n"
        )
        cls.transitionPeriod = {"identifier":"TransitionPeriod","val":4.0}
        cls.startColor = {"identifier":"StartColor","val":"ffff8000"}
        cls.startAlpha = {"identifier":"StartAlpha","val":1.0}
        cls.endColor = {"identifier":"EndColor","val":"ffc80000"}
        cls.endAlpha = {"identifier":"EndAlpha","val":2.0}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffColorOscillator(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                transitionPeriod=self.transitionPeriod,
                startColor=self.startColor,
                startAlpha=self.startAlpha,
                endColor=self.endColor,
                endAlpha=self.endAlpha
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentColorOscillator.parseString(
                self.parseString)[0]
        res = par.AffColorOscillator(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribTransitionPeriod(self):
        """ Test whether transitionPeriod attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.transitionPeriod)
        assert self.inst.transitionPeriod == exp

    def testCreationAttribStartColor(self):
        """ Test whether startColor attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.startColor)
        assert self.inst.startColor == exp

    def testCreationAttribStartAlpha(self):
        """ Test whether startAlpha attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startAlpha)
        assert self.inst.startAlpha == exp

    def testCreationAttribEndColor(self):
        """ Test whether endColor attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.endColor)
        assert self.inst.endColor == exp

    def testCreationAttribEndAlpha(self):
        """ Test whether endAlpha attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.endAlpha)
        assert self.inst.endAlpha == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

@pytest.fixture(scope="module")
def affDragFixt(affectorFixt):
    parseString,affDict = affectorFixt
    parseString += (
        "\tDragCoefficient 1.000000\n"
    )
    affDict["dragCoefficient"] = {"identifier":"DragCoefficient","val":1.0}
    return parseString,affDict

class TestAffDrag:
    desc = "Tests AffDrag:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affDragFixt):
        cls.parseString,parseDict = affDragFixt
        tco.expandParseDict(cls,parseDict)

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffDrag(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                dragCoefficient=self.dragCoefficient
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentDrag.parseString(self.parseString)[0]
        res = par.AffDrag(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribDragCoefficient(self):
        """ Test whether dragCoefficient attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.dragCoefficient)
        assert self.inst.dragCoefficient == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffFade:
    desc = "Tests AffFade:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tDoFadeOut TRUE\n"
            +"\tFadeOutTime 20.000000\n"
            +"\tDoFadeIn FALSE\n"
            +"\tFadeInTime 1.000000\n"
        )
        cls.fadeOut = {"identifier":"DoFadeOut","val":True}
        cls.fadeOutTime = {"identifier":"FadeOutTime","val":20.0}
        cls.fadeIn = {"identifier":"DoFadeIn","val":False}
        cls.fadeInTime = {"identifier":"FadeInTime","val":1.0}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffFade(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                fadeOut=self.fadeOut,
                fadeOutTime=self.fadeOutTime,
                fadeIn=self.fadeIn,
                fadeInTime=self.fadeInTime
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentFade.parseString(self.parseString)[0]
        res = par.AffFade(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribFadeOut(self):
        """ Test whether fadeOut attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.fadeOut)
        assert self.inst.fadeOut == exp

    def testCreationAttribFadeOutTime(self):
        """ Test whether fadeOutTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.fadeOutTime)
        assert self.inst.fadeOutTime == exp

    def testCreationAttribFadeIn(self):
        """ Test whether fadeIn attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.fadeIn)
        assert self.inst.fadeIn == exp

    def testCreationAttribFadeInTime(self):
        """ Test whether fadeInTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.fadeInTime)
        assert self.inst.fadeInTime == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffNearPoint:
    desc = "Tests AffNearPoint:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tPoint [ 0.000000 0.000000 0.000000 ]\n"
            +"\tDistance 10.000000\n"
        )
        cls.point = {"identifier":"Point","val":[0.0,0.0,0.0]}
        cls.dist = {"identifier":"Distance","val":10.0}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffNearPoint(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                point=self.point,
                dist=self.dist
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = par.g_contentNearPoint.parseString(self.parseString)[0]
        res = par.AffNearPoint(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribPoint(self):
        """ Test whether point attribute is created correctly
        """
        exp = par.AttribPos3d(**self.point)
        assert self.inst.point == exp

    def testCreationAttribDist(self):
        """ Test whether dist attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.dist)
        assert self.inst.dist == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestAffForce:
    desc = "Tests AffForce:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixutre(cls,affectorFixt):
        cls.parseString,parseDict = affectorFixt
        tco.expandParseDict(cls,parseDict)
        cls.parseString += (
            "\tMinForce 10000\n"
            +"\tMaxForce 20000\n"
            +"\tPoint [ 0.000000 0.000000 0.000000 ]\n"
        )
        cls.forceMin = {"identifier":"MinForce","val":10000}
        cls.forceMax = {"identifier":"MaxForce","val":20000}
        cls.vector = {"identifier":"Point","val":[0.0,0.0,0.0]}

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.AffForce(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                startTime=self.startTime,
                lifetimeInfinite=self.lifetimeInfinite,
                lifetimeTotal=self.lifetimeTotal,
                useYoungPartAffThresh=self.useYoungPartAffThresh,
                youngPartAffThresh=self.youngPartAffThresh,
                useOldPartAffThresh=self.useOldPartAffThresh,
                oldPartAffThresh=self.oldPartAffThresh,
                affectAttachedPart=self.affectAttachedPart,
                attachedEmitters=self.attachedEmitters,
                parent=self.parent,
                forceMin=self.forceMin,
                forceMax=self.forceMax,
                vector=self.vector
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = par.g_contentForcePoint.parseString(self.parseString)[0]
        res = par.AffForce(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribForceMin(self):
        """ Test whether forceMin attribt is created correctly
        """
        exp = co_attribs.AttribNum(**self.forceMin)
        assert self.inst.forceMin == exp

    def testCreationAttribForceMax(self):
        """ Test whether forceMax attribt is created correctly
        """
        exp = co_attribs.AttribNum(**self.forceMax)
        assert self.inst.forceMax == exp

    def testCreationAttribVector(self):
        """ Test whether vector attribt is created correctly
        """
        exp = par.AttribPos3d(**self.vector)
        assert self.inst.vector == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

@pytest.fixture(scope="module")
def affFullFixt(affDragFixt):
    parseString,parseDict = affDragFixt
    parseString = (
        "AffectorType \"Drag\"\n"
        +parseString
    )
    parseDict = {
        "affType":{"identifier":"AffectorType","val":"Drag"},
        "content":parseDict
    }
    return parseString,parseDict

class TestAffector:
    desc = "Tests Affector:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,affFullFixt):
        cls.parseString,parseDict = affFullFixt
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self,affParentFixt):
        self.parent = affParentFixt
        self.inst = par.Affector(
                affType=self.affType,
                parent=self.parent,
                content=self.content
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_affDrag.parseString(self.parseString)[0]
        res = par.Affector(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAffType(self):
        """ Test whether affType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=par.AFFTYPE_TO_AFFCLASS.keys(),
                **self.affType)
        assert self.inst.affType == exp

    def testCreationContent(self):
        """ Test whether content attribute is constructed correctly
        """
        exp = par.AffDrag(parent=self.parent,**self.content)
        assert self.inst.content == exp

    def testCheck(self):
        """ Test whether check returns empty list of problems
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests Emitter ##############################"""

@pytest.fixture(scope="module")
def emitterFixt():
    parseString = (
        "EmitterContents\n"
        +"\tName \"testemitter\"\n"
        +"\tEnabled TRUE\n"
        +"\tEmitRate 2.000000\n"
        +"\tHasInfiniteEmitCount TRUE\n"
        +"\tMaxEmitCount 0\n"
        +"\thasEmitIntervals FALSE\n"
        +"\temitIntervalRunDuration 0.000000\n"
        +"\temitIntervalWaitDuration 0.000000\n"
        +"\tParticleLifeTime 2.000000\n"
        +"\tParticleMinStartLinearSpeed 1.000000\n"
        +"\tParticleMaxStartLinearSpeed 1.000000\n"
        +"\tParticleMinStartAngularSpeed 0.000000\n"
        +"\tParticleMaxStartAngularSpeed 0.000000\n"
        +"\tParticleMinStartRotation 0.000000\n"
        +"\tParticleMaxStartRotation 0.000000\n"
        +"\tParticleStartMass 1.000000\n"
        +"\tParticleStartColor ffffc080\n"
        +"\tParticleWidth 1.000000\n"
        +"\tParticleHeight 1.000000\n"
        +"\tMeshName \"testmesh\"\n"
        +"\tPosition [ 0.000000 0.000000 0.000000 ]\n"
        +"\tOrientation\n"
        +"\t\t[ 1.000000 0.000000 0.000000 ]\n"
        +"\t\t[ 0.000000 1.000000 0.000000 ]\n"
        +"\t\t[ 0.000000 0.000000 1.000000 ]\n"
        +"\tRotateAboutForward 0.000000\n"
        +"\tRotateAboutUp 0.000000\n"
        +"\tRotateAboutCross 0.000000\n"
        +"\tStartTime 0.000000\n"
        +"\tHasInfiniteLifeTime TRUE\n"
        +"\tTotalLifeTime 0.000000\n"
        +"\tBillboardAnchor 0\n"
        +"\tParticleFacing 0\n"
        +"\tPipelineEffectID \"testpipe\"\n"
        +"\tAreParticlesAttached TRUE\n"
        +"\tnumTextures 1\n"
        +"\ttextureName \"testtex\"\n"
        +"\ttextureAnimationName \"testanim\"\n"
        +"\ttextureAnimationSpawnType \"RandomFrames\"\n"
        +"\ttextureAnimationOnParticleFPS 0.000000\n"
        +"\tParticlesRotate FALSE\n"
        +"\tMeshParticleRotationAxisType 0\n"
        +"\tMeshParticleRotationAxis [ 0.000000 0.100000 0.000000 ]\n"
        +"\tRotationDirectionType 0\n"
    )
    parseDict = {
        "identifier":"EmitterContents",
        "name":{"identifier":"Name","val":"testemitter"},
        "enabled":{"identifier":"Enabled","val":True},
        "emitRate":{"identifier":"EmitRate","val":2.0},
        "isEmitCountInf":{"identifier":"HasInfiniteEmitCount","val":True},
        "emitCountMax":{"identifier":"MaxEmitCount","val":0},
        "hasEmitIntervals":{"identifier":"hasEmitIntervals","val":False},
        "emitIntervalRun":{"identifier":"emitIntervalRunDuration",
            "val":0.0},
        "emitIntervalWait":{"identifier":"emitIntervalWaitDuration",
            "val":0.0},
        "partLifetime":{"identifier":"ParticleLifeTime","val":2.0},
        "partStartSpeedLinMin":{"identifier":"ParticleMinStartLinearSpeed",
            "val":1.0},
        "partStartSpeedLinMax":{"identifier":"ParticleMaxStartLinearSpeed",
            "val":1.0},
        "partStartSpeedAngMin":{"identifier":"ParticleMinStartAngularSpeed",
            "val":0.0},
        "partStartSpeedAngMax":{"identifier":"ParticleMaxStartAngularSpeed",
            "val":0.0},
        "partStartRotMin":{"identifier":"ParticleMinStartRotation",
            "val":0.0},
        "partStartRotMax":{"identifier":"ParticleMaxStartRotation",
            "val":0.0},
        "partStartMass":{"identifier":"ParticleStartMass","val":1.0},
        "partStartColor":{"identifier":"ParticleStartColor",
            "val":"ffffc080"},
        "partWidth":{"identifier":"ParticleWidth","val":1.0},
        "partHeight":{"identifier":"ParticleHeight","val":1.0},
        "mesh":{"identifier":"MeshName","val":"testmesh"},
        "pos":{"identifier":"Position","val":[0.0,0.0,0.0]},
        "orientation":{"identifier":"Orientation","vals":[
            [1.0,0.0,0.0],
            [0.0,1.0,0.0],
            [0.0,0.0,1.0]
        ]},
        "rotateForward":{"identifier":"RotateAboutForward","val":0.0},
        "rotateUp":{"identifier":"RotateAboutUp","val":0.0},
        "rotateAcross":{"identifier":"RotateAboutCross","val":0.0},
        "startTime":{"identifier":"StartTime","val":0.0},
        "infiniteLifetime":{"identifier":"HasInfiniteLifeTime","val":True},
        "totalLifetime":{"identifier":"TotalLifeTime","val":0.0},
        "billboardAnchor":{"identifier":"BillboardAnchor","val":0},
        "partFacing":{"identifier":"ParticleFacing","val":0},
        "pipelineEffect":{"identifier":"PipelineEffectID","val":"testpipe"},
        "particlesAttached":{"identifier":"AreParticlesAttached",
            "val":True},
        "textures":{
            "counter":{"identifier":"numTextures","val":1},
            "elements":[
                {"identifier":"textureName","val":"testtex"}
            ]
        },
        "textureAnim":{"identifier":"textureAnimationName",
            "val":"testanim"},
        "texanimSpawnType":{"identifier":"textureAnimationSpawnType",
            "val":"RandomFrames"},
        "texanimFPS":{"identifier":"textureAnimationOnParticleFPS",
            "val":0.0},
        "partRotate":{"identifier":"ParticlesRotate","val":False},
        "meshRotAxisType":{"identifier":"MeshParticleRotationAxisType",
            "val":0},
        "meshRotAxis":{"identifier":"MeshParticleRotationAxis",
            "val":[0.0,0.1,0.0]},
        "rotDirectionType":{"identifier":"RotationDirectionType",
            "val":0}
    }
    return parseString,parseDict

class TestEmitterShared:
    desc = "Tests EmitterShared:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,emitterFixt):
        cls.parseString,parseDict = emitterFixt
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.EmitterShared(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                emitRate=self.emitRate,
                isEmitCountInf=self.isEmitCountInf,
                emitCountMax=self.emitCountMax,
                hasEmitIntervals=self.hasEmitIntervals,
                emitIntervalRun=self.emitIntervalRun,
                emitIntervalWait=self.emitIntervalWait,
                partLifetime=self.partLifetime,
                partStartSpeedLinMin=self.partStartSpeedLinMin,
                partStartSpeedLinMax=self.partStartSpeedLinMax,
                partStartSpeedAngMin=self.partStartSpeedAngMin,
                partStartSpeedAngMax=self.partStartSpeedAngMax,
                partStartRotMin=self.partStartRotMin,
                partStartRotMax=self.partStartRotMax,
                partStartMass=self.partStartMass,
                partStartColor=self.partStartColor,
                partWidth=self.partWidth,
                partHeight=self.partHeight,
                mesh=self.mesh,
                pos=self.pos,
                orientation=self.orientation,
                rotateForward=self.rotateForward,
                rotateUp=self.rotateUp,
                rotateAcross=self.rotateAcross,
                startTime=self.startTime,
                infiniteLifetime=self.infiniteLifetime,
                totalLifetime=self.totalLifetime,
                billboardAnchor=self.billboardAnchor,
                partFacing=self.partFacing,
                pipelineEffect=self.pipelineEffect,
                particlesAttached=self.particlesAttached,
                textures=self.textures,
                textureAnim=self.textureAnim,
                texanimSpawnType=self.texanimSpawnType,
                texanimFPS=self.texanimFPS,
                partRotate=self.partRotate,
                meshRotAxisType=self.meshRotAxisType,
                meshRotAxis=self.meshRotAxis,
                rotDirectionType=self.rotDirectionType
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_emitterShared.parseString(self.parseString)
        res = par.EmitterShared(**parseRes)
        assert res == self.inst

    def testCreationAttribName(self):
        """ Test whether name attribute is created correctly
        """
        exp = co_attribs.AttribString(**self.name)
        assert self.inst.name == exp

    def testCreationAttribEnabled(self):
        """ Test whether enabled attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.enabled)
        assert self.inst.enabled == exp

    def testCreationAttribEmitRate(self):
        """ Test whether emitRate attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.emitRate)
        assert self.inst.emitRate == exp

    def testCreationAttribIsEmitCountInf(self):
        """ Test whether isEmitCountInf attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.isEmitCountInf)
        assert self.inst.isEmitCountInf == exp

    def testCreationAttribEmitCountMax(self):
        """ Test whether emitCountMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.emitCountMax)
        assert self.inst.emitCountMax == exp

    def testCreationAttribHasEmitIntervals(self):
        """ Test whether hasEmitIntervals attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.hasEmitIntervals)
        assert self.inst.hasEmitIntervals == exp

    def testCreationAttribEmitIntervalRun(self):
        """ Test whether emitIntervalRun attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.emitIntervalRun)
        assert self.inst.emitIntervalRun == exp

    def testCreationAttribEmitIntervalWait(self):
        """ Test whether emitIntervalWait attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.emitIntervalWait)
        assert self.inst.emitIntervalWait == exp

    def testCreationAttribPartLifetime(self):
        """ Test whether partLifetime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partLifetime)
        assert self.inst.partLifetime == exp

    def testCreationAttribPartStartSpeedLinMin(self):
        """ Test whether partStartSpeedLinMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartSpeedLinMin)
        assert self.inst.partStartSpeedLinMin == exp

    def testCreationAttribPartStartSpeedLinMax(self):
        """ Test whether partStartSpeedLinMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartSpeedLinMax)
        assert self.inst.partStartSpeedLinMax == exp

    def testCreationAttribPartStartSpeedAngMin(self):
        """ Test whether partStartSpeedAngMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartSpeedAngMin)
        assert self.inst.partStartSpeedAngMin == exp

    def testCreationAttribPartStartSpeedAngMax(self):
        """ Test whether partStartSpeedAngMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartSpeedAngMax)
        assert self.inst.partStartSpeedAngMax == exp

    def testCreationAttribPartStartRotMin(self):
        """ Test whether partStartRotMin attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartRotMin)
        assert self.inst.partStartRotMin == exp

    def testCreationAttribPartStartRotMax(self):
        """ Test whether partStartRotMax attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartRotMax)
        assert self.inst.partStartRotMax == exp

    def testCreationAttribPartStartMass(self):
        """ Test whether partStartMass attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partStartMass)
        assert self.inst.partStartMass == exp

    def testCreationAttribPartStartColor(self):
        """ Test whether partStartColor attribute is created correctly
        """
        exp = co_attribs.AttribColor(**self.partStartColor)
        assert self.inst.partStartColor == exp

    def testCreationAttribPartWidth(self):
        """ Test whether partWidth attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partWidth)
        assert self.inst.partWidth == exp

    def testCreationAttribPartHeight(self):
        """ Test whether partHeight attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partHeight)
        assert self.inst.partHeight == exp

    def testCreationAttribMesh(self):
        """ Test whether mesh attribute is created correctly
        """
        exp = meshes.MeshRef(canBeEmpty=True,**self.mesh)
        assert self.inst.mesh == exp

    def testCreationAttribPos(self):
        """ Test whether pos attribute is created correctly
        """
        exp = par.AttribPos3d(**self.pos)
        assert self.inst.pos == exp

    def testCreationAttribOrientation(self):
        """ Test whether orientation attribute is created correctly
        """
        exp = par.Orientation(**self.orientation)
        assert self.inst.orientation == exp

    def testCreationAttribRotateForward(self):
        """ Test whether rotateForward attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.rotateForward)
        assert self.inst.rotateForward == exp

    def testCreationAttribRotateUp(self):
        """ Test whether rotateUp attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.rotateUp)
        assert self.inst.rotateUp == exp

    def testCreationAttribRotateAcross(self):
        """ Test whether rotateAcross attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.rotateAcross)
        assert self.inst.rotateAcross == exp

    def testCreationAttribStartTime(self):
        """ Test whether startTime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.startTime)
        assert self.inst.startTime == exp

    def testCreationAttribInfiniteLifetime(self):
        """ Test whether infiniteLifetime attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.infiniteLifetime)
        assert self.inst.infiniteLifetime == exp

    def testCreationAttribTotalLifetime(self):
        """ Test whether totalLifetime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.totalLifetime)
        assert self.inst.totalLifetime == exp

    def testCreationAttribBillboardAnchor(self):
        """ Test whether billboardAnchor attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.billboardAnchor)
        assert self.inst.billboardAnchor == exp

    def testCreationAttribPartFacing(self):
        """ Test whether partFacing attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.partFacing)
        assert self.inst.partFacing == exp

    def testCreationAttribPipelineEffect(self):
        """ Test whether pipelineEffect attribute is created correctly
        """
        exp = par.PipelineRef(**self.pipelineEffect)
        assert self.inst.pipelineEffect == exp

    def testCreationAttribParticlesAttached(self):
        """ Test whether particlesAttached attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.particlesAttached)
        assert self.inst.particlesAttached == exp

    def testCreationAttribTextures(self):
        """ Test whether textures attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=ui.UITextureRef,
                elemArgs={"canBeEmpty":True},
                **self.textures)
        assert self.inst.textures == exp

    def testCreationAttribTextureAnim(self):
        """ Test whether textureAnim attribute is created correctly
        """
        exp = anim.TextureAnimRef(canBeEmpty=True,**self.textureAnim)
        assert self.inst.textureAnim == exp

    def testCreationAttribTexanimSpawnType(self):
        """ Test whether texanimSpawnType attribute is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=par.TEXANIM_SPAWN_TYPES,
                **self.texanimSpawnType)
        assert self.inst.texanimSpawnType == exp

    def testCreationAttribTexanimFPS(self):
        """ Test whether texanimFPS attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.texanimFPS)
        assert self.inst.texanimFPS == exp

    def testCreationAttribPartRotate(self):
        """ Test whether partRotate attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.partRotate)
        assert self.inst.partRotate == exp

    def testCreationAttribMeshRotAxisType(self):
        """ Test whether meshRotAxisType attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.meshRotAxisType)
        assert self.inst.meshRotAxisType == exp

    def testCreationAttribMeshRotAxis(self):
        """ Test whether meshRotAxis attribute is created correctly
        """
        exp = par.AttribPos3d(**self.meshRotAxis)
        assert self.inst.meshRotAxis == exp

    def testCreationAttribRotDirectionType(self):
        """ Test whether rotDirectionType attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.rotDirectionType)
        assert self.inst.rotDirectionType == exp

    @pytest.fixture()
    def checkFixt(self):
        patcherText = mock.patch.object(self.inst.textures,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTextures = patcherText.start()
        patcherMesh = mock.patch.object(self.inst.mesh,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockMesh = patcherMesh.start()
        patcherPipe = mock.patch.object(self.inst.pipelineEffect,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockPipeline = patcherPipe.start()
        patcherAnim = mock.patch.object(self.inst.textureAnim,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockTexanim = patcherAnim.start()
        yield
        patcherText.stop()
        patcherMesh.stop()
        patcherPipe.stop()
        patcherAnim.stop()

    def testCheck(self,checkFixt):
        """ Test whether check returns empty list with no problems
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckMesh(self,checkFixt):
        """ Test whether problems in mesh attribute are returned 
        """
        self.mockMesh.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockMesh.assert_called_once_with(self.mmod,False)

    def testCheckPipelineEffect(self,checkFixt):
        """ Test whether problems in pipelineEffect attribute are returned 
        """
        self.mockPipeline.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockPipeline.assert_called_once_with(self.mmod,False)

    def testCheckTextures(self,checkFixt):
        """ Test whether problems in textures attribute are returned 
        """
        self.mockTextures.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockTextures.assert_called_once_with(self.mmod,False)

    def testCheckTextureAnim(self,checkFixt):
        """ Test whether problems in textureAnim attribute are returned 
        """
        self.mockTexanim.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockTexanim.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

@pytest.fixture(scope="module")
def emitterPointFixt(emitterFixt):
    parseString,parseDict = emitterFixt
    parseString += (
        "\tAngleVariance 2.500000\n"
    )
    parseDict["angleVariance"] = {"identifier":"AngleVariance","val":2.5}
    return parseString,parseDict

class TestEmitterPoint:
    desc = "Tests EmitterPoint:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,emitterPointFixt):
        cls.parseString,parseDict = emitterPointFixt
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.EmitterPoint(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                emitRate=self.emitRate,
                isEmitCountInf=self.isEmitCountInf,
                emitCountMax=self.emitCountMax,
                hasEmitIntervals=self.hasEmitIntervals,
                emitIntervalRun=self.emitIntervalRun,
                emitIntervalWait=self.emitIntervalWait,
                partLifetime=self.partLifetime,
                partStartSpeedLinMin=self.partStartSpeedLinMin,
                partStartSpeedLinMax=self.partStartSpeedLinMax,
                partStartSpeedAngMin=self.partStartSpeedAngMin,
                partStartSpeedAngMax=self.partStartSpeedAngMax,
                partStartRotMin=self.partStartRotMin,
                partStartRotMax=self.partStartRotMax,
                partStartMass=self.partStartMass,
                partStartColor=self.partStartColor,
                partWidth=self.partWidth,
                partHeight=self.partHeight,
                mesh=self.mesh,
                pos=self.pos,
                orientation=self.orientation,
                rotateForward=self.rotateForward,
                rotateUp=self.rotateUp,
                rotateAcross=self.rotateAcross,
                startTime=self.startTime,
                infiniteLifetime=self.infiniteLifetime,
                totalLifetime=self.totalLifetime,
                billboardAnchor=self.billboardAnchor,
                partFacing=self.partFacing,
                pipelineEffect=self.pipelineEffect,
                particlesAttached=self.particlesAttached,
                textures=self.textures,
                textureAnim=self.textureAnim,
                texanimSpawnType=self.texanimSpawnType,
                texanimFPS=self.texanimFPS,
                partRotate=self.partRotate,
                meshRotAxisType=self.meshRotAxisType,
                meshRotAxis=self.meshRotAxis,
                rotDirectionType=self.rotDirectionType,
                angleVariance=self.angleVariance
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentPoint.parseString(self.parseString)[0]
        res = par.EmitterPoint(**parseRes)
        assert res == self.inst

    def testCreationAttribAngleVariance(self):
        """ Test whether angleVariance attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.angleVariance)
        assert self.inst.angleVariance == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestEmitterRing:
    desc = "Tests EmitterRing:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,emitterFixt):
        cls.parseString,emitterDict = emitterFixt
        cls.parseString += (
            "\tRingRadiusXMin 40.000000\n"
            +"\tRingRadiusXMax 40.000000\n"
            +"\tRingRadiusYMin 40.000000\n"
            +"\tRingRadiusYMax 40.000000\n"
            +"\tParticleMaxStartSpeedTangential 0.000000\n"
            +"\tParticleMaxStartSpeedRingNormal -5.000000\n"
            +"\tScaleStartSpeedsByRadius FALSE\n"
            +"\tSpawnAngleStart 0.000000\n"
            +"\tSpawnAngleStop 6.283185\n"
            +"\tminSpawnHeight 0.000000\n"
            +"\tmaxSpawnHeight 0.000000\n"
            +"\tspawnDirectionIsParallelToPlane TRUE\n"
            +"\tisSpawnAngleRandom FALSE\n"
            +"\tnonRandomSpawnLoopEmittedParticleCount 30\n"
        )
        tco.expandParseDict(cls,emitterDict)
        cls.radiusXMin = {"identifier":"RingRadiusXMin","val":40.0}
        cls.radiusXMax = {"identifier":"RingRadiusXMax","val":40.0}
        cls.radiusYMin = {"identifier":"RingRadiusYMin","val":40.0}
        cls.radiusYMax = {"identifier":"RingRadiusYMax","val":40.0}
        cls.maxStartSpeedTang = {"identifier":"ParticleMaxStartSpeedTangential",
            "val":0.0}
        cls.maxStartSpeedRing = {"identifier":"ParticleMaxStartSpeedRingNormal",
            "val":-5.0}
        cls.scaleSpeedByRadius = {"identifier":"ScaleStartSpeedsByRadius",
            "val":False}
        cls.spawnAngleStart = {"identifier":"SpawnAngleStart","val":0.0}
        cls.spawnAngleStop = {"identifier":"SpawnAngleStop","val":6.283185}
        cls.spawnHeightMin = {"identifier":"minSpawnHeight","val":0.0}
        cls.spawnHeightMax = {"identifier":"maxSpawnHeight","val":0.0}
        cls.spawnParallelToPlane = {
            "identifier":"spawnDirectionIsParallelToPlane",
            "val":True}
        cls.spawnAngleRandom = {"identifier":"isSpawnAngleRandom","val":False}
        cls.nonRandomLoopPartCount = {
            "identifier":"nonRandomSpawnLoopEmittedParticleCount",
            "val":30}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.EmitterRing(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                emitRate=self.emitRate,
                isEmitCountInf=self.isEmitCountInf,
                emitCountMax=self.emitCountMax,
                hasEmitIntervals=self.hasEmitIntervals,
                emitIntervalRun=self.emitIntervalRun,
                emitIntervalWait=self.emitIntervalWait,
                partLifetime=self.partLifetime,
                partStartSpeedLinMin=self.partStartSpeedLinMin,
                partStartSpeedLinMax=self.partStartSpeedLinMax,
                partStartSpeedAngMin=self.partStartSpeedAngMin,
                partStartSpeedAngMax=self.partStartSpeedAngMax,
                partStartRotMin=self.partStartRotMin,
                partStartRotMax=self.partStartRotMax,
                partStartMass=self.partStartMass,
                partStartColor=self.partStartColor,
                partWidth=self.partWidth,
                partHeight=self.partHeight,
                mesh=self.mesh,
                pos=self.pos,
                orientation=self.orientation,
                rotateForward=self.rotateForward,
                rotateUp=self.rotateUp,
                rotateAcross=self.rotateAcross,
                startTime=self.startTime,
                infiniteLifetime=self.infiniteLifetime,
                totalLifetime=self.totalLifetime,
                billboardAnchor=self.billboardAnchor,
                partFacing=self.partFacing,
                pipelineEffect=self.pipelineEffect,
                particlesAttached=self.particlesAttached,
                textures=self.textures,
                textureAnim=self.textureAnim,
                texanimSpawnType=self.texanimSpawnType,
                texanimFPS=self.texanimFPS,
                partRotate=self.partRotate,
                meshRotAxisType=self.meshRotAxisType,
                meshRotAxis=self.meshRotAxis,
                rotDirectionType=self.rotDirectionType,
                radiusXMin=self.radiusXMin,
                radiusXMax=self.radiusXMax,
                radiusYMin=self.radiusYMin,
                radiusYMax=self.radiusYMax,
                maxStartSpeedTang=self.maxStartSpeedTang,
                maxStartSpeedRing=self.maxStartSpeedRing,
                scaleSpeedByRadius=self.scaleSpeedByRadius,
                spawnAngleStart=self.spawnAngleStart,
                spawnAngleStop=self.spawnAngleStop,
                spawnHeightMin=self.spawnHeightMin,
                spawnHeightMax=self.spawnHeightMax,
                spawnParallelToPlane=self.spawnParallelToPlane,
                spawnAngleRandom=self.spawnAngleRandom,
                nonRandomLoopPartCount=self.nonRandomLoopPartCount
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_contentRing.parseString(self.parseString)[0]
        res = par.EmitterRing(**parseRes)
        assert res == self.inst

    def testCreationAttribRadiusXMin(self):
        """ Test whether attribute radiusXMin is created correctly
        """
        exp = co_attribs.AttribNum(**self.radiusXMin)
        assert self.inst.radiusXMin == exp

    def testCreationAttribRadiusXMax(self):
        """ Test whether attribute radiusXMax is created correctly
        """
        exp = co_attribs.AttribNum(**self.radiusXMax)
        assert self.inst.radiusXMax == exp

    def testCreationAttribRadiusYMin(self):
        """ Test whether attribute radiusYMin is created correctly
        """
        exp = co_attribs.AttribNum(**self.radiusYMin)
        assert self.inst.radiusYMin == exp

    def testCreationAttribRadiusYMax(self):
        """ Test whether attribute radiusYMax is created correctly
        """
        exp = co_attribs.AttribNum(**self.radiusYMax)
        assert self.inst.radiusYMax == exp

    def testCreationAttribMaxStartSpeedTang(self):
        """ Test whether attribute maxStartSpeedTang is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxStartSpeedTang)
        assert self.inst.maxStartSpeedTang == exp

    def testCreationAttribMaxStartSpeedRing(self):
        """ Test whether attribute maxStartSpeedRing is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxStartSpeedRing)
        assert self.inst.maxStartSpeedRing == exp

    def testCreationAttribScaleSpeedByRadius(self):
        """ Test whether attribute scaleSpeedByRadius is created correctly
        """
        exp = co_attribs.AttribBool(**self.scaleSpeedByRadius)
        assert self.inst.scaleSpeedByRadius == exp

    def testCreationAttribSpawnAngleStart(self):
        """ Test whether attribute spawnAngleStart is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnAngleStart)
        assert self.inst.spawnAngleStart == exp

    def testCreationAttribSpawnAngleStop(self):
        """ Test whether attribute spawnAngleStop is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnAngleStop)
        assert self.inst.spawnAngleStop == exp

    def testCreationAttribSpawnHeightMin(self):
        """ Test whether attribute spawnHeightMin is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnHeightMin)
        assert self.inst.spawnHeightMin == exp

    def testCreationAttribSpawnHeightMax(self):
        """ Test whether attribute spawnHeightMax is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnHeightMax)
        assert self.inst.spawnHeightMax == exp

    def testCreationAttribSpawnParallelToPlane(self):
        """ Test whether attribute spawnParallelToPlane is created correctly
        """
        exp = co_attribs.AttribBool(**self.spawnParallelToPlane)
        assert self.inst.spawnParallelToPlane == exp

    def testCreationAttribSpawnAngleRandom(self):
        """ Test whether attribute spawnAngleRandom is created correctly
        """
        exp = co_attribs.AttribBool(**self.spawnAngleRandom)
        assert self.inst.spawnAngleRandom == exp

    def testCreationAttribNonRandomLoopPartCount(self):
        """ Test whether attribute nonRandomLoopPartCount is created correctly
        """
        exp = co_attribs.AttribNum(**self.nonRandomLoopPartCount)
        assert self.inst.nonRandomLoopPartCount == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestEmitterSphere:
    desc = "Tests EmitterSphere:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,emitterFixt):
        cls.parseString,emitterDict = emitterFixt
        tco.expandParseDict(cls,emitterDict)
        cls.parseString += (
            "\tSphereRadiusXMax 300.000000\n"
            +"\tSphereRadiusXMin 45.000000\n"
            +"\tSphereRadiusYMax 300.000000\n"
            +"\tSphereRadiusYMin 45.000000\n"
            +"\tSphereRadiusZMax 300.000000\n"
            +"\tSphereRadiusZMin 45.000000\n"
            +"\tParticleMaxStartSpeedAzimuthalTangential 0.000000\n"
            +"\tParticleMaxStartSpeedPolarTangential 0.000000\n"
            +"\tScaleStartSpeedsByRadius FALSE\n"
            +"\tSpawnAngleLatitudinalStart 0.000000\n"
            +"\tSpawnAngleLatitudinalStop 3.140000\n"
            +"\tSpawnAngleLongitudinalStart 0.000000\n"
            +"\tSpawnAngleLongitudinalStop 6.280000\n"
        )
        cls.radiusXMax = {"identifier":"SphereRadiusXMax","val":300.0}
        cls.radiusXMin = {"identifier":"SphereRadiusXMin","val":45.0}
        cls.radiusYMax = {"identifier":"SphereRadiusYMax","val":300.0}
        cls.radiusYMin = {"identifier":"SphereRadiusYMin","val":45.0}
        cls.radiusZMax = {"identifier":"SphereRadiusZMax","val":300.0}
        cls.radiusZMin = {"identifier":"SphereRadiusZMin","val":45.0}
        cls.maxStartSpeedAzimuTang = {
            "identifier":"ParticleMaxStartSpeedAzimuthalTangential",
            "val":0.0}
        cls.maxStartSpeedPolarTang = {
            "identifier":"ParticleMaxStartSpeedPolarTangential",
            "val":0.0}
        cls.scaleSpeedByRadius = {"identifier":"ScaleStartSpeedsByRadius",
            "val":False}
        cls.spawnAngleLatStart = {"identifier":"SpawnAngleLatitudinalStart",
            "val":0.0}
        cls.spawnAngleLatStop = {"identifier":"SpawnAngleLatitudinalStop",
            "val":3.14}
        cls.spawnAngleLongStart = {"identifier":"SpawnAngleLongitudinalStart",
            "val":0.0}
        cls.spawnAngleLongStop = {"identifier":"SpawnAngleLongitudinalStop",
            "val":6.28}

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.EmitterSphere(
                identifier=self.identifier,
                name=self.name,
                enabled=self.enabled,
                emitRate=self.emitRate,
                isEmitCountInf=self.isEmitCountInf,
                emitCountMax=self.emitCountMax,
                hasEmitIntervals=self.hasEmitIntervals,
                emitIntervalRun=self.emitIntervalRun,
                emitIntervalWait=self.emitIntervalWait,
                partLifetime=self.partLifetime,
                partStartSpeedLinMin=self.partStartSpeedLinMin,
                partStartSpeedLinMax=self.partStartSpeedLinMax,
                partStartSpeedAngMin=self.partStartSpeedAngMin,
                partStartSpeedAngMax=self.partStartSpeedAngMax,
                partStartRotMin=self.partStartRotMin,
                partStartRotMax=self.partStartRotMax,
                partStartMass=self.partStartMass,
                partStartColor=self.partStartColor,
                partWidth=self.partWidth,
                partHeight=self.partHeight,
                mesh=self.mesh,
                pos=self.pos,
                orientation=self.orientation,
                rotateForward=self.rotateForward,
                rotateUp=self.rotateUp,
                rotateAcross=self.rotateAcross,
                startTime=self.startTime,
                infiniteLifetime=self.infiniteLifetime,
                totalLifetime=self.totalLifetime,
                billboardAnchor=self.billboardAnchor,
                partFacing=self.partFacing,
                pipelineEffect=self.pipelineEffect,
                particlesAttached=self.particlesAttached,
                textures=self.textures,
                textureAnim=self.textureAnim,
                texanimSpawnType=self.texanimSpawnType,
                texanimFPS=self.texanimFPS,
                partRotate=self.partRotate,
                meshRotAxisType=self.meshRotAxisType,
                meshRotAxis=self.meshRotAxis,
                rotDirectionType=self.rotDirectionType,
                radiusXMax=self.radiusXMax,
                radiusXMin=self.radiusXMin,
                radiusYMax=self.radiusYMax,
                radiusYMin=self.radiusYMin,
                radiusZMax=self.radiusZMax,
                radiusZMin=self.radiusZMin,
                maxStartSpeedAzimuTang=self.maxStartSpeedAzimuTang,
                maxStartSpeedPolarTang=self.maxStartSpeedPolarTang,
                scaleSpeedByRadius=self.scaleSpeedByRadius,
                spawnAngleLatStart=self.spawnAngleLatStart,
                spawnAngleLatStop=self.spawnAngleLatStop,
                spawnAngleLongStart=self.spawnAngleLongStart,
                spawnAngleLongStop=self.spawnAngleLongStop
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse result
        """
        parseRes = par.g_contentSphere.parseString(self.parseString)[0]
        res = par.EmitterSphere(**parseRes)
        assert res == self.inst

    def testCreationAttribRadiusXMax(self):
        """ Test whether radiusXMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusXMax)
        assert self.inst.radiusXMax == exp

    def testCreationAttribRadiusXMin(self):
        """ Test whether radiusXMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusXMin)
        assert self.inst.radiusXMin == exp

    def testCreationAttribRadiusYMax(self):
        """ Test whether radiusYMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusYMax)
        assert self.inst.radiusYMax == exp

    def testCreationAttribRadiusYMin(self):
        """ Test whether radiusYMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusYMin)
        assert self.inst.radiusYMin == exp

    def testCreationAttribRadiusZMax(self):
        """ Test whether radiusZMax attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusZMax)
        assert self.inst.radiusZMax == exp

    def testCreationAttribRadiusZMin(self):
        """ Test whether radiusZMin attribute is constructed correctly
        """
        exp = co_attribs.AttribNum(**self.radiusZMin)
        assert self.inst.radiusZMin == exp

    def testCreationAttribmaxSpeedAzimuTang(self):
        """ Test whether maxStartSpeedAzimuTang attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxStartSpeedAzimuTang)
        assert self.inst.maxStartSpeedAzimuTang == exp

    def testCreationAttribmaxSpeedPolarTang(self):
        """ Test whether maxStartSpeedPolarTang attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxStartSpeedPolarTang)
        assert self.inst.maxStartSpeedPolarTang == exp

    def testCreationAttribScaleSpeedByRadius(self):
        """ Test whether scaleSpeedByRadius attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.scaleSpeedByRadius)
        assert self.inst.scaleSpeedByRadius == exp

    def testCreationAttribSpawnAngleLatStart(self):
        """ Test whether spawnAngleLatStart attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnAngleLatStart)
        assert self.inst.spawnAngleLatStart == exp

    def testCreationAttribSpawnAngleLatStop(self):
        """ Test whether spawnAngleLatStop attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnAngleLatStop)
        assert self.inst.spawnAngleLatStop == exp

    def testCreationAttribSpawnAngleLongStart(self):
        """ Test whether spawnAngleLongStart attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnAngleLongStart)
        assert self.inst.spawnAngleLongStart == exp

    def testCreationAttribSpawnAngleLongStop(self):
        """ Test whether spawnAngleLongStop attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnAngleLongStop)
        assert self.inst.spawnAngleLongStop == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

@pytest.fixture(scope="module")
def emittFullFixt(emitterPointFixt):
    emitterString,emitterDict = emitterPointFixt
    parseString = "EmitterType \"Point\"\n"+emitterString
    parseDict = {
        "content":emitterDict,
        "emitterType":{"identifier":"EmitterType","val":"Point"}
    }
    return parseString,parseDict

class TestEmitter:
    desc = "Tests Emitter:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,emittFullFixt):
        cls.parseString,parseDict = emittFullFixt
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.Emitter(
                emitterType=self.emitterType,
                content=self.content
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = par.g_emitter.parseString(self.parseString)[0]
        res = par.Emitter(**parseRes)
        assert res == self.inst

    def testCreationAttribEmitterType(self):
        """ Test whether emitterType attribute is constructed correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=par.EMITTERTYPE_TO_EMITTERCLASS.keys(),
                **self.emitterType)
        assert self.inst.emitterType == exp

    def testCreationAttribContent(self):
        """ Test whether content attribute is constructed correctly
        """
        exp = par.EmitterPoint(**self.content)
        assert self.inst.content == exp

    def testCheck(self):
        """ Test whether check returns empty list without problems
        """
        with mock.patch.object(self.inst.content,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckContentProbs(self):
        """ Test whether check returns problems in content attrib correctly
        """
        probs = [mock.sentinel.prob]
        with mock.patch.object(self.inst.content,"check",autospec=True,
                spec_set=True,return_value=probs) as mocked:
            res = self.inst.check(self.mmod)
            assert res == probs
            mocked.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests ParticleFile ###########################"""

class TestParticleFile:
    desc = "Tests ParticleFile"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,emittFullFixt,affFullFixt):
        _,emitDict = emittFullFixt
        _,affDict = affFullFixt
        cls.filepath = "Particle.particle"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()

        cls.emitters = {
            "counter":{"identifier":"NumEmitters","val":1},
            "elements":[emitDict]
        }
        cls.affectors = {
            "counter":{"identifier":"NumAffectors","val":1},
            "elements":[affDict]
        }
        cls.infiniteLife = {"identifier":"HasInfiniteLifeTime","val":True}
        cls.totalLifetime = {"identifier":"TotalLifeTime","val":10.0}
        cls.length = {"identifier":"length","val":0.0}
        cls.identifier = "ParticleSimulation"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.ParticleFile(
                filepath=self.filepath,
                identifier=self.identifier,
                infiniteLife=self.infiniteLife,
                totalLifetime=self.totalLifetime,
                emitters=self.emitters,
                affectors=self.affectors,
                length=self.length
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        res = par.ParticleFile.createInstance(self.filepath)
        assert res == self.inst

    def testCreationParseMalformed(self):
        """ Test whether parse errors during creation are returned
        """
        res = par.ParticleFile.createInstance("ParticleMalformed.particle")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.ParseProblem)

    def testCreationAttribInfiniteLife(self):
        """ Test whether infiniteLife attribute is created correctly
        """
        exp = co_attribs.AttribBool(**self.infiniteLife)
        assert self.inst.infiniteLife == exp

    def testCreationAttribTotalLifetime(self):
        """ Test whether totalLifetime attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.totalLifetime)
        assert self.inst.totalLifetime == exp

    def testCreationAttribEmitters(self):
        """ Test whether emitters attribute is created correctly
        """
        exp = co_attribs.AttribListKeyed(
                elemType=par.Emitter,
                keyfunc=lambda val:val.content.name.value,
                **self.emitters
        )
        assert self.inst.emitters == exp

    def testCreationAttribAffectors(self):
        """ Test whether affectors attribute is created correctly
        """
        exp = co_attribs.AttribList(
                elemType=par.Affector,
                elemArgs={"parent":self.inst},
                **self.affectors
        )
        assert self.inst.affectors == exp

    def testCreationAttribLength(self):
        """ Test whether length attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.length)
        assert self.inst.length == exp

    @pytest.fixture()
    def checkFixture(self):
        patcherEmitters = mock.patch.object(self.inst.emitters,"check",
                autospec=True,spec_set=True,return_value=[])
        mockEmitters = patcherEmitters.start()
        patcherAffectors = mock.patch.object(self.inst.affectors,"check",
                autospec=True,spec_set=True,return_value=[])
        mockAffectors = patcherAffectors.start()
        yield mockEmitters,mockAffectors
        patcherEmitters.stop()
        patcherAffectors.stop()

    def testCheck(self,checkFixture):
        """ Test whether check returns empty list without problems
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckEmitters(self,checkFixture,mockProb):
        """ Test whether problems in emitters attribute are returned
        """
        mockEmitters,_ = checkFixture
        mockEmitters.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]

    def testCheckAffectors(self,checkFixture,mockProb):
        """ Test whether problems in affectors attribute are returned
        """
        _,mockAffectors = checkFixture
        mockAffectors.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################### Tests ModParticles ##########################"""

class TestModParticlesCreation:
    desc = "Tests of ModParticles creation:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.moddir = os.path.abspath("./comp")
        cls.rebdir = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebdir)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.ModParticles(self.mmod)

    def testDiscovery(self):
        """ Test whether initial discovery finds correct particle files
        """
        assert len(self.inst.compFilesMod) == 2
        assert "par1.particle" in self.inst.compFilesMod
        assert "par2.particle" in self.inst.compFilesMod

@pytest.fixture()
def createFixt():
    mockCreate = mock.create_autospec(
            par.ParticleFile.createInstance,spec_set=True)
    patcher = mock.patch(
            "pysoase.mod.particles.ParticleFile.createInstance",
            new=mockCreate)
    patcher.start()
    yield mockCreate
    patcher.stop()

@pytest.fixture()
def partFixt(createFixt):
    mock1 = mock.create_autospec(par.ParticleFile,spec_set=True)
    mock2 = mock.create_autospec(par.ParticleFile,spec_set=True)
    mockCreate = createFixt
    mockCreate.side_effect = [mock1,mock2]
    yield mockCreate,mock1,mock2

class TestModParticlesLoading:
    desc = "Tests of ModParticles loading:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.moddir = os.path.abspath("./comp")
        cls.rebdir = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebdir)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.ModParticles(self.mmod)

    def testLoadParticle(self,partFixt):
        """ Test whether particle is loaded correctly
        """
        _,mock1,_ = partFixt
        res = self.inst.loadParticle("par1.particle")
        assert res == []
        assert self.inst.compFilesMod["par1.particle"] is mock1

    def testLoadParticleMissing(self):
        """ Test whether MissingRef is returned for missing particle
        """
        res = self.inst.loadParticle("missing.particle")
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        assert len(self.inst.compFilesMod["missing.particle"]) == 1
        assert isinstance(self.inst.compFilesMod["missing.particle"][0],
                co_probs.MissingRefProblem)

    def testLoadParticleMalformed(self,createFixt):
        """ Test whether problem is returned for malformed particle
        """
        createFixt.side_effect = [[mock.sentinel.prob]]
        res = self.inst.loadParticle("par1.particle")
        assert len(res) == 1
        assert res[0] == mock.sentinel.prob
        assert len(self.inst.compFilesMod["par1.particle"]) == 1
        assert self.inst.compFilesMod["par1.particle"][0] == mock.sentinel.prob

    def testLoadAll(self,mockTqdm,partFixt):
        """ Test whether loadAll loads all particles
        """
        res = self.inst.loadAll()
        assert res == []
        assert len(self.inst.compFilesMod) == 2
        assert isinstance(self.inst.compFilesMod["par1.particle"],
                par.ParticleFile)
        assert isinstance(self.inst.compFilesMod["par2.particle"],
                par.ParticleFile)

    def testLoadAllProbs(self,mockTqdm,partFixt):
        """ Test whether loadAll returns problems occuring during loading
        """
        mockCreate,mock1,_ = partFixt
        mockCreate.side_effect = [mock1,[mock.sentinel.prob]]
        res = self.inst.loadAll()
        assert res == [mock.sentinel.prob]
        assert len(self.inst.compFilesMod) == 2

class TestModParticleChecking:
    desc = "Tests of ModParticles checking:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.moddir = os.path.abspath("./comp")
        cls.rebdir = os.path.abspath("./compreb")
        cls.mmod = tco.genMockMod(cls.moddir,cls.rebdir)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = par.ModParticles(self.mmod)

    def testCheckParticle(self,partFixt):
        """ Test whether an empty problem list returned for a correct particle
        """
        _,mock1,_ = partFixt
        mock1.check.return_value = []
        res = self.inst.checkParticle("par1.particle")
        assert res == []
        mock1.check.assert_called_once_with(self.mmod)

    def testCheckParticleProb(self,partFixt):
        """ Test whether problems are returned by checkParticle
        """
        _,mock1,_ = partFixt
        mock1.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkParticle("par1.particle")
        assert res == [mock.sentinel.prob]
        mock1.check.assert_called_once_with(self.mmod)

    def testCheckParticleLoadProb(self,createFixt):
        """ Test whether loading problems are returned by checkParticle
        """
        createFixt.side_effect = None
        createFixt.return_value = [mock.sentinel.prob]
        res = self.inst.checkParticle("par1.particle")
        assert res == [mock.sentinel.prob]

    def testCheckParticlePreloadLoadProb(self,createFixt):
        """ Test whether loading problems preload are returned by checkParticle
        """
        createFixt.side_effect = None
        createFixt.return_value = [mock.sentinel.prob]
        self.inst.loadParticle("par1.particle")
        res = self.inst.checkParticle("par1.particle")
        assert res == [mock.sentinel.prob]

    def testCheck(self,partFixt,mockTqdm):
        """ Test whether check returns empty list for good particles
        """
        _,mock1,mock2 = partFixt
        mock1.check.return_value = []
        mock2.check.return_value = []
        res = self.inst.check()
        assert res == []
        mock1.check.assert_called_once_with(self.mmod)
        mock2.check.assert_called_once_with(self.mmod)

    def testCheckProbs(self,partFixt,mockTqdm):
        """ Test whether check returns problems correctly
        """
        _,mock1,mock2 = partFixt
        mock1.check.return_value = [mock.sentinel.prob1]
        mock2.check.return_value = [mock.sentinel.prob2]
        res = self.inst.check()
        assert len(res) == 2
        assert mock.sentinel.prob1 in res
        assert mock.sentinel.prob2 in res
        mock1.check.assert_called_once_with(self.mmod)
        mock2.check.assert_called_once_with(self.mmod)

    def testCheckFile(self,partFixt):
        """ Test whether checkFile returns empty list for good particles
        """
        _,mock1,_ = partFixt
        mock1.check.return_value = []
        res = self.inst.checkFile("par1.particle")
        assert res == []
        mock1.check.assert_called_once_with(self.mmod)

    def testCheckFileProbs(self,partFixt):
        """ Test whether checkFile returns problems correctly
        """
        _,mock1,_ = partFixt
        mock1.check.return_value = [mock.sentinel.prob]
        res = self.inst.checkFile("par1.particle")
        assert res == [mock.sentinel.prob]
        mock1.check.assert_called_once_with(self.mmod)

    def testCheckFileInvalid(self):
        """ Test whether checkFile raises error for invalid file type
        """
        with pytest.raises(RuntimeError):
            self.inst.checkFile("ta1.foo")
