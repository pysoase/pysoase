""" Tests for the pysoase.mod.events module.
"""

import collections
import os
from unittest import mock

import pytest

import pysoase.common.attributes as co_attribs
import pysoase.common.misc as co_misc
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.events as events
import pysoase.mod.ui as ui
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","events")

"""########################## Tests GameEventRef ###########################"""

class TestGameEventRef:
    desc = "Tests GameEventRef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "ident \"testref\"\n"
        cls.identifier = "ident"
        cls.val = "testref"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.parent = mock.create_autospec(events.GameEventDef,spec_set=True)
        self.parent.events.__contains__ = mock.MagicMock(return_value=True)

    def testCheckEmpty(self):
        """ Test whether check is called for empty references
        """
        inst = events.GameEventRef(
                identifier=self.identifier,
                val="",
                parent=self.parent
        )
        inst.check(self.mmod)
        assert not self.parent.events.__contains__.called

    def testCheckInvalid(self):
        """ Test whether check is called for Invalid references
        """
        inst = events.GameEventRef(
                identifier=self.identifier,
                val="Invalid",
                parent=self.parent
        )
        inst.check(self.mmod)
        assert not self.parent.events.__contains__.called

    def testCheckPresent(self):
        """ Test whether check returns empty list when event exists
        """
        inst = events.GameEventRef(
                identifier=self.identifier,
                val=self.val,
                parent=self.parent
        )
        res = inst.check(self.mmod)
        assert res == []
        self.parent.events.__contains__.assert_called_once_with(self.val)

    def testCheckMissing(self):
        """ Test whether check returns problem for missing event
        """
        inst = events.GameEventRef(
                identifier=self.identifier,
                val=self.val,
                parent=self.parent
        )
        self.parent.events.__contains__.return_value = False
        res = inst.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        self.parent.events.__contains__.assert_called_once_with(self.val)

"""########################### Tests GameEvent #############################"""

@pytest.fixture(scope="module")
def gameEventFixture():
    parseString = (
        "GameEventDef:AllianceCeaseFireBroken\n"
        +"\tcategoryId \"Alliance\"\n"
        +"\thudButtonBrush \"testbrush1\"\n"
        +"\tentityThrottleTime 0.000000\n"
        +"\tglobalSoundThrottleTime 0.000000\n"
        +"\tthottleSharedGameEventId \"Invalid\"\n"
        +"\tonlyAddIfNotFocused FALSE\n"
        +"\tonlyAddIfSelected FALSE\n"
        +"\tonlyAddIfNotInView FALSE\n"
        +"\tshouldRemoveAllSimilarEvents TRUE\n"
    )
    parseDict = {
        "identifier":"AllianceCeaseFireBroken",
        "category":{"identifier":"categoryId","val":"Alliance"},
        "icon":{"identifier":"hudButtonBrush","val":"testbrush1"},
        "entityThrottle":{"identifier":"entityThrottleTime","val":0.0},
        "soundThrottle":{"identifier":"globalSoundThrottleTime","val":0.0},
        "throttledEvent":{"identifier":"thottleSharedGameEventId",
            "val":"Invalid"},
        "addIfNotFocused":{"identifier":"onlyAddIfNotFocused","val":False},
        "addIfSelected":{"identifier":"onlyAddIfSelected","val":False},
        "addIfNotInView":{"identifier":"onlyAddIfNotInView","val":False},
        "removeSimilarEvents":{"identifier":"shouldRemoveAllSimilarEvents",
            "val":True}
    }
    return parseString,parseDict

class TestGameEvent:
    desc = "Tests GameEvent:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,gameEventFixture):
        cls.parseString,parseDict = gameEventFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.parent = mock.create_autospec(events.GameEventDef,spec_set=True)
        self.inst = events.GameEvent(
                parent=self.parent,
                identifier=self.identifier,
                category=self.category,
                icon=self.icon,
                entityThrottle=self.entityThrottle,
                soundThrottle=self.soundThrottle,
                throttledEvent=self.throttledEvent,
                addIfNotFocused=self.addIfNotFocused,
                addIfSelected=self.addIfSelected,
                addIfNotInView=self.addIfNotInView,
                removeSimilarEvents=self.removeSimilarEvents
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_GameEvent.parseString(self.parseString)[0]
        res = events.GameEvent(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribCategory(self):
        """ Test whether category attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=events.EVENT_CATEGORIES,
                **self.category)
        assert self.inst.category == exp

    def testCreationAttribIcon(self):
        """ Test whether icon attrib is created correctly
        """
        exp = ui.BrushRef(canBeEmpty=True,**self.icon)
        assert self.inst.icon == exp

    def testCreationAttribEntityThrottle(self):
        """ Test whether entityThrottle attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.entityThrottle)
        assert self.inst.entityThrottle == exp

    def testCreationAttribSoundThrottle(self):
        """ Test whether soundThrottle attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.soundThrottle)
        assert self.inst.soundThrottle == exp

    def testCreationAttribThrottledEvent(self):
        """ Test whether throttledEvent attrib is created correctly
        """
        exp = events.GameEventRef(parent=self.parent,**self.throttledEvent)
        assert self.inst.throttledEvent == exp

    def testCreationAttribAddIfNotFocused(self):
        """ Test whether addIfNotFocused attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.addIfNotFocused)
        assert self.inst.addIfNotFocused == exp

    def testCreationAttribAddIfSelected(self):
        """ Test whether addIfSelected attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.addIfSelected)
        assert self.inst.addIfSelected == exp

    def testCreationAttribAddIfNotInView(self):
        """ Test whether addIfNotInView attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.addIfNotInView)
        assert self.inst.addIfNotInView == exp

    def testCreationAttribRemoveSimilarEvents(self):
        """ Test whether removeSimilarEvents attrib is created correctly
        """
        exp = co_attribs.AttribBool(**self.removeSimilarEvents)
        assert self.inst.removeSimilarEvents == exp

    def testCheck(self):
        """ Test whether check with no problems returns empty list
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckIcon(self):
        """ Test whether check returns problems in icon attribute
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.brushes.checkBrush.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]
        self.mmod.brushes.checkBrush.return_value = []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests CullList #############################"""

@pytest.fixture(scope="module",autouse=True)
def cullListFixture():
    parseString = (
        "cullList\n"
        +"\tmaxCount 3\n"
        +"\teventTypeCount 1\n"
        +"\tevent \"AllianceCeaseFireFormed\"\n"
    )
    parseDict = {
        "identifier":"cullList",
        "maxCount":{"identifier":"maxCount","val":3},
        "eventTypes":{
            "counter":{"identifier":"eventTypeCount","val":1},
            "elements":[
                {"identifier":"event","val":"AllianceCeaseFireFormed"}
            ]
        }
    }
    return parseString,parseDict

class TestCullList:
    desc = "Tests CullList:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,cullListFixture):
        cls.parseString,parseDict = cullListFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.parent = mock.create_autospec(events.GameEventDef,spec_set=True)
        self.inst = events.CullList(
                identifier=self.identifier,
                maxCount=self.maxCount,
                parent=self.parent,
                eventTypes=self.eventTypes
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_cullList.parseString(self.parseString)[0]
        res = events.CullList(parent=self.parent,**parseRes)
        assert res == self.inst

    def testCreationAttribMaxCount(self):
        """ Test whether maxCount attribute is created correctly
        """
        exp = co_attribs.AttribNum(**self.maxCount)
        assert self.inst.maxCount == exp

    def testCreationAttribEventTypes(self):
        """ Test whether eventTypes attribute is created correctly
        """
        exp = co_attribs.AttribList(elemType=events.GameEventRef,
                elemArgs={"parent":self.parent},
                **self.eventTypes)
        assert self.inst.eventTypes == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.eventTypes,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckProbEventTypes(self):
        """ Test whether check returns problems from eventTypes attrib
        """
        with mock.patch.object(self.inst.eventTypes,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests GameEventDef ###########################"""

class TestGameEventDef:
    desc = "Tests GameEventDef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,gameEventFixture,cullListFixture):
        _,cullDict = cullListFixture
        _,geDict = gameEventFixture
        cls.filepath = "./GameEventData.gameeventdata"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.resetSelectedEventTime = {"identifier":"resetSelectedGameEventTime",
            "val":8}
        cls.showNewEventTime = {"identifier":"showNewGameEventTime",
            "val":30}
        cls.fadeEventTime = {"identifier":"fadeGameEventTime","val":4}
        cls.removeExpiredEventTime = {"identifier":"removeExpiredGameEventTime",
            "val":1200}
        cls.events = [
            geDict,
            {
                "identifier":"AllianceCeaseFireFormed",
                "category":{"identifier":"categoryId","val":"Alliance"},
                "icon":{"identifier":"hudButtonBrush","val":"testbrush2"},
                "entityThrottle":{"identifier":"entityThrottleTime",
                    "val":0.0},
                "soundThrottle":{"identifier":"globalSoundThrottleTime",
                    "val":0.0},
                "throttledEvent":{"identifier":"thottleSharedGameEventId",
                    "val":"Invalid"},
                "addIfNotFocused":{"identifier":"onlyAddIfNotFocused",
                    "val":False},
                "addIfSelected":{"identifier":"onlyAddIfSelected",
                    "val":False},
                "addIfNotInView":{"identifier":"onlyAddIfNotInView",
                    "val":False},
                "removeSimilarEvents":{
                    "identifier":"shouldRemoveAllSimilarEvents",
                    "val":True}
            }

        ]
        cls.cullLists = {
            "counter":{"identifier":"cullLists","val":1},
            "elements":[cullDict]
        }
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.GameEventDef(
                filepath=self.filepath,
                resetSelectedEventTime=self.resetSelectedEventTime,
                showNewEventTime=self.showNewEventTime,
                fadeEventTime=self.fadeEventTime,
                removeExpiredEventTime=self.removeExpiredEventTime,
                events=self.events,
                cullLists=self.cullLists
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from file
        """
        res = events.GameEventDef.createInstance(self.filepath)
        assert res == self.inst

    def testCreationAttribResetSelectedEventTime(self):
        """ Test whether resetSelectedEventTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.resetSelectedEventTime)
        assert self.inst.resetSelectedEventTime == exp

    def testCreationAttribShowNewEventTime(self):
        """ Test whether showNewEventTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.showNewEventTime)
        assert self.inst.showNewEventTime == exp

    def testCreationAttribFadeEventTime(self):
        """ Test whether fadeEventTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.fadeEventTime)
        assert self.inst.fadeEventTime == exp

    def testCreationAttribRemoveExpiredEventTime(self):
        """ Test whether removeExpiredEventTime attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.removeExpiredEventTime)
        assert self.inst.removeExpiredEventTime == exp

    def testCreationAttribEvents(self):
        """ Test whether events attrib is created correctly
        """
        exp = {e["identifier"]:events.GameEvent(parent=self.inst,**e)
            for e in self.events}
        assert self.inst.events == exp

    def testCreationAttribCullLists(self):
        """ Test whether cullLists attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=events.CullList,
                elemArgs={"parent":self.inst},
                **self.cullLists)
        assert self.inst.cullLists == exp

    @pytest.fixture()
    def mockEvents(self):
        mockEvents = []
        patchers = []
        for e in self.inst.events.values():
            tmp = mock.patch.object(e,"check",autospec=True,spec_set=True,
                    return_value=[])
            patchers.append(tmp)
            mockEvents.append(tmp.start())
        yield mockEvents
        for p in patchers:
            p.stop()

    def testCheck(self,mockEvents):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []
        for m in mockEvents:
            m.assert_called_once_with(self.mmod,False)

    def testCheckEvents(self,mockEvents):
        """ Test whether check returns problems from events attrib
        """
        mockProb1 = mock.MagicMock(probFile=None)
        mockProb1.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        mockProb2 = mock.MagicMock(probFile=None)
        mockProb2.mock_add_spec(co_probs.BasicProblem,spec_set=True)
        mockEvents[0].return_value = [mockProb1]
        mockEvents[1].return_value = [mockProb2]
        res = self.inst.check(self.mmod)
        assert len(res) == 2
        assert mockProb1 in res
        assert mockProb2 in res

    def testCheckCullList(self,mockEvents,mockProb):
        """ Test whether check returns problems from cullLists attrib
        """
        with mock.patch.object(self.inst.cullLists,"check",autospec=True,
                spec_set=True,return_value=[mockProb]):
            res = self.inst.check(self.mmod)
            assert res == [mockProb]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testGatherRefsAll(self):
        """ Test whether gatherRefs returns all references
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res)
        assert "brush" in res
        assert len(res["brush"]) == 2
        assert "testbrush1" in res["brush"]
        assert "testbrush2" in res["brush"]

    def testGatherRefsBrush(self):
        """ Test whether gatherRefs returns brush references
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,types={"brush"})
        assert "brush" in res
        assert len(res["brush"]) == 2
        assert "testbrush1" in res["brush"]
        assert "testbrush2" in res["brush"]

    def testGatherRefsNA(self):
        """ Test whether gatherRefs returns empty with no fitting types
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res,types={"audiofile"})
        assert len(res) == 0

    def testGetRefs(self):
        """ Test whether getRefs returns all references
        """
        res = self.inst.getRefs()
        assert len(res) == 3
        for e in self.inst.events.values():
            assert e.icon in res
        for l in self.inst.cullLists.elements:
            for r in l.eventTypes.elements:
                assert r in res

"""######################### Tests SpawnProjectile #########################"""

@pytest.fixture(scope="module")
def spawnProjectileFixture():
    parseString = (
        "spawnType \"Projectile\"\n"
        +"projectileTarget \"RandomConnectedLocation\"\n"
        +"spawnEntity \"testentity\"\n"
    )
    parseDict = {
        "spawnType":{"identifier":"spawnType",
            "val":"Projectile"},
        "target":{"identifier":"projectileTarget",
            "val":"RandomConnectedLocation"},
        "entity":{"identifier":"spawnEntity","val":"testentity"}
    }
    return parseString,parseDict

class TestSpawnProjectile:
    desc = "Tests SpawnProjectile:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,spawnProjectileFixture):
        cls.parseString,parseDict = spawnProjectileFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.SpawnProjectile(
                spawnType=self.spawnType,
                target=self.target,
                entity=self.entity
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_spawnType.parseString(self.parseString)[0]
        res = events.SpawnType.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribTarget(self):
        """ Test whether target attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=events.PROJECTILE_TARGETS,
                **self.target)
        assert self.inst.target == exp

    def testCreationAttribEntity(self):
        """ Test whether entity attrib is created correctly
        """
        exp = coe.EntityRef(types=["CannonShell"],**self.entity)
        assert self.inst.entity == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.entity,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == []

    def testCheckEntity(self):
        """ Test whether check returns problems from entity attrib
        """
        with mock.patch.object(self.inst.entity,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################### Tests RacialBuff ############################"""

@pytest.fixture(scope="module")
def racialBuffFixture():
    parseString = (
        "race \"myrace\"\n"
        +"buff \"mybuff\"\n"
    )
    parseDict = {
        "race":{"identifier":"race","val":"myrace"},
        "buff":{"identifier":"buff","val":"mybuff"}
    }
    return parseString,parseDict

class TestRacialBuff:
    desc = "Tests RacialBuff:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,racialBuffFixture):
        cls.parseString,parseDict = racialBuffFixture
        tco.expandParseDict(cls,parseDict)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.RacialBuff(
                race=self.race,
                buff=self.buff
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_racialBuff.parseString(self.parseString)[0]
        res = events.RacialBuff(**parseRes)
        assert res == self.inst

    def testCreationAttribRace(self):
        """ Test whether race attrib is created correctly
        """
        exp = ui.StringReference(**self.race)
        assert self.inst.race == exp

    def testCreationAttribBuff(self):
        """ Test whether buff attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        assert self.inst.buff == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == []
            mocked.assert_called_once_with(self.mmod,False)
            self.mmod.strings.checkString.assert_called_once_with("myrace")

    def testCheckRace(self):
        """ Test whether check returns problems from race attrib
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.strings.checkString.return_value = [mockProb]
        with mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[]):
            res = self.inst.check(self.mmod)
            assert res == [mockProb]

    def testCheckBuff(self):
        """ Test whether check returns problems from buff attrib
        """
        with mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################### Tests RacialBuffAtLoc #########################"""

@pytest.fixture(scope="module")
def racialBuffAtLocFixture(racialBuffFixture):
    buffString,buffDict = racialBuffFixture
    parseString = (
        "spawnType \"RacialBuffAtLocation\"\n"
        +"racialBuffCount 1\n"
        +buffString
    )
    parseDict = {
        "spawnType":{"identifier":"spawnType","val":"RacialBuffAtLocation"},
        "buffs":{
            "counter":{"identifier":"racialBuffCount","val":1},
            "elements":[buffDict]
        }
    }
    return parseString,parseDict

class TestRacialBuffAtLoc:
    desc = "Tests RacialBuffAtLoc:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,racialBuffAtLocFixture):
        cls.parseString,parseDict = racialBuffAtLocFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.RacialBuffAtLoc(
                spawnType=self.spawnType,
                buffs=self.buffs
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_spawnType.parseString(self.parseString)[0]
        res = events.SpawnType.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribBuffs(self):
        """ Test whether buffs attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=events.RacialBuff,**self.buffs)
        assert self.inst.buffs == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.buffs,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == []
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckBuffs(self):
        """ Test whether check returns problems from buffs attrib 
        """
        with mock.patch.object(self.inst.buffs,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

"""############################ Tests BuffAtLoc ############################"""

@pytest.fixture(scope="module")
def buffAtLocFixture():
    parseString = (
        "spawnType \"BuffAtLocation\"\n"
        +"spawnEntity \"mybuff\"\n"
    )
    parseDict = {
        "spawnType":{"identifier":"spawnType","val":"BuffAtLocation"},
        "buff":{"identifier":"spawnEntity","val":"mybuff"}
    }
    return parseString,parseDict

class TestBuffAtLoc:
    desc = "Tests BuffAtLoc:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,buffAtLocFixture):
        cls.parseString,parseDict = buffAtLocFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.BuffAtLoc(
                spawnType=self.spawnType,
                buff=self.buff
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_buffAtLoc.parseString(self.parseString)[0]
        res = events.SpawnType.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribBuff(self):
        """ Test whether buff attrib is created correctly
        """
        exp = coe.EntityRef(types=["Buff"],**self.buff)
        assert self.inst.buff == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        with mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[]) as mocked:
            res = self.inst.check(self.mmod)
            assert res == []
            mocked.assert_called_once_with(self.mmod,False)

    def testCheckBuff(self):
        """ Test whether check returns problems from buff attrib
        """
        with mock.patch.object(self.inst.buff,"check",autospec=True,
                spec_set=True,return_value=[mock.sentinel.prob]):
            res = self.inst.check(self.mmod)
            assert res == [mock.sentinel.prob]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""######################## Tests PeriodicBuffAtLoc ########################"""

class TestPeriodicBuffAtLoc:
    desc = "Tests PeriodicBuffAtLoc:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = (
            "spawnType \"PeriodicBuffAtLocation\"\n"
            +"spawnEntity \"mybuff\"\n"
            +"duration 300.000000\n"
            +"period 5.000000\n"
        )
        cls.spawnType = {"identifier":"spawnType",
            "val":"PeriodicBuffAtLocation"}
        cls.buff = {"identifier":"spawnEntity","val":"mybuff"}
        cls.duration = {"identifier":"duration","val":300.0}
        cls.period = {"identifier":"period","val":5.0}
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.PeriodicBuffAtLoc(
                spawnType=self.spawnType,
                buff=self.buff,
                duration=self.duration,
                period=self.period
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_spawnType.parseString(self.parseString)[0]
        res = events.SpawnType.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribDuration(self):
        """ Test whether duration attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.duration)
        assert self.inst.duration == exp

    def testCreationAttribPeriod(self):
        """ Test whether period attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.period)
        assert self.inst.period == exp

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests SpawnTimer ###########################"""

@pytest.fixture(scope="module")
def spawnTimerFixture():
    parseDict = {
        "spawnReq":{"identifier":"spawnRequirement","val":"Timer"},
        "nextSpawnTimeMin":{"identifier":"nextSpawnTimeMin","val":300.0},
        "nextSpawnTimeMax":{"identifier":"nextSpawnTimeMax","val":1200.0}
    }
    return parseDict

class TestSpawnTimerNoRespawn:
    desc = "Tests SpawnTimer without respawn count:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,spawnTimerFixture):
        cls.parseString = (
            "spawnRequirement \"Timer\"\n"
            +"nextSpawnTimeMin 300.000000\n"
            +"nextSpawnTimeMax 1200.000000\n"
        )
        tco.expandParseDict(cls,spawnTimerFixture)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.SpawnTimer(
                spawnReq=self.spawnReq,
                nextSpawnTimeMin=self.nextSpawnTimeMin,
                nextSpawnTimeMax=self.nextSpawnTimeMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_spawnRequirement.parseString(self.parseString)[0]
        res = events.SpawnRequirement.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribNextSpawnTimeMin(self):
        """ Test whether nextSpawnTimeMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nextSpawnTimeMin)
        assert self.inst.nextSpawnTimeMin == exp

    def testCreationAttribNextSpawnTimeMax(self):
        """ Test whether nextSpawnTimeMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nextSpawnTimeMax)
        assert self.inst.nextSpawnTimeMax == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

class TestSpawnTimerRespawn:
    desc = "Tests SpawnTimer with respawn count:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,spawnTimerFixture):
        parseDict = spawnTimerFixture
        cls.parseString = (
            "spawnRequirement \"Timer\"\n"
            +"respawnCount 3\n"
            +"nextSpawnTimeMin 300.000000\n"
            +"nextSpawnTimeMax 1200.000000\n"
        )
        parseDict.update({
            "respawn":{"identifier":"respawnCount","val":3}
        })
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def setUp(self):
        self.inst = events.SpawnTimer(
                spawnReq=self.spawnReq,
                respawn=self.respawn,
                nextSpawnTimeMin=self.nextSpawnTimeMin,
                nextSpawnTimeMax=self.nextSpawnTimeMax
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_spawnRequirement.parseString(self.parseString)[0]
        res = events.SpawnRequirement.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribRespawn(self):
        """ Test whether respawn attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.respawn)
        assert self.inst.respawn == exp

    def testCreationAttribNextSpawnTimeMin(self):
        """ Test whether nextSpawnTimeMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nextSpawnTimeMin)
        assert self.inst.nextSpawnTimeMin == exp

    def testCreationAttribNextSpawnTimeMax(self):
        """ Test whether nextSpawnTimeMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.nextSpawnTimeMax)
        assert self.inst.nextSpawnTimeMax == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################### Tests SpawnWeighted #########################"""

@pytest.fixture(scope="module")
def spawnWeightedFixture():
    parseString = (
        "spawnRequirement \"TriggerCultureFlip\"\n"
        +"triggerWeight 0.100000\n"
    )
    parseDict = {
        "spawnReq":{"identifier":"spawnRequirement",
            "val":"TriggerCultureFlip"},
        "weight":{"identifier":"triggerWeight","val":0.1}
    }
    return parseString,parseDict

class TestSpawnWeighted:
    desc = "Tests SpawnWeighted:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,spawnWeightedFixture):
        cls.parseString,parseDict = spawnWeightedFixture
        tco.expandParseDict(cls,parseDict)
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.SpawnWeighted(
                spawnReq=self.spawnReq,
                weight=self.weight
        )

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_spawnRequirement.parseString(self.parseString)[0]
        res = events.SpawnRequirement.factory(**parseRes)
        assert res == self.inst

    def testCreationAttribWeight(self):
        """ Test whether weight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.weight)
        assert self.inst.weight == exp

    def testCheck(self):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""############################ Tests RandomEvent ##########################"""

@pytest.fixture(scope="module")
def randEventFixture(buffAtLocFixture,spawnWeightedFixture):
    buffStr,buffDict = buffAtLocFixture
    spawnStr,spawnDict = spawnWeightedFixture
    parseString = (
        "randomEventDef\n"
        +"\tid \"ECONOMICDOWNTURN\"\n"
        +"\tnameID \"namestring\"\n"
        +"\twarningID \"warningstring\"\n"
        +co_misc.indentText(buffStr,1)
        +"\tplayerOwnerType \"LocationOwner\"\n"
        +"\tspawnLocation \"AllLocations\"\n"
        +"\texcludedSpawnLocationCount 1\n"
        +"\texcludedSpawnLocation \"excludedloc\"\n"
        +co_misc.indentText(spawnStr,1)
        +"\tspawnWeight 0.200000\n"
    )
    parseDict = {
        "identifier":"randomEventDef",
        "id":{"identifier":"id","val":"ECONOMICDOWNTURN"},
        "name":{"identifier":"nameID","val":"namestring"},
        "warning":{"identifier":"warningID","val":"warningstring"},
        "spawnType":buffDict,
        "ownerType":{"identifier":"playerOwnerType","val":"LocationOwner"},
        "location":{"identifier":"spawnLocation","val":"AllLocations"},
        "excludedLocs":{
            "counter":{"identifier":"excludedSpawnLocationCount","val":1},
            "elements":[
                {"identifier":"excludedSpawnLocation","val":"excludedloc"}
            ]
        },
        "spawnReq":spawnDict,
        "spawnWeight":{"identifier":"spawnWeight","val":0.2}
    }
    return parseString,parseDict

class TestRandomEvent:
    desc = "Tests RandomEvent:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,randEventFixture):
        cls.parseString,parseDict = randEventFixture
        tco.expandParseDict(cls,parseDict)

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.RandomEvent(
                identifier=self.identifier,
                id=self.id,
                name=self.name,
                warning=self.warning,
                spawnType=self.spawnType,
                ownerType=self.ownerType,
                location=self.location,
                excludedLocs=self.excludedLocs,
                spawnReq=self.spawnReq,
                spawnWeight=self.spawnWeight
        )
        self.mmod = tco.genMockMod("./")

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = events.g_randomEvent.parseString(self.parseString)[0]
        res = events.RandomEvent(**parseRes)
        assert res == self.inst

    def testCreationAttribId(self):
        """ Test whether id attrib is created correctly
        """
        exp = co_attribs.AttribString(**self.id)
        assert self.inst.id == exp

    def testCreationAttribName(self):
        """ Test whether name attrib is created correctly
        """
        exp = ui.StringReference(**self.name)
        assert self.inst.name == exp

    def testCreationAttribWarning(self):
        """ Test whether warning attrib is created correctly
        """
        exp = ui.StringReference(**self.warning)
        assert self.inst.warning == exp

    def testCreationAttribSpawnType(self):
        """ Test whether spawnType attrib is created correctly
        """
        exp = events.BuffAtLoc(**self.spawnType)
        assert self.inst.spawnType == exp

    def testCreationAttribOwnerType(self):
        """ Test whether ownerType attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=events.OWNER_TYPES,
                **self.ownerType)
        assert self.inst.ownerType == exp

    def testCreationAttribLocation(self):
        """ Test whether location attrib is created correctly
        """
        exp = co_attribs.AttribEnum(possibleVals=events.SPAWN_LOCATIONS,
                **self.location)
        assert self.inst.location == exp

    def testCreationAttribExcludedLocs(self):
        """ Test whether excludedLocs attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=coe.EntityRef,
                elemArgs={"types":["Planet","Star"]},
                **self.excludedLocs)
        assert self.inst.excludedLocs == exp

    def testCreationAttribSpawnReq(self):
        """ Test whether spawnReq attrib is created correctly
        """
        exp = events.SpawnWeighted(**self.spawnReq)
        assert self.inst.spawnReq == exp

    def testCreationAttribSpawnWeight(self):
        """ Test whether spawnWeight attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.spawnWeight)
        assert self.inst.spawnWeight == exp

    @pytest.fixture()
    def checkFixture(self):
        patcherSpawn = mock.patch.object(self.inst.spawnType,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockSpawnType = patcherSpawn.start()
        patcherLocs = mock.patch.object(self.inst.excludedLocs,"check",
                autospec=True,spec_set=True,return_value=[])
        self.mockExcludedLocs = patcherLocs.start()
        yield
        patcherSpawn.stop()
        patcherLocs.stop()

    def testCheck(self,checkFixture):
        """ Test whether check returns empty list when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckName(self,checkFixture):
        """ Test whether check returns problems from name attrib
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.strings.checkString.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]
        self.mmod.strings.checkString.assert_any_call(
                self.name["val"])

    def testCheckWarning(self,checkFixture):
        """ Test whether check returns problems from warning attrib
        """
        mockProb = mock.MagicMock(probLine=None)
        mockProb.mock_add_spec(
                co_probs.BasicProblem,spec_set=True)
        self.mmod.strings.checkString.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        assert res == [mockProb]
        self.mmod.strings.checkString.assert_any_call(
                self.warning["val"])

    def testCheckSpawnType(self,checkFixture):
        """ Test whether check returns problems from spawnType attrib
        """
        self.mockSpawnType.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockSpawnType.assert_called_once_with(self.mmod,False)

    def testCheckExcludedLocs(self,checkFixture):
        """ Test whether check returns problems from excludedLocs attrib
        """
        self.mockExcludedLocs.return_value = [mock.sentinel.prob]
        res = self.inst.check(self.mmod)
        assert res == [mock.sentinel.prob]
        self.mockExcludedLocs.assert_called_once_with(self.mmod,False)

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

"""########################## Tests RandomEventDefs ########################"""

class TestRandomEventDef:
    desc = "Tests RandomEventDef:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls,randEventFixture,spawnProjectileFixture,
            racialBuffAtLocFixture,spawnWeightedFixture):
        _,eventDict = randEventFixture
        _,projectileDict = spawnProjectileFixture
        _,racialDict = racialBuffAtLocFixture
        _,weightedDict = spawnWeightedFixture
        cls.filepath = "./RandomEventDefs.randomeventdefs"
        with open(cls.filepath,'r',encoding="utf-8-sig") as f:
            cls.parseString = f.read()
        cls.initialSpawnTimeMin = {"identifier":"initialSpawnTimeMin",
            "val":480.0}
        cls.initialSpawnTimeMax = {"identifier":"initialSpawnTimeMax",
            "val":780.0}
        cls.failedRespawnTimeMin = {"identifier":"failedRespawnTimeMin",
            "val":120.0}
        cls.failedRespawnTimeMax = {"identifier":"failedRespawnTimeMax",
            "val":300.0}
        cls.lowAllegianceCheckPeriod = {"identifier":"lowAllegianceCheckPeriod",
            "val":600.0}
        cls.lowAllegianceThresh = {"identifier":"lowAllegianceThreshold",
            "val":0.15}
        cls.events = {
            "counter":{"identifier":"randomEventDefCount","val":3},
            "elements":[
                eventDict,
                {
                    "identifier":"randomEventDef",
                    "id":{"identifier":"id","val":"CORONALMASSEJECTION"},
                    "name":{"identifier":"nameID","val":"namestring"},
                    "warning":{"identifier":"warningID","val":"warningstring"},
                    "spawnType":projectileDict,
                    "ownerType":{"identifier":"playerOwnerType",
                        "val":"LocationOwner"},
                    "location":{"identifier":"spawnLocation",
                        "val":"AllLocations"},
                    "excludedLocs":{
                        "counter":{"identifier":"excludedSpawnLocationCount",
                            "val":0},
                        "elements":[]
                    },
                    "spawnReq":weightedDict,
                    "spawnWeight":{"identifier":"spawnWeight","val":0.2}
                },
                {
                    "identifier":"randomEventDef",
                    "id":{"identifier":"id","val":"PARTISANS"},
                    "name":{"identifier":"nameID","val":"namestring"},
                    "warning":{"identifier":"warningID","val":"warningstring"},
                    "spawnType":racialDict,
                    "ownerType":{"identifier":"playerOwnerType",
                        "val":"LocationOwner"},
                    "location":{"identifier":"spawnLocation",
                        "val":"AllLocations"},
                    "excludedLocs":{
                        "counter":{"identifier":"excludedSpawnLocationCount",
                            "val":0},
                        "elements":[]
                    },
                    "spawnReq":weightedDict,
                    "spawnWeight":{"identifier":"spawnWeight","val":0.2}
                }
            ]
        }
        cls.expRefs = collections.defaultdict(set)
        cls.expRefs["entity"] = {"mybuff.entity","excludedloc.entity",
            "testentity.entity"}
        cls.expRefs["string"] = {"namestring","warningstring","myrace"}
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = events.RandomEventDef(
                filepath=self.filepath,
                initialSpawnTimeMin=self.initialSpawnTimeMin,
                initialSpawnTimeMax=self.initialSpawnTimeMax,
                failedRespawnTimeMin=self.failedRespawnTimeMin,
                failedRespawnTimeMax=self.failedRespawnTimeMax,
                lowAllegianceCheckPeriod=self.lowAllegianceCheckPeriod,
                lowAllegianceThresh=self.lowAllegianceThresh,
                events=self.events
        )
        self.maxDiff = None

    def testCreationParse(self):
        """ Test whether instance is created correctly from file
        """
        res = events.RandomEventDef.createInstance(self.filepath)
        assert res == self.inst

    def testCreationAttribInitialSpawnTimeMin(self):
        """ Test whether initialSpawnTimeMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.initialSpawnTimeMin)
        assert self.inst.initialSpawnTimeMin == exp

    def testCreationAttribInitialSpawnTimeMax(self):
        """ Test whether initialSpawnTimeMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.initialSpawnTimeMax)
        assert self.inst.initialSpawnTimeMax == exp

    def testCreationAttribFailedRespawnTimeMin(self):
        """ Test whether failedRespawnTimeMin attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.failedRespawnTimeMin)
        assert self.inst.failedRespawnTimeMin == exp

    def testCreationAttribFailedRespawnTimeMax(self):
        """ Test whether failedRespawnTimeMax attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.failedRespawnTimeMax)
        assert self.inst.failedRespawnTimeMax == exp

    def testCreationAttribLowAllegianceCheckPeriod(self):
        """ Test whether lowAllegianceCheckPeriod attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.lowAllegianceCheckPeriod)
        assert self.inst.lowAllegianceCheckPeriod == exp

    def testCreationAttribLowAllegianceThresh(self):
        """ Test whether lowAllegianceThresh attrib is created correctly
        """
        exp = co_attribs.AttribNum(**self.lowAllegianceThresh)
        assert self.inst.lowAllegianceThresh == exp

    def testCreationAttribEvents(self):
        """ Test whether events attrib is created correctly
        """
        exp = co_attribs.AttribList(elemType=events.RandomEvent,**self.events)
        assert self.inst.events == exp

    def testGatherRefsAll(self):
        """ Test whether all references are gathered correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res)
        assert res == self.expRefs

    def testGatherRefsString(self):
        """ Test whether string type references are gathered correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res)
        assert res["string"] == self.expRefs["string"]

    def testGatherRefsEntity(self):
        """ Test whether entity type references are gathered correctly
        """
        res = collections.defaultdict(set)
        self.inst.gatherRefs(res)
        assert res["entity"] == self.expRefs["entity"]

    @pytest.fixture()
    def checkFixture(self):
        patcher = mock.patch.object(self.inst.events,"check",autospec=True,
                spec_set=True,return_value=[])
        mockEvents = patcher.start()
        yield mockEvents
        patcher.stop()

    def testCheck(self,checkFixture):
        """ Test whether check returns empty lis when no problems occured
        """
        res = self.inst.check(self.mmod)
        assert res == []

    def testCheckEvents(self,checkFixture,mockProb):
        """ Test whether check returns problems from events attrib
        """
        mockEvents = checkFixture
        mockEvents.return_value = [mockProb]
        res = self.inst.check(self.mmod)
        mockEvents.assert_called_once_with(self.mmod,False)
        assert res == [mockProb]

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString
