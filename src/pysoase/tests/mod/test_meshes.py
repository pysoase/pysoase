import os

import pytest

import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.meshes as meshes
import pysoase.tests.conftest as tco

testdata = os.path.join("mod","meshes")

"""############################# Tests MeshRef #############################"""

class TestMeshRef:
    desc = "Test MeshRef with good input:"

    @classmethod
    @pytest.fixture(scope="class",autouse=True)
    def classFixture(cls):
        cls.parseString = "testRef \"TestMesh\"\n"
        cls.identifier = "testRef"
        cls.val = "TestMesh"
        cls.mmod = tco.genMockMod("./")

    @pytest.fixture(autouse=True)
    def testFixture(self):
        self.inst = meshes.MeshRef(
                identifier=self.identifier,
                val=self.val)

    def testCreationParse(self):
        """ Test whether instance is created correctly from parse results
        """
        parseRes = co_parse.genStringAttrib("testRef").parseString(
                self.parseString)[0]
        res = meshes.MeshRef(**parseRes)
        assert res == self.inst

    def testToString(self):
        """ Test whether string representation is constructed correctly
        """
        assert self.inst.toString(0) == self.parseString

    def testCheckEmpty(self):
        """ Test whether check returns no problems
        """
        assert self.inst.check(self.mmod) == []

    def testCheckWrongCase(self):
        """ Test whether check handles differences in name case correctly
        """
        m = meshes.MeshRef(identifier="testRef",val="TESTMESH")
        assert m.check(self.mmod) == []

    def testCheckMissing(self):
        """ Test whether check produces a MissingRef problem
        """
        m = meshes.MeshRef(identifier="testRef",val="MissingRef")
        res = m.check(self.mmod)
        assert len(res) == 1
        assert isinstance(res[0],co_probs.MissingRefProblem)
        assert res[0].ref == "MissingRef.mesh"