""" CLI Frontend for the PySoaSE library
"""
import argparse
import configparser
import os
import sys

from pysoase.common.problems import ProblemSeverity
from pysoase.common.constants import FILEREF_TO_EXT
from pysoase.entities.component import ModEntities
from pysoase.tools import checking
from pysoase.tools import modify
from pysoase.tools import showing
from pysoase.tools import unref

VERSION = "PySoaSE 1.6.0"
commandDict = {
    "check":checking,
    "modify":modify,
    "show":showing,
#    "unref":unref
}

main_parser = argparse.ArgumentParser(
        description="Some usefull tools for Sins modding.")
main_parser.add_argument("--version",action="version",version=VERSION)
main_parser.add_argument("--quiet",action="store_true",
        help="Suppress all progress output")
main_parser.add_argument(
        "-o",
        "--out",
        help="File to write output to (all former contents will be overrriden!)",
        default="-",
        type=argparse.FileType("w")
)
subparsers = main_parser.add_subparsers(dest="command",title="subcommands",
        metavar="<command>")

check = subparsers.add_parser(
        "check",
        description=("Check a whole mod or subsets thereof for a large number "
                     "of different bugs and errors. Examples are: Referenced "
                     "but missing files, syntax errors or missing entries in "
                     "manifest files."),
        help="Check a mod for errors and bugs.")
check.add_argument(
        "-m",
        "--mod",
        help="The mods root dir",
        default=None,
        dest="moddir",
        metavar="<modpath>"
)
check.add_argument(
        "--rebref",
        help="The directory with the rebellion Sins files in TXT format",
        default=None,
        dest="rebrefdir",
        metavar="<rebrefpath>"
)
check.add_argument(
        "--store",
        help="Directory to store problems in pickled format. Files will be "
             +"named with date of creation.",
        default=None,
        dest="store",
        metavar="<storage directory>"
)
check.add_argument(
        "-s",
        "--sev",
        help="The minimum problem severity to return",
        default=ProblemSeverity.warn,
        type=ProblemSeverity.toSeverity,
        choices=[mem for name,mem in ProblemSeverity.__members__.items()],
        dest="severity",
        metavar="<severity>"
)
check_subparsers = check.add_subparsers(title="subcheckers",
        metavar="<subchecker>")
checker_syntax = check_subparsers.add_parser(
        "syntax",
        description="Check the given files for syntactic errors",
        help="Check the given files for syntactic errors")
checker_syntax.add_argument(
        "files",
        help="The files to check",
        metavar="file",
        nargs="*")
checker_syntax.set_defaults(subcmd="CheckerSyntax")
checker_general = check_subparsers.add_parser(
        "general",
        description="Check the given files for problems",
        help="Check the given files for problems")
checker_general.add_argument(
        "files",
        help="The files to check",
        metavar="file",
        nargs="*")
checker_general.set_defaults(subcmd="CheckerGeneral")

checker_entities = check_subparsers.add_parser(
        "entities",
        description="Check the given entity types",
        help="Check the given types of entities in the given GameInfo dir.")
checker_entities.add_argument(
        "gameInfoDir",
        help="The GameInfo directory where the entities live",
        metavar="<GameInfo DIRECTORY>")
checker_entities.add_argument(
        "types",
        nargs="*",
        default="",
        choices=list(ModEntities.ENT_TO_CLASS.keys()).append(""),
        help="The entity types which should be checked from the given GameInfo dir",
        metavar="type")
checker_entities.set_defaults(subcmd="CheckerEntities")

checker_mod = check_subparsers.add_parser(
        "mod",
        description=("Run all implemented tests and checks on the mod "
                     +"found in the given mod directory."),
        help="Check the given mod")
checker_mod.add_argument(
        "directory",
        help="The root directory of the mod",
        metavar="<MOD ROOT DIRECTORY>")
checker_mod.add_argument(
        "--strict",
        help="Only check files present in manifests",
        action="store_true")
checker_mod.set_defaults(subcmd="CheckerMod")

cmd_showing = subparsers.add_parser(
        "show",
        description=("Extract information from Sins files or a whole mod,"
                     "for example all references in a given file."),
        help="Print information about a Sins file")
cmd_showing.add_argument(
        "-m",
        "--mod",
        help="The mods root dir",
        default=None,
        dest="moddir",
        metavar="<modpath>"
)
cmd_showing.add_argument(
        "--rebref",
        help="The directory with the rebellion Sins files in TXT format",
        default=None,
        dest="rebrefdir",
        metavar="<rebrefpath>"
)
showing_subparsers = cmd_showing.add_subparsers(title="Printers",
        metavar="<printer>")
printer_refs = showing_subparsers.add_parser(
        "refs",
        description="Show all references in a file",
        help="Extract and disply all references in a given file or files")
printer_refs.add_argument(
        "files",
        help="The files to print",
        metavar="file",
        nargs="+")
printer_refs.add_argument("-c","--combined",
        help="Show references ordered by type, not by file",action="store_true",
        default=False)
printer_refs.set_defaults(subcmd="ShowRefs")
printer_buffchains = showing_subparsers.add_parser(
        "buffchains",
        description="Print Buffchains emanating from an Ability or Buff",
        help="Print Buffchains emanating from an Ability or Buff")
printer_buffchains.add_argument(
        "files",
        help="The starting points for the Buffchains to print",
        metavar="file",
        nargs="+")
printer_buffchains.set_defaults(subcmd="ShowBuffchain")
showing_stringdiff = showing_subparsers.add_parser(
        "stringdiff",
        description="Show String differences between mod and vanilla",
        help="Show all strings in a mod's stringfile which are either not in "
        +"the vanilla file or which are different than in the vanilla file")
showing_stringdiff.add_argument(
        "language",
        help="The specific language string file to be compared to vanilla",
        metavar="language")
showing_stringdiff.add_argument("-u","--update",
        help="Update stringdiff files in the current directory or OUTDIR"
        +" and output strings added since those files were produced",
        action="store_true")
showing_stringdiff.add_argument("--outdir",
        help="Storage location for stringdiff files",
        default=False)
showing_stringdiff.set_defaults(subcmd="ShowStringdiff")

# cmd_unref = subparsers.add_parser(
#         "unref",
#         description=("Check whether specific files or whole categories of "
#                      "files are referenced anywhere in the mod. "
#                      "Entries in files, like Brushes or Strings, can also be "
#                      "checked."),
#         help="Check a mod for unused files.")
# cmd_unref.add_argument(
#         "-m",
#         "--mod",
#         help="The mods root dir",
#         default=None,
#         dest="moddir",
#         metavar="<modpath>"
# )
# cmd_unref.add_argument(
#         "--rebref",
#         help="The directory with the rebellion Sins files in TXT format",
#         default=None,
#         dest="rebrefdir",
#         metavar="<rebrefpath>"
# )
# unref_subparsers = cmd_unref.add_subparsers(title="by",
#         metavar="<by>")
# unref_file = unref_subparsers.add_parser(
#         "files",
#         description="Check whether the given files are used",
#         help="Check whether the given files are used")
# unref_file.add_argument(
#         "files",
#         help="The files to check",
#         metavar="file",
#         nargs="+")
# unref_file.set_defaults(subcmd="UnrefFiles")
#
# unref_types = unref_subparsers.add_parser(
#         "types",
#         description="Check whether all files of given type are used",
#         help="Check whether all files of given type are used")
# unref_types.add_argument(
#         "directory",
#         help="The root directory of the mod",
#         metavar="<MOD ROOT DIRECTORY>")
# unref_types.add_argument(
#         "types",
#         choices=FILEREF_TO_EXT.keys(),
#         help="The types to check",
#         metavar="type",
#         nargs="*")
# unref_types.set_defaults(subcmd="UnrefTypes")

cmd_modify = subparsers.add_parser(
        "modify",
        description=("Apply batch changes to any number of Sins files."
                     "The changes defined in the modification string are "
                     "applied to any attribute line in any of the given files "
                     "which match the identifier name.\n"
                     "The modification string has the form "
                     "\'IDENTIFIER:OPERATORVALUE[;IDENTIFIER:OPERATORVALUE]\', "
                     "e.g. \'ident:+5\' will add "
                     "5 to the value of any line with the \'ident\' identifier."),
        help="Apply batch modifications to files.")
cmd_modify.add_argument(
        "modStr",
        help="The modifications to be applied",
        metavar="<MODIFICATION DEFINITION>")
cmd_modify.add_argument(
        "files",
        help="The files to modify",
        metavar="file",
        nargs="+")
cmd_modify.set_defaults(subcmd="Modify")

def entry():
    confpath = os.path.expanduser("~/.pysoase")
    config = configparser.ConfigParser()
    args = main_parser.parse_args()
    toolclass = getattr(commandDict[args.command],args.subcmd)
    args = vars(args)
    if os.path.exists(confpath):
        config.read(confpath)
        if "all" in config:
            args.update(config["all"])
        else:
            print("Warning: You do not have a \"all\" section in your "
                  "config file")
        if args["command"] in config:
            args.update(config[args["command"]])
    del args["command"]
    del args["subcmd"]
    retVal = toolclass.execute(args)
    sys.exit(retVal)
