""" Tools for mod checks
"""

import abc
import os
import pickle
from datetime import datetime

import tqdm

import pysoase.common.problems as co_probs
import pysoase.tools.tools as tools

"""############################ Checker Classes ############################"""

class Checker(tools.Tool):
    def __init__(self,severity,store=None,**kwargs):
        super().__init__(**kwargs)
        self.severity = severity
        self.store = store

    @abc.abstractmethod
    def check(self):
        pass

    def storeProblems(self,probs):
        currTime = datetime.utcnow()
        fname = currTime.strftime("%Y%m%d%H%M%S")+".probs"
        if not os.path.exists(self.store):
            os.mkdir(self.store)
        with open(os.path.join(self.store,fname),'wb') as f:
            pickle.dump(probs,f)

    def _computeResults(self):
        res = self.check()
        res = tools.filterProblems(res,self.severity,self.mod.quiet)
        if self.store:
            self.storeProblems(res)
        retVal = 1
        for p in res:
            if p.severity==co_probs.ProblemSeverity.error:
                break
        else:
            retVal = 0
        return retVal,tools.printProblems(res,quiet=self.mod.quiet)

class CheckerSyntax(Checker):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def check(self):
        res = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.files),
                desc="Checking syntax") as pbar:
            for f in self.files:
                if os.path.isfile(f):
                    fileClass = tools.getFileClass(f)
                    succ,probs = fileClass.parse(f)
                    if not succ:
                        res.extend(probs)
                elif os.path.isdir(f):
                    dirComps = tools.getModClass(f,self.mod)
                    [res.extend(comp.checkSyntax()) for comp in dirComps]
                pbar.update()
        return res

class CheckerGeneral(Checker):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def check(self):
        res = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.files),
                desc="General Checks") as pbar:
            for f in self.files:
                if os.path.isfile(f):
                    fileComp = tools.fileToComponen(f,self.mod)
                    res.extend(fileComp.checkFile(f))
                elif os.path.isdir(f):
                    dirComps = tools.getModClass(f,self.mod)
                    [res.extend(comp.check()) for comp in dirComps]
                pbar.update()
        return res

class CheckerEntities(Checker):
    def __init__(self,gameInfoDir,types=[],**kwargs):
        super().__init__(files=[gameInfoDir],**kwargs)
        self.types = types

    def check(self):
        if not self.types:
            return self.mod.entities.checkType(
                    sorted(list(self.mod.entities.ENT_TO_CLASS.keys())))
        else:
            return self.mod.entities.checkType(self.types)

class CheckerMod(Checker):
    def __init__(self,directory,strict,**kwargs):
        kwargs["moddir"] = directory
        super().__init__(files=[],**kwargs)
        self.strict = strict

    def check(self):
        return self.mod.check(self.strict)
