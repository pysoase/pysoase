""" Base classes and helper functions for Sins tools
"""

import abc
import os
import sys
from collections import defaultdict

import tqdm

import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.entities.component as ecomp
import pysoase.entities.uncolonizables as uncol
import pysoase.mod.events as events
import pysoase.mod.manifest as manifest
import pysoase.mod.mod as mod
import pysoase.mod.particles as par
import pysoase.mod.scenarios as scen
import pysoase.mod.texanim as anims
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui
import pysoase.mod.visuals as vis

"""############################ Helper Functions ###########################"""

MOD_SUBDIRS = ["Galaxy","GameInfo","Mesh","Movie","Particle","PipelineEffect",
    "Sound","String","Textures","TextureAnimations","Window"]

EXT_TO_COMP = {
    ".asteroidDef":"entities",
    ".brushes":"brushes",
    ".entity":"entities",
    ".explosiondata":None,
    ".galaxy":"scenarios",
    ".galaxyScenarioDef":"scenarios",
    ".gameeventdata":None,
    ".manifest":None,
    ".particle":"particles",
    ".playerThemes":"themes",
    ".playerPictures":"themes",
    ".playerThemesDef":"themes",
    ".playerPicturesDef":"themes",
    ".randomeventdefs":None,
    ".renderingDef":"entities",
    ".skyboxbackdropdata":None,
    ".sounddata":"audio",
    ".starscapedata":None,
    ".str":"strings",
    ".texanim":"texanims"
}

EXT_TO_CLASS = {
    ".asteroidDef":uncol.AsteroidDef,
    ".brushes":ui.BrushFile,
    ".explosiondata":vis.Explosions,
    ".galaxy":scen.Galaxy,
    ".galaxyScenarioDef":scen.GalaxyScenarioDef,
    ".gameeventdata":events.GameEventDef,
    ".manifest":manifest.ManifestBase,
    ".particle":par.ParticleFile,
    ".playerThemes":themes.PlayerThemes,
    ".playerPictures":themes.PlayerPictures,
    ".playerThemesDef":themes.PlayerThemesDef,
    ".playerPicturesDef":themes.PlayerPicturesDef,
    ".randomeventdefs":events.RandomEventDef,
    ".renderingDef":uncol.CloudDef,
    ".skyboxbackdropdata":vis.SkyboxBackdrops,
    ".starscapedata":vis.Starscape,
    ".str":ui.StringFile,
    ".texanim":anims.TextureAnimation
}

DIR_TO_COMP = {
    "String":["strings"],
    "Window":["themes","brushes"],
    "Galaxy":["scenarios"],
    "GameInfo":["entities"],
    "Particle":["particles"],
    "Sound":["audio"],
    "TextureAnimations":["texanims"]
}

def fileToComponen(path,cMod):
    p,ext = os.path.splitext(path)
    if ext in EXT_TO_COMP:
        comp = EXT_TO_COMP[ext]
        if comp is None:
            return cMod
        else:
            return getattr(cMod,comp)
    else:
        raise RuntimeError("Unknown file type \'"+ext+"\'.")

def getFileClass(path):
    p,ext = os.path.splitext(path)
    if ext==".entity":
        entType = coe.Entity.parseType(path)
        if entType in ecomp.ModEntities.ENT_TO_CLASS:
            return ecomp.ModEntities.ENT_TO_CLASS[entType]
        else:
            raise NotImplementedError("Sorry, Entity type "+entType
                                      +" is not yet implemented.")
    elif ext in EXT_TO_CLASS:
        return EXT_TO_CLASS[ext]
    else:
        raise RuntimeError("Unknown file type \'"+ext+"\'.")

def getModClass(path,cMod):
    subdir = os.path.basename(path)
    if subdir in DIR_TO_COMP:
        return [getattr(cMod,comp) for comp in DIR_TO_COMP[subdir]]
    else:
        raise RuntimeError("Unknown subdir \'"+subdir+"\'.")

def printProblems(probs,footer=True,quiet=False):
    if not probs:
        return "No problems were found!\nCongrats!"
    probDict = defaultdict(list)
    generalProbs = []
    with tqdm.tqdm(disable=quiet,total=len(probs),
            desc="Sort problems by file") as pbar:
        for prob in probs:
            if prob.probFile is not None:
                probDict[os.path.basename(prob.probFile)].append(prob)
            else:
                generalProbs.append(prob)
            pbar.update()
    res = ""
    sKeys = sorted(probDict.keys(),
            key=lambda val:os.path.splitext(val)[0])
    sKeys = sorted(sKeys,
            key=lambda val:os.path.splitext(val)[1])
    with tqdm.tqdm(disable=quiet,total=len(probs),
            desc="Format problem output") as pbar:
        for key in sKeys:
            res += key+":\n"
            for prob in sorted(probDict[key],key=lambda val:val.severity,
                    reverse=True):
                res += prob.toString(1)
                pbar.update()
        if generalProbs:
            res += "General Problems:\n"
            for prob in sorted(generalProbs,key=lambda val:val.severity,
                    reverse=True):
                res += prob.toString(1)
                pbar.update()
    res += "\n\n"
    if footer:
        if not probDict:
            res += "Found "+str(len(probs))+" general "+(
                "problems" if len(probs)>1 else "problem")+"."
        else:
            res += "Found "+str(len(probs))+" "+("problems" if len(probs)>1 else
                    "problem")+" in "+str(len(probDict))+" "+(
                    "files" if len(probDict)>1 else "file")+"."
    return res

def filterProblems(probs,severity=co_probs.ProblemSeverity.warn,quiet=False):
    uniques = []
    with tqdm.tqdm(disable=quiet,total=len(probs),
            desc="Filtering problem duplicates") as pbar:
        for prob in probs:
            if prob not in uniques:
                uniques.append(prob)
            pbar.update()
    probs = [prob for prob in uniques if prob.severity>=severity]
    return probs

class Tool(abc.ABC):
    def __init__(self,files=None,moddir=None,rebrefdir=None,out=sys.stdout,
            quiet=False):
        self.files = [os.path.abspath(f) for f in files]
        self.out = out
        for f in self.files:
            if not os.path.exists(f):
                raise FileNotFoundError("The path \'"+f+"\' does not exist.")
        self.rebrefdir = os.path.abspath(rebrefdir) if rebrefdir else rebrefdir
        if moddir:
            self.moddir = os.path.abspath(moddir)
        elif files:
            for f in self.files:
                mdir = self._guessModdir(f)
                if mdir:
                    self.moddir = mdir
                    break
            else:
                raise RuntimeError("Could not guess moddir with given files.")
        else:
            raise RuntimeError("Could not guess moddir without \'mod\' "
                               "parameter or at least one input file to derive "
                               "mod directory.")
        self.mod = mod.Mod(moddir=self.moddir,rebrefdir=self.rebrefdir,
                quiet=quiet)

    @staticmethod
    def _guessModdir(f):
        if os.path.isdir(f):
            if os.path.basename(f) in MOD_SUBDIRS:
                return os.path.dirname(f)
            else:
                return None
        elif os.path.isfile(f):
            fdir = os.path.dirname(f)
            if (os.path.splitext(os.path.basename(f))[0]
            in manifest.MANIFEST_TYPES):
                return os.path.dirname(f)
            elif os.path.basename(fdir) in MOD_SUBDIRS:
                return os.path.dirname(fdir)
            else:
                return None
        else:
            return None

    @abc.abstractmethod
    def _computeResults(self):
        pass

    @classmethod
    def execute(cls,args):
        tool = cls(**args)
        retVal = tool._outputCLI()
        return retVal

    def _outputCLI(self):
        retVal,res = self._computeResults()
        self.out.write(res)
        self.out.write("\n")
        return retVal
