""" Tools to produce gephi-format graphs of entity-attribute relationships
"""
import itertools
from os.path import basename

from pysoase.entities.component import extract_attributes

def generate_attribute_graph(files,output,simple,distance):
    res = {}
    filecount = len(files)
    done = 1
    headbanner = "({0}/{1}) Starting {2}..."
    tailbanner = "({0}/{1}) Finished {2}..."
    for f_entity in files:
        print(headbanner.format(done,filecount,basename(f_entity)))
        etype,attributes = extract_attributes(f_entity,simple)
        res[etype] = res.get(etype,set()) | attributes
        print(tailbanner.format(done,filecount,basename(f_entity)))
        done += 1
    print("Writing graph to "+output)
    if not distance:
        with open(output,"w") as f:
            for key,value in res.items():
                f.write(key+";"+";".join([x.lower() for x in value]))
                f.write("\n")
    else:
        with open(output,"w") as f:
            # Write Matrix Header
            f.write(";")
            f.write(";".join(res.keys()))
            f.write("\n")
            for key,value in res.items():
                f.write(key+";")
                f.write(";".join(
                        [str(similarity(value,o[1])) for o in res.items()]))
                f.write("\n")
    print("FINISHED")

def attrib_counts(csv):
    attribs = {}
    with open(csv,"r") as f:
        for line in f:
            splitRes = line.strip("\n").split(sep=";")
            attribs[splitRes[0]] = set(splitRes[1:])
    print("Totals:")
    for key,val in sorted(attribs.items()):
        print("\t"+key+": "+str(len(val)))
    # total = len(all_attribs)
    all_attribs = set()
    all_attribs.update(*list(attribs.values()))
    print("Total: "+str(len(all_attribs)))

def attrib_show(csv,types,combine):
    attribs = {}
    with open(csv,"r") as f:
        for line in f:
            splitRes = line.strip("\n").split(sep=";")
            attribs[splitRes[0]] = set(splitRes[1:])
    attribs = {k:attribs[k] for k in types}
    if not combine:
        for key,val in sorted(attribs.items()):
            print(key+":\n\t"+"\n\t".join(sorted(val)))
    else:
        allhave = set(itertools.chain.from_iterable(list(attribs.values())))
        allhave.intersection_update(*list(attribs.values()))
        print("Remaining:")
        for key,value in sorted(attribs.items()):
            print(key+":\n\t"+"\n\t".join(sorted(value.difference(allhave))))
        print("In all Types:\n\t"+"\n\t".join(sorted(allhave)))

def similarity(a,b):
    return len(a & b)
