""" Tool classes for finding unreferenced files/entries
"""

import collections
import os

import tqdm

import pysoase.common.constants as co_consts
from pysoase.common.misc import indentText
import pysoase.tools.tools as tools

"""############################ Helper Functions ###########################"""

def printUnrefed(filesByType,refs,probs,quiet=False):
    res = "The following files are currently unreferenced:\n"
    hadNoUnrefed = True
    with tqdm.tqdm(disable=quiet,total=len(filesByType),
            desc="Checking Referenced Status") as pbar:
        for refType,files in sorted(filesByType.items()):
            lowerToStandard = {f.lower():f for f in files}
            unrefed = set(lowerToStandard.keys()).difference({r.lower()
                for r in refs[refType]})
            if unrefed:
                hadNoUnrefed = False
                res += "\t"+refType+"\n"
            for f in sorted(unrefed):
                res += "\t\t"+lowerToStandard[f]+"\n"
            pbar.update()
    if hadNoUnrefed:
        res += "\tThere were no unreferenced files.\n"
    if probs:
        res += indentText(
                "\n\nATTENTION: The following problems occured during "
                "reference gathering. The above list of files may contain "
                "errors! Please correct problems and rerun unref check.\n",
                0)
        for prob in probs:
            res += prob.toString(1)
    return 0 if hadNoUnrefed else 1,res

"""############################## Tool Classes #############################"""

class UnrefFiles(tools.Tool):
    def _computeResults(self):
        checkTypes = set()
        filesByType = collections.defaultdict(set)
        for f in self.files:
            name,ext = os.path.splitext(f)
            if ext in co_consts.EXT_TO_FILEREF:
                refType = co_consts.EXT_TO_FILEREF[ext]
                checkTypes.add(refType)
                filesByType[refType].add(os.path.basename(f))
            else:
                raise RuntimeError("Sorry, files of type "
                                   +ext+" are not valid.")
        refs,probs = self.mod.gatherRefs(types=checkTypes)
        return printUnrefed(filesByType,refs,probs,quiet=self.mod.quiet)

class UnrefTypes(tools.Tool):
    def __init__(self,directory,types,**kwargs):
        kwargs["moddir"] = directory
        super().__init__(files=[],**kwargs)
        self.types = set(types)
        typediff = self.types.difference(set(
                co_consts.FILEREF_TO_EXT))
        if typediff:
            raise RuntimeError("The following types are invalid: "
                               +str(typediff))

    def _computeResults(self):
        exts = set()
        for t in self.types:
            exts.add(co_consts.FILEREF_TO_EXT[t])
        files = self.mod.gatherFiles(exts)
        refs,probs = self.mod.gatherRefs(self.types)
        return printUnrefed(files,refs,probs,quiet=self.mod.quiet)
