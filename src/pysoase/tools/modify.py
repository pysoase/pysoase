""" Tool classes for modifying values
"""

import abc
import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.tools.tools as tools

"""############################# Modifications #############################"""

g_float = pp.Or([pp.Word(initChars=".",bodyChars=pp.nums),
    pp.Combine(pp.Word(pp.nums)+pp.Literal(".")+pp.Word(pp.nums))])
g_floatType = g_float+pp.Empty().setParseAction(lambda toks:"scalar")("type")
g_int = pp.Word(pp.nums)
g_intType = g_int+pp.Empty().setParseAction(lambda toks:"scalar")("type")
g_string = (pp.Literal("\'").suppress()
            +pp.Word(pp.alphanums+"_")("value")
            +pp.Literal("\'").suppress()
)
g_bool = pp.Or([co_parse.g_true,co_parse.g_false])("value")
g_scalarOp = pp.Or([pp.Literal("+"),pp.Literal("-"),pp.Literal("x"),
    pp.Literal("=")])
g_stringOp = pp.Literal("=")
g_boolOp = pp.Literal("=")
g_scalarChange = pp.Group(pp.Or([g_floatType,g_intType])("value")
)("change")
g_stringChange = pp.Group(g_string
                          +pp.Empty().setParseAction(lambda toks:"string"
                          )("type")
)("change")
g_boolChange = pp.Group(g_bool
                        +pp.Empty().setParseAction(lambda toks:"bool")("type")
)("change")
g_scalarMod = (pp.Word(pp.alphanums)("identifier")
               +pp.Literal(":").suppress()
               +g_scalarOp("operator")
               +g_scalarChange)
g_stringMod = (pp.Word(pp.alphanums)("identifier")
               +pp.Literal(":").suppress()
               +g_stringOp("operator")
               +g_stringChange)
g_boolMod = (pp.Word(pp.alphanums)("identifier")
             +pp.Literal(":").suppress()
             +g_boolOp("operator")
             +g_boolChange)
g_vecBody = pp.Optional(pp.Combine(g_scalarOp+pp.Or([g_int,g_float])),
        default="")
g_vector = (pp.Literal("[").suppress()
            +pp.delimitedList(g_vecBody)("value")
            +pp.Literal("]").suppress())+pp.Empty().setParseAction(
        lambda toks:"vector")("type")
g_vectorChange = pp.Group(g_vector)("change")
g_vectorMod = (pp.Word(pp.alphanums)("identifier")
               +pp.Literal(":").suppress()
               +g_vectorChange)
g_mod = pp.Group(pp.Or([g_scalarMod,g_stringMod,g_vectorMod,g_boolMod]))

class ModAttrib(abc.ABC):
    def __init__(self,identifier):
        self.identifier = identifier

    def __eq__(self,other):
        return self.identifier==other.identifier

    @abc.abstractmethod
    def apply(self,attrib):
        raise NotImplemented()

    def fits(self,attrib):
        return self.identifier==attrib.identifier

    @classmethod
    def factory(cls,modString):
        parseRes = g_mod.parseString(modString)[0]
        t = parseRes["change"]["type"]
        if t=="scalar":
            return ModScalarOp(**parseRes)
        elif t=="string":
            return ModStringOp(**parseRes)
        elif t=="bool":
            return ModBoolOp(**parseRes)
        elif t=="vector":
            return ModVectorOp(**parseRes)

class ModScalarOp(ModAttrib):
    def __init__(self,identifier,change,operator):
        super().__init__(identifier)
        self.op = operator
        v = change["value"]
        self.val = int(float(v)) if float(v).is_integer() else float(v)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.op==other.op)
                        and (self.val==other.val))

    def fits(self,attrib):
        res = super().fits(attrib)
        return res and isinstance(attrib,co_attribs.AttribNum)

    def apply(self,attrib):
        oldVal = attrib.value
        if self.op=="+":
            attrib.value += self.val
        elif self.op=="-":
            attrib.value -= self.val
        elif self.op=="x":
            attrib.value *= self.val
        elif self.op=="=":
            attrib.value = self.val
        return ModificationChange(attrib.identifier,oldVal,attrib.value)

class ModStringOp(ModAttrib):
    def __init__(self,identifier,change,operator):
        super().__init__(identifier)
        self.op = operator
        self.val = change["value"]

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.op==other.op)
                        and (self.val==other.val))

    def fits(self,attrib):
        res = super().fits(attrib)
        return res and (isinstance(attrib,co_attribs.AttribRef)
                        or isinstance(attrib,
                co_attribs.AttribString))

    def apply(self,attrib):
        oldVal = attrib.value
        if self.op=="=":
            attrib.value = self.val
            return ModificationChange(attrib.identifier,oldVal,attrib.value)

class ModBoolOp(ModAttrib):
    def __init__(self,identifier,change,operator):
        super().__init__(identifier)
        self.op = operator
        self.val = change["value"]
        if self.val=="TRUE":
            self.val = True
        else:
            self.val = False

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.op==other.op)
                        and (self.val==other.val))

    def fits(self,attrib):
        res = super().fits(attrib)
        return res and isinstance(attrib,co_attribs.AttribBool)

    def apply(self,attrib):
        oldVal = attrib.value
        if self.op=="=":
            attrib.value = self.val
            return ModificationChange(attrib.identifier,oldVal,attrib.value)

class ModVectorOp(ModAttrib):
    _modparser = g_scalarOp("op")+pp.Or([g_int,g_float])("val")

    def __init__(self,identifier,change):
        super().__init__(identifier)
        vals = change["value"]
        self.length = len(vals)
        self.val = []
        for v in vals:
            if v=="":
                self.val.append(None)
            else:
                p = self._modparser.parseString(v)
                p = (p.op,float(p.val))
                if p[1].is_integer():
                    p = (p[0],int(p[1]))
                self.val.append(p)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.val==other.val)

    def fits(self,attrib):
        res = super().fits(attrib)
        return res and self.length==len(attrib.value)

    def apply(self,attrib):
        newVal = ()
        for v,a in zip(self.val,attrib.value):
            if v:
                op,value = v
                if op=="+":
                    newVal = newVal+(a+value,)
                elif op=="-":
                    newVal = newVal+(a-value,)
                elif op=="x":
                    newVal = newVal+(a*value,)
                elif op=="=":
                    newVal = newVal+(value,)
            else:
                newVal = newVal+(a,)
        c = ModificationChange(attrib.identifier,attrib.value,newVal)
        attrib.value = newVal
        return c

class ModificationChange:
    _printString = "{}:\n\tChanged \'{}\' from \'{}\' to \'{}\'\n"

    def __init__(self,identifier,oldVal,newVal,modFile=None):
        self.identifier = identifier
        self.oldVal = oldVal
        self.newVal = newVal
        self.modFile = modFile

    def __eq__(self,other):
        return ((self.identifier==other.identifier)
                and (self.oldVal==other.oldVal)
                and (self.newVal==other.newVal)
                and (self.modFile==other.modFile))

    def toString(self,ident):
        res = self._printString.format(self.modFile,self.identifier,
                self.oldVal,self.newVal)
        return indentText(res,ident)

"""################################## Tool #################################"""

class Modify(tools.Tool):
    def __init__(self,modStr,files,**kwargs):
        self.modifications = [ModAttrib.factory(m) for m in modStr.split(";")]
        dirsInInput = [f for f in files if os.path.isdir(f)]
        if dirsInInput:
            raise ValueError("The modify command does not support directories "
                             +"as input:\n\t"
                             +"\n\t".join(dirsInInput))
        super().__init__(files=files,**kwargs)

    def _computeResults(self):
        res = ""
        probs = []
        changes = []
        noChangeFiles = []
        retVal = 0
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.files),
                desc="Applying Modifications") as pbar:
            for f in self.files:
                fileClass = tools.getFileClass(f)
                inst = fileClass.createInstance(f)
                if isinstance(inst,list):
                    probs.extend(inst)
                else:
                    c = inst.modify(self.modifications)
                    if c:
                        changes.extend(c)
                        inst.writeOut()
                    else:
                        noChangeFiles.append(os.path.basename(f))
                pbar.update()
        if changes:
            res += "The following changes were made:\n"
            for c in changes:
                res += c.toString(1)
            for f in noChangeFiles:
                res += "\t"+os.path.basename(f)+"\n"+"\t\tNo changes made\n"
        else:
            res += ("All files were scanned, but no changes were made.\n"
                    +"Please check your modification string for typos in the "
                    +"identifier names.\n")
            retVal = 1
        if probs:
            res += ("The following problems occured, thus no change was made "
                    "to these files:\n")
            for p in probs:
                res += p.toString(1)
            retVal = 1
        return retVal,res
