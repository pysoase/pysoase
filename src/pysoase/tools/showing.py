""" Contains classes for diplaying information from Sins mod files
"""

import collections
import os

import pyparsing as pp
import tqdm

import pysoase.common.filehandling as co_files
from pysoase.common.misc import indentText
import pysoase.mod.ui as ui
import pysoase.tools.tools as tools

"""############################ Helper Functions ###########################"""

def getRefsFromFile(f,refmap):
    probs = []
    fclass = tools.getFileClass(f)
    inst = fclass.createInstance(f)
    if isinstance(inst,list):
        probs.extend(inst)
    else:
        for ref in inst.getRefs():
            refmap[ref._REFTYPE].append(ref)
    return probs

def string_file_diff(basefile,modfile,quiet=False):
    added = []
    modified = []
    with tqdm.tqdm(disable=quiet,total=len(modfile.entries.elements),
            desc="Comparing String Files") as pbar:
        for info in modfile:
            if info.stringid not in basefile:
                added.append(info)
            elif info.value != basefile[info.stringid].value:
                modified.append(info)
            pbar.update()
    return modified,added

"""################################## Tools ################################"""

class ShowRefs(tools.Tool):
    def __init__(self,combined,**kwargs):
        super().__init__(**kwargs)
        self.combined = combined

    def _printRefPerFile(self):
        refmap = {}
        probs = {}
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.files),
                desc="Gathering References") as pbar:
            for f in self.files:
                filerefs = collections.defaultdict(list)
                fileprobs = getRefsFromFile(f,filerefs)
                if fileprobs:
                    probs[os.path.basename(f)] = fileprobs
                else:
                    refmap[os.path.basename(f)] = filerefs
                pbar.update()
        res = ""
        with tqdm.tqdm(disable=self.mod.quiet,total=len(refmap),
                desc="Preparing output") as pbar:
            for filename,refs in sorted(refmap.items()):
                res += filename+"\n"
                for ident,reflist in sorted(refs.items()):
                    res += "\t"+ident+"\n"
                    for ref in reflist:
                        if ref.ref:
                            res += ref.toString(2)
                            if isinstance(ref,ui.BrushRef):
                                res = res[:-1]
                                res += " ({})\n".format(
                                        ref.getBrushfile(self.mod))
                pbar.update()
        for filename,probs in probs.items():
            res += filename+"\n"
            for prob in probs:
                res += prob.toString(1)
        return int(bool(probs)),res

    def _printRefsCombined(self):
        refmap = collections.defaultdict(list)
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.files),
                desc="Gathering References") as pbar:
            for f in self.files:
                probs.extend(getRefsFromFile(f,refmap))
                pbar.update()
        res = ""
        with tqdm.tqdm(disable=self.mod.quiet,total=len(refmap),
                desc="Preparing output") as pbar:
            for reftype,reflist in sorted(refmap.items()):
                res += reftype+"\n"
                for ref in reflist:
                    if ref.ref:
                        res += ref.toString(1)
                        if isinstance(ref,ui.BrushRef):
                            res = res[:-1]
                            res += " ({})\n".format(ref.getBrushfile(self.mod))
                pbar.update()
        for prob in probs:
            res += prob.probFile+"\n"
            res += prob.toString(1)
        return int(bool(probs)),res

    def _computeResults(self):
        if self.combined:
            return self._printRefsCombined()
        else:
            return self._printRefPerFile()

class ShowBuffchain(tools.Tool):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def _computeResults(self):
        res = ""
        invalid = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.files),
                desc="Gathering Buffchains") as pbar:
            for f in self.files:
                fclass = tools.getFileClass(f)
                inst = fclass.createInstance(f)
                if hasattr(inst,"outputBuffchain"):
                    res += inst.outputBuffchain(self.mod,0)
                else:
                    invalid.append(f)
                pbar.update()
        if invalid:
            res += "The following files are not of a type which has Buffs:\n"
            for f in invalid:
                res += "\t"+os.path.basename(f)+"\n"
        return 1 if invalid else 0,res

class ShowStringdiff(tools.Tool):

    g_difffile = pp.OneOrMore(ui.g_stringInfo)("infos")

    def __init__(self,language="English",update=False,outdir=None,**kwargs):
        super().__init__(files=[],**kwargs)
        self.update = update
        self.outdir = self.moddir if not outdir else outdir
        self.language = language

    def _loadVanillaStringfile(self):
        retVal = 1
        res = None
        if os.path.exists(os.path.join(co_files.BUNDLE_PATH,"String",
                self.language+".str")):
            retVal = 0
            res = ui.StringFile.createInstance(
                    os.path.join(co_files.BUNDLE_PATH,"String",
                            self.language+".str"))
        elif self.mod.rebrefdir:
            if os.path.exists(
                    os.path.join(self.mod.rebrefdir,"String",
                            self.language+".str")):
                retVal = 0
                res = ui.StringFile.createInstance(
                        os.path.join(self.mod.rebrefdir,"String",
                                self.language+".str"))
            else:
                res = (
                    self.language+".str String file is not part of the vanilla "
                    +"release, thus\nthere is nothing to compare it to.\n"
                )
        else:
            # Vanilla file for this language cannot be found
            res = (
                self.language+".str is not bundled with PySoaSE.\nPlease "
                +"provide your Sins Rebellion directory via the --rebref option."
                +"\n"
            )
        return retVal,res

    def _loadModStringfile(self):
        retVal = 1
        res = None
        if not self.mod.hasSubdir("String"):
            res = (
                "Could not find "
                +"\""
                +os.path.relpath(os.path.join(self.moddir,"String"))
                +"\".\n"
                +"Perhaps you missspelled your mod directory?\n"
            )
        else:
            langSrc,_ = self.mod.checkFileExistence("String",self.language+".str")
            if langSrc==co_files.FileSource.mod:
                inst = self.mod.strings.getLanguageFile(self.language)
                if isinstance(inst,list):
                    res = ("The following syntax error occured when parsing"
                        +" the String file:\n")
                    res += indentText(tools.printProblems(inst,False,True),1)
                    res += "Aborting."
                else:
                    res = inst
                    retVal = 0
            else:
                res = (
                    "Language File "+self.language
                    +".str not found. The Mod does not contain any changes to "
                    +self.language+" strings.\n"
                )
        return retVal,res

    def _loadDiffFiles(self):
        # Check existence of output dir
        if not os.path.exists(self.outdir):
            return 1,(
                "Sorry, your given output directory \""+self.outdir+"\"\n"
                +"does not exist.\nAborting."
            )
        # Check existence of diff files in output dir
        changepath = os.path.join(self.outdir,"stringdiff_"
                                              +"changed_"+self.language+".txt")
        addpath = os.path.join(self.outdir,"stringdiff_"
                                              +"added_"+self.language+".txt")
        if not os.path.exists(changepath) or not os.path.exists(addpath):
            return 1,(
                "No Diff files found in \'"+os.path.relpath(self.outdir)+"\'.\n"
                +"Run PySoaSE without the \'--update\' option to generate them."
                +"\n"
            )
        try:
            changefile = self.g_difffile.parseFile(changepath)
            changes = [ui.StringInfo(**v) for v in changefile]
            addfile = self.g_difffile.parseFile(addpath)
            additions = [ui.StringInfo(**v) for v in addfile]
            return 0,changes,additions
        except pp.ParseException as e:
            return 1,(
                "Sorry, the diff files in \'"+os.path.relpath(self.outdir)
                +"\'\nare malformed. Please remove the \'--update\' option "
                +"to regenerate them.\n"
            )

    def _updatedStrings(self,oldAdded,newAdded,oldChanged,newChanged):
        newlyAdded = [v for v in newAdded if v not in oldAdded]
        newlyChanged = [v for v in newChanged if v not in oldChanged]
        return newlyChanged,newlyAdded

    def _computeResults(self):
        res = ""
        retVal = 0
        changes = None
        additions = None
        if self.update:
            diffFiles = self._loadDiffFiles()
            if diffFiles[0]:
                return diffFiles[0],diffFiles[1]
            else:
                changes,additions = diffFiles[1:3]
        returnVanilla,resVanilla = self._loadVanillaStringfile()
        returnMod = None
        resMod = None
        if not returnVanilla:
            returnMod,resMod = self._loadModStringfile()
        else:
            return returnVanilla,resVanilla
        if not returnMod:
            # At this point everything loaded correctly, run the comparison
            changepath = os.path.relpath(os.path.join(self.outdir,"stringdiff_"
                                           +"changed_"+self.language+".txt"))
            addpath = os.path.relpath(os.path.join(self.outdir,"stringdiff_"
                                        +"added_"+self.language+".txt"))
            newChanges,newAdditions = string_file_diff(resVanilla,resMod,
                    self.mod.quiet)
            res = "Found {0} changed and {1} added Strings in {2}.\n".format(
                    len(newChanges),
                    len(newAdditions),
                    os.path.relpath(resMod.filepath)
            )
            if newAdditions or newChanges:
                res += "Output written to "
            if newAdditions:
                res += addpath
                addStr = "".join([s.toString(0) for s in newAdditions])
                with open(addpath,'w') as f:
                    f.write(addStr)
            if newAdditions and newChanges:
                res += "\nand "
            elif newAdditions:
                res += ".\n"
            if newChanges:
                res += changepath+".\n"
                chStr = "".join([s.toString(0) for s in newChanges])
                with open(changepath,'w') as f:
                    f.write(chStr)
            if self.update:
                res += "\n"
                newlyChanged,newlyAdded = self._updatedStrings(additions,
                        newAdditions,changes,newChanges)
                if newlyAdded:
                    res += "The following newly added Strings were found:\n"
                    res += "".join(v.toString(1) for v in newlyAdded)
                else:
                    res += "There were no added Strings compared to the already " \
                           "present Diff file.\n"
                if newlyAdded and newlyChanged:
                    res += "\n"
                if newlyChanged:
                    res += "The following newly modified Strings were found:\n"
                    res += "".join(v.toString(1) for v in newlyChanged)
                else:
                    res += "There were no changed Strings compared to the " \
                           "already present Diff file.\n"
        else:
            return returnMod,resMod
        return retVal,res
