""" Parser and classes for a mod's manifest file.
"""

import abc
import os

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.meshes as meshes

"""################################# Types #################################"""

MANIFEST_TYPES = ["brush","entity","galaxy","playerPictures",
    "playerThemes","skybox"]

"""################################ Parsers ################################"""

g_ManifestBrush = (
    co_parse.g_txt
    +co_parse.genListAttrib("brushFileCount",
            co_parse.genStringAttrib("brushFile"))(
            "entries")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)
g_ManifestEntity = (
    co_parse.g_txt
    +co_parse.genListAttrib("entityNameCount",
            co_parse.genStringAttrib("entityName"))(
            "entries")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)
g_ManifestFiles = (
    co_parse.g_txt
    +co_parse.genListAttrib("fileCount",
            co_parse.genStringAttrib("fileName"))(
            "entries")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)
g_ManifestSkyboxes = (
    co_parse.g_txt
    +co_parse.genListAttrib("meshFileCount",
            co_parse.genStringAttrib("meshFileName"))(
            "entries")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################## File Classes #############################"""

class MissingManifestEntry(co_probs.Problem):
    _errorMsg = ("{} is not a valid manifest type. Valid values are "
                 +str(MANIFEST_TYPES))

    def __init__(self,ref,manifestType,**kwargs):
        if manifestType not in MANIFEST_TYPES:
            raise ValueError(self._errorMsg.format(manifestType))
        super().__init__(co_probs.ProblemSeverity.warn,**kwargs)
        self._manifestType = manifestType
        self.reference = ref

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self._manifestType==other._manifestType)
                        and (self.reference==other.reference))

    @property
    def manifestType(self):
        return self._manifestType

    @manifestType.setter
    def manifestType(self,newtype):
        if newtype not in MANIFEST_TYPES:
            raise ValueError(self._errorMsg.format(newtype))
        self._manifestType = newtype

    def toString(self,indention):
        res = super().toString(indention)
        res += "The reference \'{}\' has no entry in the {} manifest.\n".format(
                self.reference,self._manifestType)
        return indentText(res,indention)

class ManifestBase(co_basics.IsFile,
        co_basics.HasReference):
    def __init__(self,filepath,elemType,entries,filedir,elemArgs,**kwargs):
        super().__init__(filepath,**kwargs)
        self.entries = co_attribs.AttribListKeyed(elemType=elemType,elemArgs=elemArgs,
                keyfunc=lambda val:val.ref,**entries)
        self.filedir = filedir

    @classmethod
    def createInstance(cls,filepath,manType=None):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        if manType is None:
            baseFile = os.path.basename(filepath)
            if baseFile=="brush.manifest":
                return ManifestBrush.createInstance(filepath)
            elif baseFile=="entity.manifest":
                return ManifestEntity.createInstance(filepath)
            elif baseFile=="galaxy.manifest":
                return ManifestGalaxy.createInstance(filepath)
            elif baseFile=="playerPictures.manifest":
                return ManifestPlayerPicture.createInstance(filepath)
            elif baseFile=="playerThemes.manifest":
                return ManifestPlayerTheme.createInstance(filepath)
            elif baseFile=="skybox.manifest":
                return ManifestSkybox.createInstance(filepath)
            else:
                raise RuntimeError("Could not guess what type of manifest "
                                   "\'"+filepath+"\' is. "
                                   "Please provide manifest type.")
        else:
            if manType=="brush":
                return ManifestBrush.createInstance(filepath)
            elif manType=="entity":
                return ManifestEntity.createInstance(filepath)
            elif manType=="galaxy":
                return ManifestGalaxy.createInstance(filepath)
            elif manType=="playerPictures":
                return ManifestPlayerPicture.createInstance(filepath)
            elif manType=="playerThemes":
                return ManifestPlayerTheme.createInstance(filepath)
            elif manType=="skybox":
                return ManifestSkybox.createInstance(filepath)
            else:
                raise RuntimeError("Unknown manifest type \'"+manType+"\'"
                                   " given for \'"+filepath+"\'.")

    @abc.abstractmethod
    def checkEntry(self,entry,manType):
        if entry not in self.entries:
            return [MissingManifestEntry(entry,manType)]
        else:
            return []

    @classmethod
    def parse(cls,filepath,manType=None):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        if manType is None:
            baseFile = os.path.basename(filepath)
            if baseFile=="brush.manifest":
                return ManifestBrush.parse(filepath)
            elif baseFile=="entity.manifest":
                return ManifestEntity.parse(filepath)
            elif baseFile=="galaxy.manifest":
                return ManifestGalaxy.parse(filepath)
            elif baseFile=="playerPictures.manifest":
                return ManifestPlayerPicture.parse(filepath)
            elif baseFile=="playerThemes.manifest":
                return ManifestPlayerTheme.parse(filepath)
            elif baseFile=="skybox.manifest":
                return ManifestSkybox.parse(filepath)
            else:
                raise RuntimeError("Could not guess what type of manifest "
                                   "\'"+filepath+"\' is. Please provide "
                                   "manifest type.")
        else:
            if manType=="brush":
                return ManifestBrush.parse(filepath)
            elif manType=="entity":
                return ManifestEntity.parse(filepath)
            elif manType=="galaxy":
                return ManifestGalaxy.parse(filepath)
            elif manType=="playerPictures":
                return ManifestPlayerPicture.parse(filepath)
            elif manType=="playerThemes":
                return ManifestPlayerTheme.parse(filepath)
            elif manType=="skybox":
                return ManifestSkybox.parse(filepath)
            else:
                raise RuntimeError("Unknown manifest type \'"+manType+"\'"
                                   " given for \'"+filepath+"\'.")

    def check(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        res.extend(self.entries.checkDuplicates())
        for elem in self.entries.elements:
            res.extend(elem.checkExistence(currMod))
        if res is None:
            res = []
        for prob in res:
            prob.probFile = self.filepath
        return res

    def toString(self,indention):
        res = super().toString(indention)
        res += self.entries.toString(indention)
        return res

    def removeDuplicates(self):
        return self.entries.removeDuplicates()

class ManifestEntity(ManifestBase):
    parser = g_ManifestEntity

    def __init__(self,filepath,entries):
        super().__init__(filepath,coe.EntityRef,entries,"GameInfo",
                {"types":coe.POSSIBLE_ENTITY_TYPES})

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        success,parseRes = cls.parse(filepath)
        if success:
            return ManifestEntity(filepath,**parseRes)
        else:
            return parseRes

    @classmethod
    def parse(cls,filepath):
        return super(ManifestBase,cls).parse(filepath)

    def checkEntry(self,entry):
        return super().checkEntry(entry,"entity")

class ManifestBrush(ManifestBase):
    parser = g_ManifestBrush

    def __init__(self,filepath,entries):
        super().__init__(filepath,co_attribs.FileRef,entries,"Window",
                {"subdir":"Window","extension":"brushes"})

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        success,parseRes = cls.parse(filepath)
        if success:
            return ManifestBrush(filepath,**parseRes)
        else:
            return parseRes

    @classmethod
    def parse(cls,filepath):
        return super(ManifestBase,cls).parse(filepath)

    def checkEntry(self,entry):
        return super().checkEntry(entry,"brush")

class ManifestGalaxy(ManifestBase):
    parser = g_ManifestFiles

    def __init__(self,filepath,entries):
        super().__init__(filepath,co_attribs.FileRef,entries,"Galaxy",
                {"subdir":"Galaxy","extension":"galaxy"})

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        success,parseRes = cls.parse(filepath)
        if success:
            return ManifestGalaxy(filepath,**parseRes)
        else:
            return parseRes

    @classmethod
    def parse(cls,filepath):
        return super(ManifestBase,cls).parse(filepath)

    def checkEntry(self,entry):
        return super().checkEntry(entry,"galaxy")

class ManifestPlayerTheme(ManifestBase):
    parser = g_ManifestFiles

    def __init__(self,filepath,entries):
        super().__init__(filepath,co_attribs.FileRef,entries,"Window",
                {"subdir":"Window","extension":"playerThemes"})

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        success,parseRes = cls.parse(filepath)
        if success:
            return ManifestPlayerTheme(filepath,**parseRes)
        else:
            return parseRes

    @classmethod
    def parse(cls,filepath):
        return super(ManifestBase,cls).parse(filepath)

    def checkEntry(self,entry):
        return super().checkEntry(entry,"playerThemes")

class ManifestPlayerPicture(ManifestBase):
    parser = g_ManifestFiles

    def __init__(self,filepath,entries):
        super().__init__(filepath,co_attribs.FileRef,entries,"Window",
                {"subdir":"Window","extension":"playerPictures"})

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        success,parseRes = cls.parse(filepath)
        if success:
            return ManifestPlayerPicture(filepath,**parseRes)
        else:
            return parseRes

    @classmethod
    def parse(cls,filepath):
        return super(ManifestBase,cls).parse(filepath)

    def checkEntry(self,entry):
        return super().checkEntry(entry,"playerPictures")

class ManifestSkybox(ManifestBase):
    parser = g_ManifestSkyboxes

    def __init__(self,filepath,entries):
        super().__init__(filepath,meshes.MeshRef,entries,"Mesh",None)

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        success,parseRes = cls.parse(filepath)
        if success:
            return ManifestSkybox(filepath,**parseRes)
        else:
            return parseRes

    @classmethod
    def parse(cls,filepath):
        return super(ManifestBase,cls).parse(filepath)

    def checkEntry(self,entry):
        return super().checkEntry(entry,"skybox")
