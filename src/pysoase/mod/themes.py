""" Contains parsers and classes for Sins player themes/pictures files
"""

import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.mod.ui as ui

"""################################ Parsers ################################"""

def genPicEntry(ident):
    return pp.Group(
            co_parse.genHeading(ident)
            +g_pictureEntryContent
    )

"""### Attributes ###"""

g_pictureEntryContent = (
    co_parse.genStringAttrib("textureName")("texture")
    +pp.Group(
            co_parse.g_linestart
            +pp.Keyword("startTopLeft")("identifier")
            +co_parse.g_space
            +co_parse.g_pos2
            +co_parse.g_eol
    )("topLeft")
    +co_parse.genIntAttrib("width")("width")
    +co_parse.genIntAttrib("height")("height")
    +co_parse.genIntAttrib("rowItemCount")("rowItemCount")
    +co_parse.genIntAttrib("horizontalStride")("horizStride")
    +co_parse.genIntAttrib("verticalStride")("vertStride")
)

"""### PlayerPictures ###"""

g_PlayerPictures = (
    co_parse.g_txt
    +co_parse.genIntAttrib("pictureCount")("picCount")
    +genPicEntry("LargeScreenPicture")("picLarge")
    +genPicEntry("SmallScreenPicture")("picSmall")
    +genPicEntry("BottomBarPicture")("picBottomBar")
    +genPicEntry("ButtonIcon")("button")
    +genPicEntry("MediumScreenPicture")("picMedium")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_PlayerPicturesDef = (
    co_parse.g_txt
    +co_parse.genStringAttrib("defaultPlayerPictureGroupName")(
            "playerPicDefault")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### PlayerThemes ###"""

g_PlayerThemes = (
    co_parse.g_txt
    +co_parse.genIntAttrib("themeCount")("themeCount")
    +genPicEntry("Icon")("icon")
    +genPicEntry("IconOverlay")("iconOverlay")
    +genPicEntry("Picture")("picture")
    +genPicEntry("PlanetIcon")("iconPlanet")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_PlayerThemesDef = (
    co_parse.g_txt
    +co_parse.genStringAttrib("defaultPlayerThemeGroupName")("playerThemeDef")
    +co_parse.genStringAttrib("specialPlayerThemeGroupName")(
            "playerThemeSpecial")
    +co_parse.genIntAttrib("unownedThemeIndex")("unownedIndex")
    +co_parse.genIntAttrib("unknownThemeIndex")("unknownIndex")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""################################ Classes ################################"""

"""### Attributes ###"""

class PictureEntry(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,texture,topLeft,width,height,rowItemCount,horizStride,
            vertStride,**kwargs):
        super().__init__(**kwargs)
        self.texture = ui.UITextureRef(**texture)
        self.topLeft = co_attribs.AttribPos2d(**topLeft)
        self.width = co_attribs.AttribNum(**width)
        self.height = co_attribs.AttribNum(**height)
        self.rowItemCount = co_attribs.AttribNum(**rowItemCount)
        self.horizStride = co_attribs.AttribNum(**horizStride)
        self.vertStride = co_attribs.AttribNum(**vertStride)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.texture==other.texture)
                        and (self.topLeft==other.topLeft)
                        and (self.width==other.width)
                        and (self.height==other.height)
                        and (self.rowItemCount==other.rowItemCount)
                        and (self.horizStride==other.horizStride)
                        and (self.vertStride==other.vertStride))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.texture.toString(1)
        res += self.topLeft.toString(1)
        res += self.width.toString(1)
        res += self.height.toString(1)
        res += self.rowItemCount.toString(1)
        res += self.horizStride.toString(1)
        res += self.vertStride.toString(1)
        return indentText(res,indention)

"""### PlayerPictures ###"""

class PlayerPictureRef(co_attribs.FileRef):
    _REFTYPE = "Player Picture"

    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier,val,"Window","playerPictures",**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(currMod.themes.checkManifestPictures(self.ref))
        for prob in res:
            if not prob.probLine:
                prob.probLine = self.linenum
        return res

class PlayerPicturesDef(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_PlayerPicturesDef

    def __init__(self,filepath,playerPicDefault,**kwargs):
        super().__init__(filepath,**kwargs)
        self.playerPicDefault = PlayerPictureRef(**playerPicDefault)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.playerPicDefault==other.playerPicDefault)

    def toString(self,indention):
        res = super().toString(0)
        res += self.playerPicDefault.toString(0)
        return indentText(res,indention)

class PlayerPictures(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_PlayerPictures

    def __init__(self,filepath,picCount,picLarge,picSmall,
            picBottomBar,button,picMedium,**kwargs):
        super().__init__(filepath,**kwargs)
        self.picCount = co_attribs.AttribNum(**picCount)
        self.picLarge = PictureEntry(**picLarge)
        self.picSmall = PictureEntry(**picSmall)
        self.picBottomBar = PictureEntry(**picBottomBar)
        self.button = PictureEntry(**button)
        self.picMedium = PictureEntry(**picMedium)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.picCount==other.picCount)
                        and (self.picLarge==other.picLarge)
                        and (self.picSmall==other.picSmall)
                        and (self.picBottomBar==other.picBottomBar)
                        and (self.button==other.button)
                        and (self.picMedium==other.picMedium))

    def toString(self,indention):
        res = super().toString(0)
        res += self.picCount.toString(0)
        res += self.picLarge.toString(0)
        res += self.picSmall.toString(0)
        res += self.picBottomBar.toString(0)
        res += self.button.toString(0)
        res += self.picMedium.toString(0)
        return indentText(res,indention)

"""### PlayerThemes ###"""

class PlayerThemeRef(co_attribs.FileRef):
    _REFTYPE = "Player Theme"

    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier,val,"Window","playerThemes",**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(currMod.themes.checkManifestThemes(self.ref))
        for prob in res:
            if not prob.probLine:
                prob.probLine = self.linenum
        return res

class PlayerThemesDef(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_PlayerThemesDef

    def __init__(self,filepath,playerThemeDef,playerThemeSpecial,unownedIndex,
            unknownIndex):
        super().__init__(filepath)
        self.playerThemeDef = PlayerThemeRef(**playerThemeDef)
        self.playerThemeSpecial = PlayerThemeRef(**playerThemeSpecial)
        self.unownedIndex = co_attribs.AttribNum(**unownedIndex)
        self.unknownIndex = co_attribs.AttribNum(**unknownIndex)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.playerThemeDef==other.playerThemeDef)
                        and (self.playerThemeSpecial==other.playerThemeSpecial)
                        and (self.unownedIndex==other.unownedIndex)
                        and (self.unknownIndex==other.unknownIndex))

    def toString(self,indention):
        res = super().toString(0)
        res += self.playerThemeDef.toString(0)
        res += self.playerThemeSpecial.toString(0)
        res += self.unownedIndex.toString(0)
        res += self.unknownIndex.toString(0)
        return indentText(res,indention)

class PlayerThemes(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_PlayerThemes

    def __init__(self,filepath,themeCount,icon,iconOverlay,picture,iconPlanet,
            **kwargs):
        super().__init__(filepath,**kwargs)
        self.themeCount = co_attribs.AttribNum(**themeCount)
        self.icon = PictureEntry(**icon)
        self.iconOverlay = PictureEntry(**iconOverlay)
        self.picture = PictureEntry(**picture)
        self.iconPlanet = PictureEntry(**iconPlanet)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.themeCount==other.themeCount)
                        and (self.icon==other.icon)
                        and (self.iconOverlay==other.iconOverlay)
                        and (self.picture==other.picture)
                        and (self.iconPlanet==other.iconPlanet))

    def toString(self,indention):
        res = super().toString(0)
        res += self.themeCount.toString(0)
        res += self.icon.toString(0)
        res += self.iconOverlay.toString(0)
        res += self.picture.toString(0)
        res += self.iconPlanet.toString(0)
        return indentText(res,indention)

"""### ModThemes ###"""

class ModThemes(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"Window","")
        self._manifestThemes = None
        self._manifestPictures = None
        self._themesDef = None
        self._picturesDef = None
        subdir = os.path.join(currMod.moddir,"Window")
        if os.path.exists(subdir):
            for f in os.listdir(subdir):
                name,ext = os.path.splitext(f)
                if ext==".playerThemes" or ext==".playerPictures":
                    self.compFilesMod[f] = None

    @property
    def manifestThemes(self):
        if self._manifestThemes is None:
            import pysoase.mod.manifest as manifest
            inst,probs = self.loadFile("","playerThemes.manifest",
                    manifest.ManifestPlayerTheme)
            if inst is None:
                self._manifestThemes = probs
            else:
                self._manifestThemes = inst
        return self._manifestThemes

    @property
    def manifestPictures(self):
        if self._manifestPictures is None:
            import pysoase.mod.manifest as manifest
            inst,probs = self.loadFile("","playerPictures.manifest",
                    manifest.ManifestPlayerPicture)
            if inst is None:
                self._manifestPictures = probs
            else:
                self._manifestPictures = inst
        return self._manifestPictures

    @property
    def themesDef(self):
        if self._themesDef is None:
            inst,probs = self.loadFile(self.subName,
                    "PlayerThemesDef.playerThemesDef",
                    PlayerThemesDef)
            if inst is None:
                self._themesDef = probs
            else:
                self._themesDef = inst
        return self._themesDef

    @property
    def picturesDef(self):
        if self._picturesDef is None:
            inst,probs = self.loadFile(self.subName,
                    "PlayerPicturesDef.playerPicturesDef",
                    PlayerPicturesDef)
            if inst is None:
                self._picturesDef = probs
            else:
                self._picturesDef = inst
        return self._picturesDef

    def loadEntry(self,name):
        probs = []
        if name not in self.compFilesMod or self.compFilesMod[name] is None:
            fname,ext = os.path.splitext(name)
            fclass = None
            if ext==".playerPictures":
                fclass = PlayerPictures
            elif ext==".playerThemes":
                fclass = PlayerThemes
            inst,probs = self.loadFile(self.subName,name,fclass)
            if inst is None:
                self.compFilesMod[name] = probs
            else:
                self.compFilesMod[name] = inst
        elif isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name])
        return probs

    def checkEntry(self,name):
        probs = self.loadEntry(name)
        if not probs:
            probs.extend(self.compFilesMod[name].check(self.mod))
        man = None
        fname,ext = os.path.splitext(name)
        if ext==".playerPictures":
            man = self.manifestPictures
        elif ext==".playerThemes":
            man = self.manifestThemes
        if isinstance(man,list):
            probs.extend(man)
        else:
            probs.extend(man.checkEntry(name))
        return probs

    def checkManifestThemes(self,entry):
        probs = []
        if isinstance(self.manifestThemes,list):
            probs.extend(self.manifestThemes)
        else:
            probs.extend(self.manifestThemes.checkEntry(entry))
        return probs

    def checkManifestPictures(self,entry):
        probs = []
        if isinstance(self.manifestPictures,list):
            probs.extend(self.manifestPictures)
        else:
            probs.extend(self.manifestPictures.checkEntry(entry))
        return probs

    def check(self,strict=False):
        probs = []
        if strict and (not isinstance(self.manifestThemes,list)
        and not isinstance(self.manifestPictures,list)):
            with tqdm.tqdm(disable=self.mod.quiet,
                    total=len(self.manifestThemes.entries.elements)
                        +len(self.manifestPictures.entries.elements)+4,
                    desc="Checking Themes") as pbar:
                if isinstance(self.picturesDef,PlayerPicturesDef):
                    probs.extend(self.picturesDef.check(self.mod))
                else:
                    probs.extend(self.picturesDef)
                pbar.update()
                if isinstance(self.themesDef,PlayerThemesDef):
                    probs.extend(self.themesDef.check(self.mod))
                else:
                    probs.extend(self.themesDef)
                pbar.update()

                if isinstance(self.manifestThemes,list):
                    probs.extend(self.manifestThemes)
                else:
                    probs.extend(self.manifestThemes.check(self.mod))
                pbar.update()
                if isinstance(self.manifestPictures,list):
                    probs.extend(self.manifestPictures)
                else:
                    probs.extend(self.manifestPictures.check(self.mod))
                pbar.update()

                for thm in self.manifestThemes.entries.elements:
                    probs.extend(self.checkEntry(thm.ref))
                    pbar.update()
                for pic in self.manifestPictures.entries.elements:
                    probs.extend(self.checkEntry(pic.ref))
                    pbar.update()
        else:
            with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod)
                    +4,desc="Checking Themes") as pbar:
                if isinstance(self.picturesDef,PlayerPicturesDef):
                    probs.extend(self.picturesDef.check(self.mod))
                else:
                    probs.extend(self.picturesDef)
                pbar.update()
                if isinstance(self.themesDef,PlayerThemesDef):
                    probs.extend(self.themesDef.check(self.mod))
                else:
                    probs.extend(self.themesDef)
                pbar.update()

                if isinstance(self.manifestThemes,list):
                    probs.extend(self.manifestThemes)
                else:
                    probs.extend(self.manifestThemes.check(self.mod))
                pbar.update()
                if isinstance(self.manifestPictures,list):
                    probs.extend(self.manifestPictures)
                else:
                    probs.extend(self.manifestPictures.check(self.mod))
                pbar.update()

                for entry in self.compFilesMod:
                    probs.extend(self.checkEntry(entry))
                    pbar.update()
        return probs

    def checkFile(self,filepath):
        res = super().checkFile(filepath)
        checkInst = None
        if os.path.basename(filepath)=="playerPictures.manifest":
            checkInst = self.manifestPictures
        elif os.path.basename(filepath)=="playerThemes.manifest":
            checkInst = self.manifestThemes
        elif os.path.basename(filepath)=="PlayerPicturesDef.playerPicturesDef":
            checkInst = self.picturesDef
        elif os.path.basename(filepath)=="PlayerThemesDef.playerThemesDef":
            checkInst = self.themesDef
        else:
            name,ext = os.path.splitext(filepath)
            if ext==".playerThemes" or ext==".playerPictures":
                checkInst = self.checkEntry(os.path.basename(filepath))
            else:
                raise RuntimeError("Files of type "+filepath
                                   +"are not handled by ModThemes.")
        if isinstance(checkInst,list):
            res.extend(checkInst)
        else:
            res.extend(checkInst.check(self.mod))
        return res

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Loading Themes") as pbar:
            for entry in self.compFilesMod:
                probs.extend(self.loadEntry(entry))
                pbar.update()
        return probs
