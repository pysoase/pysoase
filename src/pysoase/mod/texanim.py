""" Classes and parsers concerning Sins texture animations
"""

import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.mod.ui as ui

"""################################ Parsers ################################"""

g_TextureAnimation = (
    co_parse.g_txt
    +co_parse.genStringAttrib("textureFileName")("texture")
    +co_parse.genIntAttrib("numFrames")("frames")
    +co_parse.genIntAttrib("numFramesPerRow")("framesPerRow")
    +pp.Group(
            co_parse.g_linestart
            +pp.Keyword("startTopLeft")("identifier")
            +co_parse.g_space
            +co_parse.g_pos2
            +co_parse.g_eol
    )("topLeft")
    +pp.Group(
            co_parse.g_linestart
            +pp.Keyword("frameSize")("identifier")
            +co_parse.g_space
            +co_parse.g_pos2
            +co_parse.g_eol
    )("frameSize")
    +pp.Group(
            co_parse.g_linestart
            +pp.Keyword("frameStride")("identifier")
            +co_parse.g_space
            +co_parse.g_pos2
            +co_parse.g_eol
    )("frameStride")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

class TextureAnimRef(co_attribs.FileRef):
    _REFTYPE = "Texture Animation"

    def __init__(self,**kwargs):
        super().__init__(subdir="TextureAnimations",extension="texanim",
                **kwargs)

"""############################## File Classes #############################"""

class TextureAnimation(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_TextureAnimation

    def __init__(self,texture,frames,framesPerRow,topLeft,frameSize,
            frameStride,**kwargs):
        super().__init__(**kwargs)
        self.texture = ui.UITextureRef(**texture)
        self.frames = co_attribs.AttribNum(**frames)
        self.framesPerRow = co_attribs.AttribNum(**framesPerRow)
        self.topLeft = co_attribs.AttribPos2d(**topLeft)
        self.frameSize = co_attribs.AttribPos2d(**frameSize)
        self.frameStride = co_attribs.AttribPos2d(**frameStride)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.texture==other.texture)
                        and (self.frames==other.frames)
                        and (self.framesPerRow==other.framesPerRow)
                        and (self.topLeft==other.topLeft)
                        and (self.frameSize==other.frameSize)
                        and (self.frameStride==other.frameStride))

    def toString(self,indention):
        res = super().toString(0)
        res += self.texture.toString(0)
        res += self.frames.toString(0)
        res += self.framesPerRow.toString(0)
        res += self.topLeft.toString(0)
        res += self.frameSize.toString(0)
        res += self.frameStride.toString(0)
        return indentText(res,indention)

"""################################ Component ##############################"""

class ModTextureAnim(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"TextureAnimations","texanim")

    def loadTexAnim(self,name):
        probs = []
        if name not in self.compFilesMod or self.compFilesMod[name] is None:
            inst,probs = self.loadFile(self.subName,name,TextureAnimation)
            if inst is None:
                self.compFilesMod[name] = probs
            else:
                self.compFilesMod[name] = inst
        elif isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name])
        return probs

    def checkTexAnim(self,name):
        probs = self.loadTexAnim(name)
        if not probs:
            probs.extend(self.compFilesMod[name].check(self.mod))
        return probs

    def check(self,strict=False):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Checking Texture Animations") as pbar:
            for name in self.compFilesMod:
                probs.extend(self.checkTexAnim(name))
                pbar.update()
        return probs

    def checkFile(self,filepath):
        base = os.path.basename(filepath)
        name,ext = os.path.splitext(base)
        if ext==".texanim":
            return self.checkTexAnim(base)
        else:
            raise RuntimeError(ext+" is and invalid file type for "
                                   "ModTextureAnim. It only handles .texanim "
                                   "type files.")

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Loading Texture Animations") as pbar:
            for name in self.compFilesMod:
                probs.extend(self.loadTexAnim(name))
                pbar.update()
        return probs
