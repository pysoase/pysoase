""" Parsers and classes relating to Sins Galaxies/Scenarios.
"""

import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui

"""################################# Types #################################"""

CONNECTION_TYPES = ["PhaseLane","Wormhole"]
GAME_TYPES = ["FFA","FiveTeamsOfTwo","FourTeamsOfTwo","Solo",
    "ThreeTeamsOfThree","ThreeTeamsOfTwo","TwoTeamsOfFive",
    "TwoTeamsOfFour","TwoTeamsOfThree","TwoTeamsOfTwo"]
GROUP_COND_TYPES = ["Always","NoPlanetOwner","NoPlanetOwnerWithChance",
    "PlanetOwnerIsMilitia","PlanetOwnerIsMilitiaWithChance",
    "PlanetOwnerIsRaceNormalStart","PlanetOwnerIsRaceQuickStart"]

"""################################ Parsers ################################"""

"""### Basic Elements ###"""

g_pos = pp.Group(
        co_parse.g_linestart
        +pp.Keyword("pos")("identifier")
        +co_parse.g_space
        +co_parse.g_pos2
        +co_parse.g_eol
)

g_radii = (
    co_parse.genDecimalAttrib("moveAreaRadius")("moveRadius")
    +co_parse.genDecimalAttrib("hyperspaceExitRadius")("hyperRadius")
)
g_radii_fwd = pp.Forward()

"""### Galaxy Scenario Defs ###"""

g_entityDef = co_parse.genStringAttrib("entityDefName")("entityDef")
g_orbitBodyDesignContent = (
    co_parse.genStringAttrib("designName")("name")
    +co_parse.genStringAttrib("designStringId")("designStringId")
    +co_parse.genListAttrib("orbitBodyTypeCount",
            co_parse.genStringAttrib("orbitBodyType"))("orbitBodyTypes")
)
g_orbitBodyDesignStar = pp.Group(
        co_parse.genHeading("starType")
        +g_orbitBodyDesignContent
)
g_orbitBodyDesignPlanet = pp.Group(
        co_parse.genHeading("planetType")
        +g_orbitBodyDesignContent
)
g_orbitBodyType = pp.Group(
        co_parse.genHeading("orbitBodyType")
        +co_parse.genStringAttrib("typeName")("name")
        +g_entityDef
        +co_parse.genStringAttrib("defaultTemplateName")("defTemplate")
)
g_entityDesign = (
    co_parse.genStringAttrib("designName")("name")
    +g_entityDef
)
g_planetItemDesign = pp.Group(
        co_parse.genHeading("planetItemType")
        +g_entityDesign
)
g_playerDesign = pp.Group(
        co_parse.genHeading("playerType")
        +g_entityDesign
)
g_itemCondition = pp.Group(
        co_parse.genHeading("condition")
        +co_parse.genEnumAttrib("type",GROUP_COND_TYPES)("condType")
        +co_parse.genStringAttrib("param")("param")
)("condition")
g_itemGroup = pp.Group(
        co_parse.genHeading("group")
        +g_itemCondition
        +co_parse.genStringAttrib("owner")("owner")
        +co_parse.genIntAttrib("colonizeChance")("colonizeChance")
        +co_parse.genListAttrib("items",
                co_parse.genStringAttrib("item"))("items")
)
g_planetItemTemplateContent = (
    co_parse.genStringAttrib("templateName")("name")
    +co_parse.genListAttrib("subTemplates",
            co_parse.genStringAttrib("template"))(
            "subTemplates")
    +co_parse.genListAttrib("groups",g_itemGroup)("groups")
)
g_planetItemTemplate = pp.Group(
        co_parse.genHeading("planetItemsTemplate")
        +g_planetItemTemplateContent
)
g_scenarioPlayer = pp.Group(
        co_parse.genHeading("player")
        +co_parse.genStringAttrib("designName")("name")
        +co_parse.genStringAttrib("inGameName")("ingameName")
        +co_parse.genStringAttrib("overrideRaceName")("overrideRaceName")
        +co_parse.genIntAttrib("teamIndex")("teamIndex")
        +co_parse.genIntAttrib("startingCredits")("startingCredits")
        +co_parse.genIntAttrib("startingMetal")("startingMetal")
        +co_parse.genIntAttrib("startingCrystal")("startingCrystal")
        +co_parse.genBoolAttrib("isNormalPlayer")("isNormal")
        +co_parse.genBoolAttrib("isRaidingPlayer")("isRaiding")
        +co_parse.genBoolAttrib("isInsurgentPlayer")("isInsurgent")
        +co_parse.genBoolAttrib("isOccupationPlayer")("isOccupation")
        +co_parse.genBoolAttrib("isMadVasariPlayer")("isMad")
        +co_parse.genStringAttrib("themeGroup")("theme")
        +co_parse.genIntAttrib("themeIndex")("themeIndex")
        +co_parse.genStringAttrib("pictureGroup")("picture")
        +co_parse.genIntAttrib("pictureIndex")("pictureIndex")
)
g_GalaxyScenarioDef = (
    co_parse.g_txt
    +co_parse.genListAttrib("starTypeCount",g_orbitBodyDesignStar)("starTypes")
    +co_parse.genListAttrib("planetTypeCount",
            g_orbitBodyDesignPlanet)("planetTypes")
    +co_parse.genListAttrib("orbitBodyTypeCount",g_orbitBodyType)(
            "orbitBodyTypes")
    +co_parse.genListAttrib("planetItemTypeCount",g_planetItemDesign)(
            "planetItems")
    +co_parse.genListAttrib("playerTypeCount",g_playerDesign)("players")
    +co_parse.genListAttrib("planetItemsTemplateCount",g_planetItemTemplate)(
            "planetTemplates")
    +co_parse.genListAttrib("validPictureGroups",
            co_parse.genStringAttrib("pictureGroup"))("validPictures")
    +co_parse.genListAttrib("validThemeGroups",
            co_parse.genStringAttrib("themeGroup"))("validThemes")
    +co_parse.genListAttrib("autoAddPlayers",g_scenarioPlayer)("autoPlayers")
    +co_parse.genIntAttrib("autoAddMiltiaPlayerCountPerGalaxy")(
            "autoMilitiaPerGalaxy")
    +co_parse.genListAttrib("autoAddMilitiaPlayers",g_scenarioPlayer)(
            "autoMilitiaPlayers")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Galaxy ###"""

g_orbitBodyPreamble = (
    co_parse.genStringAttrib("designName")("designName")
    +co_parse.genStringAttrib("inGameName")("ingameName")
    +co_parse.genStringAttrib("type")("type")
    +g_pos("pos")
)
g_planetItems = pp.Group(
        co_parse.genHeading("planetItems")
        +g_planetItemTemplateContent
)
g_fixedPlanet = pp.Group(
        co_parse.genHeading("planet")
        +g_orbitBodyPreamble
        +g_radii_fwd
        +co_parse.genStringAttrib("owner")("owner")
        +co_parse.genBoolAttrib("isHomePlanet")("isHome")
        +pp.Group(
                co_parse.genIntAttrib("normalStartUpgradeLevelForPopulation")(
                        "pop")
                +co_parse.genIntAttrib("normalStartUpgradeLevelForCivilianModules")(
                        "civ")
                +co_parse.genIntAttrib("normalStartUpgradeLevelForTacticalModules")(
                        "tac")
                +co_parse.genIntAttrib("normalStartUpgradeLevelForArtifacts")(
                        "artifact")
                +co_parse.genIntAttrib("normalStartUpgradeLevelForInfrastructure")(
                        "infrastructure")
        )("normalUpg")
        +pp.Group(
                co_parse.genIntAttrib("quickStartUpgradeLevelForPopulation")(
                        "pop")
                +co_parse.genIntAttrib("quickStartUpgradeLevelForCivilianModules")(
                        "civ")
                +co_parse.genIntAttrib("quickStartUpgradeLevelForTacticalModules")(
                        "tac")
                +co_parse.genIntAttrib("quickStartUpgradeLevelForArtifacts")(
                        "artifact")
                +co_parse.genIntAttrib("quickStartUpgradeLevelForInfrastructure")(
                        "infrastructure")
        )("quickUpg")
        +g_planetItems("items")
        +co_parse.genDecimalAttrib("spawnProbability")("spawnProb")
        +co_parse.genBoolAttrib("useDefaultTemplate")("useDefTemplate")
        +co_parse.genIntAttrib("entityCount")("entityCount")
        +co_parse.genIntAttrib("asteroidCount")("asteroidCount")
)
g_gravWellConnection = pp.Group(
        co_parse.genHeading("connection")
        +co_parse.genIntAttrib("planetIndexA")("indexA")
        +co_parse.genIntAttrib("planetIndexB")("indexB")
        +co_parse.genDecimalAttrib("spawnProbability")("spawnProb")
        +co_parse.genEnumAttrib("type",CONNECTION_TYPES)("connType")
)
g_starConnection = pp.Group(
        co_parse.genHeading("interStarConnection")
        +co_parse.genIntAttrib("starIndexA")("starA")
        +co_parse.genIntAttrib("planetIndexA")("planetA")
        +co_parse.genIntAttrib("starIndexB")("starB")
        +co_parse.genIntAttrib("planetIndexB")("planetB")
        +co_parse.genDecimalAttrib("spawnProbability")("spawnProb")
        +co_parse.genEnumAttrib("type",CONNECTION_TYPES)("connType")
)
g_fixexStar = pp.Group(
        co_parse.genHeading("star")
        +g_orbitBodyPreamble
        +co_parse.genDecimalAttrib("radius")("radius")
        +g_radii_fwd
        +co_parse.genListAttrib("planetCount",g_fixedPlanet)("planets")
        +co_parse.genListAttrib("connectionCount",g_gravWellConnection)(
                "connections")
        +co_parse.genIntAttrib("entityCount")("entityCount")
        +co_parse.genDecimalAttrib("spawnProbability")("spawnProb")
)
g_galaxyTemplate = pp.Group(
        co_parse.genHeading("template")
        +g_planetItemTemplateContent
)
g_fixedCartography = pp.Group(
        co_parse.genBoolAttrib("useRandomGenerator",
                co_parse.g_false)("random")
        +co_parse.genDecimalAttrib("galaxyWidth")("galWidth")
        +co_parse.genDecimalAttrib("galaxyHeight")("galHeight")
        +co_parse.genIntAttrib("nextStarNameUniqueId")("nextStarNameId")
        +co_parse.genIntAttrib("nextPlanetNameUniqueId")("nextPlanetNameId")
        +co_parse.genIntAttrib("triggerCount")("triggerCount")
        +co_parse.genListAttrib("starCount",g_fixexStar)("stars")
        +co_parse.genListAttrib("interStarConnectionCount",g_starConnection)(
                "starConnections")
        +co_parse.genListAttrib("playerCount",g_scenarioPlayer)("players")
        +co_parse.genListAttrib("templates",g_galaxyTemplate)("templates")
)("cartography")
g_minMaxPerc = (
    co_parse.genDecimalAttrib("minPercentage")("minPercent")
    +co_parse.genDecimalAttrib("maxPercentage")("maxPercent")
)
g_planetGroupContent = (
    co_parse.genIntAttrib("minCount")("minCount")
    +co_parse.genIntAttrib("maxCount")("maxCount")
    +co_parse.genListAttrib("planetTypeCount",
            co_parse.genStringAttrib("planetType"))(
            "planetTypes")
)
g_extraPlanets = pp.Group(
        co_parse.genHeading("extraPlanetGroup")
        +g_planetGroupContent
)
g_playerParams = pp.Group(
        co_parse.genHeading("playerParams")
        +co_parse.genIntAttrib("startingCredits")("startCredits")
        +co_parse.genIntAttrib("startingMetal")("startMetal")
        +co_parse.genIntAttrib("startingCrystal")("startCrystal")
        +co_parse.genStringAttrib("homePlanetType")("homePlanet")
        +pp.Group(
                co_parse.genHeading("homePlanetStarRadiusRange")
                +g_minMaxPerc
        )("homePlanetRadius")
        +co_parse.genBoolAttrib("areExtraPlanetsColonized")("extrasColonized")
        +co_parse.genIntAttrib("extraPlanetsMaxRadius")("extrasRadiusMax")
        +pp.Group(
                co_parse.genHeading("extraPlanetsRadiusRange")
                +g_minMaxPerc
        )("extrasRadiusRange")
        +co_parse.genListAttrib("extraPlanetGroupCount",g_extraPlanets)(
                "extraPlanets")
)("playerParams")
g_randParams = pp.Group(
        co_parse.genHeading("randomizerParams")
        +pp.Group(
                co_parse.genHeading("starPosOffsetRange")
                +g_minMaxPerc
        )("starPosOffset")
        +g_playerParams
)
g_planetGroup = pp.Group(
        co_parse.genHeading("planetGroup")
        +g_planetGroupContent
)
g_ring = pp.Group(
        co_parse.genHeading("ring")
        +pp.Group(
                co_parse.genHeading("starRadiusRange")
                +g_minMaxPerc
        )("starDistance")
        +co_parse.genIntAttrib("militiaColonizationPerc")("militiaColPerc")
        +co_parse.genListAttrib("planetGroupCount",g_planetGroup)("planets")
)
g_randomStar = pp.Group(
        co_parse.genHeading("star")
        +co_parse.genStringAttrib("type")("starType")
        +co_parse.genIntAttrib("radius")("radius")
        +g_radii_fwd
        +co_parse.genDecimalAttrib("connectionStarRadiusRange")("connectionRadius")
        +co_parse.genDecimalAttrib("connectionChance")("connectionChance")
        +co_parse.genIntAttrib("maxPlayerCount")("maxPlayers")
        +co_parse.genListAttrib("ringCount",g_ring)("planets")
)
g_randomCartography = pp.Group(
        co_parse.genBoolAttrib("useRandomGenerator",
                co_parse.g_true)("random")
        +g_randParams("randParams")
        +co_parse.genListAttrib("starCount",g_randomStar)("stars")
)("cartography")
g_cartography = pp.Or([g_fixedCartography,g_randomCartography])
g_homeUpgNormal = pp.Group(
        co_parse.genIntAttrib("normalStartHomePlanetUpgradeLevel:Population")(
                "pop")
        +co_parse.genIntAttrib(
                "normalStartHomePlanetUpgradeLevel:CivilianModules")("civ")
        +co_parse.genIntAttrib(
                "normalStartHomePlanetUpgradeLevel:TacticalModules")("tac")
        +co_parse.genIntAttrib("normalStartHomePlanetUpgradeLevel:Home")(
                "home")
        +co_parse.genIntAttrib(
                "normalStartHomePlanetUpgradeLevel:ArtifactLevel")("artifact")
        +co_parse.genIntAttrib(
                "normalStartHomePlanetUpgradeLevel:Infrastructure")(
                "infrastructure")
)
g_homeUpgQuick = pp.Group(
        co_parse.genIntAttrib("quickStartHomePlanetUpgradeLevel:Population")(
                "pop")
        +co_parse.genIntAttrib(
                "quickStartHomePlanetUpgradeLevel:CivilianModules")("civ")
        +co_parse.genIntAttrib(
                "quickStartHomePlanetUpgradeLevel:TacticalModules")("tac")
        +co_parse.genIntAttrib("quickStartHomePlanetUpgradeLevel:Home")(
                "home")
        +co_parse.genIntAttrib(
                "quickStartHomePlanetUpgradeLevel:ArtifactLevel")("artifact")
        +co_parse.genIntAttrib(
                "quickStartHomePlanetUpgradeLevel:Infrastructure")(
                "infrastructure")
)

def setVersionFormat(tokens):
    if tokens[0][1]==4:
        g_radii_fwd<<g_radii
    else:
        g_radii_fwd<<pp.Empty().leaveWhitespace()

g_Galaxy = (
    co_parse.g_txt
    +co_parse.genIntAttrib("versionNumber")("version").setParseAction(
            setVersionFormat)
    +co_parse.genBoolAttrib("isBrowsable")("browsable")
    +co_parse.genStringAttrib("browsePictureName")("picName")
    +co_parse.genStringAttrib("browseName")("browseName")
    +co_parse.genStringAttrib("browseDescription")("browseDesc")
    +co_parse.genBoolAttrib("isFirstCapitalShipIsFlagship")(
            "firstCapitalIsFlagship")
    +co_parse.genBoolAttrib("randomizeStartingPositions")("randStartPos")
    +co_parse.genDecimalAttrib("planetArtifactDensity")("artifactDensity")
    +co_parse.genDecimalAttrib("planetBonusDensity")("bonusDensity")
    +g_homeUpgNormal("homeUpgradesNormal")
    +g_homeUpgQuick("homeUpgradesQuick")
    +co_parse.genListAttrib("recommendedGameTypeCount",
            co_parse.genEnumAttrib("recommendedGameType",GAME_TYPES))(
            "recommGameTypes")
    +co_parse.genDecimalAttrib("metersPerGalaxyUnit")("meterPerUnit")
    +co_parse.genDecimalAttrib("pixelsPerGalaxyUnit")("meterPerPixel")
    +g_cartography
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

"""### GalaxyScenarioDef ###"""

class EntityDesign(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,entTypes,name,entityDef,**kwargs):
        super().__init__(**kwargs)
        self.entityDef = coe.EntityRef(types=entTypes,**entityDef)
        self.name = co_attribs.AttribString(**name)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.name==other.name) and (
            self.entityDef==other.entityDef)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.entityDef.toString(1)
        return indentText(res,indention)

class OrbitBodyTypeRef(co_attribs.AttribRef):
    _REFTYPE = "Scenario Orbit Body Type"
    _allowedBodyTypes = ["Planet","Star"]

    def __init__(self,parent,bodyType,**kwargs):
        super().__init__(**kwargs)
        if bodyType not in self._allowedBodyTypes:
            raise RuntimeError("Body type "+bodyType+" is invalid."
                               +"Allowed types are "
                               +str(self._allowedBodyTypes))
        if parent is None or not isinstance(parent,GalaxyScenarioDef):
            raise RuntimeError("Parent file for OrbitBodyTypeRef must be "
                               +"an instance of GalaxyScenarioDef.")
        self.parent = parent
        self.bodyType = bodyType

    def __eq__(self,other):
        return super().__eq__(other)

    def toString(self,indention):
        return super().toString(indention)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            if self.ref not in self.parent.orbitBodyTypes:
                res.append(co_probs.MissingRefProblem("Orbit Body Type",self.ref,
                        probLine=self.linenum))
            elif not self.parent.checkTypeOrbitBodyType(currMod,self.ref,
                    self.bodyType):
                res.append(
                        co_probs.BasicProblem("OrbitBodyType "+self.ref+" does not "
                                              +"have expected type "
                                              +self.bodyType+".",
                                severity=co_probs.ProblemSeverity.error,
                                probLine=self.linenum))
        return res

class OrbitBodyDesign(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,designStringId,orbitBodyTypes,identifier,parent,
            **kwargs):
        if identifier=="planetType":
            bodyType = "Planet"
        elif identifier=="starType":
            bodyType = "Star"
        else:
            raise RuntimeError("Invalid identifier \""+identifier+"\" for "
                               +"OrbitBodyDesign")
        super().__init__(identifier=identifier,**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.designStringId = ui.StringReference(**designStringId)
        self.types = co_attribs.AttribList(
                elemType=OrbitBodyTypeRef,
                elemArgs={"bodyType":bodyType,"parent":parent},
                **orbitBodyTypes)

    def __eq__(self,other):
        res = super().__eq__(other)
        return (res
                and (self.name==other.name)
                and (self.designStringId==other.designStringId)
                and (self.types==other.types))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.designStringId.toString(1)
        res += self.types.toString(1)
        return indentText(res,indention)

class TemplateRef(co_attribs.AttribRef):
    _REFTYPE = "Scenario Template"

    def __init__(self,identifier,val,parent=None,**kwargs):
        super().__init__(identifier,val,**kwargs)
        self.parent = parent

    def __eq__(self,other):
        return super().__eq__(other)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and not isinstance(currMod.scenarios.galDef,list):
            if not ((self.ref in currMod.scenarios.galDef.planetTemplates)
                    or (self.parent is not None
                        and self.ref in self.parent.templates)):
                res.append(co_probs.MissingRefProblem("Scenario Template",self.ref,
                        probLine=self.linenum))
        return res

class OrbitBodyType(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,entityDef,defTemplate,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.entityDef = coe.EntityRef(types=["Star","Planet"],**entityDef)
        self.defTemplate = TemplateRef(**defTemplate)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.entityDef==other.entityDef)
                        and (self.defTemplate==other.defTemplate))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.entityDef.toString(1)
        res += self.defTemplate.toString(1)
        return indentText(res,indention)

class Condition(co_attribs.Attribute):
    def __init__(self,condType,param,**kwargs):
        super().__init__(**kwargs)
        self.condType = co_attribs.AttribEnum(possibleVals=GROUP_COND_TYPES,**condType)
        self.param = co_attribs.AttribString(**param)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.condType==other.condType)
                        and (self.param==other.param))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.condType.toString(1)
        res += self.param.toString(1)
        return indentText(res,indention)

class ItemRef(co_attribs.AttribRef):
    _REFTYPE = "Scenario Item"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and not isinstance(currMod.scenarios.galDef,list):
            if self.ref not in currMod.scenarios.galDef.planetItems:
                res.append(
                        co_probs.MissingRefProblem("Planet Item",self.ref,
                        probLine=self.linenum))
        return res

    def __eq__(self,other):
        return super().__eq__(other)

    def toString(self,indention):
        return super().toString(indention)

class ScenarioGroup(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,condition,owner,colonizeChance,items,parent=None,
            **kwargs):
        super().__init__(**kwargs)
        self.condition = Condition(**condition)
        if owner["val"]!="PlanetOwner" and parent:
            self.owner = PlayerDesignRef(parent=parent,**owner)
        else:
            self.owner = co_attribs.AttribString(**owner)
        self.colChance = co_attribs.AttribNum(**colonizeChance)
        self.items = co_attribs.AttribList(elemType=ItemRef,**items)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.condition==other.condition)
                        and (self.owner==other.owner)
                        and (self.colChance==other.colChance)
                        and (self.items==other.items))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.condition.toString(1)
        res += self.owner.toString(1)
        res += self.colChance.toString(1)
        res += self.items.toString(1)
        return indentText(res,indention)

class ScenarioTemplate(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,subTemplates,groups,parent=None,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        if not parent:
            self.subTemplates = co_attribs.AttribList(elemType=TemplateRef,
                    **subTemplates)
            self.groups = co_attribs.AttribList(elemType=ScenarioGroup,**groups)
        else:
            self.subTemplates = co_attribs.AttribList(
                    elemType=TemplateRef,
                    elemArgs={"parent":parent},
                    **subTemplates)
            self.groups = co_attribs.AttribList(elemType=ScenarioGroup,
                    elemArgs={"parent":parent},
                    **groups)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.subTemplates==other.subTemplates)
                        and (self.groups==other.groups))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.subTemplates.toString(1)
        res += self.groups.toString(1)
        return indentText(res,indention)

class ScenarioPlayer(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,ingameName,overrideRaceName,teamIndex,
            startingCredits,startingMetal,startingCrystal,isNormal,isRaiding,
            isInsurgent,isOccupation,isMad,theme,themeIndex,picture,
            pictureIndex,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.ingameName = co_attribs.AttribString(**ingameName)
        self.overrideRaceName = co_attribs.AttribString(**overrideRaceName)
        self.teamIndex = co_attribs.AttribNum(**teamIndex)
        self.startingCredits = co_attribs.AttribNum(**startingCredits)
        self.startingMetal = co_attribs.AttribNum(**startingMetal)
        self.startingCrystal = co_attribs.AttribNum(**startingCrystal)
        self.isNormal = co_attribs.AttribBool(**isNormal)
        self.isRaiding = co_attribs.AttribBool(**isRaiding)
        self.isInsurgent = co_attribs.AttribBool(**isInsurgent)
        self.isOccupation = co_attribs.AttribBool(**isOccupation)
        self.isMad = co_attribs.AttribBool(**isMad)
        self.theme = themes.PlayerThemeRef(**theme)
        self.themeIndex = co_attribs.AttribNum(**themeIndex)
        self.picture = themes.PlayerPictureRef(**picture)
        self.pictureIndex = co_attribs.AttribNum(**pictureIndex)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.ingameName==other.ingameName)
                        and (self.overrideRaceName==other.overrideRaceName)
                        and (self.teamIndex==other.teamIndex)
                        and (self.startingCredits==other.startingCredits)
                        and (self.startingMetal==other.startingMetal)
                        and (self.startingCrystal==other.startingCrystal)
                        and (self.isNormal==other.isNormal)
                        and (self.isRaiding==other.isRaiding)
                        and (self.isInsurgent==other.isInsurgent)
                        and (self.isOccupation==other.isOccupation)
                        and (self.isMad==other.isMad)
                        and (self.theme==other.theme)
                        and (self.themeIndex==other.themeIndex)
                        and (self.picture==other.picture)
                        and (self.pictureIndex==other.pictureIndex))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.ingameName.toString(1)
        res += self.overrideRaceName.toString(1)
        res += self.teamIndex.toString(1)
        res += self.startingCredits.toString(1)
        res += self.startingMetal.toString(1)
        res += self.startingCrystal.toString(1)
        res += self.isNormal.toString(1)
        res += self.isRaiding.toString(1)
        res += self.isInsurgent.toString(1)
        res += self.isOccupation.toString(1)
        res += self.isMad.toString(1)
        res += self.theme.toString(1)
        res += self.themeIndex.toString(1)
        res += self.picture.toString(1)
        res += self.pictureIndex.toString(1)
        return indentText(res,indention)

"""### Galaxy ###"""

class HomeUpgrades(co_attribs.Attribute):
    def __init__(self,pop,civ,tac,artifact,infrastructure,home=None,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.pop = co_attribs.AttribNum(**pop)
        self.civ = co_attribs.AttribNum(**civ)
        self.tac = co_attribs.AttribNum(**tac)
        if home:
            self.home = co_attribs.AttribNum(**home)
        else:
            self.home = None
        self.artifact = co_attribs.AttribNum(**artifact)
        self.infrastructure = co_attribs.AttribNum(**infrastructure)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.pop==other.pop)
                        and (self.civ==other.civ)
                        and (self.tac==other.tac)
                        and (self.home==other.home)
                        and (self.artifact==other.artifact)
                        and (self.infrastructure==other.infrastructure))

    def toString(self,indention):
        res = super().toString(0)
        res += self.pop.toString(0)
        res += self.civ.toString(0)
        res += self.tac.toString(0)
        if self.home:
            res += self.home.toString(0)
        res += self.artifact.toString(0)
        res += self.infrastructure.toString(0)
        return indentText(res,indention)

class OrbitBodyDesignRef(co_attribs.AttribRef):
    _allowedBodyTypes = ["Planet","Star"]

    def __init__(self,bodyType,**kwargs):
        super().__init__(**kwargs)
        if bodyType not in self._allowedBodyTypes:
            raise RuntimeError("Body type "+bodyType+" is invalid."
                               +"Allowed types are "
                               +str(self._allowedBodyTypes))
        self.bodyType = bodyType

    def __eq__(self,other):
        return super().__eq__(other)

    def toString(self,indention):
        return super().toString(indention)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and not isinstance(currMod.scenarios.galDef,list):
            if (self.bodyType=="Planet"
            and self.ref not in currMod.scenarios.galDef.planetTypes):
                res.append(co_probs.MissingRefProblem("Orbit Body Design",self.ref,
                        probLine=self.linenum))
            elif (self.bodyType=="Star"
            and self.ref not in currMod.scenarios.galDef.starTypes):
                res.append(co_probs.MissingRefProblem("Orbit Body Design",self.ref,
                        probLine=self.linenum))
        return res

class MinMaxRadius(co_attribs.Attribute):
    def __init__(self,minPercent,maxPercent,**kwargs):
        super().__init__(**kwargs)
        self.minPercent = co_attribs.AttribNum(**minPercent)
        self.maxPercent = co_attribs.AttribNum(**maxPercent)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.minPercent==other.minPercent)
                        and (self.maxPercent==other.maxPercent))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.minPercent.toString(1)
        res += self.maxPercent.toString(1)
        return indentText(res,indention)

class PlanetGroup(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,minCount,maxCount,planetTypes,**kwargs):
        super().__init__(**kwargs)
        self.minCount = co_attribs.AttribNum(**minCount)
        self.maxCount = co_attribs.AttribNum(**maxCount)
        self.planetTypes = co_attribs.AttribList(
                elemType=OrbitBodyDesignRef,
                elemArgs={"bodyType":"Planet"},
                **planetTypes
        )

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.minCount==other.minCount)
                        and (self.maxCount==other.maxCount)
                        and (self.planetTypes==other.planetTypes))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.minCount.toString(1)
        res += self.maxCount.toString(1)
        res += self.planetTypes.toString(1)
        return indentText(res,indention)

class Ring(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,starDistance,militiaColPerc,planets,**kwargs):
        super().__init__(**kwargs)
        self.starDistance = MinMaxRadius(**starDistance)
        self.militiaColPerc = co_attribs.AttribNum(**militiaColPerc)
        self.planets = co_attribs.AttribList(elemType=PlanetGroup,**planets)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.starDistance==other.starDistance)
                        and (self.militiaColPerc==other.militiaColPerc)
                        and (self.planets==other.planets))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.starDistance.toString(1)
        res += self.militiaColPerc.toString(1)
        res += self.planets.toString(1)
        return indentText(res,indention)

class FixedOrbitBody(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,fileVersion,designName,ingameName,type,bodyType,pos,
            moveRadius=None,hyperRadius=None,**kwargs):
        super().__init__(**kwargs)
        self.designName = co_attribs.AttribString(**designName)
        self.ingameName = co_attribs.AttribString(**ingameName)
        self.type = OrbitBodyDesignRef(bodyType=bodyType,**type)
        self.pos = co_attribs.AttribPos2d(**pos)
        self.fileVersion = fileVersion
        if fileVersion==4:
            self.moveRadius = co_attribs.AttribNum(**moveRadius)
            self.hyperRadius = co_attribs.AttribNum(**hyperRadius)

    def __eq__(self,other):
        res = super().__eq__(other)
        if self.fileVersion==4:
            res = res and ((self.moveRadius==other.moveRadius)
                           and (self.hyperRadius==other.hyperRadius))
        return res and ((self.designName==other.designName)
                        and (self.ingameName==other.ingameName)
                        and (self.type==other.type)
                        and (self.pos==other.pos))

class RandomStar(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,starType,radius,connectionRadius,
            connectionChance,maxPlayers,planets,fileVersion,moveRadius=None,
            hyperRadius=None,**kwargs):
        super().__init__(**kwargs)
        self.starType = OrbitBodyDesignRef(bodyType="Star",**starType)
        self.radius = co_attribs.AttribNum(**radius)
        self.fileVersion = fileVersion
        if self.fileVersion==4:
            self.moveRadius = co_attribs.AttribNum(**moveRadius)
            self.hyperRadius = co_attribs.AttribNum(**hyperRadius)
        self.connectionRadius = co_attribs.AttribNum(**connectionRadius)
        self.connectionChance = co_attribs.AttribNum(**connectionChance)
        self.maxPlayers = co_attribs.AttribNum(**maxPlayers)
        self.planets = co_attribs.AttribList(elemType=Ring,**planets)

    def __eq__(self,other):
        res = super().__eq__(other)
        if self.fileVersion==4:
            res = res and ((self.moveRadius==other.moveRadius)
                           and (self.hyperRadius==other.hyperRadius))
        return res and ((self.starType==other.starType)
                        and (self.radius==other.radius)
                        and (self.connectionRadius==other.connectionRadius)
                        and (self.connectionChance==other.connectionChance)
                        and (self.maxPlayers==other.maxPlayers)
                        and (self.planets==other.planets))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.starType.toString(1)
        res += self.radius.toString(1)
        if self.fileVersion==4:
            res += self.moveRadius.toString(1)
            res += self.hyperRadius.toString(1)
        res += self.connectionRadius.toString(1)
        res += self.connectionChance.toString(1)
        res += self.maxPlayers.toString(1)
        res += self.planets.toString(1)
        return indentText(res,indention)

class PlayerParams(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,startCredits,startMetal,startCrystal,homePlanet,
            homePlanetRadius,extrasColonized,extrasRadiusMax,extrasRadiusRange,
            extraPlanets,**kwargs):
        super().__init__(**kwargs)
        self.startCredits = co_attribs.AttribNum(**startCredits)
        self.startMetal = co_attribs.AttribNum(**startMetal)
        self.startCrystal = co_attribs.AttribNum(**startCrystal)
        self.homePlanet = OrbitBodyDesignRef(bodyType="Planet",**homePlanet)
        self.homePlanetRadius = MinMaxRadius(**homePlanetRadius)
        self.extrasColonized = co_attribs.AttribBool(**extrasColonized)
        self.extrasRadiusMax = co_attribs.AttribNum(**extrasRadiusMax)
        self.extrasRadiusRange = MinMaxRadius(**extrasRadiusRange)
        self.extraPlanets = co_attribs.AttribList(elemType=PlanetGroup,**extraPlanets)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.startCredits==other.startCredits)
                        and (self.startMetal==other.startMetal)
                        and (self.startCrystal==other.startCrystal)
                        and (self.homePlanet==other.homePlanet)
                        and (self.homePlanetRadius==other.homePlanetRadius)
                        and (self.extrasColonized==other.extrasColonized)
                        and (self.extrasRadiusMax==other.extrasRadiusMax)
                        and (self.extrasRadiusRange==other.extrasRadiusRange)
                        and (self.extraPlanets==other.extraPlanets))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.startCredits.toString(1)
        res += self.startMetal.toString(1)
        res += self.startCrystal.toString(1)
        res += self.homePlanet.toString(1)
        res += self.homePlanetRadius.toString(1)
        res += self.extrasColonized.toString(1)
        res += self.extrasRadiusMax.toString(1)
        res += self.extrasRadiusRange.toString(1)
        res += self.extraPlanets.toString(1)
        return indentText(res,indention)

class RandParams(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,starPosOffset,playerParams,**kwargs):
        super().__init__(**kwargs)
        self.starPosOffset = MinMaxRadius(**starPosOffset)
        self.playerParams = PlayerParams(**playerParams)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.starPosOffset==other.starPosOffset)
                        and (self.playerParams==other.playerParams))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.starPosOffset.toString(1)
        res += self.playerParams.toString(1)
        return indentText(res,indention)

class Cartography(co_attribs.Attribute):
    def __init__(self,random,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.random = co_attribs.AttribBool(**random)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.random==other.random)

    @classmethod
    def factory(cls,random,parent,**kwargs):
        if random["val"]:
            return RandomCartography(random=random,
                    fileVersion=parent.version.value,
                    **kwargs)
        else:
            return FixedCartography(random=random,parent=parent,**kwargs)

    def toString(self,indention):
        res = super().toString(0)
        res += self.random.toString(0)
        return indentText(res,indention)

class RandomCartography(Cartography,co_basics.HasReference):
    def __init__(self,randParams,stars,fileVersion,**kwargs):
        super().__init__(**kwargs)
        self.randParams = RandParams(**randParams)
        self.stars = co_attribs.AttribList(elemType=RandomStar,
                elemArgs={"fileVersion":fileVersion},
                **stars)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.randParams==other.randParams)
                        and (self.stars==other.stars))

    def toString(self,indention):
        res = super().toString(0)
        res += self.randParams.toString(0)
        res += self.stars.toString(1)
        return indentText(res,indention)

class PlayerDesignRef(co_attribs.FileLocalRef):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        if not isinstance(self.parent,Galaxy):
            raise RuntimeError("PlayerDesignRef parent must be an instance of "
                               +"Galaxy.")

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            if not (hasattr(self.parent.cartography,"players") and
                    self.ref in self.parent.cartography.players):
                res.append(co_probs.MissingRefProblem("Scenario Player",self.ref,
                        probLine=self.linenum))
        return res

class FixedPlanet(FixedOrbitBody):
    def __init__(self,parent,owner,isHome,normalUpg,quickUpg,items,spawnProb,
            useDefTemplate,entityCount,asteroidCount,**kwargs):
        super().__init__(bodyType="Planet",
                fileVersion=parent.version.value,
                **kwargs)
        self.owner = PlayerDesignRef(parent=parent,**owner)
        self.isHome = co_attribs.AttribBool(**isHome)
        self.normalUpg = HomeUpgrades(**normalUpg)
        self.quickUpg = HomeUpgrades(**quickUpg)
        self.items = ScenarioTemplate(parent=parent,**items)
        self.spawnProb = co_attribs.AttribNum(**spawnProb)
        self.useDefTemplate = co_attribs.AttribBool(**useDefTemplate)
        self.entityCount = co_attribs.AttribNum(**entityCount)
        self.asteroidCount = co_attribs.AttribNum(**asteroidCount)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.owner==other.owner)
                        and (self.isHome==other.isHome)
                        and (self.normalUpg==other.normalUpg)
                        and (self.quickUpg==other.quickUpg)
                        and (self.items==other.items)
                        and (self.spawnProb==other.spawnProb)
                        and (self.useDefTemplate==other.useDefTemplate)
                        and (self.entityCount==other.entityCount)
                        and (self.asteroidCount==other.asteroidCount))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.designName.toString(1)
        res += self.ingameName.toString(1)
        res += self.type.toString(1)
        res += self.pos.toString(1)
        if self.fileVersion==4:
            res += self.moveRadius.toString(1)
            res += self.hyperRadius.toString(1)
        res += self.owner.toString(1)
        res += self.isHome.toString(1)
        res += self.normalUpg.toString(1)
        res += self.quickUpg.toString(1)
        res += self.items.toString(1)
        res += self.spawnProb.toString(1)
        res += self.useDefTemplate.toString(1)
        res += self.entityCount.toString(1)
        res += self.asteroidCount.toString(1)
        return indentText(res,indention)

class PlanetConnection(co_attribs.Attribute):
    _outOfBoundsMsg = ("Index out of bounds: {}={} is larger than {}, the "
                       "number of planets-1 at the star {}.")

    def __init__(self,star,indexA,indexB,spawnProb,connType,**kwargs):
        super().__init__(**kwargs)
        self.star = star
        self.indexA = co_attribs.AttribNum(**indexA)
        self.indexB = co_attribs.AttribNum(**indexB)
        self.spawnProb = co_attribs.AttribNum(**spawnProb)
        self.connType = co_attribs.AttribEnum(possibleVals=CONNECTION_TYPES,**connType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.indexA==self.indexA)
                        and (self.indexB==other.indexB)
                        and (self.spawnProb==other.spawnProb)
                        and (self.connType==other.connType))

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive=False)
        if self.indexA.value==self.indexB.value:
            res.append(co_probs.BasicProblem("planetIndexA==planetIndexB, planet "
                                             +"connected to itself in star "
                                             +self.star.designName.value+".",
                    severity=co_probs.ProblemSeverity.warn,
                    probLine=self.indexA.linenum))
        maxIndex = self.star.planets.counter-1
        if self.indexA.value>maxIndex:
            msg = self._outOfBoundsMsg.format(self.indexA.identifier,
                    self.indexA.value,maxIndex,self.star.designName.value)
            res.append(co_probs.BasicProblem(msg,
                    severity=co_probs.ProblemSeverity.error,
                    probLine=self.indexA.linenum))
        if self.indexB.value>maxIndex:
            msg = self._outOfBoundsMsg.format(self.indexB.identifier,
                    self.indexB.value,maxIndex,self.star.designName.value)
            res.append(co_probs.BasicProblem(msg,
                    severity=co_probs.ProblemSeverity.error,
                    probLine=self.indexB.linenum))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.indexA.toString(1)
        res += self.indexB.toString(1)
        res += self.spawnProb.toString(1)
        res += self.connType.toString(1)
        return indentText(res,indention)

class StarConnection(co_attribs.Attribute):
    _selfConnMsg = "Invalid self connection: starIndexA={} == starIndexB={}"
    _starOutOfBoundsMsg = ("Index out of bounds: {}={} is larger than {}, the "
                           "number of stars-1.")
    _planetOutOfBoundsMsg = (
                             "Index out of bounds: {}={} is larger than {}, "
                             "the number of planets-1 at the star {}.")

    def __init__(self,parent,starA,planetA,starB,planetB,spawnProb,connType,
            **kwargs):
        super().__init__(**kwargs)
        self.parent = parent
        self.starA = co_attribs.AttribNum(**starA)
        self.planetA = co_attribs.AttribNum(**planetA)
        self.starB = co_attribs.AttribNum(**starB)
        self.planetB = co_attribs.AttribNum(**planetB)
        self.spawnProb = co_attribs.AttribNum(**spawnProb)
        self.connType = co_attribs.AttribEnum(possibleVals=CONNECTION_TYPES,**connType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.starA==other.starA)
                        and (self.planetA==other.planetA)
                        and (self.starB==other.starB)
                        and (self.planetB==other.planetB)
                        and (self.spawnProb==other.spawnProb)
                        and (self.connType==other.connType))

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.starA.value==self.starB.value:
            res.append(co_probs.BasicProblem(self._selfConnMsg.format(
                    self.starA.value,self.starB.value),
                    severity=co_probs.ProblemSeverity.error,
                    probLine=self.starA.linenum))
        maxIndex = self.parent.cartography.stars.counter-1
        if self.starA.value>maxIndex:
            res.append(co_probs.BasicProblem(self._starOutOfBoundsMsg.format(
                    self.starA.identifier,self.starA.value,maxIndex),
                    severity=co_probs.ProblemSeverity.error,
                    probLine=self.starA.linenum))
        else:
            star = self.parent.cartography.stars.elements[self.starA.value]
            maxPlanetIndex = star.planets.counter-1
            if self.planetA.value>maxPlanetIndex:
                msg = self._planetOutOfBoundsMsg.format(self.planetA.identifier,
                        self.planetA.value,maxPlanetIndex,
                        star.designName.value)
                res.append(co_probs.BasicProblem(msg,
                        severity=co_probs.ProblemSeverity.error,
                        probLine=self.planetA.linenum))
        if self.starB.value>maxIndex:
            res.append(co_probs.BasicProblem(self._starOutOfBoundsMsg.format(
                    self.starB.identifier,self.starB.value,maxIndex),
                    severity=co_probs.ProblemSeverity.error,
                    probLine=self.starB.linenum))
        else:
            star = self.parent.cartography.stars.elements[self.starB.value]
            maxPlanetIndex = star.planets.counter-1
            if self.planetB.value>maxPlanetIndex:
                msg = self._planetOutOfBoundsMsg.format(self.planetB.identifier,
                        self.planetB.value,maxPlanetIndex,
                        star.designName.value)
                res.append(co_probs.BasicProblem(msg,
                        severity=co_probs.ProblemSeverity.error,
                        probLine=self.planetB.linenum))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.starA.toString(1)
        res += self.planetA.toString(1)
        res += self.starB.toString(1)
        res += self.planetB.toString(1)
        res += self.spawnProb.toString(1)
        res += self.connType.toString(1)
        return indentText(res,indention)

class FixedStar(FixedOrbitBody):
    def __init__(self,parent,radius,planets,connections,entityCount,spawnProb,
            **kwargs):
        super().__init__(bodyType="Star",fileVersion=parent.version.value,
                **kwargs)
        self._planets = co_attribs.AttribList(elemType=FixedPlanet,
                elemArgs={"parent":parent},**planets)
        self.radius = co_attribs.AttribNum(**radius)
        self.connections = co_attribs.AttribList(elemType=PlanetConnection,
                elemArgs={"star":self},**connections)
        self.entityCount = co_attribs.AttribNum(**entityCount)
        self.spawnProb = co_attribs.AttribNum(**spawnProb)

    @property
    def planets(self):
        return self._planets

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self._planets==other.planets)
                        and (self.radius==other.radius)
                        and (self.connections==other.connections)
                        and (self.entityCount==other.entityCount)
                        and (self.spawnProb==other.spawnProb))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.designName.toString(1)
        res += self.ingameName.toString(1)
        res += self.type.toString(1)
        res += self.pos.toString(1)
        res += self.radius.toString(1)
        if self.fileVersion==4:
            res += self.moveRadius.toString(1)
            res += self.hyperRadius.toString(1)
        res += self.planets.toString(1)
        res += self.connections.toString(1)
        res += self.entityCount.toString(1)
        res += self.spawnProb.toString(1)
        return indentText(res,indention)

class FixedCartography(Cartography,co_basics.HasReference):
    def __init__(self,parent,galWidth,galHeight,nextStarNameId,nextPlanetNameId,
            triggerCount,stars,starConnections,players,templates,**kwargs):
        super().__init__(**kwargs)
        self.galWidth = co_attribs.AttribNum(**galWidth)
        self.galHeight = co_attribs.AttribNum(**galHeight)
        self.nextStarNameId = co_attribs.AttribNum(**nextStarNameId)
        self.nextPlanetNameId = co_attribs.AttribNum(**nextPlanetNameId)
        self.triggerCount = co_attribs.AttribNum(**triggerCount)
        self._stars = co_attribs.AttribList(
                elemType=FixedStar,
                elemArgs={"parent":parent},
                **stars)
        self.starConnections = co_attribs.AttribList(
                elemType=StarConnection,
                elemArgs={"parent":parent},
                **starConnections)
        self.players = co_attribs.AttribListKeyed(
                elemType=ScenarioPlayer,
                keyfunc=lambda val:val.name.value,
                **players)
        self.templates = co_attribs.AttribList(
                elemType=ScenarioTemplate,
                elemArgs={"parent":parent},
                **templates)

    @property
    def stars(self):
        return self._stars

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.galWidth==other.galWidth)
                        and (self.galHeight==other.galHeight)
                        and (self.nextStarNameId==other.nextStarNameId)
                        and (self.nextPlanetNameId==other.nextPlanetNameId)
                        and (self.triggerCount==other.triggerCount)
                        and (self.stars==other.stars)
                        and (self.starConnections==other.starConnections)
                        and (self.players==other.players)
                        and (self.templates==other.templates))

    def toString(self,indention):
        res = super().toString(0)
        res += self.galWidth.toString(0)
        res += self.galHeight.toString(0)
        res += self.nextStarNameId.toString(0)
        res += self.nextPlanetNameId.toString(0)
        res += self.triggerCount.toString(0)
        res += self.stars.toString(0)
        res += self.starConnections.toString(0)
        res += self.players.toString(0)
        res += self.templates.toString(0)
        return indentText(res,indention)

"""############################## File Classes #############################"""

class GalaxyScenarioDef(co_basics.IsFile,co_basics.HasReference):
    _allowedPlanetItems = ["CapitalShip","Frigate","PlanetBonus",
        "PlanetModuleHangarDefense","PlanetModuleRefinery",
        "PlanetModuleShipFactory","PlanetModuleStandard",
        "PlanetModuleTradePort","PlanetModuleWeaponDefense",
        "ResourceAsteroid","SpaceMine","Squad","StarBase","Titan",
        "ResearchSubject"]
    parser = g_GalaxyScenarioDef

    def __init__(self,starTypes,planetTypes,orbitBodyTypes,planetItems,
            players,planetTemplates,validPictures,validThemes,autoPlayers,
            autoMilitiaPerGalaxy,autoMilitiaPlayers,**kwargs):
        super().__init__(**kwargs)
        self._starTypes = co_attribs.AttribListKeyed(
                elemType=OrbitBodyDesign,
                elemArgs={"parent":self},
                keyfunc=lambda val:val.name.value,
                **starTypes)
        self._planetTypes = co_attribs.AttribListKeyed(
                elemType=OrbitBodyDesign,
                elemArgs={"parent":self},
                keyfunc=lambda val:val.name.value,
                **planetTypes)
        self._orbitBodyTypes = co_attribs.AttribListKeyed(
                elemType=OrbitBodyType,
                keyfunc=lambda val:val.name.value,
                **orbitBodyTypes
        )
        self._planetItems = co_attribs.AttribListKeyed(
                elemType=EntityDesign,
                elemArgs={"entTypes":self._allowedPlanetItems},
                keyfunc=lambda val:val.name.value,
                **planetItems
        )
        self.players = co_attribs.AttribListKeyed(
                elemType=EntityDesign,
                elemArgs={"entTypes":["Player"]},
                keyfunc=lambda val:val.name.value,
                **players
        )
        self._planetTemplates = co_attribs.AttribListKeyed(
                elemType=ScenarioTemplate,
                keyfunc=lambda val:val.name.value,
                **planetTemplates
        )
        self.validPictures = co_attribs.AttribList(
                elemType=themes.PlayerPictureRef,
                **validPictures)
        self.validThemes = co_attribs.AttribList(
                elemType=themes.PlayerThemeRef,
                **validThemes
        )
        self.autoPlayers = co_attribs.AttribList(
                elemType=ScenarioPlayer,
                **autoPlayers
        )
        self.autoMilitiaPerGalaxy = co_attribs.AttribNum(**autoMilitiaPerGalaxy)
        self.autoMilitiaPlayers = co_attribs.AttribList(
                elemType=ScenarioPlayer,
                **autoMilitiaPlayers
        )

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.starTypes==other.starTypes)
                        and (self.planetTypes==other.planetTypes)
                        and (self.orbitBodyTypes==other.orbitBodyTypes)
                        and (self.planetItems==other.planetItems)
                        and (self.players==other.players)
                        and (self.planetTemplates==other.planetTemplates)
                        and (self.validPictures==other.validPictures)
                        and (self.validThemes==other.validThemes)
                        and (self.autoPlayers==other.autoPlayers)
                        and (
                        self.autoMilitiaPerGalaxy==other.autoMilitiaPerGalaxy)
                        and (self.autoMilitiaPlayers==other.autoMilitiaPlayers))

    def toString(self,indention):
        res = super().toString(0)
        res += self.starTypes.toString(0)
        res += self.planetTypes.toString(0)
        res += self.orbitBodyTypes.toString(0)
        res += self.planetItems.toString(0)
        res += self.players.toString(0)
        res += self.planetTemplates.toString(0)
        res += self.validPictures.toString(0)
        res += self.validThemes.toString(0)
        res += self.autoPlayers.toString(0)
        res += self.autoMilitiaPerGalaxy.toString(0)
        res += self.autoMilitiaPlayers.toString(0)
        return indentText(res,indention)

    @property
    def starTypes(self):
        return self._starTypes

    @property
    def planetTypes(self):
        return self._planetTypes

    @property
    def orbitBodyTypes(self):
        return self._orbitBodyTypes

    @property
    def planetItems(self):
        return self._planetItems

    @property
    def planetTemplates(self):
        return self._planetTemplates

    def checkTypeOrbitBodyType(self,currMod,name,expType):
        if name in self.orbitBodyTypes:
            res = self.orbitBodyTypes.getElement(name)
            return res.entityDef.checkType([expType],currMod)
        else:
            return False

class Galaxy(co_basics.IsFile,co_basics.HasReference):
    parser = g_Galaxy

    def __init__(self,version,browsable,picName,browseName,browseDesc,
            firstCapitalIsFlagship,randStartPos,artifactDensity,bonusDensity,
            homeUpgradesNormal,homeUpgradesQuick,recommGameTypes,meterPerUnit,
            meterPerPixel,cartography,**kwargs):
        super().__init__(**kwargs)
        self.version = co_attribs.AttribNum(**version)
        self.browsable = co_attribs.AttribBool(**browsable)
        self.picName = ui.UITextureRef(**picName)
        self.browseName = co_attribs.AttribString(**browseName)
        self.browseDesc = co_attribs.AttribString(**browseDesc)
        self.firstCapitalIsFlagship = co_attribs.AttribBool(**firstCapitalIsFlagship)
        self.randStartPos = co_attribs.AttribBool(**randStartPos)
        self.artifactDensity = co_attribs.AttribNum(**artifactDensity)
        self.bonusDensity = co_attribs.AttribNum(**bonusDensity)
        self.homeUpgradesNormal = HomeUpgrades(**homeUpgradesNormal)
        self.homeUpgradesQuick = HomeUpgrades(**homeUpgradesQuick)
        self.recommGameTypes = co_attribs.AttribList(
                elemType=co_attribs.AttribEnum,
                elemArgs={"possibleVals":GAME_TYPES},
                **recommGameTypes
        )
        self.meterPerUnit = co_attribs.AttribNum(**meterPerUnit)
        self.meterPerPixel = co_attribs.AttribNum(**meterPerPixel)
        self.cartography = Cartography.factory(parent=self,**cartography)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.version==other.version)
                        and (self.browsable==other.browsable)
                        and (self.picName==other.picName)
                        and (self.browseName==other.browseName)
                        and (self.browseDesc==other.browseDesc)
                        and (
                        self.firstCapitalIsFlagship==other.firstCapitalIsFlagship)
                        and (self.randStartPos==other.randStartPos)
                        and (self.artifactDensity==other.artifactDensity)
                        and (self.bonusDensity==other.bonusDensity)
                        and (self.homeUpgradesNormal==other.homeUpgradesNormal)
                        and (self.homeUpgradesQuick==other.homeUpgradesQuick)
                        and (self.recommGameTypes==other.recommGameTypes)
                        and (self.meterPerUnit==other.meterPerUnit)
                        and (self.meterPerPixel==other.meterPerPixel)
                        and (self.cartography==other.cartography))

    def toString(self,indention):
        res = super().toString(0)
        res += self.version.toString(0)
        res += self.browsable.toString(0)
        res += self.picName.toString(0)
        res += self.browseName.toString(0)
        res += self.browseDesc.toString(0)
        res += self.firstCapitalIsFlagship.toString(0)
        res += self.randStartPos.toString(0)
        res += self.artifactDensity.toString(0)
        res += self.bonusDensity.toString(0)
        res += self.homeUpgradesNormal.toString(0)
        res += self.homeUpgradesQuick.toString(0)
        res += self.recommGameTypes.toString(0)
        res += self.meterPerUnit.toString(0)
        res += self.meterPerPixel.toString(0)
        res += self.cartography.toString(0)
        return indentText(res,indention)

"""################################ Component ##############################"""

class ModScenarios(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"Galaxy","galaxy")
        self._manifest = None
        self._galDef = None

    @property
    def galDef(self):
        if self._galDef is None:
            inst,probs = self.loadFile("GameInfo",
                    "GalaxyScenarioDef.galaxyScenarioDef",GalaxyScenarioDef)
            if inst is None:
                self._galDef = probs
            else:
                self._galDef = inst
        return self._galDef

    @property
    def manifest(self):
        if self._manifest is None:
            import pysoase.mod.manifest as manifest
            inst,probs = self.loadFile("","galaxy.manifest",
                    manifest.ManifestGalaxy)
            if inst is None:
                self._manifest = probs
            else:
                self._manifest = inst
        return self._manifest

    def check(self,strict=False):
        probs = []
        if strict and not isinstance(self.manifest,list):
            with tqdm.tqdm(disable=self.mod.quiet,
                    total=len(self.manifest.entries.elements)+2,
                    desc="Checking Galaxies") as pbar:
                if isinstance(self.galDef,list):
                    probs.extend(self.galDef)
                else:
                    probs.extend(self.galDef.check(self.mod))
                pbar.update()
                if isinstance(self.manifest,list):
                    probs.extend(self.manifest)
                else:
                    probs.extend(self.manifest.check(self.mod))
                pbar.update()
                for galaxy in self.manifest.entries.elements:
                    probs.extend(self.checkGalaxy(galaxy.ref))
                    pbar.update()
        else:
            with tqdm.tqdm(disable=self.mod.quiet,
                    total=len(self.compFilesMod)+2,
                    desc="Checking Galaxies"
            ) as pbar:
                if isinstance(self.galDef,list):
                    probs.extend(self.galDef)
                else:
                    probs.extend(self.galDef.check(self.mod))
                pbar.update()
                if isinstance(self.manifest,list):
                    probs.extend(self.manifest)
                else:
                    probs.extend(self.manifest.check(self.mod))
                pbar.update()
                for galaxy in self.compFilesMod:
                    probs.extend(self.checkGalaxy(galaxy))
                    pbar.update()
        return probs

    def checkFile(self,filepath):
        res = super().checkFile(filepath)
        checkInst = None
        if os.path.basename(filepath)=="galaxy.manifest":
            checkInst = self.manifest
        elif os.path.basename(filepath)=="GalaxyScenarioDef.galaxyScenarioDef":
            checkInst = self.galDef
        else:
            name,ext = os.path.splitext(filepath)
            if ext==".galaxy":
                checkInst = self.checkGalaxy(os.path.basename(filepath))
            else:
                raise RuntimeError("Files of type "+filepath
                                   +"are not handled by ModScenarios.")
        if isinstance(checkInst,list):
            # There was a parsing problem with the given file, return it
            res.extend(checkInst)
        else:
            # There was no problem with the parsing, conduct the check
            res.extend(checkInst.check(self.mod))
        return res

    def checkGalaxy(self,name):
        probs = self.loadGalaxy(name)
        if not isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name].check(self.mod))
        if isinstance(self.manifest,list):
            probs.extend(self.manifest)
        else:
            probs.extend(self.manifest.checkEntry(name))
        return probs

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def loadGalaxy(self,name):
        probs = []
        if name not in self.compFilesMod or self.compFilesMod[name] is None:
            inst,probs = self.loadFile(self.subName,name,Galaxy)
            if inst is None:
                self.compFilesMod[name] = probs
            else:
                self.compFilesMod[name] = inst
        elif isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name])
        return probs

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Loading Galaxies"
        ) as pbar:
            for galaxy in self.compFilesMod:
                probs.extend(self.loadGalaxy(galaxy))
                pbar.update()
        return probs
