""" Contains parsers and classes for audio files
"""
import ntpath
import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
import pysoase.common.constants as co_consts
import pysoase.common.filehandling as co_files
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs

"""############################### Parsers #################################"""

EFFECT_TYPES = ["Dialogue","Effect","UI"]

"""### Attributes ###"""
g_soundEffectContent = (
    co_parse.genStringAttrib("name")("name")
    +co_parse.genStringAttrib("fileName")("soundfile")
)
g_soundDialogue = pp.Group(
        co_parse.genHeading("effect")
        +g_soundEffectContent
)
g_soundMusic = pp.Group(
        co_parse.genHeading("music")
        +g_soundEffectContent
        +co_parse.genBoolAttrib("isLooping")("looping")
        +co_parse.genDecimalAttrib("actionLevel")("actionLvl")
        +co_parse.genDecimalAttrib("actionRange")("actionRange")
        +co_parse.genDecimalAttrib("emotionLevel")("emotionLvl")
        +co_parse.genDecimalAttrib("emotionRange")("emotionRange")
)

g_soundEffect = pp.Group(
        co_parse.genHeading("effect")
        +g_soundEffectContent
        +co_parse.genEnumAttrib("type",EFFECT_TYPES)("effType")
        +co_parse.genBoolAttrib("is3D")("is3d")
        +co_parse.genIntAttrib("priority")("priority")
        +co_parse.genDecimalAttrib("threshold")("threshold")
        +co_parse.genDecimalAttrib("minAttenuationDist")("minAttenuationDist")
        +co_parse.genIntAttrib("maxNumPlayingSimultaneously")("maxParallelPlaying")
        +co_parse.genBoolAttrib("isLooping")("looping")
        +co_parse.genBoolAttrib("isResident")("resident")
        +co_parse.genDecimalAttrib("minRespawnTime")("respawnTimeMin")
        +co_parse.genDecimalAttrib("fadeInTime")("fadeInTime")
        +co_parse.genDecimalAttrib("fadeOutTime")("fadeOutTime")
        +co_parse.genStringAttrib("alternateGroup")("alternateGroup")
)

"""### Files ###"""
g_DialogueData = (
    co_parse.g_txt
    +co_parse.genListAttrib("numEffects",g_soundDialogue)("dialogue")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_MusicData = (
    co_parse.g_txt
    +co_parse.genListAttrib("numMusic",g_soundMusic)("music")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_EffectData = (
    co_parse.g_txt
    +co_parse.genListAttrib("numEffects",g_soundEffect)("effects")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################## Attributes ###############################"""

class AudioFileRef(co_attribs.FileRef):
    _REFTYPE = "Audio File"

    def __init__(self,identifier,val,**kwargs):
        self._vanilla = val
        self.subdir = "Sound"
        self.extensions = [".ogg",".wav"]
        name,ext = os.path.splitext(val)
        if not ext:
            # If file has no extension, default to ogg
            val = ".".join([name,"ogg"])
        co_attribs.AttribRef.__init__(self,identifier=identifier,
                val=ntpath.basename(val),
                **kwargs)

    def checkExistence(self,currMod,recursive=False):
        res = []
        fsourceBest = -1
        name,ext = os.path.splitext(self.ref)
        for ext in self.extensions:
            fsource,probs = co_files.checkFileExistence(self.subdir,name+ext,
                    currMod)
            if fsource>fsourceBest:
                res = probs
                fsourceBest = fsource
        return res

class SoundRef(co_attribs.AttribRef):
    _REFTYPE = "Sound Effect"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(currMod.audio.checkEffect(self.ref,recursive))
            for prob in res:
                if not prob.probLine:
                    prob.probLine = self.linenum
        return res

class DialogueRef(co_attribs.AttribRef):
    _REFTYPE = "Sound Dialogue"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(currMod.audio.checkEffect(self.ref,recursive))
            for prob in res:
                if not prob.probLine:
                    prob.probLine = self.linenum
        return res

class MusicRef(co_attribs.AttribRef):
    _REFTYPE = "Sound Music"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(currMod.audio.checkMusic(self.ref,recursive))
            for prob in res:
                if not prob.probLine:
                    prob.probLine = self.linenum
        return res

class SoundEffect(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,soundfile,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.soundfile = AudioFileRef(**soundfile)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.soundfile==other.soundfile))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.soundfile.toString(1)
        return indentText(res,indention)

class Music(SoundEffect):
    def __init__(self,looping,actionLvl,actionRange,emotionLvl,emotionRange,
            **kwargs):
        super().__init__(**kwargs)
        self.looping = co_attribs.AttribBool(**looping)
        self.actionLvl = co_attribs.AttribNum(**actionLvl)
        self.actionRange = co_attribs.AttribNum(**actionRange)
        self.emotionLvl = co_attribs.AttribNum(**emotionLvl)
        self.emotionRange = co_attribs.AttribNum(**emotionRange)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.looping==other.looping)
                        and (self.actionLvl==other.actionLvl)
                        and (self.actionRange==other.actionRange)
                        and (self.emotionLvl==other.emotionLvl)
                        and (self.emotionRange==other.emotionRange))

    def toString(self,indention):
        res = super().toString(0)
        res += self.looping.toString(1)
        res += self.actionLvl.toString(1)
        res += self.actionRange.toString(1)
        res += self.emotionLvl.toString(1)
        res += self.emotionRange.toString(1)
        return indentText(res,indention)

class Effect(SoundEffect):
    def __init__(self,effType,is3d,priority,threshold,minAttenuationDist,
            maxParallelPlaying,looping,resident,respawnTimeMin,fadeInTime,
            fadeOutTime,alternateGroup,**kwargs):
        super().__init__(**kwargs)
        self.effType = co_attribs.AttribEnum(possibleVals=EFFECT_TYPES,**effType)
        self.is3d = co_attribs.AttribBool(**is3d)
        self.priority = co_attribs.AttribNum(**priority)
        self.threshold = co_attribs.AttribNum(**threshold)
        self.minAttenuationDist = co_attribs.AttribNum(**minAttenuationDist)
        self.maxParallelPlaying = co_attribs.AttribNum(**maxParallelPlaying)
        self.looping = co_attribs.AttribBool(**looping)
        self.resident = co_attribs.AttribBool(**resident)
        self.respawnTimeMin = co_attribs.AttribNum(**respawnTimeMin)
        self.fadeInTime = co_attribs.AttribNum(**fadeInTime)
        self.fadeOutTime = co_attribs.AttribNum(**fadeOutTime)
        self.alternateGroup = SoundRef(**alternateGroup)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.effType==other.effType)
                        and (self.is3d==other.is3d)
                        and (self.priority==other.priority)
                        and (self.threshold==other.threshold)
                        and (self.minAttenuationDist==other.minAttenuationDist)
                        and (self.maxParallelPlaying==other.maxParallelPlaying)
                        and (self.looping==other.looping)
                        and (self.resident==other.resident)
                        and (self.respawnTimeMin==other.respawnTimeMin)
                        and (self.fadeInTime==other.fadeInTime)
                        and (self.fadeOutTime==other.fadeOutTime)
                        and (self.alternateGroup==other.alternateGroup))

    def toString(self,indention):
        res = super().toString(0)
        res += self.effType.toString(1)
        res += self.is3d.toString(1)
        res += self.priority.toString(1)
        res += self.threshold.toString(1)
        res += self.minAttenuationDist.toString(1)
        res += self.maxParallelPlaying.toString(1)
        res += self.looping.toString(1)
        res += self.resident.toString(1)
        res += self.respawnTimeMin.toString(1)
        res += self.fadeInTime.toString(1)
        res += self.fadeOutTime.toString(1)
        res += self.alternateGroup.toString(1)
        return indentText(res,indention)

"""################################# Files #################################"""

class DialogueData(co_basics.IsFile,co_basics.HasReference):
    parser = g_DialogueData

    def __init__(self,dialogue,**kwargs):
        super().__init__(**kwargs)
        self.dialogue = co_attribs.AttribListKeyed(
                elemType=SoundEffect,
                keyfunc=lambda val:val.name.value,
                **dialogue)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.dialogue==other.dialogue)

    def checkEntry(self,name,recursive=False):
        return name in self.dialogue

    def gatherRefs(self,res,types=set()):
        if not types or "audiofile" in types:
            for entry in self.dialogue.elements:
                res["audiofile"].add(entry.soundfile.ref)
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.dialogue.toString(0)
        return indentText(res,indention)

class MusicData(co_basics.IsFile,co_basics.HasReference):
    parser = g_MusicData

    def __init__(self,music,**kwargs):
        super().__init__(**kwargs)
        self.music = co_attribs.AttribListKeyed(
                elemType=Music,
                keyfunc=lambda val:val.name.value,
                **music
        )

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.music==other.music)

    def checkEntry(self,name,recursive=False):
        return name in self.music

    def gatherRefs(self,res,types=set()):
        if not types or "audiofile" in types:
            for entry in self.music.elements:
                res["audiofile"].add(entry.soundfile.ref)
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.music.toString(0)
        return indentText(res,indention)

class EffectData(co_basics.IsFile,co_basics.HasReference):
    parser = g_EffectData

    def __init__(self,effects,**kwargs):
        super().__init__(**kwargs)
        self.effects = co_attribs.AttribListKeyed(
                elemType=Effect,
                keyfunc=lambda val:val.name.value,
                **effects
        )

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.effects==other.effects)

    def checkEntry(self,name,recursive=False):
        return name in self.effects

    def gatherRefs(self,res,types=set()):
        if not types or "audiofile" in types:
            for entry in self.effects.elements:
                res["audiofile"].add(entry.soundfile.ref)

    def toString(self,indention):
        res = super().toString(0)
        res += self.effects.toString(0)
        return indentText(res,indention)

class ModAudio(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"Sound","")
        self._dialogue = None
        self._effects = None
        self._music = None

    @property
    def dialogue(self):
        if self._dialogue is None:
            inst,probs = self.loadFile("GameInfo",
                    "SoundDialogue.sounddata",
                    DialogueData)
            if inst is None:
                self._dialogue = probs
            else:
                self._dialogue = inst
        return self._dialogue

    @property
    def effects(self):
        if self._effects is None:
            inst,probs = self.loadFile("GameInfo",
                    "SoundEffects.sounddata",
                    EffectData)
            if inst is None:
                self._effects = probs
            else:
                self._effects = inst
        return self._effects

    @property
    def music(self):
        if self._music is None:
            inst,probs = self.loadFile("GameInfo",
                    "SoundMusic.sounddata",
                    MusicData)
            if inst is None:
                self._music = probs
            else:
                self._music = inst
        return self._music

    def check(self,strict=False):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=3,
                desc="Checking Audio component") as pbar:
            if isinstance(self.dialogue,DialogueData):
                probs.extend(self.dialogue.check(self.mod))
            else:
                probs.extend(self.dialogue)
            pbar.update()

            if isinstance(self.effects,EffectData):
                probs.extend(self.effects.check(self.mod))
            else:
                probs.extend(self.effects)
            pbar.update()

            if isinstance(self.music,MusicData):
                probs.extend(self.music.check(self.mod))
            else:
                probs.extend(self.music)
            pbar.update()
        return probs

    def checkEffect(self,name,recursive=False):
        found = False
        res = []
        if isinstance(self.effects,EffectData):
            if self.effects.checkEntry(name,recursive):
                found = True
        else:
            res.extend(self.effects)
        if isinstance(self.dialogue,DialogueData):
            if self.dialogue.checkEntry(name,recursive):
                found = True
        else:
            res.extend(self.dialogue)
        if not found:
            res.append(co_probs.MissingRefProblem("Sound Effect",name))
        return res

    def checkMusic(self,name,recursive=False):
        if isinstance(self.music,MusicData):
            if not self.music.checkEntry(name,recursive):
                return [co_probs.MissingRefProblem("Sound Music",name)]
            else:
                return []
        else:
            return self.music

    def checkFile(self,filepath):
        res = super().checkFile(filepath)
        checkInst = None
        if os.path.basename(filepath)=="SoundDialogue.sounddata":
            checkInst = self.dialogue
        elif os.path.basename(filepath)=="SoundEffects.sounddata":
            checkInst = self.effects
        elif os.path.basename(filepath)=="SoundMusic.sounddata":
            checkInst = self.music
        else:
            raise RuntimeError("Files of type "+filepath
                               +"are not handled by ModAudio.")
        if isinstance(checkInst,list):
            # There was a parsing problem with the given file, return it
            res.extend(checkInst)
        else:
            # There was no problem with the parsing, conduct the check
            res.extend(checkInst.check(self.mod))
        return res

    def gatherFiles(self,res,endings=set()):
        if not endings or ".ogg" in endings:
            for f in os.listdir(os.path.join(self.mod.moddir,self.subName)):
                name,ext = os.path.splitext(os.path.basename(f))
                if ext==".ogg":
                    res[co_consts.EXT_TO_FILEREF[".ogg"]].add((name+ext))

    def gatherRefs(self,res,types=set()):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=3,
                desc="Gathering refs from Audio component") as pbar:
            if not types or "audiofile" in types:
                for sndfile in [self.dialogue,self.effects,self.music]:
                    if isinstance(sndfile,list):
                        probs.extend(sndfile)
                    else:
                        sndfile.gatherRefs(res,types)
                    pbar.update()
        return probs

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=3,
                desc="Loading audio files") as pbar:
            if not isinstance(self.dialogue,DialogueData):
                probs.extend(self.dialogue)
            pbar.update()
            if not isinstance(self.effects,EffectData):
                probs.extend(self.effects)
            pbar.update()
            if not isinstance(self.music,MusicData):
                probs.extend(self.music)
            pbar.update()
        return probs
