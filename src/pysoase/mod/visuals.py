""" Parsers and classes for miscallenous graphics related files.
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.audio as audio
import pysoase.mod.meshes as meshes
import pysoase.mod.particles as par
import pysoase.mod.ui as ui

"""################################# Types #################################"""

EFFECT_LOCATION = ["Center","Surface"]

"""################################ Parsers ################################"""
g_subExplosionBasic = (
    co_parse.genDecimalAttrib("playTime")("playTime")
    +co_parse.genDecimalAttrib("cameraShakeMaxSize")("camShakeSize")
    +co_parse.genDecimalAttrib("cameraShakeStrength")("camShakeStrength")
    +co_parse.genDecimalAttrib("cameraShakeDuration")("camShakeDuration")
)
g_explosionSounds = co_parse.genListAttrib("soundCount",
        co_parse.genStringAttrib("sound"),"explosionSounds")("sounds")
g_subExplosionEffect = (
    co_parse.genKeyValPair("type","Effect")("explosionType")
    +g_subExplosionBasic
    +co_parse.genEnumAttrib("location",EFFECT_LOCATION)("location")
    +co_parse.genStringAttrib("particleSystemName")("particleSys")
    +g_explosionSounds
)
g_subExplosionSound = (
    co_parse.genKeyValPair("type","Sound")("explosionType")
    +g_subExplosionBasic
    +g_explosionSounds
)
g_subExplosion = pp.Group(
        co_parse.genHeading("subExplosion")
        +pp.Or([g_subExplosionSound,g_subExplosionEffect])
)
g_explosionEffect = pp.Group(
        co_parse.genHeading("explosionEffectDef")
        +co_parse.genDecimalAttrib("totalPlayTime")("totalTime")
        +co_parse.genDecimalAttrib("makeMeshInvisibleTime")("meshInvisibleTime")
        +co_parse.genListAttrib("subExplosionCount",g_subExplosion)("subExplosions")
)
g_explosionEffectGroup = pp.Group(
        co_parse.genHeading("explosionEffectGroup")
        +co_parse.genStringAttrib("groupName")("name")
        +co_parse.genListAttrib("explosionEffectDefCount",g_explosionEffect)(
                "effects")
)
g_ExplosionData = (
    co_parse.g_txt
    +co_parse.genListAttrib("explosionEffectGroupCount",g_explosionEffectGroup)(
            "explosions")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

g_Starscape = (
    co_parse.g_txt
    +co_parse.genDecimalAttrib("starWidth")("starWidth")
    +co_parse.genDecimalAttrib("starHeight")("starHeight")
    +co_parse.genDecimalAttrib("starDistance")("starDist")
    +co_parse.genDecimalAttrib("starBlinksPerSecond")("starBlinksPerSec")
    +co_parse.genDecimalAttrib("starFadeDuration")("starFadeDuration")
    +pp.Group(
            co_parse.genStringAttrib("starTextureName:0")
            +co_parse.genStringAttrib("starTextureName:1")
            +co_parse.genStringAttrib("starTextureName:2")
            +co_parse.genStringAttrib("starTextureName:3")
            +co_parse.genStringAttrib("starTextureName:4")
            +co_parse.genStringAttrib("starTextureName:5")
            +co_parse.genStringAttrib("starTextureName:6")
            +co_parse.genStringAttrib("starTextureName:7")
            +co_parse.genStringAttrib("starTextureName:8")
            +co_parse.genStringAttrib("starTextureName:9")
    )("starTextures")
).ignore(pp.dblSlashComment)

g_skyboxProperties = pp.Group(
        co_parse.genHeading("properties")
        +co_parse.genStringAttrib("meshName")("mesh")
        +co_parse.genColorAttrib("diffuseLiteColor")("diffuseLiteCol")
        +co_parse.genColorAttrib("diffuseDarkColor")("diffuseDarkCol")
        +co_parse.genColorAttrib("ambientLiteColor")("ambientLiteCol")
        +co_parse.genColorAttrib("ambientDarkColor")("ambientDarkCol")
        +co_parse.genColorAttrib("specularColor")("specularColor")
        +co_parse.genColorAttrib("dustCloudColor")("dustColor")
        +co_parse.genListAttrib("numFarStarIconCloudTextureNames",
                co_parse.genStringAttrib("textureName"))("farStarTextures")
        +co_parse.genStringAttrib("environmentMapName")("environMap")
        +co_parse.genStringAttrib("environmentIlluminationMapName")(
                "environIllumMap")
)

g_SkyboxBackdrops = (
    co_parse.g_txt
    +co_parse.genListAttrib("numProperties",g_skyboxProperties)("properties")
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

class SubExplosion(co_attribs.Attribute):
    _expTypes = ["Effect","Sound"]

    def __init__(self,explosionType,playTime,camShakeSize,camShakeStrength,
            camShakeDuration,**kwargs):
        super().__init__(**kwargs)
        self.explosionType = co_attribs.AttribEnum(possibleVals=self._expTypes,
                **explosionType)
        self.playTime = co_attribs.AttribNum(**playTime)
        self.camShakeSize = co_attribs.AttribNum(**camShakeSize)
        self.camShakeStrength = co_attribs.AttribNum(**camShakeStrength)
        self.camShakeDuration = co_attribs.AttribNum(**camShakeDuration)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.explosionType==other.explosionType)
                        and (self.playTime==other.playTime)
                        and (self.camShakeSize==other.camShakeSize)
                        and (self.camShakeStrength==other.camShakeStrength)
                        and (self.camShakeDuration==other.camShakeDuration))

    @classmethod
    def factory(cls,explosionType,**kwargs):
        if explosionType["val"]=="Effect":
            return EffectSubExplosion(explosionType=explosionType,**kwargs)
        elif explosionType["val"]=="Sound":
            return SoundSubExplosion(explosionType=explosionType,**kwargs)
        else:
            raise RuntimeError("Unknown explosion type "+explosionType["val"]
                               +". Possible types are:"+str(cls._expTypes))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.explosionType.toString(1)
        res += self.playTime.toString(1)
        res += self.camShakeSize.toString(1)
        res += self.camShakeStrength.toString(1)
        res += self.camShakeDuration.toString(1)
        return indentText(res,indention)

class SoundSubExplosion(SubExplosion,co_basics.HasReference):
    def __init__(self,sounds,**kwargs):
        super().__init__(**kwargs)
        self.sounds = co_attribs.AttribList(elemType=audio.SoundRef,**sounds)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.sounds==other.sounds)

    def toString(self,indention):
        res = super().toString(0)
        res += self.sounds.toString(1)
        return indentText(res,indention)

class EffectSubExplosion(SubExplosion,co_basics.HasReference):
    def __init__(self,location,particleSys,sounds,**kwargs):
        super().__init__(**kwargs)
        self.location = co_attribs.AttribEnum(possibleVals=EFFECT_LOCATION,**location)
        self.particleSys = par.ParticleRef(**particleSys)
        self.sounds = co_attribs.AttribList(elemType=audio.SoundRef,**sounds)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.location==other.location)
                        and (self.particleSys==other.particleSys)
                        and (self.sounds==other.sounds))

    def toString(self,indention):
        res = super().toString(0)
        res += self.location.toString(1)
        res += self.particleSys.toString(1)
        res += self.sounds.toString(1)
        return indentText(res,indention)

class ExplosionEffect(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,totalTime,meshInvisibleTime,subExplosions,**kwargs):
        super().__init__(**kwargs)
        self.totalTime = co_attribs.AttribNum(**totalTime)
        self.meshInvisibleTime = co_attribs.AttribNum(**meshInvisibleTime)
        self.subExplosions = co_attribs.AttribList(elemType=SubExplosion,factory=True,
                **subExplosions)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.totalTime==other.totalTime)
                        and (self.meshInvisibleTime==other.meshInvisibleTime)
                        and (self.subExplosions==other.subExplosions))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.totalTime.toString(1)
        res += self.meshInvisibleTime.toString(1)
        res += self.subExplosions.toString(1)
        return indentText(res,indention)

class ExplosionEffectGroup(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,effects,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.effects = co_attribs.AttribList(elemType=ExplosionEffect,**effects)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.effects==other.effects))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.effects.toString(1)
        return indentText(res,indention)

class ExplosionRef(co_attribs.AttribRef):
    _REFTYPE = "Explosion"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref and not isinstance(currMod.explosions,list):
            if not currMod.explosions.checkExplosionExistence(self.ref):
                res.append(co_probs.MissingRefProblem("Explosion",self.ref,
                        probLine=self.linenum))
        return res

class SkyboxProperties(
        co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,mesh,diffuseLiteCol,diffuseDarkCol,ambientLiteCol,
            ambientDarkCol,specularColor,dustColor,farStarTextures,
            environMap,environIllumMap,**kwargs):
        super().__init__(**kwargs)
        self.mesh = meshes.MeshRef(**mesh)
        self.diffuseLiteCol = co_attribs.AttribColor(**diffuseLiteCol)
        self.diffuseDarkCol = co_attribs.AttribColor(**diffuseDarkCol)
        self.ambientLiteCol = co_attribs.AttribColor(**ambientLiteCol)
        self.ambientDarkCol = co_attribs.AttribColor(**ambientDarkCol)
        self.specularColor = co_attribs.AttribColor(**specularColor)
        self.dustColor = co_attribs.AttribColor(**dustColor)
        self.farStarTextures = co_attribs.AttribList(elemType=ui.UITextureRef,
                **farStarTextures)
        self.environMap = ui.UITextureRef(**environMap)
        self.environIllumMap = ui.UITextureRef(**environIllumMap)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.mesh==other.mesh)
                        and (self.diffuseLiteCol==other.diffuseLiteCol)
                        and (self.diffuseDarkCol==other.diffuseDarkCol)
                        and (self.ambientLiteCol==other.ambientLiteCol)
                        and (self.ambientDarkCol==other.ambientDarkCol)
                        and (self.specularColor==other.specularColor)
                        and (self.dustColor==other.dustColor)
                        and (self.farStarTextures==other.farStarTextures)
                        and (self.environMap==other.environMap)
                        and (self.environIllumMap==other.environIllumMap))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.mesh.toString(1)
        res += self.diffuseLiteCol.toString(1)
        res += self.diffuseDarkCol.toString(1)
        res += self.ambientLiteCol.toString(1)
        res += self.ambientDarkCol.toString(1)
        res += self.specularColor.toString(1)
        res += self.dustColor.toString(1)
        res += self.farStarTextures.toString(1)
        res += self.environMap.toString(1)
        res += self.environIllumMap.toString(1)
        return indentText(res,indention)

"""############################## File Classes #############################"""

class Explosions(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_ExplosionData

    def __init__(self,explosions,**kwargs):
        super().__init__(**kwargs)
        self.explosions = co_attribs.AttribListKeyed(
                elemType=ExplosionEffectGroup,
                keyfunc=lambda val:val.name.value,
                **explosions
        )

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.explosions==other.explosions)

    def checkExplosionExistence(self,name):
        return name in self.explosions

    def toString(self,indention):
        res = super().toString(0)
        res += self.explosions.toString(0)
        return indentText(res,indention)

class Starscape(co_basics.IsFile,co_basics.HasReference):
    parser = g_Starscape

    def __init__(self,starWidth,starHeight,starDist,starBlinksPerSec,
            starFadeDuration,starTextures,**kwargs):
        super().__init__(**kwargs)
        self.starWidth = co_attribs.AttribNum(**starWidth)
        self.starHeight = co_attribs.AttribNum(**starHeight)
        self.starDist = co_attribs.AttribNum(**starDist)
        self.starBlinksPerSec = co_attribs.AttribNum(**starBlinksPerSec)
        self.starFadeDuration = co_attribs.AttribNum(**starFadeDuration)
        self.starTextures = tuple(map(
                lambda vals:ui.UITextureRef(canBeEmpty=True,**vals),
                starTextures))

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.starWidth==other.starWidth)
                        and (self.starHeight==other.starHeight)
                        and (self.starDist==other.starDist)
                        and (self.starBlinksPerSec==other.starBlinksPerSec)
                        and (self.starFadeDuration==other.starFadeDuration)
                        and (self.starTextures==other.starTextures))

    def gatherRefs(self,res,types=set()):
        if not types or "texture" in types:
            for t in self.starTextures:
                if t.ref:
                    res["texture"].add(t.ref)
        return res

    def getRefs(self):
        res = super().getRefs()
        res.extend([t for t in self.starTextures])
        return res

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for t in self.starTextures:
            res.extend(t.check(currMod,recursive))
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.starWidth.toString(0)
        res += self.starHeight.toString(0)
        res += self.starDist.toString(0)
        res += self.starBlinksPerSec.toString(0)
        res += self.starFadeDuration.toString(0)
        for t in self.starTextures:
            res += t.toString(0)
        return indentText(res,indention)

class SkyboxBackdrops(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_SkyboxBackdrops

    def __init__(self,properties,**kwargs):
        super().__init__(**kwargs)
        self.properties = co_attribs.AttribList(elemType=SkyboxProperties,**properties)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.properties==other.properties)

    def gatherRefs(self,res,types=set()):
        for prop in self.properties.elements:
            if not types or "texture" in types:
                res["texture"].add(prop.environMap.ref)
                res["texture"].add(prop.environIllumMap.ref)
                for t in prop.farStarTextures.elements:
                    res["texture"].add(t.ref)
            if not types or "mesh" in types:
                res["mesh"].add(prop.mesh.ref)
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.properties.toString(0)
        return indentText(res,indention)
