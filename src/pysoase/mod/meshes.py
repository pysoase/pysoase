""" Contains classes and parsers for handling mesh files
"""
import pysoase.common.attributes as co_attribs

"""############################## Attributes ###############################"""

class MeshRef(co_attribs.FileRef):
    _REFTYPE = "Mesh File"

    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier=identifier,val=val,subdir="Mesh",
                extension="mesh",**kwargs)
