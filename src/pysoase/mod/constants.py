""" Contains the parser, attributes and file class for the Gameplay.constants
"""

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.entities.common as coe

"""################################ Parsers ################################"""

"""### Spawn Ships ###"""

g_spawnRequiredShips = pp.Group(
        co_parse.genHeading("requiredShip")
        +co_parse.genStringAttrib("type")("shipType")
        +co_parse.genIntAttrib("minCount")("shipsMin")
        +co_parse.genIntAttrib("maxCount")("shipsMax")
)
g_spawnRandomShips = pp.Group(
        co_parse.genHeading("randomShip")
        +co_parse.genStringAttrib("type")("shipType")
        +co_parse.genDecimalAttrib("weight")("weight")
)
g_shipRequirements = pp.Group(
        co_parse.genListAttrib("requiredShipCount",g_spawnRequiredShips)(
                "requiredShips")
        +co_parse.genListAttrib("randomShipCount",g_spawnRandomShips)("randomShips")
)
g_spawnShips = pp.Group(
        co_parse.genHeading("spawnShips")
        +co_parse.genDecimalAttrib("minFleetPoints")("fleetPointsMin")
        +co_parse.genDecimalAttrib("maxFleetPoints")("fleetPointsMax")
        +g_shipRequirements("ships")
)

"""############################### Attributes ##############################"""

"""### Spawn Ships ###"""

class RequiredShips(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,shipType,shipsMin,shipsMax,**kwargs):
        super().__init__(**kwargs)
        self.shipType = coe.EntityRef(types=["Frigate","CapitalShip","Titan"],
                **shipType)
        self.shipsMin = co_attribs.AttribNum(**shipsMin)
        self.shipsMax = co_attribs.AttribNum(**shipsMax)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.shipType==other.shipType)
                        and (self.shipsMin==other.shipsMin)
                        and (self.shipsMax==other.shipsMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.shipType.toString(1)
        res += self.shipsMin.toString(1)
        res += self.shipsMax.toString(1)
        return indentText(res,indention)

class RandomShip(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,shipType,weight,**kwargs):
        super().__init__(**kwargs)
        self.shipType = coe.EntityRef(types=["Frigate","CapitalShip","Titan"],
                **shipType)
        self.weight = co_attribs.AttribNum(**weight)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.shipType==other.shipType)
                        and (self.weight==other.weight))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.shipType.toString(1)
        res += self.weight.toString(1)
        return indentText(res,indention)

class ShipComposition(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,requiredShips,randomShips,identifier="",**kwargs):
        super().__init__(identifier=identifier,**kwargs)
        self.requiredShips = co_attribs.AttribList(elemType=RequiredShips,
                **requiredShips)
        self.randomShips = co_attribs.AttribList(elemType=RandomShip,**randomShips)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.requiredShips==other.requiredShips)
                        and (self.randomShips==other.randomShips))

    def toString(self,indention):
        res = super().toString(0)
        lindent = 0
        if self.identifier:
            lindent += 1
            res += self.identifier+"\n"
        res += self.requiredShips.toString(lindent)
        res += self.randomShips.toString(lindent)
        return indentText(res,indention)

class ShipSpawn(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,fleetPointsMax,fleetPointsMin,ships,**kwargs):
        super().__init__(**kwargs)
        self.fleetPointsMin = co_attribs.AttribNum(**fleetPointsMin)
        self.fleetPointsMax = co_attribs.AttribNum(**fleetPointsMax)
        self.ships = ShipComposition(**ships)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.fleetPointsMin==other.fleetPointsMin)
                        and (self.fleetPointsMax==other.fleetPointsMax))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.fleetPointsMin.toString(1)
        res += self.fleetPointsMax.toString(1)
        res += self.ships.toString(1)
        return indentText(res,indention)

"""################################ Classes ################################"""

class Constants(co_basics.IsFile):
    def check(self,currMod,recrusive=False):
        pass
