""" Contains parsers and classes for particle files
"""

import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.mod.meshes as meshes
import pysoase.mod.texanim as anim
import pysoase.mod.ui as ui

TEXANIM_SPAWN_TYPES = ["FirstFrame","RandomFrames","SequentialFrames"]

"""################################ Parsers ################################"""

"""### General ###"""

g_3d = pp.Group(
        pp.Literal("[").suppress()
        +co_parse.g_space
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums+"."
        ).setParseAction(lambda t:float(t[0]))
        +co_parse.g_space
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums+"."
        ).setParseAction(lambda t:float(t[0]))
        +co_parse.g_space
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums+"."
        ).setParseAction(lambda t:float(t[0]))
        +co_parse.g_space
        +pp.Literal("]").suppress()
)

g_orientation = pp.Group(
        co_parse.genHeading("Orientation")
        +pp.Group(
                co_parse.g_linestart
                +g_3d
                +co_parse.g_eolNoAction
                +co_parse.g_linestart
                +g_3d
                +co_parse.g_eolNoAction
                +co_parse.g_linestart
                +g_3d
                +co_parse.g_eolNoAction
        )("vals")
)

def gen3DAttrib(keyword):
    return pp.Group(
            co_parse.g_linestart
            +pp.Keyword(keyword)("identifier")
            +co_parse.g_space
            +g_3d("val")
            +co_parse.g_eol
    )

"""### Affectors ###"""
g_affShared = (
    co_parse.genHeading("AffectorContents")
    +co_parse.genStringAttrib("Name")("name")
    +co_parse.genBoolAttrib("Enabled")("enabled")
    +co_parse.genDecimalAttrib("StartTime")("startTime")
    +co_parse.genBoolAttrib("HasInfiniteLifeTime")("lifetimeInfinite")
    +co_parse.genDecimalAttrib("TotalLifeTime")("lifetimeTotal")
    +co_parse.genBoolAttrib("UseYoungParticleAffectThreshold")(
            "useYoungPartAffThresh")
    +co_parse.genDecimalAttrib("YoungParticleAffectThreshold")(
            "youngPartAffThresh")
    +co_parse.genBoolAttrib("UseOldParticleAffectThreshold")(
            "useOldPartAffThresh")
    +co_parse.genDecimalAttrib("OldParticleAffectThreshold")(
            "oldPartAffThresh")
    +co_parse.genBoolAttrib("AffectAttachedParticles")("affectAttachedPart")
    +co_parse.genListAttrib("numAttachedEmitters",
            co_parse.genStringAttrib(
            "attachedEmitterName"))("attachedEmitters")
)

g_contentJitter = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("JitterForce")("jitterForce")
        +co_parse.genBoolAttrib("UseCommonForce")("useCommonForce")
)

g_affJitter = pp.Group(
        co_parse.genKeyValPair("AffectorType","Jitter")("affType")
        +g_contentJitter("content")
)

g_contentInflate = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("WidthInflateRate")("inflateRateWidth")
        +co_parse.genDecimalAttrib("HeightInflateRate")("inflateRateHeight")
)

g_affInflate = pp.Group(
        co_parse.genKeyValPair("AffectorType","LinearInflate")("affType")
        +g_contentInflate("content")
)

g_contentSizeOscillator = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("TransitionPeriod")("transitionPeriod")
        +co_parse.genDecimalAttrib("BeginSizeX")("beginSizeX")
        +co_parse.genDecimalAttrib("BeginSizeY")("beginSizeY")
        +co_parse.genDecimalAttrib("EndSizeX")("endSizeX")
        +co_parse.genDecimalAttrib("EndSizeY")("endSizeY")
)

g_affSizeOscillator = pp.Group(
        co_parse.genKeyValPair("AffectorType","SizeOscillator")("affType")
        +g_contentSizeOscillator("content")
)

g_contentAxisRotate = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("AngularVelocity")("angSpeed")
        +co_parse.genDecimalAttrib("Radius")("radius")
        +gen3DAttrib("AxisOfRotation")("axisRotation")
        +gen3DAttrib("AxisOrigin")("axisOrigin")
)

g_affAxisRotate = pp.Group(
        co_parse.genKeyValPair("AffectorType","RotateAboutAxis")("affType")
        +g_contentAxisRotate("content")
)

g_contentColorOscillator = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("TransitionPeriod")("transitionPeriod")
        +co_parse.genColorAttrib("StartColor")("startColor")
        +co_parse.genDecimalAttrib("StartAlpha")("startAlpha")
        +co_parse.genColorAttrib("EndColor")("endColor")
        +co_parse.genDecimalAttrib("EndAlpha")("endAlpha")
)

g_affColorOscillator = pp.Group(
        co_parse.genKeyValPair("AffectorType","ColorOscillator")("affType")
        +g_contentColorOscillator("content")
)

g_contentDrag = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("DragCoefficient")("dragCoefficient")
)

g_affDrag = pp.Group(
        co_parse.genKeyValPair("AffectorType","Drag")("affType")
        +g_contentDrag("content")
)

g_contentFade = pp.Group(
        g_affShared
        +co_parse.genBoolAttrib("DoFadeOut")("fadeOut")
        +co_parse.genDecimalAttrib("FadeOutTime")("fadeOutTime")
        +co_parse.genBoolAttrib("DoFadeIn")("fadeIn")
        +co_parse.genDecimalAttrib("FadeInTime")("fadeInTime")
)

g_affFade = pp.Group(
        co_parse.genKeyValPair("AffectorType","Fade")("affType")
        +g_contentFade("content")
)

g_contentNearPoint = pp.Group(
        g_affShared
        +gen3DAttrib("Point")("point")
        +co_parse.genDecimalAttrib("Distance")("dist")
)

g_affNearPoint = pp.Group(
        co_parse.genKeyValPair("AffectorType","KillParticlesNearPoint")("affType")
        +g_contentNearPoint("content")
)

g_contentForcePoint = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("MinForce")("forceMin")
        +co_parse.genDecimalAttrib("MaxForce")("forceMax")
        +gen3DAttrib("Point")("vector")
)

g_affForcePoint = pp.Group(
        co_parse.genKeyValPair("AffectorType","LinearForceToPoint")("affType")
        +g_contentForcePoint("content")
)

g_contentForceDirection = pp.Group(
        g_affShared
        +co_parse.genDecimalAttrib("MinForce")("forceMin")
        +co_parse.genDecimalAttrib("MaxForce")("forceMax")
        +gen3DAttrib("Direction")("vector")
)

g_affForceDirection = pp.Group(
        co_parse.genKeyValPair("AffectorType","LinearForceInDirection")("affType")
        +g_contentForceDirection("content")
)

g_affector = pp.Or([g_affJitter,g_affInflate,g_affSizeOscillator,
    g_affAxisRotate,g_affColorOscillator,g_affDrag,g_affFade,g_affNearPoint,
    g_affForcePoint,g_affForceDirection])

"""### Emitters ###"""

g_emitterShared = (
    co_parse.genHeading("EmitterContents")
    +co_parse.genStringAttrib("Name")("name")
    +co_parse.genBoolAttrib("Enabled")("enabled")
    +co_parse.genDecimalAttrib("EmitRate")("emitRate")
    +co_parse.genBoolAttrib("HasInfiniteEmitCount")("isEmitCountInf")
    +co_parse.genIntAttrib("MaxEmitCount")("emitCountMax")
    +co_parse.genBoolAttrib("hasEmitIntervals")("hasEmitIntervals")
    +co_parse.genDecimalAttrib("emitIntervalRunDuration")("emitIntervalRun")
    +co_parse.genDecimalAttrib("emitIntervalWaitDuration")("emitIntervalWait")
    +co_parse.genDecimalAttrib("ParticleLifeTime")("partLifetime")
    +co_parse.genDecimalAttrib("ParticleMinStartLinearSpeed")(
            "partStartSpeedLinMin")
    +co_parse.genDecimalAttrib("ParticleMaxStartLinearSpeed")(
            "partStartSpeedLinMax")
    +co_parse.genDecimalAttrib("ParticleMinStartAngularSpeed")(
            "partStartSpeedAngMin")
    +co_parse.genDecimalAttrib("ParticleMaxStartAngularSpeed")(
            "partStartSpeedAngMax")
    +co_parse.genDecimalAttrib("ParticleMinStartRotation")("partStartRotMin")
    +co_parse.genDecimalAttrib("ParticleMaxStartRotation")("partStartRotMax")
    +co_parse.genDecimalAttrib("ParticleStartMass")("partStartMass")
    +co_parse.genColorAttrib("ParticleStartColor")("partStartColor")
    +co_parse.genDecimalAttrib("ParticleWidth")("partWidth")
    +co_parse.genDecimalAttrib("ParticleHeight")("partHeight")
    +co_parse.genStringAttrib("MeshName")("mesh")
    +gen3DAttrib("Position")("pos")
    +g_orientation("orientation")
    +co_parse.genDecimalAttrib("RotateAboutForward")("rotateForward")
    +co_parse.genDecimalAttrib("RotateAboutUp")("rotateUp")
    +co_parse.genDecimalAttrib("RotateAboutCross")("rotateAcross")
    +co_parse.genDecimalAttrib("StartTime")("startTime")
    +co_parse.genBoolAttrib("HasInfiniteLifeTime")("infiniteLifetime")
    +co_parse.genDecimalAttrib("TotalLifeTime")("totalLifetime")
    +co_parse.genIntAttrib("BillboardAnchor")("billboardAnchor")
    +co_parse.genIntAttrib("ParticleFacing")("partFacing")
    +co_parse.genStringAttrib("PipelineEffectID")("pipelineEffect")
    +co_parse.genBoolAttrib("AreParticlesAttached")("particlesAttached")
    +co_parse.genListAttrib("numTextures",
            co_parse.genStringAttrib("textureName"))("textures")
    +co_parse.genStringAttrib("textureAnimationName")("textureAnim")
    +co_parse.genEnumAttrib("textureAnimationSpawnType",TEXANIM_SPAWN_TYPES)(
            "texanimSpawnType")
    +co_parse.genDecimalAttrib("textureAnimationOnParticleFPS")("texanimFPS")
    +co_parse.genBoolAttrib("ParticlesRotate")("partRotate")
    +co_parse.genIntAttrib("MeshParticleRotationAxisType")("meshRotAxisType")
    +gen3DAttrib("MeshParticleRotationAxis")("meshRotAxis")
    +co_parse.genIntAttrib("RotationDirectionType")("rotDirectionType")
)

g_contentPoint = pp.Group(
        g_emitterShared
        +co_parse.genDecimalAttrib("AngleVariance")("angleVariance")
)

g_emitterPoint = pp.Group(
        co_parse.genKeyValPair("EmitterType","Point")("emitterType")
        +g_contentPoint("content")
)

g_contentRing = pp.Group(
        g_emitterShared
        +co_parse.genDecimalAttrib("RingRadiusXMin")("radiusXMin")
        +co_parse.genDecimalAttrib("RingRadiusXMax")("radiusXMax")
        +co_parse.genDecimalAttrib("RingRadiusYMin")("radiusYMin")
        +co_parse.genDecimalAttrib("RingRadiusYMax")("radiusYMax")
        +co_parse.genDecimalAttrib("ParticleMaxStartSpeedTangential")(
                "maxStartSpeedTang")
        +co_parse.genDecimalAttrib("ParticleMaxStartSpeedRingNormal")(
                "maxStartSpeedRing")
        +co_parse.genBoolAttrib("ScaleStartSpeedsByRadius")("scaleSpeedByRadius")
        +co_parse.genDecimalAttrib("SpawnAngleStart")("spawnAngleStart")
        +co_parse.genDecimalAttrib("SpawnAngleStop")("spawnAngleStop")
        +co_parse.genDecimalAttrib("minSpawnHeight")("spawnHeightMin")
        +co_parse.genDecimalAttrib("maxSpawnHeight")("spawnHeightMax")
        +co_parse.genBoolAttrib("spawnDirectionIsParallelToPlane")(
                "spawnParallelToPlane")
        +co_parse.genBoolAttrib("isSpawnAngleRandom")("spawnAngleRandom")
        +co_parse.genIntAttrib("nonRandomSpawnLoopEmittedParticleCount")(
                "nonRandomLoopPartCount")
)

g_emitterRing = pp.Group(
        co_parse.genKeyValPair("EmitterType","Ring")("emitterType")
        +g_contentRing("content")
)

g_contentSphere = pp.Group(
        g_emitterShared
        +co_parse.genDecimalAttrib("SphereRadiusXMax")("radiusXMax")
        +co_parse.genDecimalAttrib("SphereRadiusXMin")("radiusXMin")
        +co_parse.genDecimalAttrib("SphereRadiusYMax")("radiusYMax")
        +co_parse.genDecimalAttrib("SphereRadiusYMin")("radiusYMin")
        +co_parse.genDecimalAttrib("SphereRadiusZMax")("radiusZMax")
        +co_parse.genDecimalAttrib("SphereRadiusZMin")("radiusZMin")
        +co_parse.genDecimalAttrib("ParticleMaxStartSpeedAzimuthalTangential")(
                "maxStartSpeedAzimuTang")
        +co_parse.genDecimalAttrib("ParticleMaxStartSpeedPolarTangential")(
                "maxStartSpeedPolarTang")
        +co_parse.genBoolAttrib("ScaleStartSpeedsByRadius")("scaleSpeedByRadius")
        +co_parse.genDecimalAttrib("SpawnAngleLatitudinalStart")(
                "spawnAngleLatStart")
        +co_parse.genDecimalAttrib("SpawnAngleLatitudinalStop")(
                "spawnAngleLatStop")
        +co_parse.genDecimalAttrib("SpawnAngleLongitudinalStart")(
                "spawnAngleLongStart")
        +co_parse.genDecimalAttrib("SpawnAngleLongitudinalStop")(
                "spawnAngleLongStop")
)

g_emitterSphere = pp.Group(
        co_parse.genKeyValPair("EmitterType","Sphere")("emitterType")
        +g_contentSphere("content")
)

g_emitter = pp.Or([g_emitterPoint,g_emitterRing,g_emitterSphere])

g_ParticleFile = (
    co_parse.g_txt
    +co_parse.genHeading("ParticleSimulation")
    +co_parse.genBoolAttrib("HasInfiniteLifeTime")("infiniteLife")
    +co_parse.genDecimalAttrib("TotalLifeTime")("totalLifetime")
    +co_parse.genListAttrib("NumEmitters",g_emitter)("emitters")
    +co_parse.genListAttrib("NumAffectors",g_affector)("affectors")
    +co_parse.genDecimalAttrib("length")("length")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################### Attributes ##############################"""

class AttribPos3d(co_attribs.Attribute):
    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier,**kwargs)
        self.value = list(val)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.value==other.value)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier
        x,y,z = self.value
        res += " "
        if not isinstance(x,int) \
                or not isinstance(y,int)\
                or not isinstance(z,int):
            res += "[ {0:f} {1:f} {2:f} ]\n".format(x,y,z)
        else:
            res += "[ {0:d} {1:d} {2:d} ]\n".format(x,y,z)
        return indentText(res,indention)

class Orientation(co_attribs.Attribute):
    def __init__(self,vals,**kwargs):
        super().__init__(**kwargs)
        self.vals = tuple(map(tuple,vals))

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.vals==other.vals)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        for val in self.vals:
            if not isinstance(val[0],int) \
                    or not isinstance(val[1],int) \
                    or not isinstance(val[2],int):
                res += "\t[ {0:f} {1:f} {2:f} ]\n".format(*val)
            else:
                res += "\t[ {0:d} {1:d} {2:d} ]\n".format(*val)
        return indentText(res,indention)

class PipelineRef(co_attribs.FileRef):
    _REFTYPE = "Pipeline Effect"

    def __init__(self,**kwargs):
        super().__init__(subdir="PipelineEffect",extension="fx",**kwargs)

class ParticleRef(co_attribs.FileRef):
    _REFTYPE = "Particle Effect"

    def __init__(self,**kwargs):
        super().__init__(subdir="Particle",extension="particle",**kwargs)

class EmitterRef(co_attribs.FileLocalRef):
    def __init__(self,**kwargs):
        super().__init__(**kwargs)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            if self.ref not in self.parent.emitters:
                res.append(
                        co_probs.MissingRefProblem("Emitter",self.ref,
                        probLine=self.linenum))
        return res

"""### Affectors ###"""

class AffShared(co_attribs.Attribute):
    def __init__(self,name,enabled,startTime,lifetimeInfinite,lifetimeTotal,
            useYoungPartAffThresh,youngPartAffThresh,useOldPartAffThresh,
            oldPartAffThresh,affectAttachedPart,attachedEmitters,parent,
            **kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.enabled = co_attribs.AttribBool(**enabled)
        self.startTime = co_attribs.AttribNum(**startTime)
        self.lifetimeInfinite = co_attribs.AttribBool(**lifetimeInfinite)
        self.lifetimeTotal = co_attribs.AttribNum(**lifetimeTotal)
        self.useYoungPartAffThresh = co_attribs.AttribBool(**useYoungPartAffThresh)
        self.youngPartAffThresh = co_attribs.AttribNum(**youngPartAffThresh)
        self.useOldPartAffThresh = co_attribs.AttribBool(**useOldPartAffThresh)
        self.oldPartAffThresh = co_attribs.AttribNum(**oldPartAffThresh)
        self.affectAttachedPart = co_attribs.AttribBool(**affectAttachedPart)
        self.attachedEmitters = co_attribs.AttribList(elemType=EmitterRef,
                elemArgs={"parent":parent},**attachedEmitters)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.enabled==other.enabled)
                        and (self.startTime==other.startTime)
                        and (self.lifetimeInfinite==other.lifetimeInfinite)
                        and (self.lifetimeTotal==other.lifetimeTotal)
                        and (
                        self.useYoungPartAffThresh==other.useYoungPartAffThresh)
                        and (self.youngPartAffThresh==other.youngPartAffThresh)
                        and (
                        self.useOldPartAffThresh==other.useOldPartAffThresh)
                        and (self.oldPartAffThresh==other.oldPartAffThresh)
                        and (self.affectAttachedPart==other.affectAttachedPart)
                        and (self.attachedEmitters==other.attachedEmitters))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.enabled.toString(1)
        res += self.startTime.toString(1)
        res += self.lifetimeInfinite.toString(1)
        res += self.lifetimeTotal.toString(1)
        res += self.useYoungPartAffThresh.toString(1)
        res += self.youngPartAffThresh.toString(1)
        res += self.useOldPartAffThresh.toString(1)
        res += self.oldPartAffThresh.toString(1)
        res += self.affectAttachedPart.toString(1)
        res += self.attachedEmitters.toString(1)
        return indentText(res,indention)

class AffJitter(AffShared):
    def __init__(self,jitterForce,useCommonForce,**kwargs):
        super().__init__(**kwargs)
        self.jitterForce = co_attribs.AttribNum(**jitterForce)
        self.useCommonForce = co_attribs.AttribBool(**useCommonForce)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.jitterForce==other.jitterForce)
                        and (self.useCommonForce==other.useCommonForce))

    def toString(self,indention):
        res = super().toString(0)
        res += self.jitterForce.toString(1)
        res += self.useCommonForce.toString(1)
        return indentText(res,indention)

class AffInflate(AffShared):
    def __init__(self,inflateRateWidth,inflateRateHeight,**kwargs):
        super().__init__(**kwargs)
        self.inflateRateWidth = co_attribs.AttribNum(**inflateRateWidth)
        self.inflateRateHeight = co_attribs.AttribNum(**inflateRateHeight)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.inflateRateWidth==other.inflateRateWidth)
                        and (self.inflateRateHeight==other.inflateRateHeight))

    def toString(self,indention):
        res = super().toString(0)
        res += self.inflateRateWidth.toString(1)
        res += self.inflateRateHeight.toString(1)
        return indentText(res,indention)

class AffSizeOscillator(AffShared):
    def __init__(self,transitionPeriod,beginSizeX,beginSizeY,endSizeX,endSizeY,
            **kwargs):
        super().__init__(**kwargs)
        self.transitionPeriod = co_attribs.AttribNum(**transitionPeriod)
        self.beginSizeX = co_attribs.AttribNum(**beginSizeX)
        self.beginSizeY = co_attribs.AttribNum(**beginSizeY)
        self.endSizeX = co_attribs.AttribNum(**endSizeX)
        self.endSizeY = co_attribs.AttribNum(**endSizeY)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.transitionPeriod==other.transitionPeriod)
                        and (self.beginSizeX==other.beginSizeX)
                        and (self.beginSizeY==other.beginSizeY)
                        and (self.endSizeX==other.endSizeX)
                        and (self.endSizeY==other.endSizeY))

    def toString(self,indention):
        res = super().toString(0)
        res += self.transitionPeriod.toString(1)
        res += self.beginSizeX.toString(1)
        res += self.beginSizeY.toString(1)
        res += self.endSizeX.toString(1)
        res += self.endSizeY.toString(1)
        return indentText(res,indention)

class AffRotateAxis(AffShared):
    def __init__(self,angSpeed,radius,axisRotation,axisOrigin,**kwargs):
        super().__init__(**kwargs)
        self.angSpeed = co_attribs.AttribNum(**angSpeed)
        self.radius = co_attribs.AttribNum(**radius)
        self.axisRotation = AttribPos3d(**axisRotation)
        self.axisOrigin = AttribPos3d(**axisOrigin)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.angSpeed==other.angSpeed)
                        and (self.radius==other.radius)
                        and (self.axisRotation==other.axisRotation)
                        and (self.axisOrigin==other.axisOrigin))

    def toString(self,indention):
        res = super().toString(0)
        res += self.angSpeed.toString(1)
        res += self.radius.toString(1)
        res += self.axisRotation.toString(1)
        res += self.axisOrigin.toString(1)
        return indentText(res,indention)

class AffColorOscillator(AffShared):
    def __init__(self,transitionPeriod,startColor,startAlpha,endColor,endAlpha,
            **kwargs):
        super().__init__(**kwargs)
        self.transitionPeriod = co_attribs.AttribNum(**transitionPeriod)
        self.startColor = co_attribs.AttribColor(**startColor)
        self.startAlpha = co_attribs.AttribNum(**startAlpha)
        self.endColor = co_attribs.AttribColor(**endColor)
        self.endAlpha = co_attribs.AttribNum(**endAlpha)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.transitionPeriod==other.transitionPeriod)
                        and (self.startColor==other.startColor)
                        and (self.startAlpha==other.startAlpha)
                        and (self.endColor==other.endColor)
                        and (self.endAlpha==other.endAlpha))

    def toString(self,indention):
        res = super().toString(0)
        res += self.transitionPeriod.toString(1)
        res += self.startColor.toString(1)
        res += self.startAlpha.toString(1)
        res += self.endColor.toString(1)
        res += self.endAlpha.toString(1)
        return indentText(res,indention)

class AffDrag(AffShared):
    def __init__(self,dragCoefficient,**kwargs):
        super().__init__(**kwargs)
        self.dragCoefficient = co_attribs.AttribNum(**dragCoefficient)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.dragCoefficient==other.dragCoefficient)

    def toString(self,indention):
        res = super().toString(0)
        res += self.dragCoefficient.toString(1)
        return indentText(res,indention)

class AffFade(AffShared):
    def __init__(self,fadeOut,fadeOutTime,fadeIn,fadeInTime,**kwargs):
        super().__init__(**kwargs)
        self.fadeOut = co_attribs.AttribBool(**fadeOut)
        self.fadeOutTime = co_attribs.AttribNum(**fadeOutTime)
        self.fadeIn = co_attribs.AttribBool(**fadeIn)
        self.fadeInTime = co_attribs.AttribNum(**fadeInTime)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.fadeOut==other.fadeOut)
                        and (self.fadeOutTime==other.fadeOutTime)
                        and (self.fadeIn==other.fadeIn)
                        and (self.fadeInTime==other.fadeInTime))

    def toString(self,indention):
        res = super().toString(0)
        res += self.fadeOut.toString(1)
        res += self.fadeOutTime.toString(1)
        res += self.fadeIn.toString(1)
        res += self.fadeInTime.toString(1)
        return indentText(res,indention)

class AffNearPoint(AffShared):
    def __init__(self,point,dist,**kwargs):
        super().__init__(**kwargs)
        self.point = AttribPos3d(**point)
        self.dist = co_attribs.AttribNum(**dist)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.point==other.point)
                        and (self.dist==other.dist))

    def toString(self,indention):
        res = super().toString(0)
        res += self.point.toString(1)
        res += self.dist.toString(1)
        return indentText(res,indention)

class AffForce(AffShared):
    def __init__(self,forceMin,forceMax,vector,**kwargs):
        super().__init__(**kwargs)
        self.forceMin = co_attribs.AttribNum(**forceMin)
        self.forceMax = co_attribs.AttribNum(**forceMax)
        self.vector = AttribPos3d(**vector)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.forceMin==other.forceMin)
                        and (self.forceMax==other.forceMax)
                        and (self.vector==other.vector))

    def toString(self,indention):
        res = super().toString(0)
        res += self.forceMin.toString(1)
        res += self.forceMax.toString(1)
        res += self.vector.toString(1)
        return indentText(res,indention)

AFFTYPE_TO_AFFCLASS = {
    "ColorOscillator":AffColorOscillator,
    "Drag":AffDrag,
    "Fade":AffFade,
    "Jitter":AffJitter,
    "KillParticlesNearPoint":AffNearPoint,
    "LinearForceInDirection":AffForce,
    "LinearForceToPoint":AffForce,
    "LinearInflate":AffInflate,
    "RotateAboutAxis":AffRotateAxis,
    "SizeOscillator":AffSizeOscillator
}

class Affector(co_attribs.Attribute):
    def __init__(self,affType,content,parent,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.affType = co_attribs.AttribEnum(possibleVals=AFFTYPE_TO_AFFCLASS.keys(),
                **affType)
        self.content = AFFTYPE_TO_AFFCLASS[self.affType.value](parent=parent,
                **content)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.affType==other.affType)
                        and (self.content==other.content))

    def toString(self,indention):
        res = super().toString(0)
        res += self.affType.toString(0)
        res += self.content.toString(0)
        return indentText(res,indention)

"""### Emitters ###"""

class EmitterShared(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,name,enabled,emitRate,isEmitCountInf,emitCountMax,
            hasEmitIntervals,emitIntervalRun,emitIntervalWait,partLifetime,
            partStartSpeedLinMin,partStartSpeedLinMax,partStartSpeedAngMin,
            partStartSpeedAngMax,partStartRotMin,partStartRotMax,partStartMass,
            partStartColor,partWidth,partHeight,mesh,pos,orientation,
            rotateForward,rotateUp,rotateAcross,startTime,infiniteLifetime,
            totalLifetime,billboardAnchor,partFacing,pipelineEffect,
            particlesAttached,textures,textureAnim,texanimSpawnType,texanimFPS,
            partRotate,meshRotAxis,meshRotAxisType,rotDirectionType,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.enabled = co_attribs.AttribBool(**enabled)
        self.emitRate = co_attribs.AttribNum(**emitRate)
        self.isEmitCountInf = co_attribs.AttribBool(**isEmitCountInf)
        self.emitCountMax = co_attribs.AttribNum(**emitCountMax)
        self.hasEmitIntervals = co_attribs.AttribBool(**hasEmitIntervals)
        self.emitIntervalRun = co_attribs.AttribNum(**emitIntervalRun)
        self.emitIntervalWait = co_attribs.AttribNum(**emitIntervalWait)
        self.partLifetime = co_attribs.AttribNum(**partLifetime)
        self.partStartSpeedLinMin = co_attribs.AttribNum(**partStartSpeedLinMin)
        self.partStartSpeedLinMax = co_attribs.AttribNum(**partStartSpeedLinMax)
        self.partStartSpeedAngMin = co_attribs.AttribNum(**partStartSpeedAngMin)
        self.partStartSpeedAngMax = co_attribs.AttribNum(**partStartSpeedAngMax)
        self.partStartRotMin = co_attribs.AttribNum(**partStartRotMin)
        self.partStartRotMax = co_attribs.AttribNum(**partStartRotMax)
        self.partStartMass = co_attribs.AttribNum(**partStartMass)
        self.partStartColor = co_attribs.AttribColor(**partStartColor)
        self.partWidth = co_attribs.AttribNum(**partWidth)
        self.partHeight = co_attribs.AttribNum(**partHeight)
        self.mesh = meshes.MeshRef(canBeEmpty=True,**mesh)
        self.pos = AttribPos3d(**pos)
        self.orientation = Orientation(**orientation)
        self.rotateForward = co_attribs.AttribNum(**rotateForward)
        self.rotateAcross = co_attribs.AttribNum(**rotateAcross)
        self.rotateUp = co_attribs.AttribNum(**rotateUp)
        self.startTime = co_attribs.AttribNum(**startTime)
        self.infiniteLifetime = co_attribs.AttribBool(**infiniteLifetime)
        self.totalLifetime = co_attribs.AttribNum(**totalLifetime)
        self.billboardAnchor = co_attribs.AttribNum(**billboardAnchor)
        self.partFacing = co_attribs.AttribNum(**partFacing)
        self.pipelineEffect = PipelineRef(**pipelineEffect)
        self.particlesAttached = co_attribs.AttribBool(**particlesAttached)
        self.textures = co_attribs.AttribList(elemType=ui.UITextureRef,
                elemArgs={"canBeEmpty":True},
                **textures)
        self.textureAnim = anim.TextureAnimRef(canBeEmpty=True,**textureAnim)
        self.texanimSpawnType = co_attribs.AttribEnum(possibleVals=TEXANIM_SPAWN_TYPES,
                **texanimSpawnType)
        self.texanimFPS = co_attribs.AttribNum(**texanimFPS)
        self.partRotate = co_attribs.AttribBool(**partRotate)
        self.meshRotAxisType = co_attribs.AttribNum(**meshRotAxisType)
        self.meshRotAxis = AttribPos3d(**meshRotAxis)
        self.rotDirectionType = co_attribs.AttribNum(**rotDirectionType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.enabled==other.enabled)
                        and (self.emitRate==other.emitRate)
                        and (self.isEmitCountInf==other.isEmitCountInf)
                        and (self.emitCountMax==other.emitCountMax)
                        and (self.hasEmitIntervals==other.hasEmitIntervals)
                        and (self.emitIntervalRun==other.emitIntervalRun)
                        and (self.emitIntervalWait==other.emitIntervalWait)
                        and (self.partLifetime==other.partLifetime)
                        and (
                        self.partStartSpeedLinMin==other.partStartSpeedLinMin)
                        and (
                        self.partStartSpeedLinMax==other.partStartSpeedLinMax)
                        and (
                        self.partStartSpeedAngMin==other.partStartSpeedAngMin)
                        and (
                        self.partStartSpeedAngMax==other.partStartSpeedAngMax)
                        and (self.partStartRotMin==other.partStartRotMin)
                        and (self.partStartRotMax==other.partStartRotMax)
                        and (self.partStartMass==other.partStartMass)
                        and (self.partStartColor==other.partStartColor)
                        and (self.partWidth==other.partWidth)
                        and (self.partHeight==other.partHeight)
                        and (self.mesh==other.mesh)
                        and (self.pos==other.pos)
                        and (self.orientation==other.orientation)
                        and (self.rotateForward==other.rotateForward)
                        and (self.rotateUp==other.rotateUp)
                        and (self.rotateAcross==other.rotateAcross)
                        and (self.startTime==other.startTime)
                        and (self.infiniteLifetime==other.infiniteLifetime)
                        and (self.totalLifetime==other.totalLifetime)
                        and (self.billboardAnchor==other.billboardAnchor)
                        and (self.partFacing==other.partFacing)
                        and (self.pipelineEffect==other.pipelineEffect)
                        and (self.particlesAttached==other.particlesAttached)
                        and (self.textures==other.textures)
                        and (self.textureAnim==other.textureAnim)
                        and (self.texanimSpawnType==other.texanimSpawnType)
                        and (self.texanimFPS==other.texanimFPS)
                        and (self.partRotate==other.partRotate)
                        and (self.meshRotAxisType==other.meshRotAxisType)
                        and (self.meshRotAxis==other.meshRotAxis)
                        and (self.rotDirectionType==other.rotDirectionType))

    def toString(self,indention):
        res = super().toString(indention)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.enabled.toString(1)
        res += self.emitRate.toString(1)
        res += self.isEmitCountInf.toString(1)
        res += self.emitCountMax.toString(1)
        res += self.hasEmitIntervals.toString(1)
        res += self.emitIntervalRun.toString(1)
        res += self.emitIntervalWait.toString(1)
        res += self.partLifetime.toString(1)
        res += self.partStartSpeedLinMin.toString(1)
        res += self.partStartSpeedLinMax.toString(1)
        res += self.partStartSpeedAngMin.toString(1)
        res += self.partStartSpeedAngMax.toString(1)
        res += self.partStartRotMin.toString(1)
        res += self.partStartRotMax.toString(1)
        res += self.partStartMass.toString(1)
        res += self.partStartColor.toString(1)
        res += self.partWidth.toString(1)
        res += self.partHeight.toString(1)
        res += self.mesh.toString(1)
        res += self.pos.toString(1)
        res += self.orientation.toString(1)
        res += self.rotateForward.toString(1)
        res += self.rotateUp.toString(1)
        res += self.rotateAcross.toString(1)
        res += self.startTime.toString(1)
        res += self.infiniteLifetime.toString(1)
        res += self.totalLifetime.toString(1)
        res += self.billboardAnchor.toString(1)
        res += self.partFacing.toString(1)
        res += self.pipelineEffect.toString(1)
        res += self.particlesAttached.toString(1)
        res += self.textures.toString(1)
        res += self.textureAnim.toString(1)
        res += self.texanimSpawnType.toString(1)
        res += self.texanimFPS.toString(1)
        res += self.partRotate.toString(1)
        res += self.meshRotAxisType.toString(1)
        res += self.meshRotAxis.toString(1)
        res += self.rotDirectionType.toString(1)
        return indentText(res,indention)

class EmitterPoint(EmitterShared):
    def __init__(self,angleVariance,**kwargs):
        super().__init__(**kwargs)
        self.angleVariance = co_attribs.AttribNum(**angleVariance)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.angleVariance==other.angleVariance)

    def toString(self,indention):
        res = super().toString(0)
        res += self.angleVariance.toString(1)
        return indentText(res,indention)

class EmitterRing(EmitterShared):
    def __init__(self,radiusXMin,radiusXMax,radiusYMin,radiusYMax,
            maxStartSpeedTang,maxStartSpeedRing,scaleSpeedByRadius,
            spawnAngleStart,spawnAngleStop,spawnHeightMin,spawnHeightMax,
            spawnParallelToPlane,spawnAngleRandom,nonRandomLoopPartCount,
            **kwargs):
        super().__init__(**kwargs)
        self.radiusXMin = co_attribs.AttribNum(**radiusXMin)
        self.radiusXMax = co_attribs.AttribNum(**radiusXMax)
        self.radiusYMin = co_attribs.AttribNum(**radiusYMin)
        self.radiusYMax = co_attribs.AttribNum(**radiusYMax)
        self.maxStartSpeedTang = co_attribs.AttribNum(**maxStartSpeedTang)
        self.maxStartSpeedRing = co_attribs.AttribNum(**maxStartSpeedRing)
        self.scaleSpeedByRadius = co_attribs.AttribBool(**scaleSpeedByRadius)
        self.spawnAngleStart = co_attribs.AttribNum(**spawnAngleStart)
        self.spawnAngleStop = co_attribs.AttribNum(**spawnAngleStop)
        self.spawnHeightMin = co_attribs.AttribNum(**spawnHeightMin)
        self.spawnHeightMax = co_attribs.AttribNum(**spawnHeightMax)
        self.spawnParallelToPlane = co_attribs.AttribBool(**spawnParallelToPlane)
        self.spawnAngleRandom = co_attribs.AttribBool(**spawnAngleRandom)
        self.nonRandomLoopPartCount = co_attribs.AttribNum(**nonRandomLoopPartCount)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.radiusXMin==other.radiusXMin)
                        and (self.radiusXMax==other.radiusXMax)
                        and (self.radiusYMin==other.radiusYMin)
                        and (self.radiusYMax==other.radiusYMax)
                        and (self.maxStartSpeedTang==other.maxStartSpeedTang)
                        and (self.maxStartSpeedRing==other.maxStartSpeedRing)
                        and (self.scaleSpeedByRadius==other.scaleSpeedByRadius)
                        and (self.spawnAngleStart==other.spawnAngleStart)
                        and (self.spawnAngleStop==other.spawnAngleStop)
                        and (self.spawnHeightMin==other.spawnHeightMin)
                        and (self.spawnHeightMax==other.spawnHeightMax)
                        and (
                        self.spawnParallelToPlane==other.spawnParallelToPlane)
                        and (self.spawnAngleRandom==other.spawnAngleRandom)
                        and (
                        self.nonRandomLoopPartCount==other.nonRandomLoopPartCount))

    def toString(self,indention):
        res = super().toString(0)
        res += self.radiusXMin.toString(1)
        res += self.radiusXMax.toString(1)
        res += self.radiusYMin.toString(1)
        res += self.radiusYMax.toString(1)
        res += self.maxStartSpeedTang.toString(1)
        res += self.maxStartSpeedRing.toString(1)
        res += self.scaleSpeedByRadius.toString(1)
        res += self.spawnAngleStart.toString(1)
        res += self.spawnAngleStop.toString(1)
        res += self.spawnHeightMin.toString(1)
        res += self.spawnHeightMax.toString(1)
        res += self.spawnParallelToPlane.toString(1)
        res += self.spawnAngleRandom.toString(1)
        res += self.nonRandomLoopPartCount.toString(1)
        return indentText(res,indention)

class EmitterSphere(EmitterShared):
    def __init__(self,radiusXMax,radiusXMin,radiusYMax,radiusYMin,radiusZMax,
            radiusZMin,maxStartSpeedAzimuTang,maxStartSpeedPolarTang,
            scaleSpeedByRadius,spawnAngleLatStart,spawnAngleLatStop,
            spawnAngleLongStart,spawnAngleLongStop,**kwargs):
        super().__init__(**kwargs)
        self.radiusXMax = co_attribs.AttribNum(**radiusXMax)
        self.radiusXMin = co_attribs.AttribNum(**radiusXMin)
        self.radiusYMax = co_attribs.AttribNum(**radiusYMax)
        self.radiusYMin = co_attribs.AttribNum(**radiusYMin)
        self.radiusZMax = co_attribs.AttribNum(**radiusZMax)
        self.radiusZMin = co_attribs.AttribNum(**radiusZMin)
        self.maxStartSpeedAzimuTang = co_attribs.AttribNum(**maxStartSpeedAzimuTang)
        self.maxStartSpeedPolarTang = co_attribs.AttribNum(**maxStartSpeedPolarTang)
        self.scaleSpeedByRadius = co_attribs.AttribBool(**scaleSpeedByRadius)
        self.spawnAngleLatStart = co_attribs.AttribNum(**spawnAngleLatStart)
        self.spawnAngleLatStop = co_attribs.AttribNum(**spawnAngleLatStop)
        self.spawnAngleLongStart = co_attribs.AttribNum(**spawnAngleLongStart)
        self.spawnAngleLongStop = co_attribs.AttribNum(**spawnAngleLongStop)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.radiusXMax==other.radiusXMax)
                        and (self.radiusXMin==other.radiusXMin)
                        and (self.radiusYMax==other.radiusYMax)
                        and (self.radiusYMin==other.radiusYMin)
                        and (self.radiusZMax==other.radiusZMax)
                        and (self.radiusZMin==other.radiusZMin)
                        and (
                        self.maxStartSpeedAzimuTang==other.maxStartSpeedAzimuTang)
                        and (
                        self.maxStartSpeedPolarTang==other.maxStartSpeedPolarTang)
                        and (self.scaleSpeedByRadius==other.scaleSpeedByRadius)
                        and (self.spawnAngleLatStart==other.spawnAngleLatStart)
                        and (self.spawnAngleLatStop==other.spawnAngleLatStop)
                        and (self.spawnAngleLongStop==other.spawnAngleLongStop)
                        and (
                        self.spawnAngleLongStart==other.spawnAngleLongStart))

    def toString(self,indention):
        res = super().toString(0)
        res += self.radiusXMax.toString(1)
        res += self.radiusXMin.toString(1)
        res += self.radiusYMax.toString(1)
        res += self.radiusYMin.toString(1)
        res += self.radiusZMax.toString(1)
        res += self.radiusZMin.toString(1)
        res += self.maxStartSpeedAzimuTang.toString(1)
        res += self.maxStartSpeedPolarTang.toString(1)
        res += self.scaleSpeedByRadius.toString(1)
        res += self.spawnAngleLatStart.toString(1)
        res += self.spawnAngleLatStop.toString(1)
        res += self.spawnAngleLongStart.toString(1)
        res += self.spawnAngleLongStop.toString(1)
        return indentText(res,indention)

EMITTERTYPE_TO_EMITTERCLASS = {
    "Point":EmitterPoint,
    "Ring":EmitterRing,
    "Sphere":EmitterSphere
}

class Emitter(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,emitterType,content,**kwargs):
        super().__init__(identifier="",**kwargs)
        self.emitterType = co_attribs.AttribEnum(
                possibleVals=EMITTERTYPE_TO_EMITTERCLASS.keys(),
                **emitterType)
        self.content = EMITTERTYPE_TO_EMITTERCLASS[self.emitterType.value](
                **content)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.emitterType==other.emitterType)
                        and self.content==other.content)

    def toString(self,indention):
        res = super().toString(0)
        res += self.emitterType.toString(0)
        res += self.content.toString(0)
        return indentText(res,indention)

"""############################## File Classes #############################"""

class ParticleFile(co_basics.IsFile,co_basics.HasReference):
    parser = g_ParticleFile

    def __init__(self,identifier,infiniteLife,totalLifetime,emitters,
            affectors,length,**kwargs):
        super().__init__(**kwargs)
        self.identifier = identifier
        self.infiniteLife = co_attribs.AttribBool(**infiniteLife)
        self.totalLifetime = co_attribs.AttribNum(**totalLifetime)
        self.emitters = co_attribs.AttribListKeyed(elemType=Emitter,
                keyfunc=lambda val:val.content.name.value,**emitters)
        self.affectors = co_attribs.AttribList(elemType=Affector,
                elemArgs={"parent":self},
                **affectors)
        self.length = co_attribs.AttribNum(**length)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.identifier==other.identifier)
                        and (self.infiniteLife==other.infiniteLife)
                        and (self.totalLifetime==other.totalLifetime)
                        and (self.emitters==other.emitters)
                        and (self.affectors==other.affectors)
                        and (self.length==other.length))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.infiniteLife.toString(1)
        res += self.totalLifetime.toString(1)
        res += self.emitters.toString(1)
        res += self.affectors.toString(1)
        res += self.length.toString(1)
        return indentText(res,indention)

class ModParticles(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"Particle","particle")

    def loadParticle(self,name):
        probs = []
        if name not in self.compFilesMod or self.compFilesMod[name] is None:
            inst,probs = self.loadFile(self.subName,name,ParticleFile)
            if inst is None:
                self.compFilesMod[name] = probs
            else:
                self.compFilesMod[name] = inst
        elif isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name])
        return probs

    def checkParticle(self,name):
        probs = self.loadParticle(name)
        if not probs:
            probs.extend(self.compFilesMod[name].check(self.mod))
        return probs

    def check(self,strict=False):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Checking Particles") as pbar:
            for name in self.compFilesMod:
                probs.extend(self.checkParticle(name))
                pbar.update()
        return probs

    def checkFile(self,filepath):
        base = os.path.basename(filepath)
        name,ext = os.path.splitext(base)
        if ext==".particle":
            return self.checkParticle(base)
        else:
            raise RuntimeError(ext+" is and invalid file type for "
                                   "ModParticles. It only handles .particle "
                                   "type files.")

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Loading Particles") as pbar:
            for name in self.compFilesMod:
                probs.extend(self.loadParticle(name))
                pbar.update()
        return probs
