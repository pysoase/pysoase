"""Contains elements for the Sins User Interface
"""

import ntpath
import os

import pyparsing as pp
import tqdm

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs

"""################################ Parser #################################"""

"""### Brushes ###"""
g_pixelBox = pp.Group(
        co_parse.g_linestart
        +pp.Keyword("pixelBox")("identifier")
        +co_parse.g_space
        +co_parse.g_pos4
        +co_parse.g_eol
)("pixelBox")
g_brushDef = (
    co_parse.genStringAttrib("fileName")("texture")
    +g_pixelBox
)
g_margin = (
    co_parse.genIntAttrib("left")("left")
    +co_parse.genIntAttrib("right")("right")
    +co_parse.genIntAttrib("top")("top")
    +co_parse.genIntAttrib("bottom")("bottom")
)
g_margins = (
    pp.Group(
            co_parse.genHeading("stretchMargins")
            +g_margin
    )("stretch")
    +pp.Group(
            co_parse.genHeading("contentMargins")
            +g_margin
    )("content")
)
g_states = (
    pp.Group(
            co_parse.genHeading("Disabled")
            +g_brushDef
    )("disabled")
    +pp.Group(
            co_parse.genHeading("Pressed")
            +g_brushDef
    )("pressed")
    +pp.Group(
            co_parse.genHeading("CursorOver")
            +g_brushDef
    )("cursorOver")
    +pp.Group(
            co_parse.genHeading("Focused")
            +g_brushDef
    )("focused")
    +pp.Group(
            co_parse.genHeading("Normal")
            +g_brushDef
    )("normal")
)
g_brushSimple = (
    co_parse.genKeyValPair("content","Simple")("brushType")
    +pp.Group(g_brushDef)("brushContent")
)
g_brushMargins = (
    co_parse.genKeyValPair("content","Margins")("brushType")
    +g_margins
    +pp.Group(g_brushDef)("brushContent")
)
g_brushStates = (
    co_parse.genKeyValPair("content","States")("brushType")
    +g_states
)
g_brushMarginsStates = (
    co_parse.genKeyValPair("content","MarginsAndStates")("brushType")
    +g_margins
    +g_states
)
g_brush = pp.Group(
        co_parse.genHeading("brush")
        +co_parse.genStringAttrib("name")("name")
        +pp.Or([g_brushSimple,g_brushMargins,g_brushStates,
            g_brushMarginsStates])
)
g_BrushFile = (
    co_parse.g_txt
    +co_parse.genBoolAttrib("onlyLoadOnDemand")("loadOnDemand")
    +co_parse.genListAttrib("brushCount",g_brush)("brushes")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""### Strings ###"""
g_stringInfo = pp.Group(
        co_parse.genHeading("StringInfo")
        +co_parse.genStringAttrib("ID")("stringid")
        +co_parse.genStringAttrib("Value")("val")
)
g_StringFile = (
    co_parse.g_txt
    +co_parse.genListAttrib("NumStrings",g_stringInfo)("entries")
    +pp.StringEnd().suppress()
).ignore(pp.dblSlashComment)

"""############################# File Classes ##############################"""

"""### Strings ###"""

class StringReference(co_attribs.AttribRef):
    _REFTYPE = "String"

    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier,val,**kwargs)

    def __eq__(self,other):
        return super().__eq__(other)

    def toString(self,indention):
        return super().toString(indention)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(currMod.strings.checkString(self.ref))
            for prob in res:
                if not prob.probLine:
                    prob.probLine = self.linenum
        return res

class StringInfo(co_attribs.Attribute):
    def __init__(self,stringid,val,**kwargs):
        kwargs["identifier"] = "StringInfo"
        super().__init__(**kwargs)
        if isinstance(stringid,str):
            self._stringid = co_attribs.AttribString(identifier="ID",val=stringid)
        else:
            self._stringid = co_attribs.AttribString(**stringid)
        if isinstance(val,str):
            self._value = co_attribs.AttribString(identifier="Value",val=val)
        else:
            self._value = co_attribs.AttribString(**val)

    @property
    def stringid(self):
        return self._stringid.value

    @property
    def value(self):
        return self._value.value

    def __eq__(self,other):
        res = super().__eq__(other)
        res = res and (self.stringid==other.stringid)
        res = res and (self.value==other.value)
        return res

    def toString(self,indention):
        res = super().toString(indention)
        res += "StringInfo\n"
        res += self._stringid.toString(1)
        res += self._value.toString(1)
        return indentText(res,indention)

class StringFile(co_basics.IsFile,co_basics.Checkable):
    parser = g_StringFile

    def __init__(self,filepath,entries):
        super().__init__(filepath)
        keyfunc = lambda val:val.stringid
        self.entries = co_attribs.AttribListKeyed(elemType=StringInfo,
                keyfunc=keyfunc,**entries)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and self.entries==other.entries

    @property
    def count(self):
        return self.entries.counter

    def check(self,currMod,recursive=False):
        res = super().check(currMod,recursive)
        # Filter out IDS_TAUNT duplicate problems. These duplicates are in 
        # there by design.
        res = [p for p in res if not (isinstance(p,
                co_probs.DuplicateEntryProb) and
                                      p.key.startswith("IDS_TAUNT"))]
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.entries.toString(0)
        return indentText(res,indention)

    def removeDuplicates(self):
        res = self.entries.removeDuplicates()
        if res:
            self.changed = True
        return res

    def __contains__(self,item):
        return item in self.entries

    def __iter__(self):
        return (s for s in self.entries.elements)

    def __getitem__(self, item):
        if item in self.entries:
            return self.entries.getElement(item)
        else:
            raise KeyError("Could not find "+item+" in String File "
                           +self.filepath)

class ModStrings(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"String","str")

    def checkString(self,stringid,language=None):
        check = []
        if language is None:
            for lang in self.compFilesMod:
                check.append(lang)
        elif language+".str" in self.compFilesMod:
            check.append(language+".str")
        else:
            raise RuntimeError("Could not find String file for language "
                               +language+".")
        res = []
        for lang in check:
            probs = self.loadStringfile(lang)
            if probs:
                res.extend(probs)
            elif stringid not in self.compFilesMod[lang]:
                res.append(
                        co_probs.MissingRefProblem(lang+" String",stringid))
        return res

    def checkStringfile(self,langFile):
        probs = self.loadStringfile(langFile)
        if not probs:
            probs.extend(self.compFilesMod[langFile].check(self.mod))
        return probs

    def check(self,strict=False):
        res = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Checking String files") as pbar:
            for lang in self.compFilesMod:
                res.extend(self.checkStringfile(lang))
                pbar.update()
        return res

    def checkFile(self,filepath):
        res = super().checkFile(filepath)
        res.extend(self.checkStringfile(os.path.basename(filepath)))
        return res

    def checkSyntax(self):
        res = []
        for lang in self.compFilesMod:
            succ,tmp = StringFile.parse(
                    os.path.join(self.mod.moddir,self.subName,lang))
            if not succ:
                res.extend(tmp)
        return res

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def loadStringfile(self,langFile):
        probs = []
        if langFile not in self.compFilesMod or self.compFilesMod[
                langFile] is None:
            inst,probs = self.loadFile(self.subName,langFile,StringFile)
            if inst is None:
                self.compFilesMod[langFile] = probs
            else:
                self.compFilesMod[langFile] = inst
        elif isinstance(self.compFilesMod[langFile],list):
            probs.extend(self.compFilesMod[langFile])
        return probs

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                desc="Loading String files") as pbar:
            for langFile in self.compFilesMod:
                probs.extend(self.loadStringfile(langFile))
                pbar.update()
        return probs

    def getLanguageFile(self,lang):
        # Try to load the string file with the given language
        probs = self.loadStringfile(lang+".str")
        if not probs:
            return self.compFilesMod[lang+".str"]
        else:
            return probs

"""### Brushes ###"""

class UITextureRef(co_attribs.FileRef):
    _REFTYPE = "Texture"

    def __init__(self,identifier,val,**kwargs):
        self._vanilla = val
        co_attribs.AttribRef.__init__(self,identifier=identifier,
                val=ntpath.basename(val),
                **kwargs)
        self.subdir = "Textures"
        self.extensions = [".dds",".tga"]

    def checkExistence(self,currMod,recursive=False):
        res = []
        fsourceBest = -1
        name,ext = os.path.splitext(self.ref)
        for ext in self.extensions:
            fsource,probs = co_files.checkFileExistence(self.subdir,name+ext,currMod)
            if fsource>fsourceBest:
                res = probs
                fsourceBest = fsource
        return res

class BrushRef(co_attribs.AttribRef):
    _REFTYPE = "Brush"

    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        self._brushfile = None

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive=False)
        if self.ref:
            res.extend(currMod.brushes.checkBrush(self.ref))
            for prob in res:
                if not prob.probLine:
                    prob.probLine = self.linenum
        return res

    def getBrushfile(self,currMod):
        if not self._brushfile and self.ref:
            self._brushfile = currMod.brushes.getBrushfile(self.ref)
        return self._brushfile

class Margins(co_attribs.Attribute):
    def __init__(self,left,right,top,bottom,**kwargs):
        super().__init__(**kwargs)
        self.left = co_attribs.AttribNum(**left)
        self.right = co_attribs.AttribNum(**right)
        self.top = co_attribs.AttribNum(**top)
        self.bottom = co_attribs.AttribNum(**bottom)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.left==other.left)
                        and (self.right==other.right)
                        and (self.top==other.top)
                        and (self.bottom==other.bottom))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.left.toString(1)
        res += self.right.toString(1)
        res += self.top.toString(1)
        res += self.bottom.toString(1)
        return indentText(res,indention)

class Brush(co_attribs.Attribute):
    _allowedTypes = ["Simple","Margins","States","MarginsAndStates"]

    def __init__(self,name,brushType,**kwargs):
        super().__init__(**kwargs)
        self.name = co_attribs.AttribString(**name)
        self.brushType = co_attribs.AttribEnum(possibleVals=self._allowedTypes,
                **brushType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.name==other.name)
                        and (self.brushType==other.brushType))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.name.toString(1)
        res += self.brushType.toString(1)
        return indentText(res,indention)

    @classmethod
    def factory(cls,brushType,**kwargs):
        if brushType["val"]=="Simple":
            return BrushSimple(brushType=brushType,**kwargs)
        elif brushType["val"]=="Margins":
            return BrushMargins(brushType=brushType,**kwargs)
        elif brushType["val"]=="States":
            return BrushStates(brushType=brushType,**kwargs)
        elif brushType["val"]=="MarginsAndStates":
            return BrushMarginsStates(brushType=brushType,**kwargs)
        else:
            raise RuntimeError("Unknown brush type "+brushType["val"])

class BrushSimple(Brush,co_basics.HasReference):
    def __init__(self,brushContent,**kwargs):
        super().__init__(**kwargs)
        self.brushContent = BrushContent(**brushContent)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.brushContent==other.brushContent)

    def toString(self,indention):
        res = super().toString(0)
        res += self.brushContent.toString(1)
        return indentText(res,indention)

class BrushMargins(BrushSimple):
    def __init__(self,stretch,content,**kwargs):
        super().__init__(**kwargs)
        self.stretch = Margins(**stretch)
        self.content = Margins(**content)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.stretch==other.stretch)
                        and self.content==other.content)

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.name.toString(1)
        res += self.brushType.toString(1)
        res += self.stretch.toString(1)
        res += self.content.toString(1)
        res += self.brushContent.toString(1)
        return indentText(res,indention)

class BrushStates(Brush,co_basics.HasReference):
    def __init__(self,disabled,pressed,cursorOver,focused,normal,**kwargs):
        super().__init__(**kwargs)
        self.disabled = BrushContent(**disabled)
        self.pressed = BrushContent(**pressed)
        self.cursorOver = BrushContent(**cursorOver)
        self.focused = BrushContent(**focused)
        self.normal = BrushContent(**normal)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.disabled==other.disabled)
                        and (self.pressed==other.pressed)
                        and (self.cursorOver==other.cursorOver)
                        and (self.focused==other.focused)
                        and (self.normal==other.normal))

    def toString(self,indention):
        res = super().toString(0)
        res += self.disabled.toString(1)
        res += self.pressed.toString(1)
        res += self.cursorOver.toString(1)
        res += self.focused.toString(1)
        res += self.normal.toString(1)
        return indentText(res,indention)

class BrushMarginsStates(BrushStates):
    def __init__(self,stretch,content,**kwargs):
        super().__init__(**kwargs)
        self.stretch = Margins(**stretch)
        self.content = Margins(**content)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.stretch==other.stretch)
                        and self.content==other.content)

    def toString(self,indention):
        res = Brush.toString(self,indention)
        res += self.stretch.toString(1)
        res += self.content.toString(1)
        res += self.disabled.toString(1)
        res += self.pressed.toString(1)
        res += self.cursorOver.toString(1)
        res += self.focused.toString(1)
        res += self.normal.toString(1)
        return indentText(res,indention)

class BrushContent(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,texture,pixelBox,identifier="",**kwargs):
        super().__init__(identifier=identifier,**kwargs)
        self.texture = UITextureRef(**texture)
        self.pixelBox = co_attribs.AttribPos4d(**pixelBox)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.texture==other.texture)
                        and (self.pixelBox==other.pixelBox))

    def toString(self,indention):
        res = super().toString(0)
        indent = 0
        if self.identifier:
            res += self.identifier+"\n"
            indent += 1
        res += self.texture.toString(indent)
        res += self.pixelBox.toString(indent)
        return indentText(res,indention)

class BrushFile(co_basics.IsFile,co_basics.HasReference):
    parser = g_BrushFile

    def __init__(self,loadOnDemand,brushes,**kwargs):
        super().__init__(**kwargs)
        self.loadOnDemand = co_attribs.AttribBool(**loadOnDemand)
        self.brushes = co_attribs.AttribListKeyed(elemType=Brush,
                keyfunc=lambda val:val.name.value,factory=True,**brushes)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.loadOnDemand==other.loadOnDemand)
                        and (self.brushes==other.brushes))

    def checkBrushExistence(self,name):
        return name in self.brushes

    def toString(self,indention):
        res = super().toString(0)
        res += self.loadOnDemand.toString(0)
        res += self.brushes.toString(0)
        return indentText(res,indention)

class ModBrushes(co_basics.ModComponent):
    def __init__(self,currMod):
        super().__init__(currMod,"Window","brushes")
        self._manifest = None
        self._allLoaded = False

    def checkBrush(self,name):
        probs = self.loadAll()
        for filename,brushfile in self.compFilesMod.items():
            if (isinstance(brushfile,BrushFile)
            and brushfile.checkBrushExistence(name)):
                probs.extend(self.checkManifest(filename))
                break
        else:
            probs.append(
                    co_probs.MissingRefProblem("Brush",name))
        return probs

    def checkBrushfile(self,name):
        probs = self.loadBrushfile(name)
        if not probs:
            probs.extend(self.compFilesMod[name].check(self.mod))
            probs.extend(self.checkManifest(name))
        return probs

    def check(self,strict=False):
        probs = []
        if strict and not isinstance(self.manifest,list):
            with tqdm.tqdm(disable=self.mod.quiet,
                    total=len(self.manifest.entries.elements)+1,
                    desc="Checking Brush files") as pbar:
                if isinstance(self.manifest,list):
                    probs.extend(self.manifest)
                else:
                    probs.extend(self.manifest.check(self.mod))
                pbar.update()

                for brushfile in self.manifest.entries.elements:
                    probs.extend(self.checkBrushfile(brushfile.ref))
                    pbar.update()
        else:
            with tqdm.tqdm(disable=self.mod.quiet,
                    total=len(self.compFilesMod)+1,
                    desc="Checking Brush files") as pbar:
                if isinstance(self.manifest,list):
                    probs.extend(self.manifest)
                else:
                    probs.extend(self.manifest.check(self.mod))
                pbar.update()

                for brushfile in self.compFilesMod:
                    probs.extend(self.checkBrushfile(brushfile))
                    pbar.update()
        return probs

    def checkFile(self,filepath):
        res = super().checkFile(filepath)
        checkInst = None
        if os.path.basename(filepath)=="brush.manifest":
            checkInst = self.manifest
        else:
            name,ext = os.path.splitext(filepath)
            if ext==".brushes":
                checkInst = self.checkBrushfile(os.path.basename(filepath))
            else:
                raise RuntimeError("Files of type "+filepath
                                   +"are not handled by ModBrushes.")
        if isinstance(checkInst,list):
            # There was a parsing problem with the given file, return it
            res.extend(checkInst)
        else:
            # There was no problem with the parsing, conduct the check
            res.extend(checkInst.check(self.mod))
        return res

    def checkManifest(self,entry):
        if isinstance(self.manifest,list):
            return self.manifest
        else:
            return self.manifest.checkEntry(entry)

    def gatherFiles(self,endings=set()):
        raise NotImplementedError()

    def gatherRefs(self,types=set()):
        raise NotImplementedError()

    def getBrushfile(self,brush):
        self.loadAll()
        foundIn = None
        for filename,brushfile in self.compFilesMod.items():
            if (isinstance(brushfile,BrushFile)
                    and brushfile.checkBrushExistence(brush)):
                foundIn = filename
                break
        return foundIn

    def loadAll(self):
        probs = []
        if not self._allLoaded:
            if isinstance(self.manifest,list):
                probs.extend(self.manifest)
            else:
                for entry in self.manifest.entries.elements:
                    if entry.ref not in self.compFilesMod:
                        self.compFilesMod[entry.ref] = None
            with tqdm.tqdm(disable=self.mod.quiet,total=len(self.compFilesMod),
                    desc="Loading Brush files") as pbar:
                for brushfile in self.compFilesMod:
                    probs.extend(self.loadBrushfile(brushfile))
                    pbar.update()
            self._allLoaded = True
            for prob in probs:
                if prob.probFile is None:
                    prob.probFile = "General Problems"
        return probs

    def loadBrushfile(self,name):
        probs = []
        if name not in self.compFilesMod or self.compFilesMod[name] is None:
            inst,probs = self.loadFile(self.subName,name,BrushFile)
            if inst is None:
                self.compFilesMod[name] = probs
            else:
                self.compFilesMod[name] = inst
        elif isinstance(self.compFilesMod[name],list):
            probs.extend(self.compFilesMod[name])
        return probs

    @property
    def manifest(self):
        if self._manifest is None:
            import pysoase.mod.manifest as manifest
            inst,probs = self.loadFile("","brush.manifest",
                    manifest.ManifestBrush)
            if inst is None:
                self._manifest = probs
            else:
                self._manifest = inst
        return self._manifest
