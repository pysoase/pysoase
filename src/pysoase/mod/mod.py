""" Contains classes working with a Sins mod as a whole.
"""

import collections
import os

import tqdm

import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
import pysoase.entities.component as ecomp
import pysoase.mod.audio as audio
import pysoase.mod.constants as const
import pysoase.mod.events as events
import pysoase.mod.particles as particles
import pysoase.mod.scenarios as scen
import pysoase.mod.texanim as anim
import pysoase.mod.themes as themes
import pysoase.mod.ui as ui
import pysoase.mod.visuals as vis

"""################################ Mod Class ##############################"""

class Mod(object):
    def __init__(self,moddir,rebrefdir=None,name=None,quiet=False):
        if not os.path.exists(moddir):
            raise FileNotFoundError("Moddir \'"+moddir+"\' does not exist.")
        if rebrefdir and not os.path.exists(rebrefdir):
            raise FileNotFoundError(
                    "Sins Reference file dir \'"+rebrefdir
                    +"\' does not exist.")
        self.moddir = moddir
        self.rebrefdir = rebrefdir
        self.addons = None
        self._audio = None
        self._brushes = None
        self._constants = None
        self._entities = None
        self._explosions = None
        self._gameEvents = None
        self._particles = None
        self._randEvents = None
        self._scenarios = None
        self._skyboxBackdrops = None
        self._starscape = None
        self._strings = None
        self._texanims = None
        self._themes = None
        self.dirlist_bundled = {}
        self.dirlist_rebref = {}
        self.dirlist_mod = {}
        self.quiet = quiet

    @property
    def audio(self):
        if self._audio is None:
            self._audio = audio.ModAudio(self)
        return self._audio

    @property
    def brushes(self):
        if self._brushes is None:
            self._brushes = ui.ModBrushes(self)
        return self._brushes

    @property
    def constants(self):
        if self._constants is None:
            inst,probs = self.loadFile("GameInfo","Gameplay.constants",
                    const.Constants)
            self._constants = inst if inst else probs
        return self._constants

    @property
    def entities(self):
        if self._entities is None:
            self._entities = ecomp.ModEntities(self)
        return self._entities

    @property
    def explosions(self):
        if self._explosions is None:
            inst,probs = self.loadFile("GameInfo","Explosions.explosiondata",
                    vis.Explosions)
            self._explosions = inst if inst else probs
        return self._explosions

    @property
    def gameEvents(self):
        if self._gameEvents is None:
            inst,probs = self.loadFile("GameInfo","GameEventData.gameeventdata",
                    events.GameEventDef)
            self._gameEvents = inst if inst else probs
        return self._gameEvents

    @property
    def texanims(self):
        if self._texanims is None:
            self._texanims = anim.ModTextureAnim(self)
        return self._texanims

    @property
    def themes(self):
        if self._themes is None:
            self._themes = themes.ModThemes(self)
        return self._themes

    @property
    def particles(self):
        if self._particles is None:
            self._particles = particles.ModParticles(self)
        return self._particles

    @property
    def randEvents(self):
        if self._randEvents is None:
            inst,probs = self.loadFile("GameInfo",
                    "RandomEventDefs.randomeventdefs",
                    events.RandomEventDef)
            self._randEvents = inst if inst else probs
        return self._randEvents

    @property
    def scenarios(self):
        if self._scenarios is None:
            self._scenarios = scen.ModScenarios(self)
        return self._scenarios

    @property
    def skyboxBackdrops(self):
        if self._skyboxBackdrops is None:
            inst,probs = self.loadFile("GameInfo",
                    "SkyBoxProperties.skyboxbackdropdata",
                    vis.SkyboxBackdrops)
            self._skyboxBackdrops = inst if inst else probs
        return self._skyboxBackdrops

    @property
    def starscape(self):
        if self._starscape is None:
            inst,probs = self.loadFile("GameInfo",
                    "Starscape.starscapedata",
                    vis.Starscape)
            self._starscape = inst if inst else probs
        return self._starscape

    @property
    def strings(self):
        if self._strings is None:
            self._strings = ui.ModStrings(self)
        return self._strings

    def loadAll(self):
        probs = []
        with tqdm.tqdm(disable=self.quiet,total=14,
                desc="Loading Mod") as pbar:
            probs.extend(self.audio.loadAll())
            pbar.update()
            probs.extend(self.brushes.loadAll())
            pbar.update()
            #        if isinstance(self.constants,list):
            #            probs.extend(self.constants)
            pbar.update()
            probs.extend(self.entities.loadAll())
            pbar.update()
            if isinstance(self.explosions,list):
                probs.extend(self.explosions)
            pbar.update()
            if isinstance(self.gameEvents,list):
                probs.extend(self.gameEvents)
            pbar.update()
            probs.extend(self.themes.loadAll())
            pbar.update()
            probs.extend(self.particles.loadAll())
            pbar.update()
            if isinstance(self.randEvents,list):
                probs.extend(self.randEvents)
            pbar.update()
            probs.extend(self.scenarios.loadAll())
            pbar.update()
            if isinstance(self.skyboxBackdrops,list):
                probs.extend(self.skyboxBackdrops)
            pbar.update()
            if isinstance(self.starscape,list):
                probs.extend(self.starscape)
            pbar.update()
            probs.extend(self.strings.loadAll())
            pbar.update()
            probs.extend(self.texanims.loadAll())
            pbar.update()
        return probs

    def check(self,strict=False):
        res = self.loadAll()
        with tqdm.tqdm(disable=self.quiet,total=14,
                desc="Checking mod") as pbar:
            #        if not isinstance(self.constants,list):
            #            res.extend(self.constants.check(self))
            pbar.update()
            if not isinstance(self.explosions,list):
                res.extend(self.explosions.check(self))
            pbar.update()
            if not isinstance(self.gameEvents,list):
                res.extend(self.gameEvents.check(self))
            pbar.update()
            if not isinstance(self.randEvents,list):
                res.extend(self.randEvents.check(self))
            pbar.update()
            if not isinstance(self.skyboxBackdrops,list):
                res.extend(self.skyboxBackdrops.check(self))
            pbar.update()
            if not isinstance(self.starscape,list):
                res.extend(self.starscape.check(self))
            pbar.update()
            res.extend(self.audio.check())
            pbar.update()
            res.extend(self.brushes.check(strict))
            pbar.update()
            res.extend(self.entities.check(strict))
            pbar.update()
            res.extend(self.themes.check(strict))
            pbar.update()
            res.extend(self.particles.check())
            pbar.update()
            res.extend(self.scenarios.check(strict))
            pbar.update()
            res.extend(self.strings.check())
            pbar.update()
            res.extend(self.texanims.check())
            pbar.update()
        return res

    def checkFile(self,filepath):
        res = []
        checkInst = None
        base = os.path.basename(filepath)
        if base=="brush.manifest":
            res.extend(self.brushes.checkFile(filepath))
        elif base=="entity.manifest":
            res.extend(self.entities.checkFile(filepath))
        elif base=="galaxy.manifest":
            res.extend(self.scenarios.checkFile(filepath))
        elif base=="playerThemes.manifest" or base=="playerPictures.manifest":
            res.extend(self.themes.checkFile(filepath))
        elif base=="Explosions.explosiondata":
            checkInst = self.explosions
        elif base=="GameEventData.gameeventdata":
            checkInst = self.gameEvents
        elif base=="RandomEventDefs.randomeventdefs":
            checkInst = self.randEvents
        elif base=="Starscape.starscapedata":
            checkInst = self.starscape
        elif base=="SkyBoxProperties.skyboxbackdropdata":
            checkInst = self.skyboxBackdrops
        else:
            raise RuntimeError("File checking for \'"+base+"\' not possible "
                                                           "through Mod class.")
        if isinstance(checkInst,co_basics.IsFile):
            res.extend(checkInst.check(self))
        elif isinstance(checkInst,list):
            res.extend(checkInst)
        return res

    def checkSyntax(self):
        pass

    def checkFileExistence(self,subpath,filename):
        return co_files.checkFileExistence(subpath,filename,self)

    def hasSubdir(self,subdir):
        return os.path.exists(os.path.join(self.moddir,subdir))

    def gatherFiles(self,endings=set()):
        res = collections.defaultdict(set)
        if not endings or ".ogg" in endings:
            self.audio.gatherFiles(res,endings)
        return res

    def gatherRefs(self,types=set()):
        res = collections.defaultdict(set)
        probs = []
        if not types or "audiofile" in types:
            probs.extend(self.audio.gatherRefs(res,types))
        if not types or not types.isdisjoint({"brush"}):
            if isinstance(self.gameEvents,list):
                probs.extend(self.gameEvents)
            else:
                self.gameEvents.gatherRefs(res,types)
        if not types or not types.isdisjoint({"string","entity"}):
            if isinstance(self.randEvents,list):
                probs.extend(self.randEvents)
            else:
                self.randEvents.gatherRefs(res,types)
        if not types or not types.isdisjoint({"texture"}):
            if isinstance(self.starscape,list):
                probs.extend(self.starscape)
            else:
                self.starscape.gatherRefs(res,types)
        if not types or not types.isdisjoint({"texture","mesh"}):
            if isinstance(self.skyboxBackdrops,list):
                probs.extend(self.skyboxBackdrops)
            else:
                self.skyboxBackdrops.gatherRefs(res,types)
        return res,probs

    def loadFile(self,subpath,filename,fclass):
        return co_files.loadFile(subpath,filename,self,fclass)
