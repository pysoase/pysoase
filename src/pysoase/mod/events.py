""" Contains parsers and classes for Sins events.
"""

import abc
import collections

import pyparsing as pp

import pysoase.common.attributes as co_attribs
import pysoase.common.basics as co_basics
from pysoase.common.misc import indentText
import pysoase.common.parsers as co_parse
import pysoase.common.problems as co_probs
import pysoase.entities.common as coe
import pysoase.mod.ui as ui

EVENTS = ["AllianceCeaseFireBroken","AllianceCeaseFireFormed",
    "AllianceCeaseFireOffered","AllianceOtherBroken","AllianceOtherFormed",
    "AllianceOtherOffered","AllyBackUpArrived","AllyBackUpEnRoute",
    "AllyTitanQueued","AllyTitanCompleted","EnemyTitanQueued",
    "EnemyTitanCompleted","AllyTitanLost","AllyCapitalShipLost",
    "AllyFleetArrived","AllyPlanetLost","AllyPlanetUnderAttack",
    "AllyRequestAttackPlanet","AllyRequestDefendPlanet",
    "AllyUnitUnderAttack","BountyAllDepleted","BountyIncreasedOther",
    "BountyIncreasedPlayer","BuyNoCommand","BuyNoCredits","BuyNoMetal",
    "BuyNoCrystal","BuyNoFrigateFactory","BuyNoCapitalShipFactory",
    "BuyNoTitanFactory","BuyNoAvailableShipSlots",
    "BuyNoAvailableCapitalShipSlots","BuyAlreadyHasTitan",
    "CannonShellDetected","RandomEventDetected","CannonUnderConstruction",
    "ColonizePlanetAlreadyOwned","ColonizePlanetNotColonizable",
    "ColonizePlanetAlreadyBeingColonized","ColonizeNeedPrerequisiteResearch",
    "ColonizePlanetHasTakeoverCulture","ColonizePlanetHasDebuff",
    "ColonizePlanetBlockedByEnemyStarBase","PirateFleetArrived",
    "PirateRaidImminent","PirateRaidLaunched","HostileFleetArrived",
    "HostileFleetEnRoute","HostilePlanetDestroyed","ModuleComplete",
    "MoveCannotTravelBetweenPlanets","NoValidPath","NoValidPathObserved",
    "NoValidPathBetweenStars","NoValidPathDueToBuff","Ping",
    "PlanetUpgradeAlreadyMaxLevel","PlanetUpgradeComplete",
    "PlanetUpgradeMaxLevelQueued","NewPlanetDiscovered","ArtifactDiscovered",
    "ArtifactDiscoveredOther","BonusDiscovered","NothingDiscovered",
    "LaunchSquadsDisabledByDebuff","PlayerBackUpArrived",
    "PlayerTitanCreated","PlayerTitanLost","PlayerCapitalShipCreated",
    "PlayerCapitalShipLost","PlayerStarbaseLost","PlayerFleetArrived",
    "PlayerFleetArrivedToFleetBeacon","PlayerFleetDeparted",
    "PlayerFrigateCreated","PlayerPlanetColonized",
    "PlayerCapitalPlanetChanged","PlayerCapitalPlanetLost",
    "AllyCapitalPlanetLost","EnemyCapitalPlanetLost","PlayerPlanetLost",
    "PlayerPlanetLostToCulture","PlayerPlanetSoonLostToCulture",
    "PlayerPlanetUnderAttack","PlayerReceivedResourcesFromAlly",
    "PlayerUnitUnderAttack","PlayerFlagshipUnderAttack",
    "PlayerFlagshipShieldsDown","PlayerFlagshipHullSeverelyDamaged",
    "PlayerFlagshipDestroyed","AllyFlagshipDestroyed",
    "EnemyFlagshipDestroyed","PlayerTitanShieldsDown",
    "PlayerTitanHullSeverelyDamaged","PlayerCapitalShipShieldsDown",
    "PlayerCapitalShipHullSeverelyDamaged","AchievementCompleted",
    "QuestAdded","QuestCompleted","QuestFailed","QuestEnded","QuestRejected",
    "PlayerQuestAdded","PlayerQuestCompleted","PlayerQuestFailed",
    "DiplomaticVictoryHalfway","DiplomaticVictoryComplete",
    "AllyDiplomaticVictoryHalfway","AllyDiplomaticVictoryComplete",
    "EnemyDiplomaticVictoryHalfway","EnemyDiplomaticVictoryComplete",
    "OccupationVictoryStarted","OccupationVictoryHalfway",
    "OccupationVictoryComplete","AllyOccupationVictoryStarted",
    "AllyOccupationVictoryHalfway","AllyOccupationVictoryComplete",
    "EnemyOccupationVictoryStarted","EnemyOccupationVictoryHalfway",
    "EnemyOccupationVictoryComplete","ResearchVictoryStarted",
    "ResearchVictoryHalfway","ResearchVictoryComplete","ResearchComplete",
    "AllyResearchVictoryStarted","AllyResearchVictoryHalfway",
    "AllyResearchVictoryComplete","EnemyResearchVictoryStarted",
    "EnemyResearchVictoryHalfway","EnemyResearchVictoryComplete",
    "ResearchAlreadyMaxLevel","ResearchMaxLevelQueued",
    "ResearchNotEnoughPointsInTier","ResearchNotEnoughPointsInTier",
    "ResearchNeedMoreLabModules","ResearchNeedPrerequisite",
    "MoreModuleSlotsNeeded","SpaceMineLimitReached",
    "StarBaseUpgradeNotEnoughUpgradePoints","StarBaseUpgradeAlreadyMaxLevel",
    "StarBaseUpgradeMaxLevelQueued","TitanUpgradeNotEnoughUpgradePoints",
    "TitanUpgradeAlreadyMaxLevel","InvalidModulePlacementPosition",
    "AbilityUpgradeAlreadyMaxLevel","AbilityUpgradeRequiresHigherLevel",
    "AbilityUpgradeNoUnspentPoints","ShipUpgradeAlreadyMaxLevel",
    "ShipUpgradeAlreadyQueuedMaxLevel","UseAbilityCooldownNotExpired",
    "UseAbilityDisabledByDebuff","UseAbilityNoAntimatter",
    "UseAbilityNoUndockedSquadMembers","UseAbilityNotEnoughRoomForStarBase",
    "UseAbilityCannotUseAbitraryLocationOnEntity",
    "UseAbilityTooManyStarBases","UseAbilityMineIsNotActivated",
    "UseAbilityTargetNotInRange","UseAbilityNoValidObjectType",
    "UseAbilityPhaseSpaceTarget","UseAbilityNormalSpaceTarget",
    "UseAbilityUnmetConstraintCanBeCaptured",
    "UseAbilityUnmetConstraintCanHaveFighters",
    "UseAbilityUnmetConstraintCanHaveShields",
    "UseAbilityUnmetConstraintCanSpreadWeaponDamage",
    "UseAbilityUnmetConstraintCanPhaseDodge",
    "UseAbilityUnmetConstraintHasAbilityWithCooldown",
    "UseAbilityUnmetConstraintHasAntimatter",
    "UseAbilityUnmetConstraintHasPhaseMissiles",
    "UseAbilityUnmetConstraintHasPopulation",
    "UseAbilityUnmetConstraintHasEnergyWeapons",
    "UseAbilityUnmetConstraintHasHullDamage",
    "UseAbilityUnmetConstraintHasShieldDamage",
    "UseAbilityUnmetConstraintHasAntiMatterShortage",
    "UseAbilityUnmetConstraintHasWeapons",
    "UseAbilityUnmetConstraintIsCargoShip",
    "UseAbilityUnmetConstraintIsExplored",
    "UseAbilityUnmetConstraintIsResourceExtractor",
    "UseAbilityUnmetConstraintIsMovingToLocalOrbitBody",
    "UseAbilityUnmetConstraintIsPsi",
    "UseAbilityUnmetConstraintLinkedCargoShip",
    "UseAbilityUnmetConstraintLinkedSquad",
    "UseAbilityUnmetConstraintNotSelf",
    "UseAbilityUnmetConstraintWithinSolarSystem",
    "UseAbilityUnmetConstraintModuleUnderConstruction",
    "UseAbilityUnmetConstraintModuleIsActivelyBeingConstructed",
    "UseAbilityUnmetConstraintModuleNotActivelyBeingConstructed",
    "UseAbilityUnmetConstraintIsNotHomeworld",
    "UseAbilityUnmetConstraintNotInvulnerable",
    "UseAbilityUnmetConstraintIsInFriendlyGravityWell",
    "UseAbilityUnmetConstraintIsInNonFriendlyGravityWell",
    "UseAbilityUnmetOwnerLinked","UseAbilityUnmetOwnerPlayer",
    "UseAbilityUnmetOwnerFriendly","UseAbilityUnmetOwnerHostile",
    "UseAbilityUnmetOwnerNoOwner","UseAbilityUnmetOwnerAlliedOrEnemy",
    "GameStart","GameWin","AllyGameWin","CapitalShipLevelUp",
    "AttackTargetInvalidWithNoReason",
    "AttackTargetInvalidAsNotDamagableDueToBuffs",
    "AttackTargetInvalidAsNotEnemy",
    "AttackTargetInvalidAsCantFireAtFighters",
    "AttackTargetInvalidAsCantMoveAndTargetNotInRange",
    "AttackTargetInvalidAsCantMoveToTarget",
    "AttackTargetInvalidAsCanOnlyAttackStructures",
    "AttackTargetInvalidAsMustBeInSameOrbitWell",
    "AttackTargetInvalidAsNoWeaponsToBombPlanet",
    "AttackTargetInvalidAsPlanetCantBeBombed","ScuttleShipStarted",
    "ScuttleShipStopped","ScuttlePlanetStarted","ScuttlePlanetStopped",
    "MarketBoom","MarketCrash","PlayerCultureFlippedEnemyPlanet",
    "EnvoyTargetPlanetIsNowInvalid","OffensiveMissionDiscovered",
    "MissionDiscovered","RuinPlanetNotRuinable"]
EVENT_CATEGORIES = ["Alliance","Default","Ping","Production","Threat"]
SPAWN_TYPES = ["BuffAtLocation","PeriodicBuffAtLocation","Projectile",
    "RacialBuffAtLocation"]
PROJECTILE_TARGETS = ["RandomConnectedLocation"]
SPAWN_REQS = ["Timer","TriggerCultureFlip","TriggerLowAllegiance"]
OWNER_TYPES = ["Invalid","LocationOwner","MadVasari","Null","Occupation",
    "Pirate","Rebel"]
SPAWN_LOCATIONS = ["AllLocations","Invalid","RandomLocation",
    "RandomLocationSpace","RandomNonHomeworld","RandomPirateBase",
    "RandomPlayerAllLocations","RandomStar","TriggerLocation"]

"""################################# Parsers ###############################"""

"""### GameEvents ###"""

g_GameEvent = pp.Group(
        co_parse.g_linestart
        +pp.Literal("GameEventDef:").suppress()
        +pp.oneOf(EVENTS)("identifier")
        +co_parse.g_eol
        +co_parse.genEnumAttrib("categoryId",EVENT_CATEGORIES)("category")
        +co_parse.genStringAttrib("hudButtonBrush")("icon")
        +co_parse.genDecimalAttrib("entityThrottleTime")("entityThrottle")
        +co_parse.genDecimalAttrib("globalSoundThrottleTime")("soundThrottle")
        +co_parse.genStringAttrib("thottleSharedGameEventId")("throttledEvent")
        +co_parse.genBoolAttrib("onlyAddIfNotFocused")("addIfNotFocused")
        +co_parse.genBoolAttrib("onlyAddIfSelected")("addIfSelected")
        +co_parse.genBoolAttrib("onlyAddIfNotInView")("addIfNotInView")
        +co_parse.genBoolAttrib("shouldRemoveAllSimilarEvents")(
                "removeSimilarEvents")
)

g_cullList = pp.Group(
        co_parse.genHeading("cullList")
        +co_parse.genIntAttrib("maxCount")("maxCount")
        +co_parse.genListAttrib("eventTypeCount",
                co_parse.genStringAttrib("event"))(
                "eventTypes")
)

g_GameEventDefs = (
    co_parse.g_txt
    +co_parse.genIntAttrib("resetSelectedGameEventTime")(
            "resetSelectedEventTime")
    +co_parse.genIntAttrib("showNewGameEventTime")("showNewEventTime")
    +co_parse.genIntAttrib("fadeGameEventTime")("fadeEventTime")
    +co_parse.genIntAttrib("removeExpiredGameEventTime")(
            "removeExpiredEventTime")
    +pp.OneOrMore(g_GameEvent)("events")
    +co_parse.genListAttrib("cullLists",g_cullList)("cullLists")
).ignore(pp.dblSlashComment)

"""### RandomEvents ###"""

g_spawnProjectile = pp.Group(
        co_parse.genKeyValPair("spawnType","Projectile")("spawnType")
        +co_parse.genEnumAttrib("projectileTarget",PROJECTILE_TARGETS)("target")
        +co_parse.genStringAttrib("spawnEntity")("entity")
)

g_racialBuff = pp.Group(
        co_parse.genStringAttrib("race")("race")
        +co_parse.genStringAttrib("buff")("buff")
)

g_racialBuffAtLoc = pp.Group(
        co_parse.genKeyValPair("spawnType","RacialBuffAtLocation")("spawnType")
        +co_parse.genListAttrib("racialBuffCount",g_racialBuff)("buffs")
)

g_buffAtLoc = pp.Group(
        co_parse.genKeyValPair("spawnType","BuffAtLocation")("spawnType")
        +co_parse.genStringAttrib("spawnEntity")("buff")
)

g_periodicBuffAtLoc = pp.Group(
        co_parse.genKeyValPair("spawnType","PeriodicBuffAtLocation")("spawnType")
        +co_parse.genStringAttrib("spawnEntity")("buff")
        +co_parse.genDecimalAttrib("duration")("duration")
        +co_parse.genDecimalAttrib("period")("period")
)

g_spawnType = pp.Or([g_spawnProjectile,g_racialBuffAtLoc,
    g_buffAtLoc,g_periodicBuffAtLoc])("spawnType")

g_spawnTimer = pp.Group(
        co_parse.genKeyValPair("spawnRequirement","Timer")("spawnReq")
        +pp.Optional(
                co_parse.genIntAttrib("respawnCount")("respawn"))
        +co_parse.genDecimalAttrib("nextSpawnTimeMin")("nextSpawnTimeMin")
        +co_parse.genDecimalAttrib("nextSpawnTimeMax")("nextSpawnTimeMax")
)

g_spawnWeighted = pp.Group(
        co_parse.genEnumAttrib("spawnRequirement",["TriggerCultureFlip",
            "TriggerLowAllegiance"])("spawnReq")
        +co_parse.genDecimalAttrib("triggerWeight")("weight")
)

g_spawnRequirement = pp.Or([g_spawnTimer,g_spawnWeighted])("spawnReq")

g_randomEvent = pp.Group(
        co_parse.genHeading("randomEventDef")
        +co_parse.genStringAttrib("id")("id")
        +co_parse.genStringAttrib("nameID")("name")
        +co_parse.genStringAttrib("warningID")("warning")
        +g_spawnType
        +co_parse.genEnumAttrib("playerOwnerType",OWNER_TYPES)("ownerType")
        +co_parse.genEnumAttrib("spawnLocation",SPAWN_LOCATIONS)("location")
        +co_parse.genListAttrib("excludedSpawnLocationCount",
                co_parse.genStringAttrib("excludedSpawnLocation"))(
                        "excludedLocs")
        +g_spawnRequirement
        +co_parse.genDecimalAttrib("spawnWeight")("spawnWeight")
)

g_RandomEventDefs = (
    co_parse.g_txt
    +co_parse.genDecimalAttrib("initialSpawnTimeMin")("initialSpawnTimeMin")
    +co_parse.genDecimalAttrib("initialSpawnTimeMax")("initialSpawnTimeMax")
    +co_parse.genDecimalAttrib("failedRespawnTimeMin")("failedRespawnTimeMin")
    +co_parse.genDecimalAttrib("failedRespawnTimeMax")("failedRespawnTimeMax")
    +co_parse.genDecimalAttrib("lowAllegianceCheckPeriod")(
            "lowAllegianceCheckPeriod")
    +co_parse.genDecimalAttrib("lowAllegianceThreshold")("lowAllegianceThresh")
    +co_parse.genListAttrib("randomEventDefCount",g_randomEvent)("events")
).ignore(pp.dblSlashComment)

"""################################ Attributes #############################"""

"""### GameEvents ###"""

class GameEventRef(co_attribs.FileLocalRef):
    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if (self.ref and self.ref!="Invalid"
        and self.ref not in self.parent.events):
            res.append(
                    co_probs.MissingRefProblem("GameEvent",self.ref,
                    probLine=self.linenum))
        return res

class GameEvent(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,parent,category,icon,entityThrottle,soundThrottle,
            throttledEvent,addIfNotFocused,addIfSelected,addIfNotInView,
            removeSimilarEvents,**kwargs):
        super().__init__(**kwargs)
        self.category = co_attribs.AttribEnum(possibleVals=EVENT_CATEGORIES,**category)
        self.icon = ui.BrushRef(canBeEmpty=True,**icon)
        self.entityThrottle = co_attribs.AttribNum(**entityThrottle)
        self.soundThrottle = co_attribs.AttribNum(**soundThrottle)
        self.throttledEvent = GameEventRef(parent=parent,**throttledEvent)
        self.addIfNotFocused = co_attribs.AttribBool(**addIfNotFocused)
        self.addIfSelected = co_attribs.AttribBool(**addIfSelected)
        self.addIfNotInView = co_attribs.AttribBool(**addIfNotInView)
        self.removeSimilarEvents = co_attribs.AttribBool(**removeSimilarEvents)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.category==other.category)
                        and (self.icon==other.icon)
                        and (self.entityThrottle==other.entityThrottle)
                        and (self.soundThrottle==other.soundThrottle)
                        and (self.throttledEvent==other.throttledEvent)
                        and (self.addIfNotFocused==other.addIfNotFocused)
                        and (self.addIfSelected==other.addIfSelected)
                        and (self.addIfNotInView==other.addIfNotInView)
                        and (
                        self.removeSimilarEvents==other.removeSimilarEvents))

    def toString(self,indention):
        res = super().toString(0)
        res += "GameEventDef:"+self.identifier+"\n"
        res += self.category.toString(1)
        res += self.icon.toString(1)
        res += self.entityThrottle.toString(1)
        res += self.soundThrottle.toString(1)
        res += self.throttledEvent.toString(1)
        res += self.addIfNotFocused.toString(1)
        res += self.addIfSelected.toString(1)
        res += self.addIfNotInView.toString(1)
        res += self.removeSimilarEvents.toString(1)
        return indentText(res,indention)

class CullList(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,maxCount,eventTypes,parent,**kwargs):
        super().__init__(**kwargs)
        self.maxCount = co_attribs.AttribNum(**maxCount)
        self.eventTypes = co_attribs.AttribList(elemType=GameEventRef,
                elemArgs={"parent":parent},
                **eventTypes)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.maxCount==other.maxCount)
                        and (self.eventTypes==other.eventTypes))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.maxCount.toString(1)
        res += self.eventTypes.toString(1)
        return indentText(res,indention)

"""### RandomEvents ###"""

class SpawnType(co_attribs.Attribute):
    def __init__(self,spawnType,**kwargs):
        kwargs["identifier"] = ""
        super().__init__(**kwargs)
        self.spawnType = co_attribs.AttribEnum(possibleVals=SPAWN_TYPES,**spawnType)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.spawnType==other.spawnType)

    @classmethod
    def factory(cls,spawnType,**kwargs):
        t = spawnType["val"]
        if t=="Projectile":
            return SpawnProjectile(spawnType=spawnType,**kwargs)
        elif t=="BuffAtLocation":
            return BuffAtLoc(spawnType=spawnType,**kwargs)
        elif t=="RacialBuffAtLocation":
            return RacialBuffAtLoc(spawnType=spawnType,**kwargs)
        elif t=="PeriodicBuffAtLocation":
            return PeriodicBuffAtLoc(spawnType=spawnType,**kwargs)
        else:
            raise RuntimeError("Invalid Random Event Spawn Type "+t)

    @abc.abstractmethod
    def toString(self,indention):
        res = super().toString(0)
        res += self.spawnType.toString(0)
        return indentText(res,indention)

class SpawnProjectile(SpawnType,co_basics.HasReference):
    def __init__(self,target,entity,**kwargs):
        super().__init__(**kwargs)
        self.target = co_attribs.AttribEnum(possibleVals=PROJECTILE_TARGETS,**target)
        self.entity = coe.EntityRef(types=["CannonShell"],**entity)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.target==other.target)
                        and (self.entity==other.entity))

    def toString(self,indention):
        res = super().toString(0)
        res += self.target.toString(0)
        res += self.entity.toString(0)
        return indentText(res,indention)

class RacialBuff(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,race,buff,**kwargs):
        kwargs["identifier"] = ""
        super().__init__(**kwargs)
        self.race = ui.StringReference(**race)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.race==other.race)
                        and (self.buff==other.buff))

    def toString(self,indention):
        res = super().toString(0)
        res += self.race.toString(0)
        res += self.buff.toString(0)
        return indentText(res,indention)

class RacialBuffAtLoc(SpawnType,co_basics.HasReference):
    def __init__(self,buffs,**kwargs):
        super().__init__(**kwargs)
        self.buffs = co_attribs.AttribList(elemType=RacialBuff,**buffs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buffs==other.buffs)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buffs.toString(0)
        return indentText(res,indention)

class BuffAtLoc(SpawnType,co_basics.HasReference):
    def __init__(self,buff,**kwargs):
        super().__init__(**kwargs)
        self.buff = coe.EntityRef(types=["Buff"],**buff)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.buff==other.buff)

    def toString(self,indention):
        res = super().toString(0)
        res += self.buff.toString(0)
        return indentText(res,indention)

class PeriodicBuffAtLoc(BuffAtLoc):
    def __init__(self,duration,period,**kwargs):
        super().__init__(**kwargs)
        self.duration = co_attribs.AttribNum(**duration)
        self.period = co_attribs.AttribNum(**period)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.duration==other.duration)
                        and (self.period==other.period))

    def toString(self,indention):
        res = super().toString(0)
        res += self.duration.toString(0)
        res += self.period.toString(0)
        return indentText(res,indention)

class SpawnRequirement(co_attribs.Attribute):
    def __init__(self,spawnReq,**kwargs):
        kwargs["identifier"] = ""
        super().__init__(**kwargs)
        self.spawnReq = co_attribs.AttribEnum(possibleVals=SPAWN_REQS,**spawnReq)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.spawnReq==other.spawnReq)

    @classmethod
    def factory(cls,spawnReq,**kwargs):
        r = spawnReq["val"]
        if r=="Timer":
            return SpawnTimer(spawnReq=spawnReq,**kwargs)
        elif r=="TriggerLowAllegiance" or r=="TriggerCultureFlip":
            return SpawnWeighted(spawnReq=spawnReq,**kwargs)
        else:
            raise RuntimeError("Invalid Random Event Spawn requirement "+r)

    @abc.abstractmethod
    def toString(self,indention):
        res = super().toString(0)
        res += self.spawnReq.toString(0)
        return indentText(res,indention)

class SpawnTimer(SpawnRequirement):
    def __init__(self,nextSpawnTimeMin,nextSpawnTimeMax,respawn=None,**kwargs):
        super().__init__(**kwargs)
        self.nextSpawnTimeMin = co_attribs.AttribNum(**nextSpawnTimeMin)
        self.nextSpawnTimeMax = co_attribs.AttribNum(**nextSpawnTimeMax)
        if respawn:
            self.respawn = co_attribs.AttribNum(**respawn)
        else:
            self.respawn = None

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.nextSpawnTimeMin==other.nextSpawnTimeMin)
                        and (self.nextSpawnTimeMax==other.nextSpawnTimeMax)
                        and (self.respawn==other.respawn))

    def toString(self,indention):
        res = super().toString(0)
        if self.respawn:
            res += self.respawn.toString(0)
        res += self.nextSpawnTimeMin.toString(0)
        res += self.nextSpawnTimeMax.toString(0)
        return indentText(res,indention)

class SpawnWeighted(SpawnRequirement):
    def __init__(self,weight,**kwargs):
        super().__init__(**kwargs)
        self.weight = co_attribs.AttribNum(**weight)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.weight==other.weight)

    def toString(self,indention):
        res = super().toString(0)
        res += self.weight.toString(0)
        return indentText(res,indention)

class RandomEvent(co_attribs.Attribute,co_basics.HasReference):
    def __init__(self,id,name,warning,spawnType,ownerType,location,
            excludedLocs,spawnReq,spawnWeight,**kwargs):
        super().__init__(**kwargs)
        self.id = co_attribs.AttribString(**id)
        self.name = ui.StringReference(**name)
        self.warning = ui.StringReference(**warning)
        self.spawnType = SpawnType.factory(**spawnType)
        self.ownerType = co_attribs.AttribEnum(possibleVals=OWNER_TYPES,**ownerType)
        self.location = co_attribs.AttribEnum(possibleVals=SPAWN_LOCATIONS,**location)
        self.excludedLocs = co_attribs.AttribList(
                elemType=coe.EntityRef,
                elemArgs={"types":["Planet","Star"]},
                **excludedLocs
        )
        self.spawnReq = SpawnRequirement.factory(**spawnReq)
        self.spawnWeight = co_attribs.AttribNum(**spawnWeight)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.id==other.id)
                        and (self.name==other.name)
                        and (self.warning==other.warning)
                        and (self.spawnType==other.spawnType)
                        and (self.ownerType==other.ownerType)
                        and (self.location==other.location)
                        and (self.excludedLocs==other.excludedLocs)
                        and (self.spawnReq==other.spawnReq)
                        and (self.spawnWeight==other.spawnWeight))

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier+"\n"
        res += self.id.toString(1)
        res += self.name.toString(1)
        res += self.warning.toString(1)
        res += self.spawnType.toString(1)
        res += self.ownerType.toString(1)
        res += self.location.toString(1)
        res += self.excludedLocs.toString(1)
        res += self.spawnReq.toString(1)
        res += self.spawnWeight.toString(1)
        return indentText(res,indention)

"""################################# Classes ###############################"""

class RandomEventDef(co_basics.IsFile,co_basics.HasReference):
    parser = g_RandomEventDefs

    def __init__(self,initialSpawnTimeMin,initialSpawnTimeMax,
            failedRespawnTimeMin,failedRespawnTimeMax,lowAllegianceCheckPeriod,
            lowAllegianceThresh,events,**kwargs):
        super().__init__(**kwargs)
        self.initialSpawnTimeMin = co_attribs.AttribNum(**initialSpawnTimeMin)
        self.initialSpawnTimeMax = co_attribs.AttribNum(**initialSpawnTimeMax)
        self.failedRespawnTimeMin = co_attribs.AttribNum(**failedRespawnTimeMin)
        self.failedRespawnTimeMax = co_attribs.AttribNum(**failedRespawnTimeMax)
        self.lowAllegianceCheckPeriod = co_attribs.AttribNum(**lowAllegianceCheckPeriod)
        self.lowAllegianceThresh = co_attribs.AttribNum(**lowAllegianceThresh)
        self.events = co_attribs.AttribList(elemType=RandomEvent,**events)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.initialSpawnTimeMin==other.initialSpawnTimeMin)
                        and (
                        self.initialSpawnTimeMax==other.initialSpawnTimeMax)
                        and (
                        self.failedRespawnTimeMin==other.failedRespawnTimeMin)
                        and (
                        self.failedRespawnTimeMax==other.failedRespawnTimeMax)
                        and (
                        self.lowAllegianceCheckPeriod==other.lowAllegianceCheckPeriod)
                        and (
                        self.lowAllegianceThresh==other.lowAllegianceThresh)
                        and (self.events==other.events))

    def gatherRefs(self,res,types=set()):
        for e in self.events.elements:
            if not types or "string" in types:
                res["string"].add(e.name.ref)
                res["string"].add(e.warning.ref)
                if isinstance(e.spawnType,RacialBuffAtLoc):
                    for b in e.spawnType.buffs.elements:
                        res["string"].add(b.race.ref)
            if not types or "entity" in types:
                if isinstance(e.spawnType,RacialBuffAtLoc):
                    for b in e.spawnType.buffs.elements:
                        res["entity"].add(b.buff.ref)
                elif isinstance(e.spawnType,BuffAtLoc):
                    res["entity"].add(e.spawnType.buff.ref)
                elif isinstance(e.spawnType,SpawnProjectile):
                    res["entity"].add(e.spawnType.entity.ref)
                for loc in e.excludedLocs.elements:
                    res["entity"].add(loc.ref)
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.initialSpawnTimeMin.toString(0)
        res += self.initialSpawnTimeMax.toString(0)
        res += self.failedRespawnTimeMin.toString(0)
        res += self.failedRespawnTimeMax.toString(0)
        res += self.lowAllegianceCheckPeriod.toString(0)
        res += self.lowAllegianceThresh.toString(0)
        res += self.events.toString(0)
        return indentText(res,indention)

class GameEventDef(co_basics.IsFile,
        co_basics.HasReference):
    parser = g_GameEventDefs

    def __init__(self,resetSelectedEventTime,showNewEventTime,fadeEventTime,
            removeExpiredEventTime,events,cullLists,**kwargs):
        super().__init__(**kwargs)
        self.resetSelectedEventTime = co_attribs.AttribNum(**resetSelectedEventTime)
        self.showNewEventTime = co_attribs.AttribNum(**showNewEventTime)
        self.fadeEventTime = co_attribs.AttribNum(**fadeEventTime)
        self.removeExpiredEventTime = co_attribs.AttribNum(**removeExpiredEventTime)
        self._events = collections.OrderedDict()
        for e in events:
            self._events[e["identifier"]] = GameEvent(parent=self,**e)
        self.cullLists = co_attribs.AttribList(elemType=CullList,
                elemArgs={"parent":self},
                **cullLists)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.resetSelectedEventTime==other.resetSelectedEventTime)
                        and (self.showNewEventTime==other.showNewEventTime)
                        and (self.fadeEventTime==other.fadeEventTime)
                        and (self.removeExpiredEventTime==other.removeExpiredEventTime)
                        and (self.events==other.events)
                        and (self.cullLists==other.cullLists))

    @property
    def events(self):
        return self._events

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        for e in self._events.values():
            res.extend(e.check(currMod,recursive))
        #        for t in EVENTS:
        #            if t not in self._events:
        #                res.append(co.BasicProblem("Could not find event "+t,
        #                    severity=co.ProblemSeverity.error))
        return res

    def gatherRefs(self,res,types=set()):
        if not types or "brush" in types:
            for e in self.events.values():
                res["brush"].add(e.icon.ref)
        return res

    def getRefs(self):
        res = super().getRefs()
        res.extend([e.icon for e in self.events.values()])
        return res

    def toString(self,indention):
        res = super().toString(0)
        res += self.resetSelectedEventTime.toString(0)
        res += self.showNewEventTime.toString(0)
        res += self.fadeEventTime.toString(0)
        res += self.removeExpiredEventTime.toString(0)
        for e in self.events.values():
            res += e.toString(0)
        res += self.cullLists.toString(0)
        return indentText(res,indention)
