""" This module provides the capability to compare lists of problems.
"""

import argparse
import glob
import os
import pickle
import sys

import pysoase.tools.tools as tools

def findNewProblems(oldProbs,newProbs):
    addedProbs = []
    if oldProbs:
        for prob in newProbs:
            if prob not in oldProbs:
                addedProbs.append(prob)
    else:
        addedProbs = newProbs
    return addedProbs

def loadProbs(path):
    probFiles = glob.glob(os.path.join(path,"*.probs"))
    newProbs = None
    oldProbs = None
    if len(probFiles)==1:
        with open(probFiles[0],'rb') as f:
            newProbs = pickle.load(f)
            oldProbs = []
    elif len(probFiles)>1:
        oldPath,newPath = sorted(probFiles)[-2:]
        with open(newPath,'rb') as n,open(oldPath,'rb') as o:
            newProbs = pickle.load(n)
            oldProbs = pickle.load(o)
    return oldProbs,newProbs

def evaluateProblems(probDir):
    output = ""
    retVal = 0
    if not os.path.exists(probDir):
        raise FileNotFoundError("Could not find problem directory "
                                +probDir+", aborting.")
    else:
        print("Loading problems...")
        oldProbs,currProbs = loadProbs(probDir)
        if oldProbs is None and currProbs is None:
            output = "No problem files found, aborting."
            retVal = 1
        else:
            print("Preparing problem output...")
            output += tools.printProblems(currProbs,quiet=True)
            print("Comparing problem sets...")
            newProbs = findNewProblems(oldProbs,currProbs)
            output += "\n"
            if newProbs:
                output += ("Of these, the following problems were newly "
                           +"introduced:\n\n")
                output += tools.printProblems(newProbs,False,quiet=True)
                output += ("Found "+str(len(newProbs))+" new "+(
                    "problems" if len(newProbs)>1 else "problem")+".\n")
                retVal = 1
            else:
                output += "None of these were newly introduced."
        return retVal,output

def main():
    parser = argparse.ArgumentParser(description="Compare sets of PySoaSE "
                                                 +"problems and output newly "
                                                 +"introduced errors.")
    parser.add_argument("probDir",metavar="<PROBLEM_DIRECTORY>",
            help="A directory containing files with pickled lists of PySoaSE "
                 +"problems.")
    args = parser.parse_args()
    retVal,output = evaluateProblems(args.probDir)
    sys.stdout.write(output)
    sys.exit(retVal)
