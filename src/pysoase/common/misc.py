""" Common misc functions which don't fit anywhere else
"""
import textwrap

def indentText(text,indentation):
    return textwrap.indent(text,"\t"*indentation)