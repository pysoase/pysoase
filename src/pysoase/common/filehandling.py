""" Classes and functions to handle mod files
"""
import enum
import json
import os

import pkg_resources as pkg_res

import pysoase.common.problems as co_probs

class FileSource(enum.IntEnum):
    missing = 0
    bundled = 1
    rebref = 2
    vanilla = 3
    mod = 4

VANILLA_MANIFEST = json.loads(pkg_res.resource_string("pysoase",
        "resources/vanilla.manifest").decode())
VANILLA_MANIFEST_LOW = {k:[f.lower() for f in v] for k,v in
    VANILLA_MANIFEST.items()}
BUNDLE_PATH = pkg_res.resource_filename("pysoase","resources/reference/")

def checkFileExistence(subpath,filename,currMod):
    modpath = os.path.join(currMod.moddir,subpath,filename)
    modsub = os.path.join(currMod.moddir,subpath)
    bundledSub = os.path.join(BUNDLE_PATH,subpath)
    if os.path.exists(modsub) and modsub not in currMod.dirlist_mod:
        currMod.dirlist_mod[modsub] = [entry.lower() for entry in
            os.listdir(modsub)]
    if currMod.rebrefdir:
        rebrefsub = os.path.join(currMod.rebrefdir,subpath)
        if rebrefsub not in currMod.dirlist_rebref and os.path.exists(
                rebrefsub):
            currMod.dirlist_rebref[rebrefsub] = [entry.lower() for entry
                in os.listdir(rebrefsub)]
    if os.path.exists(bundledSub) and bundledSub not in currMod.dirlist_bundled:
        currMod.dirlist_bundled[bundledSub] = [entry.lower() for entry in
            os.listdir(bundledSub)]

    if (modsub in currMod.dirlist_mod
            and filename.lower() in currMod.dirlist_mod[modsub]):
        return FileSource.mod,[]
    elif (currMod.rebrefdir
            and os.path.exists(rebrefsub)
            and filename.lower() in currMod.dirlist_rebref[rebrefsub]):
        return FileSource.rebref,[co_probs.UsingRebFileProblem(filename)]
    elif (bundledSub in currMod.dirlist_bundled
            and filename.lower() in currMod.dirlist_bundled[bundledSub]):
        return FileSource.bundled,[co_probs.UsingRebFileProblem(filename)]
    elif (subpath in VANILLA_MANIFEST_LOW
            and filename.lower() in VANILLA_MANIFEST_LOW[subpath]):
        return FileSource.vanilla,[co_probs.UsingRebFileProblem(filename)]
    else:
        return FileSource.missing,[co_probs.MissingRefProblem(
                "File",os.path.basename(modpath))]

def loadFile(subpath,filename,currMod,fclass):
    fsource,probs = checkFileExistence(subpath,filename,currMod)
    path = None
    inst = None
    if fsource==FileSource.mod:
        path = os.path.join(currMod.moddir,subpath,filename)
    elif fsource==FileSource.rebref:
        path = os.path.join(currMod.rebrefdir,subpath,filename)
    elif fsource==FileSource.bundled:
        path = os.path.join(BUNDLE_PATH,subpath,filename)
    if path:
        path = correctCaseInsensitive(path)
        inst = fclass.createInstance(path)
        if isinstance(inst,list):
            probs.extend(inst)
            inst = None
    return inst,probs

def correctCaseInsensitive(path):
    if os.path.exists(path):
        return path
    else:
        dirname,filename = os.path.split(os.path.abspath(path))
        for f in os.listdir(dirname):
            if filename.lower()==f.lower():
                return os.path.join(dirname,f)
        else:
            return None
