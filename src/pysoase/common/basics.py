""" The roots of PySoaSE's class hierarchy
"""
import abc
import glob
import itertools as it
import os.path

import pyparsing as pp

import pysoase.common.filehandling as co_files
import pysoase.common.problems as co_probs
from pysoase.common.misc import indentText
from pysoase.common.parsers import g_default

"""############################ Mod Components #############################"""

class ModComponent(abc.ABC):
    def __init__(self,currMod,subpath,compFileExt):
        self.mod = currMod
        self.subName = subpath
        tmp_subpath = os.path.abspath(os.path.join(currMod.moddir,subpath))
        self.compFilesMod = {}
        self.compFilesVanilla = {}
        if os.path.exists(tmp_subpath) and compFileExt:
            for f in glob.glob(os.path.join(tmp_subpath,"*."+compFileExt)):
                self.compFilesMod[os.path.basename(f)] = None
        if subpath in co_files.VANILLA_MANIFEST and compFileExt:
            self.compFilesVanilla = {f:None if self.mod.rebrefdir
                else [co_probs.UsingRebFileProblem(f)]
                for f in co_files.VANILLA_MANIFEST[subpath]}

    def _checkFileExistence(self,subpath,filename):
        return co_files.checkFileExistence(subpath,filename,self.mod)

    @abc.abstractmethod
    def check(self,strict=False):
        return []

    @abc.abstractmethod
    def checkFile(self,filepath):
        return []

    @abc.abstractmethod
    def gatherFiles(self,res,endings=[]):
        raise NotImplementedError()

    @abc.abstractmethod
    def gatherRefs(self,res,types=[]):
        raise NotImplementedError()

    @abc.abstractmethod
    def loadAll(self):
        raise NotImplementedError()

    def loadFile(self,subpath,filename,fclass):
        return co_files.loadFile(subpath,filename,self.mod,fclass)

"""############################# Basic Classes #############################"""

class Checkable(abc.ABC):
    def __init__(self,ignore=False,**kwargs):
        super().__init__(**kwargs)
        self.checked = False
        self.cache = None
        self.ignore = ignore

    def attribCheck(self,currMod,recursive=False):
        res = []
        if not self.checked:
            [res.extend(val.check(currMod,recursive))
                for k,val in vars(self).items()
                if hasattr(val,"check") and k!="parent"]
            self.checked = True
            self.cache = res
        else:
            res = self.cache
        return res

    def check(self,currMod,recursive=False):
        res = []
        if not self.ignore:
            if not self.checked:
                res.extend(self.attribCheck(currMod,recursive))
                res.extend(self.localCheck(currMod,recursive))
                seen = []
                for prob in res:
                    if prob not in seen:
                        seen.append(prob)
                res = seen
                self.checked = True
                self.cache = res
            else:
                res = self.cache
        return res

    def localCheck(self,currMod,recursive=False):
        return []

class HasReference(object):
    def __init__(self,**kwargs):
        self.refsChecked = False
        self.refProbCache = None
        super().__init__(**kwargs)

    def directRefs(self):
        from pysoase.common.attributes import AttribRef
        return [val for val in vars(self).values() if isinstance(val,AttribRef)]

    def attribRefs(self):
        res = []
        for attrib in [val for val in vars(self).values()
                if hasattr(val,"getRefs")]:
            res.extend(attrib.getRefs())
        return res

    def getRefs(self):
        res = []
        res.extend(self.directRefs())
        res.extend(self.attribRefs())
        return res

    def checkDirectRefs(self,currMod):
        from pysoase.common.attributes import AttribRef
        res = it.chain.from_iterable([val.check(currMod)
            for val in vars(self).values() if isinstance(val,AttribRef)])
        if res is None:
            return []
        else:
            return res

    def checkAttribRefs(self,currMod):
        res = it.chain.from_iterable([val.checkRefs(currMod)
            for val in vars(self).values() if hasattr(val,"checkRefs")])
        if res is None:
            return []
        else:
            return res

    def checkRefs(self,currMod):
        res = []
        if not self.refsChecked:
            res.extend(self.checkAttribRefs(currMod))
            res.extend(self.checkDirectRefs(currMod))
            self.refProbCache = res
            self.refsChecked = True
        else:
            res = self.refProbCache
        return res

class IsFile(Checkable,abc.ABC):
    parser = g_default

    def __init__(self,filepath,**kwargs):
        super().__init__(**kwargs)
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \""+filepath+"\" does not exist.")
        self.filepath = filepath
        self.changed = False

    def check(self,currMod,recursive=False):
        res = super().check(currMod,recursive)
        for prob in res:
            if not prob.probFile:
                prob.probFile = os.path.basename(self.filepath)
        return res

    @classmethod
    def createInstance(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        succ,parseRes = cls.parse(filepath)
        if succ:
            return cls(filepath=filepath,**parseRes)
        else:
            return parseRes

    def getRefs(self):
        res = super().getRefs()
        for ref in res:
            ref.source = os.path.basename(self.filepath)
        return res

    def modify(self,modifications):
        changes = []
        attributes = [a for a in vars(self).values() if hasattr(a,"modify")]
        for attrib in attributes:
            changes.extend(attrib.modify(modifications))
        self.changed = self.changed or changes
        for c in changes:
            c.modFile = os.path.basename(self.filepath)
        return changes

    @classmethod
    def parse(cls,filepath):
        if not os.path.exists(filepath):
            raise FileNotFoundError("File \'"+filepath+"\' does not exist.")
        res = []
        success = False
        try:
            with open(filepath,'r',encoding="utf-8-sig") as f:
                res = cls.parser.parseString(f.read())
                success = True
        except pp.ParseException as e:
            res = [co_probs.ParseProblem(filepath,e,probFile=filepath)]
        except UnicodeDecodeError:
            res = [co_probs.BasicProblem(
                    desc="Problem with file encoding of "+filepath+". Perhapts "
                         "it is a BIN format file instead of TXT?",
                    probFile=filepath,
                    severity=co_probs.ProblemSeverity.error)]
        return success,res

    @abc.abstractmethod
    def toString(self,indention):
        return indentText("TXT\n",indention)

    def writeOut(self,force=False,filepath=None):
        if filepath is None:
            filepath = self.filepath
        if self.changed or force:
            with open(filepath,"w") as f:
                f.write(self.toString(0))