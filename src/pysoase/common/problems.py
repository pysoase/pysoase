""" Problems describing errors and warnings found in a Sins Mod
"""

import enum
import os.path

from pysoase.common.misc import indentText

class ProblemSeverity(enum.IntEnum):
    info = 1
    warn = 2
    error = 3

    @classmethod
    def toSeverity(cls,sev):
        return cls[sev]

class Problem(object):
    def __init__(self,severity,probFile=None,probLine=None):
        if not isinstance(severity,ProblemSeverity):
            raise ValueError("Given severity is not a valid ProblemSeverity"
                             "value.")
        self.severity = severity
        self.probFile = probFile
        self.probLine = probLine

    def __eq__(self,other):
        res = ((self.__class__==other.__class__)
               and (self.severity==other.severity)
               and (self.probFile==other.probFile))
        return res

    def toString(self,tabs):
        res = "{severity}:{line} ".format(severity=self.severity.name.upper(),
                line="l "+str(self.probLine)+":" if self.probLine else "")
        return res

class BasicProblem(Problem):
    def __init__(self,desc,**kwargs):
        super().__init__(**kwargs)
        self.desc = desc

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.desc==other.desc)

    def toString(self,tabs):
        res = super().toString(tabs)
        res += self.desc+"\n"
        return indentText(res,tabs)

class DuplicateEntryProb(Problem):
    def __init__(self,count,key,lstName,**kwargs):
        super().__init__(ProblemSeverity.warn,**kwargs)
        if count<1:
            raise ValueError("Count must be larger than 0.")
        self.count = int(count)
        self.key = key
        self.lstName = lstName

    def toString(self,indention):
        res = super().toString(0)
        res += "{} duplicate entries were found for {} in {}.\n".format(
                self.count,self.key,self.lstName)
        return indentText(res,indention)

    def __eq__(self,other):
        res = super().__eq__(other)
        return (res and (self.count==other.count) and (self.key==other.key)
                and (self.lstName==other.lstName))

class ParseProblem(Problem):
    def __init__(self,parsedFile,parseExcept,**kwargs):
        self.parsedFile = parsedFile
        self.lineNum = parseExcept.lineno
        self.line = parseExcept.markInputline()
        self.err = parseExcept.__str__()
        super().__init__(ProblemSeverity.error,**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.parsedFile==other.parsedFile)
                        and (self.lineNum==other.lineNum)
                        and (self.line==other.line)
                        and (self.err==other.err))

    def toString(self,tabs):
        res = super().toString(tabs)
        res += "The following syntax error was found in file {0}\n".format(
                self.parsedFile)
        res = indentText(res,tabs)
        tabs += 1
        err = "line: \"{line}\"\n".format(line=self.line)
        err += "error: {err}\n".format(err=self.err)
        return res+indentText(err,tabs)

class MissingRefProblem(Problem):
    def __init__(self,refdesc,ref,**kwargs):
        super().__init__(severity=ProblemSeverity.error,**kwargs)
        self.refdesc = refdesc
        self.ref = ref

    def toString(self,tabs):
        res = super().toString(tabs)
        res += "Referenced {0} \'{1}\' could not be resolved.\n".format(
                self.refdesc,self.ref)
        return indentText(res,tabs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.refdesc==other.refdesc) and (
            self.ref==other.ref)

class UsingRebFileProblem(Problem):
    def __init__(self,filepath,**kwargs):
        super().__init__(severity=ProblemSeverity.info,**kwargs)
        self.filepath = os.path.basename(filepath)

    def toString(self,indention):
        res = super().toString(0)
        res += ("Could not find \'{}\' in Mod. Using vanilla file instead.\n"
        ).format(self.filepath)
        return indentText(res,indention)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.filepath==other.filepath)