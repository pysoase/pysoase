""" Constants commonly used by several PySoaSE modules
"""
EXT_TO_FILEREF = {
    ".ogg":"audiofile"
}
FILEREF_TO_EXT = {
    "audiofile":".ogg"
}