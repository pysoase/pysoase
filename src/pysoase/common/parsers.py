""" Common parsers used by multiple PySoaSE modules
"""
import sys

import pyparsing as pp

"""### Some Usefull Whitespaces ####"""

g_space = pp.White(" ").suppress()
g_linestart = pp.LineStart().leaveWhitespace().suppress()
g_eolNoAction = pp.LineEnd().suppress()
g_eol = pp.LineEnd().suppress().setParseAction(lambda s,l,t:pp.lineno(l,s))(
        "linenum")

"""### Basic Elements ###"""

g_int = pp.Combine(
        pp.WordStart().suppress()
        +pp.Optional("-")
        +pp.Word(pp.nums)
        +pp.Optional(
                pp.Literal(".")+pp.OneOrMore("0")
        )
        +pp.WordEnd(pp.nums).suppress()
)
g_true = pp.Literal("TRUE")
g_false = pp.Literal("FALSE")
g_pos2 = pp.Group(
        pp.Literal("[").suppress()
        +pp.Word(pp.nums).setParseAction(lambda t:int(t[0]))
        +pp.Literal(",").suppress()
        +pp.Word(pp.nums).setParseAction(lambda t:int(t[0]))
        +pp.Literal("]").suppress()
)("coord")
g_pos4 = pp.Group(
        pp.Literal("[").suppress()
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums).setParseAction(
                lambda t:int(t[0]))
        +pp.Literal(",").suppress()
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums).setParseAction(
                lambda t:int(t[0]))
        +pp.Literal(",").suppress()
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums).setParseAction(
                lambda t:int(t[0]))
        +pp.Literal(",").suppress()
        +pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums).setParseAction(
                lambda t:int(t[0]))
        +pp.Literal("]").suppress()
)("coord")
g_txt = (
    pp.StringStart().suppress()
    +pp.Literal("TXT")
    +g_eol
).suppress()
g_default = g_txt+pp.StringEnd().suppress()

"""### Generator Functions ####"""

def genBoolAttrib(keyword,expected=None):
    """Generate parser element for boolean attributes.

    This element accepts whole lines of the form 'IDENTIFIER TRUE|FALSE'.
    """
    val = pp.Or([g_true,g_false])
    if expected is not None:
        val = expected
    return pp.Group(
            g_linestart
            +pp.Keyword(keyword)("identifier")
            +g_space
            +val("val").setParseAction(lambda t:t[0]=="TRUE")
            +g_eol
    )

def genColorAttrib(keyword):
    return pp.Group(g_linestart
                    +pp.Keyword(keyword)("identifier")
                    +g_space
                    +pp.Word(pp.hexnums,max=8,min=1)("val")
                    +g_eol
    )

def genDecimalAttrib(keyword):
    """Generate parser element for decimal attributes.

    This element accepts whole lines of the form 'IDENTIFIER [-][0-9\.]+'.
    """
    return pp.Group(g_linestart
                    +pp.Keyword(keyword)("identifier")
                    +g_space
                    +pp.Or([
                            pp.Word(initChars=pp.nums+"-",bodyChars=pp.nums+"."),
                            pp.Word(initChars=".",bodyChars=pp.nums)
                            ]).setParseAction(lambda t:float(t[0]))("val")
                    +g_eol
    )

def genEnumAttrib(keyword,permitted):
    return pp.Group(g_linestart
                    +pp.Keyword(keyword)("identifier")
                    +g_space
                    +pp.oneOf(["\""+x+"\"" for x in permitted]
                    ).addParseAction(pp.removeQuotes)("val")
                    +g_eol
    )

def genHeading(keyword):
    return (
        g_linestart
        +pp.Keyword(keyword)("identifier")
        +g_eolNoAction
    )

def genIntAttrib(keyword):
    return pp.Group(g_linestart
                    +pp.Keyword(keyword)("identifier")
                    +g_space
                    +g_int.setParseAction(
                                        lambda s,l,t:int(float(t[0])),
                                        callDuringTry=True
                    )("val")
                    +g_eol
    )

def genKeyValPair(keyword,value):
    return pp.Group(
            g_linestart
            +pp.Keyword(keyword)("identifier")
            +g_space
            +pp.Literal("\"").suppress()
            +pp.Literal(value)("val")
            +pp.Literal("\"").suppress()
            +g_eol
    )


def genStringAttrib(keyword):
    return pp.Group(g_linestart
                    +pp.Keyword(keyword)("identifier")
                    +g_space
                    +(pp.Literal("\"").suppress()
                      +(pp.Regex("[^\"]*")
                      | pp.Empty().leaveWhitespace().setParseAction(
                                                                    lambda t:""
                        ))("val")
                      +pp.Literal("\"").suppress()
                    )
                    +g_eol
    )

def genListAttrib(countKeyword,elem,header=None):
    array = pp.Forward().leaveWhitespace()
    counter = genIntAttrib(countKeyword)("counter")

    def setArray(s,l,t):
        n = t[0][1]
        if n:
            array<<(pp.And([pp.Empty().leaveWhitespace()+elem]*n))("elements")
        else:
            array<<pp.Empty().leaveWhitespace()

    counter.setParseAction(setArray,callDuringTry=True)
    combined = counter+array
    if header:
        combined = (
            genHeading(header)
            +combined
        )
    return pp.Group(combined)