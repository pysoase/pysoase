""" Common Sins file attributes
"""
import abc
import collections
import copy
import itertools as it
import ntpath
import os

import pysoase.common.problems as co_probs
import pysoase.common.basics as co_basics
import pysoase.common.filehandling as co_files
from pysoase.common.misc import indentText

class Attribute(co_basics.Checkable,abc.ABC):
    def __init__(self,identifier,linenum=None,**kwargs):
        super().__init__(**kwargs)
        self.identifier = identifier
        self.linenum = linenum
        self.modified = False

    @abc.abstractmethod
    def toString(self,indention):
        return ""

    @abc.abstractmethod
    def __eq__(self,other):
        return self.identifier==other.identifier

    def modify(self,modifications):
        changes = []
        if not self.modified:
            self.modified = True
            attributes = [a for a in vars(self).values() if hasattr(a,"modify")]
            for attrib in attributes:
                changes.extend(attrib.modify(modifications))
            for mod in modifications:
                if mod.fits(self):
                    changes.append(mod.apply(self))
        return changes

class BasicAttrib(Attribute):
    @property
    @abc.abstractmethod
    def value(self):
        raise NotImplementedError()

    @value.setter
    @abc.abstractmethod
    def value(self,newVal):
        raise NotImplementedError()

class AttribBool(BasicAttrib):
    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier,**kwargs)
        self._value = val

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self,newVal):
        self._value = newVal

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.value==other.value)

    def toString(self,indention):
        res = super().toString(0)
        res += self.identifier
        res += " "
        if self.value:
            res += "TRUE"
        else:
            res += "FALSE"
        res += "\n"
        return indentText(res,indention)

class AttribColor(BasicAttrib):
    _maxColVal = int("0xffffffff",16)

    def __init__(self,val,**kwargs):
        super().__init__(**kwargs)
        self.col = int(val,16)

    @property
    def value(self):
        return self.col

    @value.setter
    def value(self,newVal):
        self.col = int(newVal,16)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.col==other.col)

    def toString(self,indention):
        res = super().toString(0)
        res += "{} {}\n".format(self.identifier,hex(self.col)[2:])
        return indentText(res,indention)

class AttribEnum(BasicAttrib):
    _errorMsg = "The value \'{}\' is invalid. Possible values are: {}"

    def __init__(self,identifier,val,possibleVals,**kwargs):
        if not possibleVals:
            raise ValueError("The list of possible values must not be empty")
        super().__init__(identifier,**kwargs)
        self._value = val
        self.possibleValues = possibleVals

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self,newval):
        if newval in self.possibleValues:
            self._value = newval
        else:
            msg = self._errorMsg.format(newval,str(self.possibleValues))
            raise ValueError(msg)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self._value in self.possibleValues:
            return res
        else:
            msg = self._errorMsg.format(self._value,str(self.possibleValues))
            res.append(co_probs.BasicProblem(msg,
                    severity=co_probs.ProblemSeverity.error,
                    probLine=self.linenum))
            return res

    def toString(self,indentation):
        res = super().toString(indentation)
        res += "{} \"{}\"\n".format(self.identifier,self._value)
        return indentText(res,indentation)

    def __eq__(self,other):
        return (super().__eq__(other)
                and self.value==other.value
                and self.possibleValues==other.possibleValues)

class AttribList(Attribute,co_basics.HasReference):
    def __init__(self,counter,elemType,elemArgs=None,elements=[],
            identifier=None,
            factory=False,**kwargs):
        super().__init__(identifier=identifier,**kwargs)
        self._counter = AttribNum(**counter)
        self._elements = []
        for elem in elements:
            args = dict(elem)
            if elemArgs:
                args.update(elemArgs)
            if factory:
                self._elements.append(elemType.factory(**args))
            else:
                self._elements.append(elemType(**args))

    def __eq__(self,other):
        if self.identifier is None and other.identifier is None:
            res = True
        else:
            res = super().__eq__(other)
        if (self.elements is not None) and (other.elements is None):
            return False
        elif (self.elements is None) and (other.elements is not None):
            return False
        elif (self.elements is not None) and (other.elements is not None):
            if len(self.elements)!=len(other.elements):
                return False
            else:
                for own,others in zip(self.elements,other.elements):
                    if own!=others:
                        return False
        return res and (self._counter==other._counter)

    def __len__(self):
        return len(self._elements)

    @property
    def counter(self):
        return self._counter.value

    @property
    def elements(self):
        return self._elements

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if not res:
            res = []
        for elem in self.elements:
            res.extend(elem.check(currMod))
        if self.counter>len(self.elements):
            msg = ("There are fewer ({0}) elements in the list than the"
                   " given count \'{1}\' ({2})").format(len(self.elements),
                    self._counter.identifier,
                    self.counter)
            res.append(co_probs.BasicProblem(msg,severity=co_probs.ProblemSeverity.error,
                    probLine=self._counter.linenum))
        elif self.counter<len(self.elements):
            msg = ("There are more ({0}) elements in the list than the"
                   " given count \'{1}\' ({2})").format(len(self.elements),
                    self._counter.identifier,
                    self.counter)
            res.append(co_probs.BasicProblem(msg,severity=co_probs.ProblemSeverity.error,
                    probLine=self._counter.linenum))
        return res

    def checkRefs(self,currMod):
        res = []
        if not self.refsChecked and self.elements:
            if isinstance(self.elements[0],AttribRef):
                res = it.chain.from_iterable([elem.check(currMod)
                    for elem in self.elements])
            elif isinstance(self.elements[0],co_basics.HasReference):
                res = it.chain.from_iterable([elem.checkRefs(currMod)
                    for elem in self.elements])
            self.refProbCache = res
            self.refsChecked = True
        else:
            res = self.refProbCache
        if not res:
            return []
        return list(res)

    def getRefs(self):
        res = []
        if self.elements:
            for elem in self.elements:
                if isinstance(elem,AttribRef):
                    res.append(elem)
                elif isinstance(elem,co_basics.HasReference):
                    res.extend(elem.getRefs())
        return res

    def toString(self,indentation):
        res = super().toString(indentation)
        localIndent = 0
        if self.identifier:
            res += self.identifier
            res += "\n"
            localIndent += 1
        res += self._counter.toString(localIndent)
        for elem in self.elements:
            res += elem.toString(localIndent)
        return indentText(res,indentation)

    def modify(self,modifications):
        changes = []
        if not self.modified:
            changes = super().modify(modifications)
            # Apply changes to list's elments
            for elem in self._elements:
                if hasattr(elem,"modify"):
                    changes.extend(elem.modify(modifications))
        return changes

class AttribListKeyed(AttribList):
    def __init__(self,counter,elemType,keyfunc,elemArgs=None,elements=[],
            identifier=None,factory=False):
        super().__init__(counter=counter,elemType=elemType,
                identifier=identifier)
        self.elemsSorted = collections.OrderedDict()
        self.keyfunc = keyfunc
        for elem in elements:
            args = dict(elem)
            if elemArgs:
                args.update(elemArgs)
            if factory:
                nelem = elemType.factory(**args)
            else:
                nelem = elemType(**args)
            self.elemsSorted.setdefault(keyfunc(nelem),[]).append(nelem)
        self._elements = list(it.chain.from_iterable(self.elemsSorted.values()))

    def __contains__(self,val):
        return val in self.elemsSorted

    @property
    def elements(self):
        return copy.copy(self._elements)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        res.extend(self.checkDuplicates())
        return res

    def checkDuplicates(self):
        res = []
        dups = [val for key,val in self.elemsSorted.items() if len(val)>1]
        if self.identifier:
            lstName = self.identifier
        else:
            lstName = self._counter.identifier
        for duplist in dups:
            if duplist:
                res.append(co_probs.DuplicateEntryProb(len(duplist),
                        self.keyfunc(duplist[0]),lstName))
        return res

    def removeDuplicates(self):
        dups = [[x for x in val if val.count(x)>1]
            for key,val in self.elemsSorted.items() if len(val)>1]
        res = []
        for duplist in dups:
            if duplist:
                vallist = self.elemsSorted[self.keyfunc(duplist[0])]
                for x in duplist:
                    for i in range(0,vallist.count(x)-1):
                        vallist.remove(x)
                        res.append(x)
        self._elements = list(it.chain.from_iterable(self.elemsSorted.values()))
        self._counter.value -= len(res)
        return res

    def getElement(self,elem):
        return self.elemsSorted[elem][0]

class AttribNum(BasicAttrib):
    def __init__(self,identifier,val,**kwargs):
        super().__init__(identifier,**kwargs)
        self._value = val

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self,newVal):
        self._value = newVal

    def __eq__(self,other):
        return super().__eq__(other) and (self.value==other.value)

    def toString(self,indention):
        res = super().toString(indention)
        if not isinstance(self._value,int):
            res += "{0} {1:f}\n".format(self.identifier,self.value)
        else:
            res += "{0} {1:d}\n".format(self.identifier,self.value)
        return indentText(res,indention)

    @classmethod
    def factory(cls,values):
        if "identifier" in values and "val" in values:
            return AttribNum(**values)
        elif "startVal" in values and "perLvlInc" in values:
            return AttribVarLevelable(**values)
        elif "levels" in values:
            return AttribFixedLevelable(**values)
        else:
            raise RuntimeError("Invalid value for AttribNum factory:"
                               +str(values))

class AttribVarLevelable(AttribNum):
    def __init__(self,startVal,perLvlInc,**kwargs):
        self.startVal = AttribNum(**startVal)
        self.perLvlInc = AttribNum(**perLvlInc)
        super().__init__(val=self.startVal.value,**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and ((self.startVal==other.startVal)
                        and (self.perLvlInc==other.perLvlInc))

    def toString(self,indention):
        res = self.identifier+"\n"
        res += self.startVal.toString(1)
        res += self.perLvlInc.toString(1)
        return indentText(res,indention)

    def setLvl(self,newLvl):
        if newLvl>0:
            self.value = self.startVal.value+(self.perLvlInc.value*(newLvl-1))
        else:
            self.value = self.startVal.value

class AttribFixedLevelable(AttribNum):
    def __init__(self,levels,**kwargs):
        self.levels = tuple(map(lambda val:AttribNum(**val),levels))
        super().__init__(val=self.levels[0].value,**kwargs)

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.levels==other.levels)

    def toString(self,indention):
        res = self.identifier+"\n"
        for i in self.levels:
            res += i.toString(1)
        return indentText(res,indention)

    def setLvl(self,newLvl):
        if 0<=newLvl<=3:
            self.value = self.levels[newLvl].value
        else:
            raise RuntimeError(str(newLvl)+" is and invalid value. The value "
                               +"must be in the interval [0,3].")

class AttribPos4d(BasicAttrib):
    def __init__(self,identifier,coord,**kwargs):
        super().__init__(identifier,**kwargs)
        self.pos = (coord[0],coord[1],coord[2],coord[3])

    @property
    def value(self):
        return self.pos

    @value.setter
    def value(self,newVal):
        if len(newVal)==4:
            self.pos = newVal
        else:
            raise ValueError("Values for Pos4d attributes need to have "
                             "a length of 4.")

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.pos==other.pos)

    def toString(self,indention):
        res = super().toString(0)
        x,y,z,a = self.pos
        res += self.identifier
        res += " "
        if not isinstance(x,int) \
                or not isinstance(y,int)\
                or not isinstance(z,int)\
                or not isinstance(a,int):
            res += "[ {0:f} , {1:f} , {2:f} , {3:f} ]\n".format(x,y,z,a)
        else:
            res += "[ {0:d} , {1:d} , {2:d} , {3:d} ]\n".format(x,y,z,a)
        return indentText(res,indention)

class AttribPos2d(BasicAttrib):
    def __init__(self,identifier,coord,**kwargs):
        super().__init__(identifier,**kwargs)
        self.pos = (coord[0],coord[1])

    @property
    def value(self):
        return self.pos

    @value.setter
    def value(self,newVal):
        if len(newVal)==2:
            self.pos = newVal
        else:
            raise ValueError("Values for Pos2d attributes need to have "
                             "a length of 2.")

    def __eq__(self,other):
        res = super().__eq__(other)
        return res and (self.pos==other.pos)

    def toString(self,indention):
        res = super().toString(0)
        x,y = self.pos
        res += self.identifier
        res += " "
        if not isinstance(x,int) \
                or not isinstance(y,int):
            res += "[{0:f},{1:f}]\n".format(x,y)
        else:
            res += "[{0:d},{1:d}]\n".format(x,y)
        return indentText(res,indention)

class AttribString(BasicAttrib):
    def __init__(self,identifier,val=None,**kwargs):
        super().__init__(identifier,**kwargs)
        if not val:
            self._value = ""
        else:
            self._value = val

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self,newVal):
        self._value = newVal

    def __eq__(self,other):
        return super().__eq__(other) and (self.value==other.value)

    def toString(self,indention):
        res = super().toString(0)
        res += "{} \"{}\"\n".format(self.identifier,self.value)
        return indentText(res,indention)

class AttribRef(BasicAttrib):
    def __init__(self,identifier,val,canBeEmpty=False,source=None,**kwargs):
        super().__init__(identifier,**kwargs)
        self.ref = val
        self.canBeEmpty = canBeEmpty
        self.source = source

    @property
    def value(self):
        return self.ref

    @value.setter
    def value(self,newVal):
        self.ref = newVal

    def __eq__(self,other):
        return super().__eq__(other) and ((self.ref==other.ref)
                                          and (
                                          self.canBeEmpty==other.canBeEmpty))

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if not self.ref and not self.canBeEmpty:
            res.append(co_probs.BasicProblem("Reference "+self.identifier+" is empty",
                    severity=co_probs.ProblemSeverity.info,
                    probLine=self.linenum))
        return res

    def toString(self,indention):
        res = super().toString(indention)
        res += "{0} \"{1}\"\n".format(self.identifier,self.ref)
        return indentText(res,indention)

class FileLocalRef(AttribRef):
    def __init__(self,parent,**kwargs):
        super().__init__(**kwargs)
        self.parent = parent

    def __eq__(self,other):
        return super().__eq__(other)

class FileRef(AttribRef):
    _REFTYPE = "File"

    def __init__(self,identifier,val,subdir,extension=None,**kwargs):
        self._vanilla = val
        root,ext = os.path.splitext(val)
        self.extensions = extension
        if extension and val:
            if not ext:
                val = ".".join([root,extension])
            elif ext!="."+extension and ext.lower()=="."+extension.lower():
                val = ".".join([root,extension])
            elif ext!="."+extension:
                val = ".".join([val,extension])
        super().__init__(identifier,ntpath.basename(val),**kwargs)
        self.subdir = subdir

    @AttribRef.value.setter
    def value(self,newVal):
        self._vanilla = newVal
        root,ext = os.path.splitext(newVal)
        if self.extensions and newVal:
            if not ext:
                newVal = ".".join([root,self.extensions])
            elif (ext!="."+self.extensions
                    and ext.lower()=="."+self.extensions.lower()):
                newVal = ".".join([root,self.extensions])
            elif ext!="."+self.extensions:
                newVal = ".".join([newVal,self.extensions])
        self.ref = ntpath.basename(newVal)

    def __eq__(self,other):
        return (super().__eq__(other) and (self.subdir==other.subdir)
                and self._vanilla==other._vanilla)

    def localCheck(self,currMod,recursive=False):
        res = super().localCheck(currMod,recursive)
        if self.ref:
            res.extend(self.checkExistence(currMod,recursive))
            for prob in res:
                if not prob.probLine:
                    prob.probLine = self.linenum
        return res

    def checkExistence(self,currMod,recursive=False):
        fsource,probs = co_files.checkFileExistence(self.subdir,self.ref,currMod)
        return probs

    def toString(self,indention):
        res = "{0} \"{1}\"\n".format(self.identifier,self._vanilla)
        return indentText(res,indention)