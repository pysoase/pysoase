#!/usr/bin/python3
""" Script to generate vanilla reference

This script takes the vanilla Sins directory and generates a file manifest
for use by PySoaSE.
"""

import argparse
import os
import glob
import json
import sys

def getVanillaFileListing(vanillaPath):
    fileDict = {"":[os.path.basename(p) for p in glob.glob(
            os.path.join(vanillaPath,"*.manifest"))],
        "Galaxy":os.listdir(os.path.join(vanillaPath,"Galaxy")),
        "GameInfo":os.listdir(os.path.join(vanillaPath,"GameInfo")),
        "Mesh":os.listdir(os.path.join(vanillaPath,"Mesh")),
        "Particle":os.listdir(os.path.join(vanillaPath,"Particle")),
        "PipelineEffect":os.listdir(os.path.join(vanillaPath,
                "PipelineEffect")),
        "Sound":os.listdir(os.path.join(vanillaPath,"Sound")),
        "String":os.listdir(os.path.join(vanillaPath,"String")),
        "TextureAnimations":os.listdir(os.path.join(vanillaPath,
                "TextureAnimations")),
        "Textures":os.listdir(os.path.join(vanillaPath,"Textures")),
        "Window":os.listdir(os.path.join(vanillaPath,"Window"))}
    return fileDict

def genVanillaManifest(vanillaPath):
    fileDict = getVanillaFileListing(vanillaPath)
    manifestPath = os.path.join(os.path.dirname(sys.argv[0]),
            "pysoase","resources","vanilla.manifest")
    if not os.path.exists(os.path.dirname(manifestPath)):
        os.mkdir(os.path.dirname(manifestPath))
    with open(manifestPath,'w') as f:
        json.dump(fileDict,f)

def main():
    parser = argparse.ArgumentParser(description="Generate vanilla file "
                                                 +"manifest for PysoaSE.")
    parser.add_argument("vanilla",help="Path to the vanilla root directory.")
    args = parser.parse_args()
    genVanillaManifest(args.vanilla)

if __name__=="__main__":
    main()
