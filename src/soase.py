#!/usr/bin/python3.4
""" Command line launcher for the PySoaSE library
"""
from pysoase.frontends.cli import entry

if __name__=="__main__":
    entry()
