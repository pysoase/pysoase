#!/usr/bin/python3
""" Script to copy all important Sins files into the package

Important Sins files are those which are needed for the checking of mods. While 
PySoaSE only needs to know whether certain files, like Entities, exist, the 
story is different for files like the *.sounddata files. A mod may not have 
any sound files of it's own and use only vanilla sounds. In such a case,
PySoaSE still needs to check whether the referenced sound entry actually exists.
And for that, PySoaSE needs access to the actual vanilla *.sounddata files. 
It is not enough to simply know whether the *.sounddata file exists. Thus,
certain vnailla files need to be distributed with PySoaSE to be able to run it 
on machines which don't have Sins installed. Or which only have the Sins files 
in BIN format.
"""

import argparse
import os
import shutil
import sys

# Dict with all Sins files which need to be copied, by their subdirectory.
NEEDED_FILES = {
    "":["brush.manifest","entity.manifest","galaxy.manifest",
        "playerPictures.manifest","playerThemes.manifest","skybox.manifest"],
    "GameInfo":["AsteroidDef.asteroidDef","Gameplay.constants",
        "Explosions.explosiondata","GalaxyScenarioDef.galaxyScenarioDef",
        "DustCloudsDef.renderingDef","SoundDialogue.sounddata",
        "SoundEffects.sounddata","SoundMusic.sounddata"],
    "Window":["Achievements.brushes","GameEvent.brushes","GameOver.brushes",
        "HUD.brushes","HUDIcon-Ability.brushes","HUDIcon-Action.brushes",
        "HUDIcon-Generic.brushes","HUDIcon-Module.brushes",
        "HUDIcon-Planet.brushes","HUDIcon-Research.brushes",
        "HUDIcon-Ship.brushes","HUDIcon-StarBase.brushes",
        "InfoCardIcon-Generic.brushes","InfoCardIcon-Module.brushes",
        "InfoCardIcon-Planet.brushes","InfoCardIcon-Ship.brushes",
        "LoadScreen.brushes","MainViewIcon-Generic.brushes",
        "MainViewIcon-Module.brushes","MainViewIcon-Planet.brushes",
        "MainViewIcon-Ship.brushes","Picture-Generic.brushes",
        "Picture-Module.brushes","Picture-Planet.brushes",
        "Picture-Ship.brushes","Screen-FrontEnd.brushes",
        "Screen-Generic.brushes","Screen-InGame.brushes"]
}

def copyReferenceFiles(refpath):
    basedir = os.path.join(os.path.dirname(sys.argv[0]),
            "pysoase","resources","reference")
    if not os.path.exists(basedir):
        os.mkdir(basedir)
    for dir,flist in NEEDED_FILES.items():
        currdir = os.path.join(basedir,dir)
        if not os.path.exists(currdir):
            os.mkdir(currdir)
        vanilladir = os.path.join(refpath,dir)
        for f in flist:
            shutil.copy(os.path.join(vanilladir,f),currdir)

def main():
    parser = argparse.ArgumentParser(description="Copy important vanilla files "
                                                 +"for PysoaSE.")
    parser.add_argument("ref",
            help="Path to directory with vanilla TXT format files.")
    args = parser.parse_args()
    copyReferenceFiles(args.ref)

if __name__=="__main__":
    main()
