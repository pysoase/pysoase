#!/usr/bin/env python3
""" Script to download most recent artifact from GitLab

This script uses the GitLab API to download the artifact file from the 
most recent successful pysoase_run CI job.
"""

import argparse
from zipfile import ZipFile

import requests

def getRecentSuccBuild(token,proj,ref):
    print("Determining most recent build...")
    url = "https://gitlab.com/api/v3/projects/"+proj+"/builds"
    headers = {"PRIVATE-TOKEN":token}
    params = {"scope":"success"}
    req = requests.get(url,headers=headers,params=params)
    if req.status_code == requests.codes.ok:
        builds = req.json()
        pysoase_jobs = [j for j in builds if j.get("name")=="pysoase_run" 
                and j.get("artifacts_file") and j.get("ref")==ref]
        if pysoase_jobs:
            skey = lambda j: j["finished_at"]
            current = sorted(pysoase_jobs,key=skey)[-1]
            return str(current["id"])
    else:
        print("Request to get all builds failed:")
        print(req.text)
        return None

def downloadArtifact(token,proj,buildId):
    print("Downloading artifact for build "+buildId+"...")
    url = "https://gitlab.com/api/v3/projects/"+proj+"/builds/"+buildId+"/artifacts"
    headers = {"PRIVATE-TOKEN":token}
    req = requests.get(url,headers=headers)
    if req.status_code == requests.codes.ok:
        with open("old_lists.zip",'wb') as f:
            f.write(req.content)
    return req.status_code == requests.codes.ok

def unpackArtifact():
    print("Unpacking most recent problem list...")
    with ZipFile("old_lists.zip",'r') as lists:
        lists.extract(max(lists.namelist()))

def main():
    parser = argparse.ArgumentParser(description="Download problem list "
            +"artifact and unpack it")
    parser.add_argument("token",help="The GitLab Access Token")
    parser.add_argument("proj",help="The project for which to download artifact")
    parser.add_argument("ref",help="The branch for which to download artifact")
    args = parser.parse_args()
    buildId = getRecentSuccBuild(args.token,args.proj,args.ref)
    if buildId:
        if downloadArtifact(args.token,args.proj,buildId):
            unpackArtifact()

if __name__=="__main__":
    main()
