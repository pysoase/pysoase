# Continuous Integration and Automated Testing

This directory contains a few scripts and configuration files for Continuous
Integration. For details, see the [documentation](http://pysoase.readthedocs.io/en/stable/automation.html#continuous-integration-and-automated-testing).

###### `artifact_download.py`
This is a helper script for the [gitlab.com](http://gitlab.com) CI. It provides
the ability to download artifact files from the most recent successful build.
These artifact files contain *pickled* lists of problems, which are later
compared to determine whether a specific commit has passed or failed.
It makes use of the [GitLab REST API](https://docs.gitlab.com/ce/api/).

###### `gitlab.yml`
This is the configuration for the GitLab CI implementation. Copy it to your
mod repository's root directory as `.gitlab-ci.yml` and commit it. After
pushing it to your GitLab instance, the first test will be run. For every
push, the last commit of that push will be checked out and full mod check 
will be run on it. In a second phase, the problems discovered will be
compared to the problems of the most recent previous push. The test will 
only fail if the current push added new problems. Otherwise, it will pass.
For more details, see the [documentation](http://pysoase.readthedocs.io/en/stable/automation.html#gitlab-ci).
